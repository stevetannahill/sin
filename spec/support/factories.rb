Factory.define :user do |f|
  f.sequence(:email) { |n| "admin#{n}@example.com" }
  f.password "foobar"
  f.password_confirmation { |u| u.password }
  
  f.association :privilege
  f.association :service_provider
end

Factory.define :privilege do |f|
  f.sequence(:name) { |n| "Test Privilege #{n}" }
end

Factory.define :service_provider do |f|
  f.sequence(:name) { |n| "Test Provider #{n}" }
  f.is_system_owner true
end