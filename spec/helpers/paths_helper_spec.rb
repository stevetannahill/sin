require 'spec_helper'
include PathsHelper

describe PathsHelper do

  describe "display_value" do
    
    it "should handle nil" do
      display_value(nil).should == nil
      display_value("").should == ""
    end
    
    it "should replace spaces with html" do
      display_value("a b").should == "a&nbsp;b"
      display_value("a  b").should == "a&nbsp;&nbsp;b"
    end
    
  end

end