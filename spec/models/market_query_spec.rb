require 'spec_helper'

describe MarketQuery do

  before(:each) do
    Time.stub!(:now).and_return(Time.parse('2010-09-21 23:15:20'))
    @market_query = MarketQuery.new
  end

  describe "appstats (multiple dbs)" do
    
    it "should return to the activerecord connection, not appstats" do
      before_conn = ActiveRecord::Base.connection
      MarketQuery.go("# regions",:run)
      after_conn = ActiveRecord::Base.connection
      before_conn.instance_variable_get(:@config).should == after_conn.instance_variable_get(:@config)
    end

  end

  describe "#available_action?" do
    
    it "should be false for nil" do
      MarketQuery.available_action?(nil).should == false
      MarketQuery.available_action?("").should == false
    end

    it "should be false for blah" do
      MarketQuery.available_action?("blah").should == false
    end

    it "should be true for buildings" do
      MarketQuery.available_action?("buildings").should == true
      MarketQuery.available_action?("Buildings").should == true
      MarketQuery.available_action?("building").should == true
    end

    it "should be true for addresses" do
      MarketQuery.available_action?("addresses").should == true
      MarketQuery.available_action?("Addresses").should == true
      MarketQuery.available_action?("address").should == true
    end

    it "should be true for users" do
      MarketQuery.available_action?("users").should == true
      MarketQuery.available_action?("user").should == true
      MarketQuery.available_action?("User").should == true
    end

    it "should be true for members" do
      MarketQuery.available_action?("members").should == true
      MarketQuery.available_action?("member").should == true
      MarketQuery.available_action?("Members").should == true
    end

    
  end

  describe "#query_to_sql" do
  
    it "should be nil if no query" do
      @market_query.query_to_sql.should == "select 0 as num"
    end
    
    it "should be based on query" do
      @market_query.query = Appstats::Query.new(:query => "# buildings")
      @market_query.query_to_sql.should == @market_query.query.query_to_sql
    end
    
  end
  
  describe "#group_query_to_sql" do
  
    it "should be nil if no query" do
      @market_query.group_query_to_sql.should == nil
    end
    
    it "should be based on query" do
      @market_query.query = Appstats::Query.new(:query => "# buildings")
      @market_query.group_query_to_sql.should == @market_query.query.group_query_to_sql
    end
    
  end
  
  describe "#go" do
  
    before(:each) do
      Appstats::Result.delete_all
      Appstats::SubResult.delete_all
      Appstats::ResultJob.delete_all
      
      @market_query = MarketQuery.new 
      MarketQuery.stub!(:new).and_return(@market_query)
    end
  
    it "should support :find when no query exists" do
      @market_query.stub!(:find).and_return("call-worked")
      MarketQuery.go("# users",:find).should == "call-worked"
    end
  
    it "should support :run and do the query" do
      @market_query.stub!(:run).and_return("call-worked")
      MarketQuery.go("# users",:run).should == "call-worked"
    end
  
  
  end
  
  describe "#process_query" do
    
    it "should support nil query" do
      @market_query.process_query
      @market_query.query_to_sql.should == "select 0 as num"
      @market_query.group_query_to_sql.should == nil
    end
  
    it "should be case insensitive on action" do
      @market_query.query = Appstats::Query.new(:query => "# Buildings", :query_type => "MarketQuery")
      @market_query.process_query
      @market_query.query_to_sql.should == "select COUNT(DISTINCT operator_accesses.id) as num from operator_accesses left join physical_addresses on physical_addresses.id = operator_accesses.physical_address_id left join operator_networks on operator_networks.id = operator_accesses.operator_network_id left join service_providers on service_providers.id = operator_networks.service_provider_id"
  
      @market_query.query = Appstats::Query.new(:query => "# bUildings", :query_type => "MarketQuery")
      @market_query.process_query
      @market_query.query_to_sql.should == "select COUNT(DISTINCT operator_accesses.id) as num from operator_accesses left join physical_addresses on physical_addresses.id = operator_accesses.physical_address_id left join operator_networks on operator_networks.id = operator_accesses.operator_network_id left join service_providers on service_providers.id = operator_networks.service_provider_id"
    end
  
    it "should support singular names" do
      @market_query.query = Appstats::Query.new(:query => "# building", :query_type => "MarketQuery")
      @market_query.process_query
      @market_query.query_to_sql.should == "select COUNT(DISTINCT operator_accesses.id) as num from operator_accesses left join physical_addresses on physical_addresses.id = operator_accesses.physical_address_id left join operator_networks on operator_networks.id = operator_accesses.operator_network_id left join service_providers on service_providers.id = operator_networks.service_provider_id"
    end
  
    it "should handle nil actions" do
      @market_query.query = Appstats::Query.new(:query => "", :query_type => "MarketQuery")
      @market_query.process_query
      @market_query.query_to_sql.should == "select 0 as num"
    end
  
    it "should call query.run" do
      @market_query.query = Appstats::Query.new(:query => "# users on smeagol.cenx.localnet", :query_type => "MarketQuery")
      @market_query.query.stub!(:run).and_return("call-worked")
      @market_query.run.should == "call-worked"
    end
    
    it "should support brackets and in clauses" do
      @market_query.query = Appstats::Query.new(:query => "# addresses where address = a or ( address in 1,2,3 )", :query_type => "MarketQuery")
      @market_query.process_query
      @market_query.query_to_sql.should == "select COUNT(DISTINCT physical_addresses.id) as num from physical_addresses left join operator_accesses on operator_accesses.physical_address_id = physical_addresses.id left join operator_networks on operator_networks.id = operator_accesses.operator_network_id left join service_providers on service_providers.id = operator_networks.service_provider_id where physical_addresses.address = 'a' or ( physical_addresses.address in ('1','2','3') )"
      @market_query.group_query_to_sql.should == nil
    end
  
    # THIS IS A SLOW TEST ONLY TO BE RUN IF THINGS START ACTING STRANGE
    # it "should actually execute code properly" do
    #   @market_query.query = Appstats::Query.new(:query => "# users on smeagol.cenx.localnet", :query_type => "MarketQuery")
    #   @market_query.run.count.should > 0
    # end
    
    describe "# buildings" do
      
      it "should support #buildings" do
        @market_query.query = Appstats::Query.new(:query => "# buildings", :query_type => "MarketQuery")
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT operator_accesses.id) as num from operator_accesses left join physical_addresses on physical_addresses.id = operator_accesses.physical_address_id left join operator_networks on operator_networks.id = operator_accesses.operator_network_id left join service_providers on service_providers.id = operator_networks.service_provider_id"
        @market_query.group_query_to_sql.should == nil
      end
      
      it "should support group by cenx_member and address lookups" do
        @market_query.query = Appstats::Query.new(:query => "# buildings group by diverse, cenx_member", :query_type => "MarketQuery")        
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT operator_accesses.id) as num from operator_accesses left join physical_addresses on physical_addresses.id = operator_accesses.physical_address_id left join operator_networks on operator_networks.id = operator_accesses.operator_network_id left join service_providers on service_providers.id = operator_networks.service_provider_id"
        @market_query.group_query_to_sql.should == "select 'diverse,cenx_member' as context_key_filter, concat(ifnull(operator_accesses.is_physically_diverse,'--'),',',ifnull(service_providers.name,'--')) as context_value_filter, COUNT(*) as num from operator_accesses left join physical_addresses on physical_addresses.id = operator_accesses.physical_address_id left join operator_networks on operator_networks.id = operator_accesses.operator_network_id left join service_providers on service_providers.id = operator_networks.service_provider_id group by context_value_filter"
      end
      
      it "should support group by media,network" do
        @market_query.query = Appstats::Query.new(:query => "# buildings group by media, network", :query_type => "MarketQuery")        
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT operator_accesses.id) as num from operator_accesses left join physical_addresses on physical_addresses.id = operator_accesses.physical_address_id left join operator_networks on operator_networks.id = operator_accesses.operator_network_id left join service_providers on service_providers.id = operator_networks.service_provider_id"
        @market_query.group_query_to_sql.should == "select 'media,network' as context_key_filter, concat(ifnull(media_types.name,'--'),',',ifnull(operator_networks.name,'--')) as context_value_filter, COUNT(*) as num from operator_accesses left join physical_addresses on physical_addresses.id = operator_accesses.physical_address_id left join operator_networks on operator_networks.id = operator_accesses.operator_network_id left join service_providers on service_providers.id = operator_networks.service_provider_id group by context_value_filter"
      end

      it "should where cenx_member" do
        @market_query.query = Appstats::Query.new(:query => "# buildings where cenx_member = My Service Provider", :query_type => "MarketQuery")
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT operator_accesses.id) as num from operator_accesses left join physical_addresses on physical_addresses.id = operator_accesses.physical_address_id left join operator_networks on operator_networks.id = operator_accesses.operator_network_id left join service_providers on service_providers.id = operator_networks.service_provider_id where service_providers.name = 'My Service Provider'"
        @market_query.group_query_to_sql.should == nil
      end
      
    end
    
    describe "# regions" do
      
      it "should support #buildings" do
        @market_query.query = Appstats::Query.new(:query => "# regions", :query_type => "MarketQuery")
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT regions.id) as num from regions left join service_providers on service_providers.id = regions.service_provider_id"
        @market_query.group_query_to_sql.should == nil
      end
      
      it "should support group by region_name,private,drawn, created_at,updated_at, cenx_member" do
        @market_query.query = Appstats::Query.new(:query => "# regions group by region_name, private, drawn, created_at, updated_at, cenx_member", :query_type => "MarketQuery")        
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT regions.id) as num from regions left join service_providers on service_providers.id = regions.service_provider_id"
        @market_query.group_query_to_sql.should == "select 'region_name,private,drawn,created_at,updated_at,cenx_member' as context_key_filter, concat(ifnull(regions.name,'--'),',',ifnull(regions.private,'--'),',',ifnull(regions.drawn,'--'),',',ifnull(regions.created_at,'--'),',',ifnull(regions.updated_at,'--'),',',ifnull(service_providers.name,'--')) as context_value_filter, COUNT(*) as num from regions left join service_providers on service_providers.id = regions.service_provider_id group by context_value_filter"
      end

      it "should where region_name,private,drawn, created_at,updated_at, cenx_member" do
        @market_query.query = Appstats::Query.new(:query => "# buildings where region_name = 1 and private = true and drawn = false and created_at = '2011-10-11' and updated_at = '2011-10-12' and cenx_member = Telus", :query_type => "MarketQuery")
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT operator_accesses.id) as num from operator_accesses left join physical_addresses on physical_addresses.id = operator_accesses.physical_address_id left join operator_networks on operator_networks.id = operator_accesses.operator_network_id left join service_providers on service_providers.id = operator_networks.service_provider_id where 1=1 and 1=1 and 1=1 and operator_accesses.created_at = '2011-10-11' and operator_accesses.updated_at = '2011-10-12' and service_providers.name = 'Telus'"
        @market_query.group_query_to_sql.should == nil
      end

    end
    
    describe "# members" do
      
      it "should support #members" do
        @market_query.query = Appstats::Query.new(:query => "# members", :query_type => "MarketQuery")
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT service_providers.id) as num from service_providers"
        @market_query.group_query_to_sql.should == nil
      end
      
      it "should support group by cenx_member" do
        @market_query.query = Appstats::Query.new(:query => "# members group by cenx_member", :query_type => "MarketQuery")        
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT service_providers.id) as num from service_providers"
        @market_query.group_query_to_sql.should == "select 'cenx_member' as context_key_filter, concat(ifnull(service_providers.name,'--')) as context_value_filter, COUNT(*) as num from service_providers group by context_value_filter"
      end

      it "should support where cenx_member" do
        @market_query.query = Appstats::Query.new(:query => "# members where cenx_member = 123", :query_type => "MarketQuery")        
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT service_providers.id) as num from service_providers where service_providers.name = '123'"
        @market_query.group_query_to_sql.should == nil
      end

    end
    
    describe "# projects" do
      
      it "should support #projects" do
        @market_query.query = Appstats::Query.new(:query => "# projects", :query_type => "MarketQuery")
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT projects.id) as num from projects left join users on users.id = projects.user_id left join service_providers on service_providers.id = users.service_provider_id"
        @market_query.group_query_to_sql.should == nil
      end
      
      it "should support group by cenx_member, email, first_name, last_name, created_at,updated_at" do
        @market_query.query = Appstats::Query.new(:query => "# projects group by cenx_member, email, first_name, last_name, created_at,updated_at", :query_type => "MarketQuery")        
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT projects.id) as num from projects left join users on users.id = projects.user_id left join service_providers on service_providers.id = users.service_provider_id"
        @market_query.group_query_to_sql.should == "select 'cenx_member,email,first_name,last_name,created_at,updated_at' as context_key_filter, concat(ifnull(service_providers.name,'--'),',',ifnull(users.email,'--'),',',ifnull(users.first_name,'--'),',',ifnull(users.last_name,'--'),',',ifnull(projects.created_at,'--'),',',ifnull(projects.updated_at,'--')) as context_value_filter, COUNT(*) as num from projects left join users on users.id = projects.user_id left join service_providers on service_providers.id = users.service_provider_id group by context_value_filter"
      end

      it "should support where cenx_member, email, first_name, last_name, created_at,updated_at" do
        @market_query.query = Appstats::Query.new(:query => "# projects where cenx_member = TWC and email = andrew.forward@cenx.com and first_name = andrew and last_name = forward and created_at = 2011-01-02 and updated_at = 2011-02-03", :query_type => "MarketQuery")        
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT projects.id) as num from projects left join users on users.id = projects.user_id left join service_providers on service_providers.id = users.service_provider_id where service_providers.name = 'TWC' and 1=1 and 1=1 and 1=1 and 1=1 and users.last_name = 'forward' and projects.created_at = '2011-01-02' and projects.updated_at = '2011-02-03'"
        @market_query.group_query_to_sql.should == nil
      end


    end

    describe "# addresses" do
      
      it "should support #addresses" do
        @market_query.query = Appstats::Query.new(:query => "# addresses", :query_type => "MarketQuery")
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT physical_addresses.id) as num from physical_addresses left join operator_accesses on operator_accesses.physical_address_id = physical_addresses.id left join operator_networks on operator_networks.id = operator_accesses.operator_network_id left join service_providers on service_providers.id = operator_networks.service_provider_id"
        @market_query.group_query_to_sql.should == nil
      end
      
      it "should understand address columns" do
        @market_query.query = Appstats::Query.new(:query => "# addresses where address = a and postal_code like '%b' and longitude < 10 and latitude > 11 || city = 'x' && state = 'y' and country = 'z' and address2 = 'p' and address_type = q and created_at > 'a' and updated_at > 'b'", :query_type => "MarketQuery")
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT physical_addresses.id) as num from physical_addresses left join operator_accesses on operator_accesses.physical_address_id = physical_addresses.id left join operator_networks on operator_networks.id = operator_accesses.operator_network_id left join service_providers on service_providers.id = operator_networks.service_provider_id where physical_addresses.address = 'a' and physical_addresses.postal_code like '%b' and physical_addresses.longitude < '10' and physical_addresses.latitude > '11' or physical_addresses.city = 'x' and physical_addresses.state = 'y' and physical_addresses.country = 'z' and physical_addresses.address2 = 'p' and physical_addresses.address_type = 'q' and physical_addresses.created_at > 'a' and physical_addresses.updated_at > 'b'"
        @market_query.group_query_to_sql.should == nil
      end
  
      it "should ignore other columns" do
        @market_query.query = Appstats::Query.new(:query => "# addresses where blah = 1", :query_type => "MarketQuery")
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT physical_addresses.id) as num from physical_addresses left join operator_accesses on operator_accesses.physical_address_id = physical_addresses.id left join operator_networks on operator_networks.id = operator_accesses.operator_network_id left join service_providers on service_providers.id = operator_networks.service_provider_id where 1=1"
        @market_query.group_query_to_sql.should == nil
      end
  
      it "should ignore context_keys only (no value)" do
        @market_query.query = Appstats::Query.new(:query => "# addresses where blah = 1 and address = a", :query_type => "MarketQuery")
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT physical_addresses.id) as num from physical_addresses left join operator_accesses on operator_accesses.physical_address_id = physical_addresses.id left join operator_networks on operator_networks.id = operator_accesses.operator_network_id left join service_providers on service_providers.id = operator_networks.service_provider_id where 1=1 and physical_addresses.address = 'a'"
        @market_query.group_query_to_sql.should == nil
      end
      
      it "should support group by" do
        @market_query.query = Appstats::Query.new(:query => "# addresses group by address, cenx_member", :query_type => "MarketQuery")        
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT physical_addresses.id) as num from physical_addresses left join operator_accesses on operator_accesses.physical_address_id = physical_addresses.id left join operator_networks on operator_networks.id = operator_accesses.operator_network_id left join service_providers on service_providers.id = operator_networks.service_provider_id"
        @market_query.group_query_to_sql.should == "select 'address,cenx_member' as context_key_filter, concat(ifnull(physical_addresses.address,'--'),',',ifnull(service_providers.name,'--')) as context_value_filter, COUNT(*) as num from physical_addresses left join operator_accesses on operator_accesses.physical_address_id = physical_addresses.id left join operator_networks on operator_networks.id = operator_accesses.operator_network_id left join service_providers on service_providers.id = operator_networks.service_provider_id group by context_value_filter"
      end
      
    end
  
    describe "# users" do
      
      it "should support #users" do
        @market_query.query = Appstats::Query.new(:query => "# users", :query_type => "MarketQuery")
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT users.id) as num from users left join service_providers on service_providers.id = users.service_provider_id"
        @market_query.group_query_to_sql.should == nil
      end
  
      it "should support email, first_name, last_name, last_login_at, created_at, updated_at, cenx_member" do
        @market_query.query = Appstats::Query.new(:query => "# users where email = a and first_name = b and last_name = c and last_login_at = d and created_at = e and updated_at = f and cenx_member = g", :query_type => "MarketQuery")
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT users.id) as num from users left join service_providers on service_providers.id = users.service_provider_id where users.email = 'a' and users.first_name = 'b' and users.last_name = 'c' and users.last_login_at = 'd' and users.created_at = 'e' and users.updated_at = 'f' and service_providers.name = 'g'"
        @market_query.group_query_to_sql.should == nil
      end
      
      it "should support group by" do
        @market_query.query = Appstats::Query.new(:query => "# users group by email, cenx_member", :query_type => "MarketQuery")
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT users.id) as num from users left join service_providers on service_providers.id = users.service_provider_id"
        @market_query.group_query_to_sql.should == "select 'email,cenx_member' as context_key_filter, concat(ifnull(users.email,'--'),',',ifnull(service_providers.name,'--')) as context_value_filter, COUNT(*) as num from users left join service_providers on service_providers.id = users.service_provider_id group by context_value_filter"
      end
      
    end
    
    describe "# market-sharings" do
  
      it "should support # market-sharings" do
        @market_query.query = Appstats::Query.new(:query => "# market-sharing", :query_type => "MarketQuery")
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT operator_security_profiles.id) as num from operator_security_profiles left join service_providers sellers on sellers.id = operator_security_profiles.owning_service_provider_id left join service_providers buyers on buyers.id = operator_security_profiles.applies_to_service_provider_id"
        @market_query.group_query_to_sql.should == nil
      end
  
      it "should support seller, buyer, show_building_lists, show_regions, show_location_details, show_service_types, show_service_guarantees" do
        @market_query.query = Appstats::Query.new(:query => "# market-sharing where seller = a and buyer = 'b' or show_building_lists = true or show_regions = false and show_location_details = 1 || show_service_types = 0 or show_service_guarantees = Yes or show_service_guarantees = No", :query_type => "MarketQuery")
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT operator_security_profiles.id) as num from operator_security_profiles left join service_providers sellers on sellers.id = operator_security_profiles.owning_service_provider_id left join service_providers buyers on buyers.id = operator_security_profiles.applies_to_service_provider_id where sellers.name = 'a' and buyers.name = 'b' or operator_security_profiles.served_locations_visible = 1 or operator_security_profiles.served_regions_visible = 0 and operator_security_profiles.operator_accesses_visible = 1 or operator_security_profiles.service_types_visible = 0 or operator_security_profiles.service_guarantees_visible = 1 or operator_security_profiles.service_guarantees_visible = 0"
        @market_query.group_query_to_sql.should == nil
      end
  
      it "should support group by" do
        @market_query.query = Appstats::Query.new(:query => "# market-sharing group by seller, buyer", :query_type => "MarketQuery")
        @market_query.process_query
        @market_query.query_to_sql.should == "select COUNT(DISTINCT operator_security_profiles.id) as num from operator_security_profiles left join service_providers sellers on sellers.id = operator_security_profiles.owning_service_provider_id left join service_providers buyers on buyers.id = operator_security_profiles.applies_to_service_provider_id"
        @market_query.group_query_to_sql.should == "select 'seller,buyer' as context_key_filter, concat(ifnull(sellers.name,'--'),',',ifnull(buyers.name,'--')) as context_value_filter, COUNT(*) as num from operator_security_profiles left join service_providers sellers on sellers.id = operator_security_profiles.owning_service_provider_id left join service_providers buyers on buyers.id = operator_security_profiles.applies_to_service_provider_id group by context_value_filter"
      end
      
      
    end
  
    
  end
  
  describe "#db_connection" do
    
    it "should use the extract_env" do
      @market_query.query = Appstats::Query.new(:query => "# blahs on market-eval.cenx.localnet")
      conn = OpenStruct.new(:connection => "setup-properly")
      ActiveRecord::Base.stub!(:establish_connection).with(:market_eval).and_return(conn)
      @market_query.db_connection.should == "setup-properly"
    end
    
  end
  
  describe "#extract_env" do
  
    it "should support local" do
      MarketQuery.extract_env('local.cenx.localnet').should == :market_local
      MarketQuery.extract_env('local').should == :market_local
    end
    
    it "should default based on the Rails.env" do
      MarketQuery.extract_env(nil).should == :market_test
      MarketQuery.extract_env('').should == :market_test
      MarketQuery.extract_env('blah').should == :market_test
    end
  
    it "should handle hayden.cenx.localnet" do
      MarketQuery.extract_env('hayden.cenx.localnet').should == :market_hayden
      MarketQuery.extract_env('hayden').should == :market_hayden
    end
  
    it "should handle smeagol.cenx.localnet" do
      MarketQuery.extract_env('smeagol.cenx.localnet').should == :market_dev
      MarketQuery.extract_env('smeagol').should == :market_dev
    end
  
    it "should handle market.cenx.com" do
      MarketQuery.extract_env('market.cenx.com').should == :market_live
      MarketQuery.extract_env('market.cenx.localnet').should == :market_live
      MarketQuery.extract_env('market').should == :market_live
    end
  
    it "should handle market-eval.cenx.com" do
      MarketQuery.extract_env('market-eval.cenx.com').should == :market_eval
      MarketQuery.extract_env('market-eval.cenx.localnet').should == :market_eval
      MarketQuery.extract_env('market-eval').should == :market_eval
    end
    
    it "should handle market-demo.cenx.com" do
      MarketQuery.extract_env('market-demo.cenx.com').should == :market_demo
      MarketQuery.extract_env('market-demo.cenx.localnet').should == :market_demo
      MarketQuery.extract_env('market-demo').should == :market_demo
    end
  
  end

end
