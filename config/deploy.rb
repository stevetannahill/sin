require "bundler/capistrano"
require 'thinking_sphinx/deploy/capistrano'

#-----------------
# CUSTOM
#-----------------

set :application, "sin"
set :repo_name, application

before "deploy:update_code",       "deploy:monit:disable" unless exists?(:headless)
after "deploy:update_code",       "deploy:copy_database_configuration"
after "deploy:update_code",       "deploy:copy_sphinx_configuration"
after "deploy:update_code",       "deploy:copy_shared_sphinx_folder"
after "deploy:update_code",       "deploy:copy_cas_configuration"
after "deploy:update_code",       "deploy:copy_cenx_apps_configuration"
# SHOULD ONLY BE RUN AGAINST sprint_rollout environment
# possibly push into ./deploy/austin.rb and ./deploy/sprint-rollout.rb
# after "deploy:update_code",       "deploy:hack_copy_ordering_screenshots"
after "deploy:update_code",       "deploy:fix_sessions_dir"
after "deploy:update_code",       "deploy:sphinx:index"
after "deploy:update_code",       "deploy:sphinx:configure_and_start"
after "deploy:update_code",       "deploy:copy_oss_db_mail_config"
after "deploy:update_code",       "deploy:copy_osl_poe_config"
after "deploy:update_code",       "deploy:copy_google_config"
after "deploy:update_code",       "deploy:populate_provider_logos"
after "deploy:symlink",           "deploy:monit:copy_configs" unless exists?(:headless)
after "deploy:symlink",           "deploy:monit:enable" unless exists?(:headless)

set :whenever_command, "bundle exec whenever"
require "whenever/capistrano"

namespace :deploy do
  #TODO reused in sin's deploy.rbenv.rb need to pull this out into tasks, or something
  namespace :monit do
    [:disable,:enable,:restart].each do |action|
      desc "#{action.to_s.capitalize} all monitoring processes during a deploy"
      task action do
        [:sin_sphinx].each do |monit_name|
          if action == :disable
            run("monit stop #{monit_name}; true") #unmonitor will fail if it isn't configured yet, and that's fine
          elsif action == :enable
            run("while ! monit monitor #{monit_name}; do ruby -e 'sleep 0.5'; done")
          else
            run("while ! monit #{action} #{monit_name}; do ruby -e 'sleep 0.5'; done")
          end
        end
      end
    end
    
    desc "copy the monit.d config files into position"
    task :copy_configs do
      run("cp #{release_path}/config/monit.d/*.cnf ~/monit.d/#{application}")
      run("monit reload")
      sleep 2 # without this a monit enable shortly after fails
    end
  end
  
  namespace :sphinx do
    task :configure_and_stop, :roles => :app do
      thinking_sphinx.stop
    end
    
    task :configure_and_start, :roles => :app do
      thinking_sphinx.start
    end
    
    task :index, :roles => :app do
      thinking_sphinx.index
    end
  end
end

#-----------------
# COMMON DEPLOY TASKS
#-----------------

set :scm, :git 
set :repository,  "git@smeagol.cenx.localnet:#{repo_name}.git" # Must be accessible from each deployment location
set :user, "deployer"
set :deploy_to, "/cenx/#{application}"
set :shared_sensitive_path, "/cenx/sensitive"
set :use_sudo, false
set :default_stage, "staging"
set :deploy_via, :remote_cache # Makes deploying much faster since it caches the git repo on the target server instead of a full clone each time
default_run_options[:pty] = true

load 'deploy/assets'
set :normalize_asset_timestamps, false # Stops capistrano from trying to update timestamps on asset files
