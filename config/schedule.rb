# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever


set :output, 'log/cron.log'
job_type :rake, "/home/deployer/local/bin/sin_env bundle exec rake :task --silent"
job_type :runner,  "/home/deployer/local/bin/sin_env bundle exec rails runner ':task'"

every :hour do
  rake "thinking_sphinx:index"
end

every 5.minutes do
  rake "reindex:check"
end
