#path relative to lib
require Rails.root + 'lib/date_range'
require Rails.root + 'lib/time_duration'
require Rails.root + 'lib/cdb/constants/common_types'
require Rails.root + 'lib/cdb/constants/common_helpers'
require Rails.root + 'lib/cdb/validations'
require Rails.root + 'lib/cdb/pop_seeds/common_pop'
require Rails.root + 'lib/cdb/pop_seeds/standard_exchange_7750'
require Rails.root + 'lib/cdb/pop_seeds/standard_exchange_7450'
require Rails.root + 'lib/cdb/pop_seeds/light_squared_exchange_7450_TAP'
require Rails.root + 'lib/cdb/pop_seeds/light_squared_exchange_7450_RSC'
require Rails.root + 'lib/cdb/pop_seeds/light_squared_exchange_7210_SGW'
require Rails.root + 'lib/cdb/pop_seeds/sprint_msc_samsung'
require Rails.root + 'lib/cdb/pop_seeds/sprint_msc_alu'
require Rails.root + 'lib/cdb/pop_seeds/sprint_msc_ericsson'
require Rails.root + 'lib/cdb/pop_seeds/cox_retail_agg_site'
require Rails.root + 'lib/cdb/pop_seeds/cox_wholesale_agg_site'
require Rails.root + 'lib/cdb/tools/sw_err'
require Rails.root + 'lib/cdb/sam_stats_if/sam_api'
require Rails.root + 'lib/cdb/bx_stats_if/bx_api'
require Rails.root + 'lib/cdb/appstats_helpers.rb'
require Rails.root + 'lib/cdb/version'
require Rails.root + 'lib/cdb/app'
require Rails.root + 'lib/cdb/name_tools/cenx_name_generator'
require Rails.root + 'lib/cdb/name_tools/qos_policy_helper.rb'
require Rails.root + 'lib/cdb/ruby-bsearch-1.5/bsearch.rb'
require Rails.root + 'lib/cdb/id_tools/cenx_id_generator.rb'
require Rails.root + 'lib/cdb/modules/common_exception.rb'
require Rails.root + 'lib/cdb/modules/exception_reports.rb'

# Try instantiate Demarc to see if database has been migrated yet since Circuit Builder is attemptimg to include Models
# TODO: Not Pretty but it works
begin 
  Demarc.new 
rescue 
  puts 'No tables yet, so CircuitBuilder not loaded. !!!!!!!!!!!!!!!!!!!!!!!!!'
else 
  require Rails.root + 'lib/cdb/model_maker/requires.rb' 
end
