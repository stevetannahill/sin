class Array
  # Iterate over array, but supply previous and next items as well
  # [1..10].each_with_prev_next do |prev, curr, nxt|
  def each_with_prev_next &block
    # ruby 1.9 could be [nil, *self, nil], splat operator (*) would expand the array into multiple elements, like flatten(1)
    # ([nil] + self + [nil]).each_cons(3, &block) works for ruby 1.8
    ([nil, *self, nil]).each_cons(3, &block)
  end

  def list(delimiter=', ')
    self.uniq.join(delimiter)
  end
  
end