# configuration for CDB integration with SIN
# Note: Some configuration is in application.rb (as needed by the initialization process.)
require 'factory_girl'

# Let Factory Girl know where to find the cdb factories
FactoryGirl::definition_file_paths << "test/factories/cdb"