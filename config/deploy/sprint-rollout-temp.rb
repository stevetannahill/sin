# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
# set :branch, "ericsson5_demo"
set :branch, "nrm_demo"
set :versionable, false

role :web, "sprint-rollout-temp.cenx.localnet"                   # Your HTTP server, Apache/etc
role :app, "sprint-rollout-temp.cenx.localnet"                   # This may be the same as your `Web` server
role :db,  "sprint-rollout-temp.cenx.localnet", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"
