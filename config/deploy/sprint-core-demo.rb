# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
set :branch, "segment_as_starting_component"
set :versionable, false

role :web, "sprint-core-demo.cenx.localnet"                   # Your HTTP server, Apache/etc
role :app, "sprint-core-demo.cenx.localnet"                   # This may be the same as your `Web` server
role :db,  "sprint-core-demo.cenx.localnet", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"
