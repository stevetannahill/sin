# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
set :branch, "2010Sep_Telus_Demo"
set :versionable, false

role :web, "market-eval.cenx.localnet"                          # Your HTTP server, Apache/etc
role :app, "market-eval.cenx.localnet"                          # This may be the same as your `Web` server
role :db,  "market-eval.cenx.localnet", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"
