set :branch, "sprint_rollout"
set :versionable, false

role :web, "dallas.cenx.localnet"                   # Your HTTP server, Apache/etc
role :app, "dallas.cenx.localnet"                   # This may be the same as your `Web` server
role :db,  "dallas.cenx.localnet", :primary => true # This is where Rails migrations will run

set :monit_configs, [ ]
