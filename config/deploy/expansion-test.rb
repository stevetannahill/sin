# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
set :branch, "master"
set :versionable, false

role :web, "expansion-test.cenx.test"                   # Your HTTP server, Apache/etc
role :app, "expansion-test.cenx.test"                   # This may be the same as your `Web` server
role :db,  "expansion-test.cenx.test", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"
