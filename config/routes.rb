Sin::Application.routes.draw do

  root :to => 'application#index'

  match '/support' => 'support#index', :as => :support
  match '/logout' => 'sin#logout', :as => :logout
  match '/logo' => 'sin#logo', :as => :logo
  match '/session_expired' => 'sin#session_expired', :as => :session_expired
  match '/login_text' => 'sin#login_text', :as => :login_text

  resources :password_resets, :except => [:index, :destroy]
  resources :roles
  resources :service_providers

  resources :schedules do
    member do
      post   :generate
      get    :reports
      delete :destroy_report
    end
  end  

  resources :settings do
    collection do
      post :update
      get :edit
    end
  end

  resources :users do
    put :set_homepage
    put :toggle_table
    resources :filter_templates, :only => [:create, :update, :destroy]
    resources :site_lists, :only => [:create, :update, :destroy]
  end

  resource :dashboard, :only => [:index] do
    get :index
    get :inventory
    get :ordering
    get :monitoring
    resources :event, :controller => :dashboards, :only => :show do
      collection do
        post :data
      end
    end
  end
  
  resource :report, :only => [:index] do
    get :index    
    get :paths

    get :sla
    get :site_group
    get :satellite_site
    get :cell_site
    get :cell_site_path
    get :metrics
    get :oem_sla
    get :fallout_exception
    get :circuit_utilization
    get :enni_utilization
    
    # required for datatables
    post :sla
    post :site_group
    post :satellite_site
    post :cell_site
    post :cell_site_path
    post :metrics
    post :oem_sla
    post :circuit_utilization
    post :enni_utilization
    post :paths
  end
  
  resources :member_handle_instances do
    collection do
      post :create_or_update
    end
  end

  def standard_component_routes
    member do
      get :ordering
      get :ordering_state
      get :ordering_state_edit
      get :ordering_attributes
      get :ordering_output
      get :ordering_audit
      put :ordering_field_update
      put :provisioning_state_update
      get :monitoring
      get :sm_state
      get :event_history
      get :availability_graph
      get :delay_graph
      get :delay_variation_graph
      get :frame_loss_ratio_graph
      get :troubleshooting_graph
    end
  end

  # Map network on map (agg sites and demarcs)
  resources :network do
    collection do 
      get :demarcs_by_site
    end
  end

  resources :monitoring

  resources :service_orders, :only => [:update]
  match 'service_orders/:id/place_order/:path_id', :to => 'service_orders#place_order', :via => [:post], :as => "place_service_order"
  match 'service_orders/screenshot/:cascade/:order_type/:ts/:num.png', :to => 'service_orders#screenshot', :as => "service_order_screenshot"

  resources :paths do
    standard_component_routes
    member do
      get :list_row
      get :revision
      get :view_data_sources
    end  
  end

  resources :cos_test_vectors do
    member do
      get :availability_graph
      get :delay_graph
      get :delay_variation_graph
      get :frame_loss_ratio_graph
      get :troubleshooting_graph
      get :network_monitoring
    end    
  end

  resources :off_net_ovcs do
    standard_component_routes
    collection do 
      get :map_info_window
      get :map_tooltip
    end
  end

  resources :on_net_ovcs do
    standard_component_routes
    member do
      get :genconfig
      get :site
    end   
    collection do 
      get :map_info_window
      get :map_tooltip
    end     
  end

  resources :nodes do
    member do
      put :provisioning_state_update
    end
    collection do 
      get :nodes
    end
  end

  resources :on_net_routers do
    standard_component_routes
    member do
      get :genconfig
      get :nodes
      get :site
    end   
    collection do 
      get :map_info_window
    end     
  end

  resources :sites do
    member do 
      get :map_info_window
    end
  end

  resources :demarcs do
    standard_component_routes
    collection do 
      get :map_info_window
      get :map_tooltip
    end
  end

  resources :ennis do
    standard_component_routes
    member do
      get :list_row
    end
  end

  resources :off_net_ovc_end_point_unis do
    standard_component_routes
  end

  resources :off_net_ovc_end_point_ennis do
    standard_component_routes
  end

  resources :on_net_ovc_end_point_ennis do
    standard_component_routes
  end

  resources :on_net_router_sub_ifs do
    standard_component_routes
  end

  resources :path_displays do
    member do
      get :monitoring_view
      get :ordering_state_view
      get :inventory_state_view
      get :monitoring_state_view
      get :demarc_view
      get :circuit_path_view
      get :bandwidth_view
      get :technical_contacts_view
      get :summary_view
    end
  end

  resources :enni_displays do
    member do
      get :ordering_state_view
      get :inventory_state_view
      get :monitoring_state_view
      get :technical_contacts_view
    end
  end
  
  resources :state

  # Compare graphs side-by-side
  resources :compares do
    collection do
      delete :clear
      put :reorder
    end
  end


  # Coresite Orders and Tenants
  resources :coresite_orders do
    member do 
      get :change
      put :process_change
      put :accept
      put :update
      put :reject
      put :cancel
      delete :disconnect
      get :_check_available_bandwidth
      get :_view_build_log
    end
    collection do
      get :_check_available_bandwidth_and_vlan
      get :_sites_select
      get :_operator_networks_select
      get :_ennis_select
      get :_phy_type
      get :_access_permission
      get :_provisioned_bandwidth
      get :_next_available_vlan
      get :_service_rates_select
      get :_contact
    end
  end

  resources :custom_application do
    collection do
      get :_locations
      get :_show_add_port
      get :_show_edit_port
      get :_show_delete_port
      get :_nodes_select
      post :_create_port
      put :_update_port
      put :_delete_port
      post :upload_tenants
      get :import_tenants
      get :access_permissions
      put :_update_access_permissions
    end
  end
  
end