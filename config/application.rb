require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require 'rails/all'

if defined?(Bundler)
  # If you precompile assets before deploying to production, use this line
  Bundler.require(*Rails.groups(:assets => %w(development test)))
  # If you want your assets lazily compiled in production, use this line
  # Bundler.require(:default, :assets, Rails.env)
end

module Sin
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Custom directories with classes and modules you want to be autoloadable.
    extra_lib_dirs = %W(sam_stats_if data_archive bx_stats_if stats id_tools name_tools cenx_crypt)
    config.autoload_paths += extra_lib_dirs.map{|dir_name| "#{config.root}/lib/cdb/#{dir_name}"}

    # cdb libs must be added to load path
    config.autoload_paths << "#{config.root}/lib/cdb"

    # cdb models must be added to load path
    config.autoload_paths << "#{config.root}/app/models/cdb"

    # cdb mailers must be added to load path
    config.autoload_paths << "#{config.root}/app/mailers/cdb"

    require 'find'
    # Add the state models to the load path
  	Find.find("#{config.root}/app/models/cdb/admin") do |path|
  		if FileTest.directory?(path)
  			config.autoload_paths << path
  		end
  	end

    # Only load the plugins named here, in the order given (default is alphabetical).
    # :all can be used as a placeholder for all plugins not explicitly named.
    # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

    # Activate observers that should always be running.
    # config.active_record.observers = :cacher, :garbage_collector, :forum_observer

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = "utf-8"

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password, :password_confirmation]

    # Enable the asset pipeline
    config.assets.enabled = true

    # Version of your assets, change this if you want to expire all your assets
    config.assets.version = '1.0'
    
    # Add flash asset path vendors
    config.assets.paths << "#{Rails.root}/vendor/assets/flash"

    # Include coresite extensions if coresite application, determined by existence of coresite_application.rb file
    if FileTest.file?("#{Rails.root}/config/coresite_application.rb")
      config.autoload_paths << "#{config.root}/lib/cdb/coresite"
      # to_prepare callbacks for Action Dispatch which will be ran per request in development, or before the first request in production.
      # Must be load (instead of require), so modifying code in development doesn't require a server restart
      config.to_prepare do
        load "coresite_extensions.rb"
      end
    end

  end
end
