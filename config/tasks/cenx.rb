def remote_file_exists?(full_path)
  capture("if [ -e #{full_path} ]; then echo 'true'; fi").strip == 'true'
end

before "deploy:update_code", "local:update_version"
namespace :deploy do

  task :copy_database_configuration do 
    template_file = "#{deploy_to}/sensitive/database.yml" 
    run "cp #{template_file} #{release_path}/config/database.yml"
  end

  task :fix_sessions_dir do
    run "mkdir #{release_path}/tmp/sessions"
  end

  task :copy_cas_configuration do 
    template_file = "#{shared_sensitive_path}/cas_name.rb"
    run "cp #{template_file} #{release_path}/config/initializers/cas_name.rb"
  end

  task :copy_sphinx_configuration do 
    template_file = "#{deploy_to}/sensitive/sphinx.yml"
    run "cp #{template_file} #{release_path}/config/sphinx.yml"
  end

  task :copy_cenx_apps_configuration do 
    config_file = "cenx_apps.rb"
    cenx_apps_config = "/cenx/sensitive/#{config_file}"
    run "cp #{cenx_apps_config} #{release_path}/config/initializers/#{config_file}"
  end

  task :copy_event_server_properties do 
    template_file = "#{deploy_to}/sensitive/event_server.properties"
    run "cp #{template_file} #{release_path}/lib/event/event_server.properties"
  end

  task :copy_shared_sphinx_folder do
    # Sphinx deploy setup
    # does something like this run "mkdir -p #{shared_path}/db/sphinx/production"
    thinking_sphinx.shared_sphinx_folder
  end
  
  task :enable_coresite_auth do 
    put "enabled = true", "#{release_path}/config/coresite_application.rb"
  end
  
  task :configure_coresite do 
    config_file = "coresite.yml"
    cenx_apps_config = "/cenx/sensitive/#{config_file}"
    run "cp #{cenx_apps_config} #{release_path}/config/#{config_file}"
  end

  desc "Somewhere in the build we are creating a 'public' directory in the /releases folder"
  task :hack_remove_public_dir do
    run "rm -rf #{deploy_to}/releases/public"
  end
  
  task :copy_oss_db_mail_config do
    config_file = "action_mailer_config.rb"
    run "cp /cenx/oss-db/sensitive/#{config_file} #{release_path}/config/initializers/#{config_file}"
  end
  
  task :copy_osl_poe_config do 
    config_file = "osl.poe.yml"
    cenx_apps_config = "/cenx/sensitive/#{config_file}"
    run "cp #{cenx_apps_config} #{release_path}/config/#{config_file}"
  end

  task :hack_copy_ordering_screenshots do 
    run "rm -rf #{release_path}/app/assets/images/circuit_orders"
    run "tar zxfv /cenx/osl/co.tar.gz -C #{release_path}/app/assets/images/"
  end

  task :hack_copy_ordering_screenshots_again do 
    run "rm -rf #{current_path}/app/assets/images/circuit_orders"
    run "tar zxfv /cenx/osl/co.tar.gz -C #{current_path}/app/assets/images/"
    deploy.assets.precompile
  end

  task :copy_google_config do 
    template_file = "#{deploy_to}/sensitive/google_config.rb"
    run "cp #{template_file} #{release_path}/config/initializers/google_config.rb; true"
  end

  desc "Populate logos for Service Providers"
  task :populate_provider_logos do
    run "cd #{release_path} && bundle exec rails runner --environment=production lib/one_off_scripts/populate_provider_logos.rb"
  end

end

namespace :local do
  task :update_version do
    if versionable
      system "rake version:deploy"
    else
      print "No updating version\n"
    end
  end
end

namespace :logging do
  desc "tail log files" 
  task :tail, :roles => ENV['ROLE'] || :web do
    last_host = ""
    max_hostname_length=19
    run "tail -f -n 50 #{shared_path}/log/#{rails_env}.log" do |channel, stream, data|
      trap("INT") { puts 'Interupted'; exit 0; }
      tag = channel[:host].split(".").first
      tag += " "*(max_hostname_length-tag.length) + ":  "
      data.strip!
      data.gsub! /\n/, "\n#{tag}"
      puts tag if channel[:host] != last_host
      puts "#{tag}#{data}"
      last_host = channel[:host]
      break if stream == :err
    end
  end
end