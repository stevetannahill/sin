namespace :deploy do
  namespace :bundle do
    task :install, :roles => :app do
      run "cd #{release_path} && rvm use #{rvm_ruby_gemset} && bundle install"
      on_rollback do
        if previous_release
          run "cd #{previous_release} && rvm use #{rvm_ruby_gemset} && bundle install"
        else
          logger.important "no previous release to rollback to, rollback of deploy:bundle:install skipped"
        end
      end
    end
  end  
end