// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// Vendor's javascript
//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require dygraph-combined
//= require jquery.activity-indicator
//= require jquery.ba-bbq
//= require jquery.daterangepicker
//= require jquery.flip
//= require jquery.json-2.2.min
//= require jquery.tbodyscroll
//= require jquery.scrollTo-1.4.2-min
//= require jquery.tipsy
//= require jquery.dataTables.min
//= require jquery.validate
//= require jquery.blockUI
//= require marker_with_label
//= require info_box
//= require keydragzoom
//= require markerclusterer
//= require waypoints.min
//= require chosen.jquery.min
//= require ajax-chosen.min
//= require socket.io.min
//
// Our javascript
//= require console
//= require general
//= require dropdown
//= require paths_and_ennis_index
//= require map
//= require network
//= require compares
//= require coresite_orders
//= require_tree ./reports
//= require_tree ./schedules
//= require_tree ./paths
//= require_tree ./users
//= require dashboard

