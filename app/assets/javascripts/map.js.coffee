class @Map
  
  constructor: (map_canvas) ->

    # track when site marker has been clicked
    @site_clicked = {}
    @site_markers_and_polylines = {}
    @default_site_zoom = 7

    @map_canvas = if (map_canvas instanceof jQuery) then map_canvas.get(0) else map_canvas
    
    # Google services
    @geocoder = new google.maps.Geocoder()
    @streetview_service = new google.maps.StreetViewService()
    @mode = $('#mode').val()
    
    @infoBoxOptions =
      boxClass: 'map_infobox'
      disableAutoPan: false
      maxWidth: 0
      pixelOffset: new google.maps.Size(-180, 0)
      closeBoxMargin: "-5px -10px 2px 2px"
      closeBoxURL: "assets/close.png"
      infoBoxClearance: new google.maps.Size(1, 1)
      isHidden: false
      pane: "floatPane"
      enableEventPropagation: false

    @toolTipOptions =
      boxClass: 'map_tooltip'
      disableAutoPan: false
      maxWidth: 0
      pixelOffset: new google.maps.Size(-180, 0)
      closeBoxURL: ""
      infoBoxClearance: new google.maps.Size(1, 1)
      isHidden: false
      pane: "floatPane"
      enableEventPropagation: false


    # Create map
    map_options = 
      mapTypeId: google.maps.MapTypeId.ROADMAP
      disableDefaultUI: false
      disableDoubleClickZoom: false
      mapTypeControlOptions:
          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
          
    @map = new google.maps.Map @map_canvas, map_options
    
    # Box drag zoom
    @map.enableKeyDragZoom();

    # Show all sites
    @show_site_markers(sites)
    
    # Show demarcs (if any filtered)
    @show_demarc_markers(demarcs)


  # show site markers
  show_site_markers: (sites) ->

    latlngs_for_bounds = []

    # Notice the use of the fat arrow (=>) syntax for the callback function; 
    # it binds the function's context current value of this. 
    # (this in the callback function is always the same this as the one at the time you defined the function.)
    site_markers = []
    $.each sites, (i, site) =>
      latlng = new google.maps.LatLng site.lat, site.lng
      latlngs_for_bounds[i] = latlng

      marker = new MarkerWithLabel(
        position: latlng
        icon: site.icon
        labelContent: site.path_count
        labelAnchor: new google.maps.Point(75, -8)
        labelClass: "map_path_count"
        crossImage: 'https://maps.gstatic.com/intl/en_us/mapfiles/drag_cross_67_16.png'
        handCursor: 'https://maps.gstatic.com/intl/en_us/mapfiles/closedhand_8_8.cur'
      )

      site_markers.push(marker)
      # initialize array for keeping track of markers for each site (hide/show)
      @site_clicked[site.id] = no
      @site_markers_and_polylines[site.id] = []
      google.maps.event.addListener marker, "click", =>
        if @site_clicked[site.id]
          elem.setVisible(true) for elem in @site_markers_and_polylines[site.id]
          existing_latlngs_for_bounds = $.map @site_markers_and_polylines[site.id], (elem) =>
            # check if position, polylines don't have a position
            elem.getPosition() if elem.position
          # add site
          existing_latlngs_for_bounds.push(latlng)
          # refit bounds to existing markers
          @fit_bounds_to_markers existing_latlngs_for_bounds
        else
          @get_demarc_markers site

        @show_info_window marker, @site_url(site, 'map_info_window')
        @site_clicked[site.id] = yes

    # Default map to North America if no sites found
    @fit_bounds_to_markers latlngs_for_bounds
    
    # Zoom out to original position if users hits Escape
    $(document).keyup (e) =>
      if e.keyCode is 27
        # Close info box
        @info_box.close() if @info_box?
        # Zoom out
        @fit_bounds_to_markers latlngs_for_bounds
        # Hide markers and polylines
        if @site_markers_and_polylines
          $.each @site_markers_and_polylines, (i, markers_and_polylines) =>
            elem.setVisible(false) for elem in markers_and_polylines
    
    # Add markers using the marker cluster lib
    markerCluster = new MarkerClusterer(@map, site_markers);
    
  # show demarc markers on map for provided site  
  get_demarc_markers: (site) ->
    cursor_wait()
    $.getJSON(site.demarcs_url)
      .success (data, textStatus, request) =>
        return false if is_login_page request
        @show_demarc_markers data, site
      .complete (request, textStatus) =>
        cursor_wait_is_over()

  show_demarc_markers: (data, site) ->
    return false if not data? or not data.demarcs? or not data.lines?
    demarcs = data.demarcs
    lines = data.lines
    latlngs_for_bounds = []
    $.each demarcs, (i, demarc) =>
      latlng = new google.maps.LatLng demarc.lat, demarc.lng
      latlngs_for_bounds[i] = latlng
      # if no icon then just used for bounds so don't place marker
      if demarc.icon?
        marker = new google.maps.Marker(
          position: latlng
          icon: demarc.icon
          map: @map
        )

        # Add marker to site array
        @site_markers_and_polylines[site.id].push(marker) if site?

        google.maps.event.addListener marker, "click", =>
          @show_info_window marker, @demarc_url(demarc, 'map_info_window')
        google.maps.event.addListener marker, "mouseover", (event) =>
          @hover_reset()
          @timer = setTimeout(=>
            @show_tooltip marker, demarc, @demarc_url(demarc, 'map_tooltip')
          , 50)
        google.maps.event.addListener marker, "mouseout", (event) =>
          @hover_reset()

          
    # TODO: Figure way to hide paths along with markers
    # markerCluster = new MarkerClusterer(@map, @site_markers_and_polylines[site.id]);
    $.each lines, (i, line) =>
      # create latlng object for each point in line
      path = $.map line.points, (point) =>
        new google.maps.LatLng point.lat, point.lng

      # Include line points when determining map bounds
      $.merge latlngs_for_bounds, path
      polyline = new google.maps.Polyline(
        path: path
        strokeColor: line.color
        strokeWeight: 3
        map: @map
      )
      @site_markers_and_polylines[site.id].push(polyline) if site?
      
      if line.ovc_type?
        google.maps.event.addListener polyline, "click", (event) =>
          @show_info_window polyline, @line_url(line, 'map_info_window'), event
        google.maps.event.addListener polyline, "mouseover", (event) =>
          @hover_reset()
          @timer = setTimeout(=>
            @show_tooltip polyline, line, @line_url(line, 'map_tooltip'), event
          , 50)
        google.maps.event.addListener polyline, "mouseout", (event) =>
          @hover_reset()

    @fit_bounds_to_markers latlngs_for_bounds

  # Don't want to generate url (path) on server, too slow
  line_url: (line, info) ->
    line.ovc_type + '/' + info + '?mode=' + @mode + '&ids[]=' + $.unique(line.ovc_ids).join('&ids[]=') + '&path_ids[]=' + $.unique(line.path_ids).join('&path_ids[]=')
  
  demarc_url: (demarc, info) ->
    # Don't want to generate url (path) on server, too slow
    'demarcs/' + info + '?mode=' + @mode + '&ids[]=' + $.unique(demarc.demarc_ids).join('&ids[]=')

  site_url: (site, info) ->
    "sites/#{site.id}/#{info}?mode=#{@mode}"

  hide_demarc_markers: (button, site_id) ->
    # hide button
    $(button).hide()
    elem.setVisible(false) for elem in @site_markers_and_polylines[site_id]

  show_tooltip: (marker, entity, url, event) ->
    @set_hover_token()
    $.get(url, {hover_token: @hover_token}).success (data, textStatus, request) =>
      return false if is_login_page request
      # Don't want more than 1 tooltip
      return false unless @hover_token == parseInt(request.getResponseHeader('x-hover-token'), 10)
      @close_tooltip()

      tooltip_text = $('<div>').addClass('map_tooltip_inner').html(data)
      @tooltip = new InfoBox @toolTipOptions
      @tooltip.setContent tooltip_text.get(0)
      if event?
        @tooltip.setPosition event.latLng
        @tooltip.open @map
      else
        @tooltip.open @map, marker
 
  close_tooltip: () ->
    @tooltip.close() if @tooltip?

  clear_timer: () ->
    if @timer
      clearTimeout(@timer)
      @timer = null

  hover_reset: () ->
    @clear_timer()
    @clear_hover_token()
    @close_tooltip()
  
  set_hover_token: () ->
    @hover_token = Math.round((new Date()).getTime());

  clear_hover_token: () ->
    @hover_token = null

  # show info window for demarc    
  show_info_window: (marker, url, event) ->
    $.get(url).success (data, textStatus, request) =>
      return false if is_login_page request
      # new google.maps.InfoWindow(content: data).open @map, marker
      box_text = $('<div>').addClass('map_infobox_inner').html(data)
      @info_box.close() if @info_box?
      @info_box = new InfoBox @infoBoxOptions
      @info_box.setContent box_text.get(0)
      if event?
        @info_box.setPosition event.latLng
        @info_box.open @map
      else
        @info_box.open @map, marker
        

  # search address via google geocoding
  search_address: (address) ->
    address_results = $("#search_address_results")
    address_results.hide()
    @geocoder.geocode
      address: address
      (results, status) =>
        if status is google.maps.GeocoderStatus.ZERO_RESULTS
          $("#search_address_button").show_activity(false)
        else if status is google.maps.GeocoderStatus.OK
          $("#search_address_button").show_activity(false)
          if results.length is 1
            bounds = results[0].geometry.bounds
            @map.fitBounds bounds if bounds
          else if results.length > 1
            address_results_list = address_results.find("ul")
            address_results_list.empty()
            $.each results, (index, result) ->
              geometry = result.geometry
              bounds = geometry.bounds
              address = $("<a>").attr("href", "#").text(result.formatted_address).attr("data-latlng", (if bounds then bounds else geometry.location).toUrlValue())
              address_results_list.append $("<li>").append(address)

            address_results.show()

  # show street view for address (lat, lng)
  show_streetview: (button, latitude, longitude) ->
    latlng = new google.maps.LatLng(latitude, longitude)
    @streetview_service.getPanoramaByLocation latlng, 50, (result, status) =>
      if status is google.maps.StreetViewStatus.OK
        panorama = new google.maps.StreetViewPanorama(
          @map_canvas
          pano: result.location.pano
          enableCloseButton: true
        )
      else
        $(button).closest(".map_marker_info").find(".no_streetview").text("Sorry, no street view available for this location").slideDown "slow"

  fit_bounds_to_markers: (latlngs_for_bounds) ->
    if latlngs_for_bounds?
      # Only one address fit to address instead of bounds (bounds will be zoomed in too far)
      if latlngs_for_bounds.length is 1
        @fit_bounds_to_address [latlngs_for_bounds[0].lat(), latlngs_for_bounds[0].lng()], @default_site_zoom
      else
        # No bounds defined, fit to USA
        bounds = new google.maps.LatLngBounds()
        latlngs_for_bounds = [new google.maps.LatLng(24.7433195, -124.7844079), new google.maps.LatLng(49.3457868, -66.9513812)] if latlngs_for_bounds.length is 0
        bounds.extend latlng for latlng in latlngs_for_bounds
        @map.fitBounds bounds
    
  fit_bounds_to_address: (latlngs, zoom = 17) ->
    if latlngs.length is 2
      latlng = new google.maps.LatLng latlngs[0], latlngs[1]
      @map.setCenter latlng
      @map.setZoom zoom
    else
      sw_latlng = new google.maps.LatLng latlngs[0], latlngs[1]
      ne_latlng = new google.maps.LatLng latlngs[2], latlngs[3]
      bounds = new google.maps.LatLngBounds sw_latlng, ne_latlng
      @map.fitBounds bounds