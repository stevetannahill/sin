// Need this global so we can access in infowindow links.  
// Google doesn't like jQuery's .live so we use old school javascript and put right on the element (onclick)
var map;

$(function(){

  $('#network_filter_and_mode .submit_button').click(function(){
    $(this).toggleButton($('#network_filter_and_mode_content'));
  });

  $('#find_and_sort_dialog').hide().css('top', '95px');
  
  $('#network-index').slice(0,1).each(function(){
    // jQuery events for index page
    $(".quick_find .expand").live('click',function(){
      $('#find_and_sort_dialog').show();
      return false;
    });
  
    $("#find_and_sort_dialog .close_button").live('click',function(){
      $('#find_and_sort_dialog').hide();
      return false;
    });
    
    $('body').css({
      'overflow': 'hidden'
    });
    
    $('#footer').hide();
    
    function update_sizes(){
      var window_height = $(window).height(), 
          window_width = $(window).width(),
          map_elem = $('#map_wrapper'),
          map_top_offset = map_elem.offset().top,
          footer_height = $('#footer').height(),
          map_view_height = Math.max(window_height - map_top_offset - 12 - footer_height,0); //replace 12 with padding value from .map_container
      
      map_elem.css({
        'height': map_view_height
      });
    }
    
    // Resize the page elements on page load
    update_sizes();
    
    $(window).resize(update_sizes);

    // Initialize the Network Map
    var map_canvas = $('#map');
    if (map_canvas.exists()) {
      map = new Map(map_canvas);

      // Use googles geocoder to search addresses
      $("#search_address_form").submit(function(event) {
        event.preventDefault();
        map.search_address($("#search_address_input").val());
      });
      
      // Geocoder may return more than one result which are displayed in a list
      // attach events to display the location on the map when clicked
      $("#search_address_results li a").live("click", function(event) {
        event.preventDefault();
        var link = $(this), latlngs = link.attr("data-latlng").split(",");
        $("#search_address_results").hide();
        $("#search_address_input").val(link.text());
        map.fit_bounds_to_address(latlngs);
      });
    }
  
  });

});


// TODO: Should map_info_window_show_tab be in map class (map.js.coffee)?
//  Tabs in map info window
// PROGRAMMERS NOTE: need to use onclick (old school) since Google maps does not like .live
function map_info_window_show_tab(tab) {

  var clicked_tab = $(tab),
      clicked_tab_content = $(clicked_tab.find("a").attr("href"));

  // First remove class "active" from currently active tab
  $(".map_tabs li.active").removeClass('active');

  // Now add class "active" to the selected/clicked tab
  clicked_tab.addClass("active");

  // Hide all tab content
  $(".map_tab_content.active").hide().removeClass('active');

  // Show the selected tab content
  clicked_tab_content.fadeIn().addClass('active');

  return false;

}

// TODO: Should map_info_window_show_tab be in map class (map.js.coffee)?
//  Tabs in map info window
// PROGRAMMERS NOTE: need to use onclick (old school) since Google maps does not like .live
function map_info_window_show_section(section, reload) {
  var clicked_section = $(section),
      clicked_section_content = clicked_section.next();

  if (reload == undefined) {
    reload = false;
  }

  // Show the selected tab content
  if (clicked_section_content.is(':visible') && !reload) {
    clicked_section_content.slideUp('slow');
  } else {
    if (clicked_section_content.is(':empty') || reload) {
      clicked_section.show_activity();
      var date_range_selected = clicked_section.closest('.map_tab_content').find('.monitoring_date_range .primary');
      $.get(clicked_section_content.data('url'), {date_range_selector: date_range_selected.data('date_range_selector')},  function(data, textStatus, request){
        // This required to trap the login page that cas redirects to if ticket or session expires
        if (is_login_page(request)) return false;
        // update section content
        clicked_section_content.html(data);
        if (!reload) {
          clicked_section_content.slideDown('slow');
        }
        clicked_section.show_activity(false);
      })
    } else {
      clicked_section_content.slideDown('slow');
    }
  }

  return false;

}

function map_info_window_change_date_range(button) {
  var clicked_button = $(button), sections = clicked_button.closest('.map_tab_content').find('.map_tab_section_content:not(:empty)');
  clicked_button.siblings('.primary').removeClass('primary').addClass('alternate');
  clicked_button.addClass('primary').removeClass('alternate');
  sections.each(function(){
    map_info_window_show_section($(this).prev(), true);
  })
}

