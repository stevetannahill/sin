class OnNetRouterSubIf < RouterSubIf

  validates_class_of :segment, :is_a => "OnNetRouter"

end
