# typically loaded with only a ruby (not rails) environment, so do not use rails specific features (like blank?)
class Jruby
  
  def self.split_args(raw_args)
    jruby_args = []
    ruby_args = []
   
    unless raw_args.nil?
      raw_args.each do |arg|
        next if arg.nil? || arg == ''
        if arg.start_with?("-J")
          jruby_args<< arg
        else
          ruby_args<< arg
        end
      end
    end
    
    { :jruby_args => jruby_args, :ruby_args => ruby_args }
  end
  
end