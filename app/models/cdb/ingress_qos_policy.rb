# == Schema Information
# Schema version: 20110802190849
#
# Table name: qos_policies
#
#  id          :integer(4)      not null, primary key
#  type        :string(255)
#  policy_name :string(255)
#  policy_id   :integer(4)
#  site_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#

class IngressQosPolicy < QosPolicy

  def generate_config
    config = Config.new(get_own_policy)
    text = "configure qos #{config.ingress_qos_policy_config_name} #{policy_id} create\n"
    text += config.generate_config
    text += "exit\n"
  end

  def generate_delete
    config = Config.new(get_own_policy)
    text = "no #{config.ingress_qos_policy_config_name} #{policy_id}\n"
    return text
  end


  #This class handles the creation of an Ingress QoS Policy config section
  class Config < QosPolicy::Config
    def generate_config
      #Start with general config
      config = super

      shapers = @sections['Shaping']
      policers = @sections['Policing']
      nones = @sections['None']

      #Add needed queues (Shapers)
      if shapers.empty?
        config += "#{ingress_qos_entity_config_name} 1 create\n"
        config += "exit\n"
        config += "#{ingress_qos_entity_config_name} 11 multipoint create\n"
        config += "exit\n"
      else
        shapers.each do |s|
          config += s.generate_config
          config += "#{ingress_qos_entity_config_name} 11 multipoint create\n"
          config += "exit\n"
        end
      end

      #Add needed policers (Policers)
      policers.each do |p|
        config += p.generate_config
      end

      #Add needed blank policers (None)
      nones.each do |p|
        config += p.generate_config
      end

      #Add fc sections (Forwarding Class)
      roster = []
      all = (shapers + policers + nones)
      all.each do |rl|
        rl.register_mapping roster  #Store the mapping information for use later
        config += rl.generate_ingress_fc_config
      end

      #If we cleared out the roster, it means we must use defaults
      if roster.empty? and !all.empty?
        config += all.first.default_fc
      end

      #Register each fc with it's mapping
      roster.each_with_index do |fc, i|
        config += "dot1p #{i} fc \"#{fc}\"\n" unless fc.nil?
      end
      return config
    end

  end
end
