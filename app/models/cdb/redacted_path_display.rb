class RedactedPathDisplay < Array
  
  # no single entity, this an array
  def entity
    nil
  end

  def redacted_details
    details = self.map do |col|
      case col.entity_type
        when 'Segment' then col.on_net_ovc? ? col.entity.site.name : col.entity.service_provider.name
        else nil
      end
    end
    details.uniq.compact.join(' / ')
  end

end