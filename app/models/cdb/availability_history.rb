# == Schema Information
# Schema version: 20110802190849
#
# Table name: event_histories
#
#  id             :integer(4)      not null, primary key
#  event_filter   :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  alarm_mapping  :text
#  retention_time :integer(4)
#

class AvailabilityHistory < AlarmHistory
  
  def self.default_avaiability_alarm_mapping
    return {"cleared"     => AlarmSeverity::OK,
            "warning"     => AlarmSeverity::WARNING,
            "failed"      => AlarmSeverity::FAILED,
            "unavailable" => AlarmSeverity::UNAVAILABLE}
  end
         
  def default_mapping
    self.alarm_mapping = AvailabilityHistory::default_avaiability_alarm_mapping if alarm_mapping == nil 
  end
  
  def self.alarm_sync_all(nms = nil)
    #TODO Make generic so it can support Brix and Spirent and SAM ??
    
    return_code = true

    if SpirentCosTestVector.count > 0
      # TODO remove - will need to go through all CTV and get the archive id's and then
      # loop over each data archive to get the date
      class_name = 'ethframedelaytest'
      time_name  = 'time_start'
      columns = ['grid_circuit_id', 'time_start', 'available']
      
      # Don't bother checking to see if it's Live or not - do 100 at a time
      circuit_ids = SpirentCosTestVector.pluck("id")
      if circuit_ids.size/100 == 0
        circuit_id_chunks = [circuit_ids]
      else
        circuit_id_chunks = circuit_ids.each_slice(circuit_ids.size/100).to_a
      end
      
      circuit_id_chunks.each do |circuit_ids|
        where = circuit_ids.collect {|circuit_id| ["`grid_circuit_id` = '#{circuit_id}'"]}

        results = DataArchiveIf.last_availability_for(class_name, time_name, columns, where)
        if results.blank? 
          SW_ERR "No data"
          return_code = false
        else
          results.each do |result|
            state = result["available"] == 1 ? "cleared" : "unavailable"
            timestamp = result["time_start"]*1000
            obj = CosTestVector.find(result["grid_circuit_id"])
            event_filter = "#{obj.circuit_id}-Availability"
            details = ""
            tmp = {:event_filter => event_filter, :timestamp => timestamp, :state => state, :details => details}
            AvailabilityHistory.process_event(tmp)
          end
        end
      end
    end

    return return_code
  end      

  def alarm_sync(timestamp = nil)
    #TODO Make generic so it can support Brix and Spirent and SAM ??
    return_code = false
    object = event_ownerships.first.eventable_type.constantize.find_by_id(event_ownerships.first.eventable_id)
    if object == nil
      SW_ERR "Failed to find object associated with #{self.inspect}"
    else
      return_code = true            
      if SpirentCosTestVector.count > 0
        # TODO remove - will need to go through all CTV and get the archive id's and then
        # loop over each data archive to get the date
        class_name = 'ethframedelaytest'
        time_name  = 'time_start'
        columns = ['grid_circuit_id', 'time_start', 'available']
        where = [["`grid_circuit_id` = '#{object.grid_circuit_id}'"]]

        result = DataArchiveIf.last_availability_for(class_name, time_name, columns, where)
        if result.blank? || result.size > 1
          #SW_ERR "No data for #{object.circuit_id} or too much data!"
          return_code = true
        else
          result = result.first
          state = result["available"] == 1 ? "cleared" : "unavailable"
          timestamp = result["time_start"]*1000
          event_filter = "#{object.circuit_id}-Availability"
          details = ""
          tmp = {:event_filter => event_filter, :timestamp => timestamp, :state => state, :details => details}
          AvailabilityHistory.process_event(tmp)
        end
      end
    end
    return return_code
  end
  
  def alarm_sync_past(timestamp = nil)
    #TODO Make generic so it can support Brix and Spirent and SAM ??
    return_code = false
    object = event_ownerships.first.eventable_type.constantize.find_by_id(event_ownerships.first.eventable_id)
    if object == nil
      SW_ERR "Failed to find object associated with #{self.inspect}"
    else
      return_code = true            
      if SpirentCosTestVector.count > 0
        # TODO remove - will need to go through all CTV and get the archive id's and then
        # loop over each data archive to get the date
        class_name = 'ethframedelaytest'
        time_name  = 'time_start'
        columns = ['grid_circuit_id', 'time_start', 'available']
        where = [["`grid_circuit_id` = '#{object.grid_circuit_id}'"]]
        limit = nil

        result = DataArchiveIf.last_availability_for( class_name, time_name, columns, where, limit)
        if result.blank?
          #SW_ERR "No data for #{object.circuit_id}"
          return_code = true
        else
          result.each do |res|
            state = res["available"] == 1 ? "cleared" : "unavailable"
            timestamp = res["time_start"]*1000
            event_filter = "#{object.circuit_id}-Availability"
            details = ""
            tmp = {:event_filter => event_filter, :timestamp => timestamp, :state => state, :details => details}
            AvailabilityHistory.process_event(tmp)
          end
        end
      end
    end
    return return_code
  end
  
        
end
