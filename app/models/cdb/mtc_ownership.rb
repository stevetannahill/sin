# == Schema Information
# Schema version: 20110802190849
#
# Table name: mtc_ownerships
#
#  id             :integer(4)      not null, primary key
#  mtcable_id     :integer(4)
#  mtcable_type   :string(255)
#  mtc_history_id :integer(4)
#

class MtcOwnership < ActiveRecord::Base
belongs_to :mtcable, :polymorphic => true
belongs_to :mtc_history, :dependent => :destroy

end
