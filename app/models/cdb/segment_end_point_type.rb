# == Schema Information
# Schema version: 20110802190849
#
# Table name: segment_end_point_types
#
#  id                       :integer(4)      not null, primary key
#  name                     :string(255)
#  created_at               :datetime
#  updated_at               :datetime
#  operator_network_type_id :integer(4)
#  class_of_service_type_id :integer(4)
#  notes                    :string(255)
#

class SegmentEndPointType < TypeElement
  belongs_to :operator_network_type
  belongs_to :class_of_service_type
  has_and_belongs_to_many :ethernet_service_types
  has_many :segment_end_points, :dependent => :nullify
  has_many :instance_elements, :class_name => "SegmentEndPoint"
  
  validates_presence_of :name, :operator_network_type_id
  validates_uniqueness_of :name, :scope => :operator_network_type_id

  def default_cos_type_info
    if class_of_service_type_id == nil
      return "Drop"
    end
    # OSS Parse sets this to 0 if Default is "Lowest ordered CoS"
    if class_of_service_type_id == 0
      return notes
    end
    return class_of_service_type.name
  end

end
