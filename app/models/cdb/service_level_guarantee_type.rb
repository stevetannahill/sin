# == Schema Information
# Schema version: 20110802190849
#
# Table name: service_level_guarantee_types
#
#  id                       :integer(4)      not null, primary key
#  class_of_service_type_id :integer(4)
#  min_dist_km              :string(255)
#  max_dist_km              :string(255)
#  delay_us                 :string(255)
#  delay_variation_us       :string(255)
#  is_unbounded             :boolean(1)
#  notes                    :string(255)
#  created_at               :datetime
#  updated_at               :datetime
#

class ServiceLevelGuaranteeType < TypeElement
    belongs_to :class_of_service_type
    has_many :cos_test_vectors, :dependent => :nullify
    
    validates_presence_of :delay_us, :delay_variation_us, :class_of_service_type_id
    validates_range_of :min_dist_km, :max_dist_km,  :unbounded => ["~", "*"],:overlapping => false, :scope => :class_of_service_type_id, :greater_than_or_equal_to => 0
    #validates_numericality_of :min_dist_km, :max_dist_km, :availability

    def name
      return "#{min_dist_km} - #{max_dist_km}"
    end

    def frame_loss_ratio_percent
      return class_of_service_type ? class_of_service_type.frame_loss_ratio : 'TBD'
    end

    def availability_guarantee
      return class_of_service_type ? class_of_service_type.availability : 'TBD'
    end

    def default_delay_us
      return delay_us.gsub(/[^\d.]/, '')
    end

    def default_delay_variation_us
      return delay_variation_us.gsub(/[^\d.]/, '')
    end

    def default_frame_loss_ratio_percent
      return frame_loss_ratio_percent.gsub(/[^\d.]/, '')
    end
end
