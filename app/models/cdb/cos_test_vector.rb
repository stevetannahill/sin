# == Schema Information
#
# Table name: cos_test_vectors
#
#  id                              :integer(4)      not null, primary key
#  type                            :string(255)
#  test_type                       :string(255)
#  test_instance_class_id          :string(255)
#  notes                           :string(255)
#  cos_end_point_id                :integer(4)
#  service_level_guarantee_type_id :integer(4)
#  created_at                      :datetime
#  updated_at                      :datetime
#  delay_error_threshold           :string(255)
#  dv_error_threshold              :string(255)
#  delay_warning_threshold         :string(255)
#  dv_warning_threshold            :string(255)
#  service_instance_id             :string(255)
#  sla_id                          :string(255)
#  test_instance_id                :string(255)
#  flr_error_threshold             :string(255)
#  flr_warning_threshold           :string(255)
#  availability_guarantee          :string(255)
#  cenx_id                         :string(255)
#  flr_sla_guarantee               :string(255)
#  dv_sla_guarantee                :string(255)
#  delay_sla_guarantee             :string(255)
#  circuit_id                      :string(255)
#  ref_circuit_id                  :string(255)
#  event_record_id                 :integer(4)
#  sm_state                        :string(255)
#  sm_details                      :string(255)
#  sm_timestamp                    :integer(8)
#  last_hop_circuit_id             :string(255)
#  flr_reference                   :string(255)
#  delay_reference                 :string(255)
#  delay_min_reference             :string(255)
#  delay_max_reference             :string(255)
#  dv_reference                    :string(255)
#  dv_min_reference                :string(255)
#  dv_max_reference                :string(255)
#  loopback_address                :string(255)
#  circuit_id_format               :string(255)
#  grid_circuit_id                 :integer(4)
#  ref_grid_circuit_id             :integer(4)
#  protection_path_id              :integer(4)
#

class CosTestVector < InstanceElement
  include Eventable
  include Kpiable
  
  belongs_to :cos_end_point #structural
  belongs_to :service_level_guarantee_type

  has_many :metrics_monthly_cos_test_vectors, :primary_key => :grid_circuit_id, :foreign_key => :grid_circuit_id

  validates_presence_of :cos_end_point_id, :service_level_guarantee_type_id, :delay_error_threshold, :dv_error_threshold, :flr_error_threshold, :availability_guarantee, :circuit_id_format
  validates_presence_of :delay_warning_threshold, :flr_warning_threshold, :dv_warning_threshold
  validates_presence_of :flr_sla_guarantee, :dv_sla_guarantee, :delay_sla_guarantee
  validates_inclusion_of :test_type, :in => ServiceTestTypes::TEST_TYPES.map{|value| value },
                         :message => "invalid, Use: #{ServiceTestTypes::TEST_TYPES.map{|value| value }.join(", ")}"
  validates_as_cenx_id :cenx_id
  validates :flr_reference, :delay_reference, :delay_min_reference, :delay_max_reference, :dv_reference, :dv_min_reference, :dv_max_reference, :numericality => {:allow_blank  => true}
  validates_format_of :loopback_address,
                      :with => /(^$)|(^(\d{1,3}\.){3}(\d{1,3})\/(\d{1,2})$)|(^(([A-Fa-f0-9]{2}):){5}([A-Fa-f0-9]{2})$)/,
                      :message => "should be strict IP/CIDR or MAC address format ex: IP: 111.222.333.444/55 or MAC: 11:22:33:44:55:66", :allow_blank => true
  validate :circuit_id_valid?
  after_save :update_kpis, :if => :cos_end_point_id_changed?
  after_save :update_lkp_on_change
  after_save :update_event_histories, :if => :circuit_id_changed?
  after_create :general_after_create
  
  LKP_FIELDS = %w(grid_circuit_id ref_grid_circuit_id flr_sla_guarantee flr_error_threshold flr_warning_threshold delay_sla_guarantee delay_error_threshold delay_warning_threshold dv_sla_guarantee dv_error_threshold dv_warning_threshold availability_guarantee)
  METRIC_CONVERSION = {"delay" => "delay", "delay_variation" => "dv", "frame_loss_ratio" => "flr"}
  SEVERITY_CONVERSION = {"major" => "error","minor" => "warning"}    
  
	before_validation(:on => :create) do
		self.cenx_id = IdTools::CenxIdGenerator.generate_cenx_id(self.class)
	end
	
	def general_after_create
    update_attributes(:sm_state => AlarmSeverity::NOT_LIVE.to_s, :sm_details => "", :sm_timestamp => (Time.now.to_f*1000).to_i)
    event_record_create
  end
	
  def self.get_default_warning_threshold(error_threshold)
    return error_threshold.to_f * 0.8
  end

  def cenx_name
    return "SMTest:#{test_type}-#{cos_end_point.cos_name}-#{test_target_address}"
  end
  
  def get_monitoring_manager
    result, result_str, cenx_ep = get_cenx_monitoring_cos_end_point
    if result
      manager = cenx_ep.get_connected_monitoring_manager
      if manager
        return manager
      else
        SW_ERR "Failed to get Monitoring Manager on Cenx Endpoint: #{cenx_ep.pulldown_name}"
        return nil
      end
      #return cenx_ep.segment_end_point.enni.ports.first.connected_port.get_network_manager
    else
      SW_ERR "Failed to get corresponding CENX endpoint for #{cos_end_point.segment_end_point.cenx_name}. result = #{result_str}"
      return nil
    end
  end
  
  # CH03XC803-ETH-P-V110-CIR00050-P0
  def name
    "CircuitIdFormat::#{circuit_id_format}".constantize.name(circuit_id)    
  end
  
  def short_name 
    "CircuitIdFormat::#{circuit_id_format}".constantize.short_name(circuit_id)   
  end
  
  def primary
    "CircuitIdFormat::#{circuit_id_format}".constantize.decode(circuit_id)[:primary]    
  end
  
  def backup
    "CircuitIdFormat::#{circuit_id_format}".constantize.decode(circuit_id)[:backup]        
  end
  
  def priority
    "CircuitIdFormat::#{circuit_id_format}".constantize.decode(circuit_id)[:priority]    
  end
  
  def site_type
    "CircuitIdFormat::#{circuit_id_format}".constantize.decode(circuit_id)[:site_type]    
  end

  def vlan_id
    "CircuitIdFormat::#{circuit_id_format}".constantize.decode(circuit_id)[:vlan_id]    
  end
  
  def site_name
    "CircuitIdFormat::#{circuit_id_format}".constantize.decode(circuit_id)[:site_name]    
  end
  
  def paired_ctv
    nil
  end
  
  def mtc_history
    # there is no mtc history for CosTestVectors. Use the cos_end_point
    cos_end_point.mtc_history
  end    
  
  def update_alarm(list_of_evaled_objects = [], full_eval = false)
    changed = super
    cos_end_point.eval_alarms(list_of_evaled_objects, full_eval) if changed
    return changed
  end
  
  private
  
  def target_address
    return cos_end_point.segment_end_point.demarc_mon_address
  end
  
  def target_inner_tag_vid_is_integer
    return cos_end_point.segment_end_point.ctags =~ /^[-+]?[0-9]+$/
  end
  
  def target_inner_tag_vid
    if target_inner_tag_vid_is_integer
      return cos_end_point.segment_end_point.ctags
    else
      SW_ERR "using Invalid target_inner_tag_vid"
      return 'ERROR!'
    end

  end

  def target_inner_tag_pcp
    if cos_end_point.segment_end_point.ctags =~ /^[-+]?[0-9]+$/
      return cos_end_point.ingress_mapping
    else
      SW_ERR "using Invalid target_inner_tag_pcp"
      return 'ERROR!'
    end

  end

  def target_meg_level
    cos_end_point.segment_end_point.md_level
  end
  
  def get_cenx_monitoring_cos_end_point
    cenx_monitoring_cos_end_point = nil
    result = false
    result_str = ""

    cenx_monitoring_cos_end_point = cos_end_point.get_cenx_monitoring_cos_end_point

    if !cenx_monitoring_cos_end_point
      result_str = "No Monitoring Endpoint Found!"
      result = false
      return result, result_str, cenx_monitoring_cos_end_point
    end

    result = true
    return result, result_str, cenx_monitoring_cos_end_point

  end
  
  def cos_name
    return "#{cos_end_point.cos_name} CoS"
  end
  
  def go_in_service    
    return {:result => true, :info => ""}
  end
  
  public

  # KPI methods 
  # Override the one in kpiable as this info is not obtained from the KPI
  def get_errored_period_threshold(severity_level, metric)
    method_metric_mapping = {:delay => 'delay', :delay_variation => 'dv', :frame_loss => 'flr', :frame_loss_ratio => 'flr'}
    severity_mapping = {:major => '_error_threshold', :minor => '_warning_threshold', :critical => '_sla_guarantee'}
    # puts "#{severity_level},#{metric},#{method_metric_mapping[metric]}#{severity_mapping[severity_level]}"
    return self.availability_guarantee if metric == :availability
    # The threshold are not in KPI but in the CosTestVector    
    threshold = self.send("#{method_metric_mapping[metric]}#{severity_mapping[severity_level]}")
    if threshold == "N/A"
      threshold = 0
    end
    return threshold.to_f
  end
  
  
  def kpi_hierarchy(kpi_object)
    segment = cos_end_point.segment_end_point.segment
    on = segment.get_operator_network
    sp = segment.service_provider
    return [[self, segment],[segment, on],[on, sp], [sp, sp]]
  end
  
  def update_kpi(parent, parent_kpi, kpi_name, kpi_object)
    segment = cos_end_point.segment_end_point.segment
    parent = segment
    if parent_kpi == nil
      parent_kpi = segment.get_kpi(kpi_name, kpi_object, segment.get_operator_network)
    end
    calculate_kpi(kpi_name, kpi_object, parent_kpi, parent)    
  end  
  
  def update_kpis
    if cos_end_point != nil
      Kpi::KPI_BY_KLASS[self.class.name].each do |kpi_name|
        kpi_setup(kpi_name, self.class.name)
      end
    end
    return true
  end

  def update_lkp_on_change
   #ref_circuit_id gets updated when the edit page is used for the Monitoring Flows from nil to ""
   #so !send("#{attr_name}_change".to_sym).all?{|a|a.blank?} was added to ignore this kind of change
   if LKP_FIELDS.any? {|attr_name| send("#{attr_name}_changed?".to_sym) && !send("#{attr_name}_change".to_sym).all?{|a|a.blank?}}
     update_lkp
     lkp_reset
   end
    return true
  end

  def update_lkp
    attr = LKP_FIELDS.each_with_object({}) do |attr_name,attr|
      attr[attr_name] = self[attr_name].blank? ? nil : self[attr_name]
    end
    
    %w(major minor).each do |sev|
      %w(delay delay_variation frame_loss_ratio).each do |metric|
        samples = self.get_errored_period_max_errored_count(sev.to_sym,metric.to_sym)
        attr_name = [METRIC_CONVERSION[metric],SEVERITY_CONVERSION[sev],"period"].join("_")
        attr[attr_name] = samples if !samples.nil? && samples > 0
      end
    end
    
    LKP::CosTestVector.new(self.grid_circuit_id,attr)
    return true
  end
  
  def lkp_reset(reset_start = Time.now)
    if LKPPrismConnection.available
      begin

        LKP::Prism.publish_reset_sync_with_starttime(reset_start.to_i, self.grid_circuit_id)
      rescue Exception => e
        logger.error("Prism.publish_reset_sync failed: #{e.to_s}")
      end
    end
  end
  
  def self.full_lkp_udpate
    # this should greatly speed up all the redis updates when doing so many
    # had to split the udpate and reset to make sure the updating was completed before
    # things were reset
    LKP.redis.pipelined do
      CosTestVector.find_each do |ctv|
        ctv.update_lkp
      end
    end
  end

  def self.full_lkp_refresh(reset_start = Time.now)
    full_lkp_udpate
    if LKPPrismConnection.available
      CosTestVector.find_in_batches do |ctvs|
        LKP::Prism.publish_reset_sync_with_starttime(reset_start.to_i, *ctvs.map{|ctv|ctv.grid_circuit_id})
      end
    else
      logger.error("full_lkp_refresh failed because LKPPrismConnection was not available at rails start")
    end
  end
  
  def circuit_id_valid?
    raise "circuit_id_format not set" if circuit_id_format.blank?
    error_text = []
    error_text.each {|text| errors.add(:circuit_id, "is not valid #{text}")} unless "CircuitIdFormat::#{circuit_id_format}".constantize.valid(circuit_id, error_text)
  end
  
  def update_event_histories
    # If the circuit_id has changed then the availability event_filter should also change
    ahs = alarm_histories.find_all_by_type("AvailabilityHistory")
    AvailabilityHistory.update(ahs, [:event_filter => "#{circuit_id}-Availability"]*ahs.size)
    return true
  end

end



