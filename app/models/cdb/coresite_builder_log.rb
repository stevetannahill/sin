class CoresiteBuilderLog < AutomatedBuildLog  
  MAX_SIZE = 65535
  
  def self.summary(details = false, start_time = Time.at(0), end_time = Time.now)
    builder_logs = CoresiteBuilderLog.where(:updated_at => (start_time)..(end_time))    
    success_summary = builder_logs.inject(Hash.new(0)) {|h,i| h[i.summary] += 1; h }
    return success_summary
  end
  
  def self.csv(summary_filter = "%", cid_filter = "%", start_time = Time.at(0), end_time = Time.now)   
    builder_logs = CoresiteBuilderLog.where("summary like ? AND name like ? AND updated_at BETWEEN ? AND ?", summary_filter, cid_filter, start_time, end_time).order('updated_at DESC')    

    outfile = ""
    csv_out = CSV.new(outfile)

    attributes_to_dump = CoresiteBuilderLog.new.attributes.keys - ["grid_id"]
    csv_out << attributes_to_dump
                
    builder_logs.each do |log|
      csv_out << attributes_to_dump.collect {|what| log[what]}
    end
    return outfile
  end
  
end