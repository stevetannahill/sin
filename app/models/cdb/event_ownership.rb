# == Schema Information
# Schema version: 20110802190849
#
# Table name: event_ownerships
#
#  id               :integer(4)      not null, primary key
#  eventable_id     :integer(4)
#  eventable_type   :string(255)
#  event_history_id :integer(4)
#

class EventOwnership < ActiveRecord::Base
belongs_to :eventable, :polymorphic => true
belongs_to :event_history #, :dependent => :destroy 
before_destroy :destroy_object

  # Only delete the actual history if there are no more references to it
  def destroy_object
    if event_history.event_ownerships.size == 1
      event_history.delete
    end
  end

end
