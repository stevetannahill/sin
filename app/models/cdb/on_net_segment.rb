# == Schema Information
#
# Table name: segments
#
#  id                       :integer(4)      not null, primary key
#  type                     :string(255)
#  status                   :string(255)
#  notes                    :text
#  segment_owner_role       :string(255)
#  segment_type_id          :integer(4)
#  cenx_id                  :string(255)
#  operator_network_id      :integer(4)
#  service_id               :integer(4)
#  created_at               :datetime
#  updated_at               :datetime
#  ethernet_service_type_id :integer(4)
#  use_member_attrs         :boolean(1)
#  path_network_id          :integer(4)
#  emergency_contact_id     :integer(4)
#  event_record_id          :integer(4)
#  sm_state                 :string(255)
#  sm_details               :string(255)
#  sm_timestamp             :integer(8)
#  prov_name                :string(255)
#  prov_notes               :text
#  prov_timestamp           :integer(8)
#  order_name               :string(255)
#  order_notes              :text
#  order_timestamp          :integer(8)
#  site_id                  :integer(4)
#
class OnNetSegment < Segment

  belongs_to :site


  def site_info_segment
    site ? site.name : "NO Site!"
  end

  def site_info
    if site
      return site.name
    end
    return ''
  end

  # Added by SIN, convenience method
  has_many :on_net_ovc_end_point_ennis, :class_name => 'OnNetOvcEndPointEnni', :foreign_key => 'segment_id', :dependent => :destroy

  validates_presence_of :site_id, :service_id
  validates_uniqueness_of :service_id, :message => "VPLS ID is aready taken", :unless => 'service_id == "N/A"'
  validate :valid_service_id?

  def self.get_active_onovcs
    all.reject{|onovc| onovc.get_prov_name == "Pending"}
  end

  # Added by SIN, used by inventory controller
  def buyer_segment_endpoints
    on_net_ovc_end_point_ennis.select do |e|
      e.segment_end_points.each { |end_point| end_point.segment.segment_owner_role == 'Buyer' }
    end
  end

  # Added by SIN, used by inventory controller
  def seller_segment_endpoints
    on_net_ovc_end_point_ennis.select do |e|
      e.segment_end_points.each { |end_point| end_point.segment.segment_owner_role == 'Seller' }
    end
  end

  def pull_down_name_onovc
    return "#{cenx_name} - #{cenx_id}"
  end

  def short_cenx_name
    cenx_name
  end

  def get_operator_network
    site_id ? site.operator_network : nil
  end

  def set_operator_network on
    raise "Cannot set the operator network for a On Net OVC."
  end

  def on_info_segment
    site_id ? site.on_info_site : "TBD"
  end

  def qos_policies
    on_net_ovc_end_point_ennis.collect {|c| c.qos_policies }.flatten.uniq
  end

  def config_description
    return "#{cenx_name}; #{cenx_id}"
  end

  def is_multi_node_segment
    return node_names.size>1
  end

  def is_on_mini_site?
    nodes = nodes_involved
    nodes.each do |n|
      return true if n.is_mini_site_node?
    end
    return false
  end

  def eth_cfm_config_required
    segment_end_points.each do |ep|
      if ep.eth_cfm_configured
        return true
      end
    end
    return false
  end

  def get_cenx_monitoring_segment_endpoints
    return segment_end_points.select{ |oe| (oe.is_connected_to_monitoring_port) }
  end

  def qos_policies_correct?
    return true
# Sprint Demo Hack
#    valid = true
#    on_net_ovc_end_point_ennis.each do |ep|
#      valid = (valid and ep.qos_policies_correct?)
#    end
#    return valid
  end

  def get_orphan_qos_policies
    site.qos_policies.select { |qos| qos.associated_endpoints_count == 0  }
  end

  def configure_qos_policies
    update = Hash.new

    if on_net_ovc_end_point_ennis.empty?
      update["Errors"] = ["Segment must contain valid Endpoints"]
    end
    on_net_ovc_end_point_ennis.each do |ep|
      update["Segment Endpoint: #{ep.on_switch_name}"] = ep.configure_qos_policies
    end

    orphans = Array.new
    get_orphan_qos_policies.each do |qos|
      orphans << "#{qos.direction} policy - #{qos.policy_name} [#{qos.policy_id}]"
    end
    orphans = ["None"] if orphans.empty?
    update["Orphan Policies on Site #{site.name}"] = orphans
    return update
  end

  def get_monitored_offovc_endpoints
    monitored_offovc_endpoints = []
    enni_endpoints = on_net_ovc_end_point_ennis.reject{|e| e.is_connected_to_test_port}

    enni_endpoints.each do |e|
     monitored_offovc_endpoints += e.get_attached_monitored_offovc_endpoints
    end

    return monitored_offovc_endpoints.flatten
  end

  def get_service_monitoring_params

    monitored_offovc_endpoints = []
    monitored_remote_endpoint = nil
    ip_ping_monitoring_is_required = false
    ccm_monitoring_is_required = false
    lbm_monitoring_is_required = false
    dmm_monitoring_is_required = false

    monitored_offovc_endpoints = get_monitored_offovc_endpoints

    if monitored_offovc_endpoints.size == 1
      monitored_remote_endpoint = monitored_offovc_endpoints.first

      if monitored_remote_endpoint.cos_test_vectors.collect {|v| v.test_type}.flatten.uniq.include? "Ethernet Loopback Test"
        ccm_monitoring_is_required = true
        lbm_monitoring_is_required = true
      end

      if monitored_remote_endpoint.cos_test_vectors.collect {|v| v.test_type}.flatten.uniq.include? "Ethernet Frame Delay Test"
        ccm_monitoring_is_required = true
        dmm_monitoring_is_required = true
      end

       if monitored_remote_endpoint.cos_test_vectors.collect {|v| v.test_type}.flatten.uniq.include? "Ping Active Test"
         ip_ping_monitoring_is_required = true
       end

      if lbm_monitoring_is_required and dmm_monitoring_is_required
        SW_ERR "ERROR: Both LBM and DMM Monitoring is required"
      end
    end

   return ip_ping_monitoring_is_required, ccm_monitoring_is_required, lbm_monitoring_is_required, dmm_monitoring_is_required, monitored_remote_endpoint
 end

  # Used to correct Egress stats for FLR calculation
  # Overridden in subclasses
  def ccm_frames_generated_internally_per_minute
    ip_ping_monitoring_is_required, ccm_monitoring_is_required, lbm_monitoring_is_required, dmm_monitoring_is_required, monitored_remote_endpoint = get_service_monitoring_params

    unless ccm_monitoring_is_required
      # no CCMs
      return 0
    end

    if is_in_ls_core_transport_path
      # CCM frame egress every 100ms but because this is a down MEP the far end ones not counted on ingress
      # The value below is derived from lab testing
      return 597.2 #not exactly 600
    else
      # CCM frame egress every second but are corrected by ingress CCMs from far end
      return 0
    end
  end

  def generate_config

    SW_ERR "On Net OVC: Attempting to generate config with invalid Qos Policies" unless qos_policies_correct?

    monitored_remote_endpoint = nil
    ip_ping_monitoring_is_required = false
    ccm_monitoring_is_required = false
    lbm_monitoring_is_required = false
    dmm_monitoring_is_required = false
    text = ""

    if has_monitoring_endpoint and eth_cfm_config_required #and !is_in_ls_core_transport_path
      ip_ping_monitoring_is_required, ccm_monitoring_is_required, lbm_monitoring_is_required, dmm_monitoring_is_required, monitored_remote_endpoint = get_service_monitoring_params
      monitored_offovc_endpoints = get_monitored_offovc_endpoints

      if monitored_offovc_endpoints.empty?
        text += "#**********************************************\n"
        text += "#**********************************************\n"
        text += "# !!! WARNING !!! \n"
        text += "# A Service Monitoring endpoint is configured in this Segment\n"
        text += "# When this is the case, data for the monitored Off Net OVC\n"
        text += "# must be entered into CDB BEFORE generating the On Net OVC config\n"
        text += "#**********************************************\n"
        text += "#**********************************************\n\n"
        return text
      end

    end

    text += "#----------------------------------------------\n"
    text += "# Configuration is required on nodes: #{node_info}\n"
    text += "# IP Ping Monitoring: #{ip_ping_monitoring_is_required}\n"
    text += "# CCM Monitoring: #{ccm_monitoring_is_required} LBM: #{lbm_monitoring_is_required} DMM: #{dmm_monitoring_is_required}\n"
    text += "#----------------------------------------------\n"
    text += "\n# Define new service ID on nodes: #{node_info}\n"
    text += "#--------------------------------------------\n"
    text += "configure service\n"
    text += "#{on_switch_name} customer 1 create\n"
    text += "description \"#{config_description}\"\n"
    text += "service-name \"#{config_description}\"\n" unless is_on_mini_site?
    text += "service-mtu 9186\n" #Changed (was 9190)

    # only configured in no MC lag is in picture
    if !is_multi_node_segment
      text += "per-service-hashing\n" unless is_on_mini_site?
    end

    text += "stp shutdown\n"
    text += "exit\nexit\nexit\n"

    text += "\n# Configure Ethernet CFM associations on #{node_info}\n"
    text += "#----------------------------------------------\n"

    # Configure all applicable domains and associations and
    # Ensure our MIPs respond to Link Trace
    domains = []
    if is_in_ls_core_transport_path
      if ccm_monitoring_is_required
        domains = [monitored_remote_endpoint.md_level.to_i]
      elsif is_on_mini_site?
        eps = on_net_ovc_end_point_ennis.reject{|e| !e.is_connected_to_monitored_endpoint? }
        eps.each do |e|
          e.segment_end_points.each do |me|
            domains << me.md_level.to_i
          end
        end
      end
      domains << 8
      domains.uniq!
    else
      domains = [2,3,4,5,6,7]
      if lbm_monitoring_is_required
        domains << monitored_remote_endpoint.get_ccm_monitoring_domain_id
      end
      domains.uniq!
    end

    domains.each do |i|

      # Figure out the level and formats
      level = i==8 ? 0:i
      dom_format = 'none'
      assoc_format = 'icc-based'
      if lbm_monitoring_is_required
        if level == monitored_remote_endpoint.get_ccm_monitoring_domain_id
          level = monitored_remote_endpoint.md_level.to_i
          dom_format = "string name #{monitored_remote_endpoint.md_name_ieee}"
          assoc_format = 'string'
        end
      end

      text += "configure eth-cfm domain #{i} format #{dom_format} level #{level}\n"
      a_name = "#{level}"

      if is_in_ls_core_transport_path
        # ICC name includes the CENX ID of the Path
        if paths.count == 1
          b_name = sprintf "%012d", paths.first.cenx_id
        else
          SW_ERR "Incorrect number of parent Paths - #{paths.count}"
          b_name = '!!! ERROR: Incorrect number of parent Paths'
        end
      else
        # ICC name includes the VPLS ID
        b_name = sprintf "%012d", service_id
      end

      icc_name = a_name + b_name
      text += "association #{service_id} format #{assoc_format} name \"#{icc_name}\"\n"
      text += "bridge-identifier #{service_id}\n"
      text += "mhf-creation #{is_on_mini_site? ? 'none' : 'default' }\n" #added to enable link trace
      text += "exit\n"

      if level == 0 && is_in_ls_core_transport_path && eth_cfm_config_required
        text += "ccm-interval 100ms\n"
        text += "remote-mepid #{is_on_mini_site? ? '1' : '2'}\n"
      end

      if level == 2 && !is_in_ls_core_transport_path && eth_cfm_config_required
        text += "ccm-interval 1\n"
        segment_end_points.each do |ep|
          if ! ep.is_connected_to_test_port
            text += ep.generate_remote_meps_config
          end
        end
      end

      if ccm_monitoring_is_required
        if i == monitored_remote_endpoint.get_ccm_monitoring_domain_id and level == monitored_remote_endpoint.md_level.to_i and !is_in_ls_core_transport_path
          text += "ccm-interval 1\n"
          segment_end_points.each do |ep|
            if ! ep.is_connected_to_test_port
              text += ep.generate_ccm_monitoring_remote_meps_config
            end
          end
        end
      end

    text += "exit\nexit\nexit\n\n"
    end

    get_orphan_qos_policies.each do |orphan|
      text += "\n# #{orphan.direction} QoS Policy #{orphan.policy_id} is orphaned\n"
      text += "# If it exists it should be removed by the below.\n"
      text += "#-----------------------------------------------------------------------------\n"
      text += orphan.generate_delete
    end

    #QoS Policies
    policies = on_net_ovc_end_point_ennis.collect {|ep| ep.qos_policies}.flatten.uniq
    #A little sorting magic
    ingress = policies.select { |policy| policy.is_a? IngressQosPolicy}
    egress = policies.select { |policy| policy.is_a? EgressQosPolicy}

    (ingress + egress).each do |policy|
      saps = policy.on_net_ovc_end_point_ennis.select { |ep| ep.segment == self}
      node_str = saps.collect { |sap| sap.node_names }.flatten.uniq.join('; ')
      sap_str = saps.collect {|sap| sap.on_switch_name}.join(", ")
      text += "\n# #{policy.direction} QoS Policy #{policy.policy_id} is required, it may already be configured on #{node_str} \n"
      text += "# It will be used by #{sap_str}\n"
      text += "# It should look as follows... \n"
      text += "# If its not already there configure it now.\n"
      text += "#-----------------------------------------------------------------------------\n"
      text += policy.generate_config
    end

    node_names.each do |n|

      # Figure out if this node is test ie: secondary_node
      # and deteremine whether Service Monitoring is in the picture
      is_primary_node = node_is_primary? n
      mon_port_endpoint = nil

      segment_end_points.each do |ep|
        if ep.is_connected_to_monitoring_port
          mon_port_endpoint = ep
        end
      end

      if (is_primary_node && mon_port_endpoint != nil)
        text += "\n# EXFO Monitoring is involved, MAC Fwd filter should already be configured on #{n} \n"
        text += "# It should look as follows... \n"
        text += "# If its not already there configure it now.\n"
        text += "#-----------------------------------------------------------------------------\n"

        text += mon_port_endpoint.generate_sm_mac_fwd_filter

        text += "\n# MAC Drop filter should also already be configured on #{n} \n"
        text += "# It should look as follows... \n"
        text += "# If its not already there configure it now.\n"
        text += "#--------------------------------------------------\n"
        text += mon_port_endpoint.generate_sm_mac_drop_filter

      end


      text += "\n# Configure Service Details on node #{n}\n"
      text += "#----------------------------------------------\n"
      text += "configure service #{on_switch_name}\n"
      text += "no shutdown\n"
      segment_end_points.each do |ep|
        if (ep.exists_on_node n)
          text += ep.generate_end_point_config is_primary_node
        end
      end

      if is_multi_node_segment
        text += "\n# Service Intermediate Point Configuration\n"
        text += "#----------------------------------------------\n"

        text += "spoke-sdp 100:#{service_id} create\nno shutdown\nexit\n"
        text += "no shutdown\n"
        text += "exit\nexit\n"
      end

    end

    text += "\n#Save Configuration\n"
    text += "#------------------\n"
    text += "exit\n"
    text += "admin save\n"

    #Configure applicable SAA tests on all nodes
    if !is_in_ls_core_transport_path
      if eth_cfm_config_required
        test_data = get_saa_test_details
        test_data.each do |node, tests|
          is_primary_node = node_is_primary? node

          text += "\n#----------------------------------------------\n"
          text += "# DO NOT PASTE THE FOLLOWING CONFIG ON THE  CLI!!!\n"
          text += "# LOG INTO SAM: Configure SAA tests on node #{node} (Primary Node: #{node_is_primary? node})\n"
          text += "# Follow the steps in the Path Turnup Process Document\n"
          text += "# The commands below should NOT be copied to the CLI they are \n"
          text += "# here just for reference to compare with the config pushed by SAM\n"
          text += "#----------------------------------------------\n"
          text += "#configure saa\n"
          tests.each do |test|
            source_mep = test[:source_mep]
            destination_mep = test[:destination_mep]
            source = OnNetOvcEndPointEnni.find(test[:source_id])
            destination = OnNetOvcEndPointEnni.find(test[:destination_id])
            destination_mac = destination.enni.get_mac_address is_primary_node || !destination.enni.is_multi_chassis
            fwding_class = ServiceCategories::ON_NET_OVC_COS_TYPES.sort_by { |k,v| v[:priority] }.collect{|k,v| v}.first[:fwding_class]
            text += "test \"#{cenx_id} mep #{source_mep} to mep #{destination_mep}\" owner \"sas:x:yy:zz:n\"\n"
            text += "description \"Frame Delay Test On Net OVC #{service_id} (#{cenx_id}) mep #{source_mep} to mep #{destination_mep}\"\n"
            text += "type\n"
            text += "eth-cfm-two-way-delay #{destination_mac} mep #{source_mep} domain 2 association #{service_id} fc \"#{fwding_class}\" count 60\n"
            text += "exit\n"

            # TODO fix this - should use min value from OSS DATA - delay constant based on Cosis involved
            # Also need AR: 1-3094425 (for us granularity) fixed before this can be used.
            text += "jitter-event rising-threshold 5\n"
            text += "latency-event rising-threshold 5\n"

            text += "accounting-policy 3\n"
            text += "continuous\n"
            text += "no shutdown\n"
            text += "exit\n\n"
          end
        end
      end
    end

    text += "\n#-------------------------------\n"
    text += "# Useful Troubleshoting Commands"
    text += "\n#-------------------------------\n"

    text += "show service id #{service_id} all\n"

    text += "\n# Per Endpoint Commands\n"
    segment_end_points.each do |ep|
      text += "show service id #{service_id} #{ep.on_switch_name}\n"
      text += "show service id #{service_id} #{ep.on_switch_name} stats\n"
      text += "monitor service id #{service_id} #{ep.on_switch_name}\n"
      text += "clear service statistics #{ep.on_switch_name} counters\n"
    end

    text += "\n# SDP Commands\n"
    text += "show service id #{service_id} sdp 100:100010\n"
    text += "show service sdp 100\n"
    text += "show service sdp 100 detail\n"
    text += "oam sdp-ping 100 resp-sdp 100\n"
    text += "oam svc-ping <PEER_IP_ADDRESS> service #{service_id}\n"
    text += "monitor service id #{service_id} sdp 100:100010\n"
    text += "clear service statistics id #{service_id} counters\n"


    text += "\n# Segment Test Port ACL & VPLS MAC Learning Commands\n"
    text += "show filter mac\n"
    text += "show filter mac 199 associations\n"
    text += "show filter mac 199 counters\n"
    text += "show service id #{service_id} fdb\n"
    text += "show service id #{service_id} fdb detail\n"

    if eth_cfm_config_required
      text += "\n#Segment ETH-CFM (802.1ag & Y.1731) Commands\n"
      text += "show eth-cfm domain 1\n"
      text += "show eth-cfm domain 1 detail\n"
      text += "show eth-cfm domain 1 association #{service_id}\n"
      text += "show eth-cfm domain 1 association #{service_id} detail\n"
      text += "show eth-cfm mep 2 domain 1 association #{service_id}\n"
      text += "show eth-cfm mep 2 domain 1 association #{service_id} all-remote-mepids\n"
      text += "oam eth-cfm loopback <MAC_ADDR> mep 1 domain 1 association #{service_id} send-count 5\n"
      text += "oam eth-cfm loopback <MAC_ADDR> mep 1 domain 1 association #{service_id} send-count 3 size 1000\n"
      text += "oam eth-cfm linktrace <MAC_ADDR> mep 1 domain 1 association #{service_id}\n"
      text += "oam eth-cfm two-way-delay-test <MAC_ADDR> mep 1 domain 1 association #{service_id}\n"
      text += "oam eth-cfm eth-test <MAC_ADDR> mep 1 domain 1 association #{service_id} data-length 128\n"
      text += "show eth-cfm mep 2 domain 1 association #{service_id} eth-test-results\n"
      text += "oam eth-cfm one-way-delay-test <MAC_ADDR> mep 1 domain 1 association #{service_id}\n"
      text += "show eth-cfm mep 2 domain 1 association #{service_id} one-way-delay-test\n"
      text += "oam eth-cfm one-way-delay-test <MAC_ADDR> mep 1 domain 1 association #{service_id}\n"
      text += "show eth-cfm mep 2 domain 1 association #{service_id} one-way-delay-test\n"
    end

    return text
  end

  def get_alarm_keys
    # Get the Ethernet 2 way delay test associated with the MEP
      api = SamIf::StatsApi.new(get_network_manager)
      result, data = api.get_inventory(
          "ethernetoam.CfmTwoWayDelayTest",
          {"maintenanceAssociationId" => service_id},
          ["objectFullName"])
      if !result
        SW_ERR "Failed to create delay test alarm histories for Service: #{service_id}"
      else
        test_object_names = data.map {|i| i["objectFullName"]}
      end
      api.close

      return result, test_object_names
  end


  def go_in_service    
    return {:result => false, :info => "Failed to initialise"} if on_net_ovc_end_point_ennis.size == 0

    result = true
    info = ""
    if result
      sm_histories << SmHistory.create(:event_filter => "SM #{self.class.name}:#{cenx_id}") if sm_histories.empty?

      # Create a new Maintenance with a record being cleared ie not in maintenance period
      if mtc_periods.empty?
        self.mtc_periods = [MtcHistory.create(:event_filter => "Mtc Segment:#{cenx_id}")]
        self.set_mtc(false)
      end

      result, test_object_names = get_alarm_keys
      if result
        if !test_object_names.empty?
          # map the major alarm to Failed rather than UNAVAILABLE
          test_mapping = AlarmSeverity.default_alu_alarm_mapping 
          test_mapping["major"] = AlarmSeverity::FAILED
          new_alarms = {} 
          test_object_names.each {|filter| new_alarms[filter] = {:type => AlarmHistory, :mapping => test_mapping}}
          result = update_alarm_history_set(new_alarms)
        end 
      end
    end

    info = "Failed to initialise" unless result
    return {:result => result, :info => info}
  end
=begin  
  def go_in_service
    return false if on_net_ovc_end_point_ennis.size == 0

    result = true
    if result
      sm_histories << SmHistory.create(:event_filter => "SM #{self.class.name}:#{cenx_id}") if sm_histories.empty?

      # Create a new Maintenance with a record being cleared ie not in maintenance period
      if mtc_periods.empty?
        self.mtc_periods = [MtcHistory.create(:event_filter => "Mtc Segment:#{cenx_id}")]
        self.set_mtc(false)
      end

      result, test_object_names = get_alarm_keys
      if result
        if !test_object_names.empty?
          # map the major alarm to Failed rather than UNAVAILABLE
          test_mapping = AlarmSeverity.default_alu_alarm_mapping
          test_mapping["major"] = AlarmSeverity::FAILED
          new_alarms = {}
          test_object_names.each {|filter| new_alarms[filter] = {:type => AlarmHistory, :mapping => test_mapping}}
          result = update_alarm_history_set(new_alarms)
        end
      end
    end

    return result
  end
=end

  def alarm_state
    state = AlarmRecord.select_lowest_alarm_or_warn(alarm_histories.collect{|ah| ah.alarm_state}, AlarmSeverity::FAILED)
    ers = segment_end_points.collect {|ep| ep.event_record_id if !ep.is_connected_to_test_port}
    er_states = EventRecord.where("id in (?)", ers)
    er_states << state
    state = AlarmRecord.highest(er_states)

    state = AlarmRecord.new(:time_of_event => 0, :state => AlarmSeverity::OK, :details => "", :original_state => "cleared") if state == nil
    return state
  end

  def alarm_history
    history = AlarmHistory.merge(alarm_histories) {|events| AlarmRecord.select_lowest_alarm_or_warn(events, AlarmSeverity::FAILED)}
    segment_end_points.each {|ep| history = ep.alarm_history + history}
    return history
  end

  def sla_availability_value
    value = 1
    decimal_places = nil
    segment_end_points.each do |ep|
      sla_value = ep.sla_availability_value

      unless sla_value == 1
        sla_decimal_places = ((sla_value * 100).round == (sla_value * 100)) ? 0 : ("%f" % (sla_value * 100)).gsub(/0*$/, "").split(".")[1].length
        decimal_places = (decimal_places && decimal_places < sla_decimal_places) ? decimal_places : sla_decimal_places
      end
      value *= ep.sla_availability_value ** (1.0/segment_end_points.length)
    end
    return (decimal_places ? (value * 100 * (10 ** decimal_places)).round / (10.0 ** decimal_places) : value).to_s

  end

  # Return a Stats object which is used to hold/get Stats information
  # for the given time period
  def stats_data(start_time, end_time, current_time = Time.now, adjust_time = true)
    stats = SamStatsArchive.new(self, start_time, end_time, current_time, adjust_time)
    return stats
  end

def get_saa_test_details
  #Return object like
  #{<node name> => [{:source_mep => <string>, :source_id => <int>,
  #                  :destination_mep => <string>, :destination_id => <int>},],}
  test_data = Hash.new
  endpoints = on_net_ovc_end_point_ennis
  #Remove testing end points.
  endpoints.reject! {|ep| ep.is_connected_to_test_port}
  destinations = endpoints.dup

  #For every endpoint, remove that endpoint from the list of destinations then
  # make a test from every mep of the source to every mep of
  # every destination endpoint.
  endpoints.each do |source|
    #Since tests are originating from this source to all other destinations
    # we don't need to test back to here
    destinations.delete(source)
    #Get the endpoint's node(s)
    nodes = Array.new
    demarc = Demarc.find(source.demarc_id)
    demarc.ports.each do |port|
      nodes << Node.find(port.node_id).name
    end
    nodes.uniq!

    meps = source.get_mep_ids
      meps.each do |mep|
        node = nodes[0]
        #Check if it's a secondary mep and put in secondary node
        if mep =~ /\d+1/
          node = nodes.select { |node| not node_is_primary? node  }.first
        end
        #Create new test array if we haven't seen this node yet
        test_data[node] = Array.new unless test_data[node]

        destinations.each do |dest|
          dest_meps = dest.get_mep_ids
          dest_meps.each do |dmep|
            test_data[node] << {:source_id => source.id,
                                :destination_id => dest.id,
                                :source_mep => mep,
                                :destination_mep => dmep}
          end
        end
      end
    end
    return test_data
  end

  # Get all MEPS of a given level for this whole On Net OVC
  def get_mep_alarm_keys(acceptable_meg_levels = (0..15).to_a)
    return_code = true
    new_mep_alarms = []
    # Get the SAM object name for this sap
    api = SamIf::StatsApi.new(get_network_manager)
    nodes_involved.each do |n|
      result, data = api.get_inventory(
          "ethernetoam.Mep",
          {"maintenanceAssociationId" => service_id.to_s},
          ["objectFullName", "maintDomainLevel"])
      if !result || data.empty?
        SW_ERR "Failed to get Meps for Service: #{service_id.to_s} Node: #{node_info.to_s}"
        mep_object_names = []
        return_code = false
      else
        # Find all meps that are in the acceptable MEG level
        mep_object_names = data.collect{|mep_data| mep_data["objectFullName"] if acceptable_meg_levels.include?(mep_data["maintDomainLevel"].to_i)}.compact
      end

      if !mep_object_names.empty?
        new_mep_alarms += mep_object_names
      end
    end
    api.close

    return return_code, new_mep_alarms
  end

  def has_monitoring_endpoint
    segment_end_points.each do |ep|
      if ep.is_connected_to_monitoring_port
        return true
      end
    end
    return false
  end

  def monitored?
    # On Net OVCs are always monitored
    return true
  end

  private

  def valid_service_id?
    return if service_id == "N/A"
    unless ( service_id && service_id.to_i >= site.service_id_low && service_id.to_i <= site.service_id_high )
      errors.add(:service_id, site ? " ID not valid for #{site.name}. Range is: #{site.service_id_low} to #{site.service_id_high} or N/A" : " ID not valid. No site selected.")
    end
  end

  def node_is_primary? node
    name = (node.is_a? Node) ? node.name : node
    return !(name =~ /.ALU01/).nil?
  end


end

