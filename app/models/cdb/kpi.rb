# == Schema Information
#
# Table name: kpis
#
#  id                :integer(4)      not null, primary key
#  name              :string(255)
#  for_klass         :string(255)
#  units             :string(255)
#  warning_threshold :float
#  error_threshold   :float
#  warning_time      :integer(4)
#  error_time        :integer(4)
#  delta_t           :integer(4)
#  actual_kpi        :boolean(1)
#  created_at        :datetime
#  updated_at        :datetime
#
class Kpi < ActiveRecord::Base
  has_many :kpi_ownerships, :dependent => :destroy

  KPI_NAMES = %w(ingress_util egress_util delay delay_variation flr)
  FOR_KLASS = [EnniNew.name, BxCosTestVector.name, OnNetOvcEndPointEnni.name, SpirentCosTestVector.name]
  UNITS = %w(us % ms secs Mbits/sec)
  KPI_BY_KLASS = {EnniNew.name => %w(ingress_util egress_util), 
                  BxCosTestVector.name => %w(delay delay_variation flr),
                  OnNetOvcEndPointEnni.name =>%w(ingress_util egress_util),
                  SpirentCosTestVector.name => %w(delay delay_variation flr)}
                  
  
  validates_inclusion_of :name, :in => KPI_NAMES
  validates_inclusion_of :for_klass, :in => FOR_KLASS
  validates_inclusion_of :units, :in => UNITS, :allow_nil => true  
 
  after_save :general_after_save
  after_save :update_lkp, :if => :changed?
  

  def general_after_save
    # There should only be one mask - If the mask has been updated (ie it has a kpi_ownership) then update
    if kpi_ownerships.first != nil && actual_kpi == false
      kpi_ownerships.first.kpiable.update_kpi(kpi_ownerships.first.kpiable, nil, name, for_klass) 
    end
  end
  
  def update_lkp
    if actual_kpi
      kpi_ownerships.where(kpiable_type: "CosTestVector").each do |ownership|
        ownership.kpiable.update_lkp
      end
    end
  end
  
end

