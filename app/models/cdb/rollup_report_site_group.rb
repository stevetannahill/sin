class RollupReportSiteGroup < ActiveRecord::Base
  include CommonException
  belongs_to :site_group

  def site_percentage(count_total)
    "%0.02f" % ((count_total.to_f / self.site_count.to_f) * 100.0)
  end

  def site_ok_count
    self.site_count - self.site_minor_count - self.site_major_count
  end

  def metric_percentage(count_total)
    "%0.02f" % ((count_total.to_f / self.metric_count.to_f) * 100.0)
  end

  def metric_ok_count
    self.metric_count - self.metric_minor_count - self.metric_major_count
  end

  def demarc_percentage(count_total)
    "%0.02f" % ((count_total.to_f / self.demarc_count.to_f) * 100.0)
  end

  def demarc_ok_count
    self.demarc_count - self.demarc_minor_count - self.demarc_major_count
  end

end
