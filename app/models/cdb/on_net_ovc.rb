# encoding: UTF-8
# == Schema Information
#
# Table name: segments
#
#  id                       :integer(4)      not null, primary key
#  type                     :string(255)
#  status                   :string(255)
#  notes                    :text
#  segment_owner_role           :string(255)
#  segment_type_id              :integer(4)
#  cenx_id                  :string(255)
#  operator_network_id      :integer(4)
#  site_id              :integer(4)
#  service_id               :integer(4)
#  created_at               :datetime
#  updated_at               :datetime
#  ethernet_service_type_id :integer(4)
#  use_member_attrs         :boolean(1)
#  path_network_id           :integer(4)
#  emergency_contact_id     :integer(4)
#  event_record_id          :integer(4)
#  sm_state                 :string(255)
#  sm_details               :string(255)
#  sm_timestamp             :integer(8)
#  prov_name                :string(255)
#  prov_notes               :text
#  prov_timestamp           :integer(8)
#  order_name               :string(255)
#  order_notes              :text
#  order_timestamp          :integer(8)
#

class OnNetOvc < OnNetSegment
  belongs_to :site

  # Added by SIN, convenience method
  has_many :on_net_ovc_end_point_ennis, :class_name => 'OnNetOvcEndPointEnni', :foreign_key => 'segment_id', :dependent => :destroy

  # Added by SIN, used by inventory controller
  def buyer_segment_endpoints
    on_net_ovc_end_point_ennis.select do |e| 
      e.segment_end_points.each { |end_point| end_point.segment.segment_owner_role == 'Buyer' }
    end
  end

  # Added by SIN, used by inventory controller
  def seller_segment_endpoints
    on_net_ovc_end_point_ennis.select do |e| 
      e.segment_end_points.each { |end_point| end_point.segment.segment_owner_role == 'Seller' }
    end
  end  
  
  def cenx_name
    short_sp_name = CenxNameTools::CenxNameHelper.shorten_name path_network.service_provider.name
    short_site_name = CenxNameTools::CenxNameHelper.shorten_name site_info

    type = CenxNameTools::CenxNameHelper.generate_type_string self
    return "#{type}:#{short_sp_name}:#{short_site_name}:#{on_switch_name}"
  end
  
  def on_switch_name
    return "vpls #{service_id}"
  end

end
