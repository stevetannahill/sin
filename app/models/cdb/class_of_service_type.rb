# == Schema Information
# Schema version: 20110802190849
#
# Table name: class_of_service_types
#
#  id                             :integer(4)      not null, primary key
#  name                           :string(255)
#  operator_network_type_id       :integer(4)
#  cos_id_type_enni               :string(255)
#  cos_mapping_enni_ingress       :string(255)
#  cos_mapping_uni_ingress        :string(255)
#  availability                   :string(255)
#  frame_loss_ratio               :string(255)
#  mttr_hrs                       :string(255)
#  notes                          :text
#  created_at                     :datetime
#  updated_at                     :datetime
#  cos_id_type_uni                :string(255)     default("pcp")
#  cos_marking_enni_egress        :string(255)
#  cos_marking_uni_egress         :string(255)
#  cos_marking_enni_egress_yellow :string(255)     default("N/A")
#  cos_marking_uni_egress_yellow  :string(255)     default("N/A")
#  type                           :string(255)
#


class ClassOfServiceType < TypeElement
  self.abstract_class = false
  
  belongs_to :operator_network_type
  has_and_belongs_to_many :ethernet_service_types
  has_many :service_level_guarantee_types, :dependent => :destroy
  has_many :bw_profile_types, :dependent => :destroy
  has_many :cos_instances, :dependent => :nullify
  has_many :segment_end_point_types, :dependent => :nullify

  validates_presence_of :name, :operator_network_type_id
  validates_uniqueness_of :name , :scope => :operator_network_type_id 
#  validates_format_of :cos_mapping_enni_ingress,
#                      :with => /(^\*$)|(^([0-56]\s*;\s*)*([0-56])$)/,
#                      :message => "Should be *, integer [0-56] or colon separated list of integers [0-56]"
  validates_inclusion_of :cos_id_type_enni, :in => HwTypes::ENNI_COS_ID_TYPES,
                      :message => "invalid. Use one of #{HwTypes::ENNI_COS_ID_TYPES.join(", ")}."
  validates_inclusion_of :cos_marking_enni_egress, :in => HwTypes::COS_ENNI_EGRESS_MARKING,
                      :message => "invalid. Use one of #{HwTypes::COS_ENNI_EGRESS_MARKING.join(", ")}."
  validates_inclusion_of :cos_marking_enni_egress_yellow, :in => HwTypes::COS_ENNI_EGRESS_MARKING,
                      :message => "invalid. Use one of #{HwTypes::COS_ENNI_EGRESS_MARKING.join(", ")}."
  validates_inclusion_of :cos_id_type_uni, :in => HwTypes::UNI_COS_ID_TYPES,
                      :message => "invalid. Use one of #{HwTypes::UNI_COS_ID_TYPES.join(", ")}."
  validates_inclusion_of :cos_marking_uni_egress, :in => HwTypes::COS_UNI_EGRESS_MARKING,
                      :message => "invalid. Use one of #{HwTypes::COS_UNI_EGRESS_MARKING.join(", ")}."
  validates_inclusion_of :cos_marking_uni_egress_yellow, :in => HwTypes::COS_UNI_EGRESS_MARKING,
                      :message => "invalid. Use one of #{HwTypes::COS_UNI_EGRESS_MARKING.join(", ")}."
#  validate :valid_cos_values?

 #:with => /(^$)|(^(\d{1,3}\.){3}(\d{1,3})$)|(^(([A-Fa-f0-9]{2}):){5}([A-Fa-f0-9]{2})$)/,

  def slg_count
    return "#{service_level_guarantee_types.size}"
  end

  def cosi_count
    return cos_instances.size
  end

  def get_candidate_cos_mapping_enni_ingress
    #return cos_mapping_enni_ingress ? cos_mapping_enni_ingress.split(/\s*;\s*/) : []
    return cos_mapping_enni_ingress ? cos_mapping_enni_ingress : ""
  end

  def get_candidate_cos_mapping_uni_ingress
    #return cos_mapping_uni_ingress ? cos_mapping_uni_ingress.split(/\s*;\s*/) : []
    return cos_mapping_uni_ingress ? cos_mapping_uni_ingress : ""
  end
  
  private

end
