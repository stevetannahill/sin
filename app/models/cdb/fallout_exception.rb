class FalloutException < ActiveRecord::Base
  include Comparable  
  include CommonException

  # Lowest first 
  EXCEPTION_PRIORITY = [:ok, :minor, :major, :critical]
  EXCEPTION_TYPES = %w(version audit inventory ordering build)

  class << self
    attr_accessor :exception_types
    # @exception_types = EXCEPTION_TYPES
    
    def exception_types
      @exception_types = EXCEPTION_TYPES if @exception_types.nil?
      @exception_types
    end

  end
  

  belongs_to :fallout_item, :polymorphic => true
  belongs_to :path, :foreign_key => :fallout_item_id, :foreign_type => :fallout_item_type

  validates_presence_of  :fallout_item
  validate :validate_exception_type
  validates :timestamp, :presence => true
  validates_inclusion_of :severity, :in => EXCEPTION_PRIORITY

  after_save :update_fallout_item
  
  scope :current, where('clear_timestamp IS NULL')
  scope :cleared, where('clear_timestamp IS NOT NULL')

  def validate_exception_type
    if !FalloutException.exception_types.include?(exception_type)
      errors.add(:exception_type, "is not valid - #{exception_type} should be one of #{FalloutException.exception_types}")
    end
  end
  
  def severity
    read_attribute(:severity).to_sym
  end
  
  def severity= (value)
    write_attribute(:severity, value.to_s)
  end

  def <=>(other)
    EXCEPTION_PRIORITY.index(self.severity) <=> EXCEPTION_PRIORITY.index(other.severity)              
  end

  def time
    # timestamp is reported in seconds
    Time.at(timestamp)
  end

  def clear_time
    # timestamp is reported in seconds
    Time.at(clear_timestamp)
  end
  
  private
  
  def update_fallout_item
    obj = self.fallout_item
    if obj
      if latest_exception = obj.latest_fallout_exception
        obj.fallout_timestamp = latest_exception.timestamp
        obj.fallout_details = latest_exception.details
        obj.fallout_exception_type = latest_exception.exception_type
        obj.fallout_severity = latest_exception.severity
        obj.fallout_data_source = latest_exception.data_source
        obj.fallout_category = latest_exception.category
        obj.fallout_exception_id = latest_exception.id
      else
        obj.fallout_timestamp = ''
        obj.fallout_details = ''
        obj.fallout_exception_type = ''
        obj.fallout_severity = 'ok'
        obj.fallout_data_source = ''
        obj.fallout_category = ''
        obj.fallout_exception_id = ''
      end
      obj.save
    end
  end
end
