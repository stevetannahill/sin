# == Schema Information
# Schema version: 20110802190849
#
# Table name: member_attr_instances
#
#  id                   :integer(4)      not null, primary key
#  member_attr_id       :integer(4)
#  affected_entity_id   :integer(4)
#  affected_entity_type :string(255)
#  value                :string(255)     default("TBD")
#  created_at           :datetime
#  updated_at           :datetime
#  type                 :string(255)
#  owner_id             :integer(4)
#  owner_type           :string(255)
#

class MemberHandleInstance < MemberAttrInstance
  belongs_to :owner, :polymorphic => true
  belongs_to :affected_entity, :polymorphic => true
  
  belongs_to :service_provider, :foreign_key => 'owner_id'
  
  validates_presence_of :owner_id, :owner_type
  validates_uniqueness_of :owner_id, :scope => [:member_attr_id, :owner_type, :affected_entity_id, :affected_entity_type]
  
  after_save :set_path_sphinx_delta_flag
  
  def name
  	if affected_entity
    	affected_entity.member_handle_prefix(owner) + super
    else
    	super
    end
  end
  
  def form_id
    "mai_" + member_attr_id.to_s + "_" + owner_id.to_s + "_" + owner_type
  end

  def set_path_sphinx_delta_flag
    ReindexSphinx.schedule
    
    # Denormalize member handle into Sin Path Service Provider Sphinx table
    if affected_entity_type == 'Path' && owner_type == 'ServiceProvider'
      sesps = SinPathServiceProviderSphinx.where(:path_id => affected_entity_id, :service_provider_id => owner_id).first
      if sesps
        sesps.member_handle = value
        sesps.save
      end
    end
  end
end
