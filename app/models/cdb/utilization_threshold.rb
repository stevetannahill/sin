# == Schema Information
#
# Table name: utilization_thresholds
#
#  id                         :integer(4)      not null, primary key
#  sla_warning_threshold      :float           default(0.5)
#  utilization_high_time_over :integer(4)      default(7200)
#  utilization_low_time_over  :integer(4)      default(86400)
#  utilization_high_threshold :float           default(0.5)
#  utilization_low_threshold  :float           default(0.25)
#

class UtilizationThreshold < ActiveRecord::Base

  def self.sla_warning_threshold
    @sla_warning_threshold ||= first.sla_warning_threshold * 100
  end
  
  def self.utilization_high_threshold
    @utilization_high_threshold ||= first.utilization_high_threshold * 100
  end
  
  def self.utilization_low_threshold
    @utilization_low_threshold ||= first.utilization_low_threshold * 100
  end

  def self.utilization_high_time_over
    @utilization_high_time_over ||= first.utilization_high_time_over
  end

  def self.utilization_low_time_over
    @utilization_low_time_over ||= first.utilization_low_time_over
  end
  
end
