class CellSite < Site
  validates_presence_of :site_layout
  validate :valid_service_ids?

  SERVICE_ID_REQUIRED_SITES = [
     "StandardExchange7750",
     "StandardExchange7450",
  ]

  private
  def valid_service_ids?
    if SERVICE_ID_REQUIRED_SITES.include? site_layout
      validates_presence_of :service_id_low, :service_id_high
      validates_range_of :service_id_low, :service_id_high, :overlapping => false, :greater_than_or_equal_to => 0, :only_integer => true, :unique_ends => true
    else
      self.service_id_low = 0 if service_id_low.nil?
      self.service_id_high = 49999 if service_id_high.nil?
    end
  end
  
end

