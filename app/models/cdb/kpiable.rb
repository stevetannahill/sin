module Kpiable
 
  def self.included(base)
    base.has_many :kpis, :through => :kpi_ownerships, :source => :kpi, :dependent => :destroy
    base.has_many :kpi_ownerships, :as => :kpiable, :dependent => :destroy
  end
  
  # Should only be called by the leaf objects ie EnniNew, BxCostTestVector etc
  def get_kpi_masks(kpi_name, kpi_type)
    objects = kpi_hierarchy(kpi_type)
    objects.reverse.collect {|object| [object[0], object[0].get_kpi_mask(kpi_name, kpi_type)] if object[0].get_kpi_mask(kpi_name, kpi_type) != nil}.compact
  end
  
  # the parent can be nil but it will only return the first KPI. This is useful when leaf objects want to get their
  # KPI (as they only have one) so they don't have to give the parent  
  def get_kpi(kpi_name, kpi_type, parent)
    if parent != nil
      Kpi.joins(:kpi_ownerships).where("`kpi_ownerships`.`kpiable_id` = ? AND `kpi_ownerships`.`kpiable_type` = ? AND `kpi_ownerships`.`parent_type` = ? AND `kpi_ownerships`.`parent_id` = ? AND `kpis`.`name` = ? AND `kpis`.`for_klass` = ? AND `kpis`.`actual_kpi` = 1",
                                  self.id, self.class.base_class.name, parent.class.base_class.name, parent.id, kpi_name, kpi_type).readonly(false).first      
    else
      kpis.find_by_name_and_for_klass_and_actual_kpi(kpi_name, kpi_type, true)
    end 
  end
  
  def get_kpi_mask(kpi_name, kpi_type)
    kpis.find_by_name_and_for_klass_and_actual_kpi(kpi_name, kpi_type, false)
  end
   
   
  # Should be overridden by all classes 
  def update_kpi(parent, parent_kpi, kpi_name, kpi_object)
    SW_ERR "Method must be overridden"
  end
      
  # Used when a leaf object is created and it wants to create it's KPIs
  # This will get the KPI hierarchy and calcluate the KPIs at each level in case the
  # hierarchy does not have any KPIs yet
  def kpi_setup(kpi_name, kpi_object)
    objects = kpi_hierarchy(kpi_object)
    parent_kpi = nil
    objects.reverse_each {|object| parent_kpi = object[0].calculate_kpi(kpi_name, kpi_object, parent_kpi, object[1])}
  end
    
  # parent_kpi is the actual KPI and this will be merged with the mask kpi to make a new actual KPI
  # parent_kpi can be nil but parent must always be passed
  def calculate_kpi(kpi_name, kpi_object, parent_kpi, parent)
    old_kpi = get_kpi(kpi_name, kpi_object, parent)
    #Rails.logger.debug "calculate_kpi #{self.class}:#{self.id} #{parent_kpi.inspect} #{parent.inspect} #{old_kpi.inspect}"
    
    # if the parent_kpi is the same then nothing to do
    if parent_kpi != nil && old_kpi != nil && parent_kpi.id == old_kpi.id
      return parent_kpi
    end
    
    kpi_mask = get_kpi_mask(kpi_name, kpi_object)
    # Can't find the parent KPI or a mask so nothing to do
    if !(parent_kpi == nil && kpi_mask == nil)     
      if parent_kpi == nil
        parent_kpi = kpi_mask
      end
      # There is no mask so either create a pointer to the parent_kpi or
      # update the existing pointer to point to parent_kpi
      if kpi_mask == nil
        if old_kpi == nil
          KpiOwnership.create(:parent => parent, :kpiable => self, :kpi => parent_kpi)
        elsif old_kpi.id != parent_kpi.id
          # Update the pointer to the new KPI
          kpi_owner = kpi_ownerships.find_by_kpi_id(old_kpi.id)
          kpi_owner.kpi = parent_kpi
          kpi_owner.save
        end
      else        
        # There is a mask so calculate the new KPI
        new_kpi = parent_kpi
        [:units, :warning_threshold, :error_threshold, :error_time, :warning_time, :delta_t].each do |attribute|
          if kpi_mask[attribute] != nil
            new_kpi[attribute] =  kpi_mask[attribute]
          end
        end
        
        # Update the mask parent
            
        if old_kpi == nil
          # There is is no KPI so create a KPI and point to it          
          old_kpi = Kpi.create(:name => new_kpi.name, :for_klass => new_kpi.for_klass, :units => new_kpi.units, :actual_kpi => true)
          KpiOwnership.create(:parent => parent, :kpiable => self, :kpi => old_kpi)
        end
        # Update the KPI
        new_values = new_kpi.attributes    
        new_values.delete("id")
        new_values.delete("kpiable_type")
        new_values.delete("kpiable_id")
        new_values["actual_kpi"] = true
        old_kpi.update_attributes(new_values)
        parent_kpi = old_kpi
      end
    end 
    return parent_kpi
  end
  
  
  # KPI API's Exception processing - CosTestVector
  # These should only be called on leaf objects ie EnniNew, BxCosTestVector SprientCostTestVector and OnNetOvcEndPointEnni  
  KPI_METRIC_MAPPING = {:delay => "delay", :delay_variation => "delay_variation", :frame_loss => "flr", :frame_loss_ratio => 'flr'}  
  DIRECTION_MAPPING = {:ingress => "ingress_util", :egress => "egress_util"}
  SEVERITY_TO_COUNT_MAPPING = {:major => :error_time, :minor => :warning_time}  
  SEVERITY_MAPPING = {:major => :error_threshold, :minor => :warning_threshold}  
  
    
  # APIs for frame loss, delay and delay variation
  def get_errored_period_threshold(severity_level, metric) 
    result = nil 
    kpi = get_kpi(KPI_METRIC_MAPPING[metric], self.class.name, nil)
    if kpi == nil
      SW_ERR "No KPI found for #{self.class}:#{self.id} KPI #{KPI_METRIC_MAPPING[metric]} #{metric}"
    else
      result = kpi[SEVERITY_MAPPING[severity_level]].to_f
    end
    return result
  end
    
  def get_errored_period_delta_time_secs
    kpi = get_kpi("delay", self.class.name, nil)
    if kpi == nil
      SW_ERR "No KPI found for #{self.class}:#{self.id}"
    else
      kpi = kpi[:delta_t]
    end
    return kpi
  end
    
  def get_errored_period_max_errored_count(severity_level, metric)        
    result = nil
    kpi = get_kpi(KPI_METRIC_MAPPING[metric], self.class.name, nil)
    if kpi == nil
      SW_ERR "No KPI found for #{self.class}:#{self.id} level #{severity_level} metric #{metric}"
    else
      count_in_seconds = kpi[SEVERITY_TO_COUNT_MAPPING[severity_level]]    
      result = count_in_seconds/kpi[:delta_t]
    end
    return result
  end
  
  
  # APIs for Utilization    
  def get_utilization_threshold_mbps(direction, severity_level)        
    result = 0
    kpi = get_kpi(DIRECTION_MAPPING[direction], self.class.name, nil)
    if kpi == nil
      SW_ERR "No KPI found for #{self.class}:#{self.id} KPI #{DIRECTION_MAPPING[direction]} #{direction}"
    else
      threshold_in_percent = kpi[SEVERITY_MAPPING[severity_level]]
      result = (rate.to_f * threshold_in_percent.to_f)/100
    end
    return result
  end
  
  def get_utilization_threshold_percent(direction, severity_level)        
     result = 0
     kpi = get_kpi(DIRECTION_MAPPING[direction], self.class.name, nil)
     if kpi == nil
       SW_ERR "No KPI found for #{self.class}:#{self.id} KPI #{DIRECTION_MAPPING[direction]} #{direction}"
     else
       result = kpi[SEVERITY_MAPPING[severity_level]]
     end
     return result
   end
  
  # Return the number of octets per delta_t for the the severity  level to be met
  def get_utilization_errored_period_threshold(direction, severity_level)        
    result = nil
    kpi = get_kpi(DIRECTION_MAPPING[direction], self.class.name, nil)
    if kpi == nil
      SW_ERR "No KPI found for #{self.class}:#{self.id} KPI #{DIRECTION_MAPPING[direction]} #{direction}"
    else
      threshold_in_percent = kpi[SEVERITY_MAPPING[severity_level]]
      octets_per_delta_t = (rate(direction) * kpi[:delta_t])*1000/8
      result = (octets_per_delta_t * threshold_in_percent)/100
    end
    return result
  end
    
  def get_utilization_errored_period_delta_time_secs
    # The delta_t is is stored per direction in KPI but in fact they have to be same value
    # so just pick one
    kpi = get_kpi("ingress_util", self.class.name, nil)
    if kpi == nil
      SW_ERR "No KPI found for #{self.class}:#{self.id}"
    else
      kpi = kpi[:delta_t]
    end
    return kpi
  end
    
  def get_utilization_errored_period_max_errored_count(direction, severity_level)    
    result = nil
    kpi = get_kpi(DIRECTION_MAPPING[direction], self.class.name, nil)
    if kpi == nil
      SW_ERR "No KPI found for #{self.class}:#{self.id} KPI #{DIRECTION_MAPPING[direction]} #{direction}"
    else
      count_in_seconds = kpi[SEVERITY_TO_COUNT_MAPPING[severity_level]]
      result = count_in_seconds/kpi[:delta_t]
    end
    return result
  end
  
  
end
