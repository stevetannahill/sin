# == Schema Information
# Schema version: 20110802190849
#
# Table name: ordered_entity_groups
#
#  id                       :integer(4)      not null, primary key
#  name                     :string(255)
#  order_type_id            :string(255)
#  created_at               :datetime
#  updated_at               :datetime
#  ethernet_service_type_id :integer(4)
#

class OrderedEntityGroup < TypeElement
  belongs_to :order_type
  belongs_to :ethernet_service_type
  has_many :ordered_entity_type_links, :dependent => :destroy
  # has_many_polymorphs :ordered_entity_types, :from => [:segment_types, :demarc_types], :through => :ordered_entity_type_links, :skip_duplicates => false
  has_many :component_orders
  has_many :instance_elements, :class_name => "ComponentOrder"
  has_many :member_attrs, :as => :affected_entity
  
  validates_presence_of :name, :order_type_id, :ethernet_service_type_id
  validates_uniqueness_of :name, :scope => :order_type_id
  
  def get_candidate_ordered_entity_types
    return ethernet_service_type ? ethernet_service_type.segment_types + ethernet_service_type.demarc_types.reject{|dt| dt.is_a? EnniType} : []
  end
  
  def segment_types
    return ordered_entity_types.select{|oet| oet.is_a? SegmentType}
  end
  
  def demarc_types
    return ordered_entity_types.select{|oet| oet.is_a?  DemarcType}
  end
  
  validate_inclusion_of_each :ordered_entity_types, :in => :get_candidate_ordered_entity_types

  def ordered_entity_type_count
    ordered_entity_types.length
  end
  
  def get_candidate_ethernet_service_types
    (order_type && order_type.operator_network_type) ? order_type.operator_network_type.ethernet_service_types : []
  end
  
  def ordered_entity_types
  	ordered_entity_type_links.map(&:ordered_entity_type)
  end
end
