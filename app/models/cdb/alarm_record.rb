# == Schema Information
# Schema version: 20110802190849
#
# Table name: event_records
#
#  id               :integer(4)      not null, primary key
#  state            :string(255)
#  details          :string(255)
#  event_history_id :integer(4)
#  created_at       :datetime
#  updated_at       :datetime
#  type             :string(255)
#  original_state   :string(255)
#  time_of_event    :integer(8)      not null
#

class AlarmRecord < EventRecord

  belongs_to :alarm_history, :foreign_key => :event_history_id 
  validates_each :state do |record, attr, value|      
    record.errors.add attr, "Invalid value #{value}" if !AlarmSeverity.serverities.include?(value.to_s)
  end  
  validates_uniqueness_of :time_of_event, :scope => :event_history_id, :if => Proc.new {|a| a.event_history_id != nil}

  # Find alarm records for a date_range
  scope :from_date, lambda { |date|
    where("time_of_event >= ?", date) if date
  }
  scope :to_date, lambda { |date|
    where("time_of_event <= ?", date) if date
  }
  
  # return the highest state
  def +(other)
    return self if other == nil
    return other if self.state < other.state
    return self if self.state > other.state
    # The states are the same so return the latest one
    return self if self.time_of_event > other.time_of_event
    return other
  end 

  # return the lowest state
  def &(other)
    return self if other == nil
    return other if self.state > other.state
    return self if self.state < other.state
    # The states are the same so return the latest one
    return self if self.time_of_event > other.time_of_event
    return other
  end 

  def self.highest(events) 
    events.compact!   
    events.max {|x,y| AlarmRecord.cmp(x, y)}
  end
  
  def self.lowest(events)
    events.compact!   
    events.min {|x,y| AlarmRecord.cmp(x,y)}
  end

  def self.select_lowest_alarm_or_warn(events, error_threshold = AlarmSeverity::UNAVAILABLE)
    events.compact!
    removed_error = events.find_all {|e| e.state < error_threshold}.sort
    selected = AlarmRecord.highest(removed_error)
    max = events.find_all{|e| e.state >= error_threshold}.sort.last
    
    # if any event is > error_threshold then change the state to the lowest state or Warning if the lowest state is OK
    if (max != nil)
      selected = max if selected == nil # All events were >= error_threshold
      # The new state will be "selected" or Warning if selected = OK
      new_state = selected.state
      orginal_state = selected.state
      details = selected.details
      if selected.state == AlarmSeverity::OK
        new_state = AlarmSeverity::WARNING
        orginial_state = 'warning'
        details = max.details
      end
      selected = AlarmRecord.new(:state => new_state, :time_of_event => selected.time_of_event, :original_state => orginial_state, :details => details)
    end
    return selected
  end
  
  def self.cmp(x,y)
    result = x.state <=> y.state
    result = x <=> y if result == 0
    return result
  end


  def state
    # Translate the state attribute (string) into a AlarmSeverity so can do
    # comparisions like > < etc.
    # not sure if this good - to prevent a new object being created everytime state is
    # accessed only create a AlarmSeverity if it's the first time or if it's changed
    @alarm = AlarmSeverity.new(read_attribute(:state)) if @alarm == nil || @alarm.state != read_attribute(:state)
    @alarm
  end
  
  # Normalize state name
  def state_css_classify
    state.to_s.downcase.gsub(' ', '_')
  end
  
  # Convert state from alarm type to a string for storage in db
  def state=(other)
    write_attribute(:state, other.to_s)
  end
    
  def to_s
    return "#{state.to_s}:#{details}"  #{Time.at(time_of_event/1000).strftime("%d/%m/%y %H:%M:%S") if state != AlarmSeverity::NO_STATE}
  end
end
