# == Schema Information
# Schema version: 20110802190849
#
# Table name: event_histories
#
#  id             :integer(4)      not null, primary key
#  event_filter   :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  alarm_mapping  :text
#  retention_time :integer(4)
#

# This is for Alarms that don't have network managers for syncing
class GenericHistory < AlarmHistory
  
  def self.default_alarm_mapping
    return {"cleared"     => AlarmSeverity::OK,
            "warning"     => AlarmSeverity::WARNING,
            "failed"      => AlarmSeverity::FAILED,
            "unavailable" => AlarmSeverity::UNAVAILABLE}
  end
         
  def default_mapping
    self.alarm_mapping = GenericHistory::default_alarm_mapping if alarm_mapping == nil 
  end
  
  def self.alarm_sync_all(nms = nil)
    return true
  end      

  def alarm_sync(timestamp = nil)
    return true
  end
  
  def alarm_sync_past(timestamp = nil)
    return true
  end
  
        
end
