# == Schema Information
#
# Table name: cos_test_vectors
#
#  id                              :integer(4)      not null, primary key
#  type                            :string(255)
#  test_type                       :string(255)
#  test_instance_class_id          :string(255)
#  notes                           :string(255)
#  cos_end_point_id                :integer(4)
#  service_level_guarantee_type_id :integer(4)
#  created_at                      :datetime
#  updated_at                      :datetime
#  delay_error_threshold           :string(255)
#  dv_error_threshold              :string(255)
#  delay_warning_threshold         :string(255)
#  dv_warning_threshold            :string(255)
#  service_instance_id             :string(255)
#  sla_id                          :string(255)
#  test_instance_id                :string(255)
#  flr_error_threshold             :string(255)
#  flr_warning_threshold           :string(255)
#  availability_guarantee          :string(255)
#  cenx_id                         :string(255)
#  flr_sla_guarantee               :string(255)
#  dv_sla_guarantee                :string(255)
#  delay_sla_guarantee             :string(255)
#  circuit_id                      :string(255)
#  ref_circuit_id                  :string(255)
#  event_record_id                 :integer(4)
#  sm_state                        :string(255)
#  sm_details                      :string(255)
#  sm_timestamp                    :integer(8)
#  last_hop_circuit_id             :string(255)
#  flr_reference                   :string(255)
#  delay_reference                 :string(255)
#  delay_min_reference             :string(255)
#  delay_max_reference             :string(255)
#  dv_reference                    :string(255)
#  dv_min_reference                :string(255)
#  dv_max_reference                :string(255)
#  loopback_address                :string(255)
#  circuit_id_format               :string(255)
#  grid_circuit_id                 :integer(4)
#  ref_grid_circuit_id             :integer(4)
#  protection_path_id              :integer(4)
#


class SpirentCosTestVector < CosTestVector
  before_save :set_protection_path_id
  before_save :set_ref_grid_circuit_id
  
  validates_uniqueness_of :circuit_id

  validates_uniqueness_of :grid_circuit_id, :allow_nil => true
  validate :ref_circuit_valid, :last_hop_circuit_valid

  def set_protection_path_id
    unless protection_path_id
      self.protection_path_id = primary ? 1 : 2
    end
    return true
  end
  
  def set_ref_grid_circuit_id
    # If there is a ref_circuit_id and it exists then set the ref_grid_curit_id to the ref CTV grid_id
    unless self.ref_circuit_id.nil?
      ref_ctv = CosTestVector.find_by_circuit_id(self.ref_circuit_id)
      if ref_ctv
        self.ref_grid_circuit_id = ref_ctv.grid_circuit_id        
      end
    end
    return true
  end
             
  def cenx_name
    return "#{super}[#{circuit_id}]"
  end

  def bx_sla_name
    return cos_end_point.segment_end_point.bx_sla_name
  end

  def test_target_address
    return target_address
  end

  def can_gen_config
    return false
  end

  def generate_config
    text = "TBD"
    return text
  end

  # TODO: Rename to sync_with_server
  def get_bx_vector_info
    return true, "", "", "", ""
  end

  # TODO: Rename to get_event_filter
  def bx_event_filter
    return nil
  end
  
  def paired_ctv
    paried_ctv = nil
    paried_indicator = primary ? "B" : "P" # Find the opposite circuit
    split_circuit_id = circuit_id.split("-")
    split_circuit_id[2] = paried_indicator
    split_circuit_id[3] = "%" # Ignore vlan id   
    backup_circuit_regex = split_circuit_id.join("-")
    backup_circuit_id = SpirentCosTestVector.find(:first, :conditions => ["circuit_id like ?", backup_circuit_regex])
    paried_ctv = backup_circuit_id
    return paried_ctv
  end
  
  def get_alarm_keys
    return true, ["#{circuit_id}-Availability"]
  end
  
  def go_in_service
    return_value = true 
    info = ""
    if cos_end_point.segment_end_point.is_monitored
      return_value, alarm_keys = get_alarm_keys
      if return_value
        new_alarms = {}
        alarm_keys.each {|filter| new_alarms[filter] = {:type => AvailabilityHistory, :mapping => AvailabilityHistory.default_avaiability_alarm_mapping}}
        return_value = update_alarm_history_set(new_alarms)
      end
    else
      # It's not monitored so delete any history
      alarm_histories.destroy_all
    end  
    return_code[:info] = "Failed to initialise" unless return_value
    return {:result => return_value, :info => info}
  end    
  
  # Return a Stats object which is used to hold/get Stats information
  # for the given time period
  def stats_data(start_time, end_time, current_time=Time.now, adjust_time=true)
    stats = SpirentStatsArchive.new(self, start_time, end_time, current_time, adjust_time)
    return stats
  end
  
  # TODO Temporary until there isa reliable way to get the network manager
  def get_monitoring_manager
    manager = NetworkManager.find_by_nm_type("eScout")
  end
  
  def ref_circuit_valid
    # if there is a ref_circuit_id then it must exist
    if !ref_circuit_id.blank?
      ref_ctv = CosTestVector.find_by_circuit_id(ref_circuit_id)
      if ref_ctv 
        if (!ref_grid_circuit_id.blank?) && ref_ctv.grid_circuit_id != ref_grid_circuit_id
          # The ref ctv exists and there is a ref_circuit_id but they don't match
          errors.add(:ref_grid_circuit_id, "does not match the grid id of the reference circuit id (#{ref_circuit_id})")
        else
          # Valid - The ref_grid_id will be filled in by the before_save
        end
      else
        errors.add(:ref_circuit_id, "does not exist (#{ref_circuit_id}")
      end
    end
  end

  def last_hop_circuit_valid
    if !last_hop_circuit_id.blank?
      last_hop_ctv = CosTestVector.find_by_circuit_id(last_hop_circuit_id)
      if last_hop_ctv 
        # TODO DaveS- Check with Serge if this is needed
        # if (!last_hop_grid_circuit_id.blank?) && last_hop_ctv.grid_circuit_id != last_hop_grid_circuit_id
        #   # The ref ctv exists and there is a ref_circuit_id but they don't match
        #   errors.add(:last_hop_grid_circuit_id, "does not match the grid id of the reference circuit id (#{last_hop_circuit_id})")
        # else
        #   # Valid - The ref_grid_id will be filled in by the before_save
        # end
      else
        errors.add(:last_hop_circuit_id, "does not exist (#{last_hop_circuit_id}")
      end
    end
  end
    
end
