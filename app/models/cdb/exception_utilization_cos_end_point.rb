# == Schema Information
#
# Table name: exception_utilization_cos_end_points
#
#  id                                     :integer(4)      not null, primary key
#  time_declared                          :integer(4)
#  value                                  :float
#  severity                               :string(0)
#  cos_end_point_id                       :integer(4)
#  direction                              :string(0)
#  time_over                              :integer(4)
#  average_rate_while_over_threshold_mbps :float
#
class ExceptionUtilizationCosEndPoint < ActiveRecord::Base
  include CommonException
  
  belongs_to :cos_end_point
  validates_presence_of  :cos_end_point_id
  validates :time_declared, :presence => true
  validates :value, :presence => true
  validates_inclusion_of :severity, :in => [ :major, :minor]
  validates_inclusion_of :direction, :in => [:ingress, :egress]

  validates :direction, :presence => true
  validates :time_over, :presence => true
  validates :average_rate_while_over_threshold_mbps, :presence => true


  def direction
    read_attribute(:direction).to_sym
  end
  def direction= (value)
    write_attribute(:direction, value.to_s)
  end


  def get_value_units_s
    case read_attribute(:severity).to_sym
    when :critical
      return 'n/a'
    when :major
      return 'seconds'
    when :minor
      return 'seconds'
    end
  end

  def severity
    read_attribute(:severity).to_sym
  end
  def severity= (value)
    write_attribute(:severity, value.to_s)
  end

  def entity_type
    'On Net OVC'
  end
  
  def get_errored_period_threshold_mbps
    CosEndPoint.find(cos_end_point_id).segment_end_point.get_utilization_threshold_mbps direction, severity
  end
  
  def get_errored_period_threshold_percent
    CosEndPoint.find(cos_end_point_id).segment_end_point.get_utilization_threshold_percent direction, severity
  end
  
  def get_errored_period_threshold
    CosEndPoint.find(cos_end_point_id).segment_end_point.get_utilization_errored_period_threshold direction, severity
  end

  #   returns delta_t which can be longer than the native period (59 secs or 5 mins)
  # if 0 is returned then the native time period shall be used.
  def get_errored_period_delta_time_secs
    CosEndPoint.find(cos_end_point_id).segment_end_point.get_utilization_errored_period_delta_time_secs
  end

  # Returns the maximum number of time periods allowed over the threshold without
  # declaring an exception.
  # has to be greater than 1
  def get_errored_period_max_errored_count
    CosEndPoint.find(cos_end_point_id).segment_end_point.get_utilization_errored_period_max_errored_count direction, severity
  end


  # Returns the maximum number of time  allowed over the threshold.
  def get_errored_period_max_secs
    get_errored_period_max_errored_count * get_errored_period_delta_time_secs
  end

end

