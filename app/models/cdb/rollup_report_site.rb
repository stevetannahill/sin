class RollupReportSite < ActiveRecord::Base
  include CommonException
  belongs_to :site

  def metric_percentage(count_total)
    "%0.02f" % ((count_total.to_f / self.metric_count.to_f) * 100.0)
  end

  def metric_ok_count
    self.metric_count - self.metric_minor_count - self.metric_major_count
  end

  def demarc_percentage(count_total)
    "%0.02f" % ((count_total.to_f / self.demarc_count.to_f) * 100.0)
  end

  def ok_demarc_count
    self.demarc_count - self.minor_demarc_count - self.major_demarc_count
  end

end
