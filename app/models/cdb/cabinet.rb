# == Schema Information
# Schema version: 20110802190849
#
# Table name: cabinets
#
#  id                   :integer(4)      not null, primary key
#  name                 :string(255)
#  created_at           :datetime
#  updated_at           :datetime
#  site_id :integer(4)
#

class Cabinet < ActiveRecord::Base
  belongs_to :site
  validates_presence_of :name
  validates_uniqueness_of :name, :scope => :site_id
  has_many :nodes, :dependent => :nullify

  def node_count
    return nodes.size
  end
  
end
