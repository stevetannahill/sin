
class ScheduledAudit < ScheduledTask
  POSSIBLE_AUDITS = {"Grid" => {:script => "GridAuditReport.generate_report(%SR_ID%)", :env => "oss-db", :attachements => {}},
                     "Builder" => {:script => "BuilderAuditReport.generate_report(%SR_ID%)", :env => "oss-db", :attachements => {"csv" => true}},
                    }
  
  validates_inclusion_of :user, :in => [nil]  
  validates_uniqueness_of :name, :scope => [:user_id]
  validates :report_type, :presence => true
  validates_inclusion_of :report_type, :in => POSSIBLE_AUDITS.keys
  before_save :set_executable_script  

  def list_of_audits
    POSSIBLE_AUDITS.keys
  end
  
  def supported_attachemts
    POSSIBLE_AUDITS[report_type][:attachements]
  end
  
  private
  
  def set_executable_script
    self.executable_script = POSSIBLE_AUDITS[report_type][:script]
    self.environment = POSSIBLE_AUDITS[report_type][:env]
  end
  
end
