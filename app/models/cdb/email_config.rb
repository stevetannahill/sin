class EmailConfig < ActiveRecord::Base
# This is a singleton but the acts_as_singleton gem does not support Rails 3.2 yet...
# So I just validate that you can't create more than one and just return the first one.
#include ActiveRecord::Singleton
   
  serialize :override_event_email_address
  serialize :override_order_email_address
  serialize :debug_bcc_event_email_addresses
  serialize :debug_bcc_order_email_addresses
  serialize :cenx_ops_request
  serialize :cenx_ops
  serialize :cenx_eng
  serialize :cenx_cust_ops
  serialize :cenx_prov
  serialize :cenx_billing
  
  validate :only_one_instance
  
  def self.instance
    ec = first || create
    if Rails.env == 'production'
      ActionMailer::Base.perform_deliveries = ec.enable_delivery
    else
      ActionMailer::Base.perform_deliveries = ec.development_enable_delivery
    end
    return ec
  end
  
  # Returns an array [to, cc, bcc] for the To, Cc, Bcc email addresses for order ticket
  def get_order_recipients(order_state)
    email_addresses = case
    when order_state.is_a?(OrderAccepted)
      [cenx_prov, "", cenx_eng + debug_bcc_order_email_addresses]
    when order_state.is_a?(OrderDesignComplete)
      [cenx_prov, "", cenx_ops_request + debug_bcc_order_email_addresses]
    when order_state.is_a?(OrderProvisioned)
      [cenx_prov, "", cenx_ops_request + debug_bcc_order_email_addresses]
    when order_state.is_a?(OrderTested)
      [cenx_prov, "", cenx_cust_ops + debug_bcc_order_email_addresses]              
    when order_state.is_a?(OrderCustomerAccepted)
      [cenx_prov, "", (cenx_ops_request + cenx_billing + debug_bcc_order_email_addresses)]
    else
      SW_ERR "Invalid order state #{order_state.to_s}. Cannot find correct email addresses"
      ["", "", ""]
    end
    
    if !override_order_email_address.empty?
      email_addresses = [override_order_email_address, "", ""]
    end
    
    # remove nested arrays of addresses, nils and empty strings
    email_addresses.collect {|email_address| email_address.is_a?(Array) ? email_address.flatten.compact.reject(&:blank?) : email_address}
  end      
  
  # Returns an array [to, cc, bcc] for the To Cc and Bcc email addresses for event emails
  def get_event_recepients(obj)
    email_addresses = [debug_bcc_event_email_addresses, "", event_recipient([cenx_ops])]
    # remove nested arrays of addresses, nils and empty strings
    email_addresses.collect {|email_address| email_address.is_a?(Array) ? email_address.flatten.compact.reject(&:blank?) : email_address}
  end
      
  def event_recipient(to)
    override_event_email_address.empty? ? to : override_event_email_address
  end
  
  def order_recipient(to)
    override_order_email_address.empty? ? to : override_order_email_address
  end
  
  def self.send_test_email(email_address) 
    reply = "" 
    # Test Normal Rails email config
    begin
      CdbMailer.test_email(email_address).deliver
      reply = "Sending email from platform #{RUBY_PLATFORM} - Ok\n"
    rescue 
      err = "Exception sending test email Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}"
      Rails.logger.error err; SW_ERR err
      reply << "Failed Sending email from platform #{RUBY_PLATFORM} see SW_ERRs\n"
    end
    
    # Test emails from JMS server JRuby server
    server = "localhost"
    port = SamIfCommon::SAM_JMS_SERVER_PORT
    command = "e #{email_address}"
    begin
      socket = nil
      socket = TCPSocket.new(server, port)
      request = Marshal.dump(SamIfCommon::SamIfMsg.new(nil,command.chomp(" "), {}, {}))
      serialized_size = sprintf "%08d", request.size
      socket.write serialized_size
      socket.write(request)
      str = socket.recv(5000)
      reply << "Sending email from platform JRUBY - #{str}\n"   
    rescue
      reply << "Sending email to platform JRUBY failed sam_jms_server may not be running\n"
    end
    socket.close if socket != nil
    reply
  end
  
  private
  
  def only_one_instance
  # Only allow one instance of EmailConfig
   if EmailConfig.count > 1
     errors.add(:base, "Only one instance is allowed")
   end
  end
  
end
# == Schema Information
#
# Table name: email_configs
#
#  id                              :integer(4)      not null, primary key
#  override_event_email_address    :string(1024)    default("--- []\n\n"), not null
#  override_order_email_address    :string(1024)    default("--- []\n\n"), not null
#  debug_bcc_event_email_addresses :string(1024)    default("--- []\n\n"), not null
#  debug_bcc_order_email_addresses :string(1024)    default("--- []\n\n"), not null
#  cenx_ops_request                :string(1024)    default("--- \n- ops-request@support.cenx.com\n"), not null
#  cenx_ops                        :string(1024)    default("--- \n- support@cenx.com\n"), not null
#  cenx_eng                        :string(1024)    default("--- \n- eng@support.cenx.com\n"), not null
#  cenx_cust_ops                   :string(1024)    default("--- \n- customerops@support.cenx.com\n"), not null
#  cenx_prov                       :string(1024)    default("--- \n- Provisioning@CENX.com\n"), not null
#  cenx_billing                    :string(1024)    default("--- \n- \"\"\n"), not null
#  enable_delivery                 :boolean(1)      default(FALSE), not null
#  development_enable_delivery     :boolean(1)      default(FALSE), not null
#

