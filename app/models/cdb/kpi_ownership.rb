# == Schema Information
#
# Table name: kpi_ownerships
#
#  id           :integer(4)      not null, primary key
#  kpiable_id   :integer(4)
#  kpiable_type :string(255)
#  kpi_id       :integer(4)
#  parent_id    :integer(4)
#  parent_type  :string(255)
#

class KpiOwnership < ActiveRecord::Base
belongs_to :kpiable, :polymorphic => true
belongs_to :kpi
belongs_to :parent, :polymorphic => true

validates_presence_of :kpiable
validates_presence_of :kpi
validate :uniqueness_of_kpi

after_create :general_after_create
after_save :general_after_save
after_destroy :general_after_destroy

  def uniqueness_of_kpi
    if !kpi.actual_kpi
      if kpiable.kpis.find_by_actual_kpi_and_name_and_for_klass(false, kpi.name, kpi.for_klass) != nil
        errors.add(:base, "Kpi mask of #{kpi.name}, #{kpi.for_klass} is not unique for the object #{kpiable.class}:#{kpiable.id}")
      end
    else
      kpis = Kpi.joins(:kpi_ownerships).where("`kpi_ownerships`.`kpiable_id` = ? AND `kpi_ownerships`.`kpiable_type` = ? AND `kpi_ownerships`.`parent_type` = ? AND `kpi_ownerships`.`parent_id` = ? AND `kpis`.`name` = ? AND `kpis`.`for_klass` = ? AND `kpis`.`actual_kpi` = 1",
                                  kpiable.id, kpiable.class.base_class.name, parent.class.base_class.name, parent.id, kpi.name, kpi.for_klass)      
      if kpis == nil || kpis.size > 1
        # There is either none or one kpi - if there is more then fail.
        errors.add(:base, "Actual Kpi for #{kpiable.class}:#{kpiable.id} parent #{parent.class}:#{parent.id} KPI #{kpi.name}, #{kpi.for_klass} is not unique")
      end
    end
  end
        

  def general_after_save
    if kpiable != nil && kpi.actual_kpi == false
      kpiable.update_kpi(kpiable, nil, kpi.name, kpi.for_klass) 
    end
  end
  
  def general_after_create
    if kpiable != nil && kpi.actual_kpi == false
      # Seems odd but if a mask has been added get rid of all the old pointers to the parent actual KPIs. On the save the
      # new actual KPIs will be created
      kpiable.kpi_ownerships.joins(:kpi).where(:kpis => {:name => kpi.name, :for_klass => kpi.for_klass, :actual_kpi => true}).destroy_all
    end
  end
  
  def general_after_destroy
    if kpiable != nil && kpi.actual_kpi == false
      kpiable.kpis.where(:name => kpi.name, :for_klass => kpi.for_klass, :actual_kpi => true).destroy_all
      kpiable.update_kpi(kpiable, nil, kpi.name, kpi.for_klass)      
    end
  end    

end
