# == Schema Information
# Schema version: 20110802190849
#
# Table name: network_management_servers
#
#  id                 :integer(4)      not null, primary key
#  name               :string(255)
#  network_manager_id :integer(4)
#  primary_ip         :string(255)
#  created_at         :datetime
#  updated_at         :datetime
#

class NetworkManagementServer < ActiveRecord::Base
  belongs_to :network_manager
  
  validates_presence_of :name, :primary_ip, :network_manager_id
  validates_uniqueness_of :name, :scope => :network_manager_id

  validate :validate_primary_ip
  
  def nm_info
    network_manager.name
  end
  
  protected
  
  def validate_primary_ip
    # it must be a IP or a DNS name
    ip_regex = /^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$/
    if !primary_ip[ip_regex]
      if !valid_hostname?(primary_ip)
        errors.add(:primary_ip, "must be an IP or a valid DNS name")
      end
    end
  end
  
  def valid_hostname?(hostname)
    return false if hostname.length > 255 or hostname.scan('..').any?
    hostname = hostname[0 ... -1] if hostname.index('.', -1)
    return hostname.split('.').collect { |i|
      i.size <= 63 and not (i.rindex('-', 0) or i.index('-', -1) or i.scan(/[^a-z\d-]/i).any?)
    }.all?
  end
end
