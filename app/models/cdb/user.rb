# == Schema Information
#
# Table name: users
#
#  id                  :integer(4)      not null, primary key
#  email               :string(255)     not null
#  first_name          :string(255)
#  last_name           :string(255)
#  crypted_password    :string(40)
#  salt                :string(40)
#  remember_token      :string(255)
#  phone               :string(255)
#  service_provider_id :integer(4)
#  role_id        :integer(4)
#  created_at          :datetime        not null
#  updated_at          :datetime        not null
#  settings            :text
#

require 'digest/sha1'
class User < ActiveRecord::Base    
  
  belongs_to :service_provider
  belongs_to :role
  belongs_to :contact
  has_and_belongs_to_many :operator_network_types
  has_many :scheduled_tasks, :dependent => :destroy
  has_many :filter_templates, :dependent => :destroy
  has_many :site_lists, :dependent => :destroy
  delegate :privileges, :to => :role
  store :settings, accessors: [ :homepage, :hidden_tables, :default_mode, :time_zone ]

  default_scope :include => [:service_provider, :role]
  accepts_nested_attributes_for :operator_network_types
  
  before_validation :generate_salt
  validates_presence_of :email, :service_provider, :role
  validates_presence_of :password, :on => :create  
  validates_presence_of :operator_network_types, :unless => Proc.new { |user| user.system_admin? }
  validates_confirmation_of :password
  validates_length_of :password, :within => 6..20, :allow_blank => false, :if => "password.present?"
  validates :password, format: {with: /(?=^.{6,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, message: "must contain at least 1 lowercase letter, 1 uppercase letter, 1 digit or special character"}, :if => "password.present?"
  validates_uniqueness_of :email          
  validates :email, format: {with: /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/ }
  
  attr_accessor :password, :password_confirmation, :current_password
  # This is a list of the attributes a user is allowed to change through a mass update.
  # Never add something like service_provider_id or salt here
  attr_accessible :first_name, :last_name, :email, :homepage, :default_mode, :service_provider_id, :role_id, :operator_network_type_ids, :password, :password_confirmation, :current_password, :time_zone
  after_initialize :initialize_settings
  before_save  :encrypt_password
  before_destroy :check_system_admin
  
  def general_after_initialize
    return unless new_record?
    self.service_provider = @current_user.service_provider unless @current_user.nil?
  end
  

  def display_name
    "#{first_name} #{last_name}" rescue "User"
  end
  
  def self.authenticate(email, password)
    # not used at the moment but will be for password change
    u = find_by_email(email) # need to get the salt
    u = u && u.authenticated?(password) ? u : nil
  end
    
  def self.encrypt(password, salt)
    Digest::SHA1.hexdigest("--#{salt}--#{password}--")
  end
  
  def self.sodium(email)
    Digest::SHA1.hexdigest("--#{Time.now.to_s}--#{email}--")
  end

  def authenticated?(password)
    crypted_password == self.class.encrypt(password,salt)
  end
  
  def quick_set!(service_provider_id, email, password, *role_ids)
    self.service_provider_id = service_provider_id
    self.email = email
    self.password = password
    self.save!
  end
  
  def system_admin?
    service_provider && service_provider.is_system_owner? && operator_network_types.empty?
  end
  alias_method :is_admin?, :system_admin?
  
  def is_sprint?
    service_provider && service_proivder.is_sprint?
  end

  def has_role?(role_sym)
    roles.any? { |r| r.name.downcase.underscore.to_sym == role_sym }
  end
  
  def homepage_url(request)
    "#{request.protocol}#{request.host_with_port}#{self.homepage}"
  end

  def initialize_settings
    self.homepage      ||= '/'
    self.hidden_tables ||= []
    self.default_mode  ||= 'inventory'
    self.time_zone     ||= 'UTC'
  end

  def send_password_reset
    generate_token(:password_reset_token)
    self.password_reset_sent_at = Time.zone.now
    save!
    CdbMailer.password_reset(self).deliver
  end

  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end

  protected
  
  def generate_salt
    self.salt = self.class.sodium(email) if new_record? and salt.blank?
  end
  
  def encrypt_password
    self.crypted_password = self.class.encrypt(password,salt) unless password.blank?
  end

  private
  
  def check_system_admin
    if self.system_admin?
      errors.add(:base, "Cannot remove a system administrator")
      return false
    end
  end
  
end
