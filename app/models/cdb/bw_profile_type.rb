# == Schema Information
# Schema version: 20110802190849
#
# Table name: bw_profile_types
#
#  id                       :integer(4)      not null, primary key
#  name                     :string(255)
#  bwp_type                 :string(255)
#  class_of_service_type_id :integer(4)
#  cir_range_notes          :string(255)
#  cbs_range_notes          :string(255)
#  eir_range_notes          :string(255)
#  ebs_range_notes          :string(255)
#  color_mode               :string(255)
#  coupling_flag            :string(255)
#  notes                    :text
#  created_at               :datetime
#  updated_at               :datetime
#  rate_limiting_performed  :boolean(1)
#  rate_limiting_mechanism  :string(255)
#  rate_limiting_layer      :string(255)
#  rate_limiting_notes      :string(255)
#

class BwProfileType < TypeElement
  belongs_to :class_of_service_type

  validates_presence_of :bwp_type, :class_of_service_type_id
  validates_uniqueness_of :bwp_type , :scope => :class_of_service_type_id 
  validates_inclusion_of :bwp_type, :in => FrameTypes::LOCALE_DIRECTION.map{|value| value }, 
                         :message => "invalid, Use: #{FrameTypes::LOCALE_DIRECTION.map{|value| value }.join(", ")}"
#  validates_inclusion_of :color_mode, :in => ServiceCategories::COLOR_MODE.map{|value| value },
#                         :message => "invalid, Use: #{ServiceCategories::COLOR_MODE.map{|value| value }.join(", ")}"
#  validates_inclusion_of :coupling_flag, :in => ServiceCategories::COUPLING_FLAG.map{|value| value },
#                         :message => "invalid, Use: #{ServiceCategories::COUPLING_FLAG.map{|value| value }.join(", ")}"
  validates_inclusion_of :rate_limiting_mechanism, :in => ServiceCategories::RATE_LIMITING_MECHANISM.map{|value| value },
                         :message => "invalid, Use: #{ServiceCategories::RATE_LIMITING_MECHANISM.map{|value| value }.join(", ")}"
  validates_inclusion_of :rate_limiting_layer, :in => ServiceCategories::LAYERS.map{|value| value },
                         :message => "invalid, Use: #{ServiceCategories::LAYERS.map{|value| value }.join(", ")}", :allow_nil => true, :allow_blank => true
  def name
    return bwp_type
  end
  
end
