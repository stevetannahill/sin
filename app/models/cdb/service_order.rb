# == Schema Information
#
# Table name: service_orders
#
#  id                          :integer(4)      not null, primary key
#  type                        :string(255)
#  title                       :string(255)
#  cenx_id                     :string(255)
#  action                      :string(255)
#  primary_contact_id          :integer(4)
#  testing_contact_id          :integer(4)
#  requested_service_date      :date
#  order_received_date         :date
#  order_acceptance_date       :date
#  order_completion_date       :date
#  customer_acceptance_date    :date
#  billing_start_date          :date
#  operator_network_id         :integer(4)
#  expedite                    :boolean(1)
#  status                      :string(255)
#  notes                       :text
#  ordered_entity_id           :integer(4)
#  ordered_entity_type         :string(255)
#  created_at                  :datetime
#  updated_at                  :datetime
#  ordered_entity_subtype      :string(255)
#  path_id                     :integer(4)
#  ordered_entity_group_id     :integer(4)
#  ordered_operator_network_id :integer(4)
#  order_state                 :string(255)
#  ordered_object_type_id      :integer(4)
#  ordered_object_type_type    :string(255)
#  ordered_entity_snapshot     :text
#  technical_contact_id        :integer(4)
#  local_contact_id            :integer(4)
#  foc_date                    :date
#  bulk_order_type             :string(255)
#  design_complete_date        :date
#  order_created_date          :date
#  order_name                  :string(255)
#  order_notes                 :text
#  order_timestamp             :integer(8)
#


class ServiceOrder < InstanceElement
  include Stateable

  belongs_to :operator_network
  belongs_to :ordered_operator_network, :class_name => "OperatorNetwork"
  belongs_to :ordered_entity, :polymorphic => true
  has_many :uploaded_files, :as => :uploader, :dependent => :destroy
  has_many :order_annotations, :dependent => :destroy
  
  validates_uniqueness_of :title 
  validates_numericality_of :technical_contact_id , :only_integer => true, :message => 'is not selected', :allow_nil => true
  validates_presence_of :title, :action, :primary_contact_id, :operator_network_id, :requested_service_date, :testing_contact
  validates_inclusion_of :action, :in => ServiceProviderTypes::ORDER_TYPES.map{|disp, value| value },
                         :message => "invalid, Use: #{ServiceProviderTypes::ORDER_TYPES.map{|disp, value| value }.join(", ")}"

  validates_as_cenx_id :cenx_id
  validate :validate_no_active_service_order_exists, :if => :ordered_entity_id_changed?
  validate :valid_contacts
  after_save :update_order_state, :if => :ordered_entity_id_changed?

  attr_protected :notes
  attr_accessible :notes, :action, :title, :expedite, :requested_service_date, :order_created_date, :order_received_date, :order_acceptance_date, :design_complete_date, :order_completion_date, :customer_acceptance_date, :billing_start_date, :foc_date, :operator_network_id, :path_id, :primary_contact_id, :testing_contact_id, :technical_contact_id, :local_contact_id, :ordered_operator_network_id, :ordered_entity_group_id, :ordered_object_type_id
  

	#Active Record Callbacks
  before_validation(:on => :create) do
    self.cenx_id = IdTools::CenxIdGenerator.generate_cenx_id(self.class)
    self.ordered_entity_snapshot = ordered_entity.attr_snapshot if ordered_entity
  end

  def update_order_state
    # A service order has been linked to an entity - Update the service order state
    # by having the ordered entity change it's state to itself.    
    # Note there are 2 way to associate a service order with an entity
    # 1) The service_order.order_entity is changed to an existing entity
    # 2) The entity.service_orders << so where the entity has NOT been saved yet
    # in the latter case the after_create of the entity has not been called yet so the prov state is not created yet
    # however when it is called it will update the order state
    if ordered_entity != nil && ordered_entity.prov_state != nil
      ordered_entity.set_prov_state(ordered_entity.get_prov_state)
    end
  end
  
  def validate_no_active_service_order_exists
    if ordered_entity != nil && ordered_entity.get_latest_order != nil
      if ordered_entity.active_order?
        errors.add(:ordered_entity, "has an existing active service order")
      end
    end
  end
  
  def name
    return title
  end
  
  def service_provider
    operator_network ? operator_network.service_provider : nil
  end
  
  def ordered_service_provider
    ordered_operator_network ? ordered_operator_network.service_provider : nil
  end
 
  def new_member_attr_instances ma, mais = []
    return [] unless !mais.collect{|mai| mai.member_attr}.include?(ma) || ma.is_a?(MemberHandle)
    new_mais = []
    if ma[:type] == nil
      new_mais.push(MemberAttrInstance.new())
    elsif ma[:type] == 'MemberHandle'
      (member_handle_owners - mais.select{|mai| mai.member_attr == ma}.collect{|mai| mai.owner}).compact.each{|mao|
        m = MemberHandleInstance.new()
        m.owner = mao
        new_mais.push(m)
      }
    end
    new_mais.each{|mai|
      mai.member_attr_id = ma.id
      mai.affected_entity = self
      mai.affected_entity_id = (self.id && self.id > 0) ? self.id : self.cache_key
      mai.affected_entity_type = self.class.table_name.classify
      mai.value = ordered_entity.get_latest_order.member_attr_instance(mai.name).value if ordered_entity && ordered_entity.get_latest_order
      clear = mai.errors.empty?
      member_attr_instances.push(mai)
      mai.errors.clear if clear
    }
    return new_mais
  end


  def get_candidate_ordered_object_types
    return []
  end
  
  def sp_info_so
    service_provider.name
  end
  
  def osp_info_so
    ordered_service_provider.name
  end
  
  def on_info_so
    operator_network.name
  end
  
  def oon_info_so
    ordered_operator_network.name
  end

  def ordered_entity_info
    if ordered_entity_id == nil
      return "N/A"
    else
      return "#{ordered_entity.cenx_name}"
    end
  end

  def primary_contact
    Contact.find(primary_contact_id) rescue nil
  end

  def testing_contact
    Contact.find(testing_contact_id) rescue nil
  end
  
  def local_contact
    Contact.find(local_contact_id) rescue nil
  end

  def add_note(note, user, datetime)
    self.notes = '' if self.notes.nil?
    order_note = "<div class='comment'>#{note}<div class='comment_author'><span class='comment_meta'>Posted by</span> #{user.display_name} (#{user.email}) <span class='comment_meta'>on</span> #{datetime.strftime('%Y-%m-%d %H:%M')}</div></div>"
    self.notes += order_note
    order_note  
  end
  
  def valid_contacts
    # HACK: POE failing due to added contact requirements
    return
    if primary_contact and primary_contact.service_provider != service_provider
      errors.add :primary_contact, "cannot be added with ID #{primary_contact_id} as it is not associated with the service order's service provider #{service_provider.name}"
    end

    if testing_contact and testing_contact.service_provider != service_provider
      errors.add :testing_contact, "cannot be added with ID #{testing_contact_id} as it is not associated with the service order's service provider #{service_provider.name}"
    end

    if local_contact and local_contact.service_provider != service_provider
      errors.add :local_contact, "cannot be added with ID #{local_contact_id} as it is not associated with the service order's service provider #{service_provider.name}"
    end
  end

end

