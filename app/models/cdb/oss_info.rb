# == Schema Information
# Schema version: 20110802190849
#
# Table name: oss_infos
#
#  id                       :integer(4)      not null, primary key
#  name                     :string(255)
#  completion_date          :date
#  operator_network_type_id :integer(4)
#  cenx_contact_id          :integer(4)
#  member_contact_id        :integer(4)
#  reference_documents      :string(255)
#  notes                    :string(255)
#  created_at               :datetime
#  updated_at               :datetime
#

class OssInfo < ActiveRecord::Base
  belongs_to :operator_network_type
  has_many :uploaded_files, :as => :uploader

  validates_presence_of :name, :operator_network_type_id
  validates_uniqueness_of :name, :scope => :operator_network_type_id

  def sp_info_oss
    return operator_network_type.service_provider.name
  end

  def ont_info_oss
    return operator_network_type.name
  end

  def cenx_contact_info
    if (cenx_contact_id)
      c = Contact.find(cenx_contact_id)
      if c != nil
        return c.name
      end
    end
    return "unknown"
  end

  def member_contact_info
    if (member_contact_id)
      c = Contact.find(member_contact_id)
      if c != nil
        return c.name
      end
    end
    return "unknown"
  end

  def member
    return operator_network_type.nil? ? nil : operator_network_type.service_provider
  end

  def member_name
    return member.name unless member.nil?
  end
  
  def ests_count
    return operator_network_type.nil? ? "Unknown" : operator_network_type.est_count
  end
  
  def coss_count
    coss = operator_network_type.nil? ? [] : operator_network_type.class_of_service_types
    return coss.nil? ? "Unknown" : coss.size
  end

  def demarc_types
    return operator_network_type.nil? ? [] : operator_network_type.demarc_types
  end
  
  def demarc_types_count
    return demarc_types.nil? ? "Unknown" : demarc_types.size
  end
  
  def is_sm_supported
    results = demarc_types.collect do |dt|
      dt.reflection_mechanisms.nil? or dt.reflection_mechanisms.empty?
    end
    return results.include?(false) ? "Yes" : "No"
  end
end
