# == Schema Information
# Schema version: 20110802190849
#
# Table name: event_histories
#
#  id             :integer(4)      not null, primary key
#  event_filter   :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  alarm_mapping  :text
#  retention_time :integer(4)
#


class SystemAlarmHistory < AlarmHistory
  
  def self.update_monit(monit_file, state, details)
    if state == AlarmSeverity::OK
      File.open(monit_file,  File::RDWR|File::CREAT) do |f|
        f.flock(File::LOCK_EX)
        f.truncate(0)
      end
    else
      File.open(monit_file, "w+") do |f| 
        f.flock(File::LOCK_EX)
        f.write(details)
      end
    end
  end
  
  def self.default_system_alarm_mapping
    return {"cleared"     => AlarmSeverity::OK,
            "warning"     => AlarmSeverity::WARNING,
            "failed"      => AlarmSeverity::FAILED,
            "unavailable" => AlarmSeverity::UNAVAILABLE}
  end
  
  def default_mapping
    self.alarm_mapping = SystemAlarmHistory::default_system_alarm_mapping if alarm_mapping == nil 
  end
  
  def self.alarm_sync_all(nms = nil)
    return true
  end      
  
  def alarm_sync(timestamp = nil)
    add_event(Time.now.to_f*1000, "cleared", "")
    return true
  end
  
  
  def add_event(event_time, state, details)
    # don't add the event if it's the same state
    cenx_state = alarm_mapping[state]
    if alarm_state.state != cenx_state
      super
    else
      #  Always evaluate the alarms if the state has not changed so the monit file will always refect the truth.
      event_ownerships.each {|eo| eo.eventable.eval_alarms}
    end
  end
  
end
