# == Schema Information
#
# Table name: scheduled_tasks
#
#  id                :integer(4)      not null, primary key
#  name              :string(255)     not null
#  user_id           :integer(4)
#  filter_options    :text
#  executable_script :string(255)
#  created_at        :datetime        not null
#  updated_at        :datetime        not null
#  report_type       :string(255)
#  type              :string(255)
#
class FilterTemplate < ScheduledTask
  store :filter_options
  
  validates :user, :presence => true  
  validates :report_type, :presence => true
  validates_inclusion_of :report_type, :in => ['sla', 'network_type', 'circuit_utilization', 'enni_utilization', 'site_group', 'satellite_site', 'cell_site', 'cell_site_path', 'metrics', 'inventory_exception']
  validates_uniqueness_of :name, :scope => [:user_id, :report_type, :is_public]
  after_save :update_reports, :if => :filter_options_changed?
  before_save :set_executable_script
    
  def generate_report(schedule_id)
    eval(self.executable_script_path(schedule_id))
  end
  
  def update_reports
    reports = schedules.collect {|sr| sr.reports}.flatten
    reports.each {|rep| rep.update_report}
    true
  end
  
  def display_name
    "#{self.name}"
  end
  
  def display_report_type
    if self.report_type == 'sla'
      'SLA'
    elsif self.report_type == 'enni_utilization'
      "ENNI Utilization"
    elsif self.report_type == 'network_type'
      'Ad Hoc'
    else
      self.report_type.titleize
    end
  end
  
  def executable_script_path(schedule_id)
    self.executable_script.gsub(/%SR_ID%/,schedule_id)
  end
  

  def self.user_filters(user, report_type=nil)
    user_filters = user.filter_templates.where(:is_public => false).
                                         order('name')

    if report_type
      user_filters = user_filters.where(:report_type => report_type)
    end
    
    user_filters
  end

  def self.public_filters(user, report_type=nil)
    public_filters = self.where(:is_public => true).
                          order('name')

    if report_type
      public_filters = public_filters.where(:report_type => report_type)
    end

    unless user.system_admin?
      public_filters = public_filters.joins(:user).where('users.service_provider_id = ?', user.service_provider_id)
    end

    public_filters
  end

  def self.all_filters(user, report_type=nil)
    self.user_filters(user, report_type) + self.public_filters(user, report_type)
  end

  private
  
  def set_executable_script
    self.environment = "sin" # Filter templates always run in sin
    case self.report_type
      when 'sla'
        self.executable_script = 'SlaReport.generate_report(%SR_ID%)'
      when 'network_type'
        self.executable_script = 'NetworkTypeReport.generate_report(%SR_ID%)'        
      when 'circuit_utilization'
        self.executable_script = 'CircuitUtilizationReport.generate_report(%SR_ID%)'        
      when 'enni_utilization'
        self.executable_script = 'EnniUtilizationReport.generate_report(%SR_ID%)'        
      when 'site_group'
        self.executable_script = 'SiteGroupReport.generate_report(%SR_ID%)'
      when 'satellite_site'
        self.executable_script = 'SatelliteSiteReport.generate_report(%SR_ID%)'
      when 'cell_site'
        self.executable_script = 'CellSiteReport.generate_report(%SR_ID%)'
      when 'cell_site_path'
        self.executable_script = 'CellSitePathReport.generate_report(%SR_ID%)'
      when 'metrics'
        self.executable_script = 'MetricsReport.generate_report(%SR_ID%)'
      when 'inventory_exception'
        self.executable_script = 'FalloutException.generate_report(%SR_ID%)'
    end
  end
  
end
