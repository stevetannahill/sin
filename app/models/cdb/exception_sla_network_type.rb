# == Schema Information
#
# Table name: exception_sla_network_types
#
#  id                       :integer(4)      not null, primary key
#  time_declared            :integer(4)
#  severity                 :string(0)
#  network_type             :string(255)
#  flr                      :float
#  fd                       :float
#  fdv                      :float
#  operator_network_type_id :integer(4)
#
class ExceptionSlaNetworkType < ActiveRecord::Base
  include CommonException

  belongs_to :operator_network_type
  validates_presence_of  :operator_network_type_id
  validates :time_declared, :presence => true
  validates :flr, :presence => true
  validates :fd, :presence => true
  validates :fdv, :presence => true
  validates_inclusion_of :severity, :in => [:critical, :major, :minor, :clear]


  def severity
    read_attribute(:severity).to_sym
  end
  def severity= (value)
    write_attribute(:severity, value.to_s)
  end

  def self.to_internal_metric_type sla_metric
    case sla_metric
    when :availability
      return :availability
    when :flr
      return :frame_loss_ratio
    when :fd
      return :delay
    when :fdv
      return :delay_variation
    end
  end

  # pass in one of [:flr, :fd, :fdv]
  def get_value_units_s sla_type
    # all severities now return the converted metric units, instead of major and minor always being seconds
    Stats::METRIC_UNITS[ExceptionSlaNetworkType.to_internal_metric_type(sla_type)]
  end
  
end

