# == Schema Information
# Schema version: 20110802190849
#
# Table name: patch_panels
#
#  id                   :integer(4)      not null, primary key
#  name                 :string(255)
#  site_id :integer(4)
#  created_at           :datetime
#  updated_at           :datetime
#  floor                :string(255)
#

class PatchPanel < ActiveRecord::Base
  belongs_to :site
  validates_presence_of :name, :site_id
end
