# == Schema Information
#
# Table name: cos_test_vectors
#
#  id                              :integer(4)      not null, primary key
#  type                            :string(255)
#  test_type                       :string(255)
#  test_instance_class_id          :string(255)
#  notes                           :string(255)
#  cos_end_point_id                :integer(4)
#  service_level_guarantee_type_id :integer(4)
#  created_at                      :datetime
#  updated_at                      :datetime
#  delay_error_threshold           :string(255)
#  dv_error_threshold              :string(255)
#  delay_warning_threshold         :string(255)
#  dv_warning_threshold            :string(255)
#  service_instance_id             :string(255)
#  sla_id                          :string(255)
#  test_instance_id                :string(255)
#  flr_error_threshold             :string(255)
#  flr_warning_threshold           :string(255)
#  availability_guarantee          :string(255)
#  cenx_id                         :string(255)
#  flr_sla_guarantee               :string(255)
#  dv_sla_guarantee                :string(255)
#  delay_sla_guarantee             :string(255)
#  circuit_id                      :string(255)
#  ref_circuit_id                  :string(255)
#  event_record_id                 :integer(4)
#  sm_state                        :string(255)
#  sm_details                      :string(255)
#  sm_timestamp                    :integer(8)
#  last_hop_circuit_id             :string(255)
#  flr_reference                   :string(255)
#  delay_reference                 :string(255)
#  delay_min_reference             :string(255)
#  delay_max_reference             :string(255)
#  dv_reference                    :string(255)
#  dv_min_reference                :string(255)
#  dv_max_reference                :string(255)
#  loopback_address                :string(255)
#  circuit_id_format               :string(255)
#  grid_circuit_id                 :integer(4)
#  ref_grid_circuit_id             :integer(4)
#  protection_path_id              :integer(4)
#

class BxCosTestVector < CosTestVector
  
  def bx_sla_name
    return cos_end_point.segment_end_point.bx_sla_name
  end

  def test_target_address
    target_outer_tag_vid = ""
    target_outer_tag_pcp = ""

    result, result_str, cenx_monitoring_cos_end_point = get_cenx_monitoring_cos_end_point
    if !result || !cenx_monitoring_cos_end_point
      SW_ERR "#{result_str}"
      target_outer_tag_vid = result_str
      target_outer_tag_pcp = result_str
    else
      target_outer_tag_vid = cenx_monitoring_cos_end_point.segment_end_point.stags
      target_outer_tag_pcp = cenx_monitoring_cos_end_point.ingress_mapping
    end

    if test_type == "Ethernet Frame Delay Test"

      if cos_end_point.segment_end_point.segment.is_in_ls_core_transport_path
        return "#{target_address}/#{target_outer_tag_vid}:#{target_outer_tag_pcp}/#{target_meg_level}"
      else
        target_inner_vid_pcp = ''
        if target_inner_tag_vid_is_integer
          target_inner_vid_pcp = "#{target_inner_tag_vid}:#{target_inner_tag_pcp}"
          return "#{target_address}/#{target_outer_tag_vid}:#{target_outer_tag_pcp},#{target_inner_vid_pcp}/#{target_meg_level}"
        end
          
        return "#{target_address}/#{target_outer_tag_vid}:#{target_outer_tag_pcp}/#{target_meg_level}"

      end

    elsif test_type == "Ping Active Test"
      return "#{target_address.split("/")[0]}/#{target_outer_tag_vid}/#{target_outer_tag_pcp}"
    elsif test_type == "Ethernet Loopback Test"
      return "#{target_address}/#{target_outer_tag_vid}:#{target_outer_tag_pcp}/#{target_meg_level}"
    else
      return "Target Formatting for #{test_type} is TBD"
    end
  end
  
  def name
    bx_sla_name
  end

  def can_gen_config
    result, result_str, cenx_monitoring_cos_end_point = get_cenx_monitoring_cos_end_point
    return (cenx_monitoring_cos_end_point != nil)
  end

  def generate_config
    text = ""
    text += "# Add Service:\n"
    if (test_type == 'Ethernet Frame Delay Test')
      if cos_end_point.segment_end_point.segment.is_in_ls_core_transport_path
        text += "DMM/DMR [60 sec test - 3pps] QinQ\n"
      else
        text += "DMM/DMR [60 sec test] QinQ\n"
      end
    elsif (test_type == 'Ethernet Loopback Test')
      text += "LBM/LBR [60 sec test] QinQ\n"
    elsif (test_type == 'Ping Active Test')
      text += "IP_Ping [60 sec test] dot1Q\n"
    else
      SW_ERR "ERROR: Unknown Test Type: #{test_type}\n"
      text += "ERROR: Unknown Test Type: #{test_type}\n"
    end
    text += "- Click Add\n"

    text += "#------\n# Identifier:\n#{cos_name}\n"

    if (test_type == 'Ethernet Frame Delay Test' || test_type == 'Ethernet Loopback Test')
#      text += "#------\n# S-Tag Ethertype:\n802.1Q(0x8100)\n"
      text += "#------\n# Target MEP Address:\n#{test_target_address}\n- Click Add\n"

      if test_type == 'Ethernet Frame Delay Test'
        text += "#------\n# Average Network Round-Trip Latency threshold:\n"
      else
        text += "#------\n# Average Total Round-Trip Latency threshold:\n"
      end

      text += "# Threshold Units:\nmicroseconds\n"
      dw=delay_warning_threshold.to_f*2
      text += "# Warning threshold:\n#{dw}\n"
      de=delay_error_threshold.to_f*2
      text += "# Failure threshold:\n#{de}\n"

      if test_type == 'Ethernet Frame Delay Test'
        text += "#------\n# Average Network Round-Trip Latency Variation threshold:\n"
      else
        text += "#------\n# Average Total Round-Trip Latency Variation threshold:\n"
      end

      text += "# Threshold Units:\nmicroseconds\n"
      text += "# Warning threshold:\n#{dv_warning_threshold.to_f*2}\n"
      text += "# Failure threshold:\n#{dv_error_threshold.to_f*2}\n"
      text += "#------\n# Percent Frames Lost threshold:\n"
      text += "# Warning threshold:\n#{flr_warning_threshold}\n"
      text += "# Failure threshold:\n#{flr_error_threshold}\n"
    elsif (test_type == 'Ping Active Test')
      text += "#------\n# Host:\n- Choose 'Other' radio button\n#{test_target_address.split("/")[0]}\n"
      text += "#------\n# VLAN ID:\n#{test_target_address.split("/")[1]}\n"
      text += "#------\n# VLAN User Priority:\n#{test_target_address.split("/")[2]}\n"
      text += "#------\n# Average Jitter threshold (microseconds):\n"
      text += "# Warning threshold:\n#{dv_warning_threshold.to_f*2}\n"
      text += "# Failure threshold:\n#{dv_error_threshold.to_f*2}\n"
      text += "#------\n# Average Round-Trip Latency threshold (microseconds):\n"
      dw=delay_warning_threshold.to_f*2
      text += "# Warning threshold:\n#{dw}\n"
      de=delay_error_threshold.to_f*2
      text += "# Failure threshold:\n#{de}\n"
      text += "#------\n# Percent Lost Packets threshold (percent):\n"
      text += "# Warning threshold:\n#{flr_warning_threshold}\n"
      text += "# Failure threshold:\n#{flr_error_threshold}\n"
    else
      SW_ERR "ERROR: Unknown Test Type: #{test_type}\n"
      text += "ERROR: Unknown Test Type: #{test_type}\n"
    end
    text += "- Click 'add this Guaranteed Service'\n"
    return text
  end

  def get_bx_vector_info
    sla_id =''
    service_instance_id = ''
    test_instance_id = ''
    test_instance_class_id = ''

    brix_server = get_monitoring_manager
    if brix_server == nil
      SW_ERR "Failed to find Brix server for (#{bx_sla_name})"
      return false, sla_id, service_instance_id, test_instance_id, test_instance_class_id
    end
      
    api = BxIf::StatsApi.new(brix_server)
    result, data = api.get_sla_id("#{bx_sla_name}")
    api.close
    if !result
      SW_ERR "BxIf::StatsApi.get_sla_id(#{bx_sla_name}) FAILED"
      return result, sla_id, service_instance_id, test_instance_id, test_instance_class_id
    end

    sla_id = data[0]['sla_id']
    
    api = BxIf::StatsApi.new(brix_server)
    result, data = api.get_service_instances("#{sla_id}")
    api.close
    
    if !result
      SW_ERR "BxIf::StatsApi.get_service_instances(#{sla_id}) FAILED"
      return result, sla_id, service_instance_id, test_instance_id, test_instance_class_id
    end
    
    data.each do |d|
      if d['service_instance_name'] == cos_name
        service_instance_id = d['service_instance_id']
      end
    end

    if service_instance_id == ''
      SW_ERR "Can't find service instance with name: #{cos_name}"
      return false, sla_id, service_instance_id, test_instance_id, test_instance_class_id
    end

    module_name = get_bx_module_name

    if (! module_name || module_name == '')
      SW_ERR "ERROR: Unknown Test Type: #{test_type}\n"
      return result, sla_id, service_instance_id, test_instance_id, test_instance_class_id
    end
    
    api = BxIf::StatsApi.new(brix_server)
    result, data = api.get_test_instances("#{module_name}","#{sla_id}","#{service_instance_id}")
    api.close

    if !result
      SW_ERR "BxIf::StatsApi.get_test_instances(#{sla_id}) FAILED"
      return result, sla_id, service_instance_id, test_instance_id, test_instance_class_id
    end

    data.each do |d|
      if d['service_instance_id'] == service_instance_id
        test_instance_id = d['test_instance_id']
        test_instance_class_id = d['test_instance_class_id']
      end
    end

    return result, sla_id, service_instance_id, test_instance_id, test_instance_class_id

  end

  def get_bx_module_name
    module_name = ''
    if (test_type == 'Ethernet Frame Delay Test')
      module_name = "com.brixnet.swv.ethframedelaytest.1.333"
    elsif (test_type == 'Ping Active Test')
      module_name = "com.brixnet.pingactivetest.10.3791"
    elsif (test_type == 'Ethernet Loopback Test')
      module_name = "com.brixnet.swv.ethloopbacktest.1.333" 
    else
      SW_ERR "ERROR: Unknown Test Type: #{test_type}\n"
    end
    return module_name
  end

  def bx_event_filter
    filter = ''

    if (test_type == 'Ethernet Frame Delay Test') || (test_type == 'Ethernet Loopback Test')
      if test_target_address[/(..:|..\/){6}/] != nil
        filter = test_target_address[/(..:|..\/){6}/].gsub(/[:\/]/, '')
        filter += test_target_address[/\/.*/]
        filter += " " + bx_sla_name
      else
        "TBD"
      end
    elsif (test_type == 'Ping Active Test')
      filter = test_target_address.sub(/\/.*$/, "")
      filter += " " + bx_sla_name
    elsif (!test_type)
      filter = 'TBD'
    else
      SW_ERR "get_bx_event_filter:: Unsupported Test Type: #{test_type}"
      filter = 'Unsupported Test Type'
    end

    return filter
  end
  
  def get_alarm_keys
    new_brix_alarms = []    
    return_value = true       
    # Create alarm histories for each CosTestVector if the endpoint is monitored
    if cos_end_point.segment_end_point.is_monitored    
      # Get the list of new alarms and also update the Brix Id's for each test
      result_ok, sla_id, service_instance_id, test_instance_id, test_instance_class_id = get_bx_vector_info
      if result_ok 
        self.sla_id = sla_id
        self.service_instance_id = service_instance_id
        self.test_instance_id = test_instance_id
        self.test_instance_class_id = test_instance_class_id
        unless self.save
          SW_ERR "Failed to save CosTestVector: #{cenx_name}"
          return_value = false
        end
      else
        SW_ERR "Failed to get vector info for CosTestVector: #{ctv.cenx_name}"
        return_value = false
      end
      new_brix_alarms = [bx_event_filter]
    end
    
    return return_value, new_brix_alarms     
  end    
    
  def go_in_service
    return_value = true
    info = ""
    if cos_end_point.segment_end_point.is_monitored
      return_value, new_brix_alarms = get_alarm_keys
      if return_value
        new_alarms = {} 
        new_brix_alarms.each {|filter| new_alarms[filter] = {:type => BrixAlarmHistory, :mapping => BrixAlarmHistory.default_brix_alarm_mapping}}
        return_value = update_alarm_history_set(new_alarms)
      end
    else
      # It's not monitored so delete any history
      alarm_histories.destroy_all
    end  
    info = "Failed to initialise" unless return_value
    return {:result => return_value, :info => info}
  end 
  

  # Return a Stats object which is used to hold/get Stats information
  # for the given time period
  def stats_data(start_time, end_time, current_time=Time.now, adjust_time=true)
    stats = BxStatsArchive.new(self, start_time, end_time, current_time, adjust_time)
    return stats
  end

  private

  # TODO Delete
#  def get_cenx_monitoring_cos_end_point
#    cenx_monitoring_cos_end_point = nil
#    result = false
#    result_str = ""
#
#    enni_cos_end_points = cos_end_point.get_enni_cos_end_points
#
#    if enni_cos_end_points.size > 1
#      # Never expect > 1 in a Off Net OVC
#      # TODO not sure about On Net OVC case
#      result_str = "ERROR - too many ENNI Endpoints found (#{enni_cos_end_points.size})"
#      result = false
#      return result, result_str, cenx_monitoring_cos_end_point
#    end
#
#    if enni_cos_end_points.size == 0
#      result_str = "TBD - no Member endpoint at ENNI"
#      result = true #not an error
#      return result, result_str, cenx_monitoring_cos_end_point
#    end
#
#    # One endpoint found
#    enni_cos_end_point = enni_cos_end_points.first
#    cenx_enni_cos_end_point = enni_cos_end_point.cos_end_points.first
#
#    if (!cenx_enni_cos_end_point)
#      result_str = "TBD - no Cenx endpoint at ENNI"
#      result = true #not an error
#      return result, result_str, cenx_monitoring_cos_end_point
#    end
#
#    if (!cenx_enni_cos_end_point.kind_of? CenxCosEndPoint)
#      result_str = "ERROR - wrong type of Cenx endpoint at ENNI"
#      result = false
#      return result, result_str, cenx_monitoring_cos_end_point
#    end
#
#    cenx_monitoring_cos_end_points = cenx_enni_cos_end_point.get_cenx_monitoring_cos_endpoints
#
#    if cenx_monitoring_cos_end_points.size > 1
#      # Never expect > 1
#      result_str = "ERROR - wrong number of Cenx Monitoring Endpoints found (#{cenx_monitoring_cos_end_points.size})"
#      result = false
#      return result, result_str, cenx_monitoring_cos_end_point
#    end
#
#    if cenx_monitoring_cos_end_points.size == 0
#      result_str = "TBD - no Cenx Monitoring Endpoint Found"
#      result = true #not an error
#      return result, result_str, cenx_monitoring_cos_end_point
#    end
#
#    # One endpoint found
#    cenx_monitoring_cos_end_point = cenx_monitoring_cos_end_points.first
#    result = true
#    return result, result_str, cenx_monitoring_cos_end_point
#  end
  
end
