# == Schema Information
# Schema version: 20110802190849
#
# Table name: member_attrs
#
#  id                   :integer(4)      not null, primary key
#  name                 :string(255)
#  affected_entity_id   :integer(4)
#  affected_entity_type :string(255)
#  type                 :string(255)
#  syntax               :string(255)
#  value_range          :string(255)
#  created_at           :datetime
#  updated_at           :datetime
#  allow_blank          :boolean(1)
#  disabled             :boolean(1)
#  default_value        :string(255)
#


class MemberAttr < ActiveRecord::Base
  belongs_to :affected_entity, :polymorphic => true #structural
  has_many :member_attr_instances, :dependent => :destroy
  has_many :member_attr_controls, :dependent => :destroy, :order => :execution_order

  validates_presence_of :name, :affected_entity_type
  validates_uniqueness_of :name, :scope => [:affected_entity_id, :affected_entity_type]

  validates_inclusion_of :syntax,
                         :in => MemberAttrTypes::ATTR_TYPES.map{|disp, value| value },
                         :message => "invalid, Use: #{MemberAttrTypes::ATTR_TYPES.map{|disp, value| value }.join(", ")}"
  validate :valid_default_value?
                           
  after_save :general_after_save

  def general_after_save
    ies = affected_entity ? affected_entity.instance_elements : eval(affected_entity_type).all
    
    ies.each do |ie|
      ie.update_member_attr_instances
      ie.save
    end
  end

  def ae_info_ma
    return "#{affected_entity_type}:#{affected_entity.name}"
  end
  
  def at_info_ma
    aacs = MemberAttrTypes::ATTR_AFFECTED_CLASSES.flatten
    aacs.collect!{|aac| aac.to_s}
    return aacs[aacs.rindex(affected_entity_type) - 1]
  end
  
  def t_info_ma
    aots = MemberAttrTypes::ATTR_OBJECT_TYPES.flatten
    aots.collect!{|aot| aot.to_s}
    return aots[aots.rindex(type ? type : "MemberAttr") - 1]
  end
  
  def const
    return case syntax
    when "integer" then (min == max) ? min : nil
    when "string" then (value_range.nil? || value_range.match(/^\/([^\/]*\\\/)*[^\/]*\/$/) || value_range.empty?) ? nil : value_range
    when "boolean" then (!value_range.nil? && ["yes", "no", "true", "false", "0", "1"].include?(value_range.downcase)) ? ((["yes", "true", "1"].include? value_range.downcase) ? '1' : '0') : nil
    when "menu" then (select_options.length == 1) ? select_options[0] : nil
    end
  end

  def select_options
    return value_range.nil? ? [] : value_range.split(/\s*;\s*/)
  end

  def min
    return (value_range && !value_range.strip.empty?) ? value_range.strip.split(/\s*-\s*/)[0].to_i : MIN
  end

  def max
    return (value_range && !value_range.strip.empty?) ? (value_range.strip.split(/\s*-\s*/)[1] ? value_range.strip.split(/\s*-\s*/)[1].to_i : value_range.strip.split(/\s*-\s*/)[0].to_i) : MAX
  end
  
  def pattern
    pattern = value_range.nil? ? nil : value_range.match(/^\/([^\/]*\\\/)*[^\/]*\/$/)
    return pattern ? Regexp.new(pattern[0].gsub(/^\/([^^])/){"/^#{$1}"}.gsub(/([^$])\/$/){"#{$1}$/"}[1..-2]) : value_range || ""
  end
  
  def default_value
    (syntax == "boolean") ? (!self[:default_value].nil? && (["yes", "true", "1"].include? self[:default_value].downcase) ? '1' : '0')  : self[:default_value]
  end
  
  private
  
  def valid_default_value?
    if self[:default_value].to_s.empty?
      return true 
    elsif !const.nil?
      errors.add(:default_value, "must be empty for a constant value!")
      return false
    elsif syntax == "integer"
      if !self[:default_value].to_s.match(/\A\d+\z/)
        errors.add(:default_value, "is not a valid integer!")
        return false
      elsif self[:default_value].to_i > max
        errors.add(:default_value, "may not be larger than #{max}!")
        return false
      elsif self[:default_value].to_i < min
        errors.add(:default_value, "may not be smaller than #{min}!")
        return false
      else
        return true
      end
    elsif syntax == "menu"
      return true if select_options.include? self[:default_value]
      errors.add(:default_value, "invalid, Use: #{select_options.join(", ")}")
      return false
    elsif syntax == "string"
      return true if self[:default_value].match(pattern) || self[:default_value] == pattern
      errors.add(:default_value, "does not match #{pattern.inspect}!")
      return false
    elsif syntax == "boolean"
      unless ["yes", "no", "true", "false", "0", "1"].include?(self[:default_value].downcase)
        errors.add(:default_value, "is not a valid boolean!")
        return false
      end
      return true
    end
  end
    
  MAX = 10**256 - 1
  MIN = -(10**255 - 1)

end
