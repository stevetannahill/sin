class BuilderLog < AutomatedBuildLog
  MAX_SIZE = 65535
  
  def self.summary(details = false, start_time = Time.at(0), end_time = Time.now)
    builder_logs = BuilderLog.where(:updated_at => (start_time)..(end_time))    
    success_summary = builder_logs.inject(Hash.new(0)) {|h,i| h[i.summary] += 1; h }
    if details
      info = Hash.new{|h,k| h[k]=Hash.new{|h1,k1| h1[k1]=Hash.new(0)}}
      builder_logs.collect do |bl|
        decoded_cid = CircuitIdFormat::SpirentSprint.decode(bl.name)
        if decoded_cid
          cascade = decoded_cid[:site_name]
          cascade = cascade.gsub(/^TEST/,"") if cascade[/^TEST/] # Some TEST circuits are in the playbokk - others are not..
          playbook = PlayBook.data(cascade)
          if playbook
            info[playbook[:oem]][playbook[:msc]+" [#{playbook[:msc_clli]}]"][bl.summary] += 1
          end
        end
      end
    end
    return success_summary,info if details
    return success_summary
  end
  
  def self.csv(summary_filter = "%", cid_filter = "%", start_time = Time.at(0), end_time = Time.now)   
    builder_logs = BuilderLog.where("summary like ? AND name like ? AND updated_at BETWEEN ? AND ?", summary_filter, cid_filter, start_time, end_time).order('updated_at DESC')    

    outfile = ""
    csv_out = CSV.new(outfile)

    attributes_to_dump = BuilderLog.new.attributes.keys
    csv_out << attributes_to_dump
                
    builder_logs.each do |log|
      csv_out << attributes_to_dump.collect {|what| log[what]}
    end
    return outfile
  end
  
  def self.oem_report_csv(start_time = Time.at(0), end_time = Time.now)
    summary, details = BuilderLog.summary(true, start_time, end_time)
    results_keys = summary.keys.sort
    
    outfile = ""
    csv_out = CSV.new(outfile)
    
    details.each do |oem, markets|
      csv_out << [oem]
      csv_out << [""] + results_keys
      markets.each do |market, results|
        csv_out << [market] + results_keys.collect {|result_key| results[result_key]}
      end
      csv_out << []
    end
    return outfile
  end
  
end