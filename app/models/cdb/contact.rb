# == Schema Information
# Schema version: 20110802190849
#
# Table name: contacts
#
#  id                  :integer(4)      not null, primary key
#  name                :string(255)
#  position            :string(255)
#  work_phone          :string(255)
#  mobile_phone        :string(255)
#  e_mail              :string(255)
#  fax                 :string(255)
#  service_provider_id :integer(4)
#  created_at          :datetime
#  updated_at          :datetime
#  address             :string(255)
#

class Contact < ActiveRecord::Base
  belongs_to :service_provider
  validates_presence_of :name, :e_mail
end
