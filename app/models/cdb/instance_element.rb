class InstanceElement < ActiveRecord::Base
  self.abstract_class = true
  
  has_many :member_attr_instances, :as => :affected_entity, :dependent => :destroy, :autosave => true, :include => [:member_attr]
  
  has_many :member_handles,
           :as => :affected_entity,
           :class_name => 'MemberHandleInstance',
           :conditions => "owner_type = 'ServiceProvider'", 
           :foreign_key => 'affected_entity_id'

  #validate :valid_member_attr_instances? #validating here rather then the default allows for proper error messages

  after_save :destroy_bad_member_attrs
  
  attr_accessor :use_tma, :use_cma
  
  before_validation(:set_affected_entities,:on => :update) 
  
  def set_affected_entities
    get_member_attr_instances.each{|mai| mai.affected_entity = self}
  end
  
  after_initialize :general_after_initialize
  
  def general_after_initialize
    @use_tma = (use_tma == nil) ? true : @use_tma
    @use_cma = (use_cma == nil) ? true : @use_cma
  end
  
  # Added by SIN, used by inventory controller
  def member_handle_for_owner_object(owner)
    member_handles.detect{|m| m.service_provider.id == owner.id}
  end

  # Added by SIN, used by inventory controller
  def member_handle_for_any_object
    member_handles.first
  end

  # Added by SIN, used by inventory controller
  def member_handle_for_owner_or_any_object(owner)
    member_handle = member_handles.detect{|m| m.service_provider.id == owner.id}
    # if no member handle for owner then just use one of the member handles
    # for example ENNIs may only have one member handle
    member_handle.nil? ? member_handle_for_any_object : member_handle
  end

  # Added by SIN, used by inventory controller
  def member_handle_for_owner_or_any(owner)
    o = member_handle_for_owner_or_any_object(owner)
    o ? o.value : ""
  end

  # Added by SIN, used by inventory controller
  def member_handle_for_any
    member_handle_for_any_object ? member_handle_for_any_object.value : ""
  end
  
  def member_handle_owners
    SW_ERR "No function member_handle_owners defined in #{self.class.to_s}. Returning an empty list instead."
    return []
  end
  
  def member_attr_instance name
    m = get_member_attr_instances.select{|mai| mai.name == name}.pop
    SW_ERR("Failed to find a member attr instance of name '#{name}' on #{self.to_s}.") if m == nil
    return m
  end
  
  def member_handle_instance name, owner
    m = get_member_attr_instances.select{|mai| (mai.is_a? MemberHandleInstance) && mai.owner == owner && mai.member_attr.name == name}.pop
    SW_ERR("Failed to find a member handle instance of name '#{name}' on #{self.to_s};ID: #{self.id} owned by #{owner.to_s};ID: #{owner.id}.") if m == nil
    return m
  end

  def member_handle_instance_by_owner owner
    m = get_member_attr_instances.select{|mai| (mai.is_a? MemberHandleInstance) && mai.owner == owner}.first
    return m
  end
  
  def member_attr_instance_exists name
    !get_member_attr_instances.select{|mai| mai.name == name}.empty?
  end
  
  def member_handle_instance_exists name, owner
    !get_member_attr_instances.select{|mai| (mai.is_a? MemberHandleInstance) && mai.owner == owner && mai.member_attr.name == name}.empty?
  end
  
  def set_member_attr_instances params = {}, use_full_form_id = false
    get_member_attr_instances.each{|mai| mai.value = params[use_full_form_id ? mai.full_form_id : mai.form_id] if params.has_key?(use_full_form_id ? mai.full_form_id : mai.form_id)}
  end
  
  def get_class_member_attr_instances
    mais = member_attr_instances.select{|mai| mai.member_attr != nil && mai.member_attr.affected_entity_id == nil && mai.member_attr.affected_entity_type == self.class.table_name.classify}
    mais.reject!{|mai| mai.is_a?(MemberHandleInstance) && !member_handle_owners.include?(mai.owner)}
    MemberAttr.all(:conditions => ["affected_entity_id is NULL and affected_entity_type = ?", self.class.table_name.classify]).each do |ma|
      mais.push(new_member_attr_instances ma, mais).flatten!
    end
    return mais
  end

  def get_member_attr_instances
    general_after_initialize
    mais = []
    #puts "<<<< Get MAIsxx -> #{@use_tma} : #{@use_cma}"
    if type_member_attr_instances_enabled
      if (defined? type_element) && type_element
        mais = member_attr_instances.select{|mai| type_element.member_attrs.include? mai.member_attr}
        mais.reject!{|mai| mai.is_a?(MemberHandleInstance) && !member_handle_owners.include?(mai.owner)}
        type_element.member_attrs.each do |ma|
          mais.push(new_member_attr_instances ma, mais).flatten!
        end
      end
      if (defined? type_elements) && type_elements
        type_elements.select{|te| te != nil}.each do |te|
          mais << member_attr_instances.select{|mai| te.member_attrs.include? mai.member_attr}
          mais.flatten!
          mais.reject!{|mai| mai.is_a?(MemberHandleInstance) && !member_handle_owners.include?(mai.owner)}
          te.member_attrs.each do |ma|
            mais.push(new_member_attr_instances ma, mais).flatten!
          end
        end
      end
    end
    return (class_member_attr_instances_enabled ? self.get_class_member_attr_instances : []) + mais
  end
  
  def update_member_attr_instances
    member_attr_instances = get_member_attr_instances
  end
  
  def class_member_attr_instances_enabled
    return @use_cma && cma_enabled_logic
  end
  
  def type_member_attr_instances_enabled
    return @use_tma && tma_enabled_logic
  end
  
  def self.attr_table attrs = [], title = "Attributes", headers = ["Name", "Value"], column_length = [6, 62]
    if headers.length != column_length.length
      SW_ERR("Call to attr_tables has inconsistent length of arrays headers and column_length (#{headers.length} and #{column_length.length} respectively).")
    end
    full_length = 0
    column_length.each_index{|i| full_length += column_length[i] }
    full_length += headers.length - 1
    result = "+" + "-" * (full_length) + "+\n" +
      "| #{title} " + " " * (full_length - (title.length + 2)) + "|\n"
    column_length.each{|l| result += "+" + "-" * l}
    result += "+\n"
    headers.each_index{|i| result += "| #{headers[i]} " + " " * (column_length[i] - (headers[i].length + 2))}
    result += "|\n"
    column_length.each{|l| result += "+" + "-" * l}
    result += "+\n"
    attrs.each{|attr|
      lines = 1
      headers.each_index{|i|
        lines = [lines, attr[i].length / column_length[i] + ((attr[i].length % column_length[i] > 0) ? 1 : 0)].max
      }
      lines.times{|l|
        headers.each_index{|i|
          line = (attr[i].length > l * (column_length[i] - 2)) ? attr[i][(l*(column_length[i]-2))..((l+1)*(column_length[i]-2)-1)] : ""
          result += "| #{line} " + " " * (column_length[i] - (line.length + 2))
        }
        result += "|\n"
      }
    }
    column_length.each{|l| result += "+" + "-" * l}
    result += "+\n"
  end
  
  def attr_snapshot
    attrs = attributes.map{|key, value| [key, value.to_s]}
    mhis = get_member_attr_instances.select{|mai| mai.is_a? MemberHandleInstance}.map{|mhi| [mhi.name, mhi.value.to_s, mhi.member_attr_id.to_s, mhi.owner.name, mhi.owner_type, mhi.owner_id.to_s]}
    mais = get_member_attr_instances.reject{|mai| mai.is_a? MemberHandleInstance}.map{|mai| [mai.name, mai.value.to_s, mai.member_attr_id.to_s]}
    column_length = [6, 7, 16, 12, 12, 10]
    for i in 0..1 do
      (attrs + mhis + mais).each{|a| column_length[i] = a[i].length + 2 if a[i].length + 2 > column_length[i]}
    end
    (mhis + mais).each{|a| column_length[2] = a[2].length + 2 if a[2].length + 2 > column_length[2]}
    for i in 3..5 do
      mhis.each{|a| column_length[i] = a[i].length + 2 if a[i].length + 2 > column_length[i]}
    end
    full_length = 5
    column_length.each_index{|i|
      column_length[i] = [column_length[i], 50].min
      full_length += column_length[i]
    }
    result = InstanceElement::attr_table attrs, "Attributes", ["Name", "Value"], [column_length[0], column_length[1]+column_length[2]+column_length[3]+column_length[4]+column_length[5]+4]
    if !mhis.empty?
      result += "|" + " " * full_length + "|\n"
      result += InstanceElement::attr_table mhis, "Attributes", ["Name", "Value", "Member Attr ID", "Owner Name", "Owner Type", "Owner ID"], [column_length[0], column_length[1], column_length[2], column_length[3], column_length[4], column_length[5]]
    end
    if !mais.empty?
      result += "|" + " " * full_length + "|\n"
      result += InstanceElement::attr_table mais, "Attributes", ["Name", "Value", "Member Attr ID"], [column_length[0], column_length[1], column_length[2]+column_length[3]+column_length[4]+column_length[5]+3]
    end
    return result
  end

  # Default stats entity is just the entity itself
  def stats_entities
    [self]
  end
  
  protected
  
  def cma_enabled_logic
    return true
  end
  
  def tma_enabled_logic
    return true
  end
  
  private
    
  def destroy_bad_member_attrs
    mais_to_delete = member_attr_instances.reject{|mai| get_member_attr_instances.include? mai}
    member_attr_instances = get_member_attr_instances
    mais_to_delete.each{|mai| mai.destroy}
  end
  
  def valid_member_attr_instances?
    valid = true
    get_member_attr_instances.each{|mai|
      valid = mai.valid? && valid #valid? must be first for all errors to appear
      mai.errors.each_full{|msg| errors[:base] << msg}
    }
    return valid
  end
  
  def new_member_attr_instances ma, mais = []
    return [] unless !mais.collect{|mai| mai.member_attr}.include?(ma) || ma.is_a?(MemberHandle)
    new_mais = []
    if ma[:type] == nil
      new_mais.push(MemberAttrInstance.new())
    elsif ma[:type] == 'MemberHandle'
      (member_handle_owners - mais.select{|mai| mai.member_attr == ma}.collect{|mai| mai.owner}).compact.each{|mao|
        m = MemberHandleInstance.new()
        m.owner = mao
        new_mais.push(m)
      }
    end
    new_mais.each{|mai|
      mai.member_attr_id = ma.id
      mai.affected_entity = self
      mai.affected_entity_id = (self.id && self.id > 0) ? self.id : self.cache_key
      mai.affected_entity_type = self.class.table_name.classify
      clear = mai.errors.empty?
      member_attr_instances.push(mai)
      mai.errors.clear if clear
    }
    return new_mais
  end

end
