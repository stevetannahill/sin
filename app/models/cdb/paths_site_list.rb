# == Schema Information
#
# Table name: paths_site_lists
#
#  id           :integer(4)      not null, primary key
#  path_id      :integer(4)
#  site_list_id :integer(4)
#
class PathsSiteList < ActiveRecord::Base 
	belongs_to :path
	belongs_to :site_list
	belongs_to :sin_path_service_provider_sphinx, :foreign_key => :path_id, :primary_key => :path_id
end
