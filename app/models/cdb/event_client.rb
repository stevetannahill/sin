require 'singleton'
require 'drb'
class EventClient
  include Singleton
  
  def initialize
    @drb_server = DRb.start_service
    @evt_reporter = DRbObject.new nil, "druby://localhost:3939"
  end

  def post_event(members,evt_tag, evt_objtype, evt_objid, timestamp, evt_notif, evt_details)
    # send event, and collect its position in the queue
    begin
      channels = members.map{|sp|sp.id}
      depth = @evt_reporter.report_demo(channels,evt_tag, evt_objid, evt_objtype, timestamp, evt_notif, evt_details)
    rescue DRb::DRbConnError => e
     SW_ERR "Cannot send to event server - Exception #{e}", :IGNORE_IF_TEST
    end
  end
  
end
