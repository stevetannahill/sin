# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class State < ActiveRecord::Base
  belongs_to :stateful
  after_save :update_associated_entity, :if => :notes_changed?
  after_destroy :update_associated_entity
  
  def update_associated_entity
    if stateful != nil
      state = stateful.get_state
      stateful.update_target_entity(state)
    end
  end
  
  def timestamp
    # timestamp is stored as a bigint so convert to a Time object 
    ts = read_attribute(:timestamp)
    if ts != nil
      return Time.at(ts.to_f/1000)
    else
      return nil
    end
  end
  
  # Convert timestamp to milliseconds if it's a Time object
  def timestamp=(new_time)
    # If it's a time object store just milliseconds not useconds
    if new_time.is_a?(Time)
      ts = (new_time.to_f * 1000).to_i
    else
      # If it's anything else (eg integer just use it)
      ts = new_time
    end
    write_attribute(:timestamp, ts)
  end
  
  def pre_conditions(stateful, previous_state, failed_objects = [])    
    return true
  end
  
  def post_conditions(stateful, previous_state, failed_objects = [])
    return true
  end
  
  def self.pre_text(stateful)
    return ""
  end
  
  def self.post_text(stateful)
    return ""
  end
  
  def valid_transitions
    SW_ERR "Error - should be called"
  end
  
  
  # helper function to set a state on another stateful and update the failed objects
  def update_state_and_failed_objs(stateful, new_state, failed_objects, top_level = false)
    result = true
    target_obj = stateful.stateful_ownership.stateable
    pre_result, post_result, pre_objs, post_objs = stateful.set_state(new_state, top_level)
    if !pre_result
      failed_objects << pre_objs
      result = false
      SW_ERR "pre_condition fail #{target_obj.class}:#{target_obj.id} #{failed_objects.inspect}"     
    elsif !post_result
      failed_objects << post_objs
      result = false
      SW_ERR "post_condition fail #{target_obj.class}:#{target_obj.id} #{failed_objects.inspect}"
    end
    return result
  end
  
end
