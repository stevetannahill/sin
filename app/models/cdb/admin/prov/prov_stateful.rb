# == Schema Information
#
# Table name: statefuls
#
#  id             :integer(4)      not null, primary key
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  name           :string(255)
#

class ProvStateful < Stateful  
  before_create :default_values
  
  PENDING = "Pending" 
  TESTING = "Testing" 
  READY = "Ready" 
  LIVE = "Live" 
  MAINTENANCE = "Maintenance" 
  DELETED = "Deleted"
  
  
  private

  def default_values
    self.name ||= "Provisioning"
  end

  
  def mappings
    return {ProvPending => ProvPending, 
            ProvTesting => ProvTesting, 
            ProvReady => ProvReady, 
            ProvLive => ProvLive, 
            ProvMaintenance => ProvMaintenance, 
            ProvDeleted => ProvDeleted}
  end
  
  def name_mappings
    return {PENDING => ProvPending, 
            TESTING => ProvTesting, 
            READY => ProvReady, 
            LIVE => ProvLive, 
            MAINTENANCE => ProvMaintenance, 
            DELETED => ProvDeleted}
  end
  
  public

  def initial_state
    return ProvPending
  end
  
  def state_hierarchy
    return [ProvPending, ProvTesting, ProvReady, ProvLive, ProvMaintenance, ProvDeleted]
  end

  def to_specific(state)
    the_state = state   
    # If a Class was passed create an object so is_a? can be used
    if state.class == Class
      the_state = state.new
    elsif state.class == String
      the_state = name_mappings[state].new
    end
    return mappings.find {|k,v| the_state.is_a?(k)}[1] 
  end

  # Convert from a subclass to the super class
  def to_generic(state)
    the_state = state   
    # If a Class was passed create an object so is_a? can be used
    if state.class == Class
      the_state = state.new
    elsif state.class == String
      the_state = name_mappings[state].new
    end
    return the_state.class if the_state.class.superclass == ProvState
    return mappings.find {|k,v| the_state.is_a?(v)}[0]     
  end
  
  def name_to_class(name)
    to_specific(name_mappings[name])
  end

  # Get the pre/post_text for the order if there is one
  def get_order_pre_text(order_state)
    text = ""
    target_obj = self.stateful_ownership.stateable
    latest_order = target_obj.get_latest_order
    if latest_order != nil
      text = latest_order.order_state.to_specific(order_state).pre_text(latest_order.order_state)
    end
    return text
  end

  def get_order_post_text(order_state)
    text = ""
    target_obj = self.stateful_ownership.stateable
    latest_order = target_obj.get_latest_order
    if latest_order != nil
      text = latest_order.order_state.to_specific(order_state).post_text(latest_order.order_state)
    end
    return text
  end
  
  def wrap_go_in_service
    if Stateful.test_env
      return {:result => true, :info => ""}
    else
      target_obj = stateful_ownership.stateable
      return target_obj.go_in_service
    end
  end
  
  def update_target_entity(new_state)
    if self.stateful_ownership != nil
      target_obj = self.stateful_ownership.stateable
      target_obj.class.where("id = ?", target_obj.id).update_all(:prov_name => new_state.name, :prov_timestamp => (new_state.timestamp.to_f*1000).to_i, :prov_notes => new_state.notes)    
      # The update_all just does an sql query and does not invoke the after_save (which is what we want) BUT it does not update the object copy in memory so must reload
      target_obj.reload
    end
  end  
  
  private

  def do_notify(new_state, changed)  
    if changed
      target_obj = self.stateful_ownership.stateable         
      target_obj.update_sm
    end
  end

end
