# == Schema Information
#
# Table name: statefuls
#
#  id             :integer(4)      not null, primary key
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  name           :string(255)
#

class NodeProvStateful < ProvStateful  
  private
  def mappings
    return {ProvPending => ProvPendingNode, 
            ProvTesting => ProvTestingNode, 
            ProvReady => ProvReadyNode, 
            ProvLive => ProvLiveNode, 
            ProvMaintenance => ProvMaintenanceNode, 
            ProvDeleted => ProvDeletedNode}
  end
end
