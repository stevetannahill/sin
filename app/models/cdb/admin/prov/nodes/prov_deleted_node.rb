# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class ProvDeletedNode < ProvDeleted
  def pre_conditions(stateful, previous_state, failed_objects = [])
    result = super
    # Check all demarcs are in a deleted state
    node = stateful.stateful_ownership.stateable
    demarcs = node.ports.collect {|port| port.demarc if port.demarc != nil && port.demarc.prov_state != nil}.compact
    result = result && demarcs.all? {|demarc| demarc.get_prov_state.is_a?(ProvDeleted)}
    
    # Add any demarcs that are not in Deleted state if the pre-condition failed
    if !result
      demarcs.each {|demarc| failed_objects << {:obj => demarc, :reason => "Demarc is no in Deleted state"} if !demarc.get_prov_state.is_a?(ProvDeleted)}
    end
    
    return result
  end
end
