# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class ProvLiveNode < ProvLive
  
  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    
    # Clear the Mtc if needed
    node = stateful.stateful_ownership.stateable
    node.set_mtc(false)
    
    # Now set all ENNIs that were in Mtc to Live
    demarcs_in_mtc = node.ports.collect {|port| port.demarc if port.demarc != nil && port.demarc.prov_state != nil && port.demarc.get_prov_state.is_a?(ProvMaintenance)}.compact
    demarcs_in_mtc.each {|demarc| 
      pre_result, post_result, pre_failed_objs = demarc.set_prov_state(self, false)
      failed_objects << pre_failed_objs
      result = false if !pre_result
    }
    return result
  end
end
