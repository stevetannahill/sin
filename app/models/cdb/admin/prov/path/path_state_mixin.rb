module PathStateMixin
  # Path takes on the lowest state of all the Segment's so it's state can change from any state to any state
  def valid_transitions
    return [ProvPending, ProvTesting, ProvReady, ProvLive, ProvMaintenance, ProvDeleted]
  end
end