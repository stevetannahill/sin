# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class ProvReadyPath < ProvReady
  include PathStateMixin

  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    target_obj = stateful.stateful_ownership.stateable
    # set order to Tested if an order exists
    latest_order = target_obj.get_latest_order
    if latest_order != nil && target_obj.active_order?
      if !update_state_and_failed_objs(latest_order.order_state, OrderTested, failed_objects, true)
        result = false
      end
    end
    
    return result
  end
end
