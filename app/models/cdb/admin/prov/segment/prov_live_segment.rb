# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class ProvLiveSegment < ProvLive
  include SegmentStateMixin

  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    segment = stateful.stateful_ownership.stateable
    segment.set_mtc(false)
    
    # If we are comming from Maintenance don't update the order details
    if !previous_state.is_a?(ProvMaintenance)
      # set order to Delivered if an order exists
      latest_order = segment.get_latest_order
      if latest_order != nil
        # If the Order is created don't do anything - it means a new order has been added.
        if !latest_order.get_order_state.is_a?(OrderCreated)
          if !update_state_and_failed_objs(latest_order.order_state, OrderDelivered, failed_objects, true)
            result = false
          end
        end
      end
    end
    
    return result
  end
  
end
