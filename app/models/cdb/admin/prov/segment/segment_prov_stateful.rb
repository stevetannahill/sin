# == Schema Information
#
# Table name: statefuls
#
#  id             :integer(4)      not null, primary key
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  name           :string(255)
#

class SegmentProvStateful < ProvStateful  
  private
  def mappings
    return {ProvPending => ProvPendingSegment, 
            ProvTesting => ProvTestingSegment, 
            ProvReady => ProvReadySegment, 
            ProvLive => ProvLiveSegment, 
            ProvMaintenance => ProvMaintenanceSegment, 
            ProvDeleted => ProvDeletedSegment}
  end
end
