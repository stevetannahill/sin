module SegmentStateMixin
  def pre_conditions(stateful, previous_state, failed_objects = [])
    result = super
    segment = stateful.stateful_ownership.stateable    
    endpoints = segment.segment_end_points
    if result
      ep_results = endpoints.all? do |ep|
        if ep.prov_state != nil          
          #pre_result, post_result, pre_failed_objects, post_failed_objects = ep.prov_state.drive_state(self, false) 
#          pre_result = ep.get_prov_state.update_state_and_failed_objs(ep.prov_state, self, failed_objects)
          pre_result, post_result, pre_failed_objects, post_failed_objects = ep.prov_state.drive_state(self)
          # Add the failed endpoint to the failed objects
          if !pre_result
            failed_objects << {:obj => ep, :reason => "Failed to change state"} 
            failed_objects << pre_failed_objects            
          elsif !post_result
            failed_objects << {:obj => ep, :reason => "Post conditions failed"} 
            failed_objects << post_failed_objects
          end
          pre_result
        else
          nil
        end
      end
      if endpoints.size != 0
        result = result && ep_results
      end      
    end
    
    return result
  end
  
  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    # Update the Path(s) state based on the state of all it's Segments
    target_obj = stateful.stateful_ownership.stateable
    target_obj.paths.each do |path|
      pre_result, post_result, pre_failed_objs = path.set_prov_state(self, false)
      failed_objects << pre_failed_objs
      result = false if !pre_result
    end
    return result
  end
end
