# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class ProvPendingSegment < ProvPending 
  include SegmentStateMixin
  
  def valid_transitions
    transitions = super   
    # If an order exists and it's Not DesignComplete or Provisioned then cannot move to Testing
    segment = stateful.stateful_ownership.stateable
    latest_order = segment.get_latest_order  
    if latest_order != nil && ! (latest_order.get_order_state.is_a?(OrderDesignComplete) || latest_order.get_order_state.is_a?(OrderProvisioned))
      transitions.delete(ProvTesting)
    end
    
    return transitions
  end
  
  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    segment = stateful.stateful_ownership.stateable
    # set order to to itself if an order exists. This is the first provisioning state so just make sure
    # the post conditions are run
    latest_order = segment.get_latest_order
    if latest_order != nil
      if !update_state_and_failed_objs(latest_order.order_state, latest_order.order_state.to_generic(latest_order.get_order_state), failed_objects, true)
        result = false
      end
      # If the state is Created and it's a OnNetOvc then automatically move to Accepted state
      if result && latest_order.get_order_state.is_a?(OrderCreated)
        if segment.is_a?(OnNetOvc)
          pre_result, post_result, pre_failed_objects, post_failed_object = latest_order.order_state.drive_state(OrderAccepted.new)
          if !(pre_result && post_result)
            result = false
            failed_objects << pre_failed_objects << post_failed_object
          end
        else          
          if latest_order.ordered_operator_network != nil
            if latest_order.ordered_operator_network.service_provider != nil
              if latest_order.ordered_operator_network.service_provider.is_system_owner 
                # Someone is ordering from CENX set the order to Accepted
                pre_result, post_result, pre_failed_objects, post_failed_object = latest_order.order_state.drive_state(OrderAccepted.new)
                if !(pre_result && post_result)
                  result = false
                  failed_objects << pre_failed_objects << post_failed_object
                end               
              else
                # We are ordering from someone else. The state is already Created do nothing 
              end
            else
              SW_ERR "#{latest_order.class}:#{latest_order.id} ordered_operator_network.service_provider is nil"
            end
          else
            SW_ERR "#{latest_order.class}:#{latest_order.id} ordered_operator_network is nil"
          end
        end
      end
    end
    return result
  end
  
end
