# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class ProvTestingDemarc < ProvTesting  
  
  def pre_conditions(stateful, previous_state, failed_objects = [])
    result = super   
    target_obj = stateful.stateful_ownership.stateable
    
    if result
      go_insv_result = stateful.wrap_go_in_service
      result = go_insv_result[:result]
      failed_objects << {:obj => target_obj, :reason => go_insv_result[:info]}
    end
    return result
  end
  
  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    target_obj = stateful.stateful_ownership.stateable

    # set order to Provisioned if an order exists
    latest_order = target_obj.get_latest_order
    if latest_order != nil
      if !update_state_and_failed_objs(latest_order.order_state, OrderProvisioned, failed_objects, true)
        result = false
      end
    end
    
    return result
  end
  
  def self.pre_text(stateful)
    return "Ensure all components are configured in CDB and Network"
    return text
  end
  
  def self.post_text(stateful)
    return "Test the Demarc"
  end
  
end
