# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class ProvMaintenanceDemarc < ProvMaintenance
  
  def valid_transitions       
    transitions = super
    if stateful == nil
      SW_ERR "Could not determine stateful for state"
    else
      # If there is an active order then add the extra transitions only when the order is Design Complete
      target_obj = stateful.stateful_ownership.stateable
      latest_order = target_obj.get_latest_order
      if latest_order != nil        
        latest_order_state = latest_order.get_order_state
        if latest_order_state.is_a?(OrderDesignComplete)
          case latest_order.action 
            when "Change", "New", "Renewal"            
              transitions = [ProvTesting, ProvReady]
            else
              SW_ERR "Invalid order type #{latest_order.action}"
          end           
        elsif latest_order_state.is_a?(OrderTested) && latest_order.action == "Disconnect"
          # The order is Tested and it's a Disconnect order so allow Deleted
          transitions = [ProvDeleted]
        elsif target_obj.active_order?
          # The order is not yet Design complete so can only be in Maintenance or Pending
          transitions = [ProvMaintenance, ProvPending]
        end
      end
    end
    return transitions
  end
  
  def pre_conditions(stateful, previous_state, failed_objects = [])
    result = super
    target_obj = stateful.stateful_ownership.stateable
    # If there is an active order then the order should be Created
    latest_order = target_obj.get_latest_order
    if latest_order != nil
      if target_obj.active_order?
        if !latest_order.get_order_state.is_a?(OrderCreated)
          failed_objects << {:obj => target_obj, :reason => "Active order is not in Created state"}
          result = false
        end
      end
    end
    
    return result
  end
  
  def post_conditions(stateful, previous_state, failed_objects = [])    
    result = super
    target_obj = stateful.stateful_ownership.stateable
    latest_order = target_obj.get_latest_order
    # Set the Order to either OrderAccepted or Tested depending on the current order
    if latest_order != nil
      if latest_order.get_order_state.is_a?(OrderCreated)
        case latest_order.action
          when "Disconnect"
            new_order_state = OrderTested.new
          else
            new_order_state = OrderAccepted.new
        end
        pre_result, post_result, pre_failed_objects, post_failed_objects = latest_order.order_state.drive_state(new_order_state)
        if !pre_result
          result = false
          failed_objects << pre_failed_objects
        end
        if !post_result
          result = false
          failed_objects << post_failed_objects
        end
      end
    end
    
    target_obj.set_mtc(true)
    
    # Now set all endpoints and Segments to Mtc - Note can just set the Segment as this also sets the endpoints
    segments = target_obj.segment_end_points.collect {|ep| ep.segment if ep.segment.get_prov_state.is_a?(ProvLive)}.uniq.compact
    segments.each {|segment| 
      pre_result, post_result, pre_failed_objs, post_failed_objs = segment.set_prov_state(self, false)
      failed_objects << pre_failed_objs 
      result = false if !pre_result
    }    
    
    return result
  end
  
  def self.pre_text(stateful)
    text = ""
    target_obj = stateful.stateful_ownership.stateable
    # Warn about the number of endpoints this will affect
    text << "\nWarning: This will affect #{target_obj.segment_end_points.size} Segment endpoints" if target_obj.segment_end_points.size != 0
    
    # If there is an active order then warn user that they cannot go back to Live
    latest_order = target_obj.get_latest_order
    if latest_order != nil
      if latest_order.get_order_state.is_a?(OrderCreated)  
        text << "\nWarning: There is a created order you will not be able to go back to Live state"
      end
    end
    return text
  end
  
end
