# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class ProvLiveDemarc < ProvLive
    
  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super

    # Set the Demarc to out of maintenance
    target_obj = stateful.stateful_ownership.stateable
    target_obj.set_mtc(false)
    
    # Now set all endpoints and Segments to Live - Note can just set the Segment as this also sets the endpoints
    segments_in_mtc = target_obj.segment_end_points.collect {|ep| ep.segment if ep.segment.get_prov_state.is_a?(ProvMaintenance)}.compact.uniq
    segments_in_mtc.each {|segment|
      pre_result, post_result, pre_failed_objs = segment.set_prov_state(self, false)
      failed_objects << pre_failed_objs
      result = false if !pre_result
    }
    
    # If we are comming from Maintenance then don't update the order
    if !previous_state.is_a?(ProvMaintenance)  
      # set order to Delivered if an order exists
      latest_order = target_obj.get_latest_order
      if latest_order != nil
        # If the Order is created don't do anything - it means a new order has been added.
        if !latest_order.get_order_state.is_a?(OrderCreated)
          if !update_state_and_failed_objs(latest_order.order_state, OrderDelivered, failed_objects, true)
            result = false
          end
        end
      end
    end
    
    return result
  end
end
