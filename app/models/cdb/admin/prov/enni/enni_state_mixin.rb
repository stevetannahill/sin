module EnniStateMixin
  def pre_conditions(stateful, previous_state, failed_objects = [])
    result = super
    # Check the Nodes's are in a higher prov state (for all states except Maintainance)
    demarc = stateful.stateful_ownership.stateable
    if demarc.ports.size == 0
      result = false
      failed_objects << {:obj => demarc, :reason => "Not attached to any ports"}
    else
      nodes = demarc.ports.collect {|port| port.node}.uniq.compact
      if nodes.size == 0
        result = false
        failed_objects << {:obj => demarc, :reason => "Not attached to any nodes"}
      else
        lowest_node_state = nodes.collect {|node| stateful.state_hierarchy.index(node.prov_state.to_generic(node.get_prov_state))}.min
        demarc_state = stateful.state_hierarchy.index(stateful.to_generic(self))
        result = result && (lowest_node_state >= demarc_state)    
    
        if !(lowest_node_state >= demarc_state)
          # Add all the nodes which are of a lower state to the failed_objects list
          nodes.each {|node| failed_objects << {:obj => node, :reason => "is in a lower state"} if stateful.state_hierarchy.index(node.prov_state.to_generic(node.get_prov_state)) < demarc_state}
        end
      end
    end
    return result
  end
  
end
