# == Schema Information
#
# Table name: statefuls
#
#  id             :integer(4)      not null, primary key
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  name           :string(255)
#

class EnniProvStateful < DemarcProvStateful  
  private
  def mappings
    return {ProvPending => ProvPendingEnni,
            ProvTesting => ProvTestingEnni,
            ProvReady => ProvReadyEnni,
            ProvLive => ProvLiveEnni,
            ProvMaintenance => ProvMaintenanceEnni,
            ProvDeleted => ProvDeletedEnni}
  end
end
