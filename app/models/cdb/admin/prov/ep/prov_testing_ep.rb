# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class ProvTestingEp < ProvTesting  
  include EpStateMixin
  
  def pre_conditions(stateful, previous_state, failed_objects = [])
    result = super
    if result
      go_insv_result = stateful.wrap_go_in_service
      result = go_insv_result[:result]
      failed_objects << {:obj => stateful.stateful_ownership.stateable, :reason => go_insv_result[:info]}        
    end
    return result
  end  
end
