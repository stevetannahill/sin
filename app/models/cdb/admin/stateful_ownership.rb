# == Schema Information
#
# Table name: stateful_ownerships
#
#  id             :integer(4)      not null, primary key
#  stateable_id   :integer(4)
#  stateable_type :string(255)
#  stateful_id    :integer(4)
#

class StatefulOwnership < ActiveRecord::Base
belongs_to :stateable, :polymorphic => true
belongs_to :stateful, :dependent => :destroy
attr_accessor :disable_set_state_on_create

  # This is only to be set to false when doing a upgrade of the old system to the new one
  # for the live system. The reason being is that pre/post conditions should NOT be run
  # for objects that are already providing service as this may cause loss of data
  @@set_state_on_create = true  
  
  def self.set_state_on_create(new_value)
    @@set_state_on_create = new_value
    return @@set_state_on_create
  end
  
  after_create do
    stateful.states << stateful.to_specific(stateful.initial_state).create
    if @@set_state_on_create
      # Set the set to itself - This will run the post-conditions
      pre_fail, post_fail, pre_objs, post_objs = stateful.set_state(stateful.initial_state)
    end
  end
    
end
