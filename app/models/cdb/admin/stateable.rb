module Stateable

  def self.included(base)
    base.has_many :statefuls, :as => :stateable , :through => :stateful_ownerships, :source => :stateful,  :dependent => :destroy
    base.has_many :stateful_ownerships, :as => :stateable, :dependent => :destroy
    #base.has_one :stateful, :as => :stateable
  end
  
  def prov_state
    statefuls.reject{|o| !o.is_a?(ProvStateful)}.first
  end
  
  def get_prov_state
    klass = prov_state.name_to_class(prov_name)
    klass.new(:name => prov_name, :notes => prov_notes, :timestamp => prov_timestamp, :stateful => prov_state)
    #state_get_state(prov_state)
  end
  
  def get_prov_name
    #state_get_name(prov_state)
    prov_name
  end
  
  def set_prov_state(new_state, top_level_call = true)
    prov_state.set_state(new_state, top_level_call)
  end
  
  
  def order_state
    statefuls.reject{|o| !o.is_a?(OrderStateful)}.first
  end
  
  def get_order_state
    klass = order_state.name_to_class(order_name)
    klass.new(:name => order_name, :notes => order_notes, :timestamp => order_timestamp, :stateful => order_state)
    #state_get_state(order_state)
  end
  
  def get_order_name
    state_get_name(order_state)
  end
  
  def set_order_state(new_state, top_level_call = true)
    order_state.set_state(new_state, top_level_call)
  end
  
  # Added by SIN used for display
  # Called on an entity (ie segment, demarc, path, etc)
  def prov_state_class
    # Add Provisioning state
    state = prov_state_for_diagram
    " provisioning #{state} " unless state.blank?
  end

  def prov_state_for_diagram
    # Add Provisioning state
    state = get_prov_name
    state.blank? ? '' : state.downcase.gsub(' ', '_')
  end
  
  # Called on an Order
  def order_state_class
    state = order_state_for_diagram
    " ordering #{state} " unless state.blank?
  end
  
  def order_state_for_diagram
    state = get_order_name
    state.blank? ? '' : state.downcase.gsub(' ', '_')
  end
  
  protected
  
  def state_get_state(the_state)
    if the_state == nil
      SW_ERR "the_state is nil for  #{self.class} #{self.id}"
      return nil
    else
      the_state.get_state
    end
  end
    
  def state_get_name(the_state)
    if the_state == nil
      SW_ERR "the_state is nil for  #{self.class} #{self.id}"
      return nil
    else
      the_state.get_state.name
    end
  end
  
  
end
