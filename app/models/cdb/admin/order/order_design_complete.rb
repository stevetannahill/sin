# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class OrderDesignComplete < OrderState 
  @@state_name = OrderStateful::DESIGN_COMPLETE
  def self.state_name
    return @@state_name
  end
  
  def valid_transitions
    transitions =  [OrderDesignComplete, OrderProvisioned, OrderRejected, OrderCancelled]
    # If there is an ordered object and it's provisioned state is not Testing or Maintenance then disallow OrderProvisioned state
    so = stateful.stateful_ownership.stateable
    ordered_object = so.ordered_entity

    # TODO: Removed by Andrew as we want to be able to transition to 'Provisioned' even if not in the Testing/Maintenance state
    # if ordered_object != nil
    #   if !(ordered_object.get_prov_state.is_a?(ProvTesting) || ordered_object.get_prov_state.is_a?(ProvMaintenance))
    #     transitions.delete(OrderProvisioned)
    #   end
    # end
    return transitions
  end
  
  def self.pre_text(stateful)
    return "Have the Engineering parameters been determined for the service?"
  end
  
  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    result = result && set_order_date(stateful, "design_complete_date", Time.now, failed_objects)
    return result
  end
  
  def self.post_text(stateful)
    s =  "Create all applicable objects in CDB\n" 
    s += "Configure all components in the Network\n"
    ordered_entity_type = stateful.stateful_ownership.stateable.ordered_entity_subtype.to_s.underscore.humanize.upcase    
    s += "Move #{ordered_entity_type} to 'Testing' State in CDB"
    return s
  end
  
end
