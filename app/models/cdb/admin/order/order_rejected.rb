class OrderRejected < OrderState 
  @@state_name = OrderStateful::REJECTED
  def self.state_name
    return @@state_name
  end

  def valid_transitions
    return [OrderRejected]
  end
end
