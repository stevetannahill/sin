# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class OrderProvisioned < OrderState 
  @@state_name = OrderStateful::PROVISIONED
  def self.state_name
    return @@state_name
  end
  
  def valid_transitions
    transitions = [OrderProvisioned, OrderTested, OrderRejected, OrderCancelled]
    # If there is an ordered object and it's provisioned state is not Ready or Maintenance then disallow OrderTested state
    so = stateful.stateful_ownership.stateable
    ordered_object = so.ordered_entity

    # TODO: Removed by Andrew as we want to be able to transition to 'Tested' even if not in the Ready/Maintenance state
    # if ordered_object != nil
    #   if !(ordered_object.get_prov_state.is_a?(ProvReady) || ordered_object.get_prov_state.is_a?(ProvMaintenance))
    #     transitions.delete(OrderTested)
    #   end
    # end
    return transitions
  end
end
