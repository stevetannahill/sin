# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class OrderReceived < OrderState 
  @@state_name = OrderStateful::RECEIVED
  def self.state_name
    return @@state_name
  end
  
  def valid_transitions
    transitions = [OrderReceived, OrderAccepted, OrderRejected, OrderCancelled]
    # If there is an ordered object and it's provisioned state is not Pending or Maintenance then disallow OrderAccepted state
    so = stateful.stateful_ownership.stateable
    ordered_object = so.ordered_entity
    if ordered_object != nil
      if !(ordered_object.get_prov_state.is_a?(ProvPending) || ordered_object.get_prov_state.is_a?(ProvMaintenance))
        transitions.delete(OrderAccepted)
      end
    end
    return transitions
  end
  
  def self.post_text(stateful)
    return "Raise a ticket to add components to CDB"
  end
  
  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    result = result && set_order_date(stateful, "order_received_date", Time.now, failed_objects)
    return result
  end
  
end
