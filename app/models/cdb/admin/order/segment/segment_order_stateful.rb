# == Schema Information
#
# Table name: statefuls
#
#  id             :integer(4)      not null, primary key
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  name           :string(255)
#

class SegmentOrderStateful < OrderStateful  
  
  private
  
  def mappings
    return {OrderCreated => OrderCreatedSegment,
            OrderReadyToOrder => OrderReadyToOrderSegment,
            OrderReceived => OrderReceivedSegment,
            OrderAccepted => OrderAcceptedSegment,
            OrderDesignComplete => OrderDesignCompleteSegment,
            OrderProvisioned => OrderProvisionedSegment,
            OrderTested => OrderTestedSegment,
            OrderCustomerAccepted => OrderCustomerAcceptedSegment,
            OrderDelivered => OrderDeliveredSegment,
            OrderRejected => OrderRejectedSegment,
            OrderCancelled => OrderCancelledSegment}
  end
  

end
