# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class OrderDesignCompleteSegment < OrderDesignComplete 

  def post_conditions(stateful, previous_state, failed_objects = [])
    so = stateful.stateful_ownership.stateable
    result = super
    if result
      stateful.emails_to_be_sent << CdbMailer.order_ticket(so)
      stateful.emails_to_be_sent << CdbMailer.order_design_complete_for_customer(so)   
    end
    return result
  end
  
end
