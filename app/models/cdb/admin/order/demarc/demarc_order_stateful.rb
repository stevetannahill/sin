# == Schema Information
#
# Table name: statefuls
#
#  id             :integer(4)      not null, primary key
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  name           :string(255)
#

class DemarcOrderStateful < OrderStateful  
    
  private
  
  def mappings
    return {OrderCreated => OrderCreatedDemarc,
            OrderReadyToOrder => OrderReadyToOrderDemarc,
            OrderReceived => OrderReceivedDemarc,
            OrderAccepted => OrderAcceptedDemarc,
            OrderDesignComplete => OrderDesignCompleteDemarc,
            OrderProvisioned => OrderProvisionedDemarc,
            OrderTested => OrderTestedDemarc,
            OrderCustomerAccepted => OrderCustomerAcceptedDemarc,
            OrderDelivered => OrderDeliveredDemarc,
            OrderRejected => OrderRejectedDemarc,
            OrderCancelled => OrderCancelledDemarc}
  end
  

end
