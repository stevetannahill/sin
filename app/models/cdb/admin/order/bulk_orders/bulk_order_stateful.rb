# == Schema Information
#
# Table name: statefuls
#
#  id             :integer(4)      not null, primary key
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  name           :string(255)
#

class BulkOrderStateful < OrderStateful  
  
  private
  
  def mappings
    return {OrderCreated => OrderCreatedBulk,
            OrderReadyToOrder => OrderReadyToOrderBulk,
            OrderReceived => OrderReceivedBulk,
            OrderAccepted => OrderAcceptedBulk,
            OrderDesignComplete => OrderDesignCompleteBulk,
            OrderProvisioned => OrderProvisionedBulk,
            OrderTested => OrderTestedBulk,
            OrderCustomerAccepted => OrderCustomerAcceptedBulk,
            OrderDelivered => OrderDeliveredBulk,
            OrderRejected => OrderRejectedBulk,
            OrderCancelled => OrderCancelledBulk}
  end
  
  public
  def set_state(new_state, top_level_call = true)   
    # If the Bulk order is a Circuit Order and there is a Path then the state is the passed in new_state
    # otherwise the Bulk state is determinted by the "lowest" state of it's Bulk Orders or Component Orders 
    # Ignore the passed state and find what state it should be
    target_obj = stateful_ownership.stateable
    if target_obj.bulk_order_type == "Circuit Order" && target_obj.ordered_entity != nil
      return super
    else
      #order_states = target_obj.service_orders.collect {|order| order.order_state.to_generic(order.get_order_state) if order.order_state != nil}.uniq.compact    
      order_states = target_obj.service_orders.collect {|order| order.order_name}.uniq.compact
      order_states = order_states.collect {|state| target_obj.order_state.to_generic(target_obj.order_state.to_specific(state))}.uniq.compact      

      if !order_states.empty?
        # There are orders so find the lowest one
        new_state = state_hierarchy[order_states.collect {|order_state| state_hierarchy.index(order_state)}.compact.min]
      else
        # There are no orders just stay in the same state
        super(get_state)
      end
    end
    return super(new_state, top_level_call)    
  end

end
