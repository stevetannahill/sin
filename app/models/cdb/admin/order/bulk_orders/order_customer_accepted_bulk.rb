# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class OrderCustomerAcceptedBulk < OrderCustomerAccepted 
  include BulkStateMixin 
  
  def pre_conditions(stateful, previous_state, failed_objects = [])
    result = super
    target_obj =  stateful.stateful_ownership.stateable
    if target_obj.bulk_order_type == "Circuit Order"
      # only allow the state change to Customer Accepted if all service orders are at least Customer Accepted
      order_states = target_obj.service_orders.collect {|order| order.order_state.to_generic(order.get_order_state) if order.order_state != nil}.uniq.compact    
      if !order_states.empty?
        # There are orders so find the lowest one
        new_state = stateful.state_hierarchy[order_states.collect {|order_state| stateful.state_hierarchy.index(order_state)}.compact.min]
        result = new_state == OrderCustomerAccepted || new_state == OrderDelivered
        if !result
          failed_objects << {:obj => stateful.stateful_ownership.stateable, :reason => "Some Service orders are less than #{OrderCustomerAccepted.to_s}. Lowest state is #{new_state.to_s}"}
        end
      end      
    end
    return result
  end
  
  
end
