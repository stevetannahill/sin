class OrderCancelled < OrderState 
  @@state_name = OrderStateful::CANCELLED
  def self.state_name
    return @@state_name
  end

  def valid_transitions
    return [OrderCancelled]
  end
end
