# == Schema Information
#
# Table name: statefuls
#
#  id             :integer(4)      not null, primary key
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  name           :string(255)
#

class Stateful < ActiveRecord::Base  
  has_many :states, :dependent => :destroy, :order => "timestamp"
  has_one :stateful_ownership, :dependent => :destroy
  attr_accessor :test_env
  cattr_accessor :emails_to_be_sent
  @@emails_to_be_sent = []
  
  # test_env should ONLY be set for testing purposes. Setting this to true
  # will prevent any interaction with live hardware(eg go_in_service) and will
  # always pass 
  @@test_env = false
  
  def self.test_env=(new_value)
    @@test_env = new_value
    SW_ERR "test_env set to #{new_value}"
  end
  
  def self.test_env
    @@test_env
  end
  

  def initial_state
    SW_ERR "Should not be called #{self.inspect}"
    return nil
  end
  
  def to_specific(state)
    SW_ERR "Calling default to_specific! #{self.inspect} #{state.inspect}"
    return state.class
  end
    
  # top_level_call is used to indicate when to invoke the Service Monitoring Notifications
  # because setting a set can set other states so only when the top_level caller returns
  # should the notifications be sent.
  # Note that the pre/post condtions are within a ActiveRecord transaction. If a Pre or Post
  # condition fails then the database is rolled back. This means any other database changes made by the Pre/Post
  # conditions will be rolled back as well.
  def set_state(new_state, top_level_call = true)
    pre_result = false
    post_result = false
    pre_failed_objects = []
    post_failed_objects = []
    
    # If the parameter is a class create an object
    if new_state.class == Class
      new_state = new_state.new()
    end
    
    Stateful.transaction(:requires_new => true) do
      @@emails_to_be_sent = []
      real_state = to_specific(new_state).create(:name => new_state.name, :notes => new_state.notes, :timestamp => new_state.timestamp)
      
      previous_state = get_state    
      if pre_result = real_state.pre_conditions(self, previous_state, pre_failed_objects)
        changed = true
        if real_state.class == get_state.class
          # It's the same state so don't save it
          real_state.destroy
          real_state = get_state
          changed = false
        else
          states << real_state
        end
        update_target_entity(real_state)
        
        # If post_conditions "fail" what do we do - remain in current state ? 
        post_result = real_state.post_conditions(self, previous_state, post_failed_objects)
        if !post_result
          SW_ERR "Post conditions failed for state #{real_state.class} for #{self.stateful_ownership.stateable.class}:#{self.stateful_ownership.stateable.id} reason #{post_failed_objects.inspect}"
          raise ActiveRecord::Rollback
        end
        
        if Stateful.send_emails
          do_notify(real_state, changed)
        else
          # Failed to send emails undo any state and database changes
          post_result = false
          post_failed_objects << {:obj => self.stateful_ownership.stateable, :reason => "Failed to send emails"}
          raise ActiveRecord::Rollback
        end
      else
        SW_ERR "Failed to set state to #{new_state.class} from #{get_state.class} for #{self.stateful_ownership.stateable.class}:#{self.stateful_ownership.stateable.id} reason #{pre_failed_objects.inspect}", :TEMPORARILY_IGNORE
        real_state.destroy
        raise ActiveRecord::Rollback
      end
    end    
    return pre_result, post_result, pre_failed_objects.flatten, post_failed_objects.flatten
  end

  def get_state
    states.last
  end
  
  def get_state_at(timestamp)
    return states.where("timestamp <= ?", timestamp).last
  end
  
  def state_count
    return states.size
  end
  
  def debug_dump_states
    puts "#{self.class.to_s}:#{self.id}"
    states.each do |state|
      puts("#{Time.at(state.timestamp).strftime("%a %b %d %H:%M:%S")} #{state.class.to_s}\n")
    end
    nil
  end
  
  def update_target_entity(new_state)
    SW_ERR "Should be not called #{self.class}:#{self.id}"
  end
    
  # helper method to move a state from one state through to another state
  def drive_state(new_state, top_level = true)
    pre_failed_objects = []
    post_failed_objects = []
    pre_result = true
    post_result = true
    
    # find set of states to go through - TODO Need to check the state for range
    start_state = state_hierarchy.find_index(to_generic(self.get_state))
    end_state = state_hierarchy.find_index(to_generic(to_specific(new_state)))
    
    # Can only drive to higher states
    if start_state > end_state
      SW_ERR "Invalid drive - Tried to move #{self.stateful_ownership.stateable.class} #{self.stateful_ownership.stateable.id} from #{start_state} to #{end_state}"
      return false, false, [], []
    end
    
    start_state += 1 if start_state < end_state # if moving to a different state skip the current state
        
    state_hierarchy[start_state..end_state].each do |state|
      pre_result, post_result, pre_failed_objects, post_failed_objects = set_state(state, top_level)
      # If anything failed then stop
      if !pre_result || !post_result
        SW_ERR "#{Time.now} Failed Trying to move #{self.stateful_ownership.stateable.class} #{self.stateful_ownership.stateable.id} to #{state}"
        break
      end
    end
    
    return pre_result, post_result, pre_failed_objects.flatten, post_failed_objects.flatten
  end
  
  private
  
  def do_notify(new_state, changed)
    SW_ERR "Should be not called #{new_state.inspect} #{changed}"
  end
  
  def self.send_emails
    success = true
    begin
      @@emails_to_be_sent.each(&:deliver) if EmailConfig.instance.enable_order_email
    rescue 
      SW_ERR "Exception sending mail: Exception is #{$!}"
      success = false
    end
    @@emails_to_be_sent = []
    return success
  end
  
end
