# == Schema Information
# Schema version: 20110802190849
#
# Table name: ordered_entity_type_links
#
#  id                       :integer(4)      not null, primary key
#  ordered_entity_group_id  :integer(4)
#  ordered_entity_type_id   :integer(4)
#  ordered_entity_type_type :string(255)
#  created_at               :datetime
#  updated_at               :datetime
#

class OrderedEntityTypeLink < ActiveRecord::Base
  belongs_to :ordered_entity_type, :polymorphic => true
  belongs_to :ordered_entity_group
  
  def oet_name
    return ordered_entity_type.name
  end
  
  def oet_class
    return ordered_entity_type.class
  end
  
  def oeg_name
    return ordered_entity_group.name
  end
end
