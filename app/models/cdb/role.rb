class Role < ActiveRecord::Base
  has_many :users
  has_and_belongs_to_many :privileges
  validates_uniqueness_of :name
  validates_presence_of :name
  default_scope :order => :name
  before_destroy :check_users
  
  accepts_nested_attributes_for :privileges
  
  private
  
  def check_users
    if self.users.count > 0
      errors.add(:base, "Cannot remove a role that belongs to a user")
      return false
    end
  end
end



