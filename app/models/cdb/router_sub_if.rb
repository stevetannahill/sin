
class RouterSubIf < OnNetOvcEndPointEnni
  
  # Layer 3
  def layer
    3
  end

  def on_switch_name
    return "sub-if #{ip_address} #{interface_name}#{sap_encap_string}"
  end

end
