# == Schema Information
# Schema version: 20110802190849
#
# Table name: qos_policies
#
#  id          :integer(4)      not null, primary key
#  type        :string(255)
#  policy_name :string(255)
#  policy_id   :integer(4)
#  site_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#

class EgressQosPolicy < QosPolicy

  def generate_config
    config = Config.new(get_own_policy)
    text = "configure qos #{config.egress_qos_policy_config_name} #{policy_id} create\n"
    text += config.generate_config
    text += "exit\n"
  end

  def generate_delete
    config = Config.new(get_own_policy)
    text = "no #{config.egress_qos_policy_config_name} #{policy_id}\n"
    return text
  end

  #This class handles the creation of an Egress QoS Policy config section
  class Config < QosPolicy::Config
    def generate_config
      #Start with general config
      config = super

      shapers = @sections['Shaping']
      policers = @sections['Policing']
      nones = @sections['None']

      #Add needed queues (Shapers)
      if shapers.empty?
        config += "#{egress_qos_entity_config_name} 1 create\n"
        config += "exit\n"
      else
        shapers.each do |s|
          config += s.generate_config
        end
      end

      #Add needed policers (Policers)
      policers.each do |p|
        config += p.generate_config
      end

      #Add needed blank policers (None)
      nones.each do |p|
        config += p.generate_config
      end

      #Add fc sections (Forwarding Class)
      (shapers + policers + nones).each do |rl|
        config += rl.generate_egress_fc_config
      end
      
      return config
    end

  end
end
