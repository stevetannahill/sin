# == Schema Information
#
# Table name: segment_end_points
#
#  id                    :integer(4)      not null, primary key
#  type                  :string(255)
#  cenx_id               :string(255)
#  segment_id            :integer(4)
#  demarc_id             :integer(4)
#  segment_end_point_type_id :integer(4)
#  md_format             :string(255)
#  md_level              :string(255)
#  md_name_ieee          :string(255)
#  ma_format             :string(255)
#  ma_name               :string(255)
#  is_monitored          :boolean(1)
#  notes                 :string(255)
#  stags                 :string(255)
#  ctags                 :string(255)
#  oper_state            :string(255)
#  created_at            :datetime
#  updated_at            :datetime
#  eth_cfm_configured    :boolean(1)      default(TRUE)
#  reflection_enabled    :boolean(1)
#  mep_id                :string(255)     default("TBD")
#  event_record_id       :integer(4)
#  sm_state              :string(255)
#  sm_details            :string(255)
#  sm_timestamp          :integer(8)
#  prov_name             :string(255)
#  prov_notes            :text
#  prov_timestamp        :integer(8)
#  order_name            :string(255)
#  order_notes           :text
#  order_timestamp       :integer(8)
#

class SegmentEndPoint < InstanceElement
  include Eventable
  include Stateable

  
  belongs_to :segment #structural
  belongs_to :demarc
  belongs_to :segment_end_point_type
  belongs_to :type_element, :class_name => "SegmentEndPointType", :foreign_key => "segment_end_point_type_id"

  # Connected Endpoints
  has_and_belongs_to_many :segment_end_points,
                          # added class_name because rails is looking for SegmentEndPoints instead of SegmentEndPoint
                          :class_name => "SegmentEndPoint",
                          :foreign_key => "self_id",
                          :after_add => :add_reverse_link,
                          :after_remove => :remove_reverse_link
  has_many :cos_end_points, :dependent => :destroy
  has_many :cos_test_vectors, :through => :cos_end_points

  # Find endpoints for an Path
  scope :by_path, lambda { |path|
    {
      :include => {:segment => :paths},
      :conditions => [ "paths.id = ?", path.id ]
    }
  }


  validates_presence_of :segment_id, :segment_end_point_type_id
  #validates_class_of :segment, :is_a => "OffNetOvc", :if => Proc.new{|segmentep| !(segmentep.is_a? OnNetOvcEndPointEnni)}
  validates_as_cenx_id :cenx_id
  
  before_validation(:on => :create) do
    self.cenx_id = IdTools::CenxIdGenerator.generate_cenx_id(self.class)
  end

	# Sometimes we don't want the diagram to generated everytime we save an ep (ie Path Factory).  
  # The diagram generation method (layout_diagram) will have to be called explicitely but another method
  attr_writer :layout_diagram_on_save

  def layout_diagram_on_save
    defined?(@layout_diagram_on_save) ? @layout_diagram_on_save : true
  end

  after_save :monitored_changed, :if => :is_monitored_changed? 
  after_save :generate_diagram, :if => :layout_diagram_on_save
  after_save :assoc_service_providers_for_indexing_entities

  # Default to layer 2
  def layer
    2
  end

  def effected_members
    segment.effected_members
  end

  def self.get_bx_monitored_endpoints
    #all.reject{|ep| ep.cos_test_vectors.size == 0}
    SegmentEndPoint.joins(:cos_test_vectors).where(:is_monitored => true)
  end

  def base_get_candidate_endpoints_to_link_with
    list = demarc ? demarc.segment_end_points.reject{|e| e == self } : []
  end

  def outer_tag_value
    SW_ERR "outer_tag_value must be implemented in subclass"
    return "ERROR!"
  end

  def segment_endpoint_type_name
    (segment_end_point_type ? segment_end_point_type.name : "")
  end
  
  protected :base_get_candidate_endpoints_to_link_with
  
  def class_get_candidate_endpoints_to_link_with
    base_get_candidate_endpoints_to_link_with
  end
  
  #validate_inclusion_of_each :segment_end_points, :in => :class_get_candidate_endpoints_to_link_with, :message => "contains end points which should not be connected."
  
  before_validation(:general_before_validation_on_update,:on => :update)

  def general_before_validation_on_update
    set_affected_entities
    segment_end_points.delete(demarc.nil? ? segment_end_points : segment_end_points - demarc.segment_end_points)
  end
  
  def add_reverse_link(segmentep)
    segmentep.segment_end_points << self unless segmentep.segment_end_points.include?(self) || segmentep == self
    assoc_service_providers_for_indexing_entities
    generate_diagram if layout_diagram_on_save
  end
                          
  def remove_reverse_link(segmentep)
    segmentep.cos_end_points.each{|cose|
      cose.cos_end_points.delete(cos_end_points)
    }
    segmentep.segment_end_points.delete(self)  if segmentep.segment_end_points.include?(self) && segmentep != self
    assoc_service_providers_for_indexing_entities
    generate_diagram if layout_diagram_on_save
  end

  #---------------------------------
  # Start Instance Element Code
  #---------------------------------

  def member_handle_owners
    owners = segment ? segment.member_handle_owners : []
    # We dont want cenx included
    return owners.uniq#.reject{|mho| mho.is_system_owner}
  end

  def member_handle_prefix owner
    owner.name + "'s "
  end

  #---------------------------------
  # End Instance Element Code
  #---------------------------------

  def tma_enabled_logic
    return segment ? segment.use_member_attrs : true
  end

  def operator_info_segmentep
    return segment ? segment.operator_info_segment : 'ERROR: Unknown Operator'
  end

  def buyer_member
    return segment ? segment.buyer : nil
  end

  def site_info_segmentep
    segment.site_info_segment
  end

  def cenx_name
    #short_sp_name = CenxNameTools::CenxNameHelper.shorten_name operator_info_segmentep
    type = CenxNameTools::CenxNameHelper.generate_type_string self
    return "#{type}:#{cenx_id}:#{demarc_cenx_name}:#{(is_monitored) ? demarc_mon_address : "Not-Mon"}"
  end

  def bx_sla_name
    # WARNING THIS CODE CANT BE CHANGED
    # Please Talk to Dave Schubet first if you need to change this code
    # The string returned here is copied into BRIXWORX SLA Name
    # it is used when gatherig data via the brix API
    # it is also used when receiving SNMP traps from BRIXWORX
    type = CenxNameTools::CenxNameHelper.generate_type_string self
    return "#{type}:#{cenx_id}"
  end

  def bx_act_name
    short_buyer_name = CenxNameTools::CenxNameHelper.shorten_name buyer_member.name
    return "#{short_buyer_name}->#{demarc_bx_act_name}"
  end
  
  def demarc_mon_address
    demarc ? demarc.get_mon_loopback_address : "TBD"
  end

  def demarc_cenx_name
    return demarc ? demarc.cenx_name : "No-Demarc-Yet"
  end

  def demarc_bx_act_name
    demarc ? demarc.bx_act_name : "TBD"
  end

  def demarc_cenx_id
    return demarc ? demarc.cenx_id : "No-Demarc-Yet"
  end

  def demarc_prov_info
    return demarc ? demarc.get_prov_name : "TBD"
  end

  def is_connected_to_demarc
    return  demarc_id != nil
  end

  def supports_eth_oam_reflection
    return is_monitored && (demarc ? demarc.supports_eth_oam_reflection : false)
  end

  def demarc_connection_info_segmentep
    return demarc_cenx_name
  end

  def end_point_connection_info
    segment_end_points.empty? ? "Nothing" : segment_end_points.map{|segmentep| segmentep.cenx_name}.join("; ")
  end
  
  def get_candidate_endpoints_to_link_with
    class_get_candidate_endpoints_to_link_with - segment_end_points
  end
  
  def ethernet_service_type
    return segment ? segment.ethernet_service_type : nil
  end
  
  def get_candidate_segment_end_point_types
    return ethernet_service_type ? ethernet_service_type.segment_end_point_types : []
  end

  def demarc_string_for_view
    (self.kind_of? OvcEndPointEnni) ? "ENNI" : "Demarc"
  end

  def get_cos_instances
    return segment.cos_instances
  end

  def cos_end_point_count
    return cos_end_points.size
  end

  def connected_end_point_count
    return segment_end_points.size
  end

  def cos_instances
    return segment.cos_instances
  end
  
  def get_candidate_cos_instances
    return segment.cos_instances.reject{|cosi| cos_end_points.map{|cose| cose.cos_instance}.include? cosi}
  end

  def generate_mep_id
    if is_monitored
      self.mep_id = "TBD"
    else
      self.mep_id = "N/A"
    end
  end

  def get_mep_ids
    meps = mep_id.split(';')
    meps = [mep_id] unless meps
    return meps
  end

  def get_ccm_monitoring_domain_id
    result = 0

    if cos_test_vectors.collect {|v| v.test_type}.flatten.uniq.include? "Ethernet Loopback Test"
      result = md_level.to_i*10
    elsif cos_test_vectors.collect {|v| v.test_type}.flatten.uniq.include? "Ethernet Frame Delay Test"
      result = md_level.to_i
    end

    if result == 0
      SW_ERR "get_ccm_monitoring_domain_id - invalid result #{result}"
    end

    return result

  end

  after_create :general_after_create
  def general_after_create
    # Create Service Monitoring history
    event_record_create
    update_attributes(:sm_state => AlarmSeverity::NOT_LIVE.to_s, :sm_details => "", :sm_timestamp => (Time.now.to_f*1000).to_i)
    sm_histories << SmHistory.create(:event_filter => "SM #{self.class.name}:#{cenx_id}")
    # create the Provisioning state
    statefuls << EpProvStateful.create
    state = prov_state.get_state
    update_attributes(:prov_name => state.name , :prov_timestamp => state.timestamp, :notes => state.notes)
  end

  def assoc_service_providers_for_indexing_entities
    segment.paths do |path|
      path.assoc_service_providers_for_indexing_paths
    end
  end

  def default_cos_handling
    if segment_end_point_type
      return segment_end_point_type.default_cos_type_info
    else
      return "Undefined"
    end
  end
    
  def mtc_history  
    return segment.mtc_history    
  end    
  
  def mtc_state
    return segment.mtc_state
  end
  
  def alarm_history   
    history = super
    cos_end_points.each {|cep| history = cep.alarm_history + history} if is_monitored
    return history   
  end    

  def check_neighbour_ep(time_interval)
    ep_alarms = segment_end_points.collect do |ep|
      tmp = {:oper => ep.event_record, :prov => ep.mtc_state}
      tmp[:oper_period] = tmp[:oper]
      #tmp = {:oper => ep.get_SM_state, :prov => ep.get_prov_state, :oper_period => ep.get_SM_state}
      if time_interval != 0
        tmp_ah = ep.alarm_history.subset((Time.now.to_i-time_interval)*1000, Time.now.to_i*1000)        
        last_period_alarms = tmp_ah.alarm_records.detect {|ar| ar.state == AlarmSeverity::UNAVAILABLE} if tmp_ah.alarm_records.size != 0
      
        if last_period_alarms != nil
          tmp[:oper_period] = last_period_alarms
        else
          tmp[:oper_period] = tmp[:oper]              
        end
      end
      tmp
    end
    ep_alarms.any?{|ep_alarm| ep_alarm[:prov] == AlarmSeverity::MTC || ep_alarm[:oper_period].state == AlarmSeverity::UNAVAILABLE}      
  end
        
  def sm_state_core_ls(state)    
    if is_monitored    
      if state.state != AlarmSeverity::OK
        if check_neighbour_ep(90)          
          state = state.dup
          state.state = AlarmSeverity::DEPENDENCY
          #state.time_of_event = (Time.now.to_f*1000).to_i
          return state
        end
      end
                          
      # Which COS to we pick ???? any will do
      if cos_end_points.size != 0      
        cos_end_points_to_cenx = cos_end_points.first.get_remote_sibling_cos_end_points
        # Get the current state of each endpoint
        # Also find any endpoint that was Unavailable in the last period (1 minute for Brix but need to add 30 seconds as there is some
        # delay for Brix SNMP handling - TODO Need to remove the hardcoding at some point) as this will also cause an Dependency alarm
        cos_end_points_to_cenx.collect! do |cp|           
          tmp = {:oper => cp.segment_end_point.event_record, :prov => cp.segment_end_point.mtc_state}
          tmp[:oper_period] = tmp[:oper]
          tmp_ah = cp.segment_end_point.alarm_history.subset((Time.now.to_i-90)*1000, Time.now.to_i*1000)
          last_period_alarms = tmp_ah.alarm_records.detect {|ar| ar.state == AlarmSeverity::UNAVAILABLE} if tmp_ah.alarm_records.size != 0
          if last_period_alarms != nil
            tmp[:oper_period] = last_period_alarms
          else
            tmp[:oper_period] = tmp[:oper]              
          end
          tmp
        end                
      
        if cos_end_points_to_cenx.any? {|ar| ar[:prov].state == AlarmSeverity::MTC || ar[:oper_period].state == AlarmSeverity::UNAVAILABLE}
          # If any endpoint is UA or MTC then the member endpoint is Dependency unless the Birx returns Ok          
          if state.state != AlarmSeverity::OK
            state = state.dup
            state.state = AlarmSeverity::DEPENDENCY
            return state
          else            
            return state
          end
        end

        # if any endpoints are Dependency then mark the current endpoint as Dependency if it has any faults or
        # just update the time if they are Ok 
        if cos_end_points_to_cenx.any? {|ar| ar[:oper].state == AlarmSeverity::DEPENDENCY}          
          if state.state != AlarmSeverity::OK
            state = state.dup
            state.state = AlarmSeverity::DEPENDENCY
            return state
          else            
            return state
          end
        end
      else
        # There are no COS end points nothing else to do
      end
    else
      # if the segment state is Dependency the endpoint will be Depencency
      #segment_state = segment.alarm_state # causes nested alarm_states.....
      #segment_state = segment.get_SM_state
      segment_state = segment.event_record
      if segment_state.state == AlarmSeverity::DEPENDENCY && state.state != AlarmSeverity::OK
        state = segment_state.dup
        #state.time_of_event = (Time.now.to_f*1000).to_i
      else
        if check_neighbour_ep(0)
          state = state.dup
          state.state = AlarmSeverity::DEPENDENCY
          #state.time_of_event = (Time.now.to_f*1000).to_i
        end
      end
    end  
    return state
  end

  def alarm_state  
    state = super
    if is_monitored
       cos_end_points.each {|cep| state = cep.event_record + state }
    end
   
    # Skip this if the object is a OnNetOvcEndPointEnni or If the state is currently in Mtc, N/A or Not monitored no need to evaluate state
    # Checking for OnNetOvcEndPointEnni is not clean - The following code applies to Unis and Memeber ENNI endpoints. We would need to change the
    # class hierarchy to clean this up...
    if self.is_a?(OnNetOvcEndPointEnni)      
      return state
    end
  
    if segment.is_in_ls_core_transport_path
      state = sm_state_core_ls(state)      
      return state
    end
  
    if is_monitored
      # Which COS to we pick ???? any will do
      if cos_end_points.size != 0        
        cos_end_points_to_cenx = cos_end_points.first.get_remote_sibling_cos_end_points
        # Get the current state of each endpoint
        # Also find any endpoint that was Unavailable in the last period (1 minute for Brix but need to add 30 seconds as there is some
        # delay for Brix SNMP handling - TODO Need to remove the hardcoding at some point) as this will also cause an Dependency alarm
        cos_end_points_to_cenx.collect! do |cp|           
          tmp = {:oper => cp.segment_end_point.event_record, :prov => cp.segment_end_point.mtc_state}
          tmp[:oper_period] = tmp[:oper]
          tmp_ah = cp.segment_end_point.alarm_history.subset((Time.now.to_i-90)*1000, Time.now.to_i*1000)        
          last_period_alarms = tmp_ah.alarm_records.detect {|ar| ar.state == AlarmSeverity::UNAVAILABLE} if tmp_ah.alarm_records.size != 0
          #last_period_alarms = cp.segment_end_point.sm_histories.first.alarm_records.find(:first, :order => "time_of_event", :conditions => ["time_of_event >= ? AND state = ?", (Time.now-90.seconds).to_i * 1000, AlarmSeverity::UNAVAILABLE.to_s])
          if last_period_alarms != nil
            tmp[:oper_period] = last_period_alarms
          else
            tmp[:oper_period] = tmp[:oper]              
          end
          tmp
        end        
      
        if cos_end_points_to_cenx.any? {|ar| ar[:prov].state == AlarmSeverity::MTC || ar[:oper_period].state == AlarmSeverity::UNAVAILABLE}
          # If any endpoint is UA or MTC then the member endpoint is Dependency unless the Birx returns Ok          
          if state.state != AlarmSeverity::OK
            state = state.dup
            state.state = AlarmSeverity::DEPENDENCY            
            return state
          else            
            return state
          end
        end

        # if any endpoints are Dependency then mark the current endpoint as Dependency if it has any faults or
        # just update the time if they are Ok 
        if cos_end_points_to_cenx.any? {|ar| ar[:oper].state == AlarmSeverity::DEPENDENCY}          
          if state.state != AlarmSeverity::OK
            state = state.dup
            state.state = AlarmSeverity::DEPENDENCY            
            return state
          else            
            return state
          end
        end
      else
        # There are no COS end points nothing else to do
      end
    end    
    return state
  end


  def get_alarm_keys
    return_code = true
    new_mep_alarms = []
    
    if segment.is_in_ls_core_transport_path 
      # MEP alarms not associated with a SAP LightSquared Core case
      # Get the SAM MEP object name associated with this SAP
      new_mep_alarms = []
      segment_end_points.each do |ep|
        if ep.is_a?(OnNetOvcEndPointEnni)
          return_code, mep_alarms = ep.get_mep_alarm_keys([0])
          if !return_code
            SW_ERR "#{self.class}:#{self.id} Failed to get MEP alarms from adjcent Cenx endpoint(#{ep.class}:#{ep.id}"
            break
          end
          new_mep_alarms += mep_alarms
        end 
      end  
    end
    
    return return_code, new_mep_alarms
  end
  
  def go_in_service
    sm_histories << SmHistory.create(:event_filter => "SM #{self.class.name}:#{cenx_id}") if sm_histories.empty?
    
    return_code = true
    info = ""
    if segment.is_in_ls_core_transport_path 
      return_code, new_mep_alarms = get_alarm_keys
      if return_code
        mep_mapping = AlarmSeverity.default_alu_alarm_mapping
        mep_mapping["major"] = AlarmSeverity::WARNING 
        new_alarms = {} 
        new_mep_alarms.each {|filter| new_alarms[filter] = {:type => AlarmHistory, :mapping => mep_mapping}}
        return_code = update_alarm_history_set(new_alarms)
      end
    end
    
    if return_code
      return_code = cos_end_points.all? {|cep|
        cep_result = cep.go_in_service
        info << cep_result[:info]
        cep_result[:result]
      }
    end
    
    info = "Failed to initialise" if !return_code && info.blank?
    return {:result => return_code, :info => info}
  end
  
  def monitored?
    # If any endpoint Off Net OVC endpoint is monitored then all endpoints are monitored
    segment.segment_end_points.any? {|ep| ep.is_monitored}
  end
  
  def is_connected_to_test_port
    return false
  end

  def is_monitored_by_exfo
    return cos_test_vectors.size > 0
  end

  def is_connected_to_another_endpoint?
    return !segment_end_points.empty?
  end

  def is_connected_to_monitored_endpoint?

    segment_end_points.each do |e|
      return true if e.is_monitored
    end

    return false

  end
  
  def get_network_manager
    return segment.get_network_manager
  end

  def generate_service_monitoring_config
    text = ""
    text += "#------------------------------------------------\n"
    text += "# Configuration is required on BrixWorks\n"
    text += "#------------------------------------------------\n"
    brix_instance = "ERROR-CHECK SWERRs"
    url = nil
    manager = get_monitoring_manager
    if manager
      url = " at #{manager.url}"
      brix_instance = manager.name
    end
    text += "# Log into BrixWorx Instance: #{brix_instance}#{url}\n"
    text += "- uname: brix / passwd: brix \n"
    text += "- From sidebar choose 'SLAs'\n"
    text += "- Choose 'Add an SLA'\n"
    text += "# Add an SLA with following Parameters\n"
    #text += "# Customer Name:\n#{cenx_name}\n"
    text += "# Customer Name:\n#{bx_sla_name}\n"
    text += "# Account Number:\n#{bx_act_name}\n"
    text += "# Description:\nMonitored Segment: #{segment.cenx_id}\nMonitored Segment Endpoint #{cenx_id}\nMonitored Device: #{demarc_cenx_id} (#{demarc_cenx_name})\n"
    text += "# Sharable:\nno\n"

    result, result_str, cenx_monitoring_segment_end_point = get_monitoring_segment_end_point
    if !result || !cenx_monitoring_segment_end_point
      SW_ERR "#{result_str}"
    end

    verifier_name = "#{cenx_monitoring_segment_end_point ? cenx_monitoring_segment_end_point.get_monitoring_node.name : "UNKNOWN"}"

    text += "# Associated Verifiers\n#{verifier_name}\n- Click Add\n"

    i = 1
    cos_test_vectors.each do |ctv|
      text += "#------------------------------------------------\n"
      text += "#Guaranteed Service #{i}:\n"
      text += "#------------------------------------------------\n"
      text += ctv.generate_config
      i+=1
    end

    text += "- Click 'submit this Customer SLA'\n"
    return text
  end

  def get_monitoring_manager
    manager = nil
    manager = cos_test_vectors.first.get_monitoring_manager if !cos_test_vectors.empty?
    if !manager
      SW_ERR "get_monitoring_manager failed on segmentep: #{cenx_name}"
    end
    return manager

  end

  def update_alarm(list_of_evaled_objects = [], full_eval = false)
    changed = super
    segment.eval_alarms(list_of_evaled_objects, full_eval) if changed
    return changed
  end

  private

  def get_monitoring_segment_end_point
    cenx_monitoring_segment_end_point = nil
    result = false
    result_str = ""

    enni_segment_end_points = segment.ovc_end_point_ennis.reject{|e| e == self}
      
    if enni_segment_end_points.size > 1
      # Never expect > 1 in a Off Net OVC
      # TODO not sure about On Net OVC case
      result_str = "ERROR - too many ENNI Endpoints found (#{enni_segment_end_points.size})"
      result = false
      return result, result_str, cenx_monitoring_segment_end_point
    end

    if enni_segment_end_points.size == 0
      result_str = "TBD - no Member endpoint at ENNI"
      result = true #not an error
      return result, result_str, cenx_monitoring_segment_end_point
    end

    # One endpoint found
    enni_segment_end_point = enni_segment_end_points.first
    #TODO figure out correct way to do this. Used to be only one end point
    cenx_enni_segment_end_point = enni_segment_end_point.segment_end_points.first

    if (!cenx_enni_segment_end_point)
      result_str = "TBD - no Cenx endpoint at ENNI"
      result = true #not an error
      return result, result_str, cenx_monitoring_segment_end_point
    end

    if (!cenx_enni_segment_end_point.kind_of? OnNetOvcEndPointEnni)
      result_str = "ERROR - wrong type of Cenx endpoint at ENNI"
      result = false
      return result, result_str, cenx_monitoring_segment_end_point
    end

    cenx_monitoring_segment_end_points = cenx_enni_segment_end_point.get_cenx_monitoring_segment_endpoints

    if cenx_monitoring_segment_end_points.size > 1
      # Never expect > 1
      result_str = "ERROR - wrong number of Cenx Monitoring Endpoints found (#{cenx_monitoring_segment_end_points.size})"
      result = false
      return result, result_str, cenx_monitoring_segment_end_point
    end

    if cenx_monitoring_segment_end_points.size == 0
      result_str = "TBD - no Cenx Monitoring Endpoint Found"
      result = true #not an error
      return result, result_str, cenx_monitoring_segment_end_point
    end

    # One endpoint found
    cenx_monitoring_segment_end_point = cenx_monitoring_segment_end_points.first
    result = true
    return result, result_str, cenx_monitoring_segment_end_point

  end

  # Generate a new diagram for the Path each time an endpoint is saved
  # This will automate diagram generation.  Diagrams are used in the Path List of SIN
  def generate_diagram
    # find all paths that the cooresponding Segment is a part of.
    segment.paths.each do |path|
      path = Path.find(path.id)
      path.layout_diagram
    end
  end
  
  def assoc_service_providers_for_indexing
    
    # Use after_save because endpoints can be created without a demarc attached, it is linked later
    # populate Sphinx Access Point so we have uniqure rows for each member handle (which sphinx requires)
    # TODO: Revisit this should be after_save maybe
    segment.paths.each do |path|
     path.assoc_service_providers_for_indexing_paths
    end

    # Ennis have their own sphinx index
    enni.try(:assoc_service_providers_for_indexing)
    
  end
  
  def monitored_changed
    eval_alarms([], true)
  end
  
end
