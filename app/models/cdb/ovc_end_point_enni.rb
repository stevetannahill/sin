# == Schema Information
#
# Table name: segment_end_points
#
#  id                    :integer(4)      not null, primary key
#  type                  :string(255)
#  cenx_id               :string(255)
#  segment_id            :integer(4)
#  demarc_id             :integer(4)
#  segment_end_point_type_id :integer(4)
#  md_format             :string(255)
#  md_level              :string(255)
#  md_name_ieee          :string(255)
#  ma_format             :string(255)
#  ma_name               :string(255)
#  is_monitored          :boolean(1)
#  notes                 :string(255)
#  stags                 :string(255)
#  ctags                 :string(255)
#  oper_state            :string(255)
#  created_at            :datetime
#  updated_at            :datetime
#  eth_cfm_configured    :boolean(1)      default(TRUE)
#  reflection_enabled    :boolean(1)
#  mep_id                :string(255)     default("TBD")
#  event_record_id       :integer(4)
#  sm_state              :string(255)
#  sm_details            :string(255)
#  sm_timestamp          :integer(8)
#  prov_name             :string(255)
#  prov_notes            :text
#  prov_timestamp        :integer(8)
#  order_name            :string(255)
#  order_notes           :text
#  order_timestamp       :integer(8)
#


class OvcEndPointEnni < OvcEndPoint

  #validates_class_of :segment, :is_a => "OffNetOvc"
  validates_class_of :demarc, :is_a => "EnniNew", :allow_nil => true
  validate :valid_tag_presence?

  validates_format_of :segment_endpoint_type_name, :with => /(^ENNI.*)/, :message => "Segment ENNI Endpoint Type must be an ENNI Endpoint"
  
  after_save :assoc_service_providers_for_indexing_entities
  
  def assoc_service_providers_for_indexing_entities
    enni.try(:assoc_service_providers_for_indexing_ennis)
  end
  
  def is_connected_to_enni
    return  demarc_id != nil
  end

  def port_encap_type_onovce
    if is_connected_to_enni
	    return "#{demarc.port_enap_type}"
	  else
      return "TBD"
    end
  end

  def is_qinq_encap
    return port_encap_type_onovce == 'QINQ' ? true : false
  end

  def is_null_encap
    return port_encap_type_onovce == 'NULL' ? true : false
  end

  def sap_encap_string
    if port_encap_type_onovce == 'DOT1Q' || port_encap_type_onovce == 'NULL'
      return ":#{stags}"
    elsif port_encap_type_onovce == 'QINQ'
      return ":#{stags}.#{ctags}"
    else
      return "TBD"
    end
  end

  def get_candidate_segment_end_point_types
    return ethernet_service_type ? ethernet_service_type.segment_end_point_types.select{|t| t.name =~ /^ENNI/} : []
  end
  
  def class_get_candidate_endpoints_to_link_with
    base_get_candidate_endpoints_to_link_with.reject{|e| e.class != OnNetOvcEndPointEnni }
  end

  def nodes_involved
    nodes = []
    if is_connected_to_enni
      nodes = demarc.ports.map{|port| port.node}
    end
    return nodes.flatten.uniq
  end

  def node_names

    nodes = nodes_involved
    names = nodes.map{|n| n.name}

    if names.empty?
      names << "None"
    end

    return names

#    nodes = []
#    if is_connected_to_enni
#      demarc.ports.each do |port|
#        if !nodes.find_index(port.node.name)
#          nodes << "#{port.node.name}"
#        end
#      end
#    else
#      nodes << "TBD"
#    end
#    return nodes
  end

  def node_info
    return node_names.join("; ")
  end

  def outer_tag_value
    if segment_end_points.empty?
      if !stags
        return "TBD - not yet connected to cenx Endpoint"
      else
        ## Hack
        return stags
      end
    elsif segment.is_in_tunnel_path
      return "*"
    else
      segment_end_points.map{|segmentep| segmentep.outer_tag_value}.uniq.join(";")
    end
  end

  def get_candidate_demarcs
    get_candidate_ennis
  end

  def enni
    return demarc
  end

  protected

   def get_candidate_ennis
     candidate_member_ennis = segment.get_operator_network.demarcs.reject{|d| (segment.is_connected_to_demarc d) || !(d.kind_of? EnniNew)}
     #member_ennis = segment.get_operator_network.demarcs.reject{|d| !(d.kind_of? EnniNew)}
     #member_ennis = segment.get_operator_network.demarcs

     candidate_member_ennis +=  Demarc.find_member_ennis
     candidate_member_ennis.flatten.uniq

     #Allow connection to cenx owned ENNIs associated 
     candidate_cenx_ennis = Demarc.find_cenx_nm_ennis

     candidate_ennis = candidate_member_ennis + candidate_cenx_ennis
     return candidate_ennis.flatten.uniq

   end

   def valid_tag_presence?
     unless (ctags.nil? or ctags.to_s.empty?) and (stags.nil? or stags.to_s.empty?)
       errors.add(:base,"ctags and stags must be empty for Segment Endpoint ENNIs")
     end
   end

end
