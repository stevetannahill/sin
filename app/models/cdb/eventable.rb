module Eventable
 
  SM_ERROR = 'Internal Error'
  OUT_OF_SERVICE = 'Out of Service'
  MTC = AlarmSeverity::MTC
  NOT_MON = AlarmSeverity::NOT_MONITORED
  OK = AlarmSeverity::OK
  WARN = AlarmSeverity::WARNING
  FAILED = AlarmSeverity::FAILED
  DEPENDENCY = AlarmSeverity::DEPENDENCY
  UNAVAILABLE = AlarmSeverity::UNAVAILABLE
  NOT_LIVE = AlarmSeverity::NOT_LIVE
 
  def self.included(base)
    base.has_many :alarm_histories, :through => :event_ownerships, :source => :event_history
    base.has_many :event_ownerships, :as => :eventable, :dependent => :destroy
    base.has_many :mtc_periods, :through => :mtc_ownerships, :source => :mtc_history, :dependent => :destroy
    base.has_many :mtc_ownerships, :as => :mtcable, :dependent => :destroy
    base.has_many :sm_histories, :through => :sm_ownerships, :source => :sm_history, :dependent => :destroy
    base.has_many :sm_ownerships, :as => :smable, :dependent => :destroy
    base.belongs_to :event_record, :dependent => :destroy
  end
  
  # Sync the latest alarm
  def alarm_sync(timestamp = nil)
    alarm_histories.all? {|ah| ah.alarm_sync(timestamp)}
  end
  
  # sync the entire history ie the past
  def alarm_sync_past(timestamp = nil)
    alarm_histories.all? {|ah| ah.alarm_sync_past(timestamp)}
  end
  
  def eval_alarms(list_of_evaled_objects = [], full_eval = false)
    if list_of_evaled_objects.include?("#{self.class} #{self.id}")
      return
    else
      list_of_evaled_objects << "#{self.class} #{self.id}"
    end
    reload
    update_alarm(list_of_evaled_objects, full_eval)
  end
  
  # return true if the state has changed otherwise false
  def update_alarm(list_of_evaled_objects = [], full_eval = false)
    new_alarm = alarm_state
    if !full_eval
      return false if new_alarm.state == event_record.state && new_alarm.details == event_record.details
    end   
      
    event_record.update_attributes({:state => new_alarm.state, :time_of_event => new_alarm.time_of_event, :details => new_alarm.details, :original_state => new_alarm.original_state})
    update_sm    
    return true
  end
  
  def alarm_history  
    history = AlarmHistory.merge(alarm_histories) {|events| AlarmRecord.highest(events)}   
    return history    
  end    
  
  def alarm_state
    state = AlarmRecord.highest(alarm_histories.collect {|ah| ah.alarm_state})
    # If there are no histories then return OK
    state = AlarmRecord.new(:time_of_event => 0, :state => AlarmSeverity::OK, :details => "", :original_state => "cleared") if state == nil
    return state
  end  
  
  def mtc_history
    history = MtcHistory.merge(mtc_periods) {|events| AlarmRecord.highest(events)}       
    return history    
  end    
  
  def mtc_state
    state = AlarmRecord.highest(mtc_periods.collect {|ah| ah.alarm_state})
    state = AlarmRecord.new(:time_of_event => 0, :state => AlarmSeverity::OK, :details => "", :original_state => "cleared") if state == nil
    return state
  end  
  
  def set_mtc(in_mtc, details = "", timestamp = (Time.now.to_f*1000).to_i)
    if mtc_periods.size != 0
      # only add a record if it's changed
      if ((self.mtc_state.state == AlarmSeverity::MTC) != in_mtc) || mtc_periods.first.alarm_records.size == 0
        # Set the details if none given
        if details.blank?
          details = in_mtc ? "In Maintenance" : ""
        end
        mtc_periods.first.add_event(timestamp, in_mtc ? "maintenance": "cleared", details)
      end  
    end
  end
  
  # Creates or Deletes any needed alarm histories given the list and what it's histories are already created
  # alarm_history_set = {"filter" => {:type => AlarmHistory|BrixAlarmHistory, :mapping => bbbbb}}
  def update_alarm_history_set(alarm_history_set)
    return_value = true
    current_alarms = alarm_histories.collect{|ah| ah.event_filter}
    new_alarms = alarm_history_set.keys
    alarms_to_add = (new_alarms) - current_alarms
    alarms_to_delete = current_alarms - (new_alarms)
    alarms_to_add.each do |alarm_name|
      if alarm_history_set[alarm_name][:type].find_by_event_filter(alarm_name) == nil
        ah = alarm_history_set[alarm_name][:type].create(:event_filter => alarm_name, :alarm_mapping => alarm_history_set[alarm_name][:mapping])
        self.alarm_histories << ah        
        if !ah.alarm_sync
          # Failed to sync so delete the alarm history so on next call to go_in_service
          # the alarm history will be created again and a sync attempted
          return_value = false
          ah.destroy
        end
      else
        # An alarm history already exists so just link to it
        ah = alarm_history_set[alarm_name][:type].find_by_event_filter(alarm_name)
        self.alarm_histories << ah
        # No need to sync as it should be up-to-date as it already exists
      end
    end

    alarms_to_delete.each do |alarm_name|
      ah = alarm_histories.find_by_event_filter(alarm_name)
      ah.destroy if ah != nil
    end
    
    return return_value
  end

  def monitored?
    return true
  end

  def get_SM_state
    AlarmRecord.new(:time_of_event => sm_timestamp, :state => AlarmSeverity.new(sm_state), :details => sm_details)
  end

  def sm_update
    smh = sm_histories.first
    if smh == nil
      return false, false,
         AlarmRecord.new(:state => AlarmSeverity::NOT_LIVE, :details => "N/A", :time_of_event => (Time.now.to_f*1000).to_i, :original_state => "not_live"),
         AlarmRecord.new(:state => AlarmSeverity::NOT_LIVE, :details => "N/A", :time_of_event => (Time.now.to_f*1000).to_i, :original_state => "not_live") 
    end
    state = sm_state_calc
    previous_state = get_SM_state
    state_updated = previous_state == nil || state.state != previous_state.state    
    updated = previous_state == nil || (state_updated || state.details != previous_state.details)
    
    # The original state can be from Brix or ALU etc but this is now tranformed into a SM state
    # so pick the orginal state from the SM mappings        
    state.original_state = smh.alarm_mapping.find {|k,v| v == (state.state.to_s)}[0]    
    #Rails.logger.debug "sm_update #{self.class} #{self.id} #{updated} #{state.inspect} #{previous_state.inspect}"
    smh.add_event(state.time_of_event, state.original_state, state.details) if updated    
    reload    
    return updated, state_updated, previous_state, state
  end
  
  # For display purposes
  def get_SM_state_formatted
    return "#{sm_state}:#{sm_details}" 
  end
  
  def event_record_create
    if event_record_id == nil 
      er = AlarmRecord.create(:state => AlarmSeverity::OK, :time_of_event => Time.now.to_f*1000, :original_state => "cleared", :details => "", :event_history_id => nil)
      er.save
      update_attribute(:event_record_id, er.id)      
    end
  end
  
  def update_sm(list_of_evaled_objects = [])
    updated, state_updated, previous_oper_state, oper_state = sm_update
    if updated
      members = []
      members = effected_members if respond_to?(:effected_members)
      Rails.logger.info "#{Time.now} Notify #{self.class} #{self.id} OperState = #{oper_state.state.to_s}" 
      puts "Notify operTS #{Time.at(oper_state.time_of_event/1000)} #{self.class} #{self.id} OperState = #{oper_state.state.to_s} OperDetails #{oper_state.details}" if $debug
      EventClient.instance.post_event(members,nil, self.class.to_s, self.id, oper_state.time_of_event/1000, oper_state.state.to_s, oper_state.details)
    end
    if state_updated && EmailConfig.instance.enable_fault_email
      generate_ticket(oper_state, previous_oper_state)
    end
  end
  
  
  private
  
  def sm_state_calc    
    details = ""
    timestamp = (Time.now.to_f*1000).to_i
    state = AlarmSeverity::NOT_LIVE
    original_state = "not_live"
    
    ps = get_prov_state
    if [ProvReady, ProvLive, ProvMaintenance].any? {|klass| ps.is_a?(klass)}
      if monitored?        
        if ps.is_a?(ProvMaintenance)
          obj_state = AlarmRecord.new(:state => AlarmSeverity::MTC, :details => "In Maintenace", :time_of_event => (Time.now.to_f*1000).to_i, :original_state => "maintenance")
        else
          obj_state = event_record
        end
        
        # set the timestamp to "Now" - the SM history is the state of the object at the current time. If we did not have this then if
        # one AH has a warning at T1 and Failed at T2 for a different AH then the state will T1, T2, T1(when Failed clears).
        # The disadvantage of this is that the real alarm time can be lost for nested alarms.. 
        # The if below is tempoary for sprint Demo - it should be just obj_state.time_of_event = (Time.now.to_f*1000).to_i 
        # Temp Code
        #puts "BJW sm_state_calc #{self.class.to_s}:#{self.id} #{Time.at(obj_state.time_of_event/1000)} #{Time.at(sm_timestamp/1000)}"
        if obj_state.time_of_event < sm_timestamp
          #obj_state.time_of_event = (Time.now.to_f*1000).to_i
          obj_state.time_of_event = sm_timestamp+1
        end
        # Temp Code end
        # Real code
        #obj_state.time_of_event = (Time.now.to_f*1000).to_i  
        # Real code end      
      else
        obj_state = AlarmRecord.new(:state => AlarmSeverity::NOT_MONITORED, :details => "Not monitored", :time_of_event => (Time.now.to_f*1000).to_i, :original_state => "not_monitored")
        obj_state = obj_state + mtc_state
        #puts "BJW sm_state_calc Not M #{self.class.to_s}:#{self.id} #{Time.at(obj_state.time_of_event/1000)} #{Time.at(sm_timestamp/1000)}"
        
        obj_state.time_of_event = [obj_state.time_of_event, mtc_state.time_of_event].max
        
      end
      state = obj_state.state
      original_state = obj_state.original_state
      details = obj_state.details
      timestamp = obj_state.time_of_event
      #puts "BJW sm_state_calc1 #{self.class.to_s}:#{self.id} #{Time.at(obj_state.time_of_event/1000)} #{Time.at(timestamp/1000)}"
      
      timestamp = (Time.now.to_f*1000).to_i if timestamp == 0
      #Rails.logger.debug "sm_state #{self.class} #{self.id} #{state} #{details} #{timestamp} #{original_state}"      
    end    
    return AlarmRecord.new(:state => state, :details => details, :time_of_event => timestamp, :original_state => original_state)
  end
    
  def generate_ticket(oper_state, previous_oper_state)    
    # Only generate a ticket if it's a Path or ENNI state change and the state has gone from Ok to any failed state
    if (self.is_a?(Path) || self.is_a?(EnniNew)) && (previous_oper_state.state <= AlarmSeverity::OK && oper_state.state >= AlarmSeverity::WARNING) && get_prov_state.is_a?(ProvLive)
       begin
         if self.is_a?(Path)
           CdbMailer.path_fault_ticket(self).deliver
         elsif self.is_a?(EnniNew)
           CdbMailer.enni_new_fault_ticket(self).deliver
         end
       rescue 
         err = "Exception sending mail for #{self.class}:#{self.id}: Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}"
         Rails.logger.error err; SW_ERR err
       end
    end
  end 
  
end
