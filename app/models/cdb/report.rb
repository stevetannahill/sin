class Report < ActiveRecord::Base
  belongs_to :schedule
  has_one :scheduled_task, :through => :schedule
  validates :schedule, :presence => true
  
  before_create :general_after_create

  def general_after_create
    self.run_date = Time.zone.now
    
    self.report_period_start, self.report_period_end = get_report_period(self.run_date)
    if self.schedule != nil
      self.schedule.last_run = self.run_date
      self.schedule.save
    end
    true
  end
  
  def self.generate_report(schedule_id)
    SW_ERR "generate_report should be overridden by subclasses"
  end
  
  def update_report
    SW_ERR "update_report should be overridden by subclasses"
  end

  def self.generate_exception_report(schedule_id, exception_finder, exception_stats_finder=nil)
    scheduled_report = Schedule.find(schedule_id)
    user = scheduled_report.scheduled_task.user
    path_owner = user.system_admin? ? 'admin' : 'all'
    params = scheduled_report.scheduled_task.filter_options
    date_range_start, date_range_end = get_date_range(params['date_range_selector'], params['date_range_start'], params['date_range_end'])
    
    paths = Path.find_paths(path_owner, user).s_unique
    exceptions = send(exception_finder, paths, params, date_range_start, date_range_end, user)
    exception_stats = send(exception_stats_finder, paths, params, date_range_start, date_range_end, user) if exception_stats_finder
    
    if exception_stats
      report = self.create(:clear_exceptions => exception_stats.s_clear.count, :major_exceptions => exception_stats.s_major.count, :minor_exceptions => exception_stats.s_minor.count, :critical_exceptions => exception_stats.s_critical.count)
    elsif [:sla_exceptions, :metrics_exceptions, :cell_site_path_exceptions, :cell_site_exceptions, :satellite_site_exceptions, :site_group_exceptions].include?(exception_finder)
      report = self.create(:clear_exceptions => exceptions.s_clear.s_count, :major_exceptions => exceptions.s_major.s_count, :minor_exceptions => exceptions.s_minor.s_count, :critical_exceptions => exceptions.s_critical.s_count)
    else
      report = self.create(:clear_exceptions => exceptions.s_clear.count, :major_exceptions => exceptions.s_major.count, :minor_exceptions => exceptions.s_minor.count, :critical_exceptions => exceptions.s_critical.count)
    end
    
    scheduled_report.reports << report
    
    if scheduled_report.email_distribution
      CdbMailer.scheduled_report(report, exceptions, exception_finder, exception_stats).deliver
    end
    
    report
  end
  
  private
  
  def get_report_period(t)
    period = case schedule.trigger
    when "monthly"
      t1 = t-1.month
      [Time.zone.local(t1.year, t1.month, t1.day),  Time.zone.local(t.year, t.month, t.day)] 
    when "weekly"
        t1 = t-1.week
        [Time.zone.local(t1.year, t1.month, t1.day),  Time.zone.local(t.year, t.month, t.day)]
    when "daily"
      t1 = t-1.day
      [Time.zone.local(t1.year, t1.month, t1.day),  Time.zone.local(t.year, t.month, t.day)]
    when "yearly"
      t1 = t-1.year
      [Time.zone.local(t1.year, t1.month, t.day),  Time.zone.local(t.year, t.month, t.day)]
    else
      SW_ERR "Invalid trigger #{trigger}"
      [nil, nil]
    end
    return period
  end

end
# == Schema Information
#
# Table name: reports
#
#  id                  :integer(4)      not null, primary key
#  type                :string(255)
#  schedule_id         :integer(4)
#  run_date            :datetime
#  report_period_start :datetime
#  report_period_end   :datetime
#  minor_exceptions    :integer(4)
#  major_exceptions    :integer(4)
#  critical_exceptions :integer(4)
#  created_at          :datetime        not null
#  updated_at          :datetime        not null
#

