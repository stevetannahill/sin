class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user permission to do.
    # If you pass :manage it will apply to every action. Other common actions here are
    # :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. If you pass
    # :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities
    
    @user = user || User.new # for guest
    @user.privileges.each { |p| send(p.name.parameterize('_')) }

    # All users
    can [:update, :set_homepage, :toggle_table], User, :id => @user.id
    can :read, ServiceProvider, :id => @user.service_provider.id if @user.service_provider_id
    can :read, ServiceProvider if @user.service_provider_id && @user.service_provider.is_system_owner?

    # Tenants cannot see service provider info
    if App.is?(:coresite)
      cannot :read, ServiceProvider unless @user.service_provider_id && @user.service_provider.is_system_owner?
    end

    # Admin Users
    if @user.system_admin?
      # can :manage, :all
      can :manage, :admin_user
      can :manage, ScheduledTask
      ordering_admin
    end
  end
  
  def user_management
    can [:read, :create, :update, :destroy], User
    can :manage, Role
    can :assign_role, User
    can :assign_service_provider, User if @user.system_admin?
    can :assign_network_types, User if @user.system_admin?    
    can :assign_system_admin, User if @user.system_admin?
    can :read, ServiceProvider if @user.system_admin?
  end
  
  def network
    can :show, :network_menu
    can :read, :network
  end
  
  def inventory
    show_common_menu
    can :read, :inventory
    can :read, :inventory_dashboard
    can :read, :inventory_circuit_list
    can :read, :inventory_enni_list
  end
  
  def monitoring
    show_common_menu
    can :read, :monitoring
    can :read, :monitoring_dashboard    
    can :read, :monitoring_circuit_list
    can :read, :monitoring_enni_list    
  end
  
  def ordering
    show_common_menu
    can :read, :ordering
    can :read, :ordering_dashboard    
    can :read, :ordering_circuit_list
    can :read, :ordering_enni_list    
  end

  def ordering_admin
    can :submit, :order
    can :move,   :order
  end

  def edit_data
    can :update, Path
    # Coresite can't manage (create, update, delete) orders even if they have the permission set
    can :manage, :coresite_order unless @user.service_provider.is_system_owner?
  end

  def customize_system
    can :update, ServiceProvider
    can :manage, :customize_system
    can :manage, Settings
  end

  def sla_reports
    common_reports
    can :read, :sla_reports
  end

  def inventory_exception_reports
    common_reports
    can :read, :inventory_exception_reports
  end

  def ordering_exception_reports
    common_reports
    can :read, :ordering_exception_reports
  end

  def rolled_up_reports
    common_reports
    can :read, :rolled_up_reports
  end

  def circuit_utilization_reports
    common_reports
    can :read, :circuit_utilization_reports    
  end

  def enni_utilization_reports
    common_reports
    can :read, :enni_utilization_reports
  end

  def scheduled_reports
    common_reports
    can :manage, Schedule
  end

  def common_reports
    can :show, :reports_menu
    can :manage, SiteList, :user_id => @user.id
    can :manage, ScheduledTask, :user_id => @user.id
  end
  
  def show_common_menu
    can :show, :dashboards_menu
    can :show, :circuit_list_menu
    can :show, :enni_list_menu
  end
end
