# == Schema Information
# Schema version: 20110802190849
#
# Table name: order_annotations
#
#  id               :integer(4)      not null, primary key
#  date             :datetime
#  note             :text
#  severity         :string(255)
#  service_order_id :integer(4)
#  created_at       :datetime
#  updated_at       :datetime
#

class OrderAnnotation < ActiveRecord::Base
  belongs_to :service_order
  
  def short_title
    return note[0..25]
  end
end
