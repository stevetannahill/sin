class MarketQuery

  attr_accessor :query
  
  @@default_query = "select 0 as num"
  @@host_lookup = { "hayden" => :market_hayden, "smeagol" => :market_dev, "market-eval" => :market_eval, "market-demo" => :market_demo, "market" => :market_live, "local" => :market_local }

  @@action_to_available_contexts = 
  {
    "buildings" => [ "diverse","install_time","latitude","longitude","media","network","cenx_member", "address","postal_code","created_at","updated_at","city","state","country","address2","address_type" ],
    "regions" => [ "region_name","private","drawn", "created_at","updated_at", "cenx_member" ],
    "addresses" => [ "address","postal_code","longitude","latitude","created_at","updated_at","city","state","country","address2","address_type" ],
    "users" => ["email", "first_name", "last_name", "last_login_at", "created_at","updated_at","cenx_member"],
    "market-sharings" => [ "seller","buyer","show_building_lists","show_regions","show_location_details","show_service_types","show_service_guarantees" ],
    "members" => [ "cenx_member" ],
    "projects" => [ "cenx_member", "email", "first_name", "last_name", "created_at","updated_at"]
  }

  @@contexts_to_table_column = 
  {
    "cenx_member" => "service_providers.name",
    "address" => "physical_addresses.address",
    "postal_code" => "physical_addresses.postal_code",
    "city" => "physical_addresses.city",
    "state" => "physical_addresses.state",
    "country" => "physical_addresses.country",
    "address2" => "physical_addresses.address2",
    "address_type" => "physical_addresses.address_type",
    "diverse" => "operator_accesses.is_physically_diverse",
    "network" => "operator_networks.name",
    "media" => "media_types.name",
    "seller" => "sellers.name",
    "buyer" => "buyers.name",
    "show_building_lists" => "operator_security_profiles.served_locations_visible",
    "show_regions" => "operator_security_profiles.served_regions_visible",
    "show_location_details" => "operator_security_profiles.operator_accesses_visible",
    "show_service_types" => "operator_security_profiles.service_types_visible",
    "show_service_guarantees" => "operator_security_profiles.service_guarantees_visible",
    "region_name" => "regions.name",
    "email" => "users.email",
    "first_name" => "users.first_name",
    "last_name" => "users.last_name"
  }
  
  @@action_to_table_name = {
    "buildings" => "operator_accesses", 
    "addresses" => "physical_addresses",
    "users" => "users",
    "members" => "service_providers",
    "market-sharings" => "operator_security_profiles"
  }
  
  @@boolean_contexts = [ 
      "show_building_lists","show_regions","show_location_details","show_service_types","show_service_guarantees",
      "private","drawn",
      "sharing"
  ]

  def self.go(query_text,method = :find)
    market_query = MarketQuery.new
    market_query.query = Appstats::Query.new(:query => query_text, :query_type => "MarketQuery")
    market_query.send(method)
  end

  def query_to_sql
    return @@default_query if query.nil?
    query.query_to_sql
  end
  
  def group_query_to_sql
    return nil if query.nil?
    query.group_query_to_sql
  end

  def process_query
    return if query.nil?
    query.query_to_sql = @@default_query
    query.group_query_to_sql = nil
    return if query.action.blank?

    action = query.action.pluralize.downcase
    case action
      when "buildings"
        count_filter = "COUNT(DISTINCT operator_accesses.id)"
        query.query_to_sql = "select #{count_filter} as num from operator_accesses left join physical_addresses on physical_addresses.id = operator_accesses.physical_address_id left join operator_networks on operator_networks.id = operator_accesses.operator_network_id left join service_providers on service_providers.id = operator_networks.service_provider_id#{build_where_clause}"
      when "regions"
        count_filter = "COUNT(DISTINCT regions.id)"
        query.query_to_sql = "select #{count_filter} as num from regions left join service_providers on service_providers.id = regions.service_provider_id#{build_where_clause}"
      when "addresses"
        count_filter = "COUNT(DISTINCT physical_addresses.id)"
        query.query_to_sql = "select #{count_filter} as num from physical_addresses left join operator_accesses on operator_accesses.physical_address_id = physical_addresses.id left join operator_networks on operator_networks.id = operator_accesses.operator_network_id left join service_providers on service_providers.id = operator_networks.service_provider_id#{build_where_clause}"
      when "users"
        count_filter = "COUNT(DISTINCT users.id)"
        query.query_to_sql = "select #{count_filter} as num from users left join service_providers on service_providers.id = users.service_provider_id#{build_where_clause}"
      when "members"
        count_filter = "COUNT(DISTINCT service_providers.id)"
        query.query_to_sql = "select #{count_filter} as num from service_providers#{build_where_clause}"
      when "projects"
        count_filter = "COUNT(DISTINCT projects.id)"
        query.query_to_sql = "select #{count_filter} as num from projects left join users on users.id = projects.user_id left join service_providers on service_providers.id = users.service_provider_id#{build_where_clause}"
      when "market-sharings"
        count_filter = "COUNT(DISTINCT operator_security_profiles.id)"
        query.query_to_sql = "select #{count_filter} as num from operator_security_profiles left join service_providers sellers on sellers.id = operator_security_profiles.owning_service_provider_id left join service_providers buyers on buyers.id = operator_security_profiles.applies_to_service_provider_id#{build_where_clause}"
    end
    query.group_query_to_sql = query.query_to_sql.sub("#{count_filter} as num","#{context_key_filter_name(action)} as context_key_filter, #{context_value_filter_name(action)} as context_value_filter, COUNT(*) as num") + " group by context_value_filter" unless query.group_by.empty?
  end
  
  def run
    query.run
  end
  
  def find
    query.find
  end
  
  def db_connection
    ActiveRecord::Base.establish_connection(MarketQuery.extract_env(query.host)).connection
  end

  def self.available_action?(action)
    return false if action.blank?
    return @@action_to_available_contexts.keys.include?(action.downcase.pluralize)
  end

  def self.extract_env(host_name)
    return "market_#{Rails.env}".to_sym if host_name.blank?
    core_name = host_name.split(".")[0]
    return "market_#{Rails.env}".to_sym if @@host_lookup[core_name].nil?
    @@host_lookup[core_name]
  end
  
  private
  
    def build_where_clause
      where_clause = ""
      action = query.action.pluralize.downcase
      available_contexts = @@action_to_available_contexts[action]
      status = :context
      query.parsed_contexts.each do |lookup|
        next if status == :join && !lookup.kind_of?(String)
        
        where_clause = " where" if where_clause.blank?
        if lookup.kind_of?(String)
          where_clause += " #{lookup}" 
          status = :context
        elsif !lookup[:context_value].nil? && available_contexts.include?(lookup[:context_key])
          where_clause += " #{database_column(action,lookup[:context_key])} #{lookup[:comparator]} #{database_value(lookup[:context_key],lookup[:context_value],lookup[:comparator])}"
          status = :join
        else
          where_clause += " 1=1"
        end
      end
      where_clause
    end
    
    def context_key_filter_name(action)
      "'" + query.group_by.join(",") + "'"
    end
    
    def context_value_filter_name(action)
      database_names = query.group_by.collect do |name|
        database_column(action,name)
      end
      "concat(ifnull("+ database_names.join(",'--'),',',ifnull(") +",'--'))"
    end
  
    def database_column(action,name)
      return @@contexts_to_table_column[name] unless @@contexts_to_table_column[name].nil?
      return "#{@@action_to_table_name[action]}.#{name}" unless @@action_to_table_name[action].nil?
      "#{action}.#{name}"
    end
    
    def database_value(context,raw_value,comparator)
      value = Appstats::Query.sqlclean(raw_value)
      if @@boolean_contexts.include?(context.downcase)
        value.downcase!
        return 0 if value.starts_with?('n') || value.starts_with?('f') || value == '0'
        return 1
      else
        Appstats::Query.sqlquote(value,comparator)
      end
    end
end
