class OffNetOvcEndPointUnisController < CustomApplicationController

  before_filter :find_model, :monitoring_title
  
  protected
  
  # Each component uses a different entity to display graph tabs
  def graph_tabs
    @entity.cos_end_points
  end
  
  private

  def find_model
    @entity = OvcEndPointUni.find params[:id], :include=>[{:cos_end_points => {:cos_instance => :class_of_service_type}}] if params[:id]
  end

  def monitoring_title
    @monitoring_title = 'Member Endpoint Monitoring'
  end  

end