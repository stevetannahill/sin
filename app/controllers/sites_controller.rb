class SitesController < CustomApplicationController

  before_filter :find_model
  
  def map_info_window
    @site = @entity

    # TODO: add security
    render :map_info_window, :layout => false
  end

  private
  
  def find_model
    @entity = Site.find params[:id] if params[:id]
  end
  
end