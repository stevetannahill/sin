class MemberHandleInstancesController < ApplicationController
  def create_or_update
    
    params['handles'].each do |i,instance_element|
      member_attr = MemberAttr.find_by_affected_entity_type_and_type(instance_element["type"],"MemberHandle")
      
      handle = MemberHandleInstance.find_or_initialize_by_affected_entity_id_and_affected_entity_type_and_owner_id_and_owner_type_and_member_attr_id(instance_element["id"],instance_element["type"],current_user.service_provider.id,"ServiceProvider",member_attr.id)
      handle.value = instance_element["value"]
      handle.save
    end
    
    render :json => {}.to_json
  end

end
