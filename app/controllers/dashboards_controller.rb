require 'paths_helper'

class DashboardsController < CustomApplicationController
  EVENT_STATES = ['warning', 'failed', 'unavailable']

  include DashboardHelper

  def index
    # Can user see dashboard in requested mode
    # if not then show in a mode they can see (read)
    mode = get_mode_for_page(@mode, 'dashboard')
    redirect_to send("#{mode}_dashboard_path", {:mode => mode}) if mode
  end

  def show
    authorize! :read, :monitoring_dashboard
    get_paths_and_ennis
    set_search_filter_params

    # show the dashboard event
    @entity_type = params[:entity_type]
    @entity_id = params[:entity_id]

    if params[:entity_type] == 'path'
      @entity = @paths.where("paths.id = ?", params[:id].to_i).first
      @entity_type = 'Path'
      @entity_state = @entity.get_SM_state
    elsif params[:entity_type] == 'enninew'
      @entity = @ennis.where("demarcs.id = ?", params[:id].to_i).first
      @entity_type = 'ENNI'
    end

    respond_to do |format|
      format.json do

        event_info = if @entity && EVENT_STATES.include?(@entity.sm_state.downcase)
          [
            render_to_string(:partial => "entity_event_severity.html.haml"),
            render_to_string(:partial => "entity_event_type.html.haml"),
            render_to_string(:partial => "entity_event_link.html.haml"),
            render_to_string(:partial => "entity_event_circuits.html.haml"),
            @entity.service_provider.name,
            Time.at(@entity.sm_timestamp/1000,(@entity.sm_timestamp%1000)*1000).in_time_zone.to_formatted_s(:ymd_time),
            render_to_string(:partial => "entity_event_details.html.haml"),
            render_to_string(:partial => "entity_network_link.html.haml")
          ]
        else
          []
        end
        render :json => event_info.to_json
      end
    end
  end

  def data
    require 'will_paginate/array'
    @current_page = (params[:iDisplayStart].to_i/params[:iDisplayLength].to_i rescue 0)+1

    @events = (@paths.s_events_by_state(EVENT_STATES) + @ennis.s_events_by_state(EVENT_STATES)).sort_by{|e| [e.sm_state,-e.sm_timestamp]}.paginate(:page => @current_page, :per_page => params[:iDisplayLength])
  end


  def ordering
    authorize! :read, :ordering_dashboard
    
    get_counts

    paths = if can? :read, :ordering_exception_reports
      @paths.s_provisioning_counts_with_fallout
    else
      @paths.s_provisioning_counts
    end

    @path_ordering_counts = sort_by_state(@paths.s_ordering_counts, ordering_states, :order_name)
    @enni_ordering_counts = sort_by_state(@ennis.s_ordering_counts, ordering_states, :order_name)
    
    dashboard_response!
  end
  

  def inventory
    authorize! :read, :inventory_dashboard

    get_counts

    paths = if can? :read, :inventory_exception_reports
      @paths.s_provisioning_counts_with_fallout
    else
      @paths.s_provisioning_counts
    end

    @path_provisioning_counts = sort_by_state(paths, provisioning_states, :prov_name_display)
    @enni_provisioning_counts = sort_by_state(@ennis.s_provisioning_counts, provisioning_states, :prov_name)

    dashboard_response!    
  end


  def monitoring
    authorize! :read, :monitoring_dashboard

    get_counts    
    @path_monitoring_counts = sort_by_state(@paths.s_monitoring_counts, monitoring_states, :sm_state)
    @enni_monitoring_counts = sort_by_state(@ennis.s_monitoring_counts, monitoring_states, :sm_state)
    
    dashboard_response!
  end
  

  protected
  
  def get_counts
    include_ennis = true
    
    if !@sow_id.blank? || !@cenx_path_type.blank?
      include_ennis = false
    end
    
    if App.is?(:coresite)
      @national_tenants = ServiceProvider.all
    end

    get_site_counts(include_ennis)
    get_service_provider_counts(include_ennis)
    get_oem_counts(include_ennis)
    
    unless App.is?(:coresite)
      @site_count = @paths.s_demarc_sites.count('DISTINCT(path_displays.entity_id)')
      @agg_site_count = @paths.s_agg_site_counts(@service_provider_id).count('DISTINCT(sites.id)').length
    end

    unless @service_provider_id.blank?
      @service_provider ||= ServiceProvider.find(@service_provider_id)
      @service_providers = {}
    end

    unless @site_id.blank?
      @site ||= Site.find(@site_id)
      @sites = {}
    end
    
    unless @operator_network_type_id.blank?
      @oem ||= OperatorNetworkType.find(@operator_network_type_id)
      @oems = {}
    end
  end
  
  def get_site_counts(include_ennis)
    path_sites = @paths.s_agg_site_counts(@service_provider_id)
    enni_sites = @sub_ennis.s_agg_site_counts
    mon_ennis_sites = @mon_ennis.s_agg_site_counts
 
    @sites = path_sites.map{ |e| {:id => e.site_id, :name => e.site_name, :network_type_name => e.network_type_name} }
    
    if include_ennis
      @sites = @sites.concat(enni_sites.map{ |e| {:id => e.site_id, :name => e.site_name} })
      @sites = @sites.concat(mon_ennis_sites.map{ |e| {:id => e.site_id, :name => e.site_name} })
    end

    @sites = @sites.uniq
    @sites.delete_if {|site| site[:network_type_name].nil? }

    @sites.each do |site|
      path = path_sites.find{ |e| e.site_id == site[:id] }
      enni = enni_sites.find{ |e| e.site_id == site[:id] }
      mon_enni = mon_ennis_sites.find{ |e| e.site_id == site[:id] }
      site[:path_count] = path.nil? ? 0 : path[:path_count]
      site[:enni_count] = enni.nil? ? 0 : enni[:enni_count]
      site[:mon_enni_count] = mon_enni.nil? ? 0 : mon_enni[:enni_count]
    end
  end
  
  
  def get_service_provider_counts(include_ennis)
 
    path_service_providers = Path.s_service_provider_counts(@paths, @site_id, params)
    enni_service_providers = @sub_ennis.s_service_provider_counts
    mon_ennis_service_providers = @mon_ennis.s_service_provider_counts
 
    @service_providers = path_service_providers.map{ |e| {:id => e.service_provider_id, :name => e.service_provider_name} }

    if include_ennis
      @service_providers.concat(enni_service_providers.map{ |e| {:id => e.service_provider_id, :name => e.service_provider_name} })
      @service_providers.concat(mon_ennis_service_providers.map{ |e| {:id => e.service_provider_id, :name => e.service_provider_name} })
    end
                        
    @service_providers = @service_providers.uniq

    @service_providers.each do |sp|
      path = path_service_providers.find{ |e| e.service_provider_id == sp[:id] }
      enni = enni_service_providers.find{ |e| e.service_provider_id == sp[:id] }
      mon_enni = mon_ennis_service_providers.find{ |e| e.service_provider_id == sp[:id] }
      sp[:path_count] = path.nil? ? 0 : path[:path_count]
      sp[:site_count] = path.nil? ? 0 : path[:site_count]
      sp[:enni_count] = enni.nil? ? 0 : enni[:enni_count]
      sp[:mon_enni_count] = mon_enni.nil? ? 0 : mon_enni[:enni_count]
      # TODO: Terrible code, but good enough for now
      if App.is?(:coresite)
        coresite_service_provider = ServiceProvider.find(sp[:id])
        sp[:operator_networks_count] = coresite_service_provider.operator_networks.count
        sp[:ennis_count] = coresite_service_provider.ennis.count
        sp[:buying_orders_count] = coresite_service_provider.buying_orders(@filter_params).count
        sp[:target_orders_count] = coresite_service_provider.target_orders(@filter_params).count
      end
    end
  end


  def get_oem_counts(include_ennis)
    path_oems = Path.s_oem_counts(@paths, params)
    enni_oems = @sub_ennis.s_oem_counts
    mon_ennis_oems = @mon_ennis.s_oem_counts

    @oems = path_oems.map{ |e| {:id => e.operator_network_type_id, :name => e.operator_network_type_name} }

    if include_ennis
      @oems.concat(enni_oems.map{ |e| {:id => e.operator_network_type_id, :name => e.operator_network_type_name} })
      @oems.concat(mon_ennis_oems.map{ |e| {:id => e.operator_network_type_id, :name => e.operator_network_type_name} })
    end

    @oems = @oems.uniq
    @oems.each do |oem|
      path = path_oems.find{ |e| e.operator_network_type_id == oem[:id] }
      enni = enni_oems.find{ |e| e.operator_network_type_id == oem[:id] }
      mon_enni = mon_ennis_oems.find{ |e| e.operator_network_type_id == oem[:id] }
      oem[:path_count] = path.nil? ? 0 : path[:path_count]
      oem[:site_count] = path.nil? ? 0 : path[:site_count]      
      oem[:enni_count] = enni.nil? ? 0 : enni[:enni_count]
      oem[:mon_enni_count] = mon_enni.nil? ? 0 : mon_enni[:enni_count]      
    end
  end


  def sort_by_state(arr, states, key)
    arr.sort{|a,b| states.index{|s| s[:name] == a[key].parameterize('_')} <=> states.index{|s| s[:name] == b[key].parameterize('_')}}
  end


  def dashboard_response!
    return respond_to do |format|
      if request.xhr?
        format.js { render :json => {
                           :html => render_to_string(:partial => "dashboards/#{action_name}")
                  },
                  :callback => params[:callback]
                }
      else
        format.html
        format.csv { render "#{action_name}_#{params[:csv_data]}" }
      end
    end
  end
end
