class PathsController < CustomApplicationController
  load_and_authorize_resource :only => [:update]

  before_filter :authorized_as_coresite, :only => [:edit, :update] if App.is?(:coresite)

  before_filter :find_model, :set_editing
  
  helper_method :osl_data_source_name

  before_filter do |controller| 
    case controller.mode
      when 'monitoring'
        authorize!(:read, :monitoring_circuit_list)
      when 'inventory'
        authorize!(:read, :inventory_circuit_list)
      when 'ordering'
        authorize!(:read, :ordering_circuit_list)
    end
    cmaudit_app
    osl_app
  end
  
  def index

    # need to paginate arrays for this case only, inefficient bug hopefully temporary
    require 'will_paginate/array'

    if params[:path_details].blank?
      
      # Filter by sphinx full text first
      path_ids = fulltext_search
    
      # Get conditions from parameters
      valid_conditions = %w(cenx_path_type exchange service_provider statement_of_work oem monitoring_state provisioning_state ordering_state)
      conditions = params.select{ |k,v| valid_conditions.include?(k) }

      # @is_filtered can be set by fulltext_search as well
      unless @is_filtered
        @is_filtered = conditions.empty? ? false : true 
      end
    
      # Used for breadcrumb
      @site = Site.find(params[:site]) unless params[:site].blank?
      @operator_network = OperatorNetwork.find(params[:operator_network]) unless params[:operator_network].blank?
      @service_provider = ServiceProvider.find(params[:service_provider]) unless params[:service_provider].blank?
      @operator_network_type = OperatorNetworkType.find(params[:oem]) unless params[:oem].blank?
      @breadcrumbs = {site: @site, service_provider: @service_provider, oem: @operator_network_type}.reject{|param, value| value.nil? }

      @cenx_path_type = params[:cenx_path_type]
      @sow_id = params[:statement_of_work]
      @site_group_id = params[:site_group]
      @ordering_state = params[:ordering_state]
      @provisioning_state = params[:provisioning_state]
      @monitoring_state = params[:monitoring_state]
      @fallout_exception_type = params[:fallout_exception_type]
      @fallout_category = params[:fallout_category]
      @filter_params = request.GET.merge({'mode' => params[:mode]})
      @path_ids = params[:path_ids]

      @paths = Path.find_paths(@path_owner, @current_user, params[:service_provider], params[:site], @cenx_path_type, @ordering_state, @provisioning_state, @monitoring_state, @sow_id, params[:oem], @fallout_exception_type, @fallout_category, @path_ids, @site_group_id, @operator_network.try(:id))


      # Get service provider names and group by evc id
      @paths = @paths.s_unique

      params[:sort] = 'evc_member_handle_sort' if params[:sort].blank?
    
      @paths = case params[:sort]
      # when 'cenx_id_sort'
      #   Nothing let sphinx order
      # when 'evc_service_provider_name_sort'
      #   @paths.order("evcs.service_provider_name") 
      when 'provisioning_state_text_sort'
        @paths.order("evcs.prov_name") 
      when 'ordering_state_text_sort'
        @paths.order("evcs.order_name") 
      when 'monitoring_state_text_sort'
        @paths.order("evcs.sm_state") 
      else
        @paths.order("sin_path_service_provider_sphinxes.member_handle") 
      end
    
      # Paginate results and make sure service provider name and member handles are included so we can sort on them
      # @paths = @paths.paginate(:page => [params[:page].to_i,1].max, :per_page => 10).s_admin_member_handles.group('paths.id').order("a_member_handles")
    
      # do a big giant IN if sphinx was used to filter results
      @paths = @paths.where(:paths => {:id => path_ids}) unless path_ids.nil?
    
      # Leave .all so will_paginate doesn't receive an ActiveRelation
      @paths = @paths.paginate(:page => [params[:page].to_i,1].max, :per_page => 10).all
    
    else
      @details = true
      # Return paths as array
      @paths = [find_path(params[:path_details])]
    end

    # Remove nils
    @paths.compact!

  end

  # Rendering evc via ajax
  def list_row
    @details = true if params[:path_details]
    # using entity so shared ordering state view works
    # Wrap render partial in format.html so content is sent back (bug in Rails 3.2.2, https://github.com/rails/rails/issues/5238)
    respond_to do |format|
     format.html { render :partial => 'paths/evc', :locals => { :evc => find_path(@entity.id) } }
    end
  end
  
  def edit
    # No mode (ie inventory, ordering, monitoring) on edit page
    @mode = 'editing'
    # Find member_handles to edit
    find_member_handles
  end
  
  def update

    # Find member_handles to update
    find_member_handles
    
    @errors = []
    params['evc_member_handles'] ||= []
    params['component_member_handles'] ||= []
    
    # Update Evc member handle
    params['evc_member_handles'].each do |id, value|
      mh = @entity.member_attr_instances.find(id)
      if mh.owner_id == @current_user.service_provider.id || @current_user.service_provider.is_system_owner?
        mh.value = value
        if mh.value_changed?
          mh.save
          @errors << mh.errors if mh.errors.any?
        end
      end
    end

    # Update each components member handles
    params[:component_member_handles].each do |mh_id, value|
      mh = MemberAttrInstance.find(mh_id)
      if mh.owner_id == @current_user.service_provider.id || @current_user.service_provider.is_system_owner?
        mh.value = value
        if mh.value_changed?
          mh.save 
          @errors << mh.errors if mh.errors.any?
        end
      end
    end
    
    flash[:notice] = "IDs updated successfully" if @errors.empty?
    
    # Find model again after member handles are saved
    find_model 
    
    render "edit"
    
  end

  # View revisions (diffs for osl files)
  def view_data_sources

    if can? :read, :inventory_exception_reports

      @current_fallout_exceptions = @entity.fallout_exceptions.current
      @cleared_fallout_exceptions = @entity.fallout_exceptions.cleared
      @cascade_id = @entity.member_handle

      # Get list of formats / data sources
      formats = cmaudit_app.cmd("cm ls-formats")[:data] || {}

      unless formats.blank?
        # Revision history
        @data_sources = formats.keys
        @revisions = @data_sources.map{ |format| load_revisions(format) }.compact
        # Audit
        @audit_versions = cmaudit_app.cmd("cm audit-versions cascade #{@cascade_id}")[:data]
        @audits = cmaudit_app.cmd("cm cascade-audit #{@cascade_id}")[:data]
        @audit_data_sources = cmaudit_app.cmd("cm cascade-format? #{@cascade_id}")[:data] unless @audit_versions.blank?
        @audit_data_sources ||= []
      end
    
    end

    render :layout => false
  end

  # Ajax, return just a single revision (diff)
  def revision
    data_source = params[:data_source]
    cascade_id = params[:cascade_id]

    cm_versions = cmaudit_app.cmd("cm versions cascade #{cascade_id} #{data_source}")
    versions = cm_versions[:data].try(:reverse) || []
    revision_data = {:data_source => data_source, :cascade_id => params[:cascade_id], :versions => versions}
    version_parts_left, version_parts_right = params[:version_left].split('_'), params[:version_right].split('_')
    version_left, version_right = version_parts_left[0], version_parts_right[0]
    version_left_nbr, version_right_nbr =  version_parts_left[1], version_parts_right[1]
    comparable = cmaudit_app.cmd("cm comparable cascade #{params[:cascade_id]} #{data_source} #{version_left} #{version_right}")[:data] || { :left => {}, :right => {} }
    revision_data.merge!({:left => comparable[:left], :right => comparable[:right], :version_left => version_left, :version_right=> version_right, :version_left_nbr => version_left_nbr, :version_right_nbr => version_right_nbr})
    respond_to do |format|
      format.html { render :partial => 'revision', :locals => { :revision => revision_data } }
    end
  end

  def osl_data_source_name(data_source)
    osl_data_sources.fetch data_source.to_sym, "#{data_source.gsub(/_/, " ").titleize}"
  end

  protected
  
  def find_model
    @entity = find_path(params[:id]) if params[:id]
  end

  def load_revisions(data_source)
    revision = {:data_source => data_source, :cascade_id => @cascade_id}
    cm_versions = cmaudit_app.cmd("cm versions cascade #{@cascade_id} #{data_source}")
    if cm_versions
      versions = cm_versions[:data].try(:reverse)
      unless versions.blank?
        version_left_index, version_right_index = 0, 0
        version_left, version_right = versions[version_left_index], versions[version_right_index]
        # version_right, version_right_index = version_left, version_left_index unless version_right
        # version_left, version_left_index = version_right, version_right_index unless version_left
        version_left_nbr, version_right_nbr = versions.size - version_left_index, versions.size - version_right_index
        comparable = cmaudit_app.cmd("cm comparable cascade #{@cascade_id} #{data_source} #{version_left} #{version_right}")[:data] || { :left => {}, :right => {} }
        revision.merge!({:left => comparable[:left], :right => comparable[:right], :versions => versions, :version_left => version_left, :version_right => version_right, :version_left_nbr => version_left_nbr, :version_right_nbr => version_right_nbr })
      end
    end
    revision    
  end 

  # def version_diff(cascade_id, data_source, version_left, version_right)
  #   if version_left && version_right
  #     osl_diff_result = cmaudit_app.cmd("cm comparable cascade #{cascade_id} #{data_source} #{version_left} #{version_right}")[:data]
  #     if !osl_diff_result.nil? && osl_diff_result[:left_content] && osl_diff_result[:right_content]
  #       Diffy::Diff.new(osl_diff_result[:left_content], osl_diff_result[:right_content], :include_plus_and_minus_in_html => false)
  #     end
  #   end
  # end

  
  def find_member_handles
    @path_member_handles = @current_user.service_provider.is_system_owner? ? @entity.member_handles : @entity.member_handles.owned_by(@current_user.service_provider)
    @component_member_handles = @entity.unique_diagram_components(@current_user.service_provider).reject{|ed| ed.end_point? }
  end
  
  def set_editing
    @editing = ['edit', 'update'].include?(action_name) ? true : false
  end
  
end
