class StateController < ApplicationController

  before_filter :find_model

  # Update component states
  def update
    pre_result, post_result, pre_objs, post_objs = @order.order_state.set_state(params[:new_state].constantize)
    error_string = ""
    if !pre_result
      error_string += "Failed to change state due to objects "
      pre_objs.each do |obj|
        obj_identifier = ""
        obj_identifier = obj[:obj].name if obj[:obj].respond_to? :name
        obj_identifier = obj[:obj].cenx_id if obj[:obj].respond_to? :cenx_id

        error_string += "#{obj[:obj].class.to_s}:#{obj_identifier} #{obj[:reason]}\n"
      end
    elsif !post_result
      error_string += "State changed but post conditions failed due to objects "
      post_objs.each do |obj|
        obj_identifier = ""
        obj_identifier = obj[:obj].name if obj[:obj].respond_to? :name
        obj_identifier = obj[:obj].cenx_id if obj[:obj].respond_to? :cenx_id
        error_string += "#{obj[:obj].class.to_s}:#{obj[:obj].cenx_id} #{obj[:reason]}\n"
      end
    end

    message = error_string.blank? ? {:text => 'State updated successfully', :type => :successful} : {:text => error_string, :type => :error}
    respond_to do |format|
      format.html { render :partial => 'shared/ordering_state', :locals => {:order => @order, :message => message, :edit => true, :history => true} }
    end
    
  end

  protected
  
  def find_model
    order_type = params[:type]
    # order_type holds name of order object to instantiate
    # TODO: Could create separate controllers for each type like we do with components, then we would need todo the const_get code
    @order = Object.const_get(order_type).find(params[:id])
    @entity = @order.ordered_entity
  end
  
end