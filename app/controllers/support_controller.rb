class SupportController < ApplicationController

  def index
    @phone = Settings['support.phone']
    @phone_text = Settings['support.phone_text']
    @email = Settings['support.email']
    @email_text = Settings['support.email_text']
  end
  
end
