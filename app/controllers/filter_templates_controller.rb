class FilterTemplatesController < ApplicationController
  include FilterTemplatesHelper

  load_and_authorize_resource
  before_filter :find_user


  def update
    @filter_template = @user.filter_templates.find(params[:id])

    respond_to do |format|
      if @filter_template.update_attributes(params[:filter_template])
        format.json { head :no_content }
      else
        format.json { render json: @filter_template.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @filter_template = @user.filter_templates.build(params[:filter_template])
    @filter_template.filter_options = request.GET
    
    respond_to do |format|
      if @filter_template.save
        format.html { redirect_to filter_template_url(@filter_template), notice: "#{@filter_template.name} filter was successfully created." }
        format.json { render json: @filter_template, status: :created, location: @filter_template }
      else
        format.html { redirect_to filter_template_url(@filter_template), alert: "Filter could not be saved - #{@filter_template.errors.full_messages.map(&:downcase).to_sentence}." }
        format.json { render json: @filter_template.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @filter_template = @user.filter_templates.find(params[:id])

    respond_to do |format|
      if @filter_template.destroy
        format.json { head :no_content }
      else
        format.json { render json: @filter_template.errors, status: 400 }
      end
    end
  end
  
  private
  
  def find_user
    @user = User.find(params[:user_id])
  end
      
end
