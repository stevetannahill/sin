class ReportsController < CustomApplicationController
  include PathsHelper
  include DashboardHelper
  before_filter :common_rollup_report, :only => [:site_group, :satellite_site, :cell_site, :cell_site_path, :metrics]
  before_filter :common_fallout_exception_report, :only => [:fallout_exception]
  before_filter :set_common_params

  def index
    if can? :read, :sla_reports
      redirect_to sla_report_path(preferred_mode(:monitoring))
    elsif can? :read, :inventory_exception_reports
      redirect_to fallout_exception_report_path({:fallout_exception_type => 'audit'}.merge(preferred_mode(:inventory)))
    elsif can? :read, :ordering_exception_reports
      redirect_to fallout_exception_report_path({:fallout_exception_type => 'ordering'}.merge(preferred_mode(:ordering)))
    elsif can? :read, :rolled_up_reports
      redirect_to site_group_report_path(preferred_mode(:monitoring))
    elsif can? :read, :circuit_utilization_reports
      redirect_to circuit_utilization_report_path(preferred_mode(:monitoring))
    elsif can? :read, :enni_utilization_reports
      redirect_to enni_utilization_report_path(preferred_mode(:ordering))
    elsif can? :manage, Schedule
      redirect_to schedules_path
    end
  end
  
  def paths
    authorize! :read, :sla_reports

    @paths = @paths.s_unique
    @sites = SinPathServiceProviderSphinx.select('path_id AS id, member_handle').
                                where('member_handle LIKE ?', "%#{params[:term]}%").
                                where('path_id IN (?)', @paths.pluck('paths.id')).
                                where('service_provider_id = ?', @current_user.service_provider.id).
                                limit(20)

    sites_hash = {}
    @sites.map {|site| sites_hash[site[:id]] = site[:member_handle]}

    respond_to do |format|
      format.json { render :json => sites_hash }
    end
  end

  def site_group
    authorize! :read, :rolled_up_reports
    @exceptions = site_group_exceptions(@paths, params, @date_range_start, @date_range_end, @current_user)

    if request.format.json?
      @total = @exceptions.s_count
    else
      @cell_sites = cell_site_exceptions(@paths, params, @date_range_start, @date_range_end, @current_user)
      get_severity_counts(@cell_sites)    
    end

    report_response!        
  end

  def satellite_site
    authorize! :read, :rolled_up_reports
    @exceptions = satellite_site_exceptions(@paths, params, @date_range_start, @date_range_end, @current_user)

    if request.format.json?
      @total = @exceptions.s_count
    else
      @cell_sites = cell_site_exceptions(@paths, params, @date_range_start, @date_range_end, @current_user)
      get_severity_counts(@cell_sites)    
    end

    report_response!    
  end

  def cell_site
    authorize! :read, :rolled_up_reports
    @exceptions = cell_site_exceptions(@paths, params, @date_range_start, @date_range_end, @current_user)
    get_severity_counts(@exceptions)    

    report_response!
  end

  def cell_site_path
    authorize! :read, :rolled_up_reports
    @metrics_report = true  
    @exceptions = cell_site_path_exceptions(@paths, params, @date_range_start, @date_range_end, @current_user)
    get_severity_counts(@exceptions)

    report_response!
  end

  def metrics
    authorize! :read, :rolled_up_reports
    @metrics_report = true
    @exceptions = metrics_exceptions(@paths, params, @date_range_start, @date_range_end, @current_user)
    get_severity_counts(@exceptions)

    report_response!
  end

  def sla
    authorize! :read, :sla_reports
    @exceptions = sla_exceptions(@paths, params, @date_range_start, @date_range_end, @current_user)    
    get_severity_counts(@exceptions)    

    report_response!
  end

  def oem_sla
    authorize! :read, :oem_reports
        
    # TODO: move hardcoded values elsewhere
    @flr_major = 10.0
    @flr_minor = 5.0
    @fd_major = 30.0
    @fd_minor = 15.0
    @fdv_major = 2500.0
    @fdv_minor = 1000.0

    @paths = @paths.s_unique
    @exceptions = oem_sla_exceptions(@paths, params, @date_range_start, @date_range_end, @current_user)    
    unless request.format.json?
      @minor = @exceptions.s_minor.s_count
      @major = @exceptions.s_major.s_count
      @critical = @exceptions.s_critical.s_count
      @total = @minor + @major + @critical      
    end

    report_response!
  end


  def fallout_exception
    if ['ordering', 'audit'].include?(params[:fallout_exception_type]) && params[:fallout_exception_type] == 'ordering' 
      authorize! :read, :ordering_exception_reports
    else
      authorize! :read, :inventory_exception_reports
    end

    if App.is?(:coresite)
      @fallout_build = @paths.s_fallout_build.s_date_range(@date_range_start, @date_range_end).count('paths.id', :distinct => true)
    end

    # @fallout_categories = @paths.s_fallout_category_counts.s_date_range(@date_range_start, @date_range_end)

    # Allow user to search by Path ID
    path_ids = fulltext_search

    # do a big giant IN if sphinx was used to filter results
    @paths = @paths.where(:paths => {:id => path_ids}) unless path_ids.nil?

    @paths = @paths.s_unique    

    # Get All exceptions not just current one
    @exceptions = fallout_exceptions(@paths, params, @date_range_start, @date_range_end, @current_user)

    # a path can have multiple exceptions so this scope does not behave as we would like
    # @fallout_categories = @exceptions.collect { |e| e.category }.inject(Hash.new(0)) { |hash, item| item = "Other" if item.nil?; hash[item] += 1; hash }
    @fallout_categories = @exceptions.select('fallout_exceptions.category as fallout_category, count(*) as fallout_category_count').group('fallout_exceptions.category')

    report_response!
  end


  def circuit_utilization
    authorize! :read, :circuit_utilization_reports
    
    @paths = @paths.s_unique
    @exceptions = circuit_utilization_exceptions(@paths, params, @date_range_start, @date_range_end, @current_user)
    
    # TODO: Limit the rows returns
    @top_talkers = TopTalkerCosEndPoint.includes([:cos_end_point => {:segment_end_point => [:demarc, {:segment => :paths}]}]).
                                        where('paths.id in (:path_ids)', {:path_ids => @paths.pluck('paths.id')}).
                                        group('cos_end_point_id, direction')
    
    unless request.format.json?
      @minor = @exceptions.s_minor.s_count
      @major = @exceptions.s_major.s_count
      @critical = @exceptions.s_critical.s_count
      @total = @minor + @major + @critical      
    end

    report_response!                                        
  end


  def enni_utilization
    authorize! :read, :enni_utilization_reports
    require 'will_paginate/array'
    @exceptions = []      
    @ennis = @ennis.s_unique
    @ennis = @ennis.includes([:segment_end_points])
              
    if params[:js_data] == 'provisioned_bandwidth'
      sort_columns = [nil, 'sites.name', nil, nil, nil, 'cir_limit', 'eir_limit']
      sort_column = sort_columns[params[:iSortCol_0].to_i || 2]
      sort_direction = params[:sSortDir_0] || "DESC"
      
      @ennis = @ennis.order("#{sort_column} #{sort_direction}")

      unless request.format.csv? || request.format.pdf?
        @ennis = @ennis.paginate(:page => @current_page, :per_page => params[:iDisplayLength])
      end
    else
      @exceptions = enni_utilization_exceptions(@ennis, params, @date_range_start, @date_range_end, @current_user)
             
      unless request.format.json?
        @minor = @exceptions.s_minor.s_count
        @major = @exceptions.s_major.s_count
        @critical = @exceptions.s_critical.s_count
        @total = @minor + @major + @critical      
      end
                        
      # TODO: Limit the rows returns
      @top_talkers = TopTalkerEnni.includes(:enni_new).
                                    where('enni_id in (:enni_ids)', {:enni_ids => @ennis.pluck('demarcs.id').uniq}).
                                    group('enni_id, direction')                                     
    end
    
    report_response!           
  end
  

  protected

  def preferred_mode(mode)
    can?(:read, mode.to_sym)? {:mode => mode} : {}
  end

  def set_default_date_range
    @default_date_range = @rollup_report ? 'seven_days_previous' : 'month_to_date'
  end

  def set_common_params
    @params = request.GET
    @default_date_range = 'month_to_date' if @default_date_range.nil?
    @date_range_start, @date_range_end = get_date_range(params['date_range_selector'], params['date_range_start'], params['date_range_end'], @default_date_range ? @default_date_range : 'month_to_date')
    @reports_page = true
    @date_range_select = true
    @component_condition = params[:component_condition] || 'and'
    @filter_template = FilterTemplate.new
    @site_list = SiteList.new
    @current_page = (params[:iDisplayStart].to_i/params[:iDisplayLength].to_i rescue 0)+1
    @site = Site.find(params[:site]) if params[:site].present?
    @site_group = SiteGroup.find(params[:site_group]) if params[:site_group].present?
    @demarc = Demarc.find(params[:demarc]) if params[:demarc].present?
  end
  
  def common_rollup_report
    @rollup_report = true
    @default_date_range = 'week_to_date'
  end

  def common_fallout_exception_report
    @default_date_range = 'seven_days_previous'
  end

  def get_severity_counts(exceptions)
    unless request.format.json?
      @counts = exceptions.s_severity_count    
      @clear = @counts.find {|k| k['severity'] == 'clear'}.try(:[], :count) || 0
      @minor = @counts.find {|k| k['severity'] == 'minor'}.try(:[], :count) || 0
      @major = @counts.find {|k| k['severity'] == 'major'}.try(:[], :count) || 0
      @critical = @counts.find {|k| k['severity'] == 'critical'}.try(:[], :count) || 0
      @total = @clear + @minor + @major + @critical
    else
      @total = @exceptions.s_count
    end
  end

  def report_response!
    unless request.format.csv? || request.format.pdf? || request.format.html?
      @exceptions = @exceptions.paginate(:page => @current_page, :per_page => params[:iDisplayLength])
    end
    
    respond_to do |format|
      format.js { render "#{action_name}_#{params[:js_data]}" }
      format.csv { render "#{action_name}_#{params[:csv_data]}", :locals => {:exceptions => @exceptions, :user => @current_user} }
      format.pdf { render :pdf => "#{action_name}_#{params[:pdf_data]}", :template => "/reports/#{action_name}_#{params[:pdf_data]}", :layout => 'pdf.html', :disposition => 'attachment', :header => {:right => '[page] of [topage]'}}
      # HACK: currently only configured for fallout_exceptions
      format.xlsx do 
        filename = "Sprint_BH_Audit_Results_#{Time.now.strftime("%Y-%m-%d")}.xlsx"
        send_data cmaudit_app.cmd("fallout report-xls-stream \"#{Tempfile.new('fallout_exception_exceptions').path}\"",false)[:data].string, :filename => filename
      end
      format.html
    end
  end
end