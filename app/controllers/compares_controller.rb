class ComparesController < CustomApplicationController

  # Add a new graph to the list of graphs to compare
  def create
    graph_id = Time.now.to_i
    graph_details = {'url' => params[:url], 'date_range_start' => params[:date_range_start], 'date_range_end' => params[:date_range_end], 'title' => params[:title], 'graph_pdf_url' => params[:graph_pdf_url], 'graph_csv_url' => params[:graph_csv_url]}
    session['graphs_to_compare'][graph_id] = graph_details

    # Wrap render partial in format.html so content is sent back (bug in Rails 3.2.2, https://github.com/rails/rails/issues/5238)
    respond_to do |format|
      format.html { render :partial => 'graph_details', :locals => {:graph_details => graph_details, :graph_id => graph_id} }
    end

  end

  def reorder
    graph_ids = params['graph_ids']
    # Reorder graphs
    session['graphs_to_compare'] = graph_ids.each_with_object({}) do |graph_id, new_graphs_to_compare|
      new_graphs_to_compare[graph_id] = session['graphs_to_compare'][graph_id]
    end
    render :text => 'Ok', :layout => false
  end
  
  # Remove graph from list of graphs to compare
  def destroy
    session['graphs_to_compare'].delete(params[:id])
    render :text => params[:id], :layout => false
  end

  # Remove all graphs from list of graphs to compare
  def clear
    session['graphs_to_compare'].clear
    render :text => 'Ok', :layout => false
  end
  
end
