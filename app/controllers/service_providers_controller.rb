class ServiceProvidersController < ApplicationController
  load_and_authorize_resource
  before_filter do |c| @service_providers_page = true end
  before_filter :find_service_provider, :except => [:index]
  
  # GET /service_providers
  def index
    if @current_user.service_provider.is_system_owner?
      @service_providers = ServiceProvider.all
    else
      @service_providers = [@current_user.service_provider]
    end
  end

  
  def show
    respond_to do |format|
      format.html { render :layout => "application" }
      format.json { render json: @service_provider }
    end
  end

  # GET /service_providers/1/edit
  def edit
  end

  # PUT /service_providers/1
  # PUT /service_providers/1.json
  def update
    respond_to do |format|
      if @service_provider.update_attributes(params[:service_provider])
        if params[:set_default_terms] && can?(:update_all, ServiceProvider)
          # OPTIMIZE ServiceProvider.update_all currently fails with serialized fields
          ServiceProvider.all.each {|sp| sp.update_attributes(:terms => @service_provider.terms)}
        end

        if params[:set_system_logo] && can?(:manage, :customize_system)
          ServiceProvider.set_system_logo(@service_provider)
        end

        format.html { redirect_to service_providers_path, notice: "#{@service_provider.name} was successfully updated." }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @service_provider.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  
  def find_service_provider
    @service_provider = ServiceProvider.find(params[:id])
  end
    
end
