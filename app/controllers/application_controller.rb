# encoding: UTF-8
# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.
require 'csv'

class ApplicationController < ActionController::Base

  include ApplicationHelper
  # To get date range stuff  
  include ExceptionReports


  if $cas_server_enabled then
    before_filter :cas_filter, :except => [:logo, :login_text]
  elsif $coresite_authentication
    before_filter CoreSite::Authentication::Filter, :except => [:logo, :session_expired]
  end

  before_filter :set_current_user, :set_time_zone, :set_mode, :set_path, :set_path_owner, :set_component_html_id, :set_reverse_diagram, :set_graphs_to_compare, :except => [:logo, :session_expired, :login_text]
  # Wanted this in CustomApplicationController (coresite_extensions) but it was being run too late, need it run here before get_paths_and_ennis
  before_filter :coresite_init, :except => :session_expired if App.is?(:coresite)
  before_filter :get_paths_and_ennis, :set_search_filter_params, :user_login_stats, :appstats_login, :appstats_page_counter, :except => [:logo, :session_expired, :login_text, :map_tooltip, :map_info_window, :show]
  before_filter :google_api_setup

  after_filter :set_hover_token
  
  layout :compute_layout

  helper :all # include all helpers, all the time
  # Must be turned off for CAS authenication
  # protect_from_forgery # See ActionController::RequestForgeryProtection for details

  helper_method :current_user, :member_attrs_hash, :member_handle, :provider_member_handles, :other_provider_member_handles, :yes_no, :numeric?, :osl_app, :entity_is_not_redacted?
  helper_method :coresite_role

  DATE_FORMAT = '%Y-%m-%d'
  HISTORY_PAGE_SIZE = 10
  THRESHOLD_PERCENTAGE = 0.75

  MINOR_THRESHOLD_COLOR = '#fccd06'
  MAJOR_THRESHOLD_COLOR = '#ff8d20'

  rescue_from CanCan::AccessDenied do |exception|
    render file: "#{Rails.root}/public/403", formats: [:html], status: 403, layout: false
  end

  def index
    # dynamically redirect root_path based on preference or privilege
    if @current_user.login_count == 1
      redirect_to edit_user_url(@current_user), notice: 'Please change your default password.'
    elsif @current_user.homepage != root_path
      redirect_to @current_user.homepage
    elsif can? :read, :network
      redirect_to network_index_url
    elsif can? :show, :dashboards_menu
      redirect_to dashboard_url
    elsif can? :show, :reports_menu
      redirect_to report_url      
    elsif can? :read, ServiceProvider
      redirect_to service_providers_path
    else
      redirect_to edit_user_url(@current_user)      
    end
  end
  
  def google_api_setup
    @google_js_api = if Sin::Application.config.use_http_for_jsapi
      "http://www.google.com/jsapi"
    else
      "https://www.google.com/jsapi"
    end
    @google_maps_api = "https://maps.googleapis.com/maps/api/js?client=#{Sin::Application.config.client_id_for_maps}&sensor=false&v=3.8"
  end

  # Required for declarative authorization
  def set_current_user
    if $cas_server_enabled || $coresite_authentication
      @current_user = User.find_by_email(session['cas_user'])
    else
      # ["user@samsung.com"].each do |default|
      ["steve.rycroft@cenx.com","admin@sprint.com", "admin@verizon.com", "admin@cenx.com", "admin@alu.com","admin@att.com"].each do |default|
        session['cas_user'] = default
        @current_user = User.find_by_email(session['cas_user'])
        break unless @current_user.nil?
      end
    end

    if @current_user
      if @current_user.service_provider.nil?
        # Make sure there is a service provider for this user in cdb db
        return internal_server_error
      else
        session['service_provider_id'] = @current_user.service_provider.id
      end
    elsif $cas_server_enabled
      # Removed invalid reference to @current_user.homepage_url(request), as @current_user is nil
      CASClient::Frameworks::Rails::Filter.logout(self)
    end
  end
  
  def appstats(action,contexts)
    service_provider = current_user.service_provider.nil? ? nil : current_user.service_provider.name
    common_contexts = { :user_email => current_user.email, :service_provider => service_provider }
    Appstats::Logger.entry(action, common_contexts.merge(contexts))
  end  

  def user_login_stats
    return unless logged_in?
    return unless session[:already_logged_in].nil?
    if $cas_server_enabled
      @current_user.touch(:last_login_at)
      @current_user.increment!(:login_count)
    end
  end

  def appstats_login
    return unless logged_in?
    return unless session[:already_logged_in].nil?
    if $cas_server_enabled
      action = 'login'
      ticket = session[:cas_last_valid_ticket]
      return if ticket.nil?
    else
      action = 'local-login'
      session[:cas_local_ticket] ||= Random.rand(10000)
      ticket = session[:cas_local_ticket]
      return if ticket.nil?
    end
    session[:already_logged_in] = true
    appstats(action,:ticket => ticket)
  end

  def logged_in?
    current_user
  end

  def current_user
    @current_user
  end
  
  def current_user= user
    @current_user = user
  end

  def mode
    @mode
  end

  def path_owner
    @path_owner
  end

  def set_path_owner
    # possible values: 'user', 'admin' 'other', 'all'
    @path_owner = params[:path_owner] || 'all'

    if current_user.system_admin?
      @path_owner = 'admin'
    end
  end

  def set_mode
    @exporting = true if request.format == 'csv'
    
    @mode = if params[:mode]
      APP_MODES.include?(params[:mode]) ? params[:mode] : nil
    elsif params[:action]
      APP_MODES.include?(params[:action]) ? params[:action] : nil
    end

    # Use default mode if no mode explicitly set
    @mode = @current_user.default_mode if @mode.nil?    

    @monitoring_mode = true if @mode == 'monitoring'
    
    params[:mode] = @mode
  end

  def set_component_html_id
    @component_html_id = params[:component_html_id]
  end
  
  def set_reverse_diagram
    @reverse_diagram = params[:reverse_diagram].present? && params[:reverse_diagram] == 'true'
  end
  
  def set_search_filter_params

    if App.is?(:coresite) 
      @coresite_national_tenant_select = ServiceProvider.order(:name)
    end

    @service_provider_select = Path.s_service_provider_counts(@paths_all, @site_id, {})

    @service_provider_select = @service_provider_select.concat(@ennis_all.s_service_provider_counts)
    @service_provider_select.uniq! {|sp| sp.service_provider_id}
    
    @site_select = @paths_all.s_agg_site_counts(@service_provider_id)
    @site_select = @site_select.concat(@ennis_all.s_agg_site_counts)
    @site_select.uniq! {|site| site.site_id}
        
    # Statement of Work select
    @statement_of_work_select = BulkOrder.select('service_orders.title, service_orders.id').
                                          where(['service_orders.bulk_order_type = :bulk_order_type', {:bulk_order_type => 'Statement of Work'}])
    
    unless @current_user.system_admin?
      @statement_of_work_select = @statement_of_work_select.joins(:operator_network).where(:operator_networks => {:service_provider_id => @current_user.service_provider.id})
    end

    # OEM select
    if @current_user.system_admin?
      @oem_select = OperatorNetworkType.order(:name)
    else
      @oem_select = @current_user.operator_network_types.order(:name)
    end
    
    
  end
  
  def set_path
    @path = find_path(params[:path_id]) if params[:path_id]
    @metric = @path.layer == 3 ? 'Packet' : 'Frame' if @path
  end

  def set_time_zone
    Time.zone = params['time_zone'] ? params['time_zone'] : @current_user.time_zone
  end

  # CoreSite
  def coresite_init
    @current_national_tenant = @current_user.service_provider
    # Limit tenants to an operator network
    if coresite_role
      @tenant_operator_network = params[:operator_network] unless params[:operator_network].blank?
    else
      params[:operator_network] = session[:tenant]
      params[:service_provider] = session[:service_provider_id]
      @current_operator_network = OperatorNetwork.find(session[:tenant])
    end
    @national_tenant = ServiceProvider.find(params[:service_provider]) if params[:service_provider].present? && numeric?(params[:service_provider])
  end

  def coresite_role
    if App.is?(:coresite)
      @coresite_role = @current_user.service_provider.is_system_owner? if @coresite_role.nil? 
    else
      @coresite_role = true
    end
    @coresite_role
  end

  # Get all member attributes (except member handles) and return hash of label, value
  def member_attrs_hash(entity)
    entity.member_attr_instances.reject{|m| m.is_a?(MemberHandleInstance)}.map{|m| {:label => m.member_attr.name, :value => m.value} }
  end

  # Member handles by provider, ie AT&T's ID: AUXB100
  def provider_member_handles(entity)
    entity.member_handles.map { |mh|
      # SPRINT HACK: Ignore TBD and -, validaiton should allow blank instead and data be migrated
      {:label => "#{mh.service_provider.name}'s ID", :value => mh.value, :service_provider_id => mh.service_provider.id} unless mh.value.blank? || mh.value.strip.upcase == 'TBD' || mh.value.strip == '-' || mh.service_provider.nil?
    }.compact
  end

  # Member handles that are not owned by current user
  def other_provider_member_handles(entity)
    provider_member_handles(@order.path).reject{ |mh| mh[:service_provider_id] == @current_user.service_provider.id }
  end

  # Just get a member handle, if none for current user then return some other providers member handle or cenx id
  def member_handle(entity)
    provider_member_handles(entity).first.try(:[],:value)
  end

  # Convert boolean to Yes or No
  def yes_no(boolean)
    boolean ? 'Yes' : 'No'
  end

  # shared actions
  def show
  end

  # Update provisioning state of Segments
  def provisioning_state_update

    if coresite_role
      pre_result, post_result, pre_objs, post_objs = @entity.prov_state.set_state(params[:new_state].constantize)
      error_string = ""
      if !pre_result
        error_string += "Failed to change state due to objects "
        pre_objs.each do |obj|
          obj_identifier = ""
          obj_identifier = obj[:obj].name if obj[:obj].respond_to? :name
          obj_identifier = obj[:obj].cenx_id if obj[:obj].respond_to? :cenx_id

          error_string += "#{obj[:obj].class.to_s}:#{obj_identifier} #{obj[:reason]}\n"
        end
      elsif !post_result
        error_string += "State changed but post conditions failed due to objects "
        post_objs.each do |obj|
          obj_identifier = ""
          obj_identifier = obj[:obj].name if obj[:obj].respond_to? :name
          obj_identifier = obj[:obj].cenx_id if obj[:obj].respond_to? :cenx_id
          error_string += "#{obj[:obj].class.to_s}:#{obj[:obj].cenx_id} #{obj[:reason]}\n"
        end
      end

      message = error_string.blank? ? {:text => 'State updated successfully', :type => :successful} : {:text => error_string, :type => :error}
      respond_to do |format|
        format.html { render :partial => 'shared/provisioning_state_update', :locals => {:segment => @entity, :message => message, :edit => true} }
      end
    else
      forbidden
    end    
  end

  # Monitoring bubble
  def monitoring

    # Get entites for each graphing tab
    @graph_tabs = graph_tabs

    # Start and end date range for graphs
    initialize_graph_dates
    
    # End Time for live events table
    @end_time = RangeDate.new(params[:time], :now)

    render 'shared_monitoring/monitoring'

  end

  # Ordering bubble
  def ordering
    @order = initialize_order
    @osl_order = initialize_order_forms
    @ordering_forms =  @order && @osl_order ? (render_to_string :partial => 'shared/ordering_forms').html_safe : nil
    render 'shared/ordering'
  end
  
  # update an order field
  def ordering_field_update
    begin
      @order = @entity.respond_to?(:get_latest_order) ? @entity.get_latest_order : nil
      case params['field']
      when "D/TREC" 
        datetime = DateTime.strptime(params[:value], '%Y-%m-%d-%H%M%p').to_time
        @order.order_received_date = datetime
        @order.save
      when "SECLOC"
        @entity.far_side_clli = params[:value]
        @entity.save
      end
    rescue Exception => e
      render :text => e.message, :status => 500
    else
      render :text => 'Successful'
    end
  end
  
  def ordering_state_edit
    @order = initialize_order
    if @order
      # Wrap render partial in format.html so content is sent back (bug in Rails 3.2.2, https://github.com/rails/rails/issues/5238)
      respond_to do |format|
        format.html { render :partial => 'shared/ordering_state', :locals => {:order => @order, :edit => true, :history =>true} }
      end
    else
      render :text => 'No order'
    end    
  end
  
  def ordering_attributes
    @order = initialize_order
    @osl_order = initialize_order_forms
    respond_to do |format|
      format.html { render :partial => 'shared/ordering_forms' }
    end
  end

  def ordering_output
    @order = initialize_order
    @osl_order = initialize_order_forms
    respond_to do |format|
      format.html {render :partial => 'shared/ordering_forms' }
    end
  end

  def order_history

    mode = @entity.type == "Uni" ? "uni" : "evc"
    osl_app.cmd("tail #{@path.member_handle}.#{mode}.#{ts}")

    history = chris_app("order.#{@path.cascade_id}.#{@entity.id}").cmd("tail")[:data]
    return [] if history.nil?
    history.collect do |entry|
      should_hide = false
      [ "appid", "ruby ChrisApp", "sub ls", "sub process", "reset", "eve " ].each do |hidden|
        if entry.start_with?(hidden)
          should_hide = true
          break          
        end
      end

      if should_hide
        nil
      elsif !entry.index("password").nil?
        "*** FILTERED OUT PASSWORD ***"
      else
        entry
      end
    end.compact
  end

  def order_mode
    @entity.type == "Uni" ? "uni" : "evc"
  end

  def cascade
    @path.member_handle
  end

  def ordering_audit
    @cm_dir = cmaudit_app.cmd("cm dir",false)[:data]
    @cascade = @path.member_handle
    @order_type = @entity.type == "Uni" ? "uni" : "evc"
    @service_order_data = osl_app.cmd("att ls-#{@order_type} #{@cascade}")[:data] || { :timestamps => [] }

    @order_history = {} 
    @service_order_data[:timestamps].each do |ts|
      @order_history[ts] = osl_app.cmd("tail #{@cascade}.order-#{@order_type}.#{ts}")[:data].collect do |entry|
        if !entry.index("password").nil?
          "*** FILTERED OUT PASSWORD ***"
        else
          entry
        end
      end.compact
    end

    respond_to do |format|
      format.html {render :partial => 'shared/ordering_audit' }
    end
  end

  # TODO: Fix up export graphs to work again
  def export_graphs
    
    @exporting = true

    initialize_graph_dates
    @stats = @entity.stats_data(@graph_date_range_start.to_i, @graph_date_range_end.to_i)
    
    # Make sure we are not missing data points, fill with nil
    @stats.pad_results = true

    respond_to do |format|
      format.csv { graphs_to_csv }
    end
    
  end

  # Paginated event history for monitoring
  def event_history

    # Pagination page
    page = (params[:page] && params[:page].to_i) || 1

    @end_time = RangeDate.new(params[:time], :now)

    # Set start and end times for date range
    initialize_event_dates

    if sm_history = @entity.sm_histories.first
      all_records = sm_history.alarm_records.from_date(@event_date_range_start.to_ms).to_date(@event_date_range_end.to_ms).reverse
    else
      all_records = []
    end

    @history_without_paging = all_records.to_a.slice((page-1)*HISTORY_PAGE_SIZE, HISTORY_PAGE_SIZE)

    @history = WillPaginate::Collection.create(page, HISTORY_PAGE_SIZE, all_records.size) do |pager|
      pager.replace(@history_without_paging)
    end

    render 'shared_monitoring/event_history', :layout => false
  end

  def sm_state
    render 'shared/monitoring_state'
  end

  def ordering_state
    render 'shared/ordering_state'
  end

  def availability_graph
    respond_to do |format|
      @data = add_unavailable_and_maintenance(availability_graph_json_view)
      format.json { render :json => @data }
      format.pdf { render :pdf => "availability_graph", :template => "/shared_monitoring/graph", :layout => 'pdf.html', :disposition => 'attachment'}
      format.html { render :template => "/shared_monitoring/graph", :layout => 'pdf.html'}
      format.csv { graphs_to_csv(@data) }
    end
  end

  def frame_loss_ratio_graph
    respond_to do |format|
      @data = add_unavailable_and_maintenance(frame_loss_ratio_graph_json_view)

      format.json { render :json => @data }
      format.pdf { render :pdf => "frame_loss_ratio_graph", :template => "/shared_monitoring/graph", :layout => 'pdf.html', :disposition => 'attachment'}
      format.csv { graphs_to_csv(@data) }
    end
  end

  def delay_graph
    respond_to do |format|
      @data = add_unavailable_and_maintenance(delay_graph_json_view(:delay))

      format.json { render :json => @data }
      format.pdf { render :pdf => "delay_graph", :template => "/shared_monitoring/graph", :layout => 'pdf.html', :disposition => 'attachment'}
      format.csv { graphs_to_csv(@data) }
    end
  end

  def delay_variation_graph
    respond_to do |format|
      @data = add_unavailable_and_maintenance(delay_graph_json_view(:delay_variation))

      format.json { render :json => @data }
      format.pdf { render :pdf => "delay_variation_graph", :template => "/shared_monitoring/graph", :layout => 'pdf.html', :disposition => 'attachment'}
      format.csv { graphs_to_csv(@data) }
    end
  end

  def troubleshooting_graph
    respond_to do |format|
      @data = add_unavailable_and_maintenance(troubleshooting_graph_json_view)

      format.json { render :json => @data }
      format.pdf { render :pdf => "troubleshooting_graph", :template => "/shared_monitoring/graph", :layout => 'pdf.html', :disposition => 'attachment'}
      format.csv { graphs_to_csv(@data) }
    end
  end

  protected

  def fulltext_search
    
    sphinx_search, @is_filtered = params[:filter].blank? ? ["", false] : [params[:filter].to_s, true]
    with_options = {}
    
    # Filter evc list by enni (ie list all evcs that go through the specified enni)
    unless params[:enni].blank?
      with_options[:enni_ids] = params[:enni]
      @enni = EnniNew.find(params[:enni])
      @is_filtered = true
    end

    # filter by evc list by evc (ie list all evcs that use the specified tunnel)
    unless params[:has_evc].blank?
      with_options[:has_path_ids] = params[:has_evc]
      @has_evc = Evc.find(params[:has_evc])
      @is_filtered = true
    end
    
    # only use sphinx if user supplied a filter
    if @is_filtered  
      # TODO need a better way todo monitoring states, the following solution will not scale and is only temporary
      sphinx_results = path_sphinx.search_for_ids(sphinx_search, 
                                                 :populate => true,
                                                 :match_mode => :all,
                                                 :with => with_options,
                                                 :per_page => 1000, # Get as many as we can
                                                 :group => 'path_id')

      # Do not want to instantiate SinPathServiceProviderSphinx objects just to get path_id                                   
      SinPathServiceProviderSphinx.where(:id => sphinx_results).pluck('path_id')
    else
      nil
    end
    
  end

  def cas_filter 
    if $cas_server_enabled
      CASClient::Frameworks::Rails::Filter.filter(self)
    end
  end 

  def get_paths_and_ennis

    @cenx_path_type = params[:cenx_path_type]
    @site_id = params[:site]
    @operator_network = OperatorNetwork.find(params[:operator_network]) unless params[:operator_network].blank?
    @site_group_id = params[:site_group]
    @sow_id = params[:statement_of_work]
    @service_provider_id = params[:service_provider]
    @ordering_state = params[:ordering_state]
    @provisioning_state = params[:provisioning_state]
    @monitoring_state = params[:monitoring_state]
    @operator_network_type_id = params[:oem]
    @fallout_exception_type = params[:fallout_exception_type]
    @fallout_category = params[:fallout_category]
    @filter_params = request.GET.merge({'mode' => params[:mode]})
    @paths_all = Path.find_paths(@path_owner, @current_user)
    @ennis_all = EnniNew.find_ennis(@path_owner, @current_user)
    
    @paths = Path.find_paths(@path_owner, @current_user, @service_provider_id, @site_id, @cenx_path_type, @ordering_state, @provisioning_state, @monitoring_state, @sow_id, @operator_network_type_id, @fallout_exception_type, @fallout_category, @path_ids, @site_group_id,  @operator_network.try(:id))
    @ennis = EnniNew.find_ennis(@path_owner, @current_user, @service_provider_id, @site_id, @ordering_state, @provisioning_state, @monitoring_state, @operator_network_type_id)
    @sub_ennis = EnniNew.find_ennis(@path_owner, @current_user, @service_provider_id, @site_id, @ordering_state, @provisioning_state, @monitoring_state, @operator_network_type_id, false)
    @mon_ennis = EnniNew.find_ennis(@path_owner, @current_user, @service_provider_id, @site_id, @ordering_state, @provisioning_state, @monitoring_state, @operator_network_type_id, true)

  end

  def find_path(id)
    Path.find_paths('all', @current_user).where(:paths => {:id => id}).first
  end

  # Searching needs to be scoped by user
  def path_sphinx
    SinPathServiceProviderSphinx.by_user(current_user)
  end

  def find_enni(id)
    sin_enni = @current_user.system_admin? ? SinEnniServiceProviderSphinx : SinEnniServiceProviderSphinx.with_provider(@current_user.service_provider.id)
    sin_enni.where(:enni_id => id).first.try(:enni)
  end

  # Searching needs to be scoped by user
  def enni_sphinx
    SinEnniServiceProviderSphinx.by_user(current_user)
  end
  
  def initialize_order
    @entity.respond_to?(:get_latest_order) ? @entity.get_latest_order : nil
  end

  def osl_app
    @osl_app = Eve::Application.new(:config => "#{File.dirname(__FILE__)}/../../config/osl.poe.yml") if @osl_app.nil?
    @osl_app
  end

  def cmaudit_app
    @cmaudit_app = Eve::Application.new(:config => "#{File.dirname(__FILE__)}/../../config/cmaudit.yml") if @cmaudit_app.nil?
    # @cmaudit_app.cmd("cm-meta audit yoigo") # HACK -- audit target not loading properly for yoigo branch
    @cmaudit_app
  end

  def chris_app(id)
    Eve::Application.new(:config => "#{File.dirname(__FILE__)}/../../config/chris.yml", :id => id) 
  end

  def initialize_order_forms
    Osl::Order.new(:app => osl_app, :entity => @entity, :latest_order => @order, :evc => @path) if @order
  end

  def compute_layout
     !['dashboards', 'coresite_orders', 'coresite_national_tenants'].include?(controller_name) && %w(ordering show monitoring sm_state ordering_state ordering_field_update).include?(action_name) ? "bubble" : "application" 
  end

  # Get SM state for monitoring
  def get_SM_state_hash(model_object)

    # do not get monitoring view data unless we are on the monitoring view
    return {} unless @monitoring_mode

    state, details, timestamp = model_object.get_SM_state

    {
      :notification_type => state,
      :details => details,
      :datestamp => timestamp
    }

  end

  # Graphing methods ************************

  # Convert graphs (hashes) to csv
  def graphs_to_csv(data)
    export_file_name = "#{@entity.respond_to?(:short_name) ? @entity.short_name : "#{@monitoring_title} Graphs"}-#{action_name}"
    
    csv_string = CSV.generate do |csv|
      # Each support type of graph may actually be multiple sets of graph
      data[:graphs].each do |graph_set|
        # export each graph in graph set
        graph_set.each do |graph|
          # Add Column headings
          csv << graph[:data][:cols].map{ |col| col[:label] }
          # Add each column of each row to csv
          graph[:data][:rows].each do  |row| 
            csv << row[:c].map{ |col| col[:v] }
          end
          csv << []
        end

        csv << ['Date Range Start:', @graph_date_range_start]
        csv << ['Date Range End:', @graph_date_range_end]
      end
    end
    
    # send it to the browsah
    send_data csv_string,
              :type => 'text/csv; charset=utf-8; header=present',
              :disposition => "attachment; filename=#{export_file_name}.csv"
    
  end


  def availability_graph_json_view

    # Initialize cos_name, service id, date range
    initialize_graph_inputs

    # Get availability data
    availability_data = @stats.stats_availability_results(AlarmSeverity::UNAVAILABLE)
    availability_data_points = availability_data[:data]

    rows = []
    availability_data_points.each do |point|
      rows << {:c => [{:v => javascript_time(point[:x]), :r => point[:x]}, {:v => point[:y]}]}
    end

    aggregate_formatted, aggregate, sla, cell_class, cell_title = availability_aggregate_formatted(availability_data)

    # Define columns, colors and legend
    cols = [{:id => 'date', :label => 'Date', :type =>'datetime'},
            {:id => 'availability', :label => 'Available', :type => 'number', :color => '#2648BF'}]

    # Return data points in json format for google visualizations to plot
    {
      :graphs => [[{:type => 'dygraph',
                    :name => 'availability',
                    :title => "Availability",
                    :legend => graph_legend(cols),
                    :colors => series_colors(cols),
                    :data => {
                      :cols => cols,
                      :rows => rows
                    }
                   },
                   {:type => 'table',
                    :data => {
                      :cols =>[{:label => 'Description', :type => 'string'},
                               {:label => 'Percentage', :type => 'number'}],
                      :rows =>[{:c => [{:v => 'Guaranteed Monthly (SLA):', :p => {:css_class => "label"}},  {:v => sla, :f => "#{sla}%"}]},
                               {:c => [{:v => 'Avg. for period:', :p => {:css_class => "label"}},  {:v => aggregate, :f => aggregate_formatted, :p => {:css_class => cell_class, :title => cell_title}}]}]
                    }
                   }]]
    }

  end

  def frame_loss_ratio_graph_json_view

    dashed_line = [7, 3]

    # Initialize cos_name, service id, date range
    initialize_graph_inputs

    # Set interval at which to retrieve data points
    @stats.stats_set_collection_interval(@interval)

    # Get Frame or Packet loss data
    normalized_data = @stats.stats_get_normalized_flr_data
    
    flr_data = @stats.stats_flr_results(normalized_data)

    # Get data points for frame loss ratio
    flr_data_for_cos = flr_data[@cos_name]
    flr_data_points  = flr_data_for_cos[:data].fetch("Total", [])

    if flr_data_for_cos[:data]["ETH"].present?
      ref_flr_data_points = flr_data_for_cos[:data]["ETH"]
      ref_flr_data = ref_flr_data_points.present?
    end

    if flr_data_for_cos[:data]["ETH_delta"].present?
      ref_delta_flr_data_points = flr_data_for_cos[:data]["ETH_delta"]
      ref_delta_flr_data = ref_delta_flr_data_points.present?
    end

    if flr_data_for_cos[:data]["LastHop_delta"].present?
      lasthop_delta_flr_data_points = flr_data_for_cos[:data]["LastHop_delta"]
      lasthop_delta_flr_data = lasthop_delta_flr_data_points.present?
    end

    if flr_data_for_cos[:ref].present? && flr_data_for_cos[:ref][:reference].present?
      turnup_test = flr_data_for_cos[:ref][:reference]
    end

    high_data_point = turnup_test ? turnup_test : 0

    # generate rows for google graphs
    rows = []
    flr_data_points.each_with_index do |point, index|
      row = [{:v => javascript_time(point[:x])}]

      if lasthop_delta_flr_data
        lasthop_delta_point = lasthop_delta_flr_data_points[index]
        row << {:v => lasthop_delta_point[:y]}
      end

      row << {:v => point[:y]}

      if ref_delta_flr_data
        ref_delta_point = ref_delta_flr_data_points[index]
        row << {:v => ref_delta_point[:y]}
      end

      # if reference circuit then plot on same graph
      if ref_flr_data
        ref_point = ref_flr_data_points[index]
        row << {:v => ref_point[:y]}
      end

      # Add row
      rows << {:c => row}

      # Get maximum data point for set
      high_data_point = [point[:y], (ref_flr_data ? ref_point[:y] : nil), 
                         (ref_delta_flr_data ? ref_delta_point[:y] : nil), 
                         (lasthop_delta_flr_data ? lasthop_delta_point[:y] : nil), 
                         high_data_point].compact.max

    end

    aggregate_formatted, aggregate, sla, minor_threshold, major_threshold, cell_class, cell_title = frame_loss_aggregate_formatted flr_data_for_cos

    # Define columns, colors and legend
    cols = [{:id => 'date', :label => 'Date', :type =>'datetime'}]

    if lasthop_delta_flr_data
      cols << {:id => 'lasthop-delta-frame-loss-ratio', :label => 'Previous Hop', :type => 'number', :color => '#02AD1A'}
    end

    cols << {:id => 'frame-loss-ratio', :label => 'Total', :type => 'number', :color => '#000000'}

    if ref_delta_flr_data
      cols << {:id => 'reference-delta-frame-loss-ratio', :label => get_term(:microwatt), :type => 'number', :color => '#004FFF'}
    end

    if ref_flr_data
      cols << {:id => 'reference-frame-loss-ratio', :label => get_term(:aav), :type => 'number', :color => '#6F3700'}
    end

    # Display SLA in table (beside graph)
    table_rows = [{:c => [{:v => 'Guaranteed Monthly (SLA):', :p => {:css_class => "label"}},  {:v => sla, :f =>  exponential_notation(sla) + '%'}]},
                  {:c => [{:v => 'Avg. for period:', :p => {:css_class => "label"}},  {:v => aggregate, :f => aggregate_formatted, :p => {:css_class => cell_class, :title => cell_title}}]}]


    if minor_threshold
      cols << {:id => 'minor_threshold', :label => 'Minor Threshold', :type => 'number', :color => MINOR_THRESHOLD_COLOR, :dashed_line => dashed_line, :hidden_threshold => not(high_data_point > minor_threshold.to_i * THRESHOLD_PERCENTAGE || @force_threshold)}
      rows.map { |row| row[:c] << {:v => minor_threshold} }
      table_rows << {:c => [{:v => 'Minor Threshold:', :p => {:css_class => "label"}}, {:v => minor_threshold, :f => exponential_notation(minor_threshold) + '%'}]}
    end

    if major_threshold
      cols << {:id => 'major_threshold', :label => 'Major Threshold', :type => 'number', :color => MAJOR_THRESHOLD_COLOR, :dashed_line => dashed_line, :hidden_threshold => not(high_data_point > major_threshold.to_i * THRESHOLD_PERCENTAGE || @force_threshold)}
      rows.map { |row| row[:c] << {:v => major_threshold} }
      table_rows << {:c => [{:v => 'Major Threshold:', :p => {:css_class => "label"}}, {:v => major_threshold, :f => exponential_notation(major_threshold) + '%'}]}
    end

    if turnup_test
      cols << {:id => 'turnup_test', :label => 'Turnup Test', :type => 'number', :color => '#CE0FB8', :dashed_line => dashed_line}
      rows.map { |row| row[:c] << {:v => turnup_test} }
    end

    details_series_array = details_series(cols)
    hidden_threshold_series_array = hidden_threshold_series(cols)

    # Return data points in json format for google visualizations to plot
    # Handle multiple sets of graphs
    {
      :graphs => [[{:type => 'dygraph',
                    :name => 'frame_loss_ratio',
                    :title => "#{@metric} Loss Ratio",
                    :legend => graph_legend(cols),
                    :colors => series_colors(cols),
                    :per_series_customizations => per_series_customizations(cols),
                    :details_series => details_series_array,
                    :hidden_threshold_series => hidden_threshold_series_array,
                    :visibility => series_visibility(cols, details_series_array, hidden_threshold_series_array),
                    :data => {
                      :cols => cols,
                      :rows => rows
                    }
                   },
                   {:type => 'table',
                    :data => {
                      :cols =>[{:label => 'Description', :type => 'string'},
                               {:label => 'Percentage', :type => 'number'}],
                      :rows => table_rows
                    }
                   }]]
    }


  end

  def delay_graph_json_view(delay_type)

    # Initialize cos_name, service id, date range
    initialize_graph_inputs
    graphs = []
    paired_test = 'ETH'
    dashed_line = [7, 3]

    # Type of delay graph
    type = (delay_type == :delay ? "#{@metric} Delay (one-way)" : "#{@metric} Delay Variation (one-way)")

    # Set interval at which to retrieve data points
    @stats.stats_set_collection_interval(@interval)

    # List delay attributes available
    delay_attributes = [delay_type] +  @stats.get_delay_attributes(:troubleshooting)
    min_delay_attr, max_delay_attr = delay_type == :delay ? [:min_delay, :max_delay] : [:min_delay_variation, :max_delay_variation]

    # get raw data for types of data requested (ie delay (avg, min, max), delay variation (avg, min, max))
    raw_data = @stats.stats_get_delay_data(delay_attributes)

    # Pass in true so method returns data even if there is no SLA
    delay_data = @stats.stats_delay_results(raw_data, delay_type, true)

    min_data = delay_attributes.include?(min_delay_attr) ? @stats.stats_delay_results(raw_data, min_delay_attr, true) : nil
    max_data = delay_attributes.include?(max_delay_attr) ? @stats.stats_delay_results(raw_data, max_delay_attr, true) : nil

    # Service level agreement (to be plotted against sam data)
    turnup_test = delay_data[:ref].try(:[], @cos_name).try(:[], :reference).to_f
    turnup_test_min_data = min_data.try(:[], :ref).try(:[], @cos_name).try(:[], :reference).to_f
    turnup_test_max_data = max_data.try(:[], :ref).try(:[], @cos_name).try(:[], :reference).to_f

    aggregate_formatted, aggregate, sla, minor_threshold, major_threshold, cell_class, cell_title = delay_aggregate_formatted delay_data, type

    # Total Data
    total_delay_data = nil
    if delay_data[:data][paired_test]
      total_delay_data = delay_data[:data].delete('Total')
    else
      total_delay_data = nil
    end

    total_min_data = nil
    if min_data.try(:[], :data).try(:[], paired_test)
      total_min_data = min_data[:data].delete('Total')
    end

    total_max_data = nil
    if max_data.try(:[], :data).try(:[], paired_test)
      total_max_data = max_data[:data].delete('Total')
    end

    # Reference Delta Data
    ref_delta_delay_data = delay_data.try(:[], :data).try(:delete, 'ETH_delta')
    ref_delta_min_data = min_data.try(:[], :data).try(:delete, 'ETH_delta')
    ref_delta_max_data = max_data.try(:[], :data).try(:delete , 'ETH_delta')

    # Last Hop Delta Data
    lasthop_delta_delay_data = delay_data.try(:[], :data).try(:delete, 'LastHop_delta')
    lasthop_delta_min_data = min_data.try(:[], :data).try(:delete, 'LastHop_delta')
    lasthop_delta_max_data = max_data.try(:[], :data).try(:delete, 'LastHop_delta')

    # Map each test as separate graph
    delay_data[:data].each do |test, cos|

      # Get data points for min and max series
      min_data_for_cos = min_data[:data][test][@cos_name] if min_data
      max_data_for_cos = max_data[:data][test][@cos_name] if max_data
      ref_delta_min_data_for_cos = ref_delta_min_data[@cos_name] if ref_delta_min_data
      ref_delta_max_data_for_cos = ref_delta_max_data[@cos_name] if ref_delta_max_data
      ref_delta_avg = ref_delta_delay_data[@cos_name] if ref_delta_delay_data
      lasthop_delta_min_data_for_cos = lasthop_delta_min_data[@cos_name] if lasthop_delta_min_data
      lasthop_delta_max_data_for_cos = lasthop_delta_max_data[@cos_name] if lasthop_delta_max_data
      lasthop_delta_avg = lasthop_delta_delay_data[@cos_name] if lasthop_delta_delay_data
      total_min_data_for_cos = total_min_data[@cos_name] if total_min_data
      total_max_data_for_cos = total_max_data[@cos_name] if total_max_data
      total_avg = total_delay_data[@cos_name] if total_delay_data

      test_name = test == paired_test ? get_term(:aav) : test

      high_data_point = 0

      # init columns to be displayed in graph
      cols = [{:id => 'date', :label => 'Date', :type =>'datetime'}]

      cols << {:id => 'lasthop-delta-frame-delay-min', :label => "Previous Hop Min", :type => 'number', :color => '#72DD7C', :details => true} if lasthop_delta_min_data
      cols << {:id => 'lasthop-deltal-frame-delay-max', :label => "Previous Hop Max", :type => 'number', :color => '#35C443', :details => true} if lasthop_delta_max_data
      cols << {:id => 'total-frame-delay-min', :label => "Total Min", :type => 'number', :color => '#9E9E9E', :details => true} if total_min_data
      cols << {:id => 'total-frame-delay-max', :label => "Total Max", :type => 'number', :color => '#727271', :details => true} if total_max_data
      cols << {:id => 'ref-delta-frame-delay-min', :label => "#{get_term(:microwatt)} Min", :type => 'number', :color => '#B9CAFF', :details => true} if ref_delta_min_data
      cols << {:id => 'ref-deltal-frame-delay-max', :label => "#{get_term(:microwatt)} Max", :type => 'number', :color => '#8EB0FF', :details => true} if ref_delta_max_data
      cols << {:id => 'frame-delay-min', :label => "#{test_name} Min", :type => 'number', :color => '#B15602', :details => true} if min_data
      cols << {:id => 'frame-delay-max', :label => "#{test_name} Max", :type => 'number', :color => '#944901', :details => true} if max_data

      cols << {:id => 'lasthop-delta-frame-delay', :label => "Previous Hop Avg", :type => 'number', :color => '#02A311'} if lasthop_delta_avg
      cols << {:id => 'total-frame-delay', :label => "Total Avg", :type => 'number', :color => '#000000'} if total_avg
      cols << {:id => 'ref-delta-frame-delay', :label => "#{get_term(:microwatt)} Avg", :type => 'number', :color => '#004FFF'} if ref_delta_avg
      cols << {:id => 'frame-delay', :label => "#{test_name} Avg", :type => 'number', :color => '#6F3700'} # add average last so displays on top of min and max
      
      rows = []

      # Sam returns all cos, we are only interested in one (defined by cos_name)
      cos[@cos_name].each_with_index do |avg, index|

        row = [{:v => javascript_time(avg[:x])}]
        row << {:v => lasthop_delta_min_data_for_cos[index][:y]} if lasthop_delta_min_data
        row << {:v => lasthop_delta_max_data_for_cos[index][:y]} if lasthop_delta_max_data
        row << {:v => total_min_data_for_cos[index][:y]} if total_min_data
        row << {:v => total_max_data_for_cos[index][:y]} if total_max_data
        row << {:v => ref_delta_min_data_for_cos[index][:y]} if ref_delta_min_data
        row << {:v => ref_delta_max_data_for_cos[index][:y]} if ref_delta_max_data
        row << {:v => min_data_for_cos[index][:y]} if min_data
        row << {:v => max_data_for_cos[index][:y]} if max_data

        row << {:v => lasthop_delta_avg[index][:y]} if lasthop_delta_avg
        row << {:v => total_avg[index][:y]} if total_avg
        row << {:v => ref_delta_avg[index][:y]} if ref_delta_avg
        row << {:v => avg[:y]}

        # Add row
        rows << {:c => row}

        # Get maximum data point for set
        high_data_point = [(max_data ? max_data_for_cos[index][:y] : avg[:y]), 
                           (ref_delta_max_data ? ref_delta_max_data_for_cos[index][:y] : ref_delta_avg ? ref_delta_avg[index][:y] : nil),
                           (lasthop_delta_max_data ? lasthop_delta_max_data_for_cos[index][:y] : lasthop_delta_avg ? lasthop_delta_avg[index][:y] : nil),
                           (total_max_data ? total_max_data_for_cos[index][:y] : total_avg ? total_avg[index][:y] : nil),
                           high_data_point].compact.max

      end

      # Display SLA in table (beside graph)
      table_rows = [{:c => [{:v => 'Guaranteed (SLA):', :p => {:css_class => "label"}},  {:v => sla, :f => sla == 0 ? 'None' : "#{number_with_delimiter(sla)} µs"}]}]
      if aggregate
        table_rows << {:c => [{:v => 'Avg. for period:', :p => {:css_class => "label"}},  {:v => aggregate, :f => aggregate_formatted, :p => {:css_class => cell_class, :title => cell_title}}]}
      end
      
      if minor_threshold
        cols << {:id => 'minor_threshold', :label => 'Minor Threshold', :type => 'number', :color => MINOR_THRESHOLD_COLOR, :dashed_line => dashed_line, :hidden_threshold => not(high_data_point > minor_threshold.to_i * THRESHOLD_PERCENTAGE || @force_threshold)}
        rows.map { |row| row[:c] << {:v => minor_threshold} }
        table_rows << {:c => [{:v => 'Minor Threshold:', :p => {:css_class => "label"}}, {:v => minor_threshold, :f => "#{minor_threshold} µs"}]}
      end

      if major_threshold
        cols << {:id => 'major_threshold', :label => 'Major Threshold', :type => 'number', :color => MAJOR_THRESHOLD_COLOR, :dashed_line => dashed_line, :hidden_threshold => not(high_data_point > major_threshold.to_i * THRESHOLD_PERCENTAGE || @force_threshold)}
        rows.map { |row| row[:c] << {:v => major_threshold} }
        table_rows << {:c => [{:v => 'Major Threshold:', :p => {:css_class => "label"}}, {:v => major_threshold, :f => "#{major_threshold} µs"}]}
      end

      if turnup_test && turnup_test > 0
        cols << {:id => 'turnup_test', :label => 'Turnup Test', :type => 'number', :color => '#CE0FB8', :dashed_line => dashed_line}
        rows.map { |row| row[:c] << {:v => turnup_test} }
      end

      if turnup_test_min_data && turnup_test_min_data > 0
        cols << {:id => 'turnup_test_min', :label => 'Turnup Test Min', :type => 'number', :color => '#CEAEC1', :dashed_line => dashed_line, :details => true}
        rows.map { |row| row[:c] << {:v => turnup_test_min_data} }
      end

      if turnup_test_max_data && turnup_test_max_data > 0
        cols << {:id => 'turnup_test_max', :label => 'Turnup Test Max', :type => 'number', :color => '#CE97C1', :dashed_line => dashed_line, :details => true}
        rows.map { |row| row[:c] << {:v => turnup_test_max_data} }
      end

      table_rows << {:c => [{:v => 0, :f => ''}]}

      details_series_array = details_series(cols)
      hidden_threshold_series_array = hidden_threshold_series(cols)

      # push graph set to graphs array
      graphs.push([{:type => 'dygraph',
                    :name => delay_type.to_s,
                    :title => type,
                    :legend => graph_legend(cols),
                    :colors => series_colors(cols),
                    :per_series_customizations => per_series_customizations(cols),
                    :details_series => details_series_array,
                    :hidden_threshold_series => hidden_threshold_series_array,
                    :visibility => series_visibility(cols, details_series_array, hidden_threshold_series_array),
                    :data => {
                      :cols => cols,
                      :rows => rows
                     }
                   },
                   {:type => 'table',
                    :data => {
                      :cols =>[{:label => 'Description', :type => 'string'},
                               {:label => 'Microseconds', :type => 'number'}],
                      :rows => table_rows
                    }
                   }
                  ])

    end

    # If no graphs then put in default one so we have a title
    if graphs.empty?
      graphs.push([{:type => 'dygraph',
                    :name => delay_type.to_s,
                    :title => type,
                    :data => {:cols => [],:rows => []}
                   }
                  ])
    end

    # Return data points in json format for google visualizations to plot
    {:graphs => graphs}

  end

  def troubleshooting_graph_json_view

    # Initialize cos_name, service id, date range
    initialize_graph_inputs
    graphs = []

    # Set interval at which to retrieve data points
    @stats.stats_set_collection_interval(@interval)

    # Troubleshooting graphs for ENNI
    if @enni
      # No dropped counts for ENNI
      normalized_data = @stats.stats_get_enni_count_data([:frame_counts, :octet_counts])
      graph_configs = [
        {:type => 'traffic_rate', :data => {:forwarded_data => @stats.stats_frame_results(normalized_data, :octet_counts, @stats.octets_to_mbps)}},
        {:type => 'frames', :data => {:forwarded_data => @stats.stats_frame_results(normalized_data, :frame_counts)}}
      ]

      # Get list of LAgs/ports to display (x by 2 since there are no endpoint pairs)
      display_pairs = [[@entity.stats_displayed_name] * 2]
      if @entity.is_lag
        display_pairs += @entity.ports.collect {|port| ["#{port.node.name} " + port.stats_displayed_name] * 2}
      end

    # Troubleshooting graphs for CENX OVC
    else
      # Forwarded and Dropped counts
      normalized_data = @stats.stats_get_frame_count_data([:dropped_frames, :frame_counts, :dropped_octets, :octet_counts])
      graph_configs = [
        {:type => 'traffic_rate', :data => {:forwarded_data => @stats.stats_frame_results(normalized_data, :octet_counts, @stats.octets_to_mbps)}},
        {:type => 'frames', :data => {:forwarded_data => @stats.stats_frame_results(normalized_data, :frame_counts), :dropped_data => @stats.stats_frame_results(normalized_data, :dropped_frames)}},
        {:type => 'octets', :data => {:forwarded_data => @stats.stats_frame_results(normalized_data, :octet_counts), :dropped_data => @stats.stats_frame_results(normalized_data, :dropped_octets)}}
      ]

      # To make for better display if the OVC is a simple 2 endpoint COS (with an optional test endpoint) then plot
      # The Endpoint_1 Ingress and Endpoint_2 Egress on one graph and Endpoint_2 Ingress and Endpoint_1 Egress
      # on another and then the optional test endpoint.
      # If there are more than 2 endpoints then just plot a graph per endpoint ie Ingress and Egress for that endpoint
      mon_endpoints = @entity.get_cenx_monitoring_segment_endpoints
      endpoints = @entity.segment_end_points.reject {|ep| mon_endpoints.include?(ep)}

      display_pairs = []
      if endpoints.size == 2
        ep1, ep2 = endpoints.first, endpoints.last
        display_pairs << [ep1.stats_displayed_name, ep2.stats_displayed_name]
        display_pairs << [ep2.stats_displayed_name, ep1.stats_displayed_name]
      else
        # ep1 and ep2
        display_pairs = endpoints.collect {|ep| [ep.stats_displayed_name] * 2}
      end

      # Add on monitoring endpoints
      display_pairs += mon_endpoints.collect {|ep| [ep.stats_displayed_name] * 2}

    end

    # iterate over each endpoint pair
    display_pairs.each do |pair|

      # generate graphs for each graph config
      graph_configs.each do |config|


        graph_name = config[:type].split('_').each{|word| word.capitalize!}.join(' ')
        p1_name, p2_name = pair
        p_names = (p1_name == p2_name ? p1_name : p1_name + " -> " + p2_name)
        rows, cols = [], []

        # Make sure data was returned
        unless config[:data][:forwarded_data].blank?

          # Get SLA data if any
          sla_data = @enni ? config[:data][:forwarded_data][p1_name][:sla] : config[:data][:forwarded_data][p1_name][:data][@cos_name][:sla].try(:[], :ingress)

          # OVC Frame and Octet Counts
          if config[:data][:dropped_data]

            title = "Forwarded / Dropped #{graph_name}: <span>#{p_names}</span>"
            display_thresholds = false

            # init columns to be displayed in graph
            cols = [{:id => 'date', :label => 'Date', :type =>'datetime'},
                    {:id => "frames-forwarded-1", :label => "Forwarded: #{p1_name} Ingress", :type => 'number', :color => '#2648BF'},
                    {:id => "frames-forwarded-2", :label => "Forwarded: #{p2_name} Egress", :type => 'number', :color => '#22881B'},
                    {:id => "frames-dropped-1", :label => "Dropped: #{p1_name} Ingress", :type => 'number', :color => '#F07E00'},
                    {:id => "frames-dropped-2", :label => "Dropped: #{p2_name} Egress", :type => 'number', :color => '#F0DE00'}]


            # get data for this cos
            forwarded_data, dropped_data = config[:data][:forwarded_data], config[:data][:dropped_data]
            all_data_points = {
              :p1_forwarded => forwarded_data[p1_name][:data][@cos_name][:ingress],
              :p2_forwarded => forwarded_data[p2_name][:data][@cos_name][:egress],
              :p1_dropped => dropped_data[p1_name][:data][@cos_name][:ingress],
              :p2_dropped => dropped_data[p2_name][:data][@cos_name][:egress]
            }

            # group data points by date time and
            # loop over each set of data points and create rows for graph
             group_data_points_by_date(all_data_points).each do |date_time, data|
              # PROGRAMMERS NOTE: .try(:[], :p1_forwarded) incase ep is nil (data_points can be nil)
              rows << {:c => [{:v => javascript_time(date_time)}, {:v => data.try(:[],:p1_forwarded)}, {:v => data.try(:[],:p2_forwarded)}, {:v => data.try(:[],:p1_dropped)}, {:v => data.try(:[],:p2_dropped)}]}
            end

          # ENNI Frame and Octet Counts and Traffic Rate (for ENNI / OVC)
          else

            title = "Forwarded #{graph_name}: <span>#{p_names}</span>"

            # Display thresholds for Traffic graphs
            display_thresholds = true
            high_data_point = 0

            # init columns to be displayed in graph
            cols = [{:id => 'date', :label => 'Date', :type =>'datetime'},
                    {:id => "frames-forwarded-1", :label => "Forwarded: #{p1_name} Ingress", :type => 'number', :color => '#2648BF'},
                    {:id => "frames-forwarded-2", :label => "Forwarded: #{p2_name} Egress", :type => 'number', :color => '#22881B'}]


            # get data for this cos
            forwarded_data, dropped_data = config[:data][:forwarded_data]
            
            all_data_points = {
              :p1_forwarded => forwarded_data[p1_name][:data][@cos_name][:ingress],
              :p2_forwarded => forwarded_data[p2_name][:data][@cos_name][:egress],
            }

            # group data points by date time and
            # loop over each set of data points and create rows for graph
            group_data_points_by_date(all_data_points).each do |date_time, data|
              # PROGRAMMERS NOTE: .try(:[], :p1_forwarded) incase ep is nil (data_points can be nil)
              p1, p2 = data.try(:[],:p1_forwarded), data.try(:[],:p2_forwarded)

              rows << {:c => [{:v => javascript_time(date_time)}, {:v => p1}, {:v => p2}]}

              # Get maximum data point for set
              high_data_point = [p1, p2, high_data_point].compact.max

            end

          end

          # Display thresholds on graph
          if display_thresholds && sla_data

            if minor_threshold = sla_data[:utilization_low_threshold]
              cols << {:id => 'minor_threshold', :label => 'Minor Threshold', :type => 'number', :color => MINOR_THRESHOLD_COLOR, :hidden_threshold => not(high_data_point > minor_threshold.to_i * THRESHOLD_PERCENTAGE || @force_threshold)}
              rows.map { |row| row[:c] << {:v => minor_threshold} }
            end

            # Add thresholds if they exist
            if major_threshold = sla_data[:utilization_high_threshold]
              cols << {:id => 'major_threshold', :label => 'Major Threshold', :type => 'number', :color => MAJOR_THRESHOLD_COLOR, :hidden_threshold => not(high_data_point > major_threshold.to_i * THRESHOLD_PERCENTAGE || @force_threshold)}
              rows.map { |row| row[:c] << {:v => major_threshold} }
            end

          end

          hidden_threshold_series_array = hidden_threshold_series(cols)

          # push graph set to graphs array
          graphs_set = [{:type => 'dygraph',
                         :name => config[:type],
                         :title => title,
                         :legend => graph_legend(cols),
                         :colors => series_colors(cols),
                         :hidden_threshold_series => hidden_threshold_series_array,
                         :visibility => series_visibility(cols, [], hidden_threshold_series_array),
                         :data => {
                           :cols => cols,
                           :rows => rows
                          }
                       }]

          if sla_data

            rows = [{:c => [{:v => 'Capacity:', :p => {:css_class => "label"}},  {:v => sla_data[:line_rate], :f => "#{sla_data[:line_rate]} Mbps"}]}]

            if display_thresholds
              rows << {:c => [{:v => 'Minor Threshold:', :p => {:css_class => "label"}},  {:v => minor_threshold, :f => "#{minor_threshold} Mbps"}]} if minor_threshold
              rows << {:c => [{:v => 'Major Threshold:', :p => {:css_class => "label"}},  {:v => major_threshold, :f => "#{major_threshold} Mbps"}]} if major_threshold
            end

            graphs_set << {:type => 'table',
                           :data => {
                             :cols =>[{:label => 'Description', :type => 'string'},
                                      {:label => 'Mbps', :type => 'number'}],
                             :rows => rows
                            }
                          }
          end

          # push graph set to graphs array
          graphs.push(graphs_set)

        end

      end

    end

    # Return data points in json format for google visualizations to plot
    {:graphs => graphs}

  end

  # Group data points for troubleshooting graphs by date and time since we are plotting forwarded (ingress/egress) and dropped (ingress/egress) for frames and octets
  # on the same graph.
  def group_data_points_by_date(all_data_points)
    # group data points by date time
    all_data_points.inject({}) do |results, (key, data_points)|
      data_points.each do |p|
        date_time, value = p[:x], p[:y]
        results[date_time] ||= {}
        results[date_time][key] = value
      end
      results
    end
  end

  def initialize_graph_inputs

    # Default cos_name to N/A if no cos_names passed in
    @cos_name = params['cos_name'] ? params['cos_name'] : 'N/A'

    @enni = @entity.class.name == 'EnniNew'

    # Date range for data points
    initialize_graph_dates

    # Get stats data, defined in entity controller (ie on_net_ovc_controller)
    @stats = @entity.stats_data @graph_date_range_start.to_i, @graph_date_range_end.to_i

    # Make sure we are not missing data points, fill with nil
    @stats.pad_results = true

    @force_threshold = params['force_threshold']

    # Date range selected by user
    range = @graph_date_range_end.to_i - @graph_date_range_start.to_i

    # Set interval for retrieving data points based on date range selected by user (and type of stats)
    # Longer date range = less data points (less granularity)
    @interval = case range
    when 0.days .. 3.days then
      if @stats.is_a?(SamStatsArchive)
        5.minutes
      elsif @stats.is_a?(SpirentStatsArchive)
        5.minutes
      elsif @stats.is_a?(BxStatsArchive)
        2.minutes
      else
        10.minutes
      end
    when 3.days .. 7.days then
      10.minutes
    when 7.days .. 15.days then
      20.minutes
    else
      40.minutes
    end

    @interval = 5.minutes

  end

  def initialize_graph_dates
    # Default graph dates to today
    @graph_date_range_start = RangeDateStart.new(params['date_range_start'], :today)
    @graph_date_range_end = RangeDateEnd.new(params['date_range_end'], :today)
  end

  def initialize_event_dates
     @event_date_range_start = RangeDateStart.new(params['date_range_start'])
     @event_date_range_end = RangeDateEnd.new(params['date_range_end'])
  end

  def add_unavailable_and_maintenance(graph_data)
    mtc_and_ua_periods = @stats.unavailable_periods([:mtc_times, :alarm_times])
    graph_data.merge({:maintenance_periods => javascript_periods(mtc_and_ua_periods[:mtc_times]), :unavailable_periods => javascript_periods(mtc_and_ua_periods[:alarm_times])})
  end

  # Takes date in format "%Y, %m, %e, %H, %M, %S" in returns same format with a 0 indexed month (January = 0)
  def javascript_time(timestamp)
    date = timestamp_in_time_zone(timestamp)
    if @exporting
      date.strftime "%Y-%m-%d %H:%M:%S"
    else
      formatted_date = sprintf "%d, %d, %d, %d, %d, %d", date.year, date.month, date.day, date.hour, date.min, date.sec
      time_split =  formatted_date.split(',')
      month = time_split[1].to_i
      month = (month - 1) # Do not pad with zeros as number will be taken as an octal by javascript
      time_split[1] = month
      "Date(#{time_split.join(',')})"
    end
  end

  # Take periods (start and end timestamps and convert to javascript dates)
  def javascript_periods(periods)
    periods.map do |period|
      # Period start and end times are in Milliseconds so divide by 1000
      {'start' => period['start'].nil? ? nil : javascript_time(period['start'] / 1000), 'end' => period['end'].nil? ? nil : javascript_time(period['end'] / 1000)}
    end
  end

  def google_req_id(tqx)
    # get request id to send back to google visualization request
    reqId = 0
    tqx.each do |tqx_param|
      reqId = tqx_param.split(':')[1]
    end
    return reqId
  end

  def numeric?(string)
    Float(string) != nil rescue false
  end

  # TODO: do this in a view?
  def graph_legend(cols)
    "Time of Day in #{Time.zone.to_s}"
    # legend_str = cols.inject('<div>Time of Day</div> <ul class="graph_legend">') do |legend, item|
    #   legend += '<li><span class="rect" style="background-color: ' + item[:color] + '"></span> ' + item[:label] + '</li>' if item[:type] == 'number'
    #   legend
    # end
    # legend_str += '<li class="clearfix"></li></ul><div class="clearfix"></div>'
  end

  def series_colors(cols)
    cols.map{|item| item[:color] if item[:type] == 'number'}.compact
  end

  def per_series_customizations(cols)
    cols.inject({}) do |result, col|
      if col[:dashed_line].present?
        result[col[:label]] ||= {}
        result[col[:label]].merge!({:strokePattern => col[:dashed_line]})
      end
      result
    end
  end

  def details_series(cols)
    # no block passed to each_with_index so return enumerable
    # delete date from array
    cols.drop(1).each_with_index.map{ |col, i| col[:details].present? ? i : nil }.compact
  end

  def hidden_threshold_series(cols)
    # no block passed to each_with_index so return enumerable
    # delete date from array
    cols.drop(1).each_with_index.map{ |col, i| col[:hidden_threshold].present? ? i : nil }.compact
  end

  def series_visibility(cols, details_series_array, hidden_threshold_series_array)
    cols.drop(1).each_with_index.map{ |col, i| details_series_array.include?(i) || hidden_threshold_series_array.include?(i) ? false : true }
  end

  def number_with_delimiter(number, delimiter=",")
    number_parts = number.to_s.split('.')
    number_parts[0].gsub(/(\d)(?=(\d\d\d)+(?!\d))/, "\\1#{delimiter}") + (number_parts[1] ? '.' + number_parts[1] : '');
  end
  
  def strip_tags(str)
    str.gsub(/<\/?[^>]*>/, "") if str
  end

  # Used by Evc controller and Compares controller to compare graphs
  def set_graphs_to_compare
    session['graphs_to_compare'] ||= {}
    @graphs_to_compare = session['graphs_to_compare']
  end
  
  def exponential_notation(float)
    "%g" % float
  end

  # Overwrittne by CustomApplication Controller (if included)
  def entity_is_not_redacted?(entity)
    true
  end

  def internal_server_error
    redirect_to '/500.html', :status => :internal_server_error
  end

  def server_error
    render :text => "Server Error", :status => 500
  end

  def forbidden
    render :text => "Forbidden", :status => :forbidden
  end

  # Used to make sure event that initated hover is still valid after the response is returned
  def set_hover_token
    response.headers["x-hover-token"] = params[:hover_token] if params[:hover_token]
  end


  def availability_aggregate_formatted(data)

    aggregate = data[:aggregate_avail]
    sla = data[:sla_monthly].to_f

    # Aggregate may not be number, could be a message
    if (numeric? aggregate)
      aggregate = '0' if aggregate.to_f < 0
      aggregate_formatted =  "%.3f" % aggregate + '%'
      # Does aggregate value satify SLA (Service Level Agreement)
      cell_class, cell_title = if aggregate.to_f >= sla
        ['ok', 'Average Availability for displayed period <strong>does not exceed</strong> the Guaranteed Monthly SLA']
      else
        ['warning', 'Average Availability for displayed period <strong>exceeds</strong> the Guaranteed Monthly SLA']
      end
    else
      aggregate_formatted = aggregate
      cell_class, cell_title = 'no_data', ''
    end

    [aggregate_formatted, aggregate, sla, cell_class, cell_title]

  end

  def frame_loss_aggregate_formatted(data)
    
    # Service level agreement (to be plotted against sam data)
    sla_data = data[:sla]
    sla = sla_data[:sla_threshold].to_f
    minor_threshold = sla_data[:sla_warning_threshold].to_f
    major_threshold = sla_data[:sla_error_threshold].to_f

    # get aggregate flr over period
    aggregate = data[:aggregate_flr]

    # Aggregate may not be number, could be a message
    if (numeric? aggregate)
      # Aggregate could be less than zero
      aggregate = '0' if aggregate.to_f < 0
      aggregate_formatted =  exponential_notation(aggregate) + '%'
      
      aggregate_value = aggregate.to_f
      cell_class, cell_title = case 
      when sla && aggregate_value >= sla                             then ['critical', 'Average Frame Loss for displayed period <strong>exceeds</strong> the Guaranteed Monthly SLA']
      when major_threshold > 0 && aggregate_value >= major_threshold then ['major', 'Average Frame Loss for displayed period <strong>exceeds</strong> the Major Threshold']
      when minor_threshold > 0 && aggregate_value >= minor_threshold then ['minor', 'Average Frame Loss for displayed period <strong>exceeds</strong> the Minor Threshold']
      else ['ok', 'Average Frame Loss for displayed period <strong>does not exceed</strong> any thresholds']
      end

    else
      aggregate_formatted = aggregate
      cell_class, cell_title = 'no_data', ''
    end

    [aggregate_formatted, aggregate, sla, minor_threshold, major_threshold, cell_class, cell_title]


  end

  def delay_aggregate_formatted(data, type)

    sla_data = data[:sla][@cos_name]
    sla = sla_data[:sla_threshold].to_i

    minor_threshold = sla_data[:sla_warning_threshold].to_f
    major_threshold = sla_data[:sla_error_threshold].to_f

    # get aggregate delay over period
    aggregate = data[:aggregate]

    # Aggregate may not be number, could be a message
    if (numeric? aggregate)
      # Aggregate could be less than zero
      aggregate = '0' if aggregate.to_f < 0
      aggregate_formatted =  "%.2f" % aggregate + 'µs'
  
      aggregate_value = aggregate.to_f
      cell_class, cell_title = case 
      when sla && aggregate_value >= sla                             then ['critical', "Average #{type} for displayed period <strong>exceeds</strong> the Guaranteed Monthly SLA"]
      when major_threshold > 0 && aggregate_value >= major_threshold then ['major', "Average #{type} for displayed period <strong>exceeds</strong> the Major Threshold"]
      when minor_threshold > 0 && aggregate_value >= minor_threshold then ['minor', "Average #{type} for displayed period <strong>exceeds</strong> the Minor Threshold"]
      else ['ok', "Average #{type} for displayed period <strong>does not exceed</strong> any thresholds"]
      end

    else
      aggregate_formatted = aggregate
      cell_class, cell_title = 'no_data', ''
    end

    [aggregate_formatted, aggregate, sla, minor_threshold, major_threshold, cell_class, cell_title]

  end

end

