class EnnisController < CustomApplicationController

  before_filter :find_model, :monitoring_title
  before_filter do |controller| 
    case controller.mode
      when 'monitoring'
        authorize!(:read, :monitoring_enni_list)
      when 'inventory'
        authorize!(:read, :inventory_enni_list)
      when 'ordering'
        authorize!(:read, :ordering_enni_list)
    end
  end
  
  def index    
    
    page = [params[:page].to_i,1].max
    per_page = 10
    
    provider_id = current_user.service_provider.id
    @is_filtered = false
    
    include_hash = {:enni => {:operator_network => :service_provider}}
    
    if params[:enni_details].blank?
      sphinx_search = if params[:filter].blank?
        ""
      else
        @is_filtered = true
        params[:filter].to_s
      end
  
      with_options = if params[:real_ennis].present?
        {:monitoring_nis => false} 
      elsif params[:monitoring_nis].present?
        {:monitoring_nis => true} 
      else
        {:list_ennis => true} 
      end

      # Hack for now to not include ennis which have a router as demarc icon
      without_options = {:demarc_icon => 'router'.to_crc32}

      #State filters
      [:provisioning_state, :ordering_state, :monitoring_state].each do |sym|
        unless params[sym].blank?
          with_options[sym] = params[sym].to_crc32
          @is_filtered = true
        end
      end

      unless params[:site].blank?
        with_options[:enni_site_id] = params[:site]
        @site = Site.find(params[:site])
        @is_filtered = true
      end

      unless params[:service_provider].blank?
        with_options[:enni_service_provider_id] = params[:service_provider]
        @service_provider = ServiceProvider.find(params[:service_provider])
        @is_filtered = true
      end

      unless params[:oem].blank?
        with_options[:oem_id] = params[:oem]
        @operator_network_type = OperatorNetworkType.find(params[:oem])
        @is_filtered = true
      end
    
      sphinx_order = if params[:sort].blank?
        current_user.system_admin? ? 'cenx_id_sort' : 'enni_member_handle_sort'
      else
        params[:sort]
      end

      @breadcrumbs = {site: @site, service_provider: @service_provider, oem: @operator_network_type}.reject{|param, value| value.nil? }

      # search ennis based on filter and paginate the results
      if params[:monitoring_state].blank?
        @sphinx_results = enni_sphinx.search(sphinx_search, 
                                             :match_mode => :all,
                                             :with => with_options,
                                             :without => without_options,
                                             :include => include_hash, 
                                             :page => page, 
                                             :per_page => per_page,
                                             :group => 'enni_id',
                                             :group_clause => "#{sphinx_order} ASC")
      else
        # need to paginate arrays for this case only, inefficient bug hopefully temporary
        require 'will_paginate/array'
        
        @sphinx_results = enni_sphinx.search(sphinx_search, 
                                             :match_mode => :all,
                                             :with => with_options,
                                             :without => without_options,
                                             :include => include_hash, 
                                             :per_page => 1000, # Get as many as we can
                                             :group => 'enni_id',
                                             :group_clause => "#{sphinx_order} ASC")
          
          
        # filter monitoring_state results:
        # monitoring states can change rapidly before sphinx has time to re-index
        @is_filtered = true
        @sphinx_results.delete_if {|sr| sr.enni.get_SM_state.state != params[:monitoring_state]}
        @sphinx_results = @sphinx_results.paginate(:page => page, :per_page => per_page)

      end
      @ennis = @sphinx_results.compact.collect(&:enni)


    else
      @details = true
      # CENX Administrator can access all ENNIs 
      # @sphinx_results = enni_sphinx.search("",
      #                                      :with => {:enni_id => params[:enni_details].to_i},
      #                                      :include => include_hash,
      #                                      :group => 'enni_id')

      @ennis = [find_enni(params[:enni_details])].compact

    end
    
  end
  
  
  # Rendering enni via ajax
  def list_row
    @details = true if params[:enni_details]
    # Wrap render partial in format.html so content is sent back (bug in Rails 3.2.2, https://github.com/rails/rails/issues/5238)
    respond_to do |format|
      format.html { render :partial => 'ennis/enni', :locals => { :enni => @entity } }
    end
  end
  
  protected

  def find_model
    @entity = find_enni(params[:id]) if params[:id]
  end

  def graph_tabs
    [@entity]
  end

  def monitoring_title
    @monitoring_title = "#{get_term(:enni)} Service Monitoring"
  end  
  
  def find_member_handles
    @enni_member_handles = @enni.member_handles.owned_by(@current_user.service_provider)
  end
  
end



