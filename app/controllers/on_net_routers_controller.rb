class OnNetRoutersController < OnNetOvcsController

  private
  
  def find_model
    @entity = OnNetRouter.find params[:id], :include=>[{:cos_instances => :class_of_service_type}] if params[:id]
  end

end
