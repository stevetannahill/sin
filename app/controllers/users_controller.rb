class UsersController < ApplicationController
  load_and_authorize_resource
  before_filter do |c| @users_page = true end
  before_filter :check_system_admin_params, :only => [:update, :create]
  before_filter :find_user, :except => [:create]
  before_filter :set_operator_network_types
  before_filter :set_default_modes, :except => [:index]
  
  # GET /users
  # GET /users.json
  def index
    if @current_user.system_admin?
      @users = User.all
      @service_providers = ServiceProvider.all
    else
      @users = User.joins(:operator_network_types).where(:service_provider_id => @current_user.service_provider.id).where(:operator_network_types => {:id => @operator_network_types}).group(:id)
      @service_providers = [@current_user.service_provider]
    end
    
    @roles = Role.includes(:users).all
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    respond_to do |format|
      if @user.save
        format.html { redirect_to users_url, notice: "#{@user.email} was successfully created." }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update_attributes(params[:user])
        notice = "#{@user.email} was successfully updated."
        if can? :read, User
          format.html { redirect_to users_url, notice: notice }
        else
          format.html { redirect_to edit_user_url(@user), notice: notice }
        end
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1/homepage
  def set_homepage
    @user.homepage = params[:homepage]
    respond_to do |format|
      if @user.save
        format.html { redirect_to :back, notice: "Your default homepage has been set to the current page." }
      else
        format.html { redirect_to :back, alert: "Your homepage could not be set - #{@user.errors.full_messages.map(&:downcase).to_sentence}." }
      end
    end
  end

  # PUT /users/1/toggle_table
  def toggle_table
    unless @user.hidden_tables.delete(params[:table_name])
      @user.hidden_tables << params[:table_name]
    end

    respond_to do |format|
      if @user.save
        format.json { head :no_content }
      else
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end
  
  private
  
  def set_default_modes
    @default_modes = []
    @default_modes << ['Inventory', 'inventory'] if can? :read, :inventory
    @default_modes << ['Monitoring', 'monitoring'] if can? :read, :monitoring
    @default_modes << ['Ordering', 'ordering'] if can? :read, :ordering
  end

  def find_user
    id = params[:id] || params[:user_id]
    if id
      if @current_user.system_admin?
        @user = User.find(id)
      else
        @user = User.where(:service_provider_id => @current_user.service_provider.id).find(id)
        # Ensure that a regular user is not editing an admin
        if @user && @user.system_admin? && !@current_user.system_admin?
          authorize! :manage, :admin_user
        end
      end
    end
  end
  
  def set_operator_network_types
    # limit the scope of operator_network_types
    if @current_user.system_admin?
      @operator_network_types = OperatorNetworkType.all
    elsif ['new', 'index'].include?(action_name)
      @operator_network_types = @current_user.operator_network_types
    else
      @operator_network_types = @user.operator_network_types
    end
  end
  
  def check_system_admin_params
    authorize! :assign_service_provider, @current_user if params[:user][:service_provider_id]
    authorize! :assign_role, @current_user if params[:user][:role_id]
    authorize! :assign_network_types, @current_user if params[:user][:operator_network_type_ids]

    if params[:system_admin] && can?(:assign_system_admin, @current_user)
      params[:user][:operator_network_type_ids] = []
    end

    @user = User.new(params[:user])

    unless can? :assign_service_provider, @current_user
      @user.service_provider_id = @current_user.service_provider_id
    end

    unless can? :assign_network_types, @current_user
      @user.operator_network_type_ids = @current_user.operator_network_type_ids
    else
      params[:user][:operator_network_type_ids] ||= []
    end
    
  end
    
end
