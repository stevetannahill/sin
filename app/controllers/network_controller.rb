class NetworkController < CustomApplicationController

  before_filter :set_page
  before_filter do |controller| authorize!(:read, :network) end
  
  def index
    @sites = @site_select.map do |site|
      site_marker(Site.find(site.site_id))
    end
    
    @demarcs = filtered_demarcs
  end

  # Ajax to get all demarcs and lines for an site
  def demarcs_by_site
    respond_to do |format|
      # Use json gem encoding instead of rails (much faster)
      format.json { render :json => filtered_demarcs }
    end
  end

  private
  
  # TODO: Can this be shared with CSS / SASS?
  MONITORING_COLORS = {
    no_state: '#333333',
    maintenance: '#858585',
    not_live: '#d9d9d9',
    not_monitored: '#74a1ae',
    unavailable: '#640d0d',
    failed: '#ba2a2a',
    warning: '#ff8d20',
    dependency: '#dcde32',
    ok: '#609c25'
  }
    
  # Filter demarcs by various filters
  def filtered_demarcs

    @cenx_path_type = params[:cenx_path_type]
    @site_id = params[:site]
    @site_group_id = params[:site_group]
    @service_provider_id = params[:service_provider]
    @sow_id = params[:statement_of_work]
    @operator_network_type_id = params[:oem]
    @filter_params = request.GET.merge({'mode' => params[:mode]})

    # Filter by sphinx full text first
    path_ids = fulltext_search

    # @is_filtered can be set by fulltext_search as well
    unless @is_filtered
      @is_filtered = if @cenx_path_type.blank? && 
                        @site_id.blank? && 
                        @service_provider_id.blank? && 
                        @sow_id.blank? &&
                        @operator_network_type_id.blank?
                     then false else true end
    end
  
    if @is_filtered

      # Get all Evcs matching filter
      paths = Path.find_paths(@path_owner, @current_user, @service_provider_id, @site_id, @cenx_path_type, nil, nil, nil, @sow_id, @operator_network_type_id, @fallout_exception_type, @fallout_category, @path_ids, @site_group_id)

      # do a big giant IN if sphinx was used to filter results
      paths = paths.where(:paths => {:id => path_ids}) if path_ids
    
      # Only show demarcs if we are filtering on something
      demarcs(paths.pluck("distinct paths.id"))
      
    end
  end

  # filter paths using sphinx fulltext
  def fulltext_search
    
    sphinx_search, @is_filtered = params[:filter].blank? ? ["", false] : [params[:filter].to_s, true]
    
    # only use sphinx if user supplied a filter
    if @is_filtered  
      # TODO need a better way todo monitoring states, the following solution will not scale and is only temporary
      sphinx_results = path_sphinx.search_for_ids(sphinx_search, 
                                                 :match_mode => :all,
                                                 :per_page => 1000, # Get as many as we can
                                                 :group => 'path_id')

      # Do not want to instantiate SinPathServiceProviderSphinx objects just to get path_id                                   
      SinPathServiceProviderSphinx.where(:id => sphinx_results).pluck('path_id')
    else
      nil
    end
  end  
  
  # Generate hash for demarc markers and polylines
  def demarcs(paths)
    
    demarcs = {}
    lines = {}
    components_index = {}

    # Map each path
    rows = PathDisplay.connection.select_all(PathDisplay.where(:path_id => paths).by_service_provider(@current_user.service_provider).network_map.to_sql)
    # rows = path.network_components.by_service_provider(@current_user.service_provider).network_map
    # Go through each site and create markers (exhanges and demarcs)
    
    rows.each_with_prev_next do |previous_component, component, next_component|
      
      # Don't want to process same component twice
      component_index = "#{component['entity_type']}#{component['entity_id']}" 
      unless components_index.has_key?(component_index) 
        
        # Keep track of processed demarcs, only display marker once
        components_index[component_index] = true

        # Use demarcs to place markers
        # Do not place markers for ennis unless demarc icon is a cell site
        if %w(Demarc Uni).include?(component['component_type']) || (component['component_type'] == 'EnniNew' && component['demarc_icon'].humanize.downcase == 'cell site')
          demarcs = demarc_marker(component, demarcs)
        end

        # Use Ovcs to draw line
        if previous_component && previous_component['row'] == component['row'] && previous_component['path_id'] == component['path_id']
          lines = ovc_line(component, previous_component, next_component, lines)
        end
        
      end
    end

    {:demarcs => demarcs.values, :lines => lines.values}
    
  end

  def ovc_line(component, previous_component, next_component, lines)
    
    # Draw line to represent ovc new segment (off net or on net)
    line_start = {:lat => previous_component['latitude'], :lng => previous_component['longitude']}
    line_end = false
    
    # Determine line end and color of line
    if component['entity_type'] == 'Segment' && next_component
      if component['path_id'] == next_component['path_id']
        # On Net or Off net
        if @mode == 'monitoring'
          state = Segment.find(component['entity_id']).get_SM_state.try(:state_css_classify)
          line_color = MONITORING_COLORS[state.to_sym]
        else
          # On Net or Off net
          line_color = component['service_provider_id'] == component['owner_service_provider_id'] ? '#004FFF' : '#6F3700'
        end
        
        line_end = {:lat => next_component['latitude'], :lng => next_component['longitude']}
        ovc_type = convert_to_path(component['component_type']).pluralize
      end
    # Draw line to represent readacted portion of path (two demarcs in a row)
    elsif previous_component['entity_type'] == 'Demarc' && component['entity_type'] == 'Demarc'
      line_color = '#111111'
      line_end = {:lat => component['latitude'], :lng => component['longitude']}
      ovc_type = nil
    end

    if line_end && line_start != line_end

      # Did we process an ovc for this path (lat/lng to lat/lng) already?
      line_index = "#{line_start[:lat]}_#{line_start[:lng]}_#{line_end[:lat]}_#{line_end[:lng]}"
      previous_line = lines[line_index]
      
      # Track ovc_ids incase multipe ovcs traverse the lat/lng path
      ovc_ids = (previous_line ? previous_line[:ovc_ids] : []) << component['entity_id'] 
      path_ids = (previous_line ? previous_line[:path_ids] : []) << component['path_id'] 

      # current line to lines
      lines[line_index] = {:points => [line_start, line_end], 
                           :color => line_color, 
                           :ovc_type => ovc_type, 
                           :ovc_ids => ovc_ids,
                           :path_ids => path_ids
                          } 
   
    end
    
    lines
    
  end

  def site_marker(site)
    # TODO: Conflicting rules with vzw and segment as starting end point
    # {:icon => "/assets/map_markers/#{site.type == 'AggregationSite' ? 'site' : site.type.underscore}.png",

    {:icon => "/assets/map_markers/#{site.general_type}.png",
     :lat => site.latitude, 
     :lng => site.longitude,
     :path_count => "#{site.name} <br/> #{site.path_count_by_operator_network_type(current_user.operator_network_types)} #{get_term(:path, true)}",
     :demarcs_url => demarcs_by_site_network_index_path({:site => site.id, :mode => @mode}),
     :id => site.id
    }
  end
  
  def demarc_marker(demarc, demarcs)

    # Did we process a demarc for this location (lat/lng) already?
    demarc_index = "#{demarc['latitude']}_#{demarc['longitude']}"
    previous_demarc = demarcs[demarc_index]

    # HACK: looking at previous marker for suffix (site_type) so we know how to color code.  
    # Example router is rn but cell_site has not site_type, but we are displaying cell site marker.  Need it to be color coded as rn
    # have to plot sites again so map can be fit to bounds properly (including demarcs and site)
    site_type = demarc['site_type'].try(:downcase)
    # if site_type.nil? or site_type.blank?
    #   debugger
    # end
    icon_suffix = site_type.present? ? "#{site_type}" : (previous_demarc && previous_demarc[:site_type] ? "#{previous_demarc[:site_type]}" : 'eth') 

    # Track demarc ids for this location (lat/lng)
    demarc_ids = (previous_demarc ? previous_demarc[:demarc_ids] : []) << demarc['entity_id'] 
    demarc_icon = demarc['demarc_icon'].nil? ? 'building' : demarc['demarc_icon'].downcase.gsub(' ','_')
    demarc_icon = 'building' unless %w(building cell_site router mw_site enni).include?(demarc_icon)
    demarc_icon += '_' + icon_suffix unless icon_suffix.blank?
    
    demarcs[demarc_index] = {
      :icon => "/assets/map_markers/#{demarc_icon}.png",
      :site_type => icon_suffix,
      :lat => demarc['latitude'], 
      :lng => demarc['longitude'],
      :demarc_ids => demarc_ids
      # Do not use map_info_window_demarcs_path helper ... slow
    }
    
    # current line to lines
    demarcs 
    
  end
  
  private

  def set_page
    @network_page = true
  end

end