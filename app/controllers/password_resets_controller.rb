class PasswordResetsController < ApplicationController
  skip_before_filter :cas_filter, :set_current_user, :set_time_zone, :set_mode, :set_path, :set_path_owner, :set_component_html_id, :set_reverse_diagram, :set_graphs_to_compare
  skip_before_filter :get_paths_and_ennis, :set_search_filter_params  
  skip_before_filter :appstats_login, :appstats_page_counter, :user_login_stats

  def new
  end

  def create
    user = User.find_by_email(params[:email])
    user.send_password_reset if user
    redirect_to new_password_reset_path, :notice => "Email sent with password reset instructions."
  end
  
  def edit
    @user = User.find_by_password_reset_token!(params[:id])
  end
  
  def update
    @user = User.find_by_password_reset_token!(params[:id])
    if @user.password_reset_sent_at < 2.hours.ago
      redirect_to new_password_reset_path, :alert => "Password reset has expired."
    elsif @user.update_attributes(params[:user])
      redirect_to root_url, :notice => "Password has been reset!"
    else
      render :edit
    end
  end
end