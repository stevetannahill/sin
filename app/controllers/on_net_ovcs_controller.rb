class OnNetOvcsController < CustomApplicationController

  before_filter :find_model, :find_collection, :monitoring_title

  def map_info_window
    # TODO: add security
    @date_range_selected = :today
    @path_ids = params[:path_ids] if params[:path_ids]
    if @mode == 'monitoring'
      render 'shared_monitoring/map_info_window_monitoring', :layout => false
    else
      render :map_info_window, :layout => false
    end
  end
  
  # Show member handles in tooltip
  def map_tooltip
    render :map_tooltip, :layout => false
  end

  # Generate Config 
  def genconfig
    # configure qos policies
    # TODO: :disabled => (@ovcn.on_net_ovc_end_point_ennis.empty? or not @ovcn.qos_policies_correct?
    @entity.configure_qos_policies
    @genconfig_text = @entity.generate_config
    # TODO: Better way todo layout false in a more global way
    render :genconfig, :layout => false
  end

  protected

  def graph_tabs
    @entity.cos_instances
  end

  private
  
  def find_collection
    @ovcs = OffNetOvc.find params[:ids] if params[:ids]
  end

  def find_model
    @entity = OnNetOvc.find params[:id], :include=>[{:cos_instances => :class_of_service_type}] if params[:id]
  end

  def monitoring_title
    @monitoring_title = 'CENX OVC Monitoring'
  end    
end
