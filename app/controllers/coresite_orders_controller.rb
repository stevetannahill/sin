class CoresiteOrdersController < CustomApplicationController

  before_filter :authorize_manage, :except => [:index, :show, :_view_build_log]
  before_filter :init_from_params, :only => [:create, :update, :_check_available_bandwidth_and_vlan]
  before_filter :init_from_model

  def index
    # do not list coresite orders here, now integrated with Dashboard
    redirect_to inventory_dashboard_path
  end

  def show
    @form_action = if @order.can_be_accepted_by_role?(@current_operator_network)
      view_context.accept_coresite_order_path(@order.id)
    else
      view_context.coresite_order_path(@order.id)
    end
  end

  def new
    @order = ServiceOrder.new()
  end

  def update
    @mh = @order.path.member_handle_for_owner_object(@current_user.service_provider)
    unless @mh
      @mh = MemberHandleInstance.new({affected_entity: @order.path, owner: @current_user.service_provider, member_attr: MemberAttr.find_by_affected_entity_type_and_type('Path', 'MemberHandle')})
    end
    @mh.value = params[:order_member_handle]
    respond_to do |format|
      if @mh.save
        format.html { redirect_to coresite_order_path(@order), :notice => 'ID was successfully updated.' }
      else
        flash[:alert] = "ID could not be updated for the following reason: ID #{@mh.errors[:value].join(', ')}"
        show
        format.html { render action: "show" }
      end
    end
  end

  def create

    unless error = can_buyer_access_target?(@buying_operator_network, @target_operator_network)

      order_info = {
        :tenant_a               => @buying_operator_network.id, 
        :nni_a                  => params[:buying_port], 
        :vlan_a                 => params[:buying_vlan], 
        :member_handle_a        => params[:buying_order_member_handle], 
        :tenant_z               => @target_operator_network.id, 
        :cir                    => params[:service_rate].to_i, 
        :requested_service_date => params[:requested_service_date],
        :user                   => @current_user.email
      }.reject{|k, v| v.blank?}

      # To place an order for a XC
      result = CoresiteBuildCenter.new.process_order(order_info, @buying_operator_network.auto_accept?(@target_operator_network))
    end

    respond_to do |format| 
      if error || result[:summary] == 'Failed'
        flash[:alert] = "Order not processed. " + (error ? error : result[:summary_detail])
        new # new method (action)
        format.html { render action: "new" } 
      elsif result[:summary] == 'Fallout'
        flash[:alert] = "Order is in a Fallout state. #{result[:summary_detail]}"
        format.html { redirect_to coresite_order_path(result[:associated_object_id]) } 
      else
        format.html { redirect_to coresite_order_path(result[:associated_object_id]), notice: 'Order was successfully created.' } 
      end
    end

  end

  def change
    if @order.can_be_changed_by_role?(@current_operator_network)
      @form_action = view_context.process_change_coresite_order_path(@order.id)
      @action = :change
    end
  end

  def process_change
    update_order(:change) if @order.can_be_changed_by_role?(@current_operator_network)
  end

  def accept
    if @order.can_be_accepted_by_role?(@current_operator_network)
      update_order(:accept) 
    else
      flash[:alert] = "Order cannot be accepted because order was cancelled by the Buyer."
      respond_to do |format|
        show # include show instance variables
        format.html { render action: "show" } 
      end
    end
  end

  def reject
    if @order.can_be_rejected_by_role?(@current_operator_network)
      update_order(:reject)
    else
      flash[:alert] = "Order cannot be rejected because order was cancelled by the Buyer."
      respond_to do |format|
        show # include show instance variables
        format.html { render action: "show" } 
      end
    end
  end

  def cancel
    if @order.can_be_cancelled_by_role?(@current_operator_network)
      update_order(:cancel) 
    else
      review_action = case @order.order_name.downcase 
      when 'rejected' 
        'rejected'
      when 'delivered'
        'accepted'
      else
        'reviewed'
      end
      flash[:alert] = "Order cannot be cancelled because order has already been #{review_action} by the Target."
      respond_to do |format|
        show # include show instance variables
        format.html { render action: "show" } 
      end
    end

  end

  def destroy
    
    result = if @order.can_be_deleted_by_role?(@current_operator_network)
      CoresiteBuildCenter.new.remove_order({:so_id => @order.id})
    else 
      false
    end

    respond_to do |format|
      if result
        format.html { redirect_to inventory_dashboard_path, notice: 'Order was successfully deleted.' }
      else
        format.html { redirect_to coresite_order_path(@order.id), alert: 'Order cannot be deleted' }
      end
    end

  end

  def disconnect 

    if @order.can_be_disconnected_by_role?(@current_operator_network)
      update_order(:disconnect)
    else 
      flash[:alert] = "Path cannot be disconnected."
      respond_to do |format|
        format.html { redirect_to inventory_dashboard_path }
      end
    end

  end

  def _view_build_log
    build_log = @order.try(:automated_build_logs).try(:first)
    render :partial => 'coresite_shared/build_log', :locals => {build_log: build_log}
  end

  # AJAX Actions

  def _check_available_bandwidth_and_vlan

    if error = can_buyer_access_target?(@buying_operator_network, @target_operator_network)
      message = "<p class='failure'>Cannot check availability. #{error}</p>"

    else
      order_info = {
        :tenant_a               => @buying_operator_network.id, 
        :nni_a                  => (params[:buying_port].blank? ? @order.try(:buying_enni).try(:id) : params[:buying_port]).try(:to_i), 
        :vlan_a                 => params[:buying_vlan], 
        :member_handle_a        => params[:buying_order_member_handle], 
        :tenant_z               => @target_operator_network.id, 
        :nni_z                  => (params[:target_port].blank? ? @order.try(:target_enni).try(:id) : params[:target_port]).try(:to_i), 
        :vlan_z                 => params[:target_vlan], 
        :cir                    => params[:service_rate].to_i, 
        :requested_service_date => params[:requested_service_date],
        :user                   => @current_user.email
      }.reject{|k, v| v.blank?}

      result = CoresiteBuildCenter.new.check_cir_and_vlan(order_info, @buying_operator_network.auto_accept?(@target_operator_network))

      message = if result[:result] == true
        "<p class='success'>Bandwidth and VLAN(s) are available.</p>"
      else
        reason = result[:reason].present? ? result[:reason] : 'Problem reserving bandwith and/or VLAN(s).'
        "<p class='failure'>#{reason}</p>"
      end
    end

    respond_to do |format|
      format.html { render :inline => message }
    end

  end

  def _check_available_bandwidth

    if error = can_buyer_access_target?(@order.buying_operator_network, @order.target_operator_network)
      message = "<p class='failure'>Cannot check bandwidth. #{error}</p>"

    else
      nnis = {}
      nnis[:buyer] = (params[:buying_port].blank? ? @order.try(:buying_enni).try(:id) : params[:buying_port]).try(:to_i)
      nnis[:target] = (params[:target_port].blank? ? @order.try(:target_enni).try(:id) : params[:target_port]).try(:to_i)
      segment = @order.path.segments.first.id
      result = CoresiteBuildCenter.new.check_cirs(segment, nnis, params[:service_rate].to_i)

      message = if result[:result] == true
        "<p class='success'>Bandwidth available.</p>"
      else
        reason = result[:reason].present? ? result[:reason] : 'Problem reserving bandwith and/or VLAN(s).'
        "<p class='failure'>#{reason}</p>"
      end
    end

    respond_to do |format|
      format.html { render :inline => message }
    end

  end

  def _sites_select
    operator_network = OperatorNetwork.find(params[:parent_id])
    if authorized_as_buyer?(operator_network)
      respond_to do |format|
        format.html { render :inline => select_options(operator_network.site.try(:site_group_sites), params[:selected], "id", "name") }
      end
    end
  end

  def _operator_networks_select
    site = Site.find(params[:parent_id])
    buying_operator_network = OperatorNetwork.find(params[:buying_operator_network]) 
    if authorized_as_target?(site) && authorized_as_buyer?(buying_operator_network)
      accessible_operator_networks_with_nnis = site.accessible_operator_networks_by_buying_operator_network(buying_operator_network).reject{ |o| o.live_ennis.count.zero? }
      respond_to do |format|
        format.html { render :inline => select_options(accessible_operator_networks_with_nnis, params[:selected], "id", "tenant_name") }
      end
    end
  end
  
  def _ennis_select
    operator_network = OperatorNetwork.find(params[:parent_id])
    if authorized_as_buyer?(operator_network)
      respond_to do |format|
        format.html { render :inline => select_options(operator_network.try(:enni_news), params[:selected], "id", "port_name") }
      end
    end
  end

  def _phy_type
    if enni = get_enni(params[:parent_id])
      respond_to do |format|
        format.html { render :inline => enni.physical_medium }
      end
    end
  end

  def _provisioned_bandwidth
    if enni = get_enni(params[:parent_id])
      respond_to do |format|
        format.html { render :inline => "#{enni.coresite_provisioned_bandwidth} Mbps"}
      end
    end
  end

  def _service_rates_select
    if enni = get_enni(params[:parent_id])
      respond_to do |format|
        format.html { render :inline => view_context.options_for_select(service_rate_options(enni), params[:selected]) }
      end
    end
  end

  def _contact
    # Operator Network id = Tenant id
    type = params[:type]
    operator_network = OperatorNetwork.find(params[:parent_id]) 
    if (%w(billing).include?(type) && authorized_as_buyer?(operator_network)) || (%w(engineering coresite).include?(type) && authorized_as_target?(operator_network.try(:site)))
      respond_to do |format|
        format.html { render :partial => 'coresite_shared/contact', :locals => {:contact => operator_network.send("#{type}_contact")} }
      end
    end
  end

  def _next_available_vlan
    if enni = get_enni(params[:enni_id])
      next_vlan_hash = enni.try(:suggest_valid_stag)
      if next_vlan_hash.try(:[], 0)
        respond_to do |format|
          format.html { render :inline => next_vlan_hash[1].to_s }
        end
      end
    end
  end

  def _access_permission
    target_operator_network = OperatorNetwork.find(params[:parent_id])
    if authorized_as_target?(target_operator_network.try(:site))
      respond_to do |format|
        format.html { render :inline => @current_operator_network.auto_accept?(target_operator_network) ? '' : 'Approval by Target Tenant Required' }
      end
    end
  end

  private

  def get_enni(id)
    EnniNew.find_by_operator_network_id_and_id(@current_operator_network, id)
  end

  def authorize_manage
    authorize! :manage, :coresite_order
  end

  def authorized_as_buyer?(buying_operator_network)
    coresite_role || buying_operator_network.id == @current_operator_network.id
  end

  def authorized_as_target?(target_site)
    coresite_role || @current_operator_network.site.try(:site_group_sites).map(&:id).include?(target_site.id)
  end

  def init_from_params
    
    # Must be buying or target of order OR CoreSite
    if coresite_role || [params['buying_operator_network'].to_i, params['target_operator_network'].to_i].include?(@current_operator_network.id)
      @buying_operator_network = OperatorNetwork.find(params['buying_operator_network']) 
      @target_operator_network = OperatorNetwork.find(params['target_operator_network']) if params['target_operator_network']
      @order = OnNetOvcOrder.find(params[:order_id]) if params[:order_id]
    end

  end

  def init_from_model
    # TODO: Validation / returned from process_order?
    if params[:id]
      @order = OnNetOvcOrder.find(params[:id]) 
      # Must be buying or target of order OR CoreSite
      server_error unless coresite_role || [@order.buying_operator_network.id, @order.target_operator_network.id].include?(@current_operator_network.id)
    end
  end

  def update_order(action)

    # Make sure buyer can still access target
    @error = can_buyer_access_target?(@order.buying_operator_network, @order.target_operator_network) unless action == :disconnect

    unless @error
      order_info = {
        :path_id                => @order.path.id, 
        :segment_id             => @order.path.segments.first.id,
        :tenant_a               => @order.buying_operator_network.id, 
        :nni_a                  => @order.buying_enni.id, 
        :vlan_a                 => @order.buying_end_point.vlan, 
        :member_handle_a        => @order.path.member_handle_for_owner_object(@order.buying_operator_network.service_provider).try(:value), 
        :tenant_z               => @order.target_operator_network.id, 
        :user                   => @current_user.email
      }.merge(
        if action == :change
          {
            :cir                    => params[:service_rate].to_i, 
            :requested_service_date => params[:requested_service_date]
          }
        else
          {
            :cir                    => @order.service_rate, 
            :requested_service_date => @order.requested_service_date.to_formatted_s(:requested_service_date)
          }
        end

      ).merge(
        if action == :accept
          {
            :nni_z              => params[:target_port], 
            :vlan_z             => params[:target_vlan], 
            :member_handle_z    => params[:order_member_handle]
          }
        else
          {
            :nni_z              => @order.target_enni.try(:id), 
            :vlan_z             => @order.target_end_point.try(:vlan), 
            :member_handle_z    => @order.path.member_handle_for_owner_object(@order.target_operator_network.service_provider).try(:value)
          }
        end
      ).merge(
        if action == :reject
          {
            :reject_reason      => params[:reject_reason], 
          }
        else
          {}
        end
      ).reject{|k, v| v.blank?}

      begin

        build_center = CoresiteBuildCenter.new

        result = case action
        when :accept
          notice = "Order was successfully accepted."
          if @order.change_order?
            build_center.accept_change_order(order_info)
          else
            build_center.accept_order(order_info)
          end
        when :reject
          notice = 'Order was successfully rejected.'
          build_center.reject_order(order_info)
        when :change
          if @order.service_rate.to_i == order_info[:cir]
            @error = "Please select a new Service Rate before submitting your Change Order."
          else
            notice = 'Order was successfully changed.'
            build_center.process_change_order(order_info, @order.buying_operator_network.auto_accept?(@order.target_operator_network))
          end
        when :cancel
          notice = 'Order was successfully canceled.'
          build_center.cancel_order(order_info)
        when :disconnect
          notice = 'Path was successfully disconnected.'
          build_center.process_disconnect_order(order_info)
        end
      rescue Exception => e
        @error = e.to_s
      end

    end

    respond_to do |format| 

      alert = if @error 
        "#{action_error(action)} #{@error}" 
      elsif result[:summary] == 'Failed'
        "#{action_error(action)} #{result[:summary_detail]}"
      elsif result[:summary] == 'Fallout'
        "#{notice} However #{get_term(:path)} is in Fallout: #{result[:summary_detail]}"
      end
        
      if alert
        flash[:alert] = alert
        if action == :change
          change
          format.html { render action: "change" } 
        else
          show # include show instance variables
          format.html { render action: "show" } 
        end
      else
        format.html { redirect_to inventory_dashboard_path, notice: notice }
      end
    end
  end

  def can_buyer_access_target?(buying_operator_network, target_operator_network)
    # unless target_operator_network.site.accessible_operator_networks_by_buying_operator_network(buying_operator_network).map(&:id).include?(target_operator_network.id)
    if buying_operator_network.access_permission_given_by_target(target_operator_network) == :no_access
      "#{buying_operator_network.tenant_name} is no longer allowed to order from #{target_operator_network.tenant_name}."
    end
  end

  def action_error(action)
    action_error = case action
    when :accept
      "Order could not be accepted."
    when :reject
      "Order could not be rejected."
    when :change
      "Order could not be changed."
    when :cancel
      "Order could not be cancelled."
    when :disconnect
      "Order could not be disconnected."
    else
      "Could not process request."
    end  
  end

end
