class OffNetOvcsController < CustomApplicationController
 
  before_filter :find_model, :find_collection, :monitoring_title

  def map_info_window
    # TODO: add security
    @date_range_selected = :today
    @path_ids = params[:path_ids] if params[:path_ids]
    if @mode == 'monitoring'
      render 'shared_monitoring/map_info_window_monitoring', :layout => false
    else
      render :map_info_window, :layout => false
    end
  end
  
  # Show member handles in tooltip
  def map_tooltip
    render :map_tooltip, :layout => false
  end

  protected
  
  # Default graph tabs if not defined in controller
  def graph_tabs
    @entity.monitored? ? @entity.cos_instances : nil
  end

  private

  def find_collection
    @ovcs = OffNetOvc.find params[:ids] if params[:ids]
  end

  def find_model
    @entity = OffNetOvc.find params[:id], :include=>[{:cos_instances => :class_of_service_type}] if params[:id]
  end

  def monitoring_title
    @monitoring_title = 'Member OVC Monitoring'
  end  

end