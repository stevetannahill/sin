class OnNetRouterSubIfsController < OnNetOvcEndPointEnnisController

  private
  
  def find_model
    @entity = OnNetRouterSubIf.find params[:id], :include=>[{:cos_end_points => {:cos_instance => :class_of_service_type}}] if params[:id]
  end
  
end
