class EnniDisplaysController < CustomApplicationController
  before_filter :find_row
  layout false
    
  protected
  
  # We are faking an enni displays (rows, columns) to be consistent with evc displays 
  def find_row
    enni = EnniNew.find(params[:id])
    column = PathDisplay.new({:id => enni.id, :entity_id => enni.id, :entity_type => 'Demarc'})
    # Get Diagram parts
    @row = [nil, column, nil]
  end
  
end