class SinController < ApplicationController
  def logout
    if $cas_server_enabled then
      # Clean up CAS client and redirect to CAS logout page
      CASClient::Frameworks::Rails::Filter.logout(self, @current_user.homepage_url(request))
    elsif $coresite_authentication
      CoreSite::Authentication::Filter.remove_session_data(self)
      redirect_to root_path
    else
      redirect_to root_path
    end
  end

  def logo
    redirect_to render_to_string('logo', :layout => false)
  end

  def session_expired
    # unauthorized (401)
    render 'session_expired', :status => :unauthorized, :layout => false
  end  

  def login_text
    render :json => Settings['system.login_text'].to_json, :callback => params[:callback]
  end

end
