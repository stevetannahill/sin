class ServiceOrdersController < ApplicationController

  def update    
    @service_order = ServiceOrder.find(params[:id])
    @note = @service_order.add_note(params[:notes], @current_user, DateTime.now)
    
    respond_to do |format|
      format.html do
        if request.xhr?
          if @service_order.save
            render :partial => "notes", :locals => {:notes => @note}, :layout => false
          else
            render :text => "There was an error saving the comment: #{@service_order.errors.full_messages.to_sentence}", :status => :unprocessable_entity, :layout => false
          end
        end
      end
    end
  end

  def place_order
    @cm_dir = cmaudit_app.cmd("cm dir",false)[:data]
    @service_order = ServiceOrder.find(params[:id])
    @path = Path.find(params[:path_id])
    @order_type = @service_order.ordered_entity_subtype == "UNI" ? "uni" : "evc"
    @cascade = @path.member_handle
    osl_app.cmd("pub send order.q att order-#{@order_type} #{@cascade}")
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  def screenshot
    cm_dir = cmaudit_app.cmd("cm dir",false)[:data]
    image_url = "#{cm_dir}/orders/#{params[:cascade]}/#{params[:order_type]}/#{params[:ts]}/#{params[:num]}.png"
    # TODO: cache the results once things settle down
    # response.headers['Cache-Control'] = "public, max-age=#{84.hours.to_i}"
    response.headers['Cache-Control'] = "no-cache"
    response.headers['Content-Type'] = 'image/png'
    response.headers['Content-Disposition'] = 'inline'
    render :layout => false, :text => open(image_url, "rb").read
  end

end