class SchedulesController < ApplicationController
  load_and_authorize_resource
  before_filter do |c| @reports_page = true end
  before_filter :find_schedule, :except => [:index, :new, :create]
  before_filter :set_default_params, :only => [:edit, :update, :create]

  # GET /schedules
  # GET /schedules.json
  def index
    if @current_user.system_admin?
      @schedules = Schedule.includes({:scheduled_task => :user}, :reports).where('scheduled_tasks.user_id IS NOT NULL')
    else
      @schedules = Schedule.for_user(@current_user)
    end
    @filters = FilterTemplate.all_filters(@current_user)  

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @schedules }
    end
  end

  # GET /schedules/1
  # GET /schedules/1.json
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @schedule }
    end
  end

  # GET /schedules/new
  # GET /schedules/new.json
  def new
    @schedule = Schedule.new
    @schedule.execution_time = DateTime.parse("00:00") 
    @schedule.enabled = true
    @schedule.email_distribution = true
    @schedule.trigger = 'weekly'
    @schedule.formats[:web_link] = true
    @user = @current_user
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @schedule }
    end
  end

  # GET /schedules/1/edit
  def edit
  end

  # POST /schedules
  # POST /schedules.json
  def create
    @schedule = Schedule.new(params[:schedule])
    @user = @schedule.scheduled_task.user
    
    respond_to do |format|
      if @schedule.save
        format.html { redirect_to schedules_path, notice: "Report #{@schedule.name} has been scheduled." }
        format.json { render json: @schedule, status: :created, location: @schedule }
      else
        format.html { render action: "new" }
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /schedules/1
  # PUT /schedules/1.json
  def update
    respond_to do |format|
      if @schedule.update_attributes(params[:schedule])
        format.html { redirect_to schedules_url, notice: 'Scheduled report was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schedules/1
  # DELETE /schedules/1.json
  def destroy
    @schedule.destroy

    respond_to do |format|
      format.html { redirect_to schedules_url }
      format.json { head :no_content }
    end
  end
  
  def generate
    report = @schedule.scheduled_task.generate_report(params[:id])
    report.run_manually = true
    respond_to do |format|
      if report.save
        format.html { redirect_to reports_schedule_path(@schedule), :notice => "#{@schedule.name} report has been successfully generated." }
        format.json { head :no_content }
      else
        format.html { redirect_to reports_schedule_path(@schedule), :alert => "#{@schedule.name} report could not be generated." }
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def reports
    @reports = @schedule.reports.order('created_at DESC')
    respond_to do |format|
      format.html # reports.html.erb
      format.json { render json: @schedule.reports }
    end    
  end
  
  def destroy_report
    @schedule.reports.find(params[:report_id]).destroy

    respond_to do |format|
      format.html { redirect_to reports_schedule_path(@schedule) }
      format.json { head :no_content }
    end  
  end
  
  private
  
  def find_schedule
    if @current_user.system_admin?
      @schedule = Schedule.find(params[:id])
    else
      @schedule = Schedule.for_user(@current_user).where('schedules.id = ?', params[:id]).first
    end

    @user = @schedule.scheduled_task.user
  end

  def set_default_params
    Time.zone = @schedule.timezone

    if params[:schedule].present?
      if params[:schedule][:formats].blank?
        params[:schedule][:formats] = {}
      end

      [:web_link, :csv, :pdf].each do |format|
        params[:schedule][:formats][format] ||= false
      end
    end
  end
end
