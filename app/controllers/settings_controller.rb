class SettingsController < ApplicationController
  before_filter do |c| authorize!(:manage, Settings) end

  # GET /settings
  def edit
  end

  # PUT /settings
  # PUT /settings.json
  def update
    Settings['system.login_text'] = params[:login_text] if params[:login_text].present?
    Settings['support.phone'] = params[:support_phone] if params[:support_phone].present?
    Settings['support.phone_text'] = params[:support_phone_text] if params[:support_phone_text].present?
    Settings['support.email'] = params[:support_email] if params[:support_email].present?
    Settings['support.email_text'] = params[:support_email_text] if params[:support_email_text].present?

    respond_to do |format|
      format.html { redirect_to edit_settings_path, notice: "System settings have been successfully updated." }
      format.json { head :no_content }
    end
  end    
end
