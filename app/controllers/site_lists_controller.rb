class SiteListsController < ApplicationController
  load_and_authorize_resource
  before_filter :find_user

  def update
    @site_list = @user.site_lists.find(params[:id])

    respond_to do |format|
      if @site_list.update_attributes(params[:site_list])
        format.json { head :no_content }
      else
        format.json { render json: @site_list.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @site_list = @user.site_lists.build(params[:site_list])

    respond_to do |format|
      if @site_list.save
        format.html { redirect_to :back, notice: "#{@site_list.name} was successfully created." }
        format.json { render json: @site_list, status: :created, location: @site_list }
      else
        format.html { redirect_to :back, alert: "Site List could not be saved - #{@site_list.errors.full_messages.map(&:downcase).to_sentence}." }
        format.json { render json: @site_list.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @site_list = @user.site_lists.find(params[:id])

    respond_to do |format|
      if @site_list.destroy
        format.json { head :no_content }
      else
        format.json { render json: @site_list.errors, status: 400 }
      end
    end
  end
  
  private
  
  def find_user
    @user = User.find(params[:user_id])
  end
      
end
