class NodesController < CustomApplicationController

  before_filter :find_model

  private
  
  def find_model
    @entity = Node.find params[:id] if params[:id]
  end

end