class PathDisplaysController < CustomApplicationController
  
  before_filter :find_row
  layout false
    
  protected
  
  def find_row
    if params[:id]
      @path = find_path(params[:id])
      # Get Diagram parts
      @row = @path.diagram_parts(current_user.service_provider, @reverse_diagram, params[:row].to_i)[0]
    end
  end
  
end