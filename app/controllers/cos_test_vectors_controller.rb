class CosTestVectorsController < ApplicationController

  before_filter :find_model

  # Content for network monitoring bubble
  def network_monitoring
    initialize_graph_inputs

    # Get aggregate values
    @aggregate_avail, aggregate, sla, @cell_class_avail, @cell_title_avail = availability_aggregate_formatted @stats.stats_availability_summary

    # TODO: only handling one cos at the moment
    @aggregate_flr, aggregate, sla, minor_threshold, major_threshold, @cell_class_flr, @cell_title_flr = frame_loss_aggregate_formatted @stats.stats_flr_summary[@cos_name]

    @aggregate_delay, aggregate, sla, minor_threshold, major_threshold, @cell_class_delay, @cell_title_delay = delay_aggregate_formatted @stats.stats_delay_summary(:delay), 'Delay'
    @aggregate_delay_variation, aggregate, sla, minor_threshold, major_threshold, @cell_class_delay_variation, @cell_title_delay_variation = delay_aggregate_formatted @stats.stats_delay_summary(:delay_variation), 'Delay Variation'

    # Get exceptions
    @exceptions = @entity.metrics_monthly_cos_test_vectors.by_date_range(@graph_date_range_start.to_i, @graph_date_range_end.to_i).count(:group => [:severity, :metric_type])

    render :partial => "shared_monitoring/map_tab_section_content_monitoring"
  end


  protected

  # Override application level default dates
  def initialize_graph_dates
    # Default graph dates to today
    # If user selected last_hour then only show aggregates and exceptions from last hour, link to dashboard will show entire day
    if params[:date_range_selector] == 'last_hour'
      now = Time.now.in_time_zone
      @graph_date_range_start, @graph_date_range_end = [now, now - 1.hour]
    else
      @graph_date_range_start, @graph_date_range_end = get_date_range(params[:date_range_selector], params['date_range_start'], params['date_range_end'], 'today')
    end
  end
      
  private

  def find_model
    @entity = CosTestVector.find params[:id] if params[:id]
  end
  
end