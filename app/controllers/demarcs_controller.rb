class DemarcsController < CustomApplicationController

  before_filter :find_model, :find_collection, :monitoring_title
  
  def map_info_window
    # TODO: add security
    render :map_info_window, :layout => false
  end

  # Show member handles in tooltip
  def map_tooltip
    # Dave only wants 1 member handle
    @demarc = @demarcs.take(1)
    render :map_tooltip, :layout => false
  end

  protected

  # Each component uses a different entity to display graph tabs
  def graph_tabs
    @entity.respond_to?(:is_monitored?) && @entity.is_monitored? ? [@entity] : nil
  end

  private

  def find_collection
    @demarcs = Demarc.find params[:ids] if params[:ids]
  end

  def find_model
    @entity = Demarc.find params[:id] if params[:id]
  end

  def monitoring_title
    @monitoring_title = 'Demarc Monitoring'
  end  
  
end