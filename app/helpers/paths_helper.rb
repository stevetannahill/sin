module PathsHelper
    
  def display_value(value)
    return value if value.blank?
    value.gsub(" ", "&nbsp;")
  end

  def entity_data_urls(path_display)

    # return entity type, override some types to make things a little cleaner
    entity = path_display.entity

    # No data url unless user can see entity
    return {} unless entity_is_not_redacted?(entity)

    # convert class name into entity path for routing
    entity_path = convert_to_path(entity)
    
    # return url used to retrieve data via ajax
    urls = {}

    urls['data-ordering_url'] = if path_display.path
      send("ordering_#{entity_path}_path", entity.id, {:path_id => path_display.path.try(:id), :mode => 'ordering'}) 
    # ENNI Display is not in context of evc
    else
      send("ordering_#{entity_path}_path", entity.id, {:mode => 'ordering'}) 
    end

    urls['data-inventory_url'] = if path_display.path
      send("#{entity_path}_path", entity.id, {:path_id => path_display.path.try(:id), :mode => 'inventory'}) 
    # ENNI Display is not in context of evc
    else
      send("#{entity_path}_path", entity.id, {:mode => 'inventory'}) 
    end

    urls['data-monitoring_url'] = if show_monitoring?(entity)
      send("monitoring_#{entity_path}_path", entity.id, {:path_id => path_display.path.try(:id), :mode => 'monitoring'})
    else
      send("sm_state_#{entity_path}_path", entity.id, {:mode => 'monitoring'})
    end
    
    urls[:class] = 'data_url_present'

    urls

  end

  # urls to generate config file for demarcs
  def generate_config_data_urls(entity)
    {'data-url'=> send("genconfig_#{convert_to_path(entity)}_path", entity.id)}
  end
  
  def show_monitoring?(entity)
    entity_path = convert_to_path(entity)
      
    # Special cases when not to show monitoring bubble
    show_monitoring = case entity_path
      when 'off_net_ovc'
        entity.is_in_ls_core_transport_path
      when 'off_net_ovc_end_point_uni'
        entity.is_monitored
      when 'uni', 'demarc'
        false
      else 
        true
    end
    # TODO: we removed the additional check that you are not in ProvPending, as we still want to show graphs for ProvPending
    # show_monitoring && !entity.get_prov_state.is_a?(ProvPending)
  end


  # determines which direction the entity popup goes (north, south, east ,west)
  def entity_popup_direction(path_display, mode)
    # return entity type, override some types to make things a little cleaner
    # default_direction = 'nswe'
    # if monitoring
    #   'nwe'
    # else
    #   default_direction
    # end
    
    # Since we can switch modes in bubbles now the direction is always down
    'nwe'

  end

  # determine if pointer should be in the centered, left side or right side for views
  def pointer_class(column)
    column.slice_pointer
  end

  def evc_top_level_entity_attrs(path, mode, details)
    ApplicationHelper::APP_MODES.inject({'data-mode' => mode}) do |hash, mode|
      params = {:mode => mode}
      params.merge!({:path_details => path.id}) if details
      hash.merge!("data-#{mode}_url" => list_row_path_path(path.id, params))
    end
  end

  def enni_top_level_entity_attrs(enni, mode, details)
    ApplicationHelper::APP_MODES.inject({'data-mode' => mode}) do |hash, mode|
      params = {:mode => mode}
      params.merge!({:enni_details => enni.id}) if details
      hash.merge!("data-#{mode}_url" => list_row_enni_path(enni.id, params))
    end
  end

  def evc_ordering_state_data_url(evc) 
    ordering_state_path_path(evc.id)
  end

  def enni_ordering_state_data_url(enni) 
    ordering_state_enni_path(enni.id)
  end
  
  def evc_monitoring_state_data_url(path) 
    sm_state_path_path(path.id)
  end
  
  def enni_monitoring_state_data_url(enni) 
    sm_state_enni_path(enni.id)
  end

  def entity_ordering_data_url(path_display) 
    # return entity type, override some types to make things a little cleaner
    entity = path_display.entity
    type = case entity.class.name
      when 'OnNetOvc' then ordering_on_net_ovc_path(entity.id)
      when 'OffNetOvc' then ordering_off_net_ovc_path(entity.id)
      when 'EnniNew' then ordering_enni_path(entity.id)
      when 'Uni', 'Demarc' then ordering_demarc_path(entity.id)
      when 'OnNetOvcEndPointEnni' then ordering_on_net_ovc_end_point_enni_path(entity.id)
      when 'OvcEndPointEnni' then ordering_off_net_ovc_end_point_enni_path(entity.id)
      when 'OvcEndPointUni' then ordering_off_net_ovc_end_point_uni_path(entity.id)
      # TODO: Handle invalid type with appropriate error message, maybe even a error_path() url
      else 'Invalid URL'
    end
  end

  # return a label and value or label and default if no value
  def list_label_and_value(label, object, method, formatter=nil, default=nil, class_name=nil)

    # Assign value, either value return from method or just a value passed in
    value = if method.is_a?(Symbol) 
      object.respond_to?(method) ? object.send(method) : nil
    else
      # method is actually a value 
      method
    end
    
    # if no value found then use default
    if value.blank? || value.to_s.strip.upcase == 'N/A'
      value = default 
    # else format the value (if formatter supplied)
    else
      # format value, pass in '%s ms' as shortcut for format('%s ms')
      value = if formatter.is_a? String
        format(formatter).call(value)
      elsif formatter.is_a? Proc
        formatter.call(value)
      else
        value
      end
    end
    
    class_attr = class_name ? " class='#{class_name}'" : ''
    
    # nil default means display nothing (not even label)
    value.nil? ? '' : raw("<li#{class_attr}><label>#{label}: </label><span>#{value}</span></li>")
  end
 
  def monitoring_state_class(state)
    "monitoring #{state_to_class(state)}"
  end

  def alarm_state_class(state)
    "alarm #{state_to_class(state)}"
  end
  
  def state_to_class(state)
    state.downcase.gsub(' ', '_')
  end

  def ordering_state_class(state)
    "ordering #{ordering_current_state(state)}"
  end
  
  def fallout_exception_type_class(path, column=nil, segment=nil)
    class_str = {}
    if can? :read, :inventory_exception_reports 
      # Don't like this but has to be done for now since segment colors are done differently then other components
      if column.nil? || (column.segment? && segment) || (!column.segment? && !segment)
        class_str = { :class => "fallout_exception_type #{path.fallout_exception_type}" } if path.fallout?
      end
    end
    class_str
  end

  def inventory_state_with_fallout(path)
    if can?(:read, :inventory_exception_reports) && path.fallout?
      "#{path.fallout_exception_type.titleize} Fallout"
    else
      path.get_prov_name  
    end
  end

  def revision_audit_class(source_val, auth_val)
    if auth_val.blank? || source_val == auth_val
      class_name = "audit_same"
    elsif source_val.blank?
      class_name = "audit_missing"
    else
      class_name = "audit_different"
    end
    {:class => class_name}
  end

  def ordering_current_state(state)
    state.downcase.gsub(' ', '_')
  end

  def unique_entity_id(entity)
    "#{entity.class.name.to_s}_#{entity.id}"
  end
  
  def truncate_by_line(text, length)
    text.split("\n").map { |line|
      truncate(line, :length => length)
    }.join("\n") unless text.nil?
  end

  # Member handle for current user
  def member_handle_for_current_user(entity, user)
    PathsHelper.member_handle_for_current_user(entity, user)
  end
  
  # Member handle for current user
  def self.member_handle_for_current_user(entity, user)
    handle = entity.member_handle_for_owner_or_any_object(user.service_provider)
    handle ? handle.value : entity.cenx_name
  end

  # Member handle labels based on member attr type
  def member_handle_label(member_handle, is_system_owner=false)
    member_attr = member_handle.member_attr
    label = case member_attr.affected_entity_type
      when 'Evc'
        "#{get_term(:path)} ID"
      when 'Demarc'
        address = member_handle.affected_entity.try(:address)
        "#{member_attr.name} <span>( Owned by </span> #{member_handle.affected_entity.service_provider.name}" + (address.blank? ? '' : " <span> / Located at </span>#{address}") + "<span> )</span>"
      when 'Segment'
        "#{member_attr.name} <span>( Owned by </span> #{member_handle.affected_entity.service_provider.name}<span> )</span>"
    end
    raw label && is_system_owner ? "<span>#{member_handle.service_provider.name}'s</span> " + label : label
  end
  
  # Entire row is readacted if not of the columns have an entity
  def entire_row_redacted?(row)
    row.any?{|col| col.entity} ? false : true
  end

  # Get service providers hidden in redacted columns
  def get_redacted_service_providers(redacted_columns)
    redacted_columns.map{|col| col.entity.try(:service_provider) if col.segment?}.compact.uniq
  end

  # Show injection and reflection indicator arrows (images) over top of entities (components)
  def injection_reflection_indicators(column)
    if column.on_net_ovc? && column.injections
      # Frames are injected at cenx ovcs
      injection_indicators(column)

    # COX HACK
    elsif column.demarc? && column.injections
      injection_indicators(column)

    elsif column.on_net_router? && column.reflection_direction
      # Frames are injected at cenx ovcs
      reflection_indicator(column)
      
    elsif column.demarc? 
      # Frames are reflected at demarcs
      reflection_indicator(column)
    end
  end

  # Show injection and reflection indicator arrows (images) over top of redacted entities (components)
  def redacted_injection_indicators(redacted_columns)
    indicators = redacted_columns.inject('') do |str, column|
      # Frames are injected at cenx ovcs
      injection_ind = injection_indicators(column)
      injection_ind ? str + injection_ind : str
    end
    # Must call raw as Rails 3 escape everything
    raw(indicators)
  end
  
  # Show reflection indicator arrow (images) over top of entity (component)
  def reflection_indicator(column)
    image_src = if column.reflection_direction == :right
      "reflection_on_right.png"
    elsif column.reflection_direction == :left
      "reflection_on_left.png"
    end
    if image_src
      render :partial => '/paths/reflection_indicator', :locals => {:column => column, :image_src => image_src} 
    end
  end

  # Show injection indicator arrow (images) over top of entity (component)
  def injection_indicators(column)
    # Multiple injection points
    if column.injections
      injections_html = column.injections.map do |injection|
        image_src = case injection[:direction] 
        when :right
          "injection-right.png"
        when :left
          "injection-left.png"
        else
          "injection.png"
        end
        render :partial => '/paths/injection_indicator', :locals => {:injection => injection[:data], :image_src => image_src} 
      end
      raw(injections_html.join(' '))
    end
  end

  def monitoring_indicator(path, previous_column, column, next_column)
    render :partial => '/paths/monitoring_indicator', :locals => {:primary_backup => column.monitoring} if show_monitoring_based_on_reflection(path, previous_column, column, next_column)
  end
  
  def show_monitoring_based_on_reflection(path, previous_column, column, next_column)
    if column.end_point? && show_monitoring?(column.entity) && column.monitoring
      show = if path.layer == 3
        # TODO: This is a hack for now
        true
      elsif column.relative_position == 'Right'
        next_column.reflection_direction == :right
      elsif column.relative_position == 'Left'
        previous_column.reflection_direction == :left
      end
    end
  end

  # TODO: This will not hold up when there is more than 1000 evcs for a provider
  def path_count_for_enni(enni)
    # Trap error when no evcs have been indexed yet
    begin
      SinPathServiceProviderSphinx.by_user(current_user).search_count("", :with => {:enni_ids => enni.id}, :group => 'path_id') 
    rescue => e
      logger.debug(e)
      0
    end
  end

  # TODO: This will not hold up when there is more than 1000 evcs for a provider
  def path_count_for_evc(evc)
    begin
      SinPathServiceProviderSphinx.by_user(current_user).search_count("", :with => {:has_path_ids => evc.id}, :group => 'path_id')
    rescue => e
      logger.debug(e)
      0
    end
  end

  # Determine Side A / B for inventory bubble of Demarc/UNI
  def determine_side_a_b_for_demarc(evc, demarc_in_diagram)
    right_ovc = PathDisplay.find_by_id_and_entity_type_and_path_id(demarc_in_diagram.id + 1, 'SegmentEndPoint', evc.id).try(:entity).try(:segment)
    # [A]<--->[B]<---->[C]
    # A: Demarc, OVC
    # B: Demarc, OVC (not OVC, OVC)
    # C: OVC, Demarc
    if right_ovc
      [demarc_in_diagram.entity, right_ovc]
    else
      left_ovc = PathDisplay.find_by_id_and_entity_type_and_path_id(demarc_in_diagram.id - 1, 'SegmentEndPoint', evc.id).try(:entity).try(:segment)
      [left_ovc, demarc_in_diagram.entity]
    end
  end

  # Reflection Indicator bubble info
  # TODO: Improve SQL performance here, this is slow, maybe even denormalize into path_displays
  def reflection_indicator_bubble(entity)

    bubble = ["<strong>Monitoring&nbsp;Reflection&nbsp;Point</strong>"]

    if entity.is_a?(Demarc)
      test_types = entity.segment_end_points.select(&:is_monitored).map{ |ep| ep.cos_test_vectors.pluck(:test_type) }.flatten.uniq.join(', ')
    # TODO: Hack for Sprint Core Demo (hardcoding to match injection)
    elsif entity.is_a?(OnNetRouter)
      test_types = 'TWAMP'
      bubble << "<strong>Device Type</strong>: QoS-Scope-1G"
      bubble << "<strong>Device Name</strong>: #{entity.site.clli}-QSCOPE01"
      bubble << "<strong>Port or IF Name</strong>: TEST1"
      bubble << "<strong>VLAN</strong>: 2"
    end

    bubble << "<strong>Test Types</strong>: #{test_types}"
    bubble.join('<br />')
    
  end

  # Injection Indicator bubble info
  # TODO: Improve SQL performance here, this is slow, maybe even denormalize into path_displays
  def injection_indicator_bubble(injection)
    
    connected_ports = injection.demarc.ports.map(&:connected_port)
    
    vlans = injection.outer_tag_value
    port_or_if = connected_ports.map(&:name).uniq.join(', ')
    device_name = connected_ports.map{|p| p.node.name}.uniq.join(', ')
    device_type = connected_ports.map{|p| p.node.node_type}.uniq.join(', ')
    
    bubble = ["<strong>Monitoring&nbsp;Injection&nbsp;Point</strong>"]
    bubble << "<strong>Device Type</strong>: #{device_type}"
    bubble << "<strong>Device Name</strong>: #{device_name}"
    bubble << "<strong>Port or IF Name</strong>: #{port_or_if}"
    bubble << "<strong>VLAN</strong>: #{vlans}"
    bubble.join('<br />')
    
  end

  # Help on hover for demarcs
  def entity_help(path_display)
    if path_display.demarc?
      demarc = path_display.entity

      member_handles = provider_member_handles(demarc).map do |member_handle|
        list_label_and_value member_handle[:label], nil, member_handle[:value] if member_handle
      end

      title = content_tag :ul, :class => 'list' do
        member_handles.join(' ') + list_label_and_value('CENX ID', demarc, :cenx_id)
      end
      {
        :class => 'help_on_hover',
        :title => title
      }
      
    else
      {}
    end
  end
  
  def site_component_groups(previous_column, column, next_column)
    [previous_column.try(:entity).try(:demarc).try(:site), next_column.try(:entity).try(:demarc).try(:site)]
  end

  def node_component_groups(previous_column, column, next_column)
    [previous_column.try(:entity).try(:nodes_involved).try(:first), next_column.try(:entity).try(:nodes_involved).try(:first)]
  end

end