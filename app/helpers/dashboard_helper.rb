module DashboardHelper
  include ActionView::Helpers::TextHelper
  
  STAT_TITLE_CLASS = 'dashboard_sub_title'
  STAT_LIST_TITLE_CLASS = 'dashboard_sub_title_list'
  STAT_CLASS = 'dashboard_stat'
  STAT_TEXT_CLASS = 'dashboard_stat_text'  
  PROVISIONING_STATES = [
    {:name => 'fallout', :color => 'ba2a2a'},
    {:name => 'requested', :color => 'b591f8'},
    {:name => 'pending', :color => 'd9d9d9'},
    {:name => 'designed', :color => 'c5d3fa'},
    {:name => 'testing', :color => 'c5e8fa'},
    {:name => 'ready', :color => '009ddb'},
    {:name => 'live', :color => '0c60b7'},
    {:name => 'maintenance', :color => '858585'},
    {:name => 'deleted', :color => '000000'}
  ]           
  ORDERING_STATES = [
    {:name => 'submitted', :color => 'ffe6c8'},
    {:name => 'created', :color => 'ffe6c8'},
    {:name => 'ready_to_order', :color => 'ffd2a1'},
    {:name => 'received', :color => 'ffc26e'},
    {:name => 'order_accepted', :color => 'e6a043'},
    {:name => 'design_complete', :color => 'b66a27'},
    {:name => 'provisioned', :color => 'bc9248'},
    {:name => 'tested', :color => '997f5b'},
    {:name => 'customer_accepted', :color => '68563d'},
    {:name => 'delivered', :color => '432e0f'},
    {:name => 'rejected', :color => 'aaaaaa'},
    {:name => 'cancelled', :color => '777777'}
  ]
  MONITORING_STATES = [
    {:name => 'no_state', :color => '333333', :order => 7},
    {:name => 'maintenance', :color => '858585', :order => 4},
    {:name => 'not_live', :color => 'd9d9d9', :order => 6},
    {:name => 'not_monitored', :color => '74a1ae', :order => 5},
    {:name => 'unavailable', :color => '640d0d', :order => 1},
    {:name => 'failed', :color => 'ba2a2a', :order => 2},
    {:name => 'warning', :color => 'ff8d20', :order => 3},
    {:name => 'dependency', :color => 'dcde32', :order => 8},
    {:name => 'ok', :color => '609c25', :order => 9}
  ]

  FALLOUT_CATEGORY_COLORS = [

    {:name => 'ready_to_order', :color => '010f85', :order => 1},
    {:name => 'ready_to_order_evc', :color => 'a9df00', :order => 2},

    {:name => 'aav_uploaded', :color => '010f85', :order => 3},
    {:name => 'aav_nni', :color => '0417ba', :order => 4},
    {:name => 'aav_vlan', :color => '085dba', :order => 5},
    {:name => 'aav_cir', :color => '0894ba', :order => 6},
    {:name => 'ipa_nni_status', :color => 'a14800', :order => 7},
    {:name => 'ipa_nni', :color => 'd46000', :order => 8},
    {:name => 'ipa_vlan', :color => 'd47f05', :order => 9},
    {:name => 'ipa_cir', :color => 'd49803', :order => 10},
    {:name => 'built_on_ipa', :color => 'd4aa02', :order => 11},
    {:name => 'ipa_xc', :color => 'd4bf00', :order => 12},
    {:name => 'ipa_bfd', :color => 'd4cf00', :order => 13},
    {:name => 'ipa_ip_interface', :color => 'c0d400', :order => 14},
    {:name => 'ipa_static_route', :color => 'a9d400', :order => 15},

    {:name => 'samsung_uploaded', :color => '0417ba', :order => 16},
    {:name => 'samsung_nrm_uploaded', :color => '0417ba', :order => 17},
    {:name => 'samsung_cir', :color => '010f85', :order => 18},
    {:name => 'samsung_nrm_cir', :color => '010f85', :order => 19},
    {:name => 'samsung_topology', :color => '085dba', :order => 20},
    {:name => 'samsung_nrm_topology', :color => '085dba', :order => 21},
    {:name => 'samsung_se1200-1_vlan', :color => '0894ba', :order => 22},
    {:name => 'samsung_se1200-2_vlan', :color => 'a14800', :order => 23},
    {:name => 'samsung_aav', :color => 'd46000', :order => 24},
    {:name => 'samsung_mw_site', :color => 'd47f05', :order => 25},
    {:name => 'samsung_nrm_mw_site', :color => 'd47f05', :order => 26},
    {:name => 'samsung_q-scope_vlan_-_primary', :color => '010f85', :order => 27},
    {:name => 'samsung_q-scope_vlan_-_alternate', :color => '0417ba', :order => 28},

    {:name => 'order_accepted', :color => '010f85', :order => 29},
    {:name => 'order_accepted_evc', :color => '0417ba', :order => 30},
    {:name => 'order_provisioned', :color => '085dba', :order => 31},
    {:name => 'order_delivered', :color => '0894ba', :order => 32},

    {:name => 'yoigo_ll_site_destination_service_id', :color => '0417ba', :order => 33},
    {:name => 'yoigo_ll_destination_service_type', :color => '010f85', :order => 34},
    {:name => 'yoigo_ll_capacity', :color => '085dba', :order => 35},
    {:name => 'yoigo_ll_site_name_origin', :color => '0894ba', :order => 36},
    {:name => 'yoigo_ll_service_provider_id', :color => 'a14800', :order => 37},
    {:name => 'yoigo_mw_site_destination_service_id', :color => '0417ba', :order => 38},
    {:name => 'yoigo_mw_destination_service_type', :color => '010f85', :order => 39},
    {:name => 'yoigo_mw_capacity', :color => '085dba', :order => 40},
    {:name => 'yoigo_mw_site_name_origin', :color => '0894ba', :order => 41},

    {:name => 'tellabs_id', :color => 'c0d400', :order => 42},
    {:name => 'rbs_oam_ip_subnet', :color => 'd47f05', :order => 43},
    {:name => 'rbs_iub_ip_subnet', :color => 'd47f05', :order => 44},

    {:name => 'other', :color => 'cbd400', :order => 99}
  ]

  def monitoring_states
    MONITORING_STATES
  end
  
  def ordering_states
    ORDERING_STATES
  end
  
  def provisioning_states
    PROVISIONING_STATES.reject{ |s| s[:name] == 'fallout' && cannot?(:read, :inventory_exception_reports) && cannot?(:read, :ordering_exception_reports)}
  end

  def fallout_categories
    FALLOUT_CATEGORY_COLORS
  end

  def provisioning_color(state_name)
    provisioning_states.find {|p| p[:name] == state_name.parameterize('_') }[:color]
  end

  def ordering_color(state_name)
    ordering_states.find {|p| p[:name] == state_name.parameterize('_') }[:color]
  end
  
  def monitoring_color(state_name)
    monitoring_states.find {|p| p[:name] == state_name.parameterize('_') }[:color]
  end
  
  def fallout_category_color(category)
    (fallout_categories.find {|p| p[:name] == category.parameterize('_') } || {})[:color]
  end


  def state_text(value, threshold_high, threshold_low)
    if value >= threshold_high
      monitoring_states.find {|p| p[:name] == 'failed'}[:color]
    elsif value > threshold_low && value < threshold_high
      monitoring_states.find {|p| p[:name] == 'warning'}[:color]
    else
      monitoring_states.find {|p| p[:name] == 'ok'}[:color]
    end
  end
  
  def convert_seconds_to_time(seconds)
    DashboardHelper.convert_seconds_to_time(seconds)
  end
  
  def self.convert_seconds_to_time(seconds)
    time_sentence = []
    seconds = seconds.to_i
    total_minutes = seconds / 1.minutes
    seconds_in_last_minute = seconds - total_minutes.minutes.seconds
    total_hours = total_minutes.minutes / 1.hour
    minutes_in_last_hour = (total_minutes.minutes - total_hours.hours) / 1.minutes

    time_sentence << self.pluralize(total_hours.to_i, 'hour') if total_hours > 0    
    time_sentence << self.pluralize(minutes_in_last_hour.to_i, 'minute') if minutes_in_last_hour > 0
    time_sentence << self.pluralize(seconds_in_last_minute.to_i, 'second') if seconds_in_last_minute > 0
    
    time_sentence_str = time_sentence.join(', ')

    return time_sentence.blank? ? 'N/A' : time_sentence_str
  end
  
  def self.pluralize(count, singular, plural = nil)
     "#{count || 0} " + ((count == 1 || count =~ /^1(\.0+)?$/) ? singular : (plural || singular.pluralize))
   end
  
  def dashboard_stat(title, count, html_options={})
    tag_options = tag_options(html_options)
    
    title_tag = sub_title(title)
    count_tag = content_tag("div", html_escape(number_with_delimiter(count)), :class => STAT_CLASS)
    
    "<div #{tag_options}>#{title_tag} #{count_tag}</div>".html_safe
  end

  def dashboard_stat_list(title, text, html_options={})
    tag_options = tag_options(html_options)

    title_tag = content_tag("strong", html_escape(title), :class => STAT_LIST_TITLE_CLASS)
    info_tag = content_tag("div", html_escape(text), :class => STAT_TEXT_CLASS)
    
    "<div #{tag_options}>#{title_tag} #{info_tag}</div>".html_safe
  end

  def dashboard_interactive_pie(options={})
    if options[:data].length > 0
      title = sub_title(options[:title])
      options[:id] ||= "graph_#{rand(Time.now.to_i)}"
      options[:width] ||= 300
      options[:height] ||= 200
      options[:tooltip] ||= 'hover' 
      render :partial => 'dashboards/interactive_pie', :locals => {:options => options, :title => title}
    end  
  end

  def dashboard_pie(options={})
    if options[:data].length > 0
      title = sub_title(options[:title])
      image = image_tag(Gchart.pie(graph_options(options)), :alt => options[:title])

      "#{title} #{image}".html_safe
    end
  end
  
  
  def dashboard_pie_3d(options={})
    if options[:data].length > 0
      title = sub_title(options[:title])
      image = image_tag(Gchart.pie_3d(graph_options(options)), :alt => options[:title])

      "#{title} #{image}".html_safe
    end
  end
  
  def circuit_owner_title(owner_param)
    return ""
    # case owner_param
    #   when "user"
    #     "Bought by #{@current_user.service_provider.name}"
    #   when "other"
    #     "Sold by #{@current_user.service_provider.name}"
    #   when "all"
    #     "Total"
    #   when "admin"
    #     "Total"
    # end
  end
  
  
  def sub_dashboard_path(params={})
    if @mode == 'inventory'
      inventory_dashboard_path(params)      
    elsif @mode == 'ordering'
      ordering_dashboard_path(params)
    elsif @mode == 'monitoring'
      monitoring_dashboard_path(params)
    end
  end
  
  protected
  
  def sub_title(text)
    text.blank? ? '' : content_tag("h4", html_escape(text), :class => STAT_TITLE_CLASS)
  end
  
  
  def graph_options(options={})
    options[:width] ||= 200
    options[:height] ||= 100
    options[:use_ssl] ||= true
    options[:background] ||= '00000000'
    
    { 
      :use_ssl => options[:use_ssl],
      :data => options[:data], 
      :labels => options[:labels], 
      :size => "#{options[:width]}x#{options[:height]}",
      :bar_colors => options[:bar_colors],
      :background => options[:background]
    }
  end
  
end
