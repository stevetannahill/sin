module OrderingHelper
  
  def order_heading(heading)
    # NOTE: content_tag is slow so just use strings
    "<div class=\"order_heading\"><h5>#{heading}</h5></div>".html_safe
  end 

  def ordering_field_update_url(entity, field)
    send('ordering_field_update_' + convert_to_path(entity) + '_path', entity.id, {:field => field})
  end

  def order_field(form, field, format, index=0, *attributes)
    # Get value
    value = order_field_value(form, field, index)
    # html for label and field
    # NOTE: content_tag is slow so just use strings
    str  = "<span class=\"#{value.blank? ? 'no_value' : ''} laf_wrapper\"><div#{attributes_string(attributes)}>"
    # Format could be a symbol which means it is a helper method to format the string, or an integer which is just a length
    str += order_label(field) + (format.is_a?(Symbol) ? send(format, value) : order_field_display(value, format))
    str += "</div></span>"
    str.html_safe
  end 

  def order_field_edit(entity, component_html_id, form, field, format, index=0, *attributes)
    # Get value
    value = order_field_value(form, field, index)
    str  = "<div class=\"editable_wrapper hidden\">"
    str += "<div id=\"order_field_update\" #{attributes_string(attributes)}>"
    str += order_label(field) + (format.is_a?(Symbol) ? send(format, value) : order_field_input(value, format))
    str += "<button class=\"submit_button\" type=\"submit\" data-url=\"#{ordering_field_update_url(@entity, field)}\" data-component_html_id=\"#{component_html_id}\">Save</button>"
    str += "</div></div>"
    str.html_safe
  end 

  # TODO: Bit of a hack until we get a proper workflow
  def next_state_disabled(entity, next_state)
    disabled, reason = if next_state == OrderReceived
      [entity.get_latest_order.order_received_date.blank?, "D/TREC must be filled in before advancing state to Received"]
    elsif entity.is_a?(EnniNew) && next_state == OrderAccepted
      [(entity.far_side_clli.empty? || entity.far_side_clli == 'TBD'), "SECLOC must be filled in before advancing state to Accepted"]
    end
  end

  protected

  def order_field_value(form, field, index)
    value = form.field(field)
    value = value[index] if value.kind_of?(Array) 
    value.nil? ? '' : value
  end
  
  def attributes_string(attributes)
    (attributes.empty? ? {} : attributes.first).map{ |k,v| " #{k.to_s}=\"#{v}\"" }.join(' ')
  end  

  def order_label(label)
    # NOTE: content_tag is slow so just use strings
    "<label>#{order_blank(label)}</label>"
  end

  def order_field_display(value, length)
    value = '' if value.nil?
    # put each character into a seperate span
    value.ljust(length).chars.reduce('') { |c, x|
      # NOTE: content_tag is slow so just use strings
      c + "<span#{x.blank? ? '' : ' class="highlight"'}>#{order_blank(x)}</span>"
    }
  end 

  def order_field_input(value, length)
    value = '' if value.nil?
    "<input size=\"#{length.to_i + 5}\" maxlength=\"#{length}\" value=\"#{value}\"/>"
  end 
  
  # Used for Sprint order forms
  def order_field_delimiter(string)
    # NOTE: content_tag is slow so just use strings
    "<span class=\"order_field_delimiter\"><em>#{string}</em></span>"
  end
  
  def order_blank(str='')
    str.blank? ? escape_once('&nbsp;') : str
  end
  
  def gbtn_group(value)
    p1, p2, p3 = value.split('-')
    order_field_display(p1, 3) + order_field_delimiter('-') + order_field_display(p2, 3) + order_field_delimiter('-') + order_field_display(p3, 4)
  end

  def telephone_3_group(value)
    p1, p2, p3 = value.gsub(/ /,'-').gsub(/1-/,'').split('-')
    order_field_display(p1, 3) + order_field_delimiter('-') + order_field_display(p2, 3) + order_field_delimiter('-') + order_field_display(p3, 4)
  end

  def telephone_4_group(value)
    p1, p2, p3, p4 = value.gsub(/ /,'-').gsub(/1-/,'').split('-')
    order_field_display(p1, 3) + order_field_delimiter('-') + order_field_display(p2, 3) + order_field_delimiter('-') + order_field_display(p3, 4) + order_field_delimiter('-') + order_field_display(p4, 4)
  end

  def vlan_group(value)
    p1, p2 = value.split('-')
    order_field_display(p1, 4) + order_field_delimiter('-') + order_field_display(p2, 4)
  end

  def cpt_group(value)
    p1, p2 = value.split('-')
    order_field_display(p1, 5) + order_field_delimiter('-') + order_field_display(p2, 5)
  end
  
  def remarks_124_group(value)
    order_field_display(value[0,74], 75) + order_field_display(value[75, 123], 49)
  end
  
  def remarks_186_group(value)
    order_field_display(value[0,61], 62) + order_field_display(value[62, 123], 62) + order_field_display(value[124, 185], 62)
  end

  def istn_group(value)
    p1, p2, p3 = value.split('-')
    order_field_display(p1, 3) + order_field_delimiter('-') + order_field_display(p2, 3) + order_field_delimiter('-') + order_field_display(p3, 4)
  end

  def nor_group(value)
    p1, p2 = value.split(' (of) ')
    order_field_display(p1, 2) + order_field_delimiter('of') + order_field_display(p2, 2)
  end

  def tsp_group(value)
    p1, p2 = value.split('-')
    order_field_display(p1, 9) + order_field_delimiter('-') + order_field_display(p2, 2)
  end

  # formatters for views
  # format using string format syntax, ie format = '%s ms'
  def format(format)
    proc { |x| format % x }
  end

  def format_percentage(multiplier=1)
    proc { |x| numeric?(x) ? "#{x.to_f * multiplier}%" : x }
  end

  def format_yes_no
    proc { |x| x ? 'Yes' : 'No' }
  end
  
  def format_mail
    proc { |x| x.downcase == 'tbd' ? x : mail_to(x) }
  end

  def format_address(length = 40)
    proc { |x| simple_format(truncate_by_line(x, length)) }
  end
  
  def format_kbps_to_mbps
    proc { |x| '%s Mbps' % (x.to_i / 1000) }
  end
  
  def format_truncate(length)
    proc { |x| truncate(x, :length => length) }
  end
  
  def format_utc
    proc { |x| x.utc }
  end
  
end