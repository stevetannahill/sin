module NetworkHelper
  def date_range_options(date_range_link, date_range_selected)
    {:class => date_range_link == date_range_selected ? 'primary' : 'alternate', 'data-date_range_selector' => date_range_link, 'onclick' => 'return map_info_window_change_date_range(this)'}
  end

  def pop_open_graph_path(entity, graph_type, mode)
    cos_end_point = entity.cos_end_point
    segment_end_point = cos_end_point.segment_end_point
    paths_path({
      filter: entity.circuit_id, 
      entity_id: segment_end_point.id, 
      entity_type: segment_end_point.class, 
      tab_id: cos_end_point.id, 
      section_id: entity.id, 
      graph_type: graph_type, 
      date_range_start: @graph_date_range_start.to_s,
      date_range_end: @graph_date_range_end.to_s, 
      mode: mode
    })
  end

end