#OSL_DATA_SOURCES and FALLOUT_CATEGORIES specific to master

# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper

  APP_MODES = %w(inventory monitoring ordering)

  # TODO: Best place for OSL_DATA_SOURCES constant?
  OSL_DATA_SOURCES = {
    att_odp:  'VIP', 
    ipag01:   'Aggregation Router 1', 
    ipag02:   'Aggregation Router 2', 
    ip_ptp: 'BH IP Node OAM SPx10-SE1200-PTP',
    ip_ptp_cdn: 'BH IP Bearer SPx10-SE1200-PTP',
    ip_mpt: 'BH IP Node OAM SPx10-SE1200-PMP',
    ip_mpt_cdn: 'BH IP Bearer SPx10-SE1200-PMP',
    ip_local: 'BH IP Node OAM MMBTS-SPx10',
    ip_xmu_dul: 'BH IP Node OAM MMBTS-SPx10 3G XMU DUL',
    ip_oam: 'BH IP SPx10 Loopback Address OAM',
    ip_bearer_3g: 'BH IP Bearer MMBTS-SPx10 3G',
    ip_bearer_lte: 'BH IP Bearer MMBTS-SPx10 4G',
    vlan:     'VLAN',
    ems:      'EMS', 
    sitehandler: 'Sitehandler',
    att_pon: 'ATT PON',
    att_foc: 'ATT FOC UNI',
    att_foc_evc: 'ATT FOC EVC',
    audit:    'Audit',
    att_template: 'ATT Template',
    activation_notice: 'ATT Activation Notice',
    alu_build_data_uni_evc: 'ALU Build Data - UNI/EVC',
    alu_build_data_vlan: 'ALU Build Data - VLAN',
    samsung_build_data_ip: 'Samsung Build Data - IP',
    samsung_build_data_asr: 'Samsung Build Data - ASR',
    nrm_lte_cell_site_service_instance: 'NRM LTE Cell Site Service Instance',
    nrm_lte_cell_site_service_order: 'NRM LTE Cell Site Service Order',
    spirent_feed: 'Spirent Feed',

    yoigo_conectividad: "Conectividad",
    yoigo_conectividad_ll: "Conectividad LL",
    yoigo_conectividad_re: "Conectividad MW",
    yoigo_enlaces_lls: "Enlaces",
    yoigo_leasedlines: "Leased Lines",
    yoigo_listadonodo: "Listadonodo",
    yoigo_nominales: "Nominales",
    yoigo_radiolinks: "Radio Links",
    yoigo_rnc: "RNC",
    yoigo_tlp: "TLP",
    yoigo_rnc_B: "RNC B",
    yoigo_ipag01: "IPAG01",
    yoigo_ipag02: "IPAG02",
    yoigo_iublink: "IubkLink",
    yoigo_ip_siu: "IP SIU",
    yoigo_rbs: "RBS"
  }

  AUDIT_FALLOUT_TYPES = {
    "audit" => { :privilege => :inventory_exception_reports, :name => (App.is?(:coresite) ? "Fallout" : "Inventory Exception"), :fallout_exception_type => "audit", :mode => "inventory" } , # TODO, update to inventory when done
    "ordering" => { :privilege => :ordering_exception_reports, :name => "Ordering Exception", :fallout_exception_type => "ordering", :mode => "ordering" },
  }

  def time_zone_options
    time_zone_options_for_select(Time.zone.name, [ActiveSupport::TimeZone['UTC'], ActiveSupport::TimeZone.us_zones].flatten)
  end

  def timestamp_in_time_zone(timestamp)
    timestamp = timestamp.to_i
    Time.at(timestamp > 9999999999 ? timestamp / 1000 : timestamp).in_time_zone
  end

  def timestamp_to_date_time(timestamp)
    timestamp_in_time_zone(timestamp).to_formatted_s(:ymd_time)
  end

  def timestamp_to_date(timestamp)
    timestamp_in_time_zone(timestamp).to_formatted_s(:ymd)
  end

  def fallout_category_names(fallout_exception_type = nil, oems = nil)
    fallout_exception_type ||= params[:fallout_exception_type]
    oems ||= OperatorNetworkType.select("name") if current_user.system_admin?
    oems ||= current_user.operator_network_types
    return FALLOUT_CATEGORIES.values.sort if oems.blank?
    oem_names = oems.collect { |n| n.name }.join("\", \"")
    names = cmaudit_app.cmd("fallout categories-by-oem #{fallout_exception_type} \"#{oem_names}\"")[:data]
    names
  end

  FALLOUT_CATEGORIES = {

    # RAISED BY ANY CIRCUIT
    ready_to_order: 'Ready to Order',
    ready_to_order_evc: 'Ready to Order EVC',

    # OEM-ERICSSON CIRCUITS
    aav_uploaded: 'AAV Uploaded',
    aav_nni: 'AAV NNI',
    aav_vlan: 'AAV VLAN',
    aav_cir: 'AAV CIR',
    built_on_ipa: 'built on IPA',
    ipa_nni_status: 'IPA NNI Status',
    ipa_nni: 'IPA NNI',
    ipa_vlan: 'IPA VLAN',
    ipa_cir: 'IPA CIR',
    ipa_xc: 'IPA XC',
    ipa_bfd: 'IPA BFD',
    ipa_ip_interface: 'IPA IP Interface',
    ipa_static_route: 'IPA Static Route',

    # OEM-SAMSUNG CIRCUITS
    samsung_uploaded: 'Samsung Uploaded',
    samsung_nrm_uploaded: 'Samsung NRM Uploaded',
    samsung_cir: 'Samsung CIR',
    samsung_nrm_cir: 'Samsung NRM CIR',
    samsung_topology: 'Samsung Topology',
    samsung_nrm_topology: 'Samsung NRM Topology',
    :"samsung_se1200-1_vlan" => 'Samsung SE1200-1 VLAN',
    :"samsung_se1200-2_vlan" => 'Samsung SE1200-2 VLAN',
    samsung_aav: 'Samsung AAV',
    samsung_mw_site: 'Samsung MW Site',
    samsung_nrm_mw_site: 'Samsung NRM MW Site',
    :"samsung_q-scope_vlan_-_primary" => 'Samsung Q-Scope VLAN - Primary',
    :"samsung_q-scope_vlan_-_alternate" => 'Samsung Q-Scope VLAN - Alternate',

    # Yoigo
    #  - oem: Access and Transport Network,
    yoigo_ll_conectividad_uploaded: "Yoigo LL Conectividad Uploaded",
    yoigo_mw_conectividad_uploaded: "Yoigo MW Conectividad Uploaded",
    yoigo_mw_destination_service_type: "Yoigo MW Destination Service Type",
    yoigo_mw_site_destination_service_id: "Yoigo MW Site Destination Service Id",
    yoigo_mw_site_name_origin: "Yoigo MW Site Name Origin",
    yoigo_mw_capacity: "Yoigo MW Capacity",
    yoigo_ll_destination_service_type: "Yoigo LL Destination Service Type",
    yoigo_ll_site_destination_service_id: "Yoigo LL Site Destination Service Id",
    yoigo_ll_site_name_origin: "Yoigo LL Site Name Origin ",
    yoigo_ll_capacity: "Yoigo LL Capacity",
    yoigo_ll_service_provider_id: "Yoigo LL Service Provider Id",
    tellabs_id: "Tellabs ID",
    rbs_oam_ip_subnet: "RBS OAM IP Subnet",
    rbs_iub_ip_subnet: "RBS IUB IP Subnet",

    # INTERNAL FALLOUT CATEGORIES -- MOST LIKELY NEVER RAISED
    order_accepted: 'Order Accepted',
    order_accepted_evc: 'Order Accepted EVC',
    order_provisioned: 'Order Provisioned',
    order_delivered: 'Ordere Delivered'
  }    

  # Override will paginate page_entries_info so we can use get_term properly (ie ENNI instead enni)
  def page_entries_info(collection, options = {})
    entry_name = options[:entry_name] || (collection.empty? ? 'item' : collection.first.class.name.split('::').last.titleize)
    if collection.total_pages < 2
      case collection.size
      when 0; "No #{entry_name.pluralize} found"
      when 1; "Displaying 1 #{entry_name}"
      else;   "Displaying all #{collection.length} #{entry_name.pluralize}"
      end
    else
      %{Displaying #{entry_name.pluralize} %d - %d of %d in total} % [
        collection.offset + 1,
        collection.offset + collection.length,
        collection.total_entries
      ]
    end
  end

  # Can user view requested page in requested mode, if not return a mode they can view the page in.
  def get_mode_for_page(mode, page)
    can?(:read, "#{mode}_#{page}".to_sym) ? mode : APP_MODES.find{ |m| can?(:read, "#{m}_#{page}".to_sym) }
  end

  def get_term(term, pluralize=false)
    term = @current_user.service_provider.terms[term.to_s.parameterize('_').to_sym]
    term = term.pluralize if term && pluralize
    term
  end

  def replace_terms(str)
    to_return = []
    str.split.each do |term|
      term_replace = @current_user.service_provider.terms[term.parameterize('_').to_sym] || term
      to_return << term_replace 
    end
    to_return.join(' ')
  end

  def hinted_text_field_tag(name, value = nil, hint = "Click and enter text", options={})
    value = value.nil? ? hint : value
    text_field_tag name, value, {:onclick => "if($(this).value == '#{hint}'){$(this).value = ''}", :onblur => "if($(this).value == ''){$(this).value = '#{hint}'}" }.update(options.stringify_keys)
  end

  def asset_url(asset)
    "#{request.protocol}#{request.host_with_port}#{asset_path(asset)}"
  end

  def system_logo(pdf=false, url=false)
    provider_logo(ServiceProvider.where(:is_system_logo => true).first || ServiceProvider.system_admins.first, pdf, url)
  end

  def provider_logo(service_provider, pdf=false, url=false)
    unless service_provider.blank? || service_provider.logo_url.blank?
      # width and height set so chrome knows how to display page and vertical lines on evc diagrams are sized properly on page load
      logo = "providers/#{service_provider.logo_url}.png"
      options = {:class => 'provider_logo', :width => 150, :height => 37}
      if pdf
        wicked_pdf_image_tag logo, options
      elsif url
        asset_url logo
      else
        image_tag logo, options
      end
    end
  end

  def date_range_string(start_date, end_date)
    if start_date == end_date
      start_date
    else
      # Rails 3 escapes all strings now, so return raw
      raw case (start_date.blank? ? '0' : '1') + (end_date.blank? ? '0' : '1')
        when '11' then "<em>from</em> #{start_date} <em>to</em> #{end_date}"
        when '00' then "All"
        when '01' then "<em>before</em> #{end_date}"
        when '10' then "<em>after</em> #{start_date}"
      end
    end
  end
  
  def convert_to_path(entity)
    # convert class name into entity path for routing
    class_name = entity.is_a?(String) ? entity : entity.class.name
    case class_name
      when 'EnniNew'
        'enni'
      when 'Uni'
        'demarc'
      when 'OvcEndPointEnni'
        'off_net_ovc_end_point_enni'
      when 'OvcEndPointUni'
        'off_net_ovc_end_point_uni'
      # Special case for cos test vector (ie SpirentCosTestVector)
      when 'Node'
        'node'
      else
        if entity.is_a?(CosTestVector) 
          'cos_test_vector' 
        else
          class_name.underscore
        end
    end
  end
  
  def data_url(entity, path, query_hash)
    send("#{path}_#{convert_to_path(entity)}_path", entity.id, query_hash)
  end
  
  
  def get_graphs_supported(stats)

    # List of supported grapsh
    supported = stats ? stats.get_supported_stats : []
    # Second step to get supported graphs, :delay means could be :delay or :delay_variation or both
    supported = supported - [:delay] + stats.get_delay_attributes(:sla) if supported.include?(:delay)
    
    graphs = [
      {:supported => :availability,
       :name => 'availability'
      },
      {:supported => :flr,
       :name => 'frame_loss_ratio'
      },
      {:supported => :delay,
       :name => 'delay'
      },
      {:supported => :delay_variation,
       :name => 'delay_variation'
      },
      {:supported => :traffic,
       :name => 'troubleshooting'
      }
    ]

    graphs.select { |graph| supported.include?(graph[:supported]) }
    
  end

  #Helper mbps functions
  def humanize_bytes(str, format=:mbps)
    num = str.to_i
    if format == :mbps
      value = if num < 1000
        num.to_s + ' Mbps'
      else
        (num / 1000).to_s + ' Gbps'
      end
    end
  end

  def general_type_to_string(general_type)
    # TODO use get_term instead of hard coding
    {'agg_site' =>  get_term('aggregation_site'), 'wdc'=> get_term('wdc'), 'gateway_site'=> get_term('gateway_site')}[general_type]
  end

end
