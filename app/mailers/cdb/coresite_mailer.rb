if RUBY_PLATFORM != "java"
  # Jruby does not support ntlm authenication
  require 'ntlm/smtp'
end

# NOTE: each email method must call EmailConfig.instance even if it's not used. This method
# gets the current email config but also set the ActionMailer perform_deliveries. The reason this must be called
# is that emails can be sent from different applications/daemons so setting the perform_delivery in one app won't update the other apps
# so they must reload the EmailConfig to set the perform_deliveries.
# It would be nice if ActionMailer had a before_filter but it does not..
class CoresiteMailer < ActionMailer::Base
  cattr_accessor :cdb_url, :instance_writer => false
  cattr_accessor :sin_url, :instance_writer => false
  @@cdb_url = CENX_APPS[:cdb]
  @@sin_url = CENX_APPS[:sin]
  @@sin_host = @@sin_url.sub(/http[s]?:\/\//, '').sub(/\/$/, '')
  @@cdb_host = @@cdb_url.sub(/http[s]?:\/\//, '').sub(/\/$/, '')
  
  @@server = Socket.gethostname.split(".").first
  @@server = (@@server == "gandalf") ? "Live" : @@server

  FROM_ADDRESS = "automailer@cenx.com"
  ENNI_NEW_SUBJECT = "An NNI has been created"
  ENNI_DELETE_SUBJECT = "An NNI has been disconnected"
  
  FAILURE_SUBJECT = "An Order has failed"
  
  default :from => FROM_ADDRESS,
          :reply_to => "do-not-reply@cenx.com"
  
  # Order Placed emails
  def coresite_order_placed(path, order_info)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = "New"
    # Add the work_order_id
    @order_info[:work_order_id] = "TBD"
    set_coresite_info(@order_info)
    @coresite_contact = coresite_contact
    mail(:to => @coresite_contact.e_mail, :subject => "#{@@server}: #{placed_subject}")
  end

  def buyer_order_placed(path, order_info)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = "New"
    # Don't show Target Tenant port/VLAN
    [:nni_z, :vlan_z].each {|key| @order_info.delete(key)}
    # Add the work_order_id
    @order_info[:work_order_id] = "TBD"
    set_nni_info(order_info)
    @coresite_contact = coresite_contact
    @tenant_contacts = tenant_contacts(order_info[:tenant_a], order_info[:sitename_a])
    to = @tenant_contacts.collect {|tenant| tenant.e_mail}
    mail(:to => to, :subject => "#{@@server}: #{placed_subject} with CoreSite")
  end
  
  def seller_order_placed(path, order_info)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = "New"
    # Don't show Buyer Tenant port/VLAN
    [:nni_a, :vlan_a].each {|key| @order_info.delete(key)}
    set_nni_info(order_info)
    @coresite_contact = coresite_contact
    @tenant_contacts = tenant_contacts(order_info[:tenant_z], order_info[:sitename_z])
    to = @tenant_contacts.collect {|tenant| tenant.e_mail}
    mail(:to => to, :subject => "#{@@server}: #{placed_subject} with CoreSite")
  end
   
   
  # Order Accepted emails
  def coresite_order_accepted(path, order_info)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = "New"
    # Add the work_order_id
    @order_info[:work_order_id] = path.member_handle_instance_by_owner(ServiceProvider.system_owner).value
    set_coresite_info(@order_info)
    @coresite_contact = coresite_contact
    mail(:to => @coresite_contact.e_mail, :subject => "#{@@server}: #{accepted_subject}")      
  end

  def buyer_order_accepted(path, order_info)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = "New"
    # Don't show Target Tenant port/VLAN
    [:nni_z, :vlan_z].each {|key| @order_info.delete(key)}
    # Add the work_order_id
    @order_info[:work_order_id] = path.member_handle_instance_by_owner(ServiceProvider.system_owner).value
    set_nni_info(order_info)
    @coresite_contact = coresite_contact
    @tenant_contacts = tenant_contacts(order_info[:tenant_a], order_info[:sitename_a])
    to = @tenant_contacts.collect {|tenant| tenant.e_mail}
    mail(:to => to, :subject => "#{@@server}: #{accepted_subject}")
  end
  
  def seller_order_accepted(path, order_info)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = "New"
    # Don't show Buyer Tenant port/VLAN
    [:nni_a, :vlan_a].each {|key| @order_info.delete(key)}
    set_nni_info(order_info)
    @coresite_contact = coresite_contact
    @tenant_contacts = tenant_contacts(order_info[:tenant_z], order_info[:sitename_z])
    to = @tenant_contacts.collect {|tenant| tenant.e_mail}
    mail(:to => to, :subject => "#{@@server}: #{accepted_subject}")
  end 
  
  
  # Change Order emails
  def coresite_change_order(path, order_info)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = "Change"  
    # Add the old work_order_id
    @order_info[:old_work_order_id] = path.member_handle_instance_by_owner(ServiceProvider.system_owner).value
    set_coresite_info(@order_info)
    @coresite_contact = coresite_contact
    mail(:to => @coresite_contact.e_mail,
      :subject => "#{@@server}: #{placed_subject}",
      :template_name => "coresite_order_placed")
      
  end

  def buyer_change_order(path, order_info)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = "Change"
    # Don't show Target Tenant port/VLAN
    [:nni_z, :vlan_z].each {|key| @order_info.delete(key)}
    # Add the old work_order_id
    @order_info[:old_work_order_id] = path.member_handle_instance_by_owner(ServiceProvider.system_owner).value
    set_nni_info(order_info)
    @coresite_contact = coresite_contact
    @tenant_contacts = tenant_contacts(order_info[:tenant_a], order_info[:sitename_a])
    to = @tenant_contacts.collect {|tenant| tenant.e_mail} 
    mail(:to => to,
      :subject => "#{@@server}: #{placed_subject} with CoreSite",
      :template_name => "buyer_order_placed")
  end
  
  def seller_change_order(path, order_info)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = "Change"
    # Don't show Buyer Tenant port/VLAN
    [:nni_a, :vlan_a].each {|key| @order_info.delete(key)}
    set_nni_info(order_info)
    @coresite_contact = coresite_contact
    @tenant_contacts = tenant_contacts(order_info[:tenant_z], order_info[:sitename_z])
    to = @tenant_contacts.collect {|tenant| tenant.e_mail}
    mail(:to => to,
      :subject => "#{@@server}: #{placed_subject} with CoreSite",
      :template_name => "seller_order_placed")
  end 
   

  def coresite_change_order_accepted(path, order_info)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = "Change"
    # Add the work_order_id
    @order_info[:work_order_id] = path.member_handle_instance_by_owner(ServiceProvider.system_owner).value
    set_coresite_info(@order_info)
    @coresite_contact = coresite_contact
    mail(:to => @coresite_contact.e_mail,
      :subject => "#{@@server}: #{accepted_subject}",
      :template_name => "coresite_order_accepted")      
  end

  def buyer_change_order_accepted(path, order_info)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = "Change"
    # Don't show Target Tenant port/VLAN
    [:nni_z, :vlan_z].each {|key| @order_info.delete(key)}
    # Add the work_order_id
    @order_info[:work_order_id] = path.member_handle_instance_by_owner(ServiceProvider.system_owner).value
    set_nni_info(order_info)
    @coresite_contact = coresite_contact
    @tenant_contacts = tenant_contacts(order_info[:tenant_a], order_info[:sitename_a])
    to = @tenant_contacts.collect {|tenant| tenant.e_mail}
    mail(:to => to,
      :subject => "#{@@server}: #{accepted_subject}",
      :template_name => "buyer_order_accepted")
  end
  
  def seller_change_order_accepted(path, order_info)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = "Change"
    # Don't show Buyer Tenant port/VLAN
    [:nni_a, :vlan_a].each {|key| @order_info.delete(key)}
    set_nni_info(order_info)
    @coresite_contact = coresite_contact
    @tenant_contacts = tenant_contacts(order_info[:tenant_z], order_info[:sitename_z])
    to = @tenant_contacts.collect {|tenant| tenant.e_mail}
    mail(:to => to,
      :subject => "#{@@server}: #{accepted_subject}",
      :template_name => "seller_order_accepted")
  end


  # Disconnect Order emails
  def coresite_disconnect_order(path, order_info)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = "Disconnect"
    # Add the old work_order_id
    @order_info[:old_work_order_id] = path.member_handle_instance_by_owner(ServiceProvider.system_owner).value
    set_coresite_info(@order_info)
    @coresite_contact = coresite_contact
    mail(:to => @coresite_contact.e_mail,
      :subject => "#{@@server}: #{placed_subject}",
      :template_name => "coresite_order_placed")      
  end

  def buyer_disconnect_order(path, order_info)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = "Disconnect"
    # Don't show Target Tenant port/VLAN
    [:nni_z, :vlan_z].each {|key| @order_info.delete(key)}
    # Add the old work_order_id
    @order_info[:old_work_order_id] = path.member_handle_instance_by_owner(ServiceProvider.system_owner).value
    set_nni_info(order_info)
    @coresite_contact = coresite_contact
    @tenant_contacts = tenant_contacts(order_info[:tenant_a], order_info[:sitename_a])
    to = @tenant_contacts.collect {|tenant| tenant.e_mail}
    mail(:to => to,
      :subject => "#{@@server}: #{placed_subject} with CoreSite",
      :template_name => "buyer_order_placed")
  end
  
  def seller_disconnect_order(path, order_info)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = "Disconnect"
    # Don't show Buyer Tenant port/VLAN
    [:nni_a, :vlan_a].each {|key| @order_info.delete(key)}
    set_nni_info(order_info)
    @coresite_contact = coresite_contact
    @tenant_contacts = tenant_contacts(order_info[:tenant_z], order_info[:sitename_z])
    to = @tenant_contacts.collect {|tenant| tenant.e_mail}
    mail(:to => to,
      :subject => "#{@@server}: #{placed_subject} with CoreSite",
      :template_name => "seller_order_placed")
  end 
   

  def coresite_disconnect_order_accepted(path, order_info)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = "Disconnect"
    set_coresite_info(@order_info)
    # Add the work_order_id
    @order_info[:work_order_id] = path.member_handle_instance_by_owner(ServiceProvider.system_owner).value
    @coresite_contact = coresite_contact
    mail(:to => @coresite_contact.e_mail,
      :subject => "#{@@server}: #{accepted_subject}",
      :template_name => "coresite_order_accepted")
  end

  def buyer_disconnect_order_accepted(path, order_info)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = "Disconnect"
    # Don't show Target Tenant port/VLAN
    [:nni_z, :vlan_z].each {|key| @order_info.delete(key)}
    # Add the work_order_id
    @order_info[:work_order_id] = path.member_handle_instance_by_owner(ServiceProvider.system_owner).value
    set_nni_info(order_info)
    @coresite_contact = coresite_contact
    @tenant_contacts = tenant_contacts(order_info[:tenant_a], order_info[:sitename_a])
    to = @tenant_contacts.collect {|tenant| tenant.e_mail}
    mail(:to => to,
      :subject => "#{@@server}: #{accepted_subject}",
      :template_name => "buyer_order_accepted")
  end
  
  def seller_disconnect_order_accepted(path, order_info)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = "Disconnect"
    # Don't show Buyer Tenant port/VLAN
    [:nni_a, :vlan_a].each {|key| @order_info.delete(key)}
    set_nni_info(order_info)
    @coresite_contact = coresite_contact
    @tenant_contacts = tenant_contacts(order_info[:tenant_z], order_info[:sitename_z])
    to = @tenant_contacts.collect {|tenant| tenant.e_mail}
    mail(:to => to,
      :subject => "#{@@server}: #{accepted_subject}",
      :template_name => "seller_order_accepted")
  end


  # Coresite ENNI emails
  def coresite_enni_create(enni, order_info)
    ec = EmailConfig.instance
    @enni = enni
    @port_name = enni.port_name
    @order_info = order_info
    @order_type = "New"
    @coresite_contact = coresite_contact
    mail(:to => @coresite_contact.e_mail,
      :subject => "#{@@server}: #{ENNI_NEW_SUBJECT}",
      :template_name => "coresite_enni")
  end
  
  def coresite_enni_disconnect(enni, order_info, port_name)
    ec = EmailConfig.instance
    @enni = enni
    @port_name = port_name
    @order_info = order_info
    @order_type = "Disconnect"
    @coresite_contact = coresite_contact
    mail(:to => @coresite_contact.e_mail,
      :subject => "#{@@server}: #{ENNI_DELETE_SUBJECT}",
      :template_name => "coresite_enni")
  end

  # Order Rejected/Cancelled emails
  def coresite_order_aborted(path,order_info, action)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = path.on_net_ovcs.first.get_latest_order.action
    @order_abort_reason = action
    set_coresite_info(@order_info)
    # There is no work order id
    @order_info[:work_order_id] = nil 
    @coresite_contact = coresite_contact
    mail(:to => @coresite_contact.e_mail,
      :subject => "#{@@server}: #{abort_subject}",
      :template_name => "coresite_order_aborted")
  end
  
  def buyer_order_aborted(path,order_info, action)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = path.on_net_ovcs.first.get_latest_order.action
    @order_abort_reason = action
    # Don't show Target Tenant port/VLAN
    [:nni_z, :vlan_z].each {|key| @order_info.delete(key)}
    # There is no work order id
    @order_info[:work_order_id] = nil
    set_nni_info(order_info)
    @coresite_contact = coresite_contact
    @tenant_contacts = tenant_contacts(order_info[:tenant_a], order_info[:sitename_a])
    to = @tenant_contacts.collect {|tenant| tenant.e_mail}
    Rails.logger.debug @order_info
    mail(:to => to,
      :subject => "#{@@server}: #{abort_subject}",
      :template_name => "buyer_order_aborted")
  end
  
  def seller_order_aborted(path,order_info, action)
    ec = EmailConfig.instance
    @path = path
    @order_info = order_info.clone
    @order_type = path.on_net_ovcs.first.get_latest_order.action
    @order_abort_reason = action
    # Don't show Buyer Tenant port/VLAN
    [:nni_a, :vlan_a].each {|key| @order_info.delete(key)}
    set_nni_info(order_info)
    @coresite_contact = coresite_contact
    @tenant_contacts = tenant_contacts(order_info[:tenant_z], order_info[:sitename_z])
    to = @tenant_contacts.collect {|tenant| tenant.e_mail}
    mail(:to => to,
      :subject => "#{@@server}: #{abort_subject}",
      :template_name => "seller_order_aborted")
  end

  # Coresite failure notification
  def coresite_failure(build_log)
    ec = EmailConfig.instance
    @build_log = build_log
    @coresite_contact = coresite_contact
    mail(:to => @coresite_contact.e_mail, :subject => "#{@@server}: #{FAILURE_SUBJECT}")
  end
  
  private
  
  def placed_subject
    "Open Cloud Exchange #{@order_type} Order has been Placed"
  end
  
  def accepted_subject
    "Open Cloud Exchange #{@order_type} Order #{@order_info[:work_order_id].present? ? "(#{@order_info[:work_order_id]})" : ""} has been Accepted"
  end
  
  def abort_subject
    "Open Cloud Exchange #{@order_type} Order #{@order_info[:work_order_id].present? ? "(#{@order_info[:work_order_id]})" : ""} has been #{@order_abort_reason}"
  end
  
  def set_coresite_info(order_info)
    tenant_z = OperatorNetwork.find_by_name("#{order_info[:tenant_z]} [#{order_info[:sitename_z]}]")
    tenant_a = OperatorNetwork.find_by_name("#{order_info[:tenant_a]} [#{order_info[:sitename_a]}]")
    @buyer_mh_evc = @path.member_handle_instance_by_owner(tenant_a.service_provider).value  
    set_nni_info(order_info)
  end
  
  def set_nni_info(order_info)
    tenant_z = OperatorNetwork.find_by_name("#{order_info[:tenant_z]} [#{order_info[:sitename_z]}]")
    tenant_a = OperatorNetwork.find_by_name("#{order_info[:tenant_a]} [#{order_info[:sitename_a]}]")
    if order_info[:nni_a].present?
      buyer_member_attr = MemberAttrInstance.find_by_affected_entity_type_and_value_and_owner_type_and_owner_id('Demarc', order_info[:nni_a], ServiceProvider, tenant_a.service_provider.id)
      @buyer_nni = buyer_member_attr.affected_entity
      @buyer_mh_nni = @buyer_nni.member_handle_instance_by_owner(tenant_a.service_provider).value
      @buyer_coresite_mh_nni = @buyer_nni.member_handle_instance_by_owner(ServiceProvider.system_owner).value  
    end
    if order_info[:nni_z].present?
      seller_member_attr = MemberAttrInstance.find_by_affected_entity_type_and_value_and_owner_type_and_owner_id('Demarc', order_info[:nni_z], ServiceProvider, tenant_z.service_provider.id)
      @seller_nni = seller_member_attr.affected_entity
      @seller_mh_nni = @seller_nni.member_handle_instance_by_owner(tenant_z.service_provider).value  
      @seller_coresite_mh_nni = @seller_nni.member_handle_instance_by_owner(ServiceProvider.system_owner).value  
    end
  end
  
  def coresite_contact
    sp = ServiceProvider.system_owner
    if sp
      contact = sp.contacts.coresite.first
      raise("Can't find Coresite contact") if contact.nil?
    else
      raise("Failed to find CoreSite Service Provider")
    end
    return contact
  end
  
  def tenant_contacts(tenant, site)
    contacts = []
    on = OperatorNetwork.find_by_name(tenant + " [#{site}]")
    if on && on.service_provider
      sp = on.service_provider
      contacts = sp.contacts.where("name like ? and position in (?)", "%[#{site}]%", ["Primary", "Secondary"])
      raise("Can't find Tenant contacts (#{tenant})") if contacts.blank?
    else
      raise("Failed to find Tenant #{tenant}")
    end
    return contacts
  end
  
  # Needed to render the views to get class names
  def self.parent_type_from_table(table)
    CDBInfo::CLASS_SHORT_NAMES[table.is_a?(ActiveRecord::Base) ? table.class_name : table.to_s]
  end
  
end
