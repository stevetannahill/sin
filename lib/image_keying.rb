module ImageKeying
  require 'digest/md5'
  # require "#{Rails.root}/lib/cdb/id_tools/cenx_id_generator.rb"
  SECRET_IMAGE_KEY = "34jo23h4089853hl2bsafh3l2jh2adf32lh2lkj454336lkajl23j366632;1321jlj598halh2ph89h2lkj4982"
  
  def self.key(service_provider)
    if !service_provider.logo_url && !service_provider.website.blank?
      service_provider.logo_url = self.name(service_provider.website)
      service_provider.save!
    end
    
    service_provider.logo_url
  end
  
  def self.name(website)
    Digest::MD5.hexdigest(website+SECRET_IMAGE_KEY)
  end
end