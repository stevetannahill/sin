class Time
  def duration_string
    difference = to_i
    days = (difference/(3600*24)).to_i
    hours = ((difference%(3600*24))/3600).to_i
    mins = ((difference%(3600))/60).to_i
    secs = (difference%60).to_i

    str = "#{days} "
    str += days > 1 || days == 0 ? "days" : "day"
    str += " #{ '%02d' % hours}:#{'%02d' % mins}:#{'%02d' % secs} hours"
    
    str
  end
end