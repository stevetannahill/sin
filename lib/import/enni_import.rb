require File.dirname(__FILE__) + "/common_import"

def process_enni_data section_data

  @raw_item_count += section_data.size

  ep = EnniProfile.new
  ep.offered_services_study_id = @oss.id

  section_title = section_data[0]
  ep.name = section_title

  #Control flags
  current_table_pointer = 0
  next_table_pointer = 0
  table_data = []


  result_ok, next_table_pointer = find_next_table section_data, 1

  while result_ok
    current_table_pointer = next_table_pointer
    result_ok, next_table_pointer = find_next_table section_data, current_table_pointer+1
    if result_ok
      print_info "\n<------------------------------------>"
      #print_info "==== Next Table [#{current_table_pointer}..#{next_table_pointer-1}]"
      table_data = section_data[current_table_pointer..next_table_pointer-1]
      result_ok = process_enni_table_data table_data, ep
    end
  end

  save_to_database "ENNI Profile", ep
  @db_row_count += 1

end


def process_enni_table_data table_data, ep
  result_ok = true
  @table_count += 1
  @item_count += table_data.size
  
  table_title = table_data[0]
  print_info "Process Title: #{table_title}; Size: #{table_data.size} ..."

  case table_title
    when "<Physical Layer>"
      process_enni_physical_layer table_data, ep
    when "<Frame Format>"
      process_enni_frame_format table_data, ep
    when "<Number of Links / Protection>"
      process_enni_protection table_data, ep
    when "<ENNI Maximum Transmission Unit Size>"
      process_enni_mtu table_data, ep
    when "<Maximum Number of OVCs>"
      process_enni_max_ovcs table_data, ep
    when "<ENNI OAM Characteristics>"
      process_enni_oam_characteristics table_data, ep
    when "<Layer 2 Control Protocol Handling Un-tagged>"
      process_enni_l2cp_handling_untagged table_data, ep
    when "<Layer 2 Control Protocol Handling Tagged>"
      process_enni_l2cp_handling_tagged table_data, ep
    when "<Connected Device Type>"
      process_enni_connected_device table_data, ep
    else
      table_processing_error table_title, table_data.size
      result_ok = false
  end


  return result_ok
end

# class TableRow < Struct.new(:type, :name, :value); end

def process_enni_physical_layer table_data, ep

  print_array_data table_data

  #Sanity check
  expected_size = 12
  if( table_data.size != expected_size)
    print_error "Table: #{table_data[0]} is wrong size: #{table_data.size} expected #{expected_size}"
  end

  rows = [
    TableRow.new("table_title", "xxx", table_data[0]),
    TableRow.new("string", 'phy_type', table_data[1]),
    TableRow.new("append_string", 'phy_type', table_data[2]),
    TableRow.new("append_string", 'phy_type', table_data[3]),
    TableRow.new("append_string", 'phy_type', table_data[4]),
    TableRow.new("string", 'phy_layer_notes', table_data[5]),
    TableRow.new("string", 'tx_power_lo', table_data[6]),
    TableRow.new("string", 'tx_power_hi', table_data[7]),
    TableRow.new("string", 'rx_power_lo', table_data[8]),
    TableRow.new("string", 'rx_power_hi', table_data[9]),
    TableRow.new("boolean", 'auto_negotiate', table_data[10]),
    TableRow.new("string", 'additional_phy_notes', table_data[11])
    ]

   rows.each do |r| process_row r, ep end
   
end

def process_enni_frame_format table_data, ep

  print_array_data table_data

  #Sanity check
  expected_size = 6
  if( table_data.size != expected_size)
    print_error "Table: #{table_data[0]} is wrong size: #{table_data.size} expected #{expected_size}"
  end

  rows = [
    TableRow.new("table_title", "xxx", table_data[0]),
    TableRow.new("string", 'ether_type', table_data[1]),
    TableRow.new("string", 'other_ether_type_notes', table_data[2]),
    TableRow.new("boolean", 'consistent_ethertype', table_data[3]),
    TableRow.new("boolean", 'outer_tag_ovc_mapping', table_data[4]),
    TableRow.new("string", 'frame_format_notes', table_data[5])
    ]

  rows.each do |r| process_row r, ep end

end

def process_enni_protection table_data, ep
  print_array_data table_data

  #Sanity check
  expected_size = 8
  if( table_data.size != expected_size)
    print_error "Table: #{table_data[0]} is wrong size: #{table_data.size} expected #{expected_size}"
  end

  rows = [
    TableRow.new("table_title", "xxx", table_data[0]),
    TableRow.new("boolean", 'lag_supported', table_data[1]),
    TableRow.new("string", 'lag_type', table_data[2]),
    TableRow.new("boolean", 'multi_link_support', table_data[3]),
    TableRow.new("string", 'max_links_supported', table_data[4]),
    TableRow.new("boolean", 'lacp_supported', table_data[5]),
    TableRow.new("boolean", 'lacp_priority_support', table_data[6]),
    TableRow.new("string", 'protection_notes', table_data[7])
  ]

  rows.each do |r| process_row r, ep end

end

def process_enni_mtu table_data, ep
  print_array_data table_data

  #Sanity check
  expected_size = 3
  if( table_data.size != expected_size)
    print_error "Table: #{table_data[0]} is wrong size: #{table_data.size} expected #{expected_size}"
  end

  rows = [
      TableRow.new("table_title", "xxx", table_data[0]),
      TableRow.new("integer", 'mtu', table_data[1]),
      TableRow.new("string", 'mtu_notes', table_data[2])
    ]

  rows.each do |r| process_row r, ep end

end

def process_enni_max_ovcs table_data, ep
  print_array_data table_data

  #Sanity check
  expected_size = 3
  if( table_data.size != expected_size)
    print_error "Table: #{table_data[0]} is wrong size: #{table_data.size} expected #{expected_size}"
  end

  rows = [
      TableRow.new("table_title", "xxx", table_data[0]),
      TableRow.new("integer", 'max_num_ovcs', table_data[1]),
      TableRow.new("string", 'max_num_ovcs_notes', table_data[2])
    ]

  rows.each do |r| process_row r, ep end

end

def process_enni_oam_characteristics table_data, ep
  print_array_data table_data

  #Sanity check
  expected_size = 4
  if( table_data.size != expected_size)
    print_error "Table: #{table_data[0]} is wrong size: #{table_data.size} expected #{expected_size}"
  end

  rows = [
    TableRow.new("table_title", "xxx", table_data[0]),
    TableRow.new("boolean", 'ah_supported', table_data[1]),
    TableRow.new("boolean", 'ag_supported', table_data[2]),
    TableRow.new("string", 'oam_notes', table_data[3])
  ]

  rows.each do |r| process_row r, ep end
end

def process_enni_l2cp_handling_untagged table_data, ep
  print_array_data table_data

  #Sanity check
  expected_size = 12
  if( table_data.size != expected_size)
    print_error "Table: #{table_data[0]} is wrong size: #{table_data.size} expected #{expected_size}"
  end

  rows = [
    TableRow.new("table_title", "xxx", table_data[0]),
    TableRow.new("string", 'stp', table_data[1]),
    TableRow.new("string", 'rstp', table_data[1]),
    TableRow.new("string", 'mstp', table_data[1]),
    TableRow.new("string", 'pause', table_data[2]),
    TableRow.new("string", 'lacp', table_data[3]),
    TableRow.new("string", 'lamp', table_data[3]),
    TableRow.new("string", 'link_oam', table_data[4]),
    TableRow.new("string", 'port_auth', table_data[5]),
    TableRow.new("string", 'e_lmi', table_data[6]),
    TableRow.new("string", 'lldp', table_data[7]),
    TableRow.new("string", 'garp', table_data[8]),
    TableRow.new("string", 'mrp_b', table_data[8]),
    TableRow.new("string", 'cisco_bpdu', table_data[9]),
    TableRow.new("string", 'default_l2cp', table_data[10]),

    TableRow.new("string", 'lacp_notes', table_data[11])
  ]
 
  rows.each do |r| process_row r, ep end
end

def process_enni_l2cp_handling_tagged table_data, ep
  print_array_data table_data

  #Sanity check
  expected_size = 12
  if( table_data.size != expected_size)
    print_error "Table: #{table_data[0]} is wrong size: #{table_data.size} expected #{expected_size}"
  end

  rows = [
    TableRow.new("table_title", "xxx", table_data[0]),
    TableRow.new("string", 'stp_tagged', table_data[1]),
    TableRow.new("string", 'rstp_tagged', table_data[1]),
    TableRow.new("string", 'mstp_tagged', table_data[1]),
    TableRow.new("string", 'pause_tagged', table_data[2]),
    TableRow.new("string", 'lacp_tagged', table_data[3]),
    TableRow.new("string", 'lamp_tagged', table_data[3]),
    TableRow.new("string", 'link_oam_tagged', table_data[4]),
    TableRow.new("string", 'port_auth_tagged', table_data[5]),
    TableRow.new("string", 'e_lmi_tagged', table_data[6]),
    TableRow.new("string", 'lldp_tagged', table_data[7]),
    TableRow.new("string", 'garp_tagged', table_data[8]),
    TableRow.new("string", 'mrp_b_tagged', table_data[8]),
    TableRow.new("string", 'cisco_bpdu_tagged', table_data[9]),
    TableRow.new("string", 'default_l2cp_tagged', table_data[10]),

    TableRow.new("string", 'lacp_notes_tagged', table_data[11])
  ]

  rows.each do |r| process_row r, ep end

end

def process_enni_connected_device table_data, ep
  print_array_data table_data

  #Sanity check
  expected_size = 3
  if( table_data.size != expected_size)
    print_error "Table: #{table_data[0]} is wrong size: #{table_data.size} expected #{expected_size}"
  end

  rows = [
  TableRow.new("table_title", "xxx", table_data[0]),
  TableRow.new("string", 'connected_device_type', table_data[1]),
  TableRow.new("string", 'notes', table_data[2])
  ]

  rows.each do |r| process_row r, ep end
  
end
