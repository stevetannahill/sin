require "rubygems"
require "active_record"
require 'optparse'
require 'optparse/time'
require 'ostruct'
require 'pp'

require File.dirname(__FILE__) + "/../../lib/name_tools/cenx_name_generator.rb"
require File.dirname(__FILE__) + "/../../app/models/service_provider"
require File.dirname(__FILE__) + "/../../app/models/operator_network"
require File.dirname(__FILE__) + "/../../app/models/offered_services_study"
require File.dirname(__FILE__) + "/../../app/models/enni_profile"
require File.dirname(__FILE__) + "/../../app/models/service_type"
require File.dirname(__FILE__) + "/../../app/models/class_of_service"
require File.dirname(__FILE__) + "/../../app/models/service_level_guarantee"
require File.dirname(__FILE__) + "/../../app/models/bandwidth_profile"
require File.dirname(__FILE__) + "/../../app/models/uni_profile"
require File.dirname(__FILE__) + "/header_import"
require File.dirname(__FILE__) + "/enni_import"
require File.dirname(__FILE__) + "/service_import"
require File.dirname(__FILE__) + "/common_import"

if RUBY_PLATFORM =~ /java/ #manual change req'd in database.yml:   adapter: <jdbc>mysql
  # puts"...Java..."
  require "jdbc/mysql"
else
  # puts"...No Java..."
end

def process_section_data section_data
  section_title = section_data[0]
  result_ok = true
  @section_count += 1

  print_info "Processing Section #{section_title}; Size: #{section_data.size}..."

  case section_title
    when "[Completed by/for]"
      process_header_data section_data
    when "[ENNI Service Attributes]"
      process_enni_data section_data
      #result_ok = false
    when "[Seller Service Attributes]"
      process_service_data section_data, "Seller"
    when "[Buyer Service Attributes]"
      process_service_data section_data, "Buyer"
    else
      print_error "Section #{section_title} not recognized..."
      result_ok = false
      @section_count -= 1
  end

  return result_ok
  #process_any_section

end

def parse_options args
  opts = OptionParser.new do |opts|
    opts.banner = "Usage: #{File.basename(__FILE__)} [options]"

    opts.separator ""
    opts.separator "Specific options:"

    # Mandatory argument.
    opts.on("-f", "--file FILE",
            "Need FILE to parse") do |file|
      @options.file_to_parse << file
    end

    # Optional argument; Member Name
    opts.on("-m", "--member [MEMBER_NAME]",
            "Name of Member (Service Provider) OSS applies to") do |m_name|
      @options.member_name = m_name
    end

    opts.on("-n", "--network [NETWORK_NAME]",
            "Name of Network OSS applies to") do |n_name|
      @options.network_name = n_name
    end

    # Optional argument with keyword completion.
    opts.on("--db [DB]", ["production", "development", "test"],
            "Select Data Base (production, development, test)") do |db|
      @options.db_type = db
    end

    # Boolean switch.
    opts.on("-v", "--[no-]verbose", "Run verbosely") do |v|
      @options.verbose = v
    end

    opts.separator ""
    opts.separator "Common options:"

    # No argument, shows at tail.  This will print an options summary.
    # Try it and see!
    opts.on_tail("-h", "--help", "Show this message") do
      puts opts
      pp @options
      exit
    end
  end

  opts.parse!(args)
  
end

def get_member_name
  sps = ServiceProvider.find_rows_for_index
  got_selection = false
  i = 0

  puts "\nWhich Member does the Offered Services Study apply to ?"
  while !got_selection
    i = 0
    sps.each do |sp|
      puts "[#{i}] #{sp.name}"
      i += 1
    end
    puts "Choose [0 - #{i-1}]:"

    choice = gets.chomp
    if choice.to_i < sps.size
      got_selection = true
    else
       print_error "Invalid Choice"
    end
  end

  @options.member_name = sps[choice.to_i].name
  puts "Member is: #{@options.member_name}"

end

def get_network_name
  ons = @sp.operator_networks
  got_selection = false
  i = 0

  puts "\nWhich Network owned by #{@sp.name} does the Offered Services Study apply to ?"
  while !got_selection
    i = 0
    ons.each do |on|
      puts "[#{i}] #{on.name}"
      i += 1
    end
    puts "Choose [0 - #{i-1}]:"

    choice = gets.chomp
    if choice.to_i < ons.size
      got_selection = true
    else
       print_error "Invalid Choice"
    end
  end

  @options.network_name = ons[choice.to_i].name
  puts "Network is: #{@options.network_name}\n\n"

end

#------------------------ Main -----------------------------#
#

# Glogal
@need_to_clone_buyer_data_to_seller = false

# Parse command line options
@options = OpenStruct.new
@options.file_to_parse = ""
@options.member_name = ""
@options.network_name = ""
@options.db_type = "development"
@options.verbose = false

parse_options ARGV
puts "\nOptions Used:\n #{@options}\n"

# Debug stats
@section_count = 0
@table_count = 0
@item_count = 0
@raw_item_count = 0
@db_row_count = 0
@db_save_count = 0
@db_save_attempt = 0
@db_save_failures = ""
@warning_count = 0
@all_warnings = ""
@error_count = 0
@all_errors = ""

section_data = []
current_section_pointer = 0
next_section_pointer = 0

result_ok = false

#Establish connection to database
if RUBY_PLATFORM =~ /java/
  dbconfig = YAML::load(File.open(File.dirname(__FILE__) + '/../../config/databasej.yml'))
else
  dbconfig = YAML::load(File.open(File.dirname(__FILE__) + '/../../config/database.yml'))
end

#dbconfig = YAML::load(File.open(File.dirname(__FILE__) + '/../../config/database.yml'))
ActiveRecord::Base.establish_connection(dbconfig[@options.db_type])

if @options.file_to_parse == ""
  print_error "Please specify filename to process"
  exit(1)
end

file = File.new(@options.file_to_parse, 'r')
file_contents = file.gets

raw_data = file_contents.split(",")
@items = strip_quotes_from raw_data

puts "\nFile: #{@options.file_to_parse} has #{@items.size} pieces of data to extract..."

if @options.db_type != "development"
  puts "This will affect the #{@options.db_type} database - are you sure?"
  choice = gets.chomp

  if( !yn_to_bool choice)
    puts "Aborting...."
    return
  end
 
end

if @options.member_name == ""
  get_member_name
end

@sp = ServiceProvider.find_by_name("#{@options.member_name}")
unless @sp
  print_error"Failed to find Member: #{@options.member_name}"
  exit(1)
end

if @options.network_name == ""
  get_network_name
end

@on = OperatorNetwork.find(:first, :conditions => "name = '#{@options.network_name}' and service_provider_id = '#{@sp.id}'")
unless @on
  print_error "Failed to find Network: #{@options.network_name}"
  exit(1)
end

@oss = nil

result_ok, next_section_pointer = find_next_section @items, 0

while result_ok 
  current_section_pointer = next_section_pointer
  result_ok, next_section_pointer = find_next_section @items, current_section_pointer+1
  if result_ok
    print_info "\n[====================================]"
    print_debug "==== Next Section Data [#{current_section_pointer}..#{next_section_pointer-1}]"
    section_data = @items[current_section_pointer..next_section_pointer-1]
    result_ok = process_section_data section_data
  end
end

puts "\n-----------------------------"
puts "Parse Summary:"
puts "-----------------------------"
puts "Sections Processed: #{@section_count}"
puts "Tables Processed: #{@table_count}"
puts "Items Processed: #{@item_count}/#{@raw_item_count}"
puts "Total Items: #{@items.size}"
puts "DB Rows added: #{@db_row_count}"
puts "DB Saves: #{@db_save_count}/#{@db_save_attempt}"
puts "DB Save Failures: #{@db_save_failures}"
puts "Warnings: #{@warning_count}"
if ( @warning_count > 0 ) 
  puts "#{@all_warnings}"
end
puts "Errors: #{@error_count}"
if ( @error_count > 0 ) 
  puts "#{@all_errors}"
end
puts "............................."
puts "Options Used:\n #{@options}\n"
puts "-----------------------------"


