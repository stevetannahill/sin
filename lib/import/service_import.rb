require File.dirname(__FILE__) + "/common_import"
class RangeRow < Struct.new(:number, :lo, :hi); end

class ServiceTypeRow < Struct.new(:number, :object); end
class ClassOfServiceRow < Struct.new(:number, :st_number, :object); end
class UniProfileRow < Struct.new(:st_number, :object); end
class SlgRow < Struct.new(:st_number, :cos_number, :object); end
class BwpRow < Struct.new(:st_number, :cos_number, :object); end

def process_service_data section_data, role
  
  # Global Data initialization
  @num_service_columns = 0
  @num_cos_per_service = 0
  @current_service_type_num = 0
  @table_processing_mode = ""

  @raw_item_count += section_data.size
  
  #section_title = section_data[0]
  if is_non_empty_data section_data[1]

    if section_data[1] == "No Data"
      print_warning "No data to process in section #{section_data[0]}"
      return
    end

    if role == "Buyer"
      if(@need_to_clone_buyer_data_to_seller)
        print_error "ERROR: Asking to clone both buyer and seller data"
        return
      end
      clone_and_save_current_data_to "Buyer"
      return
    else
      print_info "*********"
      print_info "* Cloning Buyer data to #{role} data"
      print_info "*********"
      @need_to_clone_buyer_data_to_seller = true
      return
    end
  else
    @service_types = []
    @classes_of_service = []
    @uni_profiles = []
    @service_level_guarantees = []
    @bandwidth_profiles = []
  end

  #Control flags
  current_table_pointer = 0
  next_table_pointer = 0
  table_data = []

  result_ok, next_table_pointer = find_next_table section_data, 1
  while result_ok
    current_table_pointer = next_table_pointer
    result_ok, next_table_pointer = find_next_table section_data, current_table_pointer+1
    if result_ok
      print_info "\n<------------------------------------>"
      #puts "==== Next Table [#{current_table_pointer}..#{next_table_pointer-1}]"
      table_data = section_data[current_table_pointer..next_table_pointer-1]
      result_ok = process_service_table_data table_data, role
    end
  end

  @db_row_count += @service_types.size
  @db_row_count += @classes_of_service.size
  @db_row_count += @uni_profiles.size
  @db_row_count += @service_level_guarantees.size
  @db_row_count += @bandwidth_profiles.size

  if @need_to_clone_buyer_data_to_seller
    clone_and_save_current_data_to "Seller"
  end

end

def process_service_table_data table_data, role
  result_ok = true
  @table_count += 1
  @item_count += table_data.size

  table_title = table_data[0]
  print_info "Processing Table: #{table_title}; Size: #{table_data.size} ..."
  
  case table_title
    when "<Your Service Types>"
      process_service_types table_data, role
    when "<Service Attributes>"
      process_service_type_attrs table_data
    when "<Service Type CoS>"
      process_service_type_cos table_data
    when "<ENNI CoS Priority Mapping>"
      process_enni_cos_priority table_data
    when "<Service Level Specification>"
      @table_processing_mode = "SERVICE_LEVEL_SPEC"
      skip_over_table table_data
    when "<SERVICE TYPE 1>", "<SERVICE TYPE 2>", "<SERVICE TYPE 3>", "<SERVICE TYPE 4>"
      if( @table_processing_mode == "SERVICE_LEVEL_SPEC" )
        process_service_level_specs table_data
      elsif( @table_processing_mode == "ENNI-INGRESS" ||
             @table_processing_mode == "ENNI-EGRESS" ||
             @table_processing_mode == "UNI-INGRESS" ||
             @table_processing_mode == "UNI-EGRESS")
        process_rates_of_service table_data
      else
        table_processing_error table_title, table_data.size
        print_error "Unknown processing mode: #{@table_processing_mode}"
        result_ok = false
      end
    when "<Guarantees>"
      process_cos_range_guarantees table_data
    when "<General Items>"
      process_general_items table_data
    when "<ENNI INGRESS Rates of Service>"
      @table_processing_mode = "ENNI-INGRESS"
      skip_over_table table_data
    when "<ENNI EGRESS Rates of Service>"
      @table_processing_mode = "ENNI-EGRESS"
      skip_over_table table_data
    when "<UNI Attributes>"
      process_uni_attrs table_data
    when "<UNI Service Attributes>"
      process_uni_service_attrs table_data
    when "<UNI INGRESS Rates of Service>"
      @table_processing_mode = "UNI-INGRESS"
      skip_over_table table_data
    when "<UNI EGRESS Rates of Service>"
      @table_processing_mode = "UNI-EGRESS"
      skip_over_table table_data
    else
      table_processing_error table_title, table_data.size
      result_ok = false
  end
  return result_ok
end

def process_service_types table_data, role

  print_array_data table_data
  @num_service_columns = table_data.size-1

  1.upto(@num_service_columns) do |i|
    if (is_non_empty_data table_data[i])
      st = ServiceType.new
      st.offered_services_study_id = @oss.id
      st.name = "#{table_data[i]}"
      st.role = role
      @service_types << ServiceTypeRow.new( i, st )
    end
  end

  new_oss_name = "#{@oss.name}: #{role}"
  @oss.name = new_oss_name
  save_to_database "Offered Services Study", @oss

  print_info "Service Types Found: #{@service_types.size} out of possible #{@num_service_columns}"

end

def process_service_type_attrs table_data

  last_row_processed = 0
  print_array_data table_data

  @service_types.each do |st|
    print_debug "Fill [#{st.number}; #{st.object.name} with [#{st.number*2-1}] #{table_data[st.number*2-1]}"
    rows = []
    rows << TableRow.new("string", "mef9_cert", table_data[last_row_processed+st.number*2-1])
    rows << TableRow.new("string", "mef14_cert", table_data[last_row_processed+st.number*2])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns*2

  @service_types.each do |st|
    rows = []
    rows << TableRow.new("string", "max_mtu", table_data[last_row_processed+st.number])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns


  @service_types.each do |st|
    rows = []
    rows << TableRow.new("string", "c_vlan_id_preservation", table_data[last_row_processed+st.number])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns
  

  @service_types.each do |st|
    rows = []
    rows << TableRow.new("boolean", "c_vlan_cos_preservation", table_data[last_row_processed+st.number])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns

  @service_types.each do |st|
    rows = []
    rows << TableRow.new("boolean", "cos_marking_at_enni_is_pcp", table_data[last_row_processed+st.number*2-1])
    rows << TableRow.new("string", "cos_marking_at_enni_notes", table_data[last_row_processed+st.number*2])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns*2

  @service_types.each do |st|
    rows = []
    rows << TableRow.new("string", "color_marking", table_data[last_row_processed+st.number*2-1])
    rows << TableRow.new("string", "color_marking_notes", table_data[last_row_processed+st.number*2])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns*2
  
  @service_types.each do |st|
    rows = []
    rows << TableRow.new("string", "service_category", table_data[last_row_processed+st.number])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns

  @service_types.each do |st|
    print_debug "Fill [#{st.number}; #{st.object.name} with [#{st.number*2-1}] #{table_data[st.number*2-1]}"
    rows = []
    rows << TableRow.new("string", "unicast_frame_delivery_conditions", table_data[last_row_processed+st.number*2-1])
    rows << TableRow.new("string", "unicast_frame_delivery_details", table_data[last_row_processed+st.number*2])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns*2

  @service_types.each do |st|
    print_debug "Fill [#{st.number}; #{st.object.name} with [#{st.number*2-1}] #{table_data[st.number*2-1]}"
    rows = []
    rows << TableRow.new("string", "multicast_frame_delivery_conditions", table_data[last_row_processed+st.number*2-1])
    rows << TableRow.new("string", "multicast_frame_delivery_details", table_data[last_row_processed+st.number*2])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns*2

  @service_types.each do |st|
    print_debug "Fill [#{st.number}; #{st.object.name} with [#{st.number*2-1}] #{table_data[st.number*2-1]}"
    rows = []
    rows << TableRow.new("string", "broadcast_frame_delivery_conditions", table_data[last_row_processed+st.number*2-1])
    rows << TableRow.new("string", "broadcast_frame_delivery_details", table_data[last_row_processed+st.number*2])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns*2

  # Sanity check
  if (last_row_processed != table_data.size-1)
    print_error "Service Attributes not properly Processed [#{last_row_processed}/#{table_data.size-1}]"
  else
    print_info "Service Attributes Processed OK - [#{last_row_processed}/#{table_data.size-1}]"
  end

  # Save Service Types into the DB
  @service_types.each do |st|
    save_to_database "Service Type", st.object
  end

end

def process_uni_attrs table_data
  print_array_data table_data

  last_row_processed = 0
  max_number_of_phy_types = 9
  max_number_l2cp_entries = 10
  expected_table_size = @num_service_columns*(max_number_of_phy_types+18)+1

  if table_data.size != expected_table_size
    print_error "Error: Unexpected Table <#{table_data[0]}> size got: #{table_data.size} expect: #{expected_table_size}"
    return
  end

  @service_types.each do |st|
    up = UniProfile.new
    up.service_type_id = st.object.id
    @uni_profiles << UniProfileRow.new(st.number, up )
  end

  # Phy types
  @uni_profiles.each do |up|
    root_index = (up.st_number-1)*max_number_of_phy_types+1
    rows = []
    rows << TableRow.new("string", "phy_type", table_data[root_index])
    1.upto(max_number_of_phy_types-1) do |i|
      if is_non_empty_data table_data[root_index+i]
        rows << TableRow.new("append_string", "phy_type", table_data[root_index+i])
      end
    end
    rows.each do |r| process_row r, up.object end
  end
  last_row_processed += @num_service_columns*max_number_of_phy_types

  #Auto-negotiate
  @uni_profiles.each do |up|
    rows = []
    rows << TableRow.new("boolean", "auto_negotiate", table_data[last_row_processed+up.st_number])
    rows.each do |r| process_row r, up.object end
  end
  last_row_processed += @num_service_columns

  #MTU
  @uni_profiles.each do |up|
    rows = []
    rows << TableRow.new("integer", "mtu", table_data[last_row_processed+up.st_number])
    rows.each do |r| process_row r, up.object end
  end
  last_row_processed += @num_service_columns

  #Multiplexing
  @uni_profiles.each do |up|
    rows = []
    rows << TableRow.new("boolean", "service_multiplexing", table_data[last_row_processed+up.st_number*2-1])
    rows << TableRow.new("integer", "max_num_ovcs", table_data[last_row_processed+up.st_number*2])
    rows.each do |r| process_row r, up.object end
  end
  last_row_processed += @num_service_columns*2

 #Bundling
  @uni_profiles.each do |up|
    rows = []
    rows << TableRow.new("boolean", "bundling", table_data[last_row_processed+up.st_number])
    rows.each do |r| process_row r, up.object end
  end
  last_row_processed += @num_service_columns

  #ATO Bundling
  @uni_profiles.each do |up|
    rows = []
    rows << TableRow.new("boolean", "ato_bundling", table_data[last_row_processed+up.st_number])
    rows.each do |r| process_row r, up.object end
  end
  last_row_processed += @num_service_columns

  #Device Type
  @uni_profiles.each do |up|
    rows = []
    rows << TableRow.new("string", "device_Type", table_data[last_row_processed+up.st_number])
    rows.each do |r| process_row r, up.object end
  end
  last_row_processed += @num_service_columns

  #CFM
  @uni_profiles.each do |up|
    rows = []
    rows << TableRow.new("boolean", "cfm_supported", table_data[last_row_processed+up.st_number])
    rows.each do |r| process_row r, up.object end
  end
  last_row_processed += @num_service_columns

  #L2CP
  @uni_profiles.each do |up|
    rows = []
    root_index = last_row_processed+(up.st_number)
    print_debug "Root: #{root_index}"
    rows << TableRow.new("string", 'stp', table_data[root_index])
    rows << TableRow.new("string", 'rstp', table_data[root_index])
    rows << TableRow.new("string", 'mstp', table_data[root_index])
    rows << TableRow.new("string", 'pause', table_data[root_index+@num_service_columns])
    rows << TableRow.new("string", 'lacp', table_data[root_index+2*@num_service_columns])
    rows << TableRow.new("string", 'lamp', table_data[root_index+2*@num_service_columns])
    rows << TableRow.new("string", 'link_oam', table_data[root_index+3*@num_service_columns])
    rows << TableRow.new("string", 'port_auth', table_data[root_index+4*@num_service_columns])
    rows << TableRow.new("string", 'e_lmi', table_data[root_index+5*@num_service_columns])
    rows << TableRow.new("string", 'lldp', table_data[root_index+6*@num_service_columns])
    rows << TableRow.new("string", 'garp', table_data[root_index+7*@num_service_columns])
    rows << TableRow.new("string", 'mrp_b', table_data[root_index+7*@num_service_columns])
    rows << TableRow.new("string", 'cisco_bpdu', table_data[root_index+8*@num_service_columns])
    rows << TableRow.new("string", 'default_l2cp', table_data[root_index+9*@num_service_columns])
        
    rows.each do |r| process_row r, up.object end
  end

  last_row_processed += @num_service_columns*max_number_l2cp_entries
  print_debug "Last Row: #{last_row_processed}"

  # Sanity check
  if (last_row_processed != table_data.size-1)
    print_error "UNI Attributes not properly Processed [#{last_row_processed}/#{table_data.size-1}]"
  else
    print_info "UNI Attributes Processed OK - [#{last_row_processed}/#{table_data.size-1}]"
  end

  # Save UNI Profiles into the DB
  @uni_profiles.each do |up|
    save_to_database "Uni Profile", up.object
  end

end

def  process_uni_service_attrs table_data
  last_row_processed = 0
  print_array_data table_data

  @service_types.each do |st|
    print_debug "Process UNI Service Attributes for #{st.object.display_name}"
    rows = []
    rows << TableRow.new("string", "cos_marking_uni", table_data[last_row_processed+st.number])
    rows.each do |r| process_row r, st.object end

    save_to_database "Service Type", st.object

  end
  last_row_processed += @num_service_columns

  cos_index = 0
  @service_types.each do |st|
    1.upto(@num_cos_per_service) do |i|
      cos_value_data = table_data[last_row_processed+((st.number-1)*@num_cos_per_service)+i]
      if (is_non_empty_data cos_value_data)
        @classes_of_service[cos_index].object.cos_value_uni = cos_value_data
        cos_index += 1
      end
    end
  end
  
  #sanity check
  if( cos_index != @classes_of_service.size)
    print_error "Processed #{cos_index} COS Values; expecting #{@classes_of_service.size}"
  else
    print_info "Processed #{cos_index} COS Values OK"
  end


  # Save COSs into the DB
  @classes_of_service.each do |cos|
    save_to_database "COS", cos.object
  end

end

def process_service_type_cos table_data
  print_array_data table_data
  @num_cos_per_service = (table_data.size-1)/@num_service_columns

  @service_types.each do |st|
    1.upto(@num_cos_per_service) do |i|
      if (is_non_empty_data table_data[((st.number-1)*@num_cos_per_service)+i])
        cos = ClassOfService.new
        cos.service_type_id = st.object.id
        cos.name = table_data[((st.number-1)*@num_cos_per_service)+i]
        @classes_of_service << ClassOfServiceRow.new( i, st.number, cos )
      end
    end
  end

  print_info "Total COS's found: #{@classes_of_service.size}"
  print_info "Max COS's per Service Type: #{@num_cos_per_service}"

end

def process_enni_cos_priority table_data
  print_array_data table_data

  cos_index = 0
  @service_types.each do |st|
    1.upto(@num_cos_per_service) do |i|
      cos_value_data = table_data[((st.number-1)*@num_cos_per_service)+i]
      if (is_non_empty_data cos_value_data)
        @classes_of_service[cos_index].object.cos_value = cos_value_data
        print_debug "Cos #{@classes_of_service[cos_index].object.name} = #{cos_value_data}"
        cos_index += 1
      end
    end
  end

  #sanity check
  if( cos_index != @classes_of_service.size)
    print_error "ERROR: Processed #{cos_index} COS Values; expecting #{@classes_of_service.size}"
  else
    print_info "Processed #{cos_index} COS Values OK"
  end

  # Save COSs into the DB
  @classes_of_service.each do |cos|
    save_to_database "COS", cos.object
  end
end

def process_service_level_specs table_data
  print_array_data table_data
  number_of_cos_level_specs = 3
  data_items_processed = 0

  @current_service_type_num = get_service_type_number table_data[0]
  print_info "Process COS SLA Guarantees for Service Type: #{@current_service_type_num}"
  current_classes_of_service = get_coss_for_service_type @current_service_type_num, @classes_of_service

  data_items_expected = current_classes_of_service.size*number_of_cos_level_specs

  cos_counter = 0
  current_classes_of_service.each do |cos|
    rows = []
    rows << TableRow.new("string", "availability", table_data[cos_counter*number_of_cos_level_specs+1])
    rows << TableRow.new("string", "frame_loss_ratio", table_data[cos_counter*number_of_cos_level_specs+2])
    rows << TableRow.new("string", "mttr_hrs", table_data[cos_counter*number_of_cos_level_specs+3])

    rows.each do |r| process_row r, cos.object end
    data_items_processed += rows.size
    save_to_database "COS", cos.object

    cos_counter +=1
  end

  #Sanity check
  if( data_items_processed != data_items_expected )
    print_error "Processed #{data_items_processed} COS SLA specs on Servce Type: #{@current_service_type_num}; expecting #{data_items_expected}"
  else
    print_info "Processed #{data_items_processed} SLA specs OK for #{current_classes_of_service.size} COSs on Servce Type: #{@current_service_type_num}"
  end



end

def process_cos_range_guarantees table_data
  print_array_data table_data
  max_number_of_ranges = 6
  delay_data_start = max_number_of_ranges*2 + 1
  dv_data_start = delay_data_start + @num_cos_per_service * max_number_of_ranges
  delay_to_dv_data_offset = dv_data_start - delay_data_start
  ranges = []

  # Process  Ranges
 1.upto(max_number_of_ranges) do |i|
   if is_non_empty_data table_data[i*2-1]
     ranges << RangeRow.new(i, table_data[i*2-1], table_data[i*2])
   end
 end

  print_info "Process Range Guarantees for Service Type:#{@current_service_type_num} - Ranges found: #{ranges.size}"
  print_debug "Delay data start: [#{delay_data_start}]"
  print_debug "DV data start: [#{dv_data_start}]"
  print_debug "D->DV Offset: [#{delay_to_dv_data_offset}]"

  current_classes_of_service = get_coss_for_service_type @current_service_type_num, @classes_of_service

  cos_counter = 0
  current_classes_of_service.each do |cos|
    print_info "Process SLGs for Service Type #{@current_service_type_num} - COS: #{cos.object.name}"
    ranges.each do |range|
      d_d = delay_data_start+cos_counter*max_number_of_ranges+range.number-1
      dv_d = d_d + delay_to_dv_data_offset

      print_debug "SLG: lo: #{range.lo} ; hi: #{range.hi}; delay: #{table_data[d_d]}; dv: #{table_data[dv_d]}"

      slg = ServiceLevelGuarantee.new
      slg.class_of_service_id = cos.object.id
      slg.min_dist_km = range.lo
      slg.max_dist_km = range.hi
      slg.delay_us = table_data[d_d]
      slg.delay_variation_ms = table_data[dv_d]

      @service_level_guarantees << SlgRow.new( cos.st_number, cos.number, slg )
      save_to_database "SLG for #{cos.object.name}", slg

    end
    cos_counter += 1

  end

end

def process_rates_of_service table_data
  print_array_data table_data

  number_of_bwp_params = 6
  
  @current_service_type_num = get_service_type_number table_data[0]
  print_info "Process BWP for Service Type: #{@current_service_type_num}"
  current_classes_of_service = get_coss_for_service_type @current_service_type_num, @classes_of_service

  #Sanity Check
  if table_data.size != @num_cos_per_service*number_of_bwp_params+1
    print_error "Unexpected Table size <#{table_data[0]}> got: #{table_data.size} expect: #{@num_cos_per_service*6+1} - check for , where ; is needed"
    return
  end

  cos_counter = 0
  current_classes_of_service.each do |cos|
    print_info "Process BWP for Service Type #{@current_service_type_num} - COS: #{cos.object.name}"
    cir_d = 1+cos_counter
    cbs_d = cir_d+@num_cos_per_service
    eir_d = cbs_d+@num_cos_per_service
    ebs_d = eir_d+@num_cos_per_service
    cm_d = ebs_d+@num_cos_per_service
    cf_d = cm_d+@num_cos_per_service

    bwp = BandwidthProfile.new
    bwp.class_of_service_id = cos.object.id
    bwp.bwp_type = @table_processing_mode
    bwp.cir_range_notes = table_data[cir_d]
    bwp.cbs_range_notes = table_data[cbs_d]
    bwp.eir_range_notes = table_data[eir_d]
    bwp.ebs_range_notes = table_data[ebs_d]
    bwp.color_mode = table_data[cm_d]
    bwp.coupling_flag = table_data[cf_d]

    @bandwidth_profiles << BwpRow.new( cos.st_number, cos.number, bwp )
    save_to_database "#{bwp.bwp_type} BWP for #{cos.object.name}", bwp

    cos_counter += 1
  end

end

def process_general_items table_data
  print_array_data table_data
  
  @oss.sla_calculation_method = table_data[1]
  @oss.metrics_details = table_data[2]
  @oss.delay_definition = table_data[3]

  save_to_database "Offered Services Study", @oss
end

# Utility functions
#

def get_service_type_number string
  stripped_string = string[1,string.length-2]
  words = stripped_string.split
  return words.last
end

def get_coss_for_service_type st_num, coss_to_search
  coss = []
  coss_to_search.each do |cos|
    if cos.st_number == st_num.to_i
      coss << cos
    end
  end
  print_info "Service Type #{st_num} has #{coss.size} COS's"
  return coss
end

def clone_and_save_current_data_to role
  
  print_info "\n***************************************"
  print_info "* Cloning existing data to #{role} data"
  print_info "***************************************"

  if !@service_types || @service_types.empty?
    print_error "Trying to clone empty data to #{role}"
    return
  end

  new_oss_name = "#{@oss.name} : #{role}"
  @oss.name = new_oss_name
  save_to_database "Offered Services Study", @oss

  new_service_types = []
  new_classes_of_service = []
  new_uni_profiles = []
  new_service_level_guarantees = []
  new_bandwidth_profiles = []
    
  @service_types.each do |st|
    new_st = st.object.clone
    new_st.offered_services_study_id = st.object.offered_services_study_id
    new_st.name = "#{st.object.name}"
    new_st.role = role
    new_service_types << ServiceTypeRow.new( st.number, new_st )
    save_to_database "Service Type", new_st
  end

  @classes_of_service.each do |cos|
    new_cos = cos.object.clone
    new_cos.service_type_id = new_service_types[cos.st_number-1].object.id
    new_classes_of_service << ClassOfServiceRow.new( cos.number, cos.st_number, new_cos )
    save_to_database "COS", new_cos
  end

  @uni_profiles.each do |up|
    new_up = up.object.clone
    new_up.service_type_id = new_service_types[up.st_number-1].object.id
    new_uni_profiles << UniProfileRow.new(up.st_number, new_up)
    save_to_database "Uni Profile", new_up
  end

  new_service_types.each do |st|
    current_classes_of_service = get_coss_for_service_type st.number, new_classes_of_service
    @service_level_guarantees.each do |slg|
      if( slg.st_number == st.number)
        new_slg = slg.object.clone
        new_slg.class_of_service_id = current_classes_of_service[slg.cos_number-1].object.id
        new_service_level_guarantees << SlgRow.new( slg.st_number, slg.cos_number, new_slg )
        save_to_database "SLG for #{current_classes_of_service[slg.cos_number-1].object.name}", new_slg
      end
    end

    @bandwidth_profiles.each do |bwp|
      if( bwp.st_number == st.number)
        new_bwp = bwp.object.clone
        new_bwp.class_of_service_id = current_classes_of_service[bwp.cos_number-1].object.id
        new_bandwidth_profiles << BwpRow.new( bwp.st_number, bwp.cos_number, new_bwp )
        save_to_database "#{new_bwp.bwp_type} BWP for #{current_classes_of_service[bwp.cos_number-1].object.name}", new_bwp
      end

    end


  end

  #Update DB save stats
  @db_row_count += new_service_types.size
  @db_row_count += new_classes_of_service.size
  @db_row_count += new_uni_profiles.size
  @db_row_count += new_service_level_guarantees.size
  @db_row_count += new_bandwidth_profiles.size

end
