require File.dirname(__FILE__) + "/common_import"

def process_header_data section_data
  print_array_data section_data

  @item_count += section_data.size
  @raw_item_count += section_data.size

  #Sanity check
  expected_size = 9
  if( section_data.size != expected_size)
    print_error "Table: #{section_data[0]} is wrong size: #{section_data.size} expected #{expected_size}"
  end

  @oss = OfferedServicesStudy.new
  @oss.operator_network_id = @on.id

  @oss.name = "#{section_data[1]} as "
  @oss.completion_date = "#{section_data[5]}, #{section_data[6]}"
  @oss.reference_documents = section_data[8]

  save_to_database "Offered Services Study", @oss
  @db_row_count += 1

end
