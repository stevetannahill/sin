# Event Server, using DRb
app_base = File.join(File.dirname(__FILE__),"..","..")

require 'drb'
require 'patron'
require 'eventmachine'
require 'logger'
require 'yaml'
require 'json'
require "#{app_base}/lib/event/event"

# Load properties file and setup logger
srv_prop = YAML.load_file("#{app_base}/lib/event/event_server.properties")
logger = Logger.new("#{app_base}/log/#{srv_prop['log_file']}", srv_prop['log_rollover'])
logger.formatter = Logger::Formatter.new
#logger.datetime_format = "%Y-%m-%d %H:%M:%S"
logger.level = case srv_prop['log_level'].downcase
  when "debug" then Logger::DEBUG
  when "info" then Logger::INFO
  when "warn" then Logger::WARN
  when "error" then Logger::ERROR
  when "fatal" then Logger::FATAL
  else Logger::UNKNOWN
end
logger.info("Startup of CENX Event Server...")
logger.debug("Loaded properties are #{srv_prop.inspect}")

# Initialize classes to perform event receipt/transmit
evtq = EventQueue.new(logger)
evtrpt = EventReporter.new(evtq, logger)
evtpub = EventPublisher.new(evtq, logger)

DRb.start_service "druby://#{srv_prop['server_host']}:#{srv_prop['server_port']}", evtrpt
logger.info("Listening for reports on #{DRb.uri}")

finished_queue = false
EM.run do
  node_house_url = srv_prop['node_house_url']
  external_host_name = srv_prop['external_host_name']
  
  session = Patron::Session.new
  session.base_url = node_house_url
  
  logger.info("Ready to publish events to #{node_house_url}") 

  EM.add_periodic_timer(1) do
    if evtq.queue_has_events?
      logger.info("Queue has #{evtq.queue_size} events to be processed.")
      finished_queue = false
      evtpub.publish(session,external_host_name)
    else
      logger.info("Queue is empty.") unless finished_queue
      finished_queue = true
    end
  end
end
logger.close
