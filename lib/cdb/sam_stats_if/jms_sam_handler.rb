class JmsSamHandler < JmsHandler
  
  def initialize(nms, interval = 1.minute, msg_per_interval = 20, rate_sleep = 0.1)
    # load saved sysinfo
    # read and return the SAM configuration    
    sam_cfg = YAML::load(IO.read("#{Rails.root}/config/sam.yml"))[Rails.env] 
    sysinfo_dir = File.expand_path(sam_cfg["sysinfo_dir"], Rails.root)
    if !File.directory?(sysinfo_dir) || !File.writable?(sysinfo_dir)
      err = "sysinfo_dir does not exist or not writable (#{sysinfo_dir}) defaulting to app/log"
      Rails.logger.error err; SW_ERR err
      sysinfo_dir = "#{Rails.root}/log"
    end
    
    @nms = nms
    @sysinfo_file = sysinfo_dir + "/#{nms.name.gsub(/\s/,'_')}_sam_sys_info_file.txt"
    @sysinfo = {}
    @sysinfo = File.open(@sysinfo_file, "rb") {|f| Marshal.load(f)} if File.file?(@sysinfo_file)
    @rate_limiter = {:time => Time.now + interval, :counter => 0}
    @interval = interval
    @msg_per_interval = msg_per_interval
    @rate_sleep = rate_sleep
  end
    
  def on_message(message_text, listener)
    limit_rate

    doc = Hpricot.XML(message_text)   
    header = doc.at("//header")
    if header.at("ALA_category").innerHTML != "GENERAL"
      return
    end

    jms_event = doc.at("//jms")
    if jms_event != nil      
      if jms_event.at("terminateClientSessionEvent") != nil
        client_id = jms_event.at("terminateClientSessionEvent/clientId")
        if (client_id) != nil && (client_id.innerHTML == listener.client_id)
          Rails.logger.info "Received a terminateClientSessionEvent - reconnecting"
          listener.reconnect("Received a terminateClientSessionEvent")
        end
      elsif jms_event.at("stateChangeEvent/eventName") != nil        
        event = jms_event.at("stateChangeEvent/state")
        if event != nil && event.innerHTML == "jmsMissedEvents"
          Rails.logger.info "Resync due to jmsMissedEvents"
          listener.resync
        elsif event != nil && event.innerHTML == "systemInfo"
          # get the info and save it in a file and check if a restart has occured
          new_sysinfo = {}
          ["sysStandbyIp", "sysPrimaryIp", "jmsStartTime", "sysStartTime", "sysType"].each do |attr|
            new_sysinfo[attr] = jms_event.at("stateChangeEvent/#{attr}").innerHTML
          end
          File.open(@sysinfo_file, "wb") {|f| Marshal.dump(new_sysinfo, f)}          
          if new_sysinfo["sysStartTime"] != @sysinfo["sysStartTime"]
            # Do resync
            Rails.logger.info "Resync due to SAM system restart" if @sysinfo.key?("sysStartTime")
            Rails.logger.info "Resync due to no previous SAM system info in #{@sysinfo_file}" if !@sysinfo.key?("sysStartTime")
            listener.resync
          end          
          @sysinfo = new_sysinfo
        end
      end
    end 
  end
  
  private
  def limit_rate
    if Time.now > @rate_limiter[:time]
      @rate_limiter[:time] = Time.now + @interval
      @rate_limiter[:counter] = 0
    end  
    if @rate_limiter[:counter] > @msg_per_interval
      Rails.logger.info "Rate limiting - sleeping for #{@rate_sleep} seconds. #{@msg_per_interval} Msgs per #{@interval}  #{@rate_limiter.inspect}"
      sleep(@rate_sleep)
    end
    @rate_limiter[:counter] += 1
  end
end