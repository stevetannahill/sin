require 'jms_alarm_parse'

class JmsAlarmHandler < JmsHandler
  
  def initialize(database_type)
    @resync_thread = nil
  end    
  
  def on_message(message_text, listener)
    info = JmsAlarmParse.extract_JMS_alarm_info(message_text)
    begin
      AlarmHistory.process_event(info.first) if info.size > 0
    rescue
      err = "Exception handling alarm #{$!.inspect} #{$!.backtrace.join("\n")} Message = #{message_text}"
      Rails.logger.error err; SW_ERR err
    end
  end
  
  def resync(listener)
    # If there are a lot of alarms active (100's of them) this could take some time
    # Start the resync in a thread - If there is already a resync thread and another
    # resync is needed kill the thread and start again    
    Thread.kill(@resync_thread) if @resync_thread != nil && @resync_thread.status.class == String    
    @resync_thread = Thread.start {
      Thread.current["nms_name"] = listener.nms_name
      Rails.logger.info "Alarm resync thread started - status = #{Thread.current.status}"
      begin
        AlarmHistory.alarm_sync_all(listener.nms)
      rescue
        err = "Exception handling alarm resync #{$!.inspect} #{$!.backtrace.join("\n")}"
        Rails.logger.error err; SW_ERR err
      end        
      Rails.logger.info "Alarm resync thread ending - status = #{Thread.current.status}"
    }  
  end
end
