# Main line begins  
ENV["RAILS_ENV"] = ARGV.first

require File.expand_path('../../../config/environment',  __FILE__)

if ARGV.size != 1
  puts "Missing database information (development|production)"
  return
end

class FormatLogger < Logger
  def format_message(severity, timestamp, progname, msg)
    "#{timestamp.to_s} #{severity} #{Thread.current} #{Thread.current["nms_name"]} #{msg}\n"
  end
end

def print_menu
  str = "Debug menu for #{File.basename(__FILE__, '.*')}\n"
  str += "r) Reconnect\n"
  str += "d) Dump debug state\n"
  str += "l [level]) Dump Logger level or set it to level\n" 
  str += "b) Close client session\n"
  str += "t) Thread info\n"
  str += "q) Quit server\n"
  str += "e) Send test email\n"
  return str
end


def debug_handler(command, client, listeners)
  stop_server = false
  stop_debug = false
  reply = ""
  
  split_cmd = command.split(" ")
  case split_cmd[0]
  when "q"
    Rails.logger.info "Shutting down server. Bye!"
    reply = "Closing the connection. Bye!"
    stop_debug = true
    stop_server = true
  when "b"
    Rails.logger.info "Closing client session. Bye!"
    reply = "Closing client session. Bye!"
    stop_debug = true
  when "r"
    reply = "Testing Reconnect!"
    listeners.each do |l| l.test_reconnect end
  when "d"
    Rails.logger.info "Testing Debug Dump!"
    reply = ""
    listeners.each do |l| reply += l.debug_dump end
  when "t"
    Rails.logger.info "Thread Info"
    reply = "Threads = #{Thread.list.inspect} Count #{Thread.list.size}"
  when "l"
    Rails.logger.info "Logging level command"
    if split_cmd.size == 1
      reply = ["DEBUG", "INFO", "WARN", "ERROR", "FATAL", "UNKNOWN"][Rails.logger.level]
    else
      if Logger::Severity.constants.include?(split_cmd[1].upcase)
        Rails.logger.level = Logger::Severity.const_get(split_cmd[1].upcase)
        reply = "Logger level set to #{split_cmd[1].upcase}"
      else
        reply = "#{split_cmd[1]} is an invalid log level"
      end
    end
  when "e"
    email_address = split_cmd[1]    
    begin
      CdbMailer.test_email(email_address).deliver
      reply = "Ok"
    rescue 
      err = "Exception sending test email Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}"
      Rails.logger.error err; SW_ERR err
      reply = "Failed see SW_ERRs"
    end
  else
    reply = print_menu
  end
  
  if !reply.empty?
    begin
      client.puts reply
    rescue Exception => e #Errno::EIO, Errno::ECONNRESET, Errno::ENOTCONN
      SW_ERR "Socket write failure...#{e.message}"
    end   
  end  
  client.close if stop_debug

  return stop_server, stop_debug
end


def initialize_db(database_type)
  info = YAML::load(IO.read("#{Rails.root}/config/database.yml"))
  host =  info[database_type]["host"]
  database = info[database_type]["database"]
  username = info[database_type]["username"]
  password = info[database_type]["password"]

  Rails.logger.info "Database Info #{database_type} #{host} #{database} #{username} "

  ActiveRecord::Base.establish_connection(
  :adapter=> "jdbcmysql",
  :host => host,
  :database=> database,
  :username => username,
  :password => password,
  :reconnect => true)
  
  # Don't log SQL queries
  ActiveRecord::Base.logger = nil  
  ActiveRecord::Base.clear_active_connections!
end

def check_nms_servers(listeners, user, passwd, client, filter, topic_name)
  # remove/add any old/new NMS
  # Remove any network managers that no longer exist 
  all_nms = NetworkManager.find_all_by_nm_type("5620Sam") 
  
  listeners.delete_if {|l| 
    if !l.reload_model(true)
      # Shutdown the JMS listener as it no longer exists
      l.close_connection(true)
      true
    end
  }
  all_nms.each do |nms|
    if listeners.find {|l| l.nms == nms} == nil
      # add a JMS listener
      if nms.network_management_servers.size != 0
        listener = SAMJmsListener.new(nms, "Alarms Listener", user, passwd, client, filter, topic_name)
        listener.add_callback(JmsAlarmHandler.new(Rails.env))
        listener.add_callback(JmsSamHandler.new(nms))
        begin
          listener.open_connection
        rescue java.lang.Throwable
          # do nothing - the listener will found to be insane and a reconnect will be attempted
        end
        listeners << listener
      end
    end
  end
end  

if ARGV.size != 1
  puts "Missing database information (development|production)"
  return
end

# set up logging
MAX_LOG_SIZE = 10*1024*1024
logger = FormatLogger.new("#{Rails.root}/log/#{File.basename(__FILE__, '.*')}.log", 1, MAX_LOG_SIZE)
Rails.logger = logger
logger.level = Logger::Severity::INFO if Rails.env == "production"

# Log the ActionMailer
ActionMailer::Base.logger = Rails.logger

user = SamIfCommon::SAMO_USER
passwd = SamIfCommon::SAMO_PASSWD
client = "cenx_oss@#{Socket.gethostname}"
#filter = "ALA_clientId in ('#{client}', '') AND ( (MTOSI_NTType='ALA_OTHER' OR MTOSI_NTType='NT_ALARM' OR MTOSI_NTType='NT_STATE_CHANGE' OR MTOSI_NTType='NT_ATTRIBUTE_VALUE_CHANGE' OR MTOSI_NTType='NT_HEARTBEAT' OR MTOSI_NTType='ALA_RELATIONSHIPCHANGE' OR MTOSI_NTType='ALA_MANAGEDROUTE'))" #OR MTOSI_objectType in ('KeepAliveEvent', 'StateChangeEvent', 'TerminateClientSession'))"
filter = "ALA_clientId in ('#{client}', '') AND ( (MTOSI_NTType='NT_ALARM') OR MTOSI_objectType in ('KeepAliveEvent', 'StateChangeEvent', 'TerminateClientSession'))"
topic_name = "5620-SAM-topic-xml-fault"
server = TCPServer.open(SamIfCommon::SAM_JMS_SERVER_PORT)   # Socket to listen to for commands

# jruby does not catch the ^C etc via the normal ruby exception so handle the signals explicitly
# We need to clean up the JMS listenter socket on exit otherwise need to wait for
# 10 minutes before a reconnect is possible
# If the graceful exit does not work then on the 2nd signal do an exit
signal_caught = false 
sig_names = ["INT", "HUP", "TERM", "USR1", "USR2"]
sig_names.each {|sig| Signal.trap(sig) do 
    Rails.logger.info "Signal caught #{sig} exiting #{signal_caught}"
    # IO.select cannot be interrupted so send a message which exits the IO.select
    # the process will then exit
    @keep_going = false
    a = TCPSocket.new('localhost', SamIfCommon::SAM_JMS_SERVER_PORT)
    a.close        
    exit if signal_caught # This will cause an real ruby exception which is then handled
    signal_caught = true
  end
}

initialize_db(Rails.env)

listeners = []
#listener1 = SAMJmsListener.new "Alarms Listener", user, passwd, client, filter, topic_name

# add the alarm handler processing
NetworkManager.find_all_by_nm_type("5620Sam").each do |nms|
  if nms.network_management_servers.size != 0
    listener = SAMJmsListener.new(nms, "Alarms Listener", user, passwd, client, filter, topic_name)
    listener.add_callback(JmsAlarmHandler.new(Rails.env))
    listener.add_callback(JmsSamHandler.new(nms))
    begin
      listener.open_connection
    rescue
      # do nothing - In the main loop the listener will found to be insane and a reconnect will be attempted
    end
    listeners << listener
  end
end

begin
  @keep_going = true
  while @keep_going # Servers run forever
    Rails.logger.info "Listening for clients on port #{SamIfCommon::SAM_JMS_SERVER_PORT}"
    socket_list = IO.select([server], nil, nil, 60*5)    

    raise SystemExit if !@keep_going
      
    check_nms_servers(listeners,  user, passwd, client, filter, topic_name)
    # check all listeners to make sure they have received a message since the last time
    listeners.each do |l| 
      if !l.connection_sane
        # reconnect in another thread so main can still process requests
        Thread.new {
          Thread.current["nms_name"] = l.nms_name
          Rails.logger.info "Connection is not sane"
          l.reconnect("Connection is not sane")
          Rails.logger.info "exiting thread #{Thread.current}"
        }
      end
    end
    if socket_list != nil && @keep_going
      socket_list[0].each do |sock|        
        t = Thread.start(sock.accept_nonblock) do |client|      
          stop_debug = false
          while !stop_debug
            Rails.logger.info "Thread: Waiting for client msg...#{Thread.current}"
            begin
              raw_size = client.recv(8)
              if raw_size.empty?
                stop_debug = true
                break
              end
              raw_data = client.recv( 1000 )
              msg = Marshal.load(raw_data)              
            rescue Exception => e #Errno::EIO, Errno::ECONNRESET, Errno::ENOTCONN
              SW_ERR "Socket read failure...#{e.message} #{e.backtrace.join("\n")} #{raw_data}"
              client.close
              stop_debug = true
              break
            end
            command = msg.get_command            
            Rails.logger.info "Got command: #{command}"
            stop_server, stop_debug = debug_handler(command, client, listeners)
            if stop_server
              @keep_going = false
              # kick the IO.select so the main loop wakes up and sees that 
              # keep_going is false so it exits
              a = TCPSocket.new('localhost', SamIfCommon::SAM_JMS_SERVER_PORT)
              a.close
            end
          end
          Rails.logger.info "exiting thread #{Thread.current}"
        end
      end
    end
  end
ensure  
  Rails.logger.info "Server Shutting down port #{SamIfCommon::SAM_JMS_SERVER_PORT} #{listeners.size} Exception = #{$!} #{$!.backtrace.join("\n")}"
  listeners.each {|l| l.close_connection}
  Rails.logger.info "Done"
end
