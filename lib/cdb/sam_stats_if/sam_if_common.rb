module SamIfCommon

  SAM_IF_SERVER_IP = 'localhost'
  SAM_IF_SERVER_PORT = 2000
  SAM_JMS_SERVER_PORT = 2001

  SAMO_USER = "cenx-oss-client"

  # Text Passwd: CENX*oss!
  SAMO_PASSWD = "4b4b89ee9c271b13a43d73c9aaea5e0b"

  class SamIfMsg
    
    attr_reader :get_nms_id, :get_command, :get_filters, :get_result_filters
    
    def initialize(nms_id, command, filters, result_filters)
      @get_nms_id = nms_id
      @get_command = command
      @get_filters = filters
      @get_result_filters = result_filters
    end

  end
  
end
