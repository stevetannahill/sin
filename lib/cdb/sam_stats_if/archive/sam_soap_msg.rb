class SamSoapMsg

  def initialize
    @envlelope_start = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
    <SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">"
    @envlelope_end = "</SOAP:Envelope>"
    @header = ""
    @body = ""
  end

  def set_header uname, passwd, client_name, client_id
    @header = "
    <SOAP:Header> <header xmlns=\"xmlapi_1.0\">
      <security>
        <user>#{uname}</user>
        <password>#{passwd}</password>
      </security>
      <requestID>#{client_name}:#{client_id}</requestID>
    </header></SOAP:Header>"
  end

  def set_body
    @body = "
    <SOAP:Body>
    <findToFile xmlns=\"xmlapi_1.0\">
      <fullClassName>service.CompleteServiceIngressPacketOctets</fullClassName>
	  <filter>
              <between name=\"timeCaptured\" first=\"0\" second=\"9999999999999\"/>
          </filter>
      <fileName>Service.CompleteServiceEgressPacketOctets.historical.xml</fileName>
    </findToFile>
  </SOAP:Body>"
  end

  def to_string
    return "#{@envlelope_start}\n#{@header}\n#{@body}\n#{@envlelope_end}"
  end
end