require 'socket'      # Sockets are in standard library
require File.dirname(__FILE__) + "/sam_api"

api = SamIf::StatsApi.new

while 1
  puts "Enter a command for the server:"
  command = gets.chomp
  result = ""

  if command == 'quit'
    break
  elsif command == 'w'
    result, data = api.get_inventory(
      "vpls.L2AccessInterface",
      {"serviceId" => "100030", "displayedName" => "Lag 199:1030.0"},
      ["objectFullName"]
=begin
      "ethernetoam.Mep",
      {"maintenanceAssociationId" => "100030", "id" => "1"},
      ["maintenanceAssociationId", "objectFullName"]
=end
      )
  elsif command == 'x'
    result, data = api.get_service_stats(
      "100030",
      "service.CompleteServiceIngressPacketOctetsLogRecord",
      [{:start_time => ((Time.now.to_i-600)*1000).to_s, :end_time => ((Time.now.to_i)*1000).to_s}],
      ["sapId", "outOfProfileOctetsForwarded"]
      )
  elsif command == 'y'
    result, data = api.get_delay_stats(
      "100030",
      "ethernetoam.CfmTwoWayDelayTestResult",
      [{:start_time => ((Time.now.to_i-2000)*1000).to_s, :end_time => ((Time.now.to_i-1000)*1000).to_s},
       {:start_time => ((Time.now.to_i-600)*1000).to_s, :end_time => ((Time.now.to_i)*1000).to_s}],
      ["averageRoundTripTime", "neTestIndex"],
      "averageRoundTripTime",
      "58"
      )
  elsif command == 'z'
    result, data = api.get_alarm("10.10.100.5", "Port 1/1/2")
  else
    result = api.send_command command
  end


#  begin
#    server.puts command
#  rescue
#    puts "Got socket exception"
#    break
#  end
#
#  data = server.recvfrom( 255 )[0].chomp
#
#  if data == nil
#    puts "connection closed"
#    break
#  end
  
  puts "Server said:"
  puts "#{result}"
  puts "Size: #{result.size}"
end

server.close               # Close the socket when done
