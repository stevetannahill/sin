require 'socket'                # Get sockets from stdlib
require 'java'
require 'activemq-all-5.1.0.jar'
require 'samOss.jar'

#require('javax.jms.*')
#include_class "javax.jms.*"
include_class "javax.jms.Connection"
include_class "javax.jms.ConnectionFactory"
include_class "javax.jms.Destination"
include_class "javax.jms.ExceptionListener"
include_class('javax.naming.Context')
include_class('javax.naming.InitialContext')
include_class('javax.naming.NamingException')
include_class('java.io.BufferedReader')
include_class('java.io.InputStreamReader')
include_class('java.util.Hashtable')

include_class "org.apache.activemq.ActiveMQConnectionFactory"
include_class "org.apache.activemq.util.ByteSequence"
include_class "org.apache.activemq.command.ActiveMQBytesMessage"
include_class "javax.jms.MessageListener"
include_class "javax.jms.Session"
include javax.jms.ExceptionListener

include_class "JmsIf"
include_class "SOAPClient4XG"
class MessageHandler
#  include javax.jms.Session
  include javax.jms.MessageListener
  #

  def onMessage(serialized_message)
    message_body = serialized_message.get_content.get_data.inject("") { |body, byte| body << byte }
    puts message_body
  end

  def run

    java.util.Hashtable env = Hashtable.new
    env.put(Context.INITIAL_CONTEXT_FACTORY,"org.jnp.interfaces.NamingContextFactory")
    env.put(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces")
    env.put("jnp.disableDiscovery", "true")
    env.put("jnp.timeout", "60000")

    env.put(Context.PROVIDER_URL, "jnp://" + strUrl + PORT_SEP_CHAR + port + URL_SEP_CHAR);
    System.out.println("Provider URL: " + "jnp://" + strUrl + PORT_SEP_CHAR + port + URL_SEP_CHAR)
    System.out.println("Standalone URL for app server: " + strUrl)
    jndiContext =InitialContext.new(env)
    System.out.println("Initializing topic (" + strName + ")...")

    topicConnectionFactory = getExternalFactory(jndiContext)
    
  end
#    factory = ActiveMQConnectionFactory.new("http://10.1.8.135:1099")
#    connection = factory.create_connection();
#    session = connection.create_session(false, Session::AUTO_ACKNOWLEDGE);
#    queue = session.create_queue("test1-queue");
#
#    consumer = session.create_consumer(queue);
#    consumer.set_message_listener(self);
#
#    connection.start();
#    puts "Listening..."
  end
#
handler = MessageHandler.new
java.lang.System.out.println("Initializing topic .")
#handler.run

#server = TCPServer.open(2000)   # Socket to listen on port 2000
#puts "running..."
#loop {                          # Servers run forever
#  Thread.start(server.accept) do |client|
#    puts "got connection.."
#    client.puts(Time.now.ctime) # Send the time to the client
#	  client.puts "Closing the connection. Bye!"
#    client.close                # Disconnect from the client
#  end
#}

