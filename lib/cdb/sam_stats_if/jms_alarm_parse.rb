require "hpricot"

module JmsAlarmParse

  def JmsAlarmParse.extract_JMS_alarm_info event
    doc = Hpricot::XML(event)
    header = doc.at("//header")
    if header.at("MTOSI_NTType") != nil && header.at("MTOSI_NTType").innerHTML != "NT_ALARM"
      return []
    end

    # Is this a JMS reply or a normal SOAP reply
    jms_event = doc.at("//jms")

    alarms = []
    probable_cause = "" 
    #  Alarm info is in one of 2 places depending on if it's a object creation or deletion..
    if jms_event != nil
      if header.at("MTOSI_probableCause") != nil
        probable_cause = header.at("MTOSI_probableCause").innerHTML
      end      
      if jms_event.at("objectCreationEvent") != nil
        # An alarm object creation ie an alarm raised        
        alarms = [jms_event.at("objectCreationEvent")]
        timestamp = "firstTimeDetected"
      elsif jms_event.at("objectDeletionEvent") != nil        
        # An alarm object delete event ie a clear
        alarms = [jms_event.at("objectDeletionEvent")]
        timestamp = "lastTimeCleared"
      end
    else
       # It'a SOAP request
       alarms = doc.search("//result")
       timestamp = "firstTimeDetected"
    end      

    return_list = []    
    alarms.each do |alarm|
      node_name = name = state = time = nil
      name = alarm.at("affectedObjectFullName").innerHTML if !alarm.at("affectedObjectFullName").nil?        
      state = alarm.at("severity").innerHTML if !alarm.at("severity").nil?
      time =  alarm.at(timestamp).innerHTML if !alarm.at(timestamp).nil?   
      node_name = alarm.at("nodeName").innerHTML if !alarm.at("nodeName").nil?
      if probable_cause.empty?
        probable_cause_id = alarm.at("probableCause").innerHTML if !alarm.at("probableCause").nil?
        probable_cause = "Resync of Object State Requested (#{probable_cause_id})"
      end
      if name == nil || state == nil || time == nil || node_name == nil
        err = "No severity, name, time or nodeName found for Alarm name #{name}"
        Rails.logger.error err; SW_ERR err
        err = "#{event}"
        Rails.logger.error err; SW_ERR err
      else
        details = ""
        if state != "cleared"
          details = "#{node_name} : #{name} : #{probable_cause}"
        end
        return_list << {:timestamp => time, :event_filter => name, :state => state, :details => details}
      end    
    end
    return return_list
  end

end
