require 'rubygems'
require 'soap/element'
require 'soap/rpc/driver'
require 'soap/processor'
require 'soap/streamHandler'
require 'soap/property'
require "sam_if_common"

class SamSoapInterface    
  attr_reader :nms
  
  def initialize(nms_id)
    @nms = NetworkManager.find(nms_id)
    @active_nms = nil
    @mutex = Mutex.new
    reload_model
  end  

  # Need to reload the nms data from the database everytime because of caching.
  # The nms data may have changed so need to check if it still exists and
  # the active IP is still associated with the nms
  def reload_model
    result = true
    @mutex.synchronize {        
      begin
        @nms.reload
        if @nms.network_management_servers.find_by_primary_ip(@active_nms) == nil
           @active_nms = nil
           if @nms.network_management_servers.first != nil
            @active_nms = @nms.network_management_servers.first.primary_ip
          end
        end
      rescue ActiveRecord::RecordNotFound
        # The nms object no longer exists! 
        result = false
      end
    }
    return result
  end

  def get_mib_stats msg
    Rails.logger.info "Getting mib stats"

    filters = msg.get_filters
    result_filters = msg.get_result_filters
    Rails.logger.debug "Params are: #{filters.inspect} #{result_filters.inspect}" 

    request, envelope = create_soap_header
    body = XML::Node.new('SOAP:Body')
    envelope << body

    command = XML::Node.new('find')
    XML::Namespace.new(command, nil, 'xmlapi_1.0')
    body << command

    class_name = XML::Node.new('fullClassName')
    class_name.content = filters['class_name']
    command << class_name

    filter = XML::Node.new('filter')
    filter_and = XML::Node.new('and')

    filter_or = XML::Node.new('or')

    filters['sap'].each_pair do |display_name, node_ids|
      filter_display_name_and =  XML::Node.new('and')
      filter_condition = XML::Node.new('equal')
      name = XML::Attr.new(filter_condition, 'name', "displayedName")
      value = XML::Attr.new(filter_condition, 'value', "#{display_name}")
      filter_display_name_and << filter_condition
      filter_nodes_or =  XML::Node.new('or')       
      node_ids.each do |node|
        filter_array_equal = XML::Node.new('equal')
        XML::Attr.new(filter_array_equal, 'name', "monitoredObjectSiteId")
        XML::Attr.new(filter_array_equal, 'value', node) 
        filter_nodes_or << filter_array_equal
      end
      filter_display_name_and << filter_nodes_or
      filter_or << filter_display_name_and
    end

    filter_and << filter_or

    filter_time_interval = time_filter('timeCaptured', filters['collect_times'])

    filter_and << filter_time_interval

    filter << filter_and
    command << filter

    result_filter = XML::Node.new('resultFilter')
    result_filters.each do |a|
      attribute = XML::Node.new('attribute')
      attribute.content = a
      result_filter << attribute
    end

    children = XML::Node.new('children')
    result_filter << children

    command << result_filter

    request_string = request.to_s

    resp_data = send_request request_string

    result_string, data = convert_to_hash(resp_data)

    Rails.logger.info result_string
    return result_string, data 

  end

  def get_delay_stats msg
    Rails.logger.info "Getting delay stats"

    filters = msg.get_filters
    result_filters = msg.get_result_filters
    Rails.logger.debug "Params are: #{filters.inspect} #{result_filters.inspect}"  

    request, envelope = create_soap_header

    body = XML::Node.new('SOAP:Body')
    envelope << body

    command = XML::Node.new('find')
    XML::Namespace.new(command, nil, 'xmlapi_1.0')
    body << command

    class_name = XML::Node.new('fullClassName')
    class_name.content = filters['class_name']
    command << class_name

    filter = XML::Node.new('filter')
    filter_and = XML::Node.new('and')

    filters['query_filters'].each_pair do |key, value|
      filter_condition = XML::Node.new('equal')
      name = XML::Attr.new(filter_condition, 'name', "#{key}")
      value = XML::Attr.new(filter_condition, 'value', "#{value}")
      filter_and << filter_condition
    end  

    filter_or = XML::Node.new('or')
    filter_time_interval = time_filter('timeCaptured', filters['collect_times'])
    filter_or << filter_time_interval

    filter_and_limit = XML::Node.new('and')  

    # Set limit filter to filter from the very start to the very end
    filter_limit_time_interval = time_filter('timeCaptured', [:start_time => filters['collect_times'].collect {|i| i[:start_time]}.min, :end_time => filters['collect_times'].collect {|i| i[:end_time]}.max])

    filter_and_limit << filter_limit_time_interval

    filter_or1 = XML::Node.new('or')
    filters['limit_attribute'].each_index do |idx|
      filter_limit = XML::Node.new('greater')  
      name_limit = XML::Attr.new(filter_limit, 'name', filters['limit_attribute'][idx])  
      value_lmit = XML::Attr.new(filter_limit, 'value', filters['limit_value'][idx])
      filter_or1 << filter_limit
    end
    filter_and_limit << filter_or1  

    filter_or << filter_and_limit

    filter_and << filter_or

    filter << filter_and
    command << filter

    result_filter = XML::Node.new('resultFilter')
    result_filters.each do |a|
      attribute = XML::Node.new('attribute')
      attribute.content = a
      result_filter << attribute
    end

    children = XML::Node.new('children')
    result_filter << children

    command << result_filter

    request_string = request.to_s

    resp_data = send_request request_string

    result_string, data = convert_to_hash(resp_data)

    Rails.logger.info result_string
    return result_string, data 

  end

  def get_alarm msg
    Rails.logger.info "Getting alarm"

    filters = msg.get_filters
    result_filters = msg.get_result_filters
    Rails.logger.debug "Params are: #{filters.inspect} #{result_filters.inspect}"  

    request, envelope = create_soap_header

    body = XML::Node.new('SOAP:Body')
    envelope << body

    command = XML::Node.new('fm.FaultManager.findFaults')
    XML::Namespace.new(command, nil, 'xmlapi_1.0')
    body << command

    filter = XML::Node.new('faultFilter')
    filter_or = XML::Node.new('or')
    XML::Attr.new(filter_or, 'class', 'fm.AlarmInfo')

    filters['alarm_object'].each do |alarm|
      filter_condition2 = XML::Node.new('equal')
      XML::Attr.new(filter_condition2, 'name', 'affectedObjectFullName')
      XML::Attr.new(filter_condition2, 'value', alarm)
      filter_or << filter_condition2
    end

    filter << filter_or
    command << filter

    result_filter = XML::Node.new('resultFilter')
    result_filters.each do |a|
      attribute = XML::Node.new('attribute')
      attribute.content = a
      result_filter << attribute
    end

    children = XML::Node.new('children')
    result_filter << children

    command << result_filter

    request_string = request.to_s

    resp_data = send_request request_string

    result_string =  "Ok"

    Rails.logger.info result_string
    return result_string, resp_data 

  end

  def get_inventory msg
    Rails.logger.info "Getting Inventory"

    filters = msg.get_filters
    result_filters = msg.get_result_filters
    Rails.logger.debug "Params are: #{filters.inspect} #{result_filters.inspect}"  

    request, envelope = create_soap_header

    body = XML::Node.new('SOAP:Body')
    envelope << body

    command = XML::Node.new('find')
    XML::Namespace.new(command, nil, 'xmlapi_1.0')
    body << command

    class_name = XML::Node.new('fullClassName')
    class_name.content = filters['class_name']  
    command << class_name

    filter = XML::Node.new('filter')
    filter_and = XML::Node.new('and')

    filters['query_filters'].each_pair do |key, value|
      filter_condition = XML::Node.new('equal')
      name = XML::Attr.new(filter_condition, 'name', "#{key}")
      value = XML::Attr.new(filter_condition, 'value', "#{value}")
      filter_and << filter_condition
    end

    filter << filter_and
    command << filter

    result_filter = XML::Node.new('resultFilter')
    result_filters.each do |a|
      attribute = XML::Node.new('attribute')
      attribute.content = a
      result_filter << attribute
    end

    children = XML::Node.new('children')
    result_filter << children

    command << result_filter

    request_string = request.to_s

    resp_data = send_request request_string

    result_string =  "Ok"

    result_string, data = convert_to_hash(resp_data)

    Rails.logger.info result_string
    return result_string, data

  end

  def get_service_stats msg
    Rails.logger.info "Getting stats"

    filters = msg.get_filters
    result_filters = msg.get_result_filters
    Rails.logger.debug "Params are: #{filters.inspect} #{result_filters.inspect}"  

    request, envelope = create_soap_header

    body = XML::Node.new('SOAP:Body')
    envelope << body

    query_filters = filters['query_filters']
    class_names = query_filters.keys

    class_names.each do |class_name|
      command = XML::Node.new('find')
      XML::Namespace.new(command, nil, 'xmlapi_1.0')
      body << command

      classname = XML::Node.new('fullClassName')
      classname.content = class_name
      command << classname

      filter = XML::Node.new('filter')
      filter_and = XML::Node.new('and')

      filter_display_name_or =  XML::Node.new('or')
      query_filters[class_name].each_pair do |display_name, q_or_p_ids|
        filter_display_name_and =  XML::Node.new('and')
        filter_condition = XML::Node.new('equal')
        name = XML::Attr.new(filter_condition, 'name', "displayedName")
        value = XML::Attr.new(filter_condition, 'value', "#{display_name}")
        filter_display_name_and << filter_condition
        
        filter_nodes_or =  XML::Node.new('or')       
        q_or_p_ids[:nodes].each do |node|
          filter_array_equal = XML::Node.new('equal')
          XML::Attr.new(filter_array_equal, 'name', "monitoredObjectSiteId")
          XML::Attr.new(filter_array_equal, 'value', node) 
          filter_nodes_or << filter_array_equal
        end
        filter_display_name_and << filter_nodes_or
        
        filter_ids_or =  XML::Node.new('or')     
        q_or_p_ids.delete(:nodes)
        q_or_p_ids.each_pair do |id_name, ids|
          ids.each do |id|
            filter_array_equal = XML::Node.new('equal')
            XML::Attr.new(filter_array_equal, 'name', "#{id_name}")
            XML::Attr.new(filter_array_equal, 'value', "#{id}") 
            filter_ids_or << filter_array_equal
          end
          filter_display_name_and << filter_ids_or
        end
         filter_display_name_or << filter_display_name_and
      end
      filter_and << filter_display_name_or
        
      filter_and1 = XML::Node.new('and')
      filter_time_interval = time_filter('timeRecorded', filters['collect_times'])
      filter_and1 << filter_time_interval

      filter_and << filter_and1

      filter << filter_and
      command << filter

      result_filter = XML::Node.new('resultFilter')
      result_filters.each do |a|
        attribute = XML::Node.new('attribute')
        attribute.content = a
        result_filter << attribute
      end

      children = XML::Node.new('children')
      result_filter << children

      command << result_filter
    end

    request_string = request.to_s

    resp_data = send_request request_string

    result_string, data = convert_to_hash(resp_data)

    Rails.logger.info result_string
    return result_string, data
  end

private

def send_request data
  Rails.logger.debug "SENDING:"
  Rails.logger.debug data
  attempts = 1
  current_nms = @active_nms

  # No server can be found exit
  raise if current_nms == nil
  
  begin
    stream = SOAP::HTTPStreamHandler.new(SOAP::Property.new)
    stream.client.connect_timeout=10
    stream.client.send_timeout=20
    stream.client.receive_timeout=20 #10 # For each 16K block

    request = SOAP::StreamHandler::ConnectionData.new(data)
    url = "http://#{current_nms}:8080/xmlapi/invoke"
    time_at_send = Time.now
    resp_data = stream.send(url, request)
  rescue Exception => e
    if attempts >= @nms.network_management_servers.size * 2
      raise # Tried all servers twice - fail
    else 
      # Try the other server 
      Rails.logger.info "Failed to connect #{current_nms}"
      attempts = attempts + 1
      server = @nms.network_management_servers.find_by_primary_ip(current_nms)
      server = @nms.network_management_servers[@nms.network_management_servers.index(server)+1]
      server = @nms.network_management_servers.first if server == nil
      if server == nil
        current_nms = nil
      else
        current_nms = server.primary_ip
      end
      Rails.logger.info "Attempting #{current_nms}"
      retry
    end
  end
  # Managed to talk to the server - now check if it is the same server as the active_nms
  # if it's not then update the active_nms
  # Doing this allows for multiple threads as each thread will only update the active_nms
  # after it's figured out which one is working
  @mutex.synchronize {    
    @active_nms = current_nms if current_nms != @active_nms
  }
  Rails.logger.info "Got response: Duration = #{Time.now - time_at_send}"
  Rails.logger.debug resp_data.receive_string
  Rails.logger.info "Response size: #{resp_data.receive_string.size}"
  
  return resp_data.receive_string
end


def time_filter(time_attribute, collect_times)
  filter_or = XML::Node.new('or')
  collect_times.each do |collect_time|
    filter_time_interval = XML::Node.new('between')
    name = XML::Attr.new(filter_time_interval, 'name', time_attribute)
    first = XML::Attr.new(filter_time_interval, 'first', collect_time[:start_time])
    second = XML::Attr.new(filter_time_interval, 'second', collect_time[:end_time])
    filter_or << filter_time_interval
  end

  return filter_or
end

def create_soap_header
  request = XML::Document.new()
  envelope = XML::Node.new('SOAP:Envelope')
  XML::Namespace.new(envelope, 'SOAP', 'http://schemas.xmlsoap.org/soap/envelope/')
  request.root = envelope
  header = XML::Node.new('SOAP:Header')
  envelope << header
  h = XML::Node.new('header')
  XML::Namespace.new(h, nil, 'xmlapi_1.0')
  header << h
  security = XML::Node.new('security')
  h << security

  user = XML::Node.new('user')
  user.content = SamIfCommon::SAMO_USER
  security << user

  password = XML::Node.new('password')
  password.content = SamIfCommon::SAMO_PASSWD
  security << password

  return request, envelope
end  

def convert_to_hash(resp_data)
  Rails.logger.debug "Parsing..."
  context = XML::Parser::Context.string(resp_data)
  parser = XML::Parser.new(context)
  doc, statuses = parser.parse, []

  result_string = ""
  record_count = 0

  data = []
  search_string = 'SOAP:Body/*/*/*'
  records = doc.find(search_string)
  records.each do |record|
    h = {}
    record.to_a.each do |a|
      h[a.name] = a.content      
    end
    record_count += 1
    data << h 
  end

  result_string +=  "Records found: #{record_count}"
  return result_string, data
end

end
