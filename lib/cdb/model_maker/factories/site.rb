module ModelMaker
  class SiteFactory < Factory
    model Site
    attributes [
      :name,
      :address,
      :contact_info,
      :site_host,
      :floor,
      :clli,
      :icsc,
      :network_address,
      :broadcast_address,
      :loopback_address,
      :host_range,
      :hosts_per_subnet,
      :notes,
      :node_prefix,
      :postal_zip_code,
      :emergency_contact_info,
      :site_access_notes,
      :city,
      :state_province,
      :site_type,
      :latitude,
      :longitude,
      :country,
      :service_id_low,
      :service_id_high,
      :site_layout,
    ]

    relations [
      :operator_network
    ]

    metadata [
      :builder_type,
      :site_groups,
    ]
  end

  class AggregationSiteFactory < SiteFactory
    model AggregationSite
  end
end
