module ModelMaker
  class DemarcFactory < Factory
    model Demarc
    extend Stateful

    attributes [
      :cenx_demarc_type,
      :notes,
      :address,
      :port_name,
      :physical_medium,
      :connected_device_type,
      :connected_device_sw_version,
      :reflection_mechanism,
      :mon_loopback_address,
      :end_user_name,
      :is_new_construction,
      :contact_info,
      :max_num_segments,
      :ether_type,
      :protection_type,
      :lag_id,
      :fiber_handoff_type,
      :auto_negotiate,
      :ah_supported,
      :lag_mac_primary,
      :lag_mac_secondary,
      :port_enap_type,
      :lag_mode,
      :cir_limit,
      :eir_limit,
      :demarc_icon,
      :near_side_clli,
      :far_side_clli,
      :latitude,
      :longitude,
      :site_id
    ]
    
    relations [
      :operator_network,
      :demarc_type,
      :site, 
      :emergency_contact,
      :on_behalf_of_service_provider
    ]

    metadata [:builder_type, :service_order, :member_handles]
    
      
  end

  class UniFactory < DemarcFactory
    model Uni
    extend Stateful
  end

  class EnniFactory < DemarcFactory
    model EnniNew
    extend Stateful

    metadata [:ports]

    def pre_save
      if @object.lag_id.nil? and @object.is_lag
        @object.lag_id = IdTools::LagIdGenerator.generate_enni_lag_id Site.find(@object.site_id)
      end
    end
  end
end
