module ModelMaker
  class PathFactory < Factory
    model Path
    extend Stateful
    attributes [
      :cenx_path_type, 
      :description,
      :fallout_timestamp,
      :fallout_severity,
      :fallout_exception_type,
      :fallout_details,
      :fallout_data_source,
      :fallout_category,
      :fallout_exception_id
    ]
    relations [:operator_network, :path_type]
    metadata [:builder_type, :segments, :service_order, :member_handles]
    

    def pre_finalize
      @object.segments.each do |segment|
        next unless segment.is_a? OnNetOvc
        results = segment.configure_qos_policies
        raise BuilderError.new(results["Errors"]) unless results["Errors"].nil?
      end

      Path.find(@object.id).layout_diagram
    end
  end

  class EvcFactory < PathFactory
    model Evc
    extend Stateful
  end
end
