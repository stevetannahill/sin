module ModelMaker
  
  #--- Cos Instance ---#
  class CosInstanceFactory < Factory
    model CosInstance

    attributes [:notes]
    relations [:class_of_service_type, :segment]
  end

  class CenxCosInstanceFactory < CosInstanceFactory
    model CenxCosInstance
  end

  #--- End Cos Instance ---#

  #--- Cos End Point ---#
  class CosEndPointFactory < Factory
    model CosEndPoint
    extend Stateful
    
    attributes [
      :ingress_mapping,
      :ingress_cir_kbps,
      :ingress_eir_kbps,
      :ingress_cbs_kB,
      :ingress_ebs_kB,
      :egress_marking,
      :egress_cir_kbps,
      :egress_eir_kbps,
      :egress_cbs_kB,
      :egress_ebs_kB,
      :ingress_rate_limiting,
      :egress_rate_limiting,
    ]

    relations [
      :segment_end_point,
      :cos_instance,
      :connected_cos_end_point,
    ]

    metadata [:cos_test_vectors, :connected]
    

  end
  
  class CenxCosEndPointFactory < CosEndPointFactory
    model CenxCosEndPoint
    extend Stateful
    
    def pre_create
      #Handle Default Burst Rates
      if @relations[:cos_instance].is_a?(Factory)
        cosi = CosInstance.find(@relations[:cos_instance].get_id)
      elsif @relations[:cos_instance].is_a?(ActiveRecord::Base)
        cosi = @relations[:cos_instance]
      elsif @relations[:cos_instance].is_a?(Integer)
        cosi = CosInstance.find(@relations[:cos_instance])
      else
        raise BuilderError.new("CenxCosEndPointFactory: Not provided valid Cos Instance")
      end

      if @attributes[:ingress_rate_limiting] != "None"
        if @attributes[:ingress_cbs_kB].nil?
          #puts "#{cosi.cos_name} : #{@attributes[:ingress_cir_kbps]}"
          @attributes[:ingress_cbs_kB] = CenxCosEndPoint.get_default_burst_rate(cosi, @attributes[:ingress_cir_kbps], "commited")
        end
        if @attributes[:ingress_ebs_kB].nil?
          @attributes[:ingress_ebs_kB] = CenxCosEndPoint.get_default_burst_rate(cosi, @attributes[:ingress_eir_kbps], "excess")
        end
      end

      if @attributes[:egress_rate_limiting] != "None"
        if @attributes[:egress_cbs_kB].nil?
          @attributes[:egress_cbs_kB] = CenxCosEndPoint.get_default_burst_rate(cosi, @attributes[:egress_cir_kbps], "commited")
        end
        if @attributes[:egress_ebs_kB].nil?
          @attributes[:egress_ebs_kB] = CenxCosEndPoint.get_default_burst_rate(cosi, @attributes[:egress_eir_kbps], "excess")
        end
      end

    end
  end

  class MonitorCosEndPointFactory < CenxCosEndPointFactory
    model CenxCosEndPoint
    extend Stateful

    #Override CenxCosEndPointFactory#pre_create
    def pre_create
    end

    def post_create
      #Build the cos endpoint values from the predefined test values
      ServiceCategories::CENX_TEST_COS_VALUES[:rates].each do |attribute, value|
        @object.send("#{attribute}=", value)
      end

      @object.ingress_rate_limiting = ServiceCategories::CENX_TEST_COS_VALUES[:rlm]["Ingress"]
      @object.egress_rate_limiting = ServiceCategories::CENX_TEST_COS_VALUES[:rlm]["Egress"]

      cosi = @object.cos_instance
      fwding_class = ServiceCategories::ON_NET_OVC_COS_TYPES[cosi.cos_name][:fwding_class]
      @object.ingress_mapping = ServiceCategories::CENX_TEST_COS_VALUES[:mapping][fwding_class]
      @object.egress_marking = ServiceCategories::CENX_TEST_COS_VALUES[:mapping][fwding_class]
    end
  end

  #---

  class CosTestVectorFactory < Factory
    model CosTestVector
    
    attributes [
      :test_type,
      :notes,
      :flr_error_threshold,
      :flr_warning_threshold,
      :delay_error_threshold,
      :dv_error_threshold,
      :delay_warning_threshold,
      :dv_warning_threshold,
      :availability_guarantee,
      :flr_sla_guarantee,
      :dv_sla_guarantee,
      :delay_sla_guarantee,
      :circuit_id,
      :ref_circuit_id,
      :last_hop_circuit_id,
      :event_record_id, :sm_state, :sm_details, :sm_timestamp,
      :flr_reference,
      :delay_reference,
      :delay_min_reference, 
      :delay_max_reference,
      :dv_reference,
      :dv_min_reference,
      :dv_max_reference,
      :loopback_address,
      :circuit_id_format,
      :grid_circuit_id,
      :ref_grid_circuit_id,
      :test_instance_class_id,
      :service_instance_id, 
      :sla_id, 
      :test_instance_id, 
      :circuit_id, 
      :ref_circuit_id, 
      :protection_path_id
    ]
    
    relations [
     :cos_end_point,
     :service_level_guarantee_type
    ]
    
    metadata [:builder_type]

    def pre_create
      slgt = ServiceLevelGuaranteeType.find(@relations[:service_level_guarantee_type])
      if @attributes[:flr_sla_guarantee].nil?
        @attributes[:flr_sla_guarantee] = slgt.default_frame_loss_ratio_percent
      end
      if @attributes[:delay_sla_guarantee].nil?
        @attributes[:delay_sla_guarantee] = slgt.default_delay_us
      end
      if @attributes[:dv_sla_guarantee].nil?
        @attributes[:dv_sla_guarantee] = slgt.default_delay_variation_us
      end

      if @attributes[:flr_warning_threshold].nil?
        @attributes[:flr_warning_threshold] = CosTestVector.get_default_warning_threshold(@attributes[:flr_error_threshold])
      end
      if @attributes[:delay_warning_threshold].nil?
        @attributes[:delay_warning_threshold] = CosTestVector.get_default_warning_threshold(@attributes[:delay_error_threshold])
      end
      if @attributes[:dv_warning_threshold].nil?
        @attributes[:dv_warning_threshold] = CosTestVector.get_default_warning_threshold(@attributes[:dv_error_threshold])
      end
    end
  end

  class BxCosTestVectorFactory < CosTestVectorFactory
    model BxCosTestVector
  end
  
  class SpirentCosTestVectorFactory < CosTestVectorFactory
    model SpirentCosTestVector
  end

end
