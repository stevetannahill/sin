module ModelMaker
  class PortFactory < Factory
    model Port
    attributes [
      :name,
      :color_code,
      :mac,
      :phy_type,
      :other_phy_type,
      :notes,
      :patch_panel_slot_port, 
      :strands, 
      :patch_panel_id
    ]

    relations [
      :node,
      :connected_port,
      :demarc,
    ]
  end

end
