module ModelMaker

  #Circuit Building
  def self.build(order)
    circuit = Circuit.new

    begin
      ActiveRecord::Base.transaction do
        circuit = CircuitBuilder.build(order)
      end
      circuit.successful = true
    rescue BuilderError => e
      circuit.error = e
      circuit.successful = false
      SW_ERR e.message
    end

    return circuit
  end

  #Error Handling
  class BuilderError < StandardError
  end
  
end
