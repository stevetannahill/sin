module ModelMaker
  
  class PathBuilder < Builder
    
    attr_reader :path_factory
    attr_reader :connection_manager, :handle_manager, :bulk_order_manager

    def initialize(name, order, demarcs, handle_manager, bulk_order_manager)
      super(order)
      @name = name
      @connection_manager = ConnectionManager.new(@order)
      @connection_manager.add_demarcs(demarcs) unless demarcs.nil?
      @handle_manager = handle_manager
      @bulk_order_manager = bulk_order_manager
    end
        
    def build
      create_path
      build_bulk_order
      build_segments
      finalize_path
    end
    
    def create_path
      @path_factory = PathFactory.make(@params)
    end
    
    def build_bulk_order
      if @order[:service_order]
        params = @order[:service_order].dup
        params[:path] = @path_factory
        params[:ordered_entity] = @path_factory
        params[:operator_network] = @order[:operator_network]
        bulk_order = BulkOrderFactory.produce(params)
        puts "Build Bulk Order with name #{@name}"
        @bulk_order_manager.add_bulk_order(@name, bulk_order)
      end
    end
    
    def build_segments
      @order[:segments].each do |segment_name, segment_order|
        if segment_order[:id].nil?
          segment_order[:path_network] = @order[:operator_network]
          SegmentBuilder.build(segment_name, segment_order, self)
        else
          segment = Segment.find(segment_order[:id])
          @path_factory.populate(:segments, segment)
        end
        
      end
    end
    
    def finalize_path
      @connection_manager.build_connections

      path = @path_factory.finalize
      @handle_manager.add_handle(path, @order[:member_handles])
      return path
    end
    
    def default_params
      return { :operator_network => @order[:operator_network] }
    end

    #Override Class#new to provide proper subclass
    def self.new(name, order, demarcs, handle_manager, bulk_order_manager)
      path_type = order[:builder_type]
      builder_class = get_builder_by_type(path_type)
      raise BuilderError.new("Path Builder: Can not determine proper PathBuilder for Path type #{path_type}") if builder_class.nil?
      obj = builder_class.allocate
      obj.send(:initialize, name, order, demarcs, handle_manager, bulk_order_manager)
      obj
    end
    private
    def self.get_builder_by_type(type)
      builders = {
        "Evc" => EvcBuilder,
      }

      return builders[type]
    end

  end

  class EvcBuilder < PathBuilder
    def create_path
      @path_factory = EvcFactory.make(@params)
    end
  end

end
