class SpirentStatsArchive  < MonitoredEndpointStats

  SPIRENT_METRIC_COLUMNS = {
#    :availability => 'n/a',
#    :frame_loss => 'framesLost',
#    :frame_loss_ratio => 'frameLossRatio',
    :delay => 'AverageDelay/2',
    :delay_variation => 'AverageDelayVariation/2' ,
    :min_delay => 'MinimumDelay/2',
    :min_delay_variation => 'MinimumDelayVariation/2',
    :max_delay => 'MaximumDelay/2',
    :max_delay_variation => 'MaximumDelayVariation/2' ,
#    :octets_count => 'octets_count',
    :frames_sent => 'FramesSent',
    :frames_received => 'FramesReceived',
  }
  
  IP_SLA_TEST_METRIC_COLUMNS = {
#    :availability => 'n/a',
#    :frame_loss => 'framesLost',
#    :frame_loss_ratio => 'frameLossRatio',
    :delay => 'AverageDelay/2',
    :delay_variation => 'AverageDelayVariation/2' ,
    :min_delay => 'MinimumDelay/2',
    :min_delay_variation => 'MinimumDelayVariation/2',
    :max_delay => 'MaximumDelay/2',
    :max_delay_variation => 'MaximumDelayVariation/2' ,
#    :octets_count => 'octets_count',
    :frames_sent => 'FramesSent',
    :frames_received => 'FramesReceived',
    }  

  def initialize(obj, start_time, end_time, current_time=Time.now, adjust_time=true)
    super(obj, start_time, end_time, current_time, adjust_time)
  end
  
  #TODO temp for sprint Core demo - Should be merged back by to monitored
  def get_delay_attributes(attribute_types)
    attribute_types = [attribute_types] if !attribute_types.is_a?(Array)
    attributes = attribute_types.uniq.collect do |attribute|
      case attribute
      when :sla
        case @obj.test_type
        when 'Ping Active Test' # BJW not sure if all Spirent ping tests don't return delay_variation but for ATT they don't
          [:delay]
        else
          [:delay, :delay_variation]
        end
      when :troubleshooting
        case @obj.test_type
        when 'Ethernet Frame Delay Test', 'Ethernet Loopback Test', "TWAMP Test"
          [:min_delay, :max_delay, :min_delay_variation, :max_delay_variation]
        when 'Ping Active Test'
          [:min_delay, :max_delay]
        when 'IP-SLA Test'
          [:min_delay, :max_delay]          
        else
          SW_ERR "ERROR: Unknown Test Type: #{@obj.test_type}\n"
          []
        end
      else
        SW_ERR "Invalid attribute type #{attribute}"
        []
      end
    end.flatten.uniq
    
    return attributes
  end
  
  def bx_availability_kludge
    #TODO REMOVE AND MAKE SURE IT'S NOT CALLED FROM STAS_CONTROLLER
  end
  def timestamp_name
    "sample_end_epoch"
  end
  
  def stats_get_delay_tests(ignore_cos = false)   
    # If there are no COS then don't return the tests
    return_tests = {}
    {"dv" => "delay_variation", "delay" => "delay"}.each { |sla_attribute, attribute|
      return_tests[attribute] = [] 
      if stats_get_slas(sla_attribute).first.size != 0 || ignore_cos
        return_tests[attribute] = ["Total"]
        if !@obj.ref_circuit_id.blank?
          return_tests[attribute] << "ETH"
          return_tests[attribute] << "ETH_delta"
        end
        if !@obj.last_hop_circuit_id.blank?
          return_tests[attribute] << "LastHop_delta"
        end
      end
    }
    return return_tests
  end
  

  def stats_get_normalized_flr_data(ignore_sla = false)
    result_data = {}
    cep = @obj.cos_end_point
    if !cep.segment_end_point.is_monitored
      SW_ERR "The endpoint is not monitored"
    else
      sla_id = @obj.circuit_id
      test_type = @obj.test_type

      if sla_id.empty?
        SW_ERR "sla_id(#{sla_id}) is empty"
      else
        attributes = [:frame_loss_ratio]
        attribute_mappings, name_mapping =  get_test_class_attributes(attributes)
        name_mapping.each {|internal, external| result_data[external] = []}
        result_data.delete("LastHop")

        t = Time.now
        data = compute_metric_data_points(attributes)
        data = [] if data.nil?

        mappings = attribute_mappings.clone
        lasthop_internal = nil
        if name_mapping.find {|k,v| v == "LastHop"} != nil
          lasthop_internal = name_mapping.find {|k,v| v == "LastHop"}[0]
        end
        ct = 0
        data.each do |row|

          ts = to_sec(row["timeCaptured"])
          result_rows = {}
          name_mapping.each {|internal_name, external_name| result_rows[external_name] = {} } #:timeCaptured => ts}}
          result_rows.delete("LastHop")

          row.each do |internal_name, value|
            next if internal_name.starts_with?(lasthop_internal) # Ignore last hop data
            # This code looks bad but is fast
            if internal_name.starts_with?("ref")
              name = internal_name[0..4]
              attribute_name = internal_name[5..-1]
            elsif internal_name.starts_with?("delta")
              name = internal_name[0..6]
              attribute_name = internal_name[7..-1]
            else
              name = nil
              attribute_name = internal_name
            end
            # end fast code
            test_name = name_mapping[name]
            unless value.nil?
              result_rows[test_name][:timeCaptured] = ts
              result_rows[test_name][mappings[attribute_name]] = value
            end
          end
          result_rows.each do |name, value|
            result_data[name] << value unless value.empty?
          end
        end
      end
    end
    slas = stats_get_flr_slas(ignore_sla)
    refs, ref_ids = stats_get_reference_data(:frame_loss_ratio)
    
    return {cep.cos_name => {:cos_id => slas[cep.cos_name][:cos_id], :sla => slas[cep.cos_name], :ref => refs[cep.cos_name], :data => result_data}}
  end

  

  def get_test_class_attributes(delay_type)
    if !delay_type.is_a?(Array)
      delay_type = [delay_type]
    end

    name_mapping = {nil => "Total"}
    if !@obj.ref_circuit_id.blank? && !@obj.last_hop_circuit_id.blank?
      name_mapping["ref1_"] = "ETH"
      name_mapping["delta1_"] = "ETH_delta"
      name_mapping["ref2_"] = "LastHop"
      name_mapping["delta2_"] = "LastHop_delta"
    elsif !@obj.ref_circuit_id.blank? && @obj.last_hop_circuit_id.blank?
      name_mapping["ref1_"] = "ETH"
      name_mapping["delta1_"] = "ETH_delta"
    elsif @obj.ref_circuit_id.blank? && !@obj.last_hop_circuit_id.blank?
      name_mapping["ref1_"] = "LastHop"
      name_mapping["delta1_"] = "LastHop_delta"
    end

    attribute_mappings = {"timeCaptured" => "timeCaptured"}
    delay_type.each do |dt|
      case dt
      when :delay
          attribute_mappings["averageLatency"] = :delay
      when :delay_variation
          attribute_mappings["averageDelayVariation"] = :delay_variation
      when :max_delay
          attribute_mappings["maximumLatency"] = :max_delay
      when :max_delay_variation
          attribute_mappings["maximumDelayVariation"] = :max_delay_variation
      when :min_delay
          attribute_mappings["minimumLatency"] = :min_delay
      when :min_delay_variation
          attribute_mappings["minimumDelayVariation"] = :min_delay_variation
      when :frames_sent
          attribute_mappings["framesSent"] = :deltaIngress
      when :frames_received
          attribute_mappings["framesReceived"] = :deltaEgress
      when :frame_loss_ratio
          attribute_mappings["frameLossRatio"] = :frame_loss_ratio          
      else
        SW_ERR "Invalid delay type #{dt}"
      end
    end
    return attribute_mappings, name_mapping
  end
  
  def compute_delay(delay_type)
    return compute_metric(delay_type)
  end

  def stats_get_delay_data(delay_type, test_name = nil)
    return_data = {}
    result_data = []
    cep = @obj.cos_end_point

    if !cep.segment_end_point.is_monitored
      SW_ERR "The endpoint is not monitored"
    else
      test_type = @obj.test_type
      if !delay_type.is_a?(Array)
        delay_type = [delay_type]
      end
      attribute_mappings, name_mapping =  get_test_class_attributes(delay_type)
      sla_id = @obj.circuit_id

      if sla_id.empty?
        SW_ERR "sla_id(#{sla_id}) is empty"
      else
        t = Time.now
        data = compute_metric_data_points(delay_type)
        data = [] if data.nil?
        
        mappings = attribute_mappings.clone
        lasthop_internal = nil
        if name_mapping.find {|k,v| v == "LastHop"} != nil
          lasthop_internal = name_mapping.find {|k,v| v == "LastHop"}[0]
        end
        ct = 0
        data.each do |row|

          ts = to_sec(row["timeCaptured"])
          result_rows = {}
          name_mapping.each {|internal_name, external_name| result_rows[external_name] = {"neTestIndex" => external_name, "timeCaptured" => ts}}
          result_rows.delete("LastHop")

          row.each do |internal_name, value|
            next if internal_name.starts_with?(lasthop_internal) # Ignore last hop data
            # This code looks bad but is fast....  
            if internal_name.starts_with?("ref")
              name = internal_name[0..4]
              attribute_name = internal_name[5..-1]
            elsif internal_name.starts_with?("delta")
              name = internal_name[0..6]
              attribute_name = internal_name[7..-1]
            else
              name = nil
              attribute_name = internal_name
            end
            # end of fast code
            test_name = name_mapping[name]
            result_rows[test_name][mappings[attribute_name]] = value
          end
          result_data.concat(result_rows.values)
        end
      end
      return_data[cep.cos_name] = result_data
    end
    return return_data
  end

  def compute_overall_flr_value
    overall_flr = 0.0
    unless @time_periods_secs.empty?
      overall_flr = compute_metric(:frame_loss_ratio)
    end
    return overall_flr
  end

  def compute_overall_flr
    overall_flr = compute_overall_flr_value
    return {@obj.cos_end_point.cos_name => {:cos_id => @obj.cos_end_point.id, :aggregate_flr => overall_flr}}
  end

  def collection_period
    return 5.minutes
  end


  def get_table_and_selected_columns
    table_name = 'ethframedelaytest'
    if @obj.test_type == 'IP-SLA Test'
      column_info = IP_SLA_TEST_METRIC_COLUMNS
    else
      column_info = SPIRENT_METRIC_COLUMNS
    end
    return timestamp_name, table_name, column_info
  end

  def to_native the_time
    the_time.to_i
  end

  def to_sec the_time
    the_time.to_i
  end

  def get_available_time_periods_native
    return @time_periods_secs
  end

  # For Spirent the availability time is based on frame loss not events so there is no need
  # to to adjust the times around the unavailable times
  # avail_data[i][:y] == available (or not)
  # avail_data[i][:x] == time period start time
  def get_available_time_periods(avail_data)
    time_periods = []
    return time_periods if avail_data.empty?
    time_pair = [0,0]
    if 1 == avail_data[0][:y]
      time_pair[0] = avail_data[0][:x]
    end
    i = 1
    while i< avail_data.length - 1
      if 1 == avail_data[i][:y] and 0 == time_pair[0]
        time_pair[0] = avail_data[i][:x]+1
      else
        unless 0 == time_pair[0]
          time_pair[1] = avail_data[i][:x]
          time_periods << time_pair
          time_pair = [0,0]
        end
      end
      i += 1
    end
    if (0 != time_pair[0] and 0 == time_pair[1])
      time_pair[1] = avail_data.last[:x]
      time_periods << time_pair
    end
    return time_periods
  end

  def get_where
    where = {'grid_circuit_id' => @obj.grid_circuit_id }
    return where
  end
  
  def get_ref_where
    unless @obj.ref_circuit_id.nil? or @obj.ref_circuit_id.empty?
      ref_ctv = CosTestVector.find_by_circuit_id(@obj.ref_circuit_id)
      ref_where = [ {'grid_circuit_id' => ref_ctv.grid_circuit_id } ]
    end
    return ref_where
  end
  
  def get_all_ref_where
    ref_where = [] unless @obj.ref_circuit_id.nil? and @obj.last_hop_circuit_id.nil?
    unless @obj.ref_circuit_id.nil? or @obj.ref_circuit_id.empty?
      ref_ctv = CosTestVector.find_by_circuit_id(@obj.ref_circuit_id)
      ref_where << {'grid_circuit_id' => ref_ctv.grid_circuit_id } unless ref_ctv.nil?
    end
    unless @obj.ref_circuit_id.nil? or @obj.ref_circuit_id.empty?
      ref_ctv = CosTestVector.find_by_circuit_id(@obj.last_hop_circuit_id)
      ref_where << {'grid_circuit_id' => ref_ctv.grid_circuit_id } unless ref_ctv.nil?
    end
    return ref_where
  end
  

end    

