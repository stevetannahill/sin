
require "date"

# Log the date and time
MAX_LOG_SIZE = 10*1024*1024
class FormatLogger < Logger
  def format_message(severity, timestamp, progname, msg)
    "#{timestamp.to_s} #{severity} #{msg}\n"
  end
end

class ReportMaker

  def initialize
    logger = FormatLogger.new("#{Rails.root}/log/#{File.basename(__FILE__, '.*')}.log", 1, MAX_LOG_SIZE)
    Rails.logger = logger # Give logger access to all ruby objects
    Rails.logger.level = (Rails.env == "development") ? Logger::Severity::DEBUG : Logger::Severity::INFO
    Rails.logger.info "Starting rollup_report generation"
  end

  def insert_ids
    ActiveRecord::Base.transaction do
      truncate_sql = "TRUNCATE TABLE rollup_ids;"
      ActiveRecord::Base.connection.execute(truncate_sql)
      insert_ids_sql = <<EOF
 INSERT INTO rollup_ids (grid_circuit_id, circuit_id, site_id, demarc_id, protection_path_id, cos_test_vector_id)
SELECT grid_circuit_id, circuit_id, site_id, demarc_id, protection_path_id, cos_test_vector_id
FROM
(SELECT `cos_test_vectors`.id AS cos_test_vector_id, `cos_test_vectors`.grid_circuit_id AS grid_circuit_id, `cos_test_vectors`.circuit_id AS circuit_id,
  `cos_test_vectors`.protection_path_id AS protection_path_id, `sites`.id AS site_id, `segment_end_points`.demarc_id AS demarc_id
FROM `cos_test_vectors`
LEFT JOIN `cos_end_points` ON `cos_end_points`.`id`=`cos_test_vectors`.cos_end_point_id
LEFT JOIN `segment_end_points` ON `segment_end_points`.id=`cos_end_points`.segment_end_point_id
-- the following is to get the site_id:
LEFT JOIN `segments` ON `segments`.id=`segment_end_points`.segment_id
LEFT JOIN `paths_segments` ON `paths_segments`.segment_id =`segments`.id
LEFT JOIN `paths` ON `paths`.id=`paths_segments`.path_id
LEFT JOIN `sites` ON `sites`.operator_network_id =`paths`.operator_network_id
GROUP BY cos_test_vector_id
) t;
EOF
      time_start = Time.now
      result =  ActiveRecord::Base.connection.execute(insert_ids_sql)
      Rails.logger.info "Done. Gathered all required IDs in #{(Time.now-time_start).to_f} seconds."
    end
  end


  def insert_weekly_path_reports period_start_epoch, period_end_epoch
    insert_path_report_sql = <<EOF
INSERT INTO rollup_report_paths (period_start_epoch, site_id, demarc_id, protection_path_id, metric_count, metric_clear_count, metric_minor_count, metric_major_count, severity)
SELECT period_start_epoch, site_id, demarc_id, protection_path_id,
  count(*) AS metric_count,
  SUM(CASE WHEN severity='clear' THEN 1 ELSE 0 END) AS metric_clear_count,
  SUM(CASE WHEN severity='minor' THEN 1 ELSE 0 END) AS metric_minor_count,
  SUM(CASE WHEN severity='major' THEN 1 ELSE 0 END) AS metric_major_count,
  -- if severity was an enum or integer we could do:  MAX(severity) AS severity  But instead we have to do this:
  CASE MAX(CASE WHEN severity='major' THEN 2 WHEN severity='minor' THEN 1 ELSE 0 END) WHEN 2 THEN 'major' WHEN 1 THEN 'minor' ELSE 'clear' END AS severity
FROM
(SELECT site_id, demarc_id, protection_path_id, period_start_epoch, time_declared_epoch, severity, metric_type, metric, minor_time_over, major_time_over FROM rollup_ids JOIN metrics_weekly_cos_test_vectors ON rollup_ids.grid_circuit_id = metrics_weekly_cos_test_vectors.grid_circuit_id
WHERE circuit_id LIKE '%-P7' AND period_start_epoch BETWEEN #{period_start_epoch} AND #{period_end_epoch}
) t
GROUP BY demarc_id, protection_path_id;
EOF
    time_start = Time.now
    result =  ActiveRecord::Base.connection.execute(insert_path_report_sql)
    Rails.logger.info "Done. Computed path reports in #{(Time.now-time_start).to_f} seconds."

  end

  def insert_weekly_demarc_reports period_start_epoch, period_end_epoch
    insert_demarc_report_sql = <<EOF
INSERT INTO rollup_report_demarcs (site_id, period_start_epoch, demarc_id, protection_path_count, path_clear_count, path_minor_count, path_major_count,
  metric_count, metric_clear_count, metric_minor_count, metric_major_count, severity)
SELECT site_id, period_start_epoch, demarc_id,
  SUM(protection_path_count) AS protection_path_count,
  SUM(path_clear_count) AS path_clear_count,
  SUM(path_minor_count) AS path_minor_count,
  SUM(path_major_count) AS path_major_count,
  SUM(metric_count) AS metric_count,
  SUM(metric_clear_count) AS metric_clear_count,
  SUM(metric_minor_count) AS metric_minor_count,
  SUM(metric_major_count) AS metric_major_count,
  (CASE WHEN errored_protection_path_count=0 THEN 'clear'
    WHEN errored_protection_path_count>0 AND errored_protection_path_count<protection_path_count THEN 'minor'
    WHEN protection_path_count = path_minor_count THEN 'minor'
    WHEN errored_protection_path_count=protection_path_count THEN 'major'
    ELSE 'clear' END)  AS severity
FROM (
  SELECT period_start_epoch, site_id, demarc_id,
    SUM(metric_count) AS metric_count,
    SUM(metric_clear_count) AS metric_clear_count,
    SUM(metric_minor_count) AS metric_minor_count,
    SUM(metric_major_count) AS metric_major_count,
    count(*) AS protection_path_count,
    SUM(CASE WHEN severity = 'clear' THEN 1 ELSE 0 END) AS path_clear_count,
    SUM(CASE WHEN severity = 'minor' THEN 1 ELSE 0 END) AS path_minor_count,
    SUM(CASE WHEN severity = 'major' THEN 1 ELSE 0 END) AS path_major_count,
    SUM(CASE WHEN metric_minor_count>0 OR metric_major_count>0 THEN 1 ELSE 0 END) AS errored_protection_path_count,
    severity
  FROM (
    SELECT period_start_epoch, site_id, demarc_id, protection_path_id, metric_count, metric_clear_count, metric_minor_count, metric_major_count , severity FROM rollup_report_paths
    WHERE period_start_epoch BETWEEN #{period_start_epoch} AND #{period_end_epoch}
    GROUP BY demarc_id, protection_path_id
  )t1
  GROUP BY demarc_id
)t2
GROUP BY demarc_id
;
EOF
    time_start = Time.now
    result =  ActiveRecord::Base.connection.execute(insert_demarc_report_sql)
    Rails.logger.info "Done. Computed demarc reports  in #{(Time.now-time_start).to_f} seconds."
  end

  def insert_weekly_site_reports period_start_epoch, period_end_epoch
    insert_site_report_sql = <<EOF
INSERT INTO rollup_report_sites (period_start_epoch, site_id, demarc_count, clear_demarc_count, minor_demarc_count, major_demarc_count,
  metric_count, metric_clear_count, metric_minor_count, metric_major_count, severity )
SELECT
  period_start_epoch ,
  site_id,
  count(*) AS demarc_count,
  SUM(CASE WHEN severity = 'clear' THEN 1 ELSE 0 END) AS clear_demarc_count,
  SUM(CASE WHEN severity = 'minor' THEN 1 ELSE 0 END) AS minor_demarc_count,
  SUM(CASE WHEN severity = 'major' THEN 1 ELSE 0 END) AS major_demarc_count,
  SUM(metric_count) AS metric_count,
  SUM(metric_clear_count) AS metric_clear_count,
  SUM(metric_minor_count) AS metric_minor_count,
  SUM(metric_major_count) AS metric_major_count,
  CASE MAX(CASE WHEN severity='major' THEN 2 WHEN severity='minor' THEN 1 ELSE 0 END) WHEN 2 THEN 'major' WHEN 1 THEN 'minor' ELSE 'clear' END AS severity
FROM rollup_report_demarcs
  WHERE period_start_epoch BETWEEN #{period_start_epoch} AND #{period_end_epoch}
GROUP BY site_id;
EOF
    time_start = Time.now
    result =  ActiveRecord::Base.connection.execute(insert_site_report_sql)
    Rails.logger.info "Done. Computed site reports in #{(Time.now-time_start).to_f} seconds."
  end

  def insert_weekly_site_group_reports period_start_epoch, period_end_epoch
    insert_site_group_report_sql = <<EOF
INSERT INTO rollup_report_site_groups (period_start_epoch, site_group_id, site_count, site_clear_count, site_minor_count, site_major_count,
  demarc_count, demarc_clear_count, demarc_minor_count, demarc_major_count, metric_count, metric_clear_count, metric_minor_count, metric_major_count, severity )
SELECT
  period_start_epoch,
  site_group_id,
  count(*) AS site_count,
  SUM(CASE WHEN severity = 'clear' THEN 1 ELSE 0 END) AS site_clear_count,
  SUM(CASE WHEN severity = 'minor' THEN 1 ELSE 0 END) AS site_minor_count,
  SUM(CASE WHEN severity = 'major' THEN 1 ELSE 0 END) AS site_major_count,
  SUM(demarc_count) AS demarc_count,
  SUM(clear_demarc_count) AS demarc_clear_count,
  SUM(minor_demarc_count) AS demarc_minor_count,
  SUM(major_demarc_count) AS demarc_major_count,
  SUM(metric_count) AS metric_count,
  SUM(metric_clear_count) AS metric_clear_count,
  SUM(metric_minor_count) AS metric_minor_count,
  SUM(metric_major_count) AS metric_major_count,
  CASE MAX(CASE WHEN severity='major' THEN 2 WHEN severity='minor' THEN 1 ELSE 0 END) WHEN 2 THEN 'major' WHEN 1 THEN 'minor' ELSE 'clear' END AS severity
FROM site_groups_sites JOIN rollup_report_sites ON site_groups_sites.site_id = rollup_report_sites.site_id
WHERE period_start_epoch BETWEEN #{period_start_epoch} AND #{period_end_epoch}
GROUP BY site_group_id;
EOF
    time_start = Time.now
    result =  ActiveRecord::Base.connection.execute(insert_site_group_report_sql)
    Rails.logger.info "Done. Computed site_group reports in #{(Time.now-time_start).to_f} seconds."
  end

  def delete_old_records table_name, period_start_epoch, period_end_epoch
    delete_sql = <<EOF
  DELETE FROM #{table_name} WHERE `period_start_epoch` BETWEEN #{period_start_epoch} AND #{period_end_epoch}
EOF
    result =  ActiveRecord::Base.connection.execute(delete_sql)
  end

  def test
    t = Time.utc(2012, 12,31, 12, 0,0)
    x = week_start_time t
    x = week_end_time t
    t = Time.utc(2012)
    x = week_start_time t
    x = week_end_time t
  end

  def week_start_time time
    return time.beginning_of_week
  end

  def week_end_time time
    return time.end_of_week
  end

  def self.fix_protection_path_ids
    SpirentCosTestVector.find_each do |ctv|
      ctv.save
    end
  end
  
  def self.test
    t = Time.at(1338768000) + 2.day
    report_maker = ReportMaker.new
    report_maker.generate_weekly_reports t
  end


  def generate_weekly_reports t

  end

  def generate_weekly_reports t
    time_start = Time.now
    # Ensure the connection to the database is up
    ActiveRecord::Base.verify_active_connections!
    
    # compute weekly reports
    # get start of current week.
    start_time = week_start_time t
    end_time   = week_end_time t
    period_start_epoch = start_time.to_i
    period_end_epoch = end_time.to_i - 1
    # first collect all relevant IDs into a table we can use:
    insert_ids
    ActiveRecord::Base.transaction do
      delete_old_records 'rollup_report_paths', period_start_epoch, period_end_epoch
      insert_weekly_path_reports period_start_epoch, period_end_epoch
    end
    ActiveRecord::Base.transaction do
      delete_old_records 'rollup_report_demarcs', period_start_epoch, period_end_epoch
      insert_weekly_demarc_reports period_start_epoch, period_end_epoch
    end
    ActiveRecord::Base.transaction do
      delete_old_records 'rollup_report_sites', period_start_epoch, period_end_epoch
      insert_weekly_site_reports period_start_epoch, period_end_epoch
    end
    ActiveRecord::Base.transaction do
      delete_old_records 'rollup_report_site_groups', period_start_epoch, period_end_epoch
      insert_weekly_site_group_reports period_start_epoch, period_end_epoch
    end
    Rails.logger.info "Done. Computed all weekly reports in #{(Time.now-time_start).to_f} seconds."
  end

  # compute weekly reports for all weeks starting at time t.
  def generate_all_weekly_reports t
    start_time = t
    now = Time.now.utc
    while (start_time <  now)
      generate_weekly_reports start_time
      start_time += 1.week
    end
  end

  def self.gen_prev_report
    report_maker = ReportMaker.new
    now = Time.now.utc
    if (Time.utc(now.year,now.month,now.day) == now.beginning_of_week )
      # update the report from last week to be sure it uses the latest metrics data.
      report_maker.generate_weekly_reports Time.now.utc - 1.week
    end
    report_maker.generate_weekly_reports Time.now.utc
  end

  def self.refresh_ids
    report_maker = ReportMaker.new
    report_maker.insert_ids
  end

end


