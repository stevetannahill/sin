module RCache
  module Perms # Permutations helper
    def perspective_service_providers(sp_path)
      yield sp_path.service_provider_id
      yield "all" if sp_path.service_provider_id == sp_path.path.service_provider.id
    end
    
    def path_owners(sp_path,perspective_service_provider)
      yield "all"
      if perspective_service_provider != "all"
        yield (sp_path.path.service_provider.id == perspective_service_provider) ? "mine" : "other"
      end
    end
    
    def provisioning_states(sp_path)
      yield "all"
      yield sp_path.path.prov_name
    end
    
    def ordering_states(sp_path)
      yield "all"
      yield sp_path.path.order_name
    end
    
    def segment_service_providers(sp_path)
      yield "all"
      sp_ids = sp_path.segment_displays.select{|d|d.service_provider_id == sp_path.service_provider_id && d.entity.send(:operator_network)}.map{|d|d.entity.send(:operator_network).service_provider_id}.uniq
      sp_ids.each do |sp_id|
        yield sp_id
      end
    end
    
    def sites(sp_path)
      yield "all"
      sp_path.segment_displays.map{|d|d.entity.site_id}.compact.uniq.each do |site_id|
        yield site_id
      end
    end
    
    def circuit_types(sp_path)
      yield "all"
      yield sp_path.path.cenx_path_type
    end
    
    def monitoring_states(sp_path)
      yield "all"
      yield sp_path.path.sm_state
    end
    
    def statements_of_work(sp_path)
      yield "all"
      hash = sp_path.bulk_service_orders.each_with_object({}) do |bso,hash|
        bso.containing_bulk_orders.each do |so|
          hash[so.id] = true
        end
      end
      hash.keys.each do |order_id|
        yield order_id
      end
    end
    
    def oems(sp_path)
      yield "all"
      yield sp_path.path.operator_network.operator_network_type_id
    end
  end
  
  module RedisHelpers
    FILTERING_SEPERATOR = ":"
  
    def incr(*key_parts)
      key = key_parts.join(FILTERING_SEPERATOR)
      redis.sadd "dashboard_keys", key
      redis.incr key
    end
    
    def sadd(*key_parts_and_member)
      member = key_parts_and_member.pop
      key = key_parts_and_member.join(FILTERING_SEPERATOR)
      redis.sadd "dashboard_keys", key
      redis.sadd key, member
    end
    
    def redis
      @redis ||= Redis.new
    end
    
    #debug method
    def pp_keys
      k = redis.smembers "dashboard_keys"
      h = k.inject({}) do |m,k|
        if redis.type(k) == "string"
          m[k] = redis.get(k)
        elsif redis.type(k) == "set"
          m[k] = redis.smembers(k)
        end
        m
      end
      pp h
      nil
    end
  end

  class Dashboard
    extend Perms
    extend RedisHelpers
    extend Benchmark

    PAGE_SIZE = 1000
    PIPELINE_SIZE = 10_000
    
    def self.bm_recount
      bm(6) do |x|
        x.report("recount") {recount}
      end
    end
    
    def self.recount
      #TODO could optimize using the pipelining feature
      #TODO needs to eventually support transactions or something to handle incrementing and refresh being performed at the same time
      clear_previous_keys
      
      page = 0
      while page
        results = SinPathServiceProviderSphinx.
                    limit(PAGE_SIZE).
                    offset(PAGE_SIZE*page).
                    includes([{:path => {:operator_network => :service_provider}},{:segment_displays => {:entity => :operator_network}},{:bulk_service_orders => :containing_bulk_orders}])
        
        page = if results.size == PAGE_SIZE
          page + 1
        else
          false
        end
        
        results.each do |sp_path|
          # Key Layout
          # dashboard:#{perspective_service_provider}:#{path_owner}:#{segment_service_provider}:#{site}:#{circuit_type}:#{provisioning_state}:#{ordering_state}:#{monitoring_state}:#{statement_of_work}:#{oem}:#{count_name}
          perspective_service_providers(sp_path) do |perspective_service_provider|
            path_owners(sp_path,perspective_service_provider) do |path_owner|
              segment_service_providers(sp_path) do |segment_service_provider|
                sites(sp_path) do |site|
                  circuit_types(sp_path) do |circuit_type|
                    provisioning_states(sp_path) do |provisioning_state|
                      ordering_states(sp_path) do |ordering_state|
                        monitoring_states(sp_path) do |monitoring_state|
                          statements_of_work(sp_path) do |statement_of_work|
                            oems(sp_path) do |oem|
                              p_dashboard_incr( perspective_service_provider,
                                              path_owner,
                                              {"segment.service.providers" => segment_service_provider},
                                              {"sites" => site},
                                              {"circuit.types" => circuit_type},
                                              {"provision.states" => provisioning_state},
                                              {"ordering.states" => ordering_state},
                                              {"monitoring.states" => monitoring_state},
                                              {"statements.of.work" => statement_of_work},
                                              {"oems" => oem})
                            end
                          end
                        end
                      end
                    end
                  end
                end
              end
            end
          end
        end
      end
      
      e_dashboard_incr
    end
    
    def self.p_dashboard_incr(*key_parts)
      @p_dashboard_incr ||= []
      @p_dashboard_incr << key_parts
      if @p_dashboard_incr.size >= PIPELINE_SIZE
        e_dashboard_incr
      end
    end
    
    def self.e_dashboard_incr
      redis.pipelined do
        @p_dashboard_incr.each do |kp|
          dashboard_incr(*kp)
        end
        @p_dashboard_incr = []
      end
    end
    
    def self.dashboard_incr(*key_parts)
      just_values = key_parts.map do |k|
        if k.respond_to?(:values)
          k.values.first
        else
          k
        end
      end
      incr(*["dashboard"]+just_values+["paths.count"])
      
      key_parts.each_with_index do |k,i|
        if k.respond_to?(:values) && k.values.first != "all"
          sadd(*["dashboard"]+just_values.first(i)+["all"]+just_values[i+1,just_values.size]+[k.keys.first,k.values.first])
        end
      end
    end
    
    def self.clear_previous_keys
      while key = redis.spop("dashboard_keys")
        redis.del key
      end
    end
  end
end
