
# Log the date and time
MAX_LOG_SIZE = 10*1024*1024
class FormatLogger < Logger
  def format_message(severity, timestamp, progname, msg)
    "#{timestamp.to_s} #{severity} #{msg}\n"
  end
end


class AutomatedBuildCenter
  QUEUE = "cid.task_queue"
  SUPPORTED_ACCESS_PROVIDER = %w(TWC ATT Comcast Towercloud Phonoscope Zayo Dukenet CoStreet Fiberlight Charter Cox Bluebird Sprint\ MW)
  SUPPORTED_SITE_TYPE = %w(DNR ETH RS SN RN MH)
  MICROWAVE_SITE_TYPES = %w(RS SN RN MH)
  REDIS_IN_PROGRESS_KEY = "circuits.in_progress"
  BUILD_LOG_SEP = ("="*80)
  
  #Ensure the rabbitmq server is configured
  osl_app = Eve::Application.new(:config => "#{Rails.root}/config/osl.poe.yml")
  OslApp.eve = osl_app
  OslApp.eve.run([])
  osl_app.cmd("run #{Rails.root}/config/abc_config.cmd")
  
  @@grid_app = Eve::Application.new(:config => "#{Rails.root}/config/grid.yml", :observer => true)
  GridApp.eve = @@grid_app
  GridApp.eve.run([])
  
  @@standalone = false
    
  attr_accessor :poe_dir  
  
  def initialize
    @osl_output = false
    @poe_dir = Rails.root.to_s + "/tmp/poe"
    Dir::mkdir(Rails.root.to_s + "/tmp") unless File.exists?(Rails.root.to_s + "/tmp") 
    Dir::mkdir(@poe_dir) unless File.exists?(@poe_dir) 
    @build_details = ""
  end
  
  def self.standalone(stand_alone = false)
    @@standalone = stand_alone
  end
  
  def self.abc_root
    case Rails.env
    when "development" then "#{Rails.root}/abc_dev/"
    else "#{Rails.root}/abc_dev/"
    end
  end
  
  def self.daemon_init
    $first_alarm_time = Time.parse("july 3rd 00:00 utc")
    load "db/fake_go_in_service.rb"
    logger = FormatLogger.new("#{Rails.root}/log/#{File.basename(__FILE__, '.*')}.log", 1, MAX_LOG_SIZE)
    Rails.logger = logger # Give logger access to all ruby objects
    Rails.logger.level = (Rails.env == "development") ? Logger::Severity::DEBUG : Logger::Severity::INFO

    # Don't log SQL queries
    ActiveRecord::Base.logger = nil
    ActiveRecord::Base.clear_active_connections!
    Rails.logger.info "Starting daemon"
  end
  
  def builder_new_cid(osl_app, cid, grid_circuit_id) 
    start_time = Time.now
    @build_details = "\nTime Started #{start_time.utc}"
    @summary = "Failed"
    @summary_detail = "" # Should be a one line summary of the failure
    
    @poes = []   
    Rails.logger.info "Received CID #{cid}:#{grid_circuit_id}"

    begin
      if cid[/TEST/] || cid[/-DELTA/]
        # Ignore any test and delta cascades but still want a log
        @summary = "Ignored"
        set_summary_detail_and_details("Circuit #{cid} is a test/delta circuit. No action taken")
      else 
        # Ensure the connection to the database is up in case there bas been no request for several hours
        ActiveRecord::Base.verify_active_connections!  
        ActiveRecord::Base.transaction(:requires_new => true) do
          osl_app.cmd("reset-sql-cache", @osl_output) # Clear out the cache as the ports gets added
        
          # record what is being currently built
          osl_app.redis.sadd(osl_app.lookup_id(REDIS_IN_PROGRESS_KEY), cid)

          # initialize for non-microwave cases
          decoded_ref_cid = nil
          ref_playbook_data = nil

          if ctv = CosTestVector.find_by_circuit_id(cid)
            set_summary_detail_and_details("Circuit #{cid} already exists. No action taken")
            @summary = "Ok"
          elsif !(decoded_cid = CircuitIdFormat::SpirentSprint.decode(cid))
            set_summary_detail_and_details("Invalid CID format #{cid}")
          elsif !validate_circuit_id(cid)
            # error is reported inside 'validate_circuit_id'
          elsif !(playbook_data = PlayBook.data(decoded_cid[:site_name]))
            set_summary_detail_and_details("Could not find Cascade #{decoded_cid[:site_name]} in playbook")
          elsif !((process_cid, process_reason = process_circuit?(decoded_cid, playbook_data))[0])
            if !handle_manual_built_circuits(cid, grid_circuit_id, playbook_data) # This call is a hack it should be removed eventually
              set_summary_detail_and_details("#{cid}: #{process_reason}")
              @summary = "Not Currently Supported"
            end
          elsif playbook_data[:access_provider] == "Sprint MW" && !MICROWAVE_SITE_TYPES.include?(decoded_cid[:site_type])
            set_summary_detail_and_details("Circuit #{cid}: AAV is #{playbook_data[:access_provider]} but site type #{decoded_cid[:site_type]} is not a MW site type")
          elsif MICROWAVE_SITE_TYPES.include?(decoded_cid[:site_type]) && playbook_data[:access_provider] != "Sprint MW"
            set_summary_detail_and_details("Circuit #{cid}: site type #{decoded_cid[:site_type]} is a MW site type but AAV #{playbook_data[:access_provider]} when it should be Sprint MW")
          elsif !(spirent_data = get_spirent_data(grid_circuit_id, cid))
            # error is reported inside 'get_spirent_data'
          elsif ( !spirent_data["Ref_Circuit_Id"] || spirent_data["Ref_Circuit_Id"].empty? ) && MICROWAVE_SITE_TYPES.include?(decoded_cid[:site_type])
            set_summary_detail_and_details("Circuit #{cid} is missing its reference circuit ID in the spirent data.")
          elsif !(ref_ctv = CosTestVector.find_by_circuit_id(spirent_data["Ref_Circuit_Id"])) && MICROWAVE_SITE_TYPES.include?(decoded_cid[:site_type])
            set_summary_detail_and_details("Circuit #{cid}: reference circuit (#{spirent_data["Ref_Circuit_Id"]}) is not built in CDB.")
          elsif MICROWAVE_SITE_TYPES.include?(decoded_cid[:site_type]) && !(decoded_ref_cid = CircuitIdFormat::SpirentSprint.decode(ref_ctv.circuit_id))
            set_summary_detail_and_details("Reference CTV: Invalid CID format #{ref_ctv.circuit_id}")
          elsif MICROWAVE_SITE_TYPES.include?(decoded_cid[:site_type]) && decoded_ref_cid[:site_name] != playbook_data[:donor]
            set_summary_detail_and_details("Reference Cascade (#{decoded_ref_cid[:site_name]}) in decoded circuit_id from grid topology does not match Donor Cascade (#{playbook_data[:donor]})")
          elsif MICROWAVE_SITE_TYPES.include?(decoded_cid[:site_type]) && !(ref_playbook_data = PlayBook.data(decoded_ref_cid[:site_name]))
            set_summary_detail_and_details("Could not find Reference Cascade #{decoded_ref_cid[:site_name]} in playbook")
          else
            # microwave does not build sites or ennis !
            msc_built = false
            site = Site.find_by_name(playbook_data[:msc] + " [#{playbook_data[:msc_clli]}]")
            if !site && !ref_playbook_data
              # the MSC does not exist
              msc_built = create_msc_site(osl_app, cid, playbook_data)
              site = Site.find_by_name(playbook_data[:msc] + " [#{playbook_data[:msc_clli]}]")
              site = nil unless msc_built # catch the case where the MSC build failed but the MSC was actually created!!
            end

            if site || ref_playbook_data
              if ref_playbook_data || add_ennis(playbook_data[:access_provider], site)
                create_circuit(osl_app, cid, grid_circuit_id, spirent_data, playbook_data, ref_playbook_data, decoded_ref_cid)
              else
                set_summary_detail_and_details("Failed to create new ENNIs for #{site.name}")
              end
            else
              set_summary_detail_and_details("OSL built MSC '#{playbook_data[:msc]} [#{playbook_data[:msc_clli]}]' successfully but it is not in CDB") if msc_built
            end
          end

          # If the build failed throw away all the database changes
          raise ActiveRecord::Rollback if @summary != "Ok"
        end
      end
    rescue
      @build_details << "\nException occurred when processing #{cid} - Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}"
      @summary_detail = "Exception occurred when processing #{cid} - Exception is #{$!}"
      SW_ERR "Exception occurred when processing #{cid} - Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}", :IGNORE_IF_TEST
    end
    
    @build_details << "\nTime Ended #{Time.now.utc}\nTotal Build time #{Time.now-start_time} seconds"
    osl_app.redis.del(osl_app.lookup_id(REDIS_IN_PROGRESS_KEY), cid)
        
    log_poes(@poes)
    delete_poes(@poes)
    update_build_logs(cid, grid_circuit_id)
    Rails.logger.info "End of cid new: Total time:#{Time.now-start_time}"
    return @summary, @build_details
  end

  def self.current(osl_app)
    osl_app.redis.smembers(osl_app.lookup_id(REDIS_IN_PROGRESS_KEY))
  end
  
  def self.pending(osl_app)
    circuits = osl_app.cmd("sub ls #{AutomatedBuildCenter::QUEUE}", @osl_output)[:data]
    circuits = [] unless circuits
    # remove any commands that are not "cid new" and strip "cid new"
    circuits = circuits.reject {|cmd| !cmd[/cid new/]}.compact
    circuits.collect! {|cmd| cmd.gsub(/^cid new /, "")}
  end
  
  def reset(osl_app)
    # Clear out anything stored in redis
    osl_app.redis.del(osl_app.lookup_id(REDIS_IN_PROGRESS_KEY))
    # Clear out any old poe files
    poes = Dir.glob(poe_dir + "/#{poe_filename("*")}")
    delete_poes(poes)
  end
  
  def poe_filename(cid)
    "#{cid}_poe.csv"
  end
  
  def process_circuit?(decoded_cid, playbook_data)
    # This code will eventually disappear when we support everything    
    result = false
    reason = ""
    if (SUPPORTED_ACCESS_PROVIDER.include?(playbook_data[:access_provider]))
      if SUPPORTED_SITE_TYPE.include?(decoded_cid[:site_type])
        result = true
      else
        reason = "'#{decoded_cid[:site_type]}' is not a supported site type (#{SUPPORTED_SITE_TYPE.join(",")})" unless result
      end
    else
      reason = "'#{playbook_data[:access_provider]}' is not a supported access_provider (#{SUPPORTED_ACCESS_PROVIDER.join(",")})"  
    end
    return result, reason
  end
  
  private
  
  def handle_manual_built_circuits(cid, grid_circuit_id, playbook_data)
    # This is a hack should be removed ecentually - This is used to add circuits to manually built circuits that are not currently 
    # supported via OSL. This simply adds a CTV is one alreday exists for a cascade.....
    circuit_type = nil
    decoded_cid = CircuitIdFormat::SpirentSprint.decode(cid)
    
    # The following code is to support any circuits that happen to be on the rings which were created manually
    if CosTestVector.find(:first, :conditions => dual_ctv_query(decoded_cid))
      circuit_type = "dual"
    elsif CosTestVector.find(:first, :conditions => ptp_mpt_ctv_query(decoded_cid))
      circuit_type = "ptp"
    end
        
    if circuit_type
      @build_details << "\nHandling manually built topology"
      spirent_data = get_spirent_data(grid_circuit_id, cid)
      if spirent_data
        create_ctv(cid, grid_circuit_id, nil, spirent_data, circuit_type)
      end
    end
    
    return circuit_type ? true : false
  end
    
  
  def create_msc_site(osl_app, cid, playbook_data)
    # Make a POE for the MSC and create it
    result = false
    Rails.logger.info "Building new msc #{cid}"
    @build_details << "\nMSC #{playbook_data[:msc_clli]} does not exist - Attempting to build"
    msc_data = MscSite.data(playbook_data[:msc_clli])
    unless msc_data
      set_summary_detail_and_details("MSC (#{playbook_data[:msc_clli]}) could not be found in the Sat Sites file")
      return false
    end
    poe = make_msc_poe(cid, msc_data)
    @poes << poe
    result = osl_app.cmd("poe parse-again #{poe}", @osl_output, true)[:data]
    result, result_details = process_poe_result(result)
    
    if result
      @build_details << "\nOSL successfully built MSC #{playbook_data[:msc_clli]}"
    else
      set_summary_detail_and_details("OSL build failed for MSC #{playbook_data[:msc_clli]}")
      if !result_details.blank?      
        @build_details << "\nResult: #{result_details}" 
        @summary_detail << " Result: #{result_details}"
      end
    end
    return result
  end
  
  def create_circuit(osl_app, cid, grid_circuit_id, spirent_data, playbook_data, ref_playbook_data, decoded_ref_cid)
    if spirent_data
      decoded_cid = CircuitIdFormat::SpirentSprint.decode(cid)
      if ref_playbook_data
        if decoded_ref_cid[:site_type] == "DNR"
          circuit_type = circuit_type(decoded_ref_cid, ref_playbook_data)
          circuit_type = case circuit_type
          when "mpt" then "mwmpt"
          when "ptp", "dual" then "mw"
          else 
            set_summary_detail_and_details("Reference circuit's Type is unknown")
            ""
          end
        else
          set_summary_detail_and_details("Reference circuit is not DNR")
          circuit_type = ""
        end
      else 
        circuit_type = circuit_type(decoded_cid, playbook_data)
      end
    
      case circuit_type
      when "dual"
        if CosTestVector.find(:first, :conditions => ptp_mpt_ctv_query(decoded_cid)).nil?
          create_circuit_poe(osl_app, cid, grid_circuit_id, playbook_data, ref_playbook_data, spirent_data, "ptp_dual")
        elsif CosTestVector.find(:first, :conditions => dual_ctv_query(decoded_cid)).nil?
          create_circuit_poe(osl_app, cid, grid_circuit_id, playbook_data, ref_playbook_data, spirent_data, "dual")
        else
          create_ctv(cid, grid_circuit_id, playbook_data, spirent_data, "dual")
        end
      when "ptp", "mpt", "mw", "mwmpt"
        if CosTestVector.find(:first, :conditions => ptp_mpt_ctv_query(decoded_cid)).nil?
          # There is no existing Circuit for the cascade - Create a circuit
          create_circuit_poe(osl_app, cid, grid_circuit_id, playbook_data, ref_playbook_data, spirent_data, circuit_type)
        else
          # There is an existing cascade id - Need to create the correct CosTestVector on correct Ovc/segment_end_point/cos_end_point
          create_ctv(cid, grid_circuit_id, playbook_data, spirent_data, circuit_type)
        end
      end
    end
  end
  
  def create_circuit_poe(osl_app, cid, grid_circuit_id, playbook_data, ref_playbook_data, spirent_data, circuit_type)
    result = false
    decoded_cid = CircuitIdFormat::SpirentSprint.decode(cid)
    @build_details << "\nNo existing Circuits for cascade #{decoded_cid[:site_name]}. Attempting to build Circuit"
    Rails.logger.info "Building new circuit #{cid}"
    poe = make_circuit_poe(osl_app, cid, grid_circuit_id, playbook_data, ref_playbook_data, spirent_data, circuit_type)
    @poes << poe
    #1st true show messages 2nd Log the call
    result = osl_app.cmd("poe parse-again #{poe}" ,@osl_output, true)[:data]

    result, result_details = process_poe_result(result)
    if !result
      set_summary_detail_and_details("OSL build failed for #{cid}")
      if !result_details.blank?      
        @build_details << "\nResult: #{result_details}" 
        @summary_detail << " Result: #{result_details}"
      end
    else
      @build_details << "\nOSL successfully built circuit"    
      # Double check the CTV was created
      ctv = CosTestVector.find_by_circuit_id(cid)
      if !ctv
        set_summary_detail_and_details("Failed to find CosTestVector id #{cid}! OSL Built but can't find the CTV in the database") if result
      else           
        new_path = ctv.cos_end_point.segment_end_point.segment.paths.first
        if make_circuit_live(new_path)
          if ctv.alarm_sync_past
            @summary = "Ok"
            result = true
          else
            set_summary_detail_and_details("Failed to sync the availability history")
          end
        else
          set_summary_detail_and_details("Failed to bring the Circuit to a live state")
        end
      end
    end  
    result
  end
  
  def create_ctv(cid, grid_circuit_id, playbook_data, spirent_data, circuit_type)
    result = false
    Rails.logger.info "Building new CTV #{cid}"
    decoded_cid = CircuitIdFormat::SpirentSprint.decode(cid)
    @build_details << "\nA Circuit already exists for cascade #{decoded_cid[:site_name]}. Attempting to add a new CostTestVector"
    
    target_address = fix_mac_address_format(spirent_data["Target_Addr"])    
       
    if circuit_type == "dual"
      ctv = CosTestVector.find(:first, :conditions => dual_ctv_query(decoded_cid))
    else
      ctv = CosTestVector.find(:first, :conditions => ptp_mpt_ctv_query(decoded_cid))
    end
       
    new_ctv = ctv.dup
    new_ctv.circuit_id = cid
    new_ctv.grid_circuit_id = grid_circuit_id
    new_ctv.loopback_address = target_address
    new_ctv.event_record = nil
    new_ctv.event_record_create
    if new_ctv.save
      new_ctv.reload
      if new_ctv.go_in_service[:result]
        if new_ctv.alarm_sync_past
          @summary = "Ok"
          result = true
        else
          set_summary_detail_and_details("Failed to sync the availability history")
        end
      else
        set_summary_detail_and_details("Failed to put CTV #{cid} in-service")
      end
    else
      set_summary_detail_and_details("Failed to duplicate a CosTestVector #{cid} #{new_ctv.errors.full_messages}")
      @build_details << "\n#{new_ctv.errors.inspect}"
    end
    return result
  end
  
  def make_circuit_poe(osl_app, cid, grid_circuit_id, playbook_data, ref_playbook_data, spirent_data, circuit_type) 
    decoded_cid = CircuitIdFormat::SpirentSprint.decode(cid)
    if circuit_type == "dual" || circuit_type == "ptp_dual"
      decoded_cid[:site_name] += "-#{decoded_cid[:p_b]}"
      circuit_type = "ptp" if circuit_type == "ptp_dual"
    end
  
    nni_alternate = ""
    node_alternate = ""

    if circuit_type == "mpt"
      nni_primary = "NNI-1 Primary"
      nni_alternate = "NNI-1 Alternate"
      node_primary = "IPA01"
      node_alternate = "IPA02"
    else      
      if decoded_cid[:backup]
        nni_primary = "NNI-1 Alternate"
        node_primary = "IPA02"
      else
        nni_primary = "NNI-1 Primary"
        node_primary = "IPA01"
      end
    end
    
    osl_app.cmd("cm set-status #{decoded_cid[:site_name]} unbuilt", @osl_output) # Force OSL to always attempt to build the circuit

    # handle microwave stuff
    if ref_playbook_data
      leased_access_provider = ref_playbook_data[:access_provider]
    else
      leased_access_provider = playbook_data[:access_provider]
    end

    target_address = fix_mac_address_format(spirent_data["Target_Addr"])    
    data = {
      "Market" => playbook_data[:market],
      "Cascade" => decoded_cid[:site_name],
      "Cell Site type" => decoded_cid[:site_type],
      "Lat" => playbook_data[:lat],
      "Long" => playbook_data[:long],
      "Site Name" => playbook_data[:site_name],
      "Address 1" => playbook_data[:address_1],
      "Address 2" => "",
      "City" => playbook_data[:city],
      "State" => playbook_data[:state],
      "Zip" => playbook_data[:zip], 
      "Approved Vendor" => playbook_data[:oem],
      "MSC Homing" => playbook_data[:msc] + " [#{playbook_data[:msc_clli]}]",
      "Leased Access Provider" => leased_access_provider,
      "NNI Network Homing" => "HARDCODE",
      "Deployment CIR" => decoded_cid[:cir].to_i.to_s,
      "Point to Point or Point-MultiPoint" => circuit_type,
      "NNI Mapping - Primary" => nni_primary,
      "NNI Mapping - Alternate" => nni_alternate,
      "Node - Primary" => node_primary,
      "Node - Alternate" => node_alternate,
      "AT&T CNO" => "",
      "BAN" => "",
      "VTA" => "",
      "PNUM" => "",
      "Target Address" => target_address,
      "VLAN" => "TBD",
      "CWD" => "",
      "BH Bearer VLAN" => "",
      "BH OAMP VLAN" => "",
      "BH ETH OAM VLAN" => decoded_cid[:vlan_id],
      "MEPIDS" =>  "TBD",
      "MEG Levels" => "TBD",
      "CSR AAV Port" => "TBD",
      "CSR Device Type" => "??????", # Will be based on a mapping based on PB[Cascade].OEM 
      "Monitoring Reflection Mechanism" => spirent_data["Coll_Type"],
      "Period" => spirent_data["Period"],
      "ToS" => spirent_data["ToS"],
      "Circuit ID" => cid,
      "REF CIRCUIT ID" => spirent_data["Ref_Circuit_Id"], # microwave sites
      "AAV Circuit ID" => "TBD", # Should be ""
      "Interconnection" => "", # Newer version of playbook will have playbook_data[:interconnection], 
      "Site Connected List" => "", # Newer version of playbook will have playbook_data[:site_connected_list]
      "Grid Circuit ID" => grid_circuit_id
    }
    
    outfile = ""
    csv_out = CSV.new(outfile)
    csv_out << data.keys
    csv_out << data.values
    
    File.open("#{poe_dir}/#{poe_filename(cid)}", 'w') {|f| f.write(outfile)}
    return "#{poe_dir}/#{poe_filename(cid)}"
  end
   
  def make_msc_poe(cid, msc_data)
    template = "sprint_msc_#{msc_data[:oem]}_oem_site".downcase
    data = {
      "template" => template,
      "MSC CLLI" =>	msc_data[:msc_clli],
      "MSC" => msc_data[:msc],
      "Address" => msc_data[:address],
      "City" => msc_data[:city],
      "State" => msc_data[:state_],
      "Zip" => msc_data[:zip],	
      "Lat" => msc_data[:lat],
      "Long" => msc_data[:long],
      "OEM" => msc_data[:oem],
      "Qscope Name (1)" => msc_data[:qscope_name_1],
      "Qscope name (2) (if applic)" => msc_data[:qscope_name_2_if_applic],
      "Module 1 Port 1" => msc_data[:module_1_port_1],
      "Module 1 Port 2" => msc_data[:module_1_port_2],
      "Module 2 Port 1" => msc_data[:module_2_port_1],
      "Markets" => msc_data[:markets]
    }
     
    outfile = ""
    csv_out = CSV.new(outfile)
    csv_out << data.keys
    csv_out << data.values

    File.open("#{poe_dir}/#{poe_filename(msc_data[:msc_clli])}", 'w') {|f| f.write(outfile)}
    return "#{poe_dir}/#{poe_filename(msc_data[:msc_clli])}"
  end
  
  public
   def validate_circuit_id(cid)
    errors = [] 
    valid = CircuitIdFormat::SpirentSprint.valid(cid, errors)
    if !valid
      set_summary_detail_and_details("#{cid} is not valid. #{errors.join(", ")}")
    end    
    return valid    
  end
     
  def circuit_type(decoded_cid, playbook_data) 
    circuit_type = case playbook_data[:access_provider]
      when "ATT", "Phonoscope"
        case decoded_cid[:site_type]
          when "ETH" then "mpt"
          when "DNR" then "ptp"
          else
            set_summary_detail_and_details("Circuit type cannot be determined unknown site type #{decoded_cid[:site_type]}")
            ""
        end
      when "TWC", "Comcast", "Towercloud", "Zayo", "Dukenet", "CoStreet", "Fiberlight", "Charter", "Cox", "Bluebird" then "dual"
      else
        set_summary_detail_and_details("Circuit type cannot be determined unknown provider #{playbook_data[:access_provider]}")
        ""
    end
    
    return circuit_type
  end
  
  
  def add_ennis(service_provider, site)
    created = false
    # Check if Enni exists
    ennis = site.ennis.joins(:member_handles).find(:all, :conditions => ["`value` LIKE ?","NNI-% [#{service_provider}]"])
    if ennis.size == 0  
      puts "add_ennis: service_provider = #{service_provider}" 
      # Allocate a port
      nodes = site.nodes.reject {|node| !node.name[/IPA/]}
      allocated_ports = nodes.collect do |node|
        ports = node.ports.where({:connected_port_id => nil, :demarc_id => nil}).where("`name` LIKE ?", "%/%/%")
        if !ports.blank? && ports.size >= 1
          {:node => node, :port => ports.first}
        else
          nil
        end
      end.compact

      if allocated_ports.blank? || allocated_ports.size != 2
        @build_details << "\nFailed to allocate 2 ports on site #{site.name}. Nodes #{nodes.collect{|n| n.name}.join(", ")}"
      else
        sp = ServiceProvider.find_by_name(service_provider)
        sprint_sp = ServiceProvider.find_by_name("Sprint") 
        if !sp || !sprint_sp
          @build_details << "\nFailed to find ServiceProvider #{service_provider}" if !sp
          @build_details << "\nFailed to find ServiceProvider Sprint" if !sprint_sp
        else
          on = sp.operator_networks.where("`name` LIKE '% US Network'").first
          if on.blank?
            @build_details << "\nFailed to find OperatorNetwork #{service_provider} US Network"
          else
            demarc_type = on.operator_network_type.demarc_types.where(:name => "ENNI")  
            if demarc_type.blank? || demarc_type.size != 1
              @build_details << "\nFailed to find Demarc Type of ENNI for OpertorNetwork #{on.name}"
            else
              h = {
               "cenx_demarc_type"=>"ENNI",
               "operator_network_id"=>on.id,
               "physical_medium"=>"10GigE LR; 1310nm; SMF",
               "demarc_type_id"=> demarc_type.first.id,
               "ether_type"=>"0x8100",
               "protection_type"=>"unprotected",
               "fiber_handoff_type"=>"SC",
               "auto_negotiate"=>false,
               "ah_supported"=>true,
               "emergency_contact_id"=> sp.contacts.first(:conditions => ["`name` LIKE ?", "%Network Operations"]).id,
               "port_enap_type"=>"QINQ",
               "cir_limit"=>100,
               "eir_limit"=>300,
               "demarc_icon"=>"enni",
               "near_side_clli"=>"TBD",
               "far_side_clli"=>"TBD",
               "latitude"=>site.latitude,
               "longitude"=>site.longitude,
               "site_id"=>site.id}

              enni_primary = EnniNew.create(h)
              enni_alternate = EnniNew.create(h)

              allocated_ports.each do |ap|
                node = ap[:node]
                port = ap[:port]
                m_attr = MemberAttr.find_by_name("MemberHandle for ENNI/Demarc")
                if node.name[/-IPA01/] || node.name[/IPA-01/]
                  mh = enni_primary.member_handles.find {|p| p.owner == sprint_sp}        
                  mh.value = "NNI-1 Primary [#{service_provider}]"
                  mh.save!
                  port.demarc = enni_primary
                  port.save!
                elsif node.name[/-IPA02/] || node.name[/IPA-02/]
                  mh = enni_alternate.member_handles.find {|p| p.owner == sprint_sp}        
                  mh.value = "NNI-1 Alternate [#{service_provider}]"         
                  mh.save!
                  port.demarc = enni_alternate
                  port.save!
                end
              end
              created = true
            end
          end
        end
      end
    else
      @build_details << "\nSite #{site.name} already has correct Ennis"
      created = true
    end
    return created
  end
     
  def get_spirent_data(grid_circuit_id, cid)
    if !@@standalone
      spirent_data = @@grid_app.cmd("gci get-topology #{grid_circuit_id}", @osl_output)[:data]
      if spirent_data
        matching_cid = @@grid_app.cmd("gci e #{grid_circuit_id}", @osl_output)[:data]
        if !(matching_cid && matching_cid == cid)
          set_summary_detail_and_details("The corresponding Circuit Id for Grid id #{grid_circuit_id} does not match the one requested Expected:#{cid} Got:#{matching_cid}")
          spirent_data = nil
        end
      else
        set_summary_detail_and_details("Cannot find Spirent topology data")
      end
    else
      # This is for development testing only
      csv_spirent_data = SingleSpirentInfo.data(cid)
      spirent_data = {}
      if csv_spirent_data
        # map the hash to correct keys
        {:network_element => "Network_Element", :coll_type => "Coll_Type", :service_level => "Service_Level",
         :tos => "ToS", :target_addr => "Target_Addr", :aav => "AAV", :aav_circuit => "AAV_Circuit", :period => "Period",
         :oem_vendor => "OEM_Vendor", :ref_circuit_id => "Ref_Circuit_Id", :evc_topology_type => "Evc_Topology_Type", :csr_id => "CSR_Id",
         :secondary_ckt_id => "Secondary_Circuit_Id"}.each do |csv_name, real_name|
           spirent_data[real_name] = csv_spirent_data[csv_name]
         end
       else
        set_summary_detail_and_details("Circuit #{cid} is missing from the spirent data (development testing!).")
       end
     end
     
     # make sure we have all the data
     if spirent_data
       expected_keys = %w(Network_Element Coll_Type Service_Level ToS Target_Addr AAV AAV_Circuit Period OEM_Vendor Ref_Circuit_Id Evc_Topology_Type CSR_Id Secondary_Circuit_Id)       
       if spirent_data.blank? || !(expected_keys-(spirent_data.keys)).empty? || !((spirent_data.keys)-expected_keys).empty?
         set_summary_detail_and_details("Missing Spirent topology data for #{grid_circuit_id} #{spirent_data}")
         spirent_data = nil
      end
    end
    return spirent_data
  end

  def update_build_logs(cid, grid_id)
    # Set summary to be the latest result but append to the details
    builder_log = BuilderLog.find_or_create_by_name(cid)
    builder_log.grid_id = grid_id
    builder_log.summary = @summary
    builder_log.summary_detail = @summary_detail
    log_to_append = @build_details + "\n" + BUILD_LOG_SEP + "\n"
    if "#{builder_log.details}".size + log_to_append.size >= BuilderLog::MAX_SIZE
      # Truncate - Could actually remove previous build details until enough space...TODO later
      builder_log.details = ""
    end
    builder_log.details = "#{builder_log.details}" + log_to_append
    builder_log.save
  end
  
  def make_circuit_live(evc)
    @build_details << "\n"
    StateHelper.evc_live(evc, @build_details)
  end
    
  def delete_poes(poes)
    poes.each do |file|
      begin
        File.delete(file)
      rescue
        @build_details << "\nFailed to delete file #{file}\n#{$!}"
        SW_ERR "Failed to delete file #{file}\n#{$!}"
      end
    end
  end
  
  def log_poes(poes)
    poes.each do |file|
      begin
        @build_details << "\n\nPoe file #{file}:\n" << File.open(file, 'rb') { |f| f.read }
      rescue
        @build_details << "\nFailed to open/read file #{file}\n#{$!}"
      end
    end
  end
  
  def process_poe_result(result)
    # Result can be nil , true/false or a hash[:internal] = ModelMaker::Circuit class   
    result_details = ""
    if result
      if result.is_a?(Hash)
        if result[:internal] == nil
          result = false
        elsif result[:internal].is_a?(ModelMaker::Circuit)
          result_details = result[:internal].error
          result = result[:internal].successful
        else
          result_details = "Invalid return type #{result.inspect}"
          result = false
        end
      elsif result.is_a?(TrueClass)
        # True is valid - nothing to do
      else
        result_details = "Invalid return type #{result.inspect}"
        result = false
      end
    end    
    
    return result, result_details
  end
  
  def fix_mac_address_format(mac_address)
    # ALU does not add : to the Target address
    mac_address[/:/] ? mac_address : mac_address.scan(/.{1,2}/).join(":")  # ALU does not add : to the Target address
  end
  
  def set_summary_detail_and_details(log_data)
    @build_details << "\n" + (@summary_detail = log_data)
  end
  
  def dual_ctv_query(decoded_cid)
    ["circuit_id LIKE ?", "#{decoded_cid[:site_name]}-#{decoded_cid[:site_type]}-#{decoded_cid[:p_b]}-%"]
  end
  
  def ptp_mpt_ctv_query(decoded_cid)
    ["circuit_id LIKE ?", "#{decoded_cid[:site_name]}-#{decoded_cid[:site_type]}-%"]
  end
  
    
end