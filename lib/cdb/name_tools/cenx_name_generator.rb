module CenxNameTools

class CenxNameHelper
    
  def self.generate_type_string object
    if object.instance_of? EnniNew
      if object.is_connected_to_monitoring_port
        return "SMIF"
      elsif object.is_connected_to_trouble_shooting_port
        return "TIF"
      else
        return "ENNI"
      end
    elsif object.kind_of? Demarc
      return object.cenx_demarc_type
#    elsif object.kind_of? Segment
#      return object.type
    elsif object.instance_of? OnNetOvc
      return "OnOvc"
    elsif object.instance_of? OnNetRouter
      return "OnRouter"
    elsif object.instance_of? OffNetOvc
      return "OffOvc"
    elsif object.instance_of? Evc
      return "Evc"
    elsif object.instance_of? IpFlow
      return "IpFlow"
    elsif object.instance_of? OnNetOvcEndPointEnni
      return "OnOvceEnni"
    elsif object.instance_of? OvcEndPointEnni
      return "OffOvceEnni"
    elsif object.instance_of? OvcEndPointUni
      return "OffOvceUni"
    elsif object.instance_of? OnNetRouterSubIf
      return "OnRouterEP"
#    elsif object.kind_of? SegmentEndPoint
#      return object.type
    else
      SW_ERR("generate_type_string: Invalid Object Type #{object.class.to_s}")
      return "TypeERROR"
    end
  end

  def self.decode_type_string cenx_name
    result = true
    result_string = ''
    s = StringScanner.new(cenx_name)
    type = s.scan(/\w+/)

    case type
      when "ENNI", "UNI", "SMIF", "TIF"
        type_string = "demarc"
        table = Demarc
      when "Evc", "IpFlow"
        type_string = "path"
        table = Path
      when "OnOvc","OffOvc", "OnRouter"
        type_string = "segment"
        table = Segment
      when "OnOvceEnni", "OffOvceEnni", "OffOvceUni", "OnRouterEP"
        type_string = "segmentep"
        table = SegmentEndPoint
      else
        type_string = ""
        result_string += "* invalid type: #{type};\n"
        result = false
    end
    return result, result_string, type_string, table
  end

  def self.shorten_name string
    # return "" if string.nil?
    short_str = ""
    
    caps_string = string.upcase
    if  MemberMappings::NAME_TO_SHORT_NAME[caps_string]!= nil
      short_str = MemberMappings::NAME_TO_SHORT_NAME[caps_string]
      return short_str
    end

    words = string.split

    words.each do |s|
      if words.size == 1 && s.size < 7
        short_str += s
      else
        short_str += s.strip.first.capitalize
        if words.size == 1
          short_str += s.strip.last
        end
      end
    end
    return short_str
  end

  def self.shorten_phy_type string
    short_str = ''
    if string == nil
      short_str = 'TBD'
    elsif string.starts_with?('10GigE')
      short_str = '10G'
    elsif string.starts_with?('10/100/1000')
      short_str = '10/100/1000'
    elsif string.starts_with?('10/100')
      short_str = '10/100'
    elsif string.starts_with?('1000Base')
      short_str = '1G'
    elsif string.match(/\A\d*[a-zA-Z]{0,2}Base/)
      short_str = string[0..string.index("Base")-1]
    elsif string.starts_with?('N/A')
      short_str = 'N/A'
    elsif string.starts_with?('100M')
      short_str = '100M'
    elsif string.starts_with?('L3')
      short_str = 'L3'
    else
      short_str = 'Unknown-Phy!'
    end
    return short_str
  end

  def self.shorten_protection_type protection_type
    if protection_type == 'unprotected-LAG'
      return 'unp-Lag'
    elsif protection_type
      return "#{protection_type[0,3]}"
    end
    return 'TBD'
  end

end

end # module CenxNameTools
