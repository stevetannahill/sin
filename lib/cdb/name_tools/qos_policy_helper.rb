class QosPolicyHelper
  CURRENT_VERSION = 'a'
  #Since both egress and ingress are stored in a Cos Endpoint but a policy is
  #only concerned with one direction and since both directions are very similar
  #this structure helps with the minute differences and allows one piece of code
  #below. More specifically these are the methods used to gather in the information
  # name: The full name of the direction
  # variables: the names of the variables in the cosep for that direction
  # conversion: the variables that need to be converted to and from mbps
  # rate_limiting: the variable used to store the rate limiting mechanism for the direction
  # mapping: The mapping (ingress) or marking (egress) but only called mapping for sanity
  TYPE_HELPER = {
    "E" => {
      :name => "Egress",
      :variables => ["egress_cir_Mbps", "egress_cbs_kB", "egress_eir_Mbps", "egress_ebs_kB"],
      :required => ["egress_cir_Mbps", "egress_eir_Mbps"],
      :rate_limiting => "egress_rate_limiting",
      :mapping => "egress_marking"
      },
    "I" => {
      :name => "Ingress",
      :variables => ["ingress_cir_Mbps", "ingress_cbs_kB", "ingress_eir_Mbps", "ingress_ebs_kB"],
      :required => ["ingress_cir_Mbps", "ingress_eir_Mbps"],
      :rate_limiting => "ingress_rate_limiting",
      :mapping => "ingress_mapping"
      }
  }
  
  #This class is returned by the decode_policy_name function
  class Policy
    attr_reader :name, :version, :direction, :classes_of_service, :policy_primary_key
    attr_writer :policy_primary_key
    def initialize(name, version, direction, classes_of_service)
      @name = name
      @version = version
      @direction = direction
      @policy_primary_key = 0
      #Expand out the '#' special character
      #Get all used mappings
      taken = classes_of_service.collect do |cos|
        cos.mapping.collect{|k,v| v.split(',')}
      end.flatten
      
      taken.delete('#')
      #Remove those mappings from list of all mappings
      map = ("0".."7").to_a - taken
      map = map.join(',')
      #Change the '#' to the new map
      classes_of_service.each do |cos|
        cos.mapping.each do |fc, m|
          if m == '#'
            #we touch things we shouldn't because we have to
            mapping = cos.instance_variable_get("@mapping")
            mapping[fc] = map
            cos.instance_variable_set("@mapping", mapping)
          end
        end
      end
      @classes_of_service = classes_of_service
    end

  end
  
  class ClassOfService
    attr_reader :name, :rlm, :cir_mbps, :cbs_kB, :eir_mbps, :ebs_kB, :mapping
    def initialize(helper, name, rlm, data, mapping)
      @name = name
      @rlm = rlm

      key = helper[:variables]
      @cir_mbps = extract_value data[key[0]]
      @cbs_kB = extract_value data[key[1]]
      @eir_mbps = extract_value data[key[2]]
      @ebs_kB = extract_value data[key[3]]
      @mapping = mapping
    end
    #Allow us to access the properly formatted mbps cir and eir
    def cir_kbps
      extract_value(@cir_mbps * 1000)
    end
    def eir_kbps
      extract_value(@eir_mbps * 1000)
    end

    #This function removes the decimal on round numbers (eg 1000.0 = 1000)
    def extract_value value
      return nil if (value.nil? or (value.is_a? String and value.empty?))
      value = value.to_f if value.is_a? String
      value = value.to_i if value % 1 == 0
      return value
    end

    #This is to get a pretty string of the mapping
    def mapping_str
      str = ""
      mapping.each do |fc, map|
        str += "#{fc} -> #{map}, "
      end
      #Chop off trailing ", "
      return str[0..-3]
    end
  end
  
  def self.decode_policy_name policy_name
    
    return testing_qos_policy($1) if policy_name =~ /Default BV3000 (\w+) policy/
    
    classes_of_service = policy_name.split('/')
    headers = classes_of_service.delete_at(0).split(':')

    #The first section is general information about the policy
    version = headers[0]
    helper = TYPE_HELPER[headers[1]]

    #Get an array of ClassOfService to hold each cos
    classes_of_service.collect! do |section|
      info = section.split(':')
      sname = info.delete_at(0)
      name = fwding_class = nil
      ServiceCategories::ON_NET_OVC_COS_TYPES.each do |cos_name, data|
        if data[:short_name] == sname
          name = cos_name
          fwding_class = data[:fwding_class]
        end
      end
      #Get the rate limiting method
      rlm = info.delete_at(0)
      #Find the rate limiting mechanism's full name
      names = ServiceCategories::RATE_LIMITING_MECHANISM_ACTUAL.select{|s| s[0,1] == rlm }
      if names.size > 1
        SW_ERR "ERROR: RATE_LIMITING_MECHANISM_ACTUAL does not contain a unique match for the value #{rlm}"
      elsif name.empty?
        SW_ERR "ERROR: RATE_LIMITING_MECHANISM_ACTUAL contains no possible matches for the value #{rlm}"
      else
        rlm = names.first
      end

      #This will gather all data regarding the class of service
      data = {}
      helper[:variables].each do |type|
        val = info.delete_at(0)
        val = "" if val == "-"
        data[type] = val
      end

      #The final field of the cos is the mapping or marking (named only mapping for sanity)
      mapping = info.delete_at(0)
      
      ClassOfService.new(helper, name, rlm, data, {fwding_class => mapping})
    end

    Policy.new(policy_name, version, helper[:name], classes_of_service)
  end

  def self.generate_policy_name segmentep, dir

    return "Default BV3000 #{TYPE_HELPER[dir][:name].downcase} policy" if segmentep.is_connected_to_monitoring_port
    
    coseps = segmentep.cos_end_points
    helper = TYPE_HELPER[dir]

    name = CURRENT_VERSION.dup
    name << ':' + dir

    #Gather all the CoS Endpoint information
    info = {}
    coseps.each do |cosep|
      sname = ServiceCategories::ON_NET_OVC_COS_TYPES[cosep.class_of_service_type.name][:short_name]
      type = cosep.send(helper[:rate_limiting])[0,1]
      
      #Get the rate data
      rates = {}
      helper[:variables].each do |attr|
        value = cosep.send(attr).strip
        if value.empty?
          if helper[:required].include? attr
            value = "0"
          else
            value = "-"
          end
        end
        rates[attr] = value
      end

      #Store the rates data in proper order
      data = []
      helper[:variables].each do |k|
        data << rates[k]
      end

      mapping = cosep.send(helper[:mapping])

      info[sname] = {:type => type, :data => data, :mapping => mapping}
    end

    #Handle the special '#' character for ingress mapping
    if dir == "I"
      #Get all the mappings
      maps = info.keys.sort.collect do |key|
        str = info[key][:mapping]
        
        #Mapping can be comma separated, semi-colon separated, or just a string of numbers (eg 123 = [1,2,3])
        if str.include? ","
          str.split(',')
        elsif str.include? ";"
          str.split(';')
        else
          str.split('')
        end
      end
      #Are all values taken?
      if maps.flatten.sort == ('0'..'7').to_a
        #Find largest mapping
        largest = 0
        maps.each_with_index do |map, i|
          largest = i if map.size > maps[largest].size
        end
        #Change it
        maps[largest] = ['#']
        #Set the mappings to the new values
        info.keys.sort.each do |key|
          info[key][:mapping] = maps.delete_at(0).join(',')
        end
      end
    end

    #Add the information to the name (sorted alphabetically by short_name)
    info.keys.sort.each do |sname|
      name << "/"
      name << sname
      name << ':' + info[sname][:type]
      info[sname][:data].each do |attr|
        name << ':' + attr
      end
      name << ':' + info[sname][:mapping]
    end
    return name
  end
  
  private
  
  #This function returns a preset hardcoded QoS Policy for the BV-3000
  def self.testing_qos_policy direction
    name = "Default BV3000 #{direction} policy"
    version = CURRENT_VERSION
    direction = direction.capitalize
    
    #Class of Service Info
    helper = TYPE_HELPER.select { |dir, info| info[:name] == direction }.first[1]
    cos_name = "Rate limit to 1Mbps to prevent accidentally flooding Segment"
    rlm = ServiceCategories::CENX_TEST_COS_VALUES[:rlm][direction]
    values = helper[:variables].collect{|method| ServiceCategories::CENX_TEST_COS_VALUES[:rates][method]}
    #Assign each helper to their value in values
    data = Hash[*helper[:variables].to_a.zip(values).flatten]
    mapping = ServiceCategories::CENX_TEST_COS_VALUES[:mapping]
    
    cos = ClassOfService.new(helper, cos_name, rlm, data, mapping)
    policy = Policy.new(name, version, direction, [cos])
    return policy
  end

end
