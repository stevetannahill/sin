require "rubygems"
require "active_record"
require 'optparse'
#require "net/telnet"
require File.dirname(__FILE__) + "/../../lib/constants/common_types"
require File.dirname(__FILE__) + "/../../lib/constants/common_helpers"
require File.dirname(__FILE__) + "/../../app/models/mtc"
require File.dirname(__FILE__) + "/../../app/models/port"
require File.dirname(__FILE__) + "/../../app/models/node"

if RUBY_PLATFORM =~ /java/ #manual change req'd in database.yml:   adapter: <jdbc>mysql
  # puts"...Java..."
  require "jdbc/mysql" 
else
  # puts"...No Java..."
end

#Establish connection to database
if RUBY_PLATFORM =~ /java/ 
  dbconfig = YAML::load(File.open(File.dirname(__FILE__) + '/../../config/databasej.yml'))
else
  dbconfig = YAML::load(File.open(File.dirname(__FILE__) + '/../../config/database.yml'))
end

ActiveRecord::Base.establish_connection(dbconfig['development'])

def print_target(res)
  puts"==========================="
  puts"Target: #{res.name}"
  attrs = res.attributes
  #p attrs
  attrs.each do |a|
    #puts"#{a.first}: #{a.second}"
    p a
  end
  puts"\n"
end 

def list_target(target)
  res = Node.find_by_name("#{target}")
  if res
    print_target(res)
  else
    puts"Data for all targets"
    resources = Node.all
    resources.each do |res|
      print_target(res)
    end
  end
end


#------------------------ Main -----------------------------#

# This hash will hold all of the options
# parsed from the command-line by
# OptionParser.
options = {}

optparse = OptionParser.new do|opts|
  # Set a banner, displayed at the top
  # of the help screen.
  opts.banner = "Usage: optparse1.rb [options] file1 file2 ..."
  opts.banner = "Usage t <target_name> [options]"

  # Define the options, and what they do
  options[:list] = false
  options[:target] = nil
  opts.on( '-l TARGET', '--list TARGET', 'Output data for target or all' ) do|t|
      options[:list] = true
      options[:target] = t
  end

  # This displays the help screen, all programs are
  # assumed to have this option.
  opts.on( '-h', '--help', 'Display this screen' ) do
    puts opts
    exit
  end
end

#The ! removes parsed stuff from the args
optparse.parse!

if options[:list]
  list_target(options[:target])
end

# target thats left is the one to connect to...
target = ARGV[0]
unless target
  puts"Error: No target specified"
  exit(1)
end

target.chomp

res = Node.find_by_short_name("#{target}")
unless res
  puts"Failed to find target: #{target}."
  exit(1)
end


expectFilename = File.dirname(__FILE__) + '/login.exp'
# puts"Launch expect: #{expectFilename}"
return exec "expect -f #{expectFilename} #{res[:primary_ip]} #{res[:uname]} #{res[:password]} #{target}"













