puts 'This is only for Segment\'s and ENNI\'s only

Restrictions:
  Must be a Segment or Cenx ENNI
  Segment/ENNI cannot have changed Prov state during Mtc period

Be very careful with this script ALWAYS run it in a sandbox and check the results
Run it locally and check the following
- Check the SM hsitory of the Ennis, Path, On Net OVC and all the Endpoints
- Check the Mtc hsitory of the Ennis, Path, On Net OVC and all the Endpoints

After running it in the sandbox (and it passes) then run it normally and
Run it locally and check the following
- Check the SM hsitory of the Path, On Net OVC and all the Endpoints
- Check the Mtc hsitory of the Path, On Net OVC and all the Endpoints
- Generate a SLA graph and verify the mtc period is correct

Usage:
All times are in epoch seconds
SetMtc.segment(segment, start_time, end_time)
SetMtc.enni([enni1, enni2], start_time, end_time)

If you are putting multiple ENNIs into mtc during the same period they affect the same Segments
so in order to correctly apply the maintainace periods you must specify all the ENNIs in an array

The output will indicate what actions are taken and at the end it will dump the whole Provisioning,
Maintaiance and SM histories so you can verify they are correct

Examples:
o = Segment.find_by_service_id 450000
SetMtc.segment(o, Time.parse("8 june 02:00 2011").to_i, Time.parse("8 June 03:00 2011").to_i)

es = [EnniNew.find(36), EnniNew.find(56)]
SetMtc.enni(es,Time.parse("7 june 00:00 2011").to_i, Time.parse("7 June 01:00 2011").to_i)

'


class SetMtc
def self.segment(segment, start_time, end_time, update_paths = true, display_segment_data = true)
  
  if !check_params(segment, start_time, end_time, Segment)
    return false
  end

  start_time_ms = start_time.to_i*1000
  end_time_ms = end_time.to_i*1000
  
  state_b4_mtc_start = segment.prov_state.states.last(:order => "timestamp", :conditions => ["timestamp < ?", start_time_ms])
  state_b4_mtc_end = segment.prov_state.states.last(:order => "timestamp", :conditions => ["timestamp < ?", end_time_ms])
  
  paths = segment.paths
  eps = segment.segment_end_points.collect {|ep| ep if !ep.is_connected_to_test_port}.compact
  
  puts "#{segment.class.to_s}:#{segment.id} - setting prov state to Mtc at #{Time.at(start_time.to_i)} ending at #{Time.at(end_time.to_i)}"
  segment_prov = segment.prov_state
  segment_prov.states << ProvMaintenanceSegment.create(:timestamp => start_time_ms) 
  segment_prov.states << ProvLiveSegment.create(:timestamp => end_time_ms)
  
  segment.set_mtc(true, "In Maintenance", start_time_ms)
  segment.set_mtc(false, "", end_time_ms)
  
  eps.each do |ep|
    puts "#{segment.class.to_s}:#{segment.id} - setting endpoint #{ep.class.to_s}:#{ep.id} to Mtc at #{Time.at(start_time.to_i)} ending at #{Time.at(end_time.to_i)}"
    ep.prov_state.states << ProvMaintenanceEp.create(:timestamp => start_time_ms) 
    ep.prov_state.states << ProvLiveEp.create(:timestamp => end_time_ms)
  end
  
  
  if !adjust_SM_states(segment, start_time_ms, end_time_ms)
    puts "#{segment.class.to_s}:#{segment.id} - Failed to update SM states for #{segment.cenx_name}"
    return false
  end
  eps.each {|ep|
    if !adjust_SM_states(ep, start_time_ms, end_time_ms)
      puts "#{segment.class.to_s}:#{segment.id} - Failed to update SM states for #{ep.class.to_s}:#{ep.id} #{ep.cenx_name}"
      return false
    end
  }
  
  # Need to check that Path state has not changed in mtc period....No changes in prov state are supported
  if update_paths
    paths.each do |path|
      if !update_path_state(path, start_time, end_time)
        puts "#{segment.class.to_s}:#{segment.id} - Failed to update Path:#{path.id} #{path.cenx_name}"
        return false
      end
    end
  end

  # Now check each monitored Segment - If there are any alarms during the Mtc period then the SM state should be Indeterminate.
  # At the moment we assume that any errors on the memeber Segment will be caused by the Mtc actions on the Segment so the SM history
  # for the memeber Segment will show indetermiate so we don't need to anything. If however there are errors which are NOT indeterminate we need
  # to fix things up - Not supported at the moment
  off_net_ovcs = eps.collect {|ep| ep.segment_end_points.collect {|ep1| ep1.segment if ep1.segment.monitored?}.compact.uniq}.flatten.uniq
  off_net_ovcs.each do |offovc|
    all_SM_states = offovc.sm_histories.first.alarm_records.all(:order => "time_of_event", :conditions => ["time_of_event BETWEEN ? AND ?", start_time_ms, end_time_ms])
    if all_SM_states.any? {|state| [AlarmSeverity::FAILED, AlarmSeverity::WARNING, AlarmSeverity::UNAVAILABLE].include?(state.state)} 
      puts "#{segment.class.to_s}:#{segment.id} - There are alarms on the Off Net OVC #{offovc.id} which are not Indeterminate This is not supported yet...!!"
    end
  end
  
  
  # display the new SM states and Mtc States and Prov States
  if display_segment_data
    display_segment_data(segment, update_paths)
  end
  
  return true
end

def self.enni(ennis, start_time, end_time, update_paths = true)
  
  if !ennis.is_a?(Array)
    ennis = [ennis]
  end
  
  if ennis.all? {|enni| !check_params(enni, start_time, end_time, EnniNew)}
    return false
  end

  start_time_ms = start_time.to_i*1000
  end_time_ms = end_time.to_i*1000
  
  ennis.each do |enni|
    state_b4_mtc_start = enni.prov_state.states.last(:order => "timestamp", :conditions => ["timestamp < ?", start_time_ms])
    state_b4_mtc_end = enni.prov_state.states.last(:order => "timestamp", :conditions => ["timestamp < ?", end_time_ms])
  
    enni_prov = enni.prov_state
    enni_prov.states << ProvMaintenanceEnni.create(:timestamp => start_time_ms) 
    enni_prov.states << ProvLiveEnni.create(:timestamp => end_time_ms)
  
    enni.set_mtc(true, "In Maintenance", start_time_ms)
    enni.set_mtc(false, "", end_time_ms)
  
    adjust_SM_states(enni, start_time_ms, end_time_ms)
  end
  
  # Now set all endpoints and Segments to Mtc - Note can just set the Segment as this also sets the endpoints
  segments = ennis.collect {|enni| enni.segment_end_points.collect {|ep| ep.segment if ep.segment.get_prov_state.is_a?(ProvLive)}.flatten}.flatten.uniq.compact
  segments.each {|segment| 
    self.segment(segment, start_time, end_time, false, false)
  }
  
  # now update the Path's
  if update_paths
    paths = segments.collect {|segment| segment.paths}.flatten.uniq
    paths.each do |path|
      if !update_path_state(path, start_time, end_time)
        puts "#{enni.class.to_s}:#{enni.id} - Failed to update Path:#{path.id} #{path.cenx_name}"
        return false
      end
    end
  end
  
  # display the new SM states and Mtc States and Prov States
  ennis.each do |enni|
    puts "Enni:#{enni.id} info"
    puts "===================="
    if enni.prov_state != nil
      enni.prov_state.debug_dump_states
      puts "\n"
    end
    if enni.sm_histories.size != 0
      enni.sm_histories.first.alarm_debug_dump
      puts "\n"
    end
    if enni.mtc_periods.size != 0
      enni.mtc_periods.first.alarm_debug_dump
      puts "\n"
    end
  end
  
  segments.each {|segment| 
    display_segment_data(segment, update_paths)
  }
  
  return true
end


private

def self.check_params(obj, start_time, end_time, klass)
  if (start_time > Time.now.to_i) || (end_time > Time.now.to_i)
    puts "#{obj.class.to_s}:#{obj.id} - times are in the future"
    return false
  end
  if start_time > end_time
    puts "#{obj.class.to_s}:#{obj.id} -start time is after end time"
    return false
  end
  if (obj == nil) || !obj.is_a?(klass)
    puts "#{obj.class.to_s}:#{obj.id} - is nil or not a #{klass.to_s}"
    return false
  end
  
  start_time_ms = start_time.to_i*1000
  end_time_ms = end_time.to_i*1000
  
  state_b4_mtc_start = obj.prov_state.states.last(:order => "timestamp", :conditions => ["timestamp < ?", start_time_ms])
  if state_b4_mtc_start == nil
    puts "#{obj.class.to_s}:#{obj.id} - No state before the mtc period it must be Live"
    return false
  end
  
  state_b4_mtc_end = obj.prov_state.states.last(:order => "timestamp", :conditions => ["timestamp < ?", end_time_ms])
  if state_b4_mtc_end == nil
    puts "#{obj.class.to_s}:#{obj.id} - No state before the mtc period ends"
    return false
  end
  
  if state_b4_mtc_start.id != state_b4_mtc_end.id
    puts "#{obj.class.to_s}:#{obj.id} - The mtc period covers a time where the prov state has changed several times...."
    return false
  end
  
  if !state_b4_mtc_start.is_a?(ProvLive)
    puts "#{obj.class.to_s}:#{obj.id} is not Live"
    return false
  end
  
  return true
end

def self.adjust_SM_states(obj, start_time_ms, end_time_ms)
  # now need to clean up the SM states. Any alarms during the mtc period must be deleted as they would of
  # been masked out by the mtc period so insert a Mtc at the start and at the end take the last SM state
  # and change it's time to the end of the mtc period.
  # Assumption here is that the Prov state was Live and did not change in the Mtc period - Checked for at top  
  all_SM_states = obj.sm_histories.first.alarm_records.all(:order => "time_of_event", :conditions => ["time_of_event BETWEEN ? AND ?", start_time_ms, end_time_ms])
  if all_SM_states.size == 0
    # get the SM state just before the mtc period
    state_b4_mtc_start = obj.sm_histories.first.alarm_records.last(:order => "time_of_event", :conditions => ["time_of_event < ?", start_time_ms])
    if state_b4_mtc_start == nil
      puts "#{obj.class.to_s}:{#{obj.id} - No state before the mtc period error....."
      return false
    else
      sm_state = state_b4_mtc_start.clone 
      sm_state.time_of_event = end_time_ms
    end
  else
    # Get the SM state before the end of the period
    state_b4_mtc_end = obj.sm_histories.first.alarm_records.last(:order => "time_of_event", :conditions => ["time_of_event < ?", end_time_ms])
    if state_b4_mtc_end == nil
      puts "#{obj.class.to_s}:{#{obj.id} - No state before the end of mtc period error....."
      return false
    else
      sm_state = state_b4_mtc_end.clone
      sm_state.time_of_event = end_time_ms
    end
  end
  # delete all the SM states between the mtc times
  all_SM_states.each do |s|
    puts "#{obj.class.to_s}:#{obj.id} - Deleting SM entry #{s.id}:#{Time.at(s.time_of_event/1000)} #{s.state.to_s}"
    s.destroy
  end
  # add the start and end
  puts "#{obj.class.to_s}:#{obj.id} - Adding mtc start for SM at #{Time.at(start_time_ms/1000)}"
  obj.sm_histories.first.alarm_records << AlarmRecord.create(:time_of_event => start_time_ms, :state => AlarmSeverity::MTC, :original_state => "maintenance", :details => "In Maintenance")
  puts "#{obj.class.to_s}:#{obj.id} - Adding mtc end for SM entry at #{Time.at(sm_state.time_of_event/1000)} State:#{sm_state.state.to_s}"
  obj.sm_histories.first.alarm_records << sm_state
  
  return true
end  

def self.update_path_state(path, start_time, end_time)
  
  start_time_ms = start_time.to_i*1000
  end_time_ms = end_time.to_i*1000
  
  state_b4_mtc_start = path.prov_state.states.last(:order => "timestamp", :conditions => ["timestamp < ?", start_time_ms])
  state_b4_mtc_end = path.prov_state.states.last(:order => "timestamp", :conditions => ["timestamp < ?", end_time_ms])
  all_prov_states = path.prov_state.states.all(:order => "timestamp", :conditions => ["timestamp BETWEEN ? AND ?", start_time_ms, end_time_ms])
  
  if all_prov_states.size > 0
    puts "#{path.class.to_s}:{#{path.id} - The Path prov state changed during mtc period - aborting"
    return false
  end
      
  if state_b4_mtc_start.is_a?(ProvLive)
    path.prov_state.states << ProvMaintenancePath.create(:timestamp => start_time_ms) 
    path.set_mtc(true, "In Maintenance", start_time_ms)
  else
    puts "#{path.class.to_s}:{#{path.id} - Path prov state before mtc is NOT Live - aborting"
    return false
  end
  
  if state_b4_mtc_end.is_a?(ProvLive)  
    path.prov_state.states << ProvLivePath.create(:timestamp => end_time_ms)
    path.set_mtc(false, "", end_time_ms)
  else
    puts "#{path.class.to_s}:{#{path.id} - Path prov state before mtc end is NOT Live - aborting"
    return false
  end
  
  path.set_mtc(true, "In Maintenance", start_time_ms)
  path.set_mtc(false, "", end_time_ms)
  
  
  if !adjust_SM_states(path, start_time_ms, end_time_ms)
    puts "#{path.class.to_s}:{#{path.id} - Failed to update SM state for Path:#{path.id} #{path.cenx_name}"
    return false
  end
  
  return true
end

def self.display_segment_data(segment, update_paths)
  paths = segment.paths
  eps = segment.segment_end_points.collect {|ep| ep if !ep.is_connected_to_test_port}.compact
  
  puts "Segment:#{segment.class.to_s}:#{segment.id} info"
  puts "===================================="
  if segment.prov_state != nil
    segment.prov_state.debug_dump_states
    puts "\n"
  end
  if segment.sm_histories.size != 0
    segment.sm_histories.first.alarm_debug_dump
    puts "\n"
  end
  if segment.mtc_periods.size != 0
    segment.mtc_periods.first.alarm_debug_dump
    puts "\n"
  end
  
  eps.each do |ep|
    puts "Endpoint:#{ep.class.to_s}:#{ep.id} info"
    puts "===================================="
    if ep.prov_state != nil
      ep.prov_state.debug_dump_states
      puts "\n"
    end
    
    if ep.sm_histories.size != 0
      ep.sm_histories.first.alarm_debug_dump
      puts "\n"
    end
  end
  
  if update_paths
    paths.each do |path|
      puts "Path:#{path.class.to_s}:#{path.id} info"
      puts "===================================="
      if path.prov_state != nil
        path.prov_state.debug_dump_states
        puts "\n"
      end
      
      if path.sm_histories.size != 0
        path.sm_histories.first.alarm_debug_dump
        puts "\n"
      end
      if path.mtc_periods.size != 0
        path.mtc_periods.first.alarm_debug_dump
        puts "\n"
      end
    end
  end  
end


end