PathDisplay.destroy_all

Path.all.each do|path|
  path.segments.each do|segment|
    display = PathDisplay.find_or_initialize_by_path_id_and_entity_id_and_entity_type(path.id,segment.id,'Segment')
    display.row = 1
    if segment.is_a?(OffNetOvc)
      if segment.segment_owner_role == 'Buyer'
        display.column = 'B'
      elsif segment.segment_owner_role == 'Seller'
        display.column = 'F'
      end
    else
      display.column = 'D'
    end
    display.save
    
    segment.segment_end_points.each do|endpoint|
      demarc = endpoint.demarc
      unless endpoint.respond_to?(:is_connected_to_test_port) && endpoint.is_connected_to_test_port
        display = PathDisplay.find_or_initialize_by_path_id_and_entity_id_and_entity_type(path.id,demarc.id,'Demarc')
        display.row = 1
      
        end_point_display = PathDisplay.find_or_initialize_by_path_id_and_entity_id_and_entity_type_and_row(path.id,endpoint.id,'SegmentEndPoint',1)
      
        if segment.is_a?(OffNetOvc)
          if segment.segment_owner_role == 'Buyer'
            end_point_display.column = 'B'
            if demarc.cenx_demarc_type == 'UNI'
              display.column = 'A'
              end_point_display.relative_position = 'Left'
            else
              display.column = 'C'
              end_point_display.relative_position = 'Right'
            end
          elsif segment.segment_owner_role == 'Seller'
            end_point_display.column = 'F'
            if demarc.cenx_demarc_type == 'UNI'
              display.column = 'G'
              end_point_display.relative_position = 'Right'
            else
              display.column = 'E'
              end_point_display.relative_position = 'Left'
            end
          end
          display.save
        else
          end_point_display.column = 'D'
          if endpoint.segment_end_points.any?{|other_end_point| other_end_point.segment.segment_owner_role == 'Seller'}
            end_point_display.relative_position = 'Right'
          else
            end_point_display.relative_position = 'Left'
          end
        end
        begin
          end_point_display.save
        rescue
        end
      end
    end
  end
end
