#!/usr/bin/env ruby
class MoveHelper

  def self.move cenx_ids, vpls_id, dest_site = nil
    errors = []
    move_info = get_move_info cenx_ids, dest_site
    begin
      ActiveRecord::Base.transaction do
        move_info.keys.select{|k| k.is_a? Segment}.each do |segment|
          policies = segment.on_net_ovc_end_point_ennis.collect{|e| e.qos_policies}.flatten.uniq
          policies.each do |policy|
            new = policy.class.new(:policy_name => policy.policy_name,
              :policy_id => policy.policy_id,
              :site_id => dest_site.id)
            new.save!
          end
          segment.site_id = dest_site.id
          segment.service_id = vpls_id
          segment.on_net_ovc_end_point_ennis.each{ |onovce| onovce.qos_policies.clear }
          segment.save!
          segment.configure_qos_policies
        end

        move_info.keys.select{|k| k.is_a? EnniNew}.each do |enni|
          info = move_info[enni]
          enni.site_id = dest_site.id
          info.each do |port, nodes|
            dest = nodes.last
            old_port = dest.ports.find_by_name(port.name)
            old_port.destroy unless old_port.nil?
            port.node_id = dest.id
            if dest.is_service_router
              result_ok, mac = port.get_mac_address
              port.mac = mac
            end
            port.save!
            errors << port.errors.full_messages
          end
          result_ok, lag_mac_primary, lag_mac_secondary = enni.get_enni_mac_addresses
          enni.lag_mac_primary = lag_mac_primary
          enni.lag_mac_secondary = lag_mac_secondary
          enni.save!
          errors << enni.errors.full_messages
        end
      end
    rescue ActiveRecord::RecordInvalid => e
      errors << e.message
    end

    return errors.flatten
  end

  def self.get_move_info cenx_ids, dest_site
    move_info = { :errors => [], :deletions => [] }
    cenx_ids.each do |cenx_id|
      object = Demarc.find_by_cenx_id(cenx_id)
      object = Segment.find_by_cenx_id(cenx_id) if object.nil?

      if object.nil?
        move_info[:errors] << "Cenx ID #{cenx_id} is not associated with a valid object to move"
      end

      if object.is_a? Demarc
        unless object.is_a? EnniNew
          move_info[:errors] << "Cenx ID #{cenx_id} is a Demarc but not an ENNI"
          next
        end
        move_info[object], errors, deletions = get_enni_move_info object, dest_site
        move_info[:errors] << errors
        move_info[:deletions] << deletions
      end

      if object.is_a? Segment
        #Check and make sure we aren't already moving an Segment
        if move_info.keys.select { |o| o.is_a? Segment }.any?
          move_info[:errors] << "Moving Multiple Segments is not supported at this time"
          next
        end


        #Test if the Segment is connected to any other Segments
        ennis = object.segment_end_points.collect{ |ep|ep.demarc if ep.demarc.is_a? EnniNew }.uniq
        if ennis.collect { |e| e.segment_end_points.select {|ep| ep != object and ep.is_a? OnNetOvc} }.flatten.any?
          move_info[:errors] << "Moving Multiple Segments is not supported at this time (Sub Segment)"
          next
        end
        
        #Test to confirm we can move QoS policies
        policies = object.on_net_ovc_end_point_ennis.collect{|e| e.qos_policies}.flatten.uniq
        policies.each do |policy|
          id_match = dest_site.qos_policies.find_all_by_policy_id(policy.policy_id).select{|p| p.is_a? policy.class }.first
          unless id_match.nil? or id_match.policy_name == policy.policy_name
            move_info[:errors] << "QoS Policy #{policy.policy_name} on Site #{dest_site.name} conflict by ID, can not move Segment"
            next
          end
          
          name_match = dest_site.qos_policies.find_all_by_policy_name(policy.policy_name).select{|p| p.is_a? policy.class }.first
          unless name_match.nil? or name_match.policy_id == policy.policy_id
            move_info[:errors] << "QoS Policy #{policy.policy_name} on Site #{dest_site.name} conflict by Name, can not move Segment"
            next
          end
        end

        ennis.each do |enni|
          next unless move_info[enni].nil?
          move_info[enni], errors, deletions = get_enni_move_info enni, dest_site, false
          move_info[:errors] << errors
          move_info[:deletions] << deletions
        end

        move_info[object] = ennis

      end

    end
    move_info[:errors] = move_info[:errors].flatten
    move_info[:deletions] = move_info[:deletions].flatten
    return move_info
  end

  def self.get_enni_move_info enni, dest_site, check_segment=true
    errors = []
    deletions = []

    if check_segment and enni.segment_end_points.any?
      errors << "Can not move ENNI attached to Segments"
    end

    #Gather all enni ports and their connected ports
    ports = enni.ports.dup
    enni.ports.each do |port|
      ports << port.connected_port unless port.connected_port.nil?
    end

    if dest_site.demarcs.find_all_by_lag_id(enni.lag_id).any? and dest_site != enni.site and enni.lag_id != nil
      errors << "Lag ID #{enni.lag_id} already in use on site #{dest_site.name}"
    end

    if ports.collect {|p| p.node.site }.uniq.size > 1 and !dest_site.nil?
      errors << "Can not move ports as ENNI is not bound to single site"
      return {}, errors, []
    end
    
    enni_move_info = Hash.new
    ports.each do |port|
      old_node = port.node
      old_site = old_node.site
      old_prefix = old_site.node_prefix
      new_site = dest_site
      if new_site == old_site
        errors << "Can not move ENNI to the same site"
        next
      end
      new_prefix = new_site.node_prefix
      new_name = old_node.name.sub(old_prefix, new_prefix)
      new_node = Node.find_by_name(new_name)
      if new_node.nil?
        errors << "Node #{new_name} can not be found on Site #{dest_site.name}"
        next
      end
      existing_port = new_node.ports.find_by_name(port.name)
      if existing_port
        deletions << existing_port
      end

      enni_move_info[port] = [old_node, new_node]
    end

    return enni_move_info, errors, deletions
  end

  def self.copy_ports source_site, dest_site
    report = {}
    errors = []
    #Begin one big tree climb to get down to the ports
    # matching the source to the destination along the way
    if dest_site.site_type != source_site.site_type 
      errors << "Could not find matching #{source_site.site_type} site on Site #{dest_site.name}"
      return
    end

    source_site.nodes.select{|n| n.is_service_router }.each do |source_node|
      dest_node_name = source_node.name.sub(source_site.node_prefix, dest_site.node_prefix)
      dest_node = Node.find_by_name(dest_node_name)
      if dest_node.nil?
        errors << "Could not find node named #{dest_node_name} to match to #{source_node.name} on Site #{source_site.name}"
        next
      end
      
      key = {:source => source_node, :dest => dest_node}
      report[key] = []
      source_node.ports.each do |source_port|
        dest_port = dest_node.ports.find_by_name(source_port.name)
        
        if dest_port.nil?
          #Search to see if we have more info in the Source Port Name
          dest_node.ports.each do |p|
            dest_port = p if source_port.name.include? p.name
          end
        end
        
        if dest_port.nil?
          #If dest_port is still not found
          #Create New Port from Source
          dest_port = Port.new(source_port.attributes)
          dest_port.node_id = dest_node.id
          #Clear Associations
          dest_port.patch_panel_id = nil
          dest_port.demarc_id = nil
          dest_port.connected_port_id = nil
          result_ok, mac = dest_port.get_mac_address
          dest_port.mac = mac
          dest_port.notes = "" if dest_port.notes.nil?
          dest_port.notes << "\nNB: data above copied from #{source_port.name} #{source_node.name} #{source_site.name}"
          dest_port.save
          report[key] << {:port => dest_port, :new => true}
        else
          #Copy info
          dest_port.name = source_port.name #Could be carrying information
          dest_port.patch_panel_slot_port = source_port.patch_panel_slot_port
          dest_port.strands = source_port.strands
          dest_port.color_code = source_port.color_code
          result_ok, mac = dest_port.get_mac_address
          dest_port.mac = mac
          dest_port.notes = "" if dest_port.notes.nil?
          dest_port.notes << (source_port.notes.nil? ? "" : source_port.notes) + "\nNB: data above copied from #{source_port.name} #{source_node.name} #{source_site.name}"
          dest_port.save
          report[key] << {:port => dest_port, :new => false}
        end
      end
    end

    return report, errors
  end

end
