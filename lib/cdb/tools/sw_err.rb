#SW_ERR logs a SoftWare ERRor
# flags, can be :NEVER_IGNORE, :IGNORE_IF_TEST, :TEMPORARILY_IGNORE
def SW_ERR(err, flag=:NEVER_IGNORE)
  SoftwareError.instance.log(err,flag)
end

# def SW_ERR err, flag=:NEVER_IGNORE
# 
#   flags = [:NEVER_IGNORE, :IGNORE_IF_TEST, :TEMPORARILY_IGNORE]
#   unless flags.include?(flag)
#     SW_ERR "Undefined SW_ERR Flag"
#     flag = :NEVER_IGNORE
#   end
#   error = "SW_ERR(#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}): #{err}"
#   logger = get_valid_logger
#   logger.error("#{error}  #{caller[0..(caller.index{|i| i.match(/^.*\/lib\/action_controller\/routing\/route_set\.rb.*$/)}||(caller.length - 1))].join("\n  ")}\n")
#   raise error if defined?(RAILS_ENV) and RAILS_ENV == "test" and flag == :NEVER_IGNORE
# end
# 
# def get_valid_logger
#   filename = File.join(File.dirname(__FILE__), '..', '..', 'log', 'SW_ERRs.log')
#   Logger.new(filename)
# end


class SoftwareError
  include Singleton
  
  DEFAULT_MODE = :FILE
  AVAILABLE_MODES = [:FILE,:SCREEN,:MEMORY,:TEST]
  
  DEFAULT_FLAG = :NEVER_IGNORE
  AVAILABLE_FLAGS = [:NEVER_IGNORE, :IGNORE_IF_TEST, :TEMPORARILY_IGNORE]
  
  attr_accessor :mode, :logger
  
  def initialize
    filename = File.join(File.dirname(__FILE__), '..', '..', 'log', 'SW_ERRs.log')
    reset
    @logger = Logger.new(filename)
  end
  
  def reset
    @mode = DEFAULT_MODE
  end
  
  def mode=(val)
    @mode = AVAILABLE_MODES.include?(val) ? val : DEFAULT_MODE
  end
  
  def log(err,flag = :NEVER_IGNORE)
    result = ""
    unless AVAILABLE_FLAGS.include?(flag)
      result += log("Undefined SW_ERR Flag (#{flag})")
      flag = DEFAULT_FLAG
    end
    if flag == :NEVER_IGNORE || (flag == :IGNORE_IF_TEST && mode != :TEST)
      msg = log_message(err,flag)
      send("log_for_#{mode.to_s.downcase}",msg)
      result += msg
    end
    result
  end
  
  def relevant_trace_to_s(call_stack)
    max_index = call_stack.length - 1
    stop_index = call_stack.index{|i| i.match(/^.*\/lib\/action_controller\/routing\/route_set\.rb.*$/)}
    call_stack[0..(stop_index || max_index)].join("\n ")
  end
  
  private 
  
    def log_for_test(msg)
      raise msg
    end
    
    def log_for_memory(msg)
    end
    
    def log_for_screen(msg)
      STDOUT.puts(msg)
    end
    
    def log_for_file(msg)
      logger.error(msg)
    end
    
    def log_message(err,flag)
       "SW_ERR(#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}): #{err} #{relevant_trace_to_s(caller)}\n"
    end

end

