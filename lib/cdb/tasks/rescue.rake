require "resque/tasks"
task "resque:setup" => :environment

namespace :resque do

  desc "Start an Osl subscriber (WAS A WORKER)"
  task :osl do
    puts "OBSOLETE -- RUN USING AN OSL SUBSCRIBER INSTEAD"
    `bundle exec osl --config "#{File.dirname(__FILE__)}/../../config/osl.poe.yml" --cmd sub osl.poe.task_queue`
  end

  # MOVED TOWARDS A SUBSCRIPTION MODEL VIA RABBITMQ
  # task :osl do
  #   ENV['QUEUE'] = 'osl_worker'
  #   Rake::Task["resque:work"].invoke
  # end


end