require 'csv'
namespace :dbfix do

  def print_results(results)
    puts "\nSummary:"
    [:success, :failure].each do |result|
      puts " #{result.to_s.capitalize}: #{results[result].inject(0){|s,h| s+h[1]}}"
      results[result].each do |aav, stats|
        puts "  #{aav.to_s}: #{stats}"
      end
    end
  end

  desc 'This parses an OnAir spreadsheet and places the proper paths live'
  task :on_air_go_live => :environment do |t, args|
    $fake_output = false
    require './db/fake_go_in_service.rb'

    #Get the On Air spreadsheet location
    filename = ""
    if ENV["FILE"]
      filename = ENV["FILE"]
    else
      puts "Please enter the On Air spreadsheet location"
      filename = STDIN.gets.chomp
    end

    #Convert MS files into CSV
    convert = false
    if filename =~ /\.xlsx?/
      `bundle exec osl --cmd xls2csv #{filename}`
      filename = filename.gsub(/\.xlsx?/, '.csv')
      convert = true
    end

    #Results hash for final output
    results = {:success => Hash.new(0), :failure => Hash.new(0)}

    begin
      # move cursor to beginning of line
      cr = "\r"           
      clear = "\e[0K"     
      reset = cr + clear

      header = true
      processed = 0
      CSV.foreach(filename) do |row|
        #Don't parse header
        if header
          header = false
          next
        end
        #We need both a site and a backhaul live date
        next if (row[0].nil? or row[3].nil?)

        #Print status
        print "#{reset}Processed: #{processed}"
        STDOUT.flush

        #Grab all paths matching the cascade
        cascade = row[0]
        #mhs = MemberAttrInstance.find_all_by_affected_entity_type_and_value('Path', cascade)
        mhs = MemberAttrInstance.find(:all, :conditions => ["value like ? AND affected_entity_type = ?", "#{cascade}%", 'Path'])
        paths = mhs.map{|mh| mh.affected_entity}
        
        paths.each do |path|
          log = ""
          success = StateHelper.evc_live(path, log)

          puts "#{reset}--#{cascade}:#{path.id}\n#{log}\n\n" unless success

          aav = path.segments.reject{|s|s.service_provider.is_system_owner}.first.service_provider.name.to_sym
          result = success ? :success : :failure
          results[result][aav] += 1
          processed += 1
        end
      end
      File.delete(filename) if convert

    rescue Exception => e
      raise e
    ensure
      File.delete(filename) if convert
      print_results(results)
    end


  end
end
