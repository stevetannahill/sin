require 'csv'
namespace :dbfix do
  desc "Fix the missing site group issue from a spreadsheet"
  task :site_groups => :environment do
    filename = ENV["FILE"] || "../sin_doc/poe/sprint_msc_sites/sprint_mscs.csv"
    start_site_groups = SiteGroup.count
    start_site_to_site_group_mappings = ActiveRecord::Base.connection.select( "SELECT COUNT(*) FROM site_groups_sites").first.values.first

    header = true
    msc_clli = -1
    markets = -1
    CSV.foreach(filename) do |row|
      #Don't parse header
      if header
        row.each_with_index do |header, index| 
          if header =~ /MSC CLLI/
            msc_clli = index
          elsif header =~ /Markets/
            markets = index
          end
        end
        header = false
        raise Exception.new("Could not find MSC CLLI and Markets column") if (msc_clli.nil? or markets.nil?)
        next
      end
      #We need both an msc clli and a markets mapping
      next if (row[msc_clli].nil? or row[markets].nil?)

      site = Site.find_by_clli(row[msc_clli])
      next if site.nil?
      puts "Correcting Site Groups of MSC #{site.name}"
      site_groups = row[markets].split(",").map do |market| 
        market = market.strip
        site_group = SiteGroup.find_by_name(market)
        if site_group.nil?
          site_group = SiteGroup.new({:name => market})
          site_group.save
        end
        site_group
      end
      site.site_groups = site_groups
    end

    end_site_groups = SiteGroup.count
    end_site_to_site_group_mappings = ActiveRecord::Base.connection.select( "SELECT COUNT(*) FROM site_groups_sites").first.values.first

    puts "\nSummary:"
    puts "Started with #{start_site_groups} Site Groups and finished with #{end_site_groups}"
    puts "Started with #{start_site_to_site_group_mappings} Site to Site Group mappings and finished with #{end_site_to_site_group_mappings}"
  end
end
