namespace :dbfix do

  desc "Finds all monitoring Unis and Segment Endpoints and sets attributes to TBD"
  task :monitoring_reset => :environment do #[:monitoring_demarcs, :monitoring_endpoints]
    Uni.where("reflection_mechanism IS NOT NULL").update_all(:reflection_mechanism => "LBM/LBR")
    SegmentEndPoint.where("mep_id <> 'TBD'").update_all(:mep_id => 'TBD', :md_level => 'TBD', :md_format => 'TBD', :ma_format => 'TBD', :ma_name => 'TBD')
  end
end

#   desc "Finds all Unis with a reflection_mechanism and converts it"
#   task :monitoring_demarcs => :environment do 
#     cr = "\r"           
#     clear = "\e[0K"     
#     reset = cr + clear
#     results = {:modified => 0, :errors => 0}
#     demarcs = Uni.all.select{|d| d.reflection_mechanism != nil and d.reflection_mechanism != ""}
#     total = demarcs.size
#     begin
#       demarcs.each_with_index do |demarc, index|
#         print "#{reset}Converting Uni #{index+1}/#{total}"
#         STDOUT.flush
#         demarc.reflection_mechanism = "LBM/LBR"
#         demarc.save

#        if demarc.errors.any?
#           puts "#{reset}Errors for Uni(#{demarc.cenx_name})"
#           puts "  #{demarc.errors.full_messages.join("\n  ")}" 
#           demarc.save(:validate => false)
#           results[:errors] += 1
#         else
#           results[:modified] += 1
#         end
#       end
#     ensure
#       print reset
#       puts "\n\nSummary:"
#       puts "Demarcs: Total:#{Demarc.count}, Candidates:#{total}, Fixed:#{results[:modified]}, Failed:#{results[:errors]}"
#     end
#   end

#   desc "Finds all Segment Endpoints with a MEP ID and converts it"
#   task :monitoring_endpoints => :environment do 
#     cr = "\r"           
#     clear = "\e[0K"     
#     reset = cr + clear
#     results = {:modified => 0, :errors => 0}
#     endpoints = SegmentEndPoint.all.select{|ep| ep.mep_id != "TBD"}
#     total = endpoints.size
#     begin
#       endpoints.each_with_index do |endpoint, index|
#         print "#{reset}Converting Endpoint #{index+1}/#{total}"
#         STDOUT.flush
#         endpoint.mep_id = "TBD"
#         endpoint.md_level = "TBD"
#         endpoint.md_format = "TBD"
#         endpoint.ma_format = "TBD"
#         endpoint.ma_name = "TBD"
#         endpoint.save

#        if endpoint.errors.any?
#           puts "#{reset}Errors for Uni(#{endpoint.cenx_name})"
#           puts "  #{endpoint.errors.full_messages.join("\n  ")}" 
#           endpoint.save(:validate => false)
#           results[:errors] += 1
#         else
#           results[:modified] += 1
#         end
#       end
#     ensure
#       print reset
#       puts "\n\nSummary:"
#       puts "Segment Endpoint: Total:#{SegmentEndPoint.count}, Candidates:#{total}, Fixed:#{results[:modified]}, Failed:#{results[:errors]}"
#     end
#   end

# end