namespace :dbfix do
  class ErrorException < Exception
  end
  class IncompleteException < Exception
  end

  desc "Fix VLAN IDs on Multipoint Paths"
  task :mpt_vlans => :environment do
    if ENV["CENXIDS"]
      cenx_ids = ENV["CENXIDS"].split(",")
    else
      STDOUT.puts "Please enter your CENX IDs"
      cenx_ids = STDIN.gets.chomp.split(",")
    end

    # move cursor to beginning of line
    cr = "\r"           
    clear = "\e[0K"     
    reset = cr + clear

    info = {:success => [], :incomplete => {}, :failures => {}}

    begin
      cenx_ids.each_with_index do |cenx_id, index|
        begin
          ActiveRecord::Base.transaction do
            print "#{reset}Converting CENX ID (#{index+1}/#{cenx_ids.size}): #{cenx_id}"
            STDOUT.flush
            segment = Segment.find_by_cenx_id(cenx_id)
            raise IncompleteException.new("Could not find segment with CENX ID #{cenx_id}") if segment.nil?
            primary_endpoint = nil
            backup_endpoint = nil
            uni_endpoint = nil

            segment.segment_end_points.each do |ep|
              if ep.demarc.is_a? EnniNew
                if ep.demarc.ports.any? and ep.demarc.ports.first.node.name =~ /IPA01/
                  primary_endpoint = ep
                elsif ep.demarc.ports.any? and ep.demarc.ports.first.node.name =~ /IPA02/
                  backup_endpoint = ep
                end
              elsif ep.demarc.is_a? Uni
                uni_endpoint = ep
              end
            end

            raise ErrorException.new("Could not determine primary endpoint") if primary_endpoint.nil?
            raise ErrorException.new("Could not determine backup endpoint") if backup_endpoint.nil?
            raise ErrorException.new("Could not determine uni endpoint") if uni_endpoint.nil?

            primary_vlan = nil
            backup_vlan = nil
            uni_endpoint.cos_test_vectors.each do |costv|
              if costv.circuit_id =~ /-P-/
                primary_vlan = costv.vlan_id
              elsif costv.circuit_id =~ /-B-/
                backup_vlan = costv.vlan_id
              end
              break if (primary_vlan and backup_vlan)
            end

            if primary_vlan
              primary_monitor_endpoint = get_monitor_endpoint(primary_endpoint)
              raise ErrorException.new("Could not find primary monitor endpoint") if primary_monitor_endpoint.nil?
              primary_monitor_endpoint.stags = primary_vlan
              primary_monitor_endpoint.save
              raise ErrorException.new("Could not save primary monitor endpoint \n #{primary_monitor_endpoint.errors.full_messages}") unless primary_monitor_endpoint.errors.empty?
            end

            if backup_vlan
              backup_monitor_endpoint = get_monitor_endpoint(backup_endpoint)
              raise ErrorException.new("Could not find backup monitor endpoint") if backup_monitor_endpoint.nil?
              backup_monitor_endpoint.stags = backup_vlan
              backup_monitor_endpoint.save
              raise ErrorException.new("Could not save backup monitor endpoint \n #{backup_monitor_endpoint.errors.full_messages}") unless backup_monitor_endpoint.errors.empty?
            end

            raise IncompleteException.new("Could not determine primary VLAN") if primary_vlan.nil?
            raise IncompleteException.new("Could not determine backup VLAN") if backup_vlan.nil?
          end

          info[:success] << cenx_id
        rescue ErrorException => e
          info[:failures][cenx_id] = e
        rescue IncompleteException => e
          info[:incomplete][cenx_id] = e
        end
      end

    ensure
      print reset
      info[:failures].each do |cenx_id, error|
        puts "-Failures for cenx id #{cenx_id}"
        puts error.message  
        puts error.backtrace
      end
      info[:incomplete].each do |cenx_id, error|
        puts "-Incomplete info for cenx id #{cenx_id}. #{error.message}"
      end
      puts "\n\nSummary: (#{cenx_ids.size})"
      puts " Success: #{info[:success].size == cenx_ids.size ? "All (#{info[:success].size})" : info[:success].size}"
      puts " Incomplete: #{info[:incomplete].keys.size}" if info[:incomplete].keys.any?
      puts " Failures: #{info[:failures].keys.size}" if info[:failures].keys.any?
    end
  end

  desc "Fix all VLAN IDs on Multipoint Paths"
  task :all_mpt_vlans => :environment do
    puts "Finding All Multipoint to Point Segments"
    segments = Segment.all.select{|segment| segment.segment_end_points.size > 2 and (segment.segment_end_points.map {|ep| ep.demarc.class.to_s } == ["Uni", "EnniNew", "EnniNew"].sort)}
    ENV["CENXIDS"] = segments.map {|s| s.cenx_id}.join(",")
    Rake::Task['dbfix:mpt_vlans'].invoke
  end

  def get_monitor_endpoint(other_endpoint)
    return nil if other_endpoint.segment_end_points.empty?
    other_endpoint.segment_end_points.first.segment.segment_end_points.select{|ep| ep.respond_to?(:is_connected_to_monitoring_port) and ep.is_connected_to_monitoring_port}.first
  end
end