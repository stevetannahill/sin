class SaveError < Exception; end

desc "Sets up OSL for use"
task :setup_osl => :environment do
  @osl_app = Eve::Application.new(:config => "#{Rails.root}/config/osl.poe.yml")
  OslApp.register(@osl_app)
end

namespace :coresite do

  task :default => 'coresite:seed:all'

  desc "Imports all CoreSite information"
  task :import => ['coresite:import:sites', 'coresite:import:tenants', 'coresite:import:contacts']

  namespace :import do
    desc "Imports Sites"
    task :sites, [:file] => :setup_osl do |task, args|
      filename = args[:file] || ENV["SUPPORTED_SITES_FILE"] || ENV["FILE"] || "../sin_doc/coresite/poe/supported_sites.csv"
      unless File.exists? filename
        puts "Could not find file #{filename}"
        puts "Please enter the sites POE location"
        filename = STDIN.gets.chomp
      end

      @osl_app.cmd("poe parse-again #{filename}")
    end

    desc "Imports Tennants"
    task :tenants, [:file] => :environment do |task, args|
      filename = args[:file] || ENV["SUPPORTED_TENANTS_FILE"] || ENV["FILE"] || "../sin_doc/coresite/poe/supported_tenants.csv"
      unless File.exists? filename
        puts "Could not find file #{filename}"
        puts "Please enter the Tennants spreadsheet location"
        filename = STDIN.gets.chomp
      end

      file = File.open(filename)

      results = CoresiteBuildCenter.import_tenants(file)

      puts "Tenant Import Summary:"
      puts " Total: #{results[:total]}"
      puts " Success: #{results[:complete]}"
      puts " Changed: #{results[:changed]}"
      puts " Skipped due to already being present: #{results[:skipped]}"
      puts " Skipped due to missing national tenant id: #{results[:missing_id]}"
      puts " Skipped due to missing site: #{results[:missing_site]}"
      puts " Contacts created: #{results[:contacts]}"
      puts " Failed: #{results[:failed]}"
    end

    desc "Loads CoreSite contacts from a tenants file with contacts info added"
    task :contacts, [:file] do |task, args|
      filename = args[:file] || ENV["SUPPORTED_CONTACTS_FILE"] || ENV["FILE"] || "../sin_doc/coresite/poe/supported_contacts.csv"
      Rake::Task["coresite:import:tenants"].invoke(filename)
    end
  end


  desc "Loads test CoreSite objects"
  task :test =>['coresite:test:sites', 'coresite:test:tenants', 'coresite:test:contacts', 'coresite:test:ennis', 'coresite:test:xcs']
  namespace :test do

    desc "Imports test Sites"
    task :sites, [:file] do |task, args|
      filename = args[:file] || ENV["TEST_SITES_FILE"] || ENV["FILE"] || "../sin_doc/coresite/poe/test_sites.csv"
      Rake::Task["coresite:import:sites"].invoke(filename)
    end

    desc "Import test Tenants"
    task :tenants, [:file] do |task, args|
      filename = args[:file] || ENV["TEST_TENANTS_FILE"] || ENV["FILE"] || "../sin_doc/coresite/poe/test_tenants.csv"
      Rake::Task["coresite:import:tenants"].invoke(filename)
    end

    desc "Imports test ENNIs"
    task :ennis, [:file] => :setup_osl do |task, args|
      filename = args[:file] || ENV["TEST_ENNIS_FILE"] || ENV["FILE"] || "../sin_doc/coresite/poe/test_ennis.csv"
      unless File.exists? filename
        puts "Could not find file #{filename}"
        puts "Please enter the test ENNI POE location"
        filename = STDIN.gets.chomp
      end

      @osl_app.cmd("poe parse-again #{filename}")
    end

    desc "Imports test XCs"
    task :xcs, [:file] => :setup_osl do |task, args|
      filename = args[:file] || ENV["TEST_XCS_FILE"] || ENV["FILE"] || "../sin_doc/coresite/poe/test_xcs.csv"
      unless File.exists? filename
        puts "Could not find file #{filename}"
        puts "Please enter the test XC POE location"
        filename = STDIN.gets.chomp
      end

      @osl_app.cmd("poe parse-again #{filename}")
    end
  end

  desc "Loads lab CoreSite objects"
  task :lab =>['coresite:lab:sites', 'coresite:lab:tenants', 'coresite:lab:contacts', 'coresite:lab:ennis', 'coresite:lab:xcs']
  namespace :lab do

    desc "Imports lab Sites"
    task :sites, [:file] do |task, args|
      filename = args[:file] || ENV["LAB_SITES_FILE"] || ENV["FILE"] || "../sin_doc/coresite/poe/lab_sites.csv"
      Rake::Task["coresite:import:sites"].invoke(filename)
    end

    desc "Import lab Tenants"
    task :tenants, [:file] do |task, args|
      filename = args[:file] || ENV["LAB_TENANTS_FILE"] || ENV["FILE"] || "../sin_doc/coresite/poe/lab_tenants.csv"
      Rake::Task["coresite:import:tenants"].invoke(filename)
    end

    desc "Imports lab ENNIs"
    task :ennis, [:file] => :setup_osl do |task, args|
      filename = args[:file] || ENV["LAB_ENNIS_FILE"] || ENV["FILE"] || "../sin_doc/coresite/poe/lab_ennis.csv"
      unless File.exists? filename
        puts "Could not find file #{filename}"
        puts "Please enter the lab ENNI POE location"
        filename = STDIN.gets.chomp
      end

      @osl_app.cmd("poe parse-again #{filename}")
    end

    desc "Imports lab XCs"
    task :xcs, [:file] => :setup_osl do |task, args|
      filename = args[:file] || ENV["LAB_XCS_FILE"] || ENV["FILE"] || "../sin_doc/coresite/poe/lab_xcs.csv"
      unless File.exists? filename
        puts "Could not find file #{filename}"
        puts "Please enter the lab XC POE location"
        filename = STDIN.gets.chomp
      end

      @osl_app.cmd("poe parse-again #{filename}")
    end
  end

  namespace :seed do

    desc "Loads CoreSite Base DB (CDB_EXPORT_CoreSite_Base)"
    task :load_base do
      Rake::Task['db:import_snapshot'].invoke('CDB_EXPORT_CoreSite_Base.sql')
    end

    desc "Seed all CoreSite information from scratch CDB_EXPORT_CoreSite_Base"
    task :import => [:load_base, 'coresite:import']

    desc "Seed all test information from scratch CDB_EXPORT_CoreSite_Base"
    task :test => [:load_base, 'coresite:test']

    desc "Seed all lab information from scratch CDB_EXPORT_CoreSite_Base"
    task :lab => [:load_base, 'coresite:lab']

    desc "Seed all CoreSite, test and lab information from scratch CDB_EXPORT_CoreSite_Base"
    task :all => [:load_base, 'coresite:import', 'coresite:test', 'coresite:lab']

  end
  
  desc "Sync CoreSite OOS contacts with CDB"
  task :sync_contacts => :environment do
    (bc = CoresiteBuildCenter.new; bc.sync_contacts) if App.is?(:coresite)
  end
  
end