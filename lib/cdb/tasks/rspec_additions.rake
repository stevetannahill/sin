require "pathname"
require "#{File.dirname(__FILE__)}/../../spec/db_helper"

RSPEC_OPTIONS = ["--pattern **/*_spec.rb","--colour","--format documentation"] # "--format html","-o #{Rails.root}/spec/test.html"
SIN_DOC_PATH = ENV["SIN_DOC_PATH"] || "#{File.dirname(__FILE__)}/../../../sin_doc"

namespace :spec do

  desc "HELP - I don't see the right seed"
  task :help do
    puts "Looking for seeds #{SIN_DOC_PATH}, either update SIN_DOC_PATH to point to where the *.sql files are, or run the following:"
    puts "cd .."
    puts "git clone git@smeagol.cenx.localnet:sin_doc"
  end

  namespace :seed_database do
    if File.exists?(SIN_DOC_PATH)
      Dir["#{SIN_DOC_PATH}/*.sql"].each do |f|
        p = Pathname.new(f)
        filename = p.realpath.to_s
        task_name = p.basename.to_s[0...-(p.extname).length]
        desc "Load the database schema from #{task_name}"
        task task_name do
          puts `cd #{SIN_DOC_PATH} && echo "Pulling recent changes into #{SIN_DOC_PATH}" && git pull` unless ENV["DONT_PULL"]
          db = YAML::load(File.open("#{File.dirname(__FILE__)}/../../config/database.yml"))[Rails.env]
          puts "LOADING SEED: #{task_name}, INTO #{db['database']}, FOR #{Rails.env}"
          Rake::Task["db:drop"].invoke
          Rake::Task["db:create"].invoke
          execute_sql_file(db,filename,true)
          Rake::Task["db:migrate"].invoke
        end
      end      
    end
  end

  namespace :seed_data_archive_database do
    if File.exists?(SIN_DOC_PATH)
      Dir["#{SIN_DOC_PATH}/*.sql"].each do |f|
        p = Pathname.new(f)
        filename = p.realpath.to_s
        task_name = p.basename.to_s[0...-(p.extname).length]
        desc "Load the database schema from #{task_name}"
        task task_name do
          puts `cd #{SIN_DOC_PATH} && echo "Pulling recent changes into #{SIN_DOC_PATH}" && git pull` unless ENV["DONT_PULL"]
          db = YAML::load(File.open("#{File.dirname(__FILE__)}/../../config/database.yml"))["data_archive_#{Rails.env}"]
          password_string = db['password'] ? " -p#{db['password']}" : ''
          command_string = "echo 'DROP DATABASE IF EXISTS #{db['database']};' | mysql -u #{db['username']} #{password_string}"
          puts "ABOUT TO RUN: #{command_string}"
          %x[#{command_string}]
          command_string = "echo 'CREATE DATABASE IF NOT EXISTS #{db['database']};' | mysql -u #{db['username']} #{password_string}"
          puts "ABOUT TO RUN: #{command_string}"
          %x[#{command_string}]
          puts "LOADING data_archive SEED: #{task_name}, INTO #{db['database']}, FOR #{Rails.env}"
          execute_sql_file(db,filename,true)
        end
      end
    end
  end
  
  namespace :seed_data do
    if File.exists?(SIN_DOC_PATH)
      Dir["#{SIN_DOC_PATH}/*.sql"].each do |f|
        p = Pathname.new(f)
        filename = p.realpath.to_s
        task_name = p.basename.to_s[0...-(p.extname).length]

        desc "Load the schema #{task_name}, and seed the data"
        task task_name do
          Rake::Task["spec:seed_database:#{task_name}"].invoke
          Rake::Task["db:seed"].invoke
        end
      end
    end
    
    desc "Load an empty CDB schema, and seed the data "
    task :EMPTY_DB do
      Rake::Task["spec:seed_database:EMPTY_DB"].invoke
      Rake::Task["db:seed"].invoke
    end
    
  end
  
  # desc "Run all Circuit Builder specs"
  # Spec::Rake::SpecTask.new(:circuit_builder_spec) do |t|
  #   t.rspec_opts = RSPEC_OPTIONS
  #   t.pattern = 'lib/circuit_builder/spec/**/*_spec.rb'
  # end
  # 
  # desc "Build database and run Circuit Builder specs"
  # task :circuit_builder do
  #   ENV["SEED"] = ENV["CB_SEED"] || "../sin_doc/CDB_EXPORT_For_Test_Suite.sql"
  #   Rake::Task["spec:seed_database"].invoke
  #   Rake::Task["spec:circuit_builder_spec"].invoke
  # end
  # 
  # desc "Run all Service Monitoring specs"
  # RSpec::Core::RakeTask.new(:service_monitoring_spec) do |t|
  #   t.rspec_opts = RSPEC_OPTIONS
  #   t.pattern = 'spec/service_monitoring/**/*_spec.rb'
  # end
  # 
  # desc "Run a full seed and then a full spec"
  # task :full do
  #   Rake::Task["spec:circuit_builder"].invoke
  #   Rake::Task["spec:seed"].invoke
  #   Rake::Task["spec"].invoke
  # end
end
