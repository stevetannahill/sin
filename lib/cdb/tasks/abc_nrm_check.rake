# bundle exec rake scripts:abc_check 'NRM_Data_Integrity.csv'
namespace :scripts do


  desc 'Analyze the ctvs from the automated build center.'
  task :abc_nrm_check => :environment do

    check_ctv = false
    check_bl = false
    start = 0
    infile = nil
    outfile = nil

    ARGV[1..-1].each do |arg|
      if arg =~ /^_/
        if arg == "_CTV"
          check_ctv = true
        elsif arg =="_BL"
          check_bl = true
        elsif arg == "_short"
          if check_ctv
            start = 22500
          else
            puts "_short option must be after the _CTV option. Exiting..."
            exit
          end
        else
          puts "Unknown option #{arg} ('_CTV' '_BL' 'infile' '_short'). Exiting..."
          exit
        end
      else
        if check_bl && !infile
          infile = arg
          unless File.exists?(infile)
            puts "Builder Log Does not exist #{infile}. Exiting..."
            exit
          end
        else
          outfile = arg
          break
        end
      end   
    end

    if check_bl && !infile
      puts "Missing infile. Exiting..."
      exit
    end
    unless outfile
      puts "Missing outfile. Exiting..."
      exit
    end

    check_ctv = true unless check_ctv || check_bl

    ctvs_by_cascade = {}
    cascade_info = {}

    if check_ctv
      puts "Reading in and sorting CTVs..."
      count = CosTestVector.count
      progress = 0.0
      CosTestVector.find_each(start: start) do |ctv|
        ctvs_by_cascade[ctv.circuit_id.split("-")[0]] ? ctvs_by_cascade[ctv.circuit_id.split("-")[0]] << ctv : ctvs_by_cascade[ctv.circuit_id.split("-")[0]] = [ctv]
        progress += 1
        printf "#{(progress/count * 100).round(2)}%% sorted\r"
      end
      puts

      puts "Rejecting old CTVs..."
      new_ctvs_by_cascade = {}
      ctvs_by_cascade.each do |cascade, ctvs|
        ctvs_by_priority = {"P" => {}, "B" => {}}
        ctvs.each do |ctv|
          ctv_array = ctv.circuit_id.split("-")
          if ctvs_by_priority[ctv_array[2]][ctv_array[5]]
            if ctvs_by_priority[ctv_array[2]][ctv_array[5]].created_at.to_i < ctv.created_at.to_i
              ctvs_by_priority[ctv_array[2]][ctv_array[5]] = ctv
            end
          else
            ctvs_by_priority[ctv_array[2]][ctv_array[5]] = ctv
          end
        end
        new_ctvs_by_cascade[cascade] = ctvs_by_priority["P"].values + ctvs_by_priority["B"].values
      end
      puts

      puts "Collecting values..."
      progress = 0.0
      new_ctvs_by_cascade.each do |cascade, ctvs|
        cascade_info[cascade] = { "Access_Vendor" => {}, "NV_OEM" => {}, "Backhaul_Bandwidth_kbps" => {}, "BH_Site_type" => {},
                                  "MSC_Homing" => {}, "ptp_mtp" => {}, "P_VLAN" => {}, "B_VLAN" => {}, "Other_VLAN" => {} }
        ctvs.each do |ctv|
          ctv_array = ctv.circuit_id.split("-")
          add_to_key(cascade_info[cascade]["Access_Vendor"], ServiceProvider.find_by_id(OperatorNetwork.find_by_id(ctv.cos_end_point.segment_end_point.segment.operator_network_id).service_provider_id).name)

          ctv.cos_end_point.segment_end_point.segment.paths.each do |path|
            add_to_key(cascade_info[cascade]["NV_OEM"], OperatorNetworkType.find_by_id(OperatorNetwork.find_by_id(path.operator_network_id).operator_network_type_id).name)
          end

          add_to_key(cascade_info[cascade]["Backhaul_Bandwidth_kbps"], ctv.cos_end_point.ingress_cir_kbps)
          add_to_key(cascade_info[cascade]["BH_Site_type"], ctv_array[1])
          ctv.cos_end_point.segment_end_point.segment.ennis.each do |enni|
            if enni.type == "EnniNew"
              add_to_key(cascade_info[cascade]["MSC_Homing"], Site.find_by_id(enni.site_id).name)
            end
          end
          if ctv.cos_end_point.segment_end_point.segment.segment_end_points.count == 2
            add_to_key(cascade_info[cascade]["ptp_mtp"], "ptp")
          elsif ctv.cos_end_point.segment_end_point.segment.segment_end_points.count == 3
            add_to_key(cascade_info[cascade]["ptp_mtp"], "mtp")
          else
            add_to_key(cascade_info[cascade]["ptp_mtp"], ctv.cos_end_point.segment_end_point.segment.segment_end_points.count)
          end
          if ctv_array[2] == "P"
            add_to_key(cascade_info[cascade]["P_VLAN"], ctv_array[3])
          elsif ctv_array[2] == "B"
            add_to_key(cascade_info[cascade]["B_VLAN"], ctv_array[3])
          else
            add_to_key(cascade_info[cascade]["Other_VLAN"], "#{ctv_array[2]}-#{ctv_array[3]}")
          end
          progress += 1
          printf "#{(progress/count * 100).round(2)}%% done\r"
        end
      end
      puts
    end

    if check_bl
      if File.exists?(infile)
        puts "Reading Builder Logs (#{infile})..."
        ctvs_by_cascade = {}
        name_index = nil
        summary_detail_index = nil
        created_at_index = nil
        CSV.foreach(infile) do |row|
          if !name_index
            row.each_with_index do |title, index|
              if title == "name"
                name_index = index
              elsif title == "summary_detail"
                summary_detail_index = index
              elsif title == "created_at"
                created_at_index = index                  
              end
            end
          else
            if row[summary_detail_index] =~ /does not match existing/
              ctvs_by_cascade[row[name_index].split("-")[0]] ? ctvs_by_cascade[row[name_index].split("-")[0]] << [row[name_index], row[created_at_index]] :
                ctvs_by_cascade[row[name_index].split("-")[0]] = [[row[name_index], row[created_at_index]]]
            end
          end
        end
        puts

        puts "Extracting most recent ctv logs by cascade..."
        ctv_names = []
        ctvs_by_cascade.each do |cascade, bl_entries|
          ctvs = {"P" => {}, "B" => {}}
          bl_entries.each do |ctv, created_at|
            p_b = ctv.split("-")[2]
            priority = ctv.split("-")[5]
            if ctvs[p_b][priority]
              if Time.parse(ctvs[p_b][priority].last).to_i < Time.parse(created_at).to_i
                ctvs[p_b][priority] = [ctv, created_at]
              end
            else
              ctvs[p_b][priority] = [ctv, created_at]
            end
          end
          ctvs["P"].each_value do |ctv, created_at|
            ctv_names << ctv
          end
          ctvs["B"].each_value do |ctv, created_at|
            ctv_names << ctv
          end
        end
        puts

        puts "Extracting values..."
        ctv_names.each do |ctv|
          ctv_array = ctv.split("-")
          cascade = ctv_array[0]
          playbook_data = PlayBook.data(cascade)
          aav = playbook_data[:access_provider]
          oem = playbook_data[:oem].upcase
          bandwidth = ctv_array[4]
          site_type = ctv_array[1]
          msc = playbook_data[:msc]
          clli = playbook_data[:msc_clli]
          vlan = ctv_array[3]
          p_b = ctv_array[2]
          unless cascade_info[cascade]
            cascade_info[cascade] = { "Access_Vendor" => {}, "NV_OEM" => {}, "Backhaul_Bandwidth_kbps" => {}, "BH_Site_type" => {},
                                      "MSC_Homing" => {}, "ptp_mtp" => {}, "P_VLAN" => {}, "B_VLAN" => {}, "Other_VLAN" => {} }
          end
          aav = "Sprint" if aav == "Sprint MW"
          add_to_key(cascade_info[cascade]["Access_Vendor"], aav)
          add_to_key(cascade_info[cascade]["NV_OEM"], "OEM-#{oem}")
          add_to_key(cascade_info[cascade]["Backhaul_Bandwidth_kbps"], (bandwidth[3..-1].to_i * 1000).to_s)
          add_to_key(cascade_info[cascade]["BH_Site_type"], site_type)
          add_to_key(cascade_info[cascade]["MSC_Homing"], "#{msc} [#{clli}]")
          add_to_key(cascade_info[cascade]["#{p_b}_VLAN"], vlan)
        end
      else
        puts "Builder Log no longer exists #{infile}. Exiting..."
        exit
      end
    end
    puts

    puts "writing to file..."
    count = cascade_info.size
    progress = 0.0

    CSV.open(outfile, "w") do |csv|
      csv << ["Cascade", "Access_Vendor", "", "NV_OEM", "", "Backhaul_Bandwidth_kbps", "", "BH_Site_type", "", "MSC_Homing", "", "ptp_mtp", "", "P_VLAN", "", "B_VLAN", "", "Other_VLAN"]
      cascade_info.each do |cascade, info|
        row = [cascade]
        size = 0
        info.each_value do |hash|
          if size < hash.size
            size = hash.size
          end
        end
        (0 ... size).each do |index|
          add_to_row(row, info["Access_Vendor"].to_a[index])

          add_to_row(row, info["NV_OEM"].to_a[index])

          add_to_row(row, info["Backhaul_Bandwidth_kbps"].to_a[index])

          add_to_row(row, info["BH_Site_type"].to_a[index])

          add_to_row(row, info["MSC_Homing"].to_a[index])

          add_to_row(row, info["ptp_mtp"].to_a[index])

          add_to_row(row, info["P_VLAN"].to_a[index])

          add_to_row(row, info["B_VLAN"].to_a[index])

          add_to_row(row, info["Other_VLAN"].to_a[index])

          csv << row
          row = [""]
        end  
        progress += 1
        printf "#{(progress/count * 100).round(2)}%% complete\r"
      end
    end
    puts
    exit
  end

  def add_to_key(hash, key)
    hash[key] ? hash[key] += 1 : hash[key] = 1
  end

  def add_to_row(row, array)
    if array
      row << array.first
      row << array.last
    else
      row << ""
      row << ""
    end
  end
end
