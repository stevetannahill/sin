namespace :crontab do
  desc "Sync crontab with Schedules in database"
  task :sync_schedules  => :environment do
    Schedule.resync_crontab
  end
end
