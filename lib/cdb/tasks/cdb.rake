namespace :db do

  # Regenerate Diagrams
  # Redefined db:migrate task, this will run after db:migrate
  task :migrate => [:environment] do

    if Migrate.regenerate_diagrams?

      puts "Regenerating Diagrams"

      Segment.connection.schema_cache.clear!
      Segment.reset_column_information
      # Regenerate Diagrams
      error_count = 0
      ok_count = 0
      Path.find(:all).each do |p|
        # Must save to make sure proper providers are associated to path (SinEvcServiceProviderSphinxes)
        if p.save && p.reload && p.layout_diagram
          puts "Diagram generated for #{p.id} (CENX ID: #{p.cenx_id})"
          ok_count += 1
        else
          "X Problem with #{p.id} (CENX ID: #{p.cenx_id})"
          error_count += 1
        end
      end
      puts '-' * 50
      puts "SUCCESSFUL: #{ok_count}"
      puts "ERRORS: #{error_count}"
      
      # Reindex
      ReindexSphinx.schedule
    end

  end


  desc "Reload database snapshot from sin_doc. USE rake db:import_snapshot or db:import_snapshot[ATT] or db:import_snapshot[ATT_Live]"
  task :import_snapshot, :snapshot do |t, args|

    SNAPSHOT_PATH = File.join(Rails.root,"..","sin_doc")
    SIN_PATH = File.join(Rails.root,"..","sin")
    
    snapshot_fuzzy_name  = args[:snapshot]
    snapshot_name = snapshot_fuzzy_name.present? ? "#{snapshot_fuzzy_name.gsub(/_/, '.*')}" : 'sql$'

    puts "Pulling latest snapshots"
    `( cd #{SNAPSHOT_PATH} && git pull )`

    snapshot_files = Dir.entries(SNAPSHOT_PATH).select{|f| /#{snapshot_name}/i =~ f }
    if snapshot_files.size == 0
      puts "No files matched, try running just rake db:import_snapshot"
    elsif snapshot_files.size == 1
      load_snapshot(snapshot_files.first)
    else
      puts 'More than one filename match please select one by typing in the number beside the filename'
      snapshot_files.each_with_index do |filename, i|
        puts "#{i.to_s}: #{filename}"
      end

      puts "Number?"
      input = STDIN.gets.strip
      if snapshot_file = snapshot_files[input.to_i]
        load_snapshot(snapshot_file)
      else
        puts "Not a valid number"
      end      
      
    end
    
  end

  # Load snapshot into database
  def load_snapshot(snapshot_file)
    puts "Dropping existing database"
    Rake::Task['db:drop'].invoke
        
    puts "Creating empty database"
    Rake::Task['db:create'].invoke
        
    puts "Loading database snapshot #{snapshot_file}"
    `rails db < #{SNAPSHOT_PATH}/#{snapshot_file}`
        
    puts "Migrating database"
    Rake::Task['db:migrate'].invoke

    puts "Regenerate Diagrams (in SIN)"
    `rails r #{SIN_PATH}/lib/one_off_scripts/regenerate_evc_diagrams.rb`

    puts "Rebuilding Sphinx index"
    Rake::Task['ts:rebuild'].invoke

  end
  
end

namespace :cdb do

 
  # Which rake task should be run to "test" the application
  TESTING_RAKE_TASKS = {
    'ordering' => 'ci:smoke',
    'sin' => 'spec'
  }

  desc "Copy CDB to all applications, bundle those application, and then run the tests"
  task :go, :app_name do |t,args|
    if args.size == 0
      push_cdb('ordering')
      push_cdb('sin')
      bundle_app('ordering')
      test_app('ordering')
      bundle_app('sin')
      test_app('sin')
    else
      push_cdb(args[:app_name])
      bundle_app(args[:app_name])
      test_app(args[:app_name])
    end
  end

  desc "Push the CDB models, libs and factories to all applications, or specify a particular application, e.g. rake cdb:push['geotidy']"
  task :push, :app_name do |t,args|
    if args.size == 0
      push_cdb('ordering')
      push_cdb('market')
      push_cdb('sin')
      # geotidy a rails 3 application, so is not compatible with CDB
      # push_cdb_models('geotidy')
    else
      push_cdb(args[:app_name])
    end
  end
  
  desc "Pull CDB models, libs and factories from other application into CDB, e.g. rake cdb:pull['sin']"
  task :pull, :app_name do |t,args|
      pull_cdb(args[:app_name])
  end
  
  desc "Run all spec tests, or specify a particular application to run, e.g. rake cdb:test['geotidy','ruby-1.8.7-p248@geotidy']"
  task :test, :app_name do |t,args|
    
    if args.size == 0
      test_app('sin')
      test_app('ordering')
    else
      test_app(args[:app_name])
    end
  end

  def test_app(app_name)
    test_task = TESTING_RAKE_TASKS[app_name] || "spec"
    app_path = File.join(Rails.root,"..",app_name)
    command = "cd #{app_path} && rake #{test_task}"
    puts "-------------\n"
    puts "RUN THE FOLLOWING COMMAND TO TEST #{app_name}\n#{command}\n\n"
    # COMMAND DOES NOT WORK WITH BUNDLER
    # exec("cd #{app_path} && bash -l -c 'rvm use #{rvm} && rake #{test_task}'") 
  end
  
  def bundle_app(app_name)
    app_path = File.join(Rails.root,"..",app_name)
    command = "cd #{app_path} && bundle install"
    puts "-------------\n"
    puts "RUN THE FOLLOWING COMMAND TO UPDATE THE GEMS FOR #{app_name}\n#{command}\n\n"
    # COMMAND DOES NOT WORK WITH BUNDLER
    # exec("cd #{app_path} && bash -l -c 'rvm use #{rvm} && bundle'") 
  end
  
  def push_cdb(app_name)
    move_dir :models, File.join(Rails.root,"app","models"), File.join(Rails.root,"..",app_name,"app","models","cdb")
    move_dir :mailers, File.join(Rails.root,"app","mailers"), File.join(Rails.root,"..",app_name,"app","mailers","cdb")
    move_dir :lib, File.join(Rails.root,"lib"), File.join(Rails.root,"..",app_name,"lib","cdb")
    move_dir :bin, File.join(Rails.root,"bin/cdb"), File.join(Rails.root,"..",app_name,"bin","cdb")
    move_dir :coresite_mailer, File.join(Rails.root,"app/views/coresite_mailer"), File.join(Rails.root,"..",app_name,"app","views", "coresite_mailer")
    # move_dir :factories, File.join(Rails.root,"test","factories"), File.join(Rails.root,"..",app_name,"test","factories","cdb")
    remove_dir :tasks, File.join(Rails.root,"..",app_name,"lib","cdb","tasks")
  end

  def pull_cdb(app_name)
    move_dir :models, File.join(Rails.root,"..",app_name,"app","models","cdb"), File.join(Rails.root,"app","models"), false
    move_dir :mailers, File.join(Rails.root,"..",app_name,"app","mailers","cdb"), File.join(Rails.root,"app","mailers"), false
    move_dir :lib, File.join(Rails.root,"..",app_name,"lib","cdb"), File.join(Rails.root,"lib"), false
    move_dir :bin, File.join(Rails.root,"..",app_name,"bin","cdb"), File.join(Rails.root,"bin", "cdb"), false
    move_dir :coresite_mailer, File.join(Rails.root,"..",app_name,"app","views","coresite_mailer"), File.join(Rails.root,"app", "view", "coresite_mailer"), false
    # move_dir :factories, File.join(Rails.root,"..",app_name,"test","factories","cdb"), File.join(Rails.root,"test","factories"), false
  end

  def remove_dir(name, destination_path)
    if File.exists? destination_path
      puts "Removing unneeded CDB #{name} directory [#{destination_path}]"
      FileUtils.rm_rf destination_path
    else
      puts "Attempted to remove non existent CDB #{name} directory [#{destination_path}]"
    end
  end

  def move_dir(name, source_path, destination_path, delete_destination=true)
    puts "\n"
    source_file = Dir.glob("#{source_path}/*")

    if File.exists?(destination_path) && delete_destination
      puts "Removing existing CDB #{name} directory [#{destination_path}]"
      FileUtils.rm_rf destination_path   
    end
    
    if File.exists?(destination_path)
      puts "Already exists CDB #{name} directory [#{destination_path}]"
    else
      puts "Creating CDB #{name} directory [#{destination_path}]"
      FileUtils.mkdir destination_path
    end
    
    puts "About to copy #{source_file.size} source files"
    FileUtils.cp_r source_file, destination_path, :preserve => true
    puts "Done.\n"

  end

end
