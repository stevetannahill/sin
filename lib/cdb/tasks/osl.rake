
desc "Start the OSL command line prompt (OBSOLETE - USE osl <params> directly, most likely through bundler or oss-db_env)"
task :osl => :environment do
  puts "OBSOLETE - JUST USE osl directly"
end
