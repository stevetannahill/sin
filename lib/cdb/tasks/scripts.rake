require 'date'
require 'roo'
require 'iconv'

namespace :scripts do

  desc 'Create a subset of sites based on an input CSV file of monitoring export data.'
  task :extract_sites do

    #create an array that will sotre all the export.csv files
    #use the 'ARGV[i...j]' to get the arguments from the enviornment (3 dots means the last value is not included). Note: the first argument will always be the name of the task, therefore start from 1 (ARGV[1])
    all_the_files = ARGV[1...-2]
    
    #at this point, 'all_the_files' array will have the necessary files export files required to be processed 
    #get the last entry from the 'all_the_files' array into the 'sites' variable
    sites = ARGV[-1]
    
    #get the second last entry from the 'all_the_files' array into the 'playbook' variable
    playbook = ARGV[-2]
  
    
    circuit_id_index = false
    cascade_index = false
    msc_clli_playbook_index = false
    msc_clli_sites_index = false
    oem_index = false
    access_provider_index = false
    circuit_ids = {}
		msc_cllis = {}
    msc_cllis_by_aav = {}
    oems = {}
    aavs = {}
    missing_cascades = []
		extracted_row = []

		access_providers_count = {}
		id_each_file = {}
		dates_hash = {}
		date = false
		time = false
		smallest_date = Date.today + 1
    largest_date = -1

		
		#check that the last entry is sat_sites and the second last entry is PlayBook, if not report error and quit
    if sites.empty? || !sites.match(/_Sites_/) || playbook.empty? || !playbook.match(/[Pp]lay/)
      puts "Incorrect Format!!"
      puts "Please make sure the format is: 'x number of export_data_file1.csv' Playbook_file.csv' 'Sat_Sites_Address_MSC_file.csv'"
      puts sites
      puts playbook
      exit
    end
    
    all_the_files.each do |file_name|
      
      #the file_name is not in the id_each_file hash add it, and assign its value to false
      id_each_file[file_name] = false
      #set the variable 'find_date_yet' to false, when we find the date, we will set it to true
      find_date_yet = false
      #set the variable 'find_time_yet' to false, when we find the time, we will set it to true
      find_time_yet = false
    
      puts "Parsing export file #{File.basename(file_name)}..."
      infile_count = %x{wc -l < "#{file_name}"}.to_i
      count = 0

      CSV.foreach(file_name) do |export_row| 
        #get the date from the 'sch_export_data' file and store it in a global hash called 'dates_hash'. 
        #This hash will contain dates as key and time as value, one for each file; sort the hash to get the earliest date and latest date!
        if export_row.any?

          if !find_date_yet
            if export_row[0] =~ /SQL Date Flag .*: (.+)/
              #found the row with the date, get the date
              date = $1  #'$1' is the remaining value(s) associated with 'Default SQL Date Flag Used:'

              #set find_date_yet to true
              find_date_yet = true
            end
          end

          if !find_time_yet
 
            if export_row[0] =~ /SQL Time Flag .*: (.+)/
              #found the row with the date, get the date
              time = $1     #'$1' is the remaining value(s) associated with 'Default SQL Time Flag Used:'

              #set find_date_yet to true
              find_time_yet = true
              
              #assign the time to the date and output
              dates_hash[date] = time
              #dates_hash.each {|key, value| puts "Date is: #{key} and time is: #{value}"}
            end
          end
        else
          #move on
        end

        if id_each_file[file_name]  #if the index "Cricuit ID" column is known, continue!
          key = export_row[id_each_file[file_name]].gsub(/-.*/, '') #extract the cascade id (everything up till the first " - ")
          circuit_ids[key] = export_row[id_each_file[file_name]].gsub(/^\w*-/, '').gsub(/-.*/, '')    #the cascade id acts as a 'key' and everything from the first " - " to the second " - " is the value
        else
          export_row.each_with_index do |col, index|
            if col == 'Circuit ID'
              id_each_file[file_name] = index   #circuit_id_index = index
            end
          end
        end

      count += 1.0
      complete = "%0.2f" % ((count/infile_count)*100)
      printf "#{complete}%%\r"

      end
    end
    

    #If all the dates are same, the 'dates_hash.length' will equal to '1'(in ruby, if the new key is equal to the key already present in the hash, it will replace the old key), check for this first, otherwise iterate through the 'dates_hash'
    if dates_hash.length == 1
      smallest_date = dates_hash.to_a.flatten[0]
      largest_date = dates_hash.to_a.flatten[0]
    else
      dates_hash.each do |key|
        a_date = Date.strptime(key[0], '%Y-%m-%d')
        if a_date < smallest_date
          smallest_date = a_date
        elsif a_date >= largest_date
          largest_date = a_date
        end
      end
    end
    
    

	 	puts "Looking up Circuit IDs in the playbook (#{playbook})..."
    playbook_count = %x{wc -l < "#{playbook}"}.to_i

	 	count = 0
    CSV.foreach (playbook) do |playbook_row|
      if cascade_index
      	if circuit_ids[playbook_row[cascade_index]]    #in playbook_row, in the cascade column, if the value matches a value in circuit_ids, then continue
        	if msc_cllis[playbook_row[msc_clli_playbook_index]] #check if the index is set, if not, go to the else statement! 
            msc_cllis[playbook_row[msc_clli_playbook_index]][:cascades] << playbook_row[cascade_index]  #accessing the array created by else statement.....at 'literal cascade' append the corresponding value at 'playbook_row[cascade_index]' to the array specified by 'msc_cllis[playbook_row[msc_clli_playbook_index]][:cascades] '   
            msc_cllis[playbook_row[msc_clli_playbook_index]][:network_types] << circuit_ids[playbook_row[cascade_index]]
            msc_cllis[playbook_row[msc_clli_playbook_index]][:access_providers] << playbook_row[access_provider_index]
          else
            #Shwetank: I added the last literal to the msc_cllis. The literal being ':access_providers_breakdown'
            msc_cllis[playbook_row[msc_clli_playbook_index]] = {:oem => playbook_row[oem_index], :access_providers => [playbook_row[access_provider_index]], :network_types => [circuit_ids[playbook_row[cascade_index]]], :cascades => [playbook_row[cascade_index]], :matched => false, :access_providers_breakdown => {}, :array_access_providers_breakdown => []}
          end

          # oem counts
          oems[playbook_row[oem_index]] ||= 0
          oems[playbook_row[oem_index]] += 1

          # access provider counts
          aavs[playbook_row[access_provider_index]] ||= 0
          aavs[playbook_row[access_provider_index]] += 1
        end
      else
        playbook_row.each_with_index do |col, index|
          if col == 'Cascade'
            cascade_index = index
          elsif col == 'MSC CLLI'
            msc_clli_playbook_index = index
          elsif col == 'Access Provider'
            access_provider_index = index
          elsif col == 'OEM'
            oem_index = index
          end
        end
      end


      count += 1.0
      complete = "%0.2f" % ((count/playbook_count)*100)
      printf "#{complete}%%\r"
    end

		puts "Matching MSC CLLIs in the sites list (#{sites})..."
    CSV.foreach(sites) do |sites_row|
			if msc_clli_sites_index
				if msc_cllis[sites_row[msc_clli_sites_index]]
					extracted_row << sites_row
          msc_cllis[sites_row[msc_clli_sites_index]][:matched] = true
        end
      else
        sites_row.each_with_index do |col, index|
    			if col == 'MSC CLLI'
            msc_clli_sites_index = index
            extracted_row << sites_row
            break
          end
        end
      end
		end
		



		msc_cllis.each do |key, value| 

       #create and empty hash which will later store the provider and its count for individual msc_clli value (in this do loop, its the key)
		  access_provider_count_hash = {} 
		  msc_cllis[key][:access_providers].each do |individual_provider|
		    
		    #if the provider is not already in the hash, include it
		    unless access_provider_count_hash.has_key?(individual_provider)
		      access_provider_count_hash[individual_provider] = 0
	      end
	  
	      access_provider_count_hash[individual_provider] = access_provider_count_hash[individual_provider] + 1
	    end

		  msc_cllis[key][:access_providers_breakdown] = access_provider_count_hash
		  
		  #put the provider and its count into an array called ':array_access_providers_breakdown' so it becomes easier to append to the .csv file later. This array is part of the msc_cllis structure
		  msc_cllis[key][:access_providers_breakdown].each {|akey, value| msc_cllis[key][:array_access_providers_breakdown] << "#{akey} (#{value})" }
	  end


    try_csv = []

		puts "Writing to extracted_sites_#{largest_date}.csv..."

    network_types = msc_cllis.map{|key, value| value[:network_types]}.flatten.uniq
    matched_msc_cllis = msc_cllis.select{|key, msc_clli| msc_clli[:matched] == true}

		CSV.open(File.join(Rails.root, "extracted_sites_#{largest_date.to_s.split('-').join('')}.csv"), "w") do |csv|
		  csv << ["Start Date: #{smallest_date}", "Default SQL Time #{dates_hash[smallest_date.to_s]} "]
		  csv << ["End Date: #{largest_date}",  "Default SQL Time #{dates_hash[largest_date.to_s]}" ]
		  csv << []
	  	extracted_row.each_with_index do |row, index|
	  		if index == 0
          row << 'Circuits Total'
          network_types.each {|network_type| row << network_type }
          row << 'AAV'
	  			csv << row
	  		else
	  			row << msc_cllis[row[msc_clli_sites_index]][:cascades].length 
	  			
          network_types.each {|network_type| row << msc_cllis[row[msc_clli_sites_index]][:network_types].select {|nt| nt == network_type}.length}
          
          row << msc_cllis[row[msc_clli_sites_index]][:array_access_providers_breakdown].sort.join(', ')
          try_csv << row
	  		end
	  	end
	  	

	    try_csv.sort! {|a, b| a[0] <=> b[0]}
	    
	  	try_csv.each {|element| csv << element}

      # Append Summary Info
      csv << []
      csv << []
      header = ['', 'MSCs', 'Sites']
      network_types.each {|network_type| header << network_type }
      csv << header

      totals = ['Total', matched_msc_cllis.length, oems.values.inject(0, :+)]
      network_types.each {|network_type| totals << matched_msc_cllis.map{|key, msc_clli| msc_clli[:network_types]}.flatten.select{|nt| nt == network_type}.length}
      csv << totals

      oems.each do |oem, count|
        oem_msc_cllis = matched_msc_cllis.select{|key, msc_clli| msc_clli[:oem] == oem}
        row = [oem, oem_msc_cllis.length, count]
        network_types.each {|network_type| row << oem_msc_cllis.map{|key, msc_clli| msc_clli[:network_types]}.flatten.select{|nt| nt == network_type}.length}
        csv << row
      end

      csv << []

      # Append Access Provider Counts
      access_providers = matched_msc_cllis.map{|key, msc_clli| msc_clli[:access_providers]}.flatten.uniq.sort
      # network_types has already been set

      csv <<  ['AAV count: ' + access_providers.length.to_s()]

      # calculate the totals by access provider and network type

      # initialize counts to 0
      aav_network_totals = {}
      access_providers.each do |ap|
        network_types.each do |nt|
          aav_network_totals[ap + nt] = 0
        end
      end

      # get all sites
      results = []
      matched_msc_cllis.each do |key, value|
        # get all sites within this msc
        results.push(*(matched_msc_cllis[key][:access_providers].zip(matched_msc_cllis[key][:network_types])))
      end

      # count the sites
      results.each do |result|
        aav_network_totals[result[0] + result[1]] += 1
      end

      # output results
      access_providers.each do |ap|
        aav_msc_cllis = matched_msc_cllis.select{|key, msc_clli| msc_clli[:access_providers].include?(ap)}
        row = [ap, aav_msc_cllis.length, aavs[ap]]

        network_types.each do |nt|
          row << aav_network_totals[ap + nt]
        end
        csv << row
      end

      csv << []
      #analyze cascades missing from playbook

      #select circuits where the key(cascade) is not found in msc_cllis (which contains all cascades read in from playbook)
      circuits_missing_from_playbook = circuit_ids.select do |key, circuit|
        !(msc_cllis.map{|key2, msc_clli| msc_clli[:cascades].include?(key)}.any?{|bool| bool})
      end
      missing_cascades = circuits_missing_from_playbook.collect{|key, value| key}
      csv << ['Unmatched Cascades:', missing_cascades.length]
      missing_test_cascades = missing_cascades.select{|cascade| cascade.include?("TEST")}
      csv << ['Unmatched Test Cascades:', missing_test_cascades.length]
      missing_cascades -= missing_test_cascades
      csv << ['Unmatched W/O Test:', missing_cascades.length]
      csv << []

      unless missing_cascades.empty? and missing_test_cascades.empty?
        csv << ['Unmatched W/O Test:', 'Unmatched Test Cascades:']
        i = 0
        while i < missing_cascades.length or i < missing_test_cascades.length
          row = []
          if i < missing_cascades.length
            row << "#{missing_cascades[i]}"
          else
            row << ''
          end
          if i < missing_test_cascades.length
            row << "#{missing_test_cascades[i]}"
          end
          csv << row
          i += 1
        end
      end

      csv << []
	  end


	  puts 'Complete'
	  
	  # Each command in the terminal is seen as a task name by rake. Rake will look for these tasks and produce an error when it does not find it. 
	  # Therefore, launch the  remaining tasks from here and make them a no-operation task by doing 'task_name.to_sym do ; end'
	  
	  for i in 0...all_the_files.length
	    task all_the_files[i].to_sym do ; end
    end

  end 



  desc 'Output stats from given ODP files.'
  task :extract_odp_sites do



    #values pulled from odp files
    cascade_nums = Array.new
    providers = Array.new
    site_type = Array.new
    msc = Array.new
    #totals 
    diff_providers = Hash.new(0)  #total sites per provider
    diff_site_types = Hash.new(0) #total sites per site type
    diff_msc = Hash.new(0)      #total sites per msc
    #overal totals
    no_provider = 0         #number of sites with no provider listed
    num_providers = 0         #total number of providers 
    no_type = 0
    total = 0
    total_files = 0
    total_site_type = 0
    total_msc = 0
    mw_found = Array.new
    num_MW = Hash.new(0)
    no_msc = Array.new
    no_msc_hash = Hash.new(0)

    prov_values = Array.new # array of hashes circuits of each provider per msc
    type_values = Array.new # array of hashes circuits of each type per each msc

    msc_per_provider = Array.new 
    type_per_provider = Array.new #array of hashes

  

    if ARGV[4] then
      csv_file = ARGV[4]
    else
      csv_file = "odp_stats_#{Date.today.to_s.split('-').join('')}.csv"
    end

    #create array with all Excel files in given directory
    odp_files = Dir.glob(ARGV[1]+ "/*.{xls,xlsx}") + Dir.glob(ARGV[2]+ "/*.{xls,xlsx}") + Dir.glob(ARGV[3]+"/*.{xls,xlsx}")
    #odp_files = Dir.glob(ARGV[2]+ "/*.{xls,xlsx}")

    #odp_files = []
    temp_array = Dir.glob(ARGV[1]+"/*")
    temp_array.each do |file|
      if File.directory?(file) then
        odp_files = odp_files + Dir.glob(file+ "/[^~]*.{xls,xlsx}") 
      end
    end
    temp_array = Dir.glob(ARGV[2]+"/*")
    temp_array.each do |file|
      if File.directory?(file) then
        odp_files = odp_files + Dir.glob(file+ "/[^~]*.{xls,xlsx}") 
      end
    end
    temp_array = Dir.glob(ARGV[3]+"/*")
    temp_array.each do |file|
      if File.directory?(file) then
        odp_files = odp_files + Dir.glob(file+ "/[^~]*.{xls,xlsx}") 
      end
    end

    #name = "../exx_doc/phase2/odp/TN_Memphis_ Mkt_Zayo.xls"

    # odp_files = []
    # odp_files << "/Users/sarahboyd/projects/exx_doc/phase2/odp/20121113/PH2 Design vs EMS Audit 09 27 12_Memphis.xls"

    two_tabs = false
    two_tabs2 = false
    puts "\nProcessing ODPs . . . . ."
    #parse through each excel file in given directory
    odp_files.each do |name|
      puts "  --  #{File.basename(name)}"
      cascade_col = 0
      provider_col = 0
      site_type_col = 0
      msc_col = 0
      total_files += 1
      adjustment = 0  #used for files that have an extra row at the top of the worksheet
      if File.extname(name) == ".xls"
        wb = Excel.new(name)
      else 
        wb = Excelx.new(name)
      end 
      sheets = wb.sheets()
      #find "Wireless Cell Site Info" worksheet
      if name.index("PH2 Design vs EMS Audit 09 27 12_Memphis") && !two_tabs then
        odp_files << name
      end
      if name.index("PH2 Design vs EMS Audit 10 15 12_Austin") && !two_tabs then
        odp_files << name
      end


      sheets.each do |sheets|
        if sheets == "Wireless Cell Site Info" then
          wb.default_sheet = sheets
          break
        elsif (name.index("Atlanta") && sheets.index("Norcross")) 
          wb.default_sheet = sheets
          break 
        elsif (name.index("PH2 Design vs EMS Audit 09 27 12_Memphis") && sheets.index("Memphis")) && two_tabs == false
          wb.default_sheet = sheets
          two_tabs = true
          break
        elsif (name.index("PH2 Design vs EMS Audit 09 27 12_Memphis") && sheets.index("Austin")) && two_tabs == true
          wb.default_sheet = sheets
          two_tabs = false
          break
        elsif (name.index("PH2 Design vs EMS Audit 10 15 12_Austin") && sheets.index("Memphis")) && two_tabs2 == false
          wb.default_sheet = sheets
          two_tabs = true
          break
        elsif (name.index("PH2 Design vs EMS Audit 10 15 12_Austin") && sheets.index("Austin")) && two_tabs2 == true
          wb.default_sheet = sheets
          two_tabs = false
          break
        end
      end
      adjustment = -1 if name.index("PH2")
      adjustment = 1 if !wb.cell(2,1)
      adjustment = 2 if name.index("FA12.0037")
      #find column numbers
      1.upto(wb.last_column) do |col|
        if name.index("Atlanta") || name.index("TN_") || name.index('TN-_') || name.index("TN.") || name.index("PH2")
          cascade_col = col if wb.cell(1,col).to_s.downcase == "cascade" || wb.cell(1,col).to_s.downcase == "cascade id" || wb.cell(1,col).to_s.downcase == "cascade_id" || wb.cell(1,col).to_s.downcase == "site id"
          provider_col = col if wb.cell(1,col).to_s.downcase.index("provider") 
          provider_col = col if wb.cell(1,col).to_s.downcase.index("aav name") && name.index("TN_Nashville") 
          provider_col = col if wb.cell(1,col).to_s.downcase.index("vendor") && (name.index("TN-_Nashville") || name.index("TN.") || name.index("TN_Knoxville"))
          provider_col = col if wb.cell(1,col).to_s.downcase == ("aav") && name.index("PH2")
          site_type_col = col if wb.cell(3,col).to_s.downcase.index("aav") || wb.cell(3,col).to_s.downcase.index("donor") || wb.cell(3,col).to_s.downcase.index("fiber")
          msc_col = col if wb.cell(1,col).to_s.downcase == "msc homing" || wb.cell(1,col).to_s.downcase == "msc"
        else
          cascade_col = col if wb.cell(2+adjustment,col).to_s.downcase == "cascade" 
          provider_col = col if wb.cell(2+adjustment,col).to_s.downcase.index("provider") 
          site_type_col = col if site_type_col==0 && (wb.cell(3+adjustment,col).to_s.downcase.index("aav") || wb.cell(3+adjustment,col).to_s.downcase.index("donor") || wb.cell(3+adjustment,col).to_s.downcase.index("fiber only"))
          msc_col = col if wb.cell(2+adjustment,col).to_s.downcase == "nni network homing" || wb.cell(2+adjustment,col).to_s.downcase == "nni interconnection point" || wb.cell(2+adjustment,col).to_s.downcase == "nni network interconnection"
        end 
      end 

      if site_type_col == 0 then
        1.upto(wb.last_column) do |col|
          site_type_col = col if wb.cell(1,col).to_s.downcase == "oem" && wb.cell(2+adjustment,col).to_s.downcase == "site type"
        end 
      end 

      if site_type_col == 0 then
        1.upto(wb.last_column) do |col|
          site_type_col = col if wb.cell(1,col).to_s.downcase == "sprint" && wb.cell(2+adjustment,col).to_s.downcase == "site type"
          if site_type_col != 0 then
            break
          end
        end 
      end 

      if site_type_col == 0 && name.index("Atlanta") then
        1.upto(wb.last_column) do |col|
          site_type_col = col if wb.cell(1,col).to_s.downcase.index("site type")
          if site_type_col != 0 then
            break
          end
        end 
      end 

      #special case AM11.1831
      if name.index("1831") then
        #site_type_col = 6
      end
      #special case FA11.0584, FA11.0678
      if name.index("0584") then
        site_type_col = site_type_col + 1
      end
      if name.index("0678") then
        site_type_col = 17
      end
      if name.index("Waco UNI Sizing_NNI Mapping FINAL 8.19.11") then
        adjustment = 3 
        cascade_col = 1
        #site_type = 2
      end


      #check that all columns could be found
      puts "ERROR: Cascade column not found in #{File.basename(name)}" if cascade_col == 0
      puts "ERROR: Provider column not found in #{File.basename(name)}" if provider_col == 0
      puts "ERROR: Site Type column not found in #{File.basename(name)}" if site_type_col == 0
      puts "ERROR: MSC column not found in #{File.basename(name)}" if msc_col == 0

      #store cascade, provider, site type, msc
      3.upto(wb.last_row)  do |row|
        if wb.cell(row+adjustment,cascade_col) then
          if wb.cell(row+adjustment,provider_col) then
            prov = wb.cell(row+adjustment,provider_col)
            prov = "AT&T" if prov == "ATT"
            prov = "AT&T" if prov == "AT&T "
            prov = "Charter" if prov == "Charter (2012 CIR Uses Oversubscription)"
            prov = "Dukenet" if prov == "Dukenet (2012 CIR Uses Oversubscription)"
            prov = "CoStreet" if prov == "Costreet"
          else
            prov = "NONE LISTED"
          end
          type = wb.cell(row+adjustment,site_type_col)
          

          #if no type listed.. search for an older type column
          #special case for FA0208
          if name.index("0208") && row == 8 then
            type = nil
          end
          if !type && !name.index("Atlanta") then
            old_col = site_type_col
            site_type_col = 0
            old_col.upto(wb.last_column) do |col|
              site_type_col = col if wb.cell(2,col).to_s.downcase.index("site type")
              if site_type_col != 0 then
                type = wb.cell(row,site_type_col)
                break
              end
            end 
          end

          type = "UNKNOWN" if !wb.cell(row,site_type_col)
          if type.downcase.index("necklace") then
            type = "Donor-Necklace"
          elsif type.downcase.index("h") && type.downcase.index("donor") then
            type = "Donor-H&S"
          elsif type == "(DONOR)" then
            type = "Donor"
          elsif !type || type == "MACRO SITE" then
            type = "UNKNOWN"
          end
          cascade_number = wb.cell(row+adjustment,cascade_col)[0,9]
          
        
          temp = wb.cell(row+adjustment,msc_col)
          temp = "UNKNOWN" if msc_col == 0 || !temp
          temp = "Lenexa-MSC" if temp == "LENEXA-MSC"

          #check if cascade already exists in array
          if cascade_nums.include?(cascade_number) then 
            providers[cascade_nums.index(cascade_number)] = prov
            site_type[cascade_nums.index(cascade_number)] = type
            msc[cascade_nums.index(cascade_number)] = temp
          else
            cascade_nums.push cascade_number
            providers.push prov
            site_type.push type
            msc.push temp
            mw_found.push name if prov == "MW"
            no_msc.push name if temp == "UNKNOWN"
          end
        end 
      end
    end



    #count sites per provider
    providers.each do |v|
      diff_providers[v] += 1
    end
    #count sites per site type
    site_type.each do |v|
      diff_site_types[v] += 1
    end
    #counting number of MW sites found
    mw_found.each do |v|
      num_MW[v] += 1
    end
    #counts number of MSCs per provider
    msc.each do |v|
      diff_msc[v] += 1
    end
    #counts number of sites that does not have a provider
    no_msc.each do |v|
      no_msc_hash[v] += 1
    end


    #add up total sites and number of providers
    diff_providers.sort_by{|k,v| v}.reverse.each do |k, v|
      no_provider = v if k == "NONE LISTED"
      total = total + v
      num_providers += 1 if k != 'NONE LISTED'
    end

    #add up total number of site types, and count how many sites do not have a type
    diff_site_types.sort_by{|k,v| v}.reverse.each do |k, v|
      no_type = v if k == "UNKNOWN"
      total_site_type += 1 if k != "UNKNOWN"
    end

    #count number of different mscs
    diff_msc.sort_by{|k,v| k}.each do |k, v|
      total_msc += 1 if k != "UNKNOWN"
    end 

    puts "\nFiles with No MSC Found:"
    total_no_msc = 0;
    no_msc_hash.each do |k, v|
      puts "#{v} sites from #{File.basename(k)}"
      total_no_msc += v
    end


    # totals information
    puts "\nTotal Files Processed: #{total_files}"
    puts "Total Sites Found: #{total}"
    puts "Total MSC's Found: #{total_msc}"
    puts "Total Providers Found: #{num_providers}"
    puts "Total Types Found: #{total_site_type}"
    puts "No Provider Listed: #{no_provider}"
    puts "No Type Listed: #{no_type}"
    puts "No MSC Listed: #{total_no_msc}\n"

=begin
  #to see which files have Provider MW listed
    puts "Provider: MW"
    num_MW.each do |k, v|
      puts "#{v} from #{k}"
    end
=end

  


    diff_msc.sort_by{|k,v| k}.each do |k, v|
      temp1 = Hash.new(0)
      temp2 = Hash.new(0)
      cnt = 0
      providers.each do |v|
        temp1[v] += 1 if msc[cnt] == k
        cnt += 1
      end 
      cnt = 0
      site_type.each do |v|
        temp2[v] += 1 if msc[cnt] == k
        cnt += 1
      end
      prov_values.push temp1
      type_values.push temp2  
    end 

    diff_providers.sort.each do |k,v|
      temp1 = Hash.new(0)
      cnt = 0
      site_type.each do |v|
        temp1[v] +=1 if providers[cnt] == k
        cnt += 1
      end 
      type_per_provider.push temp1
    end


    diff_providers.sort.each  do |k, v|
      msc_per_provider.push 0
    end

    cnt = 0
    diff_providers.sort.each do |k,v|
      temp = 0
      diff_msc.sort.each do |x, y|
        msc_per_provider[cnt] += 1 if prov_values[temp].has_key?(k) && x != "UNKNOWN"
        temp += 1
      end
      cnt += 1
    end


    puts "\nWriting to #{File.basename(csv_file)}"
    #output to CSV file
    headers = "MSC,Circuits Total,,,"
    diff_site_types.sort_by{|k,v| k}.each do |k, v|
      headers = headers + "#{k},"
    end
    csv= File.new(csv_file,"w")
    csv << "\n"+headers + "\n"

    cnt = 0
    diff_msc.sort_by{|k,v| k}.each do |k, v|
      string = ",,"
      diff_site_types.sort.each do |x,y|
        if type_values[cnt].has_key?(x) then
          string = string + "#{type_values[cnt][x]},"
        else
          string = string + "0,"
        end 
      end
      prov_values[cnt].sort_by{|k,v| k}.each do |x, y|
        string = string + "#{x}(#{y}) "
      end
      csv << "#{k},#{v},#{string}\n"
      cnt += 1
    end

    headers_totals = "\n\n,MSC,Sites,,"
    total_types ="\nTotal,#{total_msc},#{total},," 
    diff_site_types.sort_by{|k,v| k}.each do |k, v|
      headers_totals = headers_totals + "#{k},"
      total_types = total_types + "#{v},"
    end


    csv << "#{headers_totals}"
    csv << "#{total_types}"
    csv << "\n\nProvider count: #{num_providers}\n"

    cnt = 0
    diff_providers.sort_by{|k,v| k}.each do |k, v|
      string = "#{k},#{msc_per_provider[cnt]},#{v},,"
      diff_site_types.sort.each do |x,y|
        if type_per_provider[cnt].has_key?(x) then
          string = string + "#{type_per_provider[cnt][x]},"
        else 
          string = string + "0,"
        end
      end 
      string = string + "\n"
      csv << string
      cnt += 1
    end

    puts "\nComplete"

  end

end