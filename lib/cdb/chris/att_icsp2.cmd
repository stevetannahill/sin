screenshot
#type name:secloc {eusa.secondary_location_section.SECLOC}
#type name:qpr {qpr}
type name:sei {eusa.secondary_location_section.SEI}
type name:sccea {eusa.secondary_location_section.SCCEA}
type name:otc {eusa.secondary_location_section.OCT}
type name:gcon {eusa.secondary_location_section.GCON}
#type name:gtel_area {gtel_area}
#type name:gtel_exch {gtel_exch}
#type name:gtel_num {gtel_num}
#type name:gtel_ext {gtel_ext}
#type name:ctx_area {ctx_area}
#type name:ctx_exch {ctx_exch}
#type name:ctx_num {ctx_num}
#type name:ctx_lstd_nm {ctx_lstd_nm}
#type name:sec_adm {sec_adm}
#type name:es {es}
#type name:profe {profe}
#type name:profi {profi}
#type name:serv_remarks {serv_remarks}
screenshot