screenshot
type name:nc {eusa.circuit_detail_section.NC}
type name:nci {eusa.circuit_detail_section.NCI}
#type name:tlv_t {tlv_t}
#type name:tlv_r {tlv_r}
type name:pqpr {eusa.circuit_detail_section.PQPR}
type name:sr2 {eusa.circuit_detail_section.SR}
type name:sss {eusa.circuit_detail_section.SSS}
type name:trf {eusa.circuit_detail_section.TRF}
type name:hvp {eusa.circuit_detail_section.HVP}
type name:mst {eusa.circuit_detail_section.MST}
type name:cklt {eusa.circuit_detail_section.CKLT}
type name:nsl {eusa.circuit_detail_section.NSL}
type name:muxloc {eusa.circuit_detail_section.MUXLOC}
type name:pri_adm {eusa.circuit_detail_section.PRI ADM}
type name:nvc {eusa.circuit_detail_section.NVC}
type name:pspeed {eusa.circuit_detail_section.PSPEED}
type name:lmp {eusa.circuit_detail_section.LMP}
type name:nu {eusa.circuit_detail_section.NU}
type name:bsc {eusa.circuit_detail_section.BSC}
type name:etet {eusa.circuit_detail_section.ETET}
type name:ctx_area {area({eusa.circuit_detail_section.CTX TEL})}
type name:ctx_exch {exch({eusa.circuit_detail_section.CTX TEL})}
type name:ctx_num {num({eusa.circuit_detail_section.CTX TEL})}
#type name:ipai {ipai}
#type name:ip_address {ip_address}
#type name:subnet_mask {subnet_mask}
type name:wacd1 {eusa.circuit_detail_section.WACD1}
type name:wacd2 {eusa.circuit_detail_section.WACD2}
type name:lagid {eusa.circuit_detail_section.LAG ID}
#type name:divckt {divckt}
#type name:divpon {divpon}
type name:priloc {eusa.primary_location_section.PRILOC}
type name:priloc_cfau {eusa.primary_location_section.CFAU}
type name:cfa {eusa.location_section.CFA}
type name:dir {eusa.primary_location_section.DIR}
type name:cpt1 {eusa.primary_location_section.CPT}
type name:cpt2 {eusa.primary_location_section.CPT}
type name:priloc_s25 {eusa.primary_location_section.S25}
type name:priloc_exr {eusa.location_section.ER}
type name:priloc_otc {eusa.location_section.OCT}
type name:geto {eusa.location_section.GETO}
type name:prigbtn_area {area({eusa.location_section.GBTN})}
type name:prigbtn_exch {exch({eusa.location_section.GBTN})}
type name:prigbtn_num {num({eusa.location_section.GBTN})}
type name:priloc_gcon {eusa.location_section.GCON}
type name:prigtel_area {area({eusa.location_section.GTEL})}
type name:prigtel_exch {exch({eusa.location_section.GTEL})}
type name:prigtel_num {num({eusa.location_section.GTEL})}
type name:prigtel_ext {ext({eusa.location_section.GTEL})}
type name:ccea {eusa.primary_location_section.CCEA}
#type name:ctx_lstd_nm {ctx_lstd_nm}
type name:serv_remarks {eusa.circuit_detail_section.REMARKS}
screenshot