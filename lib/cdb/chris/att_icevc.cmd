screenshot
type name:uref {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.UREF}
type name:evcnum {lpad({ethervc.ethervc_detail.EVC NUM},4,0)}
type name:nc {ethervc.ethervc_detail.NC}
#type name:evcid {evcid}
type name:nut {lpad({ethervc.ethervc_detail.NUT},2,0)}
#type name:evcckr {evcckr}
type name:icevcForm,name:uref {lpad({ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.UREF},2,0)}
#type name:aunt {aunt}
select name:uact value:{ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.UACT}
type name:rpon_uni {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.RPON}
type name:nci_uni {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.NCI}
type name:l2cp {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.L2CP}
type name:root_leaf {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.R/L}
type name:evcsp {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.EVCSP}
type name:ruid {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.RUID}
type name:cevlan {lpadif({ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.CE-VLAN.0},4,0)}
#type name:cevlan_2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan_2}
#type name:cevlan2_1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan2_1}
#type name:cevlan2_2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan2_2}
#type name:cevlan3_1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan3_1}
#type name:cevlan3_2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan3_2}
#type name:cevlan4_1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan4_1}
#type name:cevlan4_2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan4_2}
#type name:cevlan5_1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan5_1}
#type name:cevlan5_2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan5_2}
#type name:cevlan6_1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan6_1}
#type name:cevlan6_2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan6_2}
#type name:cevlan7_1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan7_1}
#type name:cevlan7_2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan7_2}
#type name:cevlan8_1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan8_1}
#type name:cevlan8_2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan8_2}
#type name:cevlan9_1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan9_1}
#type name:cevlan9_2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan9_2}
#type name:cevlan10_1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan10_1}
#type name:cevlan10_2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan10_2}
#type name:cevlan11_1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan11_1}
#type name:cevlan11_2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan11_2}
#type name:cevlan12_1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan12_1}
#type name:cevlan12_2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan12_2}
#type name:cevlan13_1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan13_1}
#type name:cevlan13_2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan13_2}
#type name:cevlan14_1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan14_1}
#type name:cevlan14_2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.cevlan14_2}
type name:pacevlan {lpadif({ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.CE-VLAN.0},4,0)}
#type name:pacevlan_2 {pacevlan_2}
#type name:pacevlan2_1 {pacevlan2_1}
#type name:pacevlan2_2 {pacevlan2_2}
#type name:pacevlan3_1 {pacevlan3_1}
#type name:pacevlan3_2 {pacevlan3_2}
#type name:pacevlan4_1 {pacevlan4_1}
#type name:pacevlan4_2 {pacevlan4_2}
#type name:pacevlan5_1 {pacevlan5_1}
#type name:pacevlan5_2 {pacevlan5_2}
#type name:pacevlan6_1 {pacevlan6_1}
#type name:pacevlan6_2 {pacevlan6_2}
#type name:pacevlan7_1 {pacevlan7_1}
#type name:pacevlan7_2 {pacevlan7_2}
#type name:pacevlan8_1 {pacevlan8_1}
#type name:pacevlan8_2 {pacevlan8_2}
#type name:pacevlan9_1 {pacevlan9_1}
#type name:pacevlan9_2 {pacevlan9_2}
#type name:pacevlan10_1 {pacevlan10_1}
#type name:pacevlan10_2 {pacevlan10_2}
#type name:pacevlan11_1 {pacevlan11_1}
#type name:pacevlan11_2 {pacevlan11_2}
#type name:pacevlan12_1 {pacevlan12_1}
#type name:pacevlan12_2 {pacevlan12_2}
#type name:pacevlan13_1 {pacevlan13_1}
#type name:pacevlan13_2 {pacevlan13_2}
#type name:pacevlan14_1 {pacevlan14_1}
#type name:pacevlan14_2 {pacevlan14_2}
type name:svlan_ind {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.S-VLAN IND}
type name:svlan {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.S-VLAN}
#type name:pa_svlan {pa_svlan}
#type name:evcmpid {evcmpid}
type name:lref1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS0.LREF}
type name:losact1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS0.LOSACT}
type name:los1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS0.LOS}
type name:spec1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS0.SPEC}
type name:pbit1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS0.P-BIT}
type name:bdw1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS0.BDW}
type name:dscp1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS0.DSCP}
type name:dscp1_2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS0.DSCP}
type name:tos1 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS0.TOS}
type name:lref2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS1.LREF}
type name:losact2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS1.LOSACT}
type name:los2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS1.LOS}
type name:spec2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS1.SPEC}
type name:pbit2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS1.P-BIT}
type name:bdw2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS1.BDW}
type name:dscp2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS1.DSCP}
type name:dscp2_2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS1.DSCP}
type name:tos2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS1.TOS}
type name:lref3 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS2.LREF}
type name:losact3 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS2.LOSACT}
type name:los3 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS2.LOS}
type name:spec3 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS2.SPEC}
type name:pbit3 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS2.P-BIT}
type name:bdw3 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS2.BDW}
type name:dscp3 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS2.DSCP}
type name:dscp3_2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS2.DSCP}
type name:tos3 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS2.TOS}
type name:lref4 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS3.LREF}
type name:losact4 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS3.LOSACT}
type name:los4 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS3.LOS}
type name:spec4 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS3.SPEC}
type name:pbit4 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS3.P-BIT}
type name:bdw4 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS3.BDW}
type name:dscp4 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS3.DSCP}
type name:dscp4_2 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS3.DSCP}
type name:tos4 {ethervc.uni_mapping_details.{ethervc.uni_mapping_details.index}.LREFS3.TOS}
type name:remarks_evc1 {ethervc.REMARKS}
screenshot
