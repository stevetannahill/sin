screenshot
type name:nc {eusa.circuit_detail_section.NC}
type name:nci {eusa.circuit_detail_section.NCI}
#type name:tlv_t {tlv_t}
#type name:tlv_r {tlv_r}
type name:secnci {eusa.circuit_detail_section.SECNCI}
type name:pqpr {eusa.circuit_detail_section.PQPR}
#type name:sectlv_t {sectlv_t}
#type name:sectlv_r {sectlv_r}
#type name:sr2 {sr2}
type name:s25 {eusa.circuit_detail_section.S25}
type name:exr {exr}
type name:sss {sss}
type name:atn {atn}
type name:trf {trf}
type name:mst {mst}
type name:hvp {hvp}
type name:cklt {cklt}
type name:nsl {nsl}
type name:etet {etet}
type name:muxloc {asr.admin_section.MUX}
type name:cfau {cfau}
type name:cfa {cfa}
type name:dir {dir}
type name:cpt1 {cpt1}
type name:cpt2 {cpt2}
type name:scfau {scfau}
type name:scfa {scfa}
type name:sdir {sdir}
type name:hban {hban}
type name:sfni {sfni}
type name:pri_adm {pri_adm}
type name:smuxloc {smuxloc}
type name:nvc {nvc}
type name:pspeed {pspeed}
type name:lmp {lmp}
type name:nu {nu}
type name:bsc {bsc}
type name:geto {geto}
type name:gbtn_area {gbtn_area}
type name:gbtn_exch {gbtn_exch}
type name:gbtn_num {gbtn_num}
type name:ccea {ccea}
type name:wacd1 {wacd1}
type name:lagid {lagid}
type name:wacd2 {wacd2}
type name:ipai {ipai}
type name:ip_address {ip_address}
type name:subnet_mask {subnet_mask}
type name:divckt {divckt}
type name:divpon {divpon}
type name:serv_remarks {ethervc.REMARKS.REMARKS}
screenshot