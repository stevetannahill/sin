screenshot
select name:action_code value:{action_code}
type name:refnum {sali.address_detail_section.REF NUM}
type name:priloc_pi {sali.address_detail_section.PI}
type name:pri_euname {sali.address_detail_section.EUNAME}
type name:priloc_aft {sali.address_detail_section.AFT}
type name:priloc_ncon {sali.address_detail_section.NCON}
type name:priloc_sapr {sali.address_detail_section.SAPR}
type name:priloc_sano {sali.address_detail_section.SANO}
type name:priloc_sasf {sali.address_detail_section.SASF}
type name:priloc_sasd {sali.address_detail_section.SASD}
type name:priloc_sasn {sali.address_detail_section.SASN}
type name:priloc_sath {sali.address_detail_section.SATH}
type name:priloc_sass {sali.address_detail_section.SASS}
type name:priloc_ld1 {sali.address_detail_section.LD1}
type name:priloc_lv1 {sali.address_detail_section.LV1}
type name:priloc_ld2 {sali.address_detail_section.LD2}
type name:priloc_lv2 {sali.address_detail_section.LV2}
type name:priloc_ld3 {sali.address_detail_section.LD3}
type name:priloc_lv3 {sali.address_detail_section.LV3}
type name:priloc_city {sali.address_detail_section.CITY}
type name:priloc_state {sali.address_detail_section.STATE}
type name:priloc_zip {sali.address_detail_section.ZIP}
type name:priloc_icol {sali.address_detail_section.ICOL}
type name:priloc_aai {sali.address_detail_section.AAI}
type name:priloc_jkcde {sali.address_detail_section.JK CODE}
type name:priloc_jknum {sali.address_detail_section.JK NUM}
type name:priloc_jkpos {sali.address_detail_section.JK POS}
type name:priloc_js {sali.address_detail_section.JS}
type name:priloc_smjk {sali.address_detail_section.SMJK}
type name:priloc_pca {sali.address_detail_section.PCA}
type name:priloc_si {sali.address_detail_section.SI}
type name:priloc_spot {sali.address_detail_section.SPOT}
type name:priloc_lcon {sali.address_detail_section.LCON}
type name:priloc_area {area({sali.address_detail_section.ACTEL})}
type name:priloc_exch {exch({sali.address_detail_section.ACTEL})}
type name:priloc_num {num({sali.address_detail_section.ACTEL})}
type name:priloc_ext {ext({sali.address_detail_section.ACTEL})}
type name:paactel_area {area({sali.address_detail_section.AACTEL})}
type name:paactel_exch {exch({sali.address_detail_section.AACTEL})}
type name:paactel_num {num({sali.address_detail_section.AACTEL})}
type name:paactel_ext {ext({sali.address_detail_section.AACTEL})}
#type name:priloc_lcon_email {priloc_lcon_email}
type name:pri_alcon {sali.address_detail_section.ALCON}
type name:palcon_area {area({sali.address_detail_section.ALCON TEL})}
type name:palcon_exch {exch({sali.address_detail_section.ALCON TEL})}
type name:palcon_num {num({sali.address_detail_section.ALCON TEL})}
type name:palcon_ext {ext({sali.address_detail_section.ALCON TEL})}
#type name:paalcon_area {area({paalcon})}
#type name:paalcon_exch {exch({paalcon})}
#type name:paalcon_num {num({paalcon})}
#type name:paalcon_ext {ext({paalcon})}
#type name:priloc_alcon_email {priloc_alcon_email}
type name:pri_acpgn {sali.address_detail_section.ACPGN}
type name:pri_acppn {sali.address_detail_section.ACPPN}
type name:priloc_acc {sali.address_detail_section.ACC}
type name:pri_wktel_area {area({sali.address_detail_section.WKTEL})}
type name:pri_wktel_exch {exch({sali.address_detail_section.WKTEL})}
type name:pri_wktel_num {num({sali.address_detail_section.WKTEL})}
screenshot