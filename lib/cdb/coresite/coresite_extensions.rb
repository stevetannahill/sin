# This is the CoreSite code to bring various objects into a Tested state
# It should create any needed SM, MTC and Alarm states. It should also 
# Configure the switches as needed

$cas_server_enabled = nil
$coresite_authentication = true
App.set :coresite

require 'pty'
require 'expect'

# I don't know why I have make sure the object are referenced but if I don't something
# funky happens with OnNetOvcEndPointEnni. The calling of the monitired? method calls
# the parent one - not the one defined in OnNetOvcEndPointEnni !!!

InstanceElement
Segment
OnNetSegment
OnNetOvc
OvcEndPoint
OvcEndPointEnni
Demarc
OnNetOvcEndPointEnni
SegmentEndPoint
OvcEndPointUni
EnniNew
CosTestVector
BxCosTestVector
SpirentCosTestVector
OperatorNetwork
ServiceProvider
OnNetOvcOrder
Contact
AggregationSite
CustomApplicationController
MemberAttrInstance
Path

$fake_output = false if !defined?($fake_output)
Rails.logger.info "Loading #{File.basename(__FILE__,".rb") } go inservice with alarm time #{$first_alarm_time}"

FalloutException.exception_types = %w(build)

Time::DATE_FORMATS[:requested_service_date] = "%Y-%m-%d"

class SaveError < Exception; end

class CustomApplicationController < ApplicationController

  before_filter :authorized_as_coresite, :only => [:import_tenants, :upload_tenants, :_show_add_port, :_show_edit_port, :_show_delete_port, :_create_port, :_update_port, :_delete_port, :_nodes_select, :genconfig, :_view_build_log]

  helper_method :select_options, :operator_networks, :service_rate_options, :entity_is_not_redacted?

  CORESITE_SERVICE_RATES = (5..50).step(5) | (50..100).step(10) | (100..500).step(50) | (500..1000).step(100) | (1000..10000).step(1000)

  # Helpers

  def select_options(options, value=nil, id="id", name="name")
    view_context.options_from_collection_for_select(options || [], id, name, value)
  end

  def operator_networks
    
    # Only show site sent in from Coresite portal
    if params[:operator_network].present?
      @national_tenant.operator_networks.where("id = ?", params[:operator_network])
    elsif coresite_role
      @national_tenant.try(:operator_networks) 
    else
     [OperatorNetwork.find(session[:tenant])]
    end

  end

  def service_rate_options(enni)
    CORESITE_SERVICE_RATES.take_while{ |sr| sr <= enni.rate }.map{ |sr| [humanize_bytes(sr), sr] }
  end

  # CoreSite Actions

  def show
    if entity_is_not_redacted?(@entity)
      render
    else
      forbidden
    end
  end

  def import_tenants
    render 'coresite_shared/upload_tenants'
  end

  def upload_tenants

    file = params[:tenants_file]
    if file.nil?
      flash[:alert] = "Please specify a Tenants file for upload"
      respond_to do |format|
        format.html { redirect_to(import_tenants_custom_application_index_path) }
      end
    else
      flash[:notice] = "Tenants file uploaded"
      data = file.read
      @output = StringIO.new
      @results = CoresiteBuildCenter.import_tenants(data, @output)
      @output = @output.string
      render 'coresite_shared/upload_tenants'
    end
  
  end

  def access_permissions
    @target_operator_network = if coresite_role 
      OperatorNetwork.find(params[:operator_network])
    else 
      @current_operator_network
    end

    render 'coresite_shared/access_permissions'
  end

  # Shared AJAX Actions

  def _update_access_permissions
    if permission = params[:permission]
      buying_operator_network = OperatorNetwork.find(params[:buyer])
      if permission == 'no_access' && (active_orders = @current_operator_network.active_orders_from(buying_operator_network).try(:length)) && active_orders > 0 
        result = {error: "#{@current_operator_network.tenant_name} must first accept or reject <strong>#{view_context.pluralize(active_orders, 'outstanding order')}</strong> from #{buying_operator_network.tenant_name}."}
      else
        begin
          # Full access, remove access permission row
          # PROGRAMMER NOTES: target_id should of been buyer_id
          access_permission = AccessPermission.find_or_initialize_by_operator_network_id_and_target_id(@current_operator_network.id, params[:buyer])
          if permission == 'full_access'
            access_permission.destroy unless access_permission.new_record?
          else
            access_permission.update_attributes({permission: permission.humanize.titleize})
          end
          result = {notice: "#{buying_operator_network.tenant_name}'s buying permissions have been updated to #{permission.humanize.titleize}."}
        rescue => e
          result = {error: "Failed to update permission."}
        end
      end
      respond_to do |format|
        format.json { render json: result }
      end
    end
  end

  def _locations
    respond_to do |format|
      format.html { render :partial => "coresite_shared/locations" }
    end
  end

  # CoreSite AJAX Actions

  def _show_add_port
    if params[:operator_network].present?
      @operator_network = OperatorNetwork.find(params[:operator_network]) 
      @site = @operator_network.site
      @form_action, @action, @method = view_context._create_port_custom_application_index_path(), :new, :post
      respond_to do |format|
        format.html { render :partial => 'coresite_shared/port_form' }
      end
    end
  end

  def _show_edit_port
    if params[:enni].present?
      init_enni
      @form_action, @action, @method = view_context._update_port_custom_application_index_path(:enni => @enni), :edit, :put
      respond_to do |format|
        format.html { render :partial => 'coresite_shared/port_form' }
      end
    end
  end

  def _show_delete_port
    if params[:enni].present?
      init_enni     
      # Cannot use method delete as it assumes a destroy action
      @form_action, @action, @method = view_context._delete_port_custom_application_index_path(:enni => @enni), :delete, :put
      respond_to do |format|
        format.html { render :partial => 'coresite_shared/port_form' }
      end
    end
  end

  def _create_port

    result = CoresiteBuildCenter.new.enni_order(order_info)

    message = if result[:summary] == 'Failed'
      { :status => :error, :message => "Failed to create #{get_term(:enni)}: #{result[:summary_detail]}" }
    else
      { :status => :success, :message => "#{get_term(:enni)} created successfully" }
    end

    respond_to do |format|
      format.json { render :json => message }
    end

  end


  def _update_port

    result = CoresiteBuildCenter.new.enni_update_order(order_info)

    message = if result[:summary] == 'Failed'
      { :status => :error, :message => "Failed to update #{get_term(:enni)}: #{result[:summary_detail]}" }
    else
      { :status => :success, :message => "#{get_term(:enni)} updated successfully" }
    end

    respond_to do |format|
      format.json { render :json => message }
    end

  end

  def _delete_port
    if params[:enni].present?
      enni = EnniNew.find(params[:enni]) 
      order_info = {:nni_id => enni.id, :work_order => enni.coresite_member_handle}

      result = CoresiteBuildCenter.new.enni_disconnect_order(order_info)

      message = if result[:summary] == 'Failed'
        { :status => :error, :message => "Failed to delete #{get_term(:enni)}: #{result[:summary_detail]}" }
      else
        { :status => :success, :message => "#{get_term(:enni)} deleted successfully" }
      end

      respond_to do |format|
        format.json { render :json => message }
      end
    end
  end

  def _nodes_select
    # TODO: Add security to these selects
    if params[:parent_id].present?
      @site = AggregationSite.find(params[:parent_id])
      respond_to do |format|
        format.html { render :inline => select_options(@site.try(:coresite_nodes)) }
      end
    end
  end

  def authorized_as_coresite
    forbidden unless coresite_role
  end


  protected

  def entity_is_not_redacted?(entity)
    operator_network = if entity.is_a?(Demarc)
      entity.operator_network
    elsif entity.is_a?(SegmentEndPoint)
      entity.demarc.operator_network
    else
      operator_network = @current_operator_network
    end
    coresite_role || operator_network == @current_operator_network
  end

  private

  def init_enni
    @enni = EnniNew.find(params[:enni]) 
    @operator_network = @enni.operator_network
    @site = @operator_network.site
    port = @enni.ports.first
    params[:node] ||= port.node.name
    params[:port] ||= port.name
    params[:work_order_number] ||= @enni.coresite_member_handle
    params[:external_name] ||= @enni.member_handle_for_owner_object(@enni.operator_network.service_provider).try(:value)
    params[:phy_type] ||= @enni.physical_medium
  end

  def order_info
    # TODO: Validation and security
    if params['operator_network'].present? && params['node'].present?
      
      @operator_network = OperatorNetwork.find(params['operator_network']) 
      @node = Node.find(params['node'])

      # Build an ENNI
      {
        :tenant_name => @operator_network.id, 
        :node => @node.id, 
        :port => params[:port], 
        :work_order => params[:work_order_number],
        :external_name => params[:external_name],
        :phy_type => params[:phy_type]
      }
    end
  end

end

module ApplicationHelper

  def cancel_order_button(order, operator_network, member_handle, text='Cancel Order')
    if order.can_be_cancelled_by_role?(operator_network)
      link_to text, cancel_coresite_order_path(order.id), confirm: "Are you sure you want to cancel this order?", method: :put,:class => 'submit_button activity negative danger blocking', :title => 'Cancel Order', 'data-blocking_message' => "<h1>Cancelling Order (#{member_handle}).</h1><h2>This may take a moment.</h2>"
    end
  end

  def disconnect_order_button(order, operator_network, member_handle, text='Disconnect')
    if order.can_be_disconnected_by_role?(operator_network)
      link_to text, disconnect_coresite_order_path(order.id), confirm: "Are you sure you want to disconnect this order?", method: :delete, :class => 'submit_button activity negative danger blocking', :title => 'Disconnect Path', 'data-blocking_message' => "<h1>Disconnecting #{get_term(:path)} associated to Order (#{member_handle}).</h1><h2>This may take a moment.</h2>"
    end
  end

  def change_order_button(order, operator_network, text='Create Change Order')
    if order.can_be_changed_by_role?(operator_network)
      link_to text, change_coresite_order_path(order.id), :class => 'submit_button activity', :title => 'Change Order'
    end
  end

  def reject_order_button(order, operator_network, member_handle, text='Reject Order')
    if order.can_be_rejected_by_role?(operator_network)
      link_to text, reject_coresite_order_path(order.id), :class => 'reject_order_btn submit_button negative danger', :title => 'Reject Order'
    end
  end

  def count_paths_by_state(paths)
    groups = paths.group_by do |path|
      # PROGRAMMERS NOTE: Leave conditions in this order (the evc is pending even if it has been rejected or cancelled)
      if path.is? :live
        :live
      elsif path.is? :deleted
        :deleted
      elsif path.on_net_ovc.try(:rejected?) || path.on_net_ovc.try(:cancelled?)
        :rejected_cancelled
      elsif path.is? :pending
        :pending
      end
    end

    # return hash with counts (filled with zero if none found)
    Hash[%w(live deleted rejected_cancelled pending).map{|g| [g.to_sym,0]}].merge(Hash[groups.map { |key,vals| [key, vals.count] }])
  end

end


class Path < InstanceElement

  def is?(state)
    prov_name.try(:downcase).to_sym == state
  end

  def on_net_ovc
    on_net_ovcs.try(:first)
  end

end

class InstanceElement < ActiveRecord::Base

  # Coresites member handle (external_name)
  def coresite_member_handle
     member_handle_for_owner_object(ServiceProvider.system_owner).try(:value)
  end

end

class AggregationSite < Site

  # Get all sites in same site group
  has_many :site_group_sites, :through => :site_groups, :source => :sites

  def accessible_operator_networks_by_buying_operator_network(buying_operator_network)
    OperatorNetwork.by_site_name(name) - buying_operator_network.no_access_operator_networks - [buying_operator_network]
  end

  def coresite_nodes
    nodes.select(&:is_coresite_node)
  end

end

class Demarc < InstanceElement
  scope :live, where('demarcs.prov_name = "Live"')
end

class OperatorNetwork < InstanceElement

  # Sites are connected to operator by a naming convention, not a traditional foreign key
  scope :by_site_name, lambda { |site_name| where("operator_networks.name LIKE ?", "%[#{site_name}]") }
  scope :coresite_by_site_name, lambda { |site_name| where("operator_networks.name LIKE ?", "CoreSite [#{site_name}]%") }

  has_many :on_net_ovc_orders, :class_name => "OnNetOvcOrder", :conditions => "service_orders.type = 'OnNetOvcOrder'"

  def tenant_name
    tenant_name = name.match /([^\[]*)/
    tenant_name ? tenant_name[1].strip : nil
  end

  def site
    AggregationSite.find_by_name(site_name) if site_name
  end

  def site_name 
    site_name = name.match /\[([^\]]*)\]/
    site_name ? site_name[1].strip : nil
  end

  def national_tenant
    service_provider
  end

  def live_ennis
    enni_news.live
  end

  def buying_orders(filters={})
    buying_orders = filter_orders(on_net_ovc_orders, filters)

    # TODO: Need a current flag on the service orders table
    buying_orders.group_by(&:ordered_entity_id).each_with_object([]) do |(i, group), result|
      sorted = group.sort_by(&:created_at)
      result << {latest: sorted.pop, history: sorted}
    end
  end

  def target_orders(filters={})
    target_orders = filter_orders(OnNetOvcOrder.where("service_orders.notes LIKE ?", "%#{name}%"), filters)

    # TODO: Need a target_operator_network_id on service_orders table
    target_orders.group_by(&:ordered_entity_id).each_with_object([]) do |(i, group), result|
      sorted = group.sort_by(&:created_at)
      result << {latest: sorted.pop, history: sorted}
    end
  end

  def engineering_contact
    service_provider.contacts.engineering(site_name).first
  end

  def billing_contact
    service_provider.contacts.billing(site_name).first
  end

  def coresite_contact
    # CoreSite contact is not associated to operator network
    ServiceProvider.system_owner.contacts.coresite.first
  end

  def valid_access_permission_targets
    OperatorNetwork.all.reject{|on| (on.site.site_groups & site.site_groups).empty? } - [self]
  end

  def operator_networks_by_site_group
    site.try(:site_group_sites).map{ |site| OperatorNetwork.by_site_name(site.name) }.flatten - [self]
  end

  # Get Buying tenants access permissions for target tenant
  def access_permission_for_buyer(buying_operator_network)
    # Use an indexed hash instead of array for fast lookups
    if access_permission = access_permissions.for_buyer(buying_operator_network).first
      access_permission.permission.parameterize.underscore.to_sym
    else
      :full_access
    end
  end

  # Get Buying tenants access permissions for target tenant
  def access_permission_given_by_target(target_operator_network)
    # Use an indexed hash instead of array for fast lookups
    if access_permission = target_operator_network.access_permissions.for_buyer(self).first
      access_permission.permission.parameterize.underscore.to_sym
    else
      :full_access
    end
  end

  # Does target tenant auto accept from buying tenant?
  def auto_accept?(target_operator_network)
    access_permission_given_by_target(target_operator_network) == :full_access
  end

  def active_orders_from(buying_operator_network)
    target_orders.map{ |o| o[:latest] }.select{|o| o.active? && o.buying_operator_network.try(:id) == buying_operator_network.id  }
  end

  def filter_orders(orders, filters)
    filters.each do |name, value|
      unless value.blank?
        case name.downcase
        when 'provisioning_state'
          # Special case for fallout state on inventory
          where = value.downcase == 'fallout' ? 'paths.fallout_severity != "ok"' : 'paths.prov_name = ?'
          orders = orders.joins(:path).where(where, value)
        end
      end
    end
    orders
  end

end

class ServiceProvider < ActiveRecord::Base

  def sites
    operator_networks.map(&:site)
  end

  def ennis
    operator_networks.map(&:enni_news).flatten
  end

  def buying_orders(filters={})
    operator_networks.map{|o| o.buying_orders(filters)}.flatten
  end

  def target_orders(filters={})
    operator_networks.map{|o| o.target_orders(filters)}.flatten
  end

end

class Contact < ActiveRecord::Base

  # TODO: Replace with real criteria, pass in site name
  scope :by_site_name, lambda { |site_name| where("operator_networks.name LIKE ?", "%[#{site_name}]") }

  scope :engineering, lambda { |site_name| where("name LIKE ?", "%[#{site_name}]").where(:position => 'Technical') }
  scope :billing, lambda { |site_name| where("name LIKE ?", "%[#{site_name}]").where(:position => 'Primary') }
  scope :coresite, where(:position => "Product Management")

  def contact_name
    contact_name = name.match /([^\[]*)/
    contact_name ? contact_name[1].strip : nil
  end

end

class OnNetOvcOrder < OvcOrder

  has_many :automated_build_logs, :as => :associated_object, :dependent => :destroy
  
  def path
    ordered_entity.paths.first
  end

  def service_rate
    notes.match(/NewCIR:\s*(\d*)/).try(:[], 1) || buying_end_point.try(:service_rate)
  end

  def segment_service_rate
    buying_end_point.try(:service_rate)
  end

  def segment_end_points
    ordered_entity.segment_end_points
  end

  def buying_end_point
    segment_end_points.select{ |ep| ep.enni.try(:operator_network_id) == operator_network_id }.first
  end

  def target_end_point
    segment_end_points.reject{ |ep| ep.id == buying_end_point.try(:id) }.first
  end

  def buying_enni
    buying_end_point.try(:enni)
  end

  def target_enni
    target_end_point.try(:enni)
  end

  def buying_operator_network
    buying_end_point.try(:demarc).try(:operator_network)
  end

  # Find target tenant for order
  def target_operator_network
    target_operator_network_name = notes.match(/.*\[[^\]]*\]$/)[0]
    OperatorNetwork.find_by_name(target_operator_network_name)
  end

  def disconnected?
    action.downcase == 'disconnect'
  end

  def change_order?
    action.downcase == 'change'
  end

  def active?
    ordered_entity.active_order? && !disconnected?
  end

  def rejected?
    order_name.try(:downcase) == 'rejected'
  end

  def cancelled?
    order_name.try(:downcase) == 'cancelled'
  end

  def not_active?
    !ordered_entity.active_order? && ordered_entity.live?
    # order_name == 'Delivered' && !disconnected?
  end

  def can_be_changed_by_role?(current_operator_network)
    buying_role(current_operator_network) && not_active? 
  end

  def can_be_disconnected_by_role?(current_operator_network)
    buying_role(current_operator_network) && not_active?
  end

  def can_be_cancelled_by_role?(current_operator_network)
    buying_role(current_operator_network) && active? 
  end

  def can_be_accepted_by_role?(current_operator_network)
    target_role(current_operator_network) && active?
  end

  def can_be_rejected_by_role?(current_operator_network)
    can_be_accepted_by_role?(current_operator_network) && active?
  end

  def can_be_deleted_by_role?(current_operator_network)
     buying_role(current_operator_network) && (rejected? || cancelled?)
  end
  
  def buying_role(current_operator_network)
    @buying_role = current_operator_network.try(:id) == buying_operator_network.id if @buying_role.nil?
    @buying_role
  end

  def target_role(current_operator_network)
    @target_role = current_operator_network.try(:id) == target_operator_network.id if @target_role.nil?
    @target_role
  end

end


class OnNetSegment < Segment
  def service_rate
    segment_end_points.try(:first).try(:service_rate)
  end

  def live?
    prov_name.try(:downcase) == 'live'
  end

  def rejected?
    order_name.try(:downcase) == 'rejected'
  end

  def cancelled?
    order_name.try(:downcase) == 'cancelled'
  end

  def get_alarm_keys      
    return true, ["#{self.class}:#{self.cenx_id}-configuration"]
  end


  def go_in_service
    puts "CoreSite #{self.class.to_s}:#{self.id} go_in_service" if $fake_output
    info = ""
    
    alarm_time = $first_alarm_time ? $first_alarm_time : Time.now 
    return {:result => false, :info => "Failed to initialise"} if on_net_ovc_end_point_ennis.size == 0
  
    result = true
    config_result = nil
    if result
      sm_histories << SmHistory.create(:event_filter => "SM Segment:#{cenx_id}") if sm_histories.empty?
      # Create a new Maintenance with a record being cleared ie not in maintenance period
      if mtc_periods.empty?
        self.mtc_periods = [MtcHistory.create(:event_filter => "Mtc Segment:#{cenx_id}")]
      end
      self.set_mtc(false, "go_in_service") if mtc_periods.first.alarm_records.size == 0
            
      result, test_object_names = get_alarm_keys
      if result
        if !test_object_names.empty?
          new_alarms = {} 
          test_object_names.each {|filter| new_alarms[filter] = {:type => GenericHistory, :mapping => GenericHistory.default_alarm_mapping}}
          if update_alarm_history_set(new_alarms)
            # Even if the configuration fails don't fail! carry-on
            router_cmds = self.generate_config_hash
            router_cmds.each do |node, config|
              config_result, output = CoresiteConfigureNode.configure(node, config)
              result = result && (config_result == CoresiteConfigureNode::CONFIGURE_OK)
              info << output << "\n"
              break unless result      
            end
            unless result
              # Log commands that should of been executed
              router_cmds.each do |node, config|
                info << "\n\nCommands which should be of been executed on #{node.name}(#{node.primary_ip})\n#{config}\n"
              end
            end
            # Set alarm to be Cleared or Failed based on output of script 
            self.alarm_histories.first.add_event(Time.now.to_f*1000, result ? "cleared" : "failed", CoresiteConfigureNode.failure_reason(config_result))
            # Always continue if the script failed
            result = true
          end
        end 
      end
    end    
    
    info = "Failed to initialise" if !result && info.blank?
    return {:result => result, :info => info}
  end

  def generate_config_info
    config = {}

    #Pregenerate all the Nodes corresponding vpls-peer lines
    nodes_involved.each do |node|
      config[node] = {:vpls_peers => []}

      other_nodes = nodes_involved - [node]
      other_nodes.each do |other_node|
        config[node][:vpls_peers] << {
          :lsp => node.get_vpls_peer(other_node.loopback_ip), 
          :loopback_ip => other_node.loopback_ip
        }
      end
    end

    #Gather all VLAN and port info for all segment endpoints
    segment_end_points.each do |segment_endpoint|
      segment_endpoint.generate_end_point_config config
    end

    return config
  end

  def get_config_service_rate
    service_rate = segment_end_points.collect{|sep| sep.cos_end_points.map{|cos| cos.ingress_cir_kbps}}.flatten.uniq.first
    service_rate = service_rate.gsub(/000000$/, "Gbps").gsub(/000$/, "Mbps")
    return service_rate
  end

  def generate_vpls_title
    #Build up the VPLS title from several locations
    buyer = paths.first.operator_network.tenant_name
    target = (ennis.map{|e| e.operator_network.tenant_name} - [buyer]).first
    target = target.nil? ? buyer : target
    buyer.gsub!(/[^\w\d]/, "") #Remove non alphanumeric characters from names
    target.gsub!(/[^\w\d]/, "")
    wo = self.member_handle_instance_by_owner(ServiceProvider.system_owner).value  
    service_rate = get_config_service_rate
    evc_num = 1

    vpls_suffix = "#{wo}-#{service_rate}-#{evc_num}"
    d = 64 - vpls_suffix.length-2
    buyer = buyer[0..(d/2-1)]
    target = target[0..(d/2-1)]
    vpls_title = "#{buyer}-#{target}-#{vpls_suffix}"
    return vpls_title
  end

  def generate_config_hash
    config = generate_config_info

    #If there is a CES node involved the config must be in vpls raw mode
    node_types = nodes_involved.map{|n| n.node_type}
    raw_mode = (node_types.include?("Brocade CES 2024") or node_types.include?("Brocade CES 2048"))

    vpls_title = generate_vpls_title
    service_rate = get_config_service_rate

    #Generate the config text for each node
    output_config = {}
    nodes_involved.each do |node|
      config_text =  "configure terminal\n"
      config_text += "router mpls\n"
      config_text += "vpls #{vpls_title} #{service_id} cos 5\n"
      config_text += "vc-mode tagged\n" unless raw_mode

      config[node][:vpls_peers].each do |vpls_peer|
        config_text += "vpls-peer #{vpls_peer[:loopback_ip]} lsp #{vpls_peer[:lsp]}\n"
      end

      config[node][:vlans].each do |vlan, ports|
        config_text += "vlan #{vlan}\n"
        config_text += "tagged ethe #{ports.join(" ethe ")}\n"
        config_text += "exit\n"
      end

      config[node][:vlans].each do |vlan, ports|
        ports.each do |port|
          config_text += "interface ethernet #{port}\n"
          config_text += "rate-limit input access-group #{vlan} policy-map #{service_rate}\n"
          config_text += "exit\n"
        end
      end

      config_text += "write mem\n"

      output_config[node] = config_text
    end

    return output_config
  end

  def generate_config_text
    return generate_config_hash.inject(""){|memo, (k,v)| memo += "CONFIG FOR NODE: #{k.name}\n--------------------------\n#{v}\n\n"}
  end

  def generate_delete_config_hash
    config = generate_config_info
    vpls_title = generate_vpls_title
    service_rate = get_config_service_rate

    output_config = {}
    nodes_involved.each do |node|
      config_text =  "configure terminal\n"
      config_text += "router mpls\n"
      config_text += "no vpls #{vpls_title} #{service_id}\n"

      config[node][:vlans].each do |vlan, ports|
        ports.each do |port|
          config_text += "interface ethernet #{port}\n"
          config_text += "no rate-limit input access-group #{vlan} policy-map #{service_rate}\n"
          config_text += "exit\n"
        end
      end

      config_text += "write mem\n"
      output_config[node] = config_text
    end
    return output_config
  end

  def generate_delete_config_text
    return generate_delete_config_hash.inject(""){|memo, (k,v)| memo += "DELETE CONFIG FOR NODE: #{k.name}\n--------------------------\n#{v}\n\n"}
  end

  def generate_config
    return generate_config_text + generate_delete_config_text
  end
end

class OnNetOvc < OnNetSegment
end

class BxCosTestVector < CosTestVector   
  def go_in_service
    puts "CoreSite #{self.class.to_s}:#{self.id} go_in_service" if $fake_output
    info = ""
    alarm_time = $first_alarm_time ? $first_alarm_time : Time.now 
    return_value = true 
    if cos_end_point.segment_end_point.is_monitored
      if alarm_histories.empty?
        alarm_histories << BrixAlarmHistory.create(:event_filter => "CoreSite CTV #{self.id}")
        alarm_histories.first.add_event((alarm_time.to_f*1000).to_i.to_s, "cleared", "")
      end      
    else
      # It's not monitored so delete any history
      alarm_histories.destroy_all
    end  
    info = "Failed to initialise" unless return_value
    return {:result => return_value, :info => info}
  end
end

class SpirentCosTestVector < CosTestVector  
  def go_in_service
    puts "CoreSite #{self.class.to_s}:#{self.id} go_in_service" if $fake_output
    info = ""
    alarm_time = $first_alarm_time ? $first_alarm_time : Time.now 
    return_value = true 
    if cos_end_point.segment_end_point.is_monitored
      if alarm_histories.empty?
        if AvailabilityHistory.exists?(:event_filter => "#{circuit_id}-Availability")
          alarm_histories << AvailabilityHistory.find_by_event_filter("#{circuit_id}-Availability")
        else
          alarm_histories << AvailabilityHistory.create(:event_filter => "#{circuit_id}-Availability")
          alarm_histories.first.add_event((alarm_time.to_f*1000).to_i.to_s, "cleared", "")
        end
      end      
    else
      # It's not monitored so delete any history
      alarm_histories.destroy_all
    end  
    info = "Failed to initialise" unless return_value
    return {:result => return_value, :info => info}
  end
end

class OnNetOvcEndPointEnni < OvcEndPointEnni
  
  def go_in_service
    puts "CoreSite #{self.class.to_s}:#{self.id} go_in_service" if $fake_output
    info = ""
    alarm_time = $first_alarm_time ? $first_alarm_time : Time.now 
    return_code = true
    if is_connected_to_test_port 
      # Delete any existing histories
      alarm_histories.destroy_all
      sm_histories.destroy_all
      info = "Failed to initialise" unless return_code
      return {:result => return_code, :info => info}
    end

    sm_histories << SmHistory.create(:event_filter => "SM EndPoint:#{cenx_id}") if sm_histories.empty?

    if alarm_histories.empty?
      alarm_histories << AlarmHistory.create(:event_filter => "CoreSite EP:#{cenx_id}", :alarm_mapping => AlarmSeverity.default_alu_alarm_mapping)
      alarm_histories.first.add_event((alarm_time.to_f*1000).to_i.to_s, "cleared", "")
    end
    info = "Failed to initialise" unless return_code
    return {:result => return_code, :info => info}
  end  
  
  def alarm_state        
    super
  end
  
  def alarm_history    
    super
  end

  def suggest_valid_stag
    result = true
    valid_stag = 400

    unless is_connected_to_enni
      result = false
    else
      valid_stag = 400
      while demarc.has_endpoint_with_stag valid_stag.to_s
         valid_stag += 1
         if valid_stag > 599
           result = false
           break
         end
      end
    end

    return result, valid_stag
  end
  
  def valid_tag_range?
    if is_connected_to_enni
      return if (stags == "TBD" or stags == "N/A")
      if stags == nil || (stags.to_i < 400 || stags.to_i > 599)
        errors.add(:stags, "Should be 400-599 or TBD (was #{stags})")
      end
    else
      return true
    end
  end

  def generate_end_point_config config
    nodes_involved.each do |node|
      node.generate_config config[node], self
    end
  end
end

class EnniNew < Demarc

  def suggest_valid_stag
    result = true
    valid_stag = 400
    while has_endpoint_with_stag valid_stag.to_s
       valid_stag += 1
       if valid_stag > 599
         result = false
         break
       end
    end

    return result, valid_stag

  end

  def coresite_provisioned_bandwidth
    get_provisioned_bandwidth.try(:[], :grand).try(:[], :total)
  end

  def go_in_service    
    puts "CoreSite #{self.class.to_s}:#{self.id} go_in_service" if $fake_output
    info = ""
    alarm_time = $first_alarm_time ? $first_alarm_time : Time.now 
    return_code = true
    if is_connected_to_test_port
      # delete alarm and mtc histories
      mtc_periods.destroy_all
      alarm_histories.destroy_all
      sm_histories.destroy_all
      return {:result => true, :info => info}
    end
        
    # create alarm and service monitoring histories
    sm_histories << SmHistory.create(:event_filter => "SM ENNI:#{cenx_id}") if sm_histories.empty?
    
    # create the Maintenance history    
    if return_code && mtc_periods.empty?
      mtc_periods << MtcHistory.create(:event_filter => "Mtc ENNI:#{cenx_id}")
      self.set_mtc(false, "go_in_service")
    end
    
    if alarm_histories.empty?      
      alarm_histories << AlarmHistory.create(:event_filter => "CoreSite ENNI:#{cenx_id}", :alarm_mapping => AlarmSeverity.default_alu_alarm_mapping)
      alarm_histories.each {|a| a.add_event((alarm_time.to_f*1000).to_i.to_s, "cleared", "")}
    end
    info = "Failed to initialise" unless return_code
    return {:result => return_code, :info => info}
  end
    
  def alarm_state        
    super
  end
  
  def alarm_history    
    super
  end
  
end


class OvcEndPointUni < OvcEndPoint

  def get_alarm_keys
    return_code, new_mep_alarms = [true, ["CoreSite EP #{self.class.name}:#{cenx_id}"]]                      
    return return_code, new_mep_alarms
  end

  def go_in_service
    puts "CoreSite #{self.class.to_s}:#{self.id} go_in_service" if $fake_output
    info = ""
    alarm_time = $first_alarm_time ? $first_alarm_time : Time.now 
    
    return_code = super         
    if return_code[:result]
      if alarm_histories.empty?
        return_code[:result], new_alarms = get_alarm_keys
        if return_code[:result]          
          new_alarms = {} 
          new_alarms.each {|filter|
            alarm_histories << AlarmHistory.create(:event_filter => filter, :alarm_mapping => AlarmSeverity.default_alu_alarm_mapping)
            alarm_histories.last.add_event((alarm_time.to_f*1000).to_i.to_s, "cleared", "")          
          }
        end
      end
    end
    
    return_code[:info] = "Failed to initialise" if !return_code[:info] && return_code[:info].blank?  
    return return_code
  end
  
  def alarm_state        
    super
  end
  
  def alarm_history    
    super
  end
  
end

class SegmentEndPoint < InstanceElement

  def service_rate
    cos_end_points.try(:first).ingress_cir_Mbps
  end

  def vlan
    outer_tag_value
  end

  def go_in_service
    puts "CoreSite #{self.class.to_s}:#{self.id} go_in_service"  if $fake_output
    info = ""
    alarm_time = $first_alarm_time ? $first_alarm_time : Time.now 
    sm_histories << SmHistory.create(:event_filter => "SM #{self.class.name}:#{cenx_id}") if sm_histories.empty?
    
    return_code = true
    if alarm_histories.empty?    
      # None
    end
    
    if return_code
      return_code = cos_end_points.all? {|cep|
        cep_result = ep.go_in_service
        info << cep_result[:info]
        cep_result[:result]
      }
    end
    info = "Failed to initialise" if !return_code && info.blank?  
    return {:result => return_code, :info => info}
  end
  
  def alarm_state        
    super
  end
  
  def alarm_history    
    super
  end
  
end

class Node < ActiveRecord::Base

  def is_coresite_node
    SOLARWIND_TYPES.include?(node_type)
  end
  
  def generate_config config, segment_endpoint
    ports = segment_endpoint.demarc.ports.select{|p| p.node == self}
    
    config[:vlans] = {} if config[:vlans].nil?
    config[:vlans][segment_endpoint.stags] ||= []
    config[:vlans][segment_endpoint.stags] += ports.map{|p| p.name}
  end

  def get_vpls_peer loopback_ip
    vpls_peer = ""
    # begin
    #   PTY.spawn("telnet #{primary_ip}") do |pty_out, pty_in, pid|
    #     pty_out.expect "Login Name"
    #     pty_in.printf "cenx\r\n"
    #     pty_out.expect "Password"
    #     pty_in.printf "cenx123\r\n"
    #     pty_out.expect "#"
    #     pty_in.printf "show mpls lsp wide\r\n"
    #     vpls_peer = pty_out.expect(/([\w\d-]*)\s*#{loopback_ip}/m)[1]
    #   end
    # rescue
    #   # Catch any IO excptions on timeouts 
    #   SW_ERR "Exception in get_vpls_peer #{$!}"
    # end
    cmds = "skip-page-display\nshow mpls lsp wide\n"
    result, output = CoresiteConfigureNode.configure(self, cmds)
    vpls_peer = output.match(/([\w\d-]*)\s*#{loopback_ip}/m).try(:[], 1)
    return vpls_peer
  end
end

class Port < ActiveRecord::Base
  validates_format_of :name, :with => /(^\d{1,2}\/\d{1,2}$)/, :scope => :node_id, :if => Proc.new { |port| port.node && Node::SOLARWIND_TYPES.include?(port.node.node_type)}, :message => "of port is invalid, port names must have format a/b"
end

class MemberAttrInstance < ActiveRecord::Base
  validates_uniqueness_of :value, :scope => [:owner_id, :owner_type, :affected_entity_type], :unless => Proc.new {|mai| mai.value == "TBD" || mai.value == "N/A"}
end

# Order state changes

OrderReadyToOrder
class OrderReadyToOrder < OrderState 
  def valid_transitions
    transitions = [OrderReadyToOrder, OrderReceived, OrderRejected, OrderCancelled]
    # If there is an ordered object and it's provisioned state is not Pending or Maintenance then disallow OrderReceived state
#    so = stateful.stateful_ownership.stateable
#    ordered_object = so.ordered_entity
#    if ordered_object != nil
#      if !(ordered_object.get_prov_state.is_a?(ProvPending) || ordered_object.get_prov_state.is_a?(ProvMaintenance))
#        transitions.delete(OrderReceived)
#      end
#    end
    return transitions 
  end
end

ProvPendingSegment
class ProvPendingSegment < ProvPending 
  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    segment = stateful.stateful_ownership.stateable
    # set order to to itself if an order exists. This is the first provisioning state so just make sure
    # the post conditions are run
    latest_order = segment.get_latest_order
    if latest_order != nil
      if !update_state_and_failed_objs(latest_order.order_state, latest_order.order_state.to_generic(latest_order.get_order_state), failed_objects, true)
        result = false
      end
      # If the state is Created and it's a OnNetOvc then automatically move to Accepted state
      if result && latest_order.get_order_state.is_a?(OrderCreated)
        if segment.is_a?(OnNetOvc)
          pre_result, post_result, pre_failed_objects, post_failed_object = latest_order.order_state.drive_state(OrderReceived.new)
          if !(pre_result && post_result)
            result = false
            failed_objects << pre_failed_objects << post_failed_object
          end
        end
      end
    end
    return result
  end
end

ProvMaintenanceSegment
class ProvMaintenanceSegment < ProvMaintenance
    def pre_conditions(stateful, previous_state, failed_objects = [])
      result = super
  #    segment = stateful.stateful_ownership.stateable
      # If there is an active order then it should be set to Created
  #    latest_order = segment.get_latest_order
  #    if latest_order != nil
  #      if segment.active_order?
  #        if !latest_order.get_order_state.is_a?(OrderCreated)
  #          failed_objects << {:obj => segment, :reason => "Active order is not in Created state"}
  #          result = false
  #        end
  #      end
  #    end    

      return result
    end
  
  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    segment = stateful.stateful_ownership.stateable
    # If there is an active order then set the order to OrderReceived
    latest_order = segment.get_latest_order
    if latest_order != nil
      if latest_order.get_order_state.is_a?(OrderCreated)
        case latest_order.action
          when "Disconnect"
            new_order_state = OrderTested.new
          else
            new_order_state = OrderReceived.new
        end
        pre_result, post_result, pre_failed_objects, post_failed_objects = latest_order.order_state.drive_state(new_order_state)
        if !pre_result
          result = false
          failed_objects << pre_failed_objects
        end
        if !post_result
          result = false
          failed_objects << post_failed_objects
        end
      end
    end
    
    segment.set_mtc(true)
    
    return result
  end
end
