require "net/http"
require "uri"

# Log the date and time
MAX_LOG_SIZE = 10*1024*1024
class FormatLogger < Logger
  def format_message(severity, timestamp, progname, msg)
    "#{timestamp.to_s} #{severity} #{msg}\n"
  end
end


class CoresiteBuildCenter
  BUILD_LOG_SEP = ("="*80)
  FE_SEVERITY_MAPPING = {"Ok" => :ok, "Failed" => :critical, "Fallout" => :critical}
  attr_accessor :poe_dir
  attr_reader :build_details
  attr_accessor :stub_options
  
  @@logger = Rails.logger

  @@logger = FormatLogger.new("#{Rails.root}/log/#{File.basename(__FILE__, '.*')}.log", 1, MAX_LOG_SIZE)
  @@logger.level = (Rails.env == "development") ? Logger::Severity::DEBUG : Logger::Severity::INFO
  
  def logger
    @@logger
  end
  
  def osl_app
    if @osl_app.nil?
      log_file = Tempfile.new(["osl_output_", ".log"], Rails.root.to_s + "/tmp")
      log_file.close
      @osl_app = Eve::Application.new(:config => "#{Rails.root}/config/osl.poe.yml", :display => {:filename => log_file})
      OslApp.register(@osl_app)
    end
    @osl_app
  end
  
  def initialize
    @poe_dir = Rails.root.to_s + "/tmp/poe"
    Dir::mkdir(Rails.root.to_s + "/tmp") unless File.exists?(Rails.root.to_s + "/tmp") 
    Dir::mkdir(@poe_dir) unless File.exists?(@poe_dir) 
    @build_details = ""
    @osl_output = true
    @stub_options = {}
    @stub_oss = CoresiteConfig::instance.config["oss_url"].blank?  
    if @stub_oss
      @stub_options = {:router_config => :ok, :oss => :ok, :contact => :ok}
    end  
  end
  
  # order_info = {:tenant_a => on_a.id, :nni_a => nni_a.id, :vlan_a => 46, :member_handle_a =>"Tenant A MH", :tenant_z => on_z.id, :cir => 100, :requested_service_date => "12/10/12", :user => "bjw@coresite.com"}
  def process_order(order_info, auto_accept = true)
    start_time = Time.now
    @build_details = "\nOrder Placed"
    log_details("Time Started #{start_time.utc}")
    @summary = "Failed"
    @summary_detail = "" # Should be a one line summary of the failure
    original_order = order_info.clone
    original_order[:auto_accept] = auto_accept unless original_order.key?(:auto_accept)
    order_info = convert_ids_to_strings(original_order)
    z_side_nni = nil
    path = nil
        
    @poes = []   
    logger.info "Received Order from #{order_info[:tenant_a]}"

    if !order_info.blank? && validate_order_info(order_info)
      begin
        z_side_nni = find_z_side_nni(order_info) if auto_accept
        valid_cir = [original_order[:nni_a], z_side_nni].compact.all? {|nni| check_cir(nni, order_info[:cir], nni == original_order[:nni_a])}
        if valid_cir
          ActiveRecord::Base.transaction(:requires_new => true) do
            result, path = create_circuit(order_info)
            if result
              if send_order_placed_emails(path, order_info)
                @summary = "Ok"
              end
            end
            raise ActiveRecord::Rollback if @summary != "Ok"
          end
          
          if @summary == "Ok" && auto_accept
            @summary = "Failed" # Set it back to Failed
            if z_side_nni
              original_order[:nni_z] = z_side_nni.id
              original_order.delete(:vlan_z)
              original_order.delete(:member_handle_z)
              original_order[:path_id] = path.id
              original_order[:segment_id] = path.on_net_ovcs.first.id
              accept_order_internal(original_order)
              # accept_order_internal will set @summary so no need to check here
            else
              set_summary_detail_and_details("Can't auto allocate the Z side NNI")
            end
          end        
        end
      rescue
        log_details("Exception occurred when processing #{order_info[:tenant_a]} - Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}")
        @summary_detail = "Exception occurred when processing #{order_info[:tenant_a]} - Exception is #{$!}"
        SW_ERR "Exception occurred when processing #{order_info[:tenant_a]} - Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}", :IGNORE_IF_TEST
      end
    end
    log_details("Time Ended #{Time.now.utc}\nTotal Build time #{Time.now-start_time} seconds")
        
    log_poes(@poes)
    delete_poes(@poes)
    bl = update_build_logs(unique_name(order_info), path ? path.on_net_ovcs.first.get_latest_order : nil)
    logger.info "End of new order: Total time:#{Time.now-start_time}"
    return bl
  end
  
  # order_info = {:path_id => 2, :segment_id => 3, :tenant_a => on_a.id, :nni_a => nni_a.id, :vlan_a => 46, :member_handle_a =>"Tenant A MH", :tenant_z => on_z.id, :nni_z => nni_z.id, :vlan_z => 567, :member_handle_z =>"Tenant Z MH", :cir => 100, :requested_service_date => "12/10/12"}
  def accept_order(order_info)
    start_time = Time.now
    @build_details = "\nTime Started #{start_time.utc}"
    @summary = "Failed"
    @summary_detail = "" # Should be a one line summary of the failure
    @poes = []
    
    accept_order_internal(order_info)
    
    log_details("Time Ended #{Time.now.utc}\nTotal Build time #{Time.now-start_time} seconds")
        
    log_poes(@poes)
    delete_poes(@poes)
    segment = Segment.find_by_id(order_info[:segment_id])
    bl = update_build_logs(unique_name(order_info), segment ? segment.get_latest_order : nil)
    logger.info "End of order accept: Total time:#{Time.now-start_time}"
    return bl      
  end
     
  # order_info = {:path_id => 10, :segment_id => 10, :tenant_a => on_a, :nni_a => nni_a, :vlan_a => 466, :member_handle_a =>"Tenant A MH", :tenant_z => on_z, :member_handle_z =>"Tenant Z MH", :nni_z => nni_z, :vlan_z => 567, :cir => 110, :requested_service_date => "13/10/12", :user => "bjw@coresite.com"}
  # The new CIR and Requested Service date will be in the :cir and :requested_service_date
  def process_change_order(order_info,  auto_accept = true)
    start_time = Time.now
    @build_details = "\nChange Order Placed"
    log_details("Time Started #{start_time.utc}")
    @summary = "Failed"
    @summary_detail = "" # Should be a one line summary of the failure
    @poes = []   
    
    original_order = order_info.clone
    original_order[:auto_accept] = auto_accept unless original_order.key?(:auto_accept)
    order_info = convert_ids_to_strings(original_order)
    z_side_nni = nil
        
    logger.info "Received Change Order from #{order_info[:tenant_a]}"
    path = Path.find_by_id(order_info[:path_id])
    segment = Segment.find_by_id(order_info[:segment_id])
    existing_cir = get_existing_cir(segment)
    
    if order_info.blank? || !validate_order_info(order_info)
    elsif !existing_cir
      set_summary_detail_and_details("Cannot find existing CIR")
    elsif check_change_disconnect_path_state(order_info, path, segment) 
      begin
        # Calculate the delta in CIR and pass that to the cir check 
        valid_cir = [original_order[:nni_a], original_order[:nni_z]].compact.all? {|nni| check_cir(nni, order_info[:cir] - existing_cir, nni == original_order[:nni_a])}
        if valid_cir
          ActiveRecord::Base.transaction(:requires_new => true) do
            # Create a change Service Order
            if create_service_order(order_info, "Change")
              @build_details << "\n"
              if !StateHelper.change_order_state(segment, OrderReceived, @build_details)
                set_summary_detail_and_details("Failed to change state to Received")
              elsif send_change_order_placed_emails(path, order_info)
                @summary = "Ok"
              end
            end
            raise ActiveRecord::Rollback if @summary != "Ok"
          end
          
          if @summary == "Ok" && auto_accept
            @summary = "Failed"
            accept_change_order_internal(original_order)
            # accept_change_order_internal will set @summary so no need to check here
          end
        end
      rescue
        log_details("Exception occurred when processing #{order_info[:tenant_a]} - Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}")
        @summary_detail = "Exception occurred when processing #{order_info[:tenant_a]} - Exception is #{$!}"
        SW_ERR "Exception occurred when processing #{order_info[:tenant_a]} - Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}", :IGNORE_IF_TEST
      end
    end
    log_details("Time Ended #{Time.now.utc}\nTotal Build time #{Time.now-start_time} seconds")
        
    log_poes(@poes)
    delete_poes(@poes)
    bl = update_build_logs(unique_name(order_info), segment ? segment.get_latest_order : nil)
    logger.info "End of change order: Total time:#{Time.now-start_time}"
    return bl
  end

  # order_info = {:path_id => 2, :segment_id => 3, :tenant_a => on_a.id, :nni_a => nni_a.id, :vlan_a => 46, :member_handle_a =>"Tenant A MH", :tenant_z => on_z.id, :nni_z => nni_z.id, :vlan_z => 567, :member_handle_z =>"Tenant Z MH", :cir => 100, :requested_service_date => "12/10/12"}
  def accept_change_order(order_info)
    start_time = Time.now
    @build_details = "\nTime Started #{start_time.utc}"
    @summary = "Failed"
    @summary_detail = "" # Should be a one line summary of the failure
    @poes = []   
  
    accept_change_order_internal(order_info)
    
    log_details("Time Ended #{Time.now.utc}\nTotal Build time #{Time.now-start_time} seconds")
      
    log_poes(@poes)
    delete_poes(@poes)
    segment = Segment.find_by_id(order_info[:segment_id])
    bl = update_build_logs(unique_name(order_info), segment ? segment.get_latest_order : nil)
    logger.info "End of change order accept: Total time:#{Time.now-start_time}"
    return bl      
  end
    

  # order_info = {:path_id => 10, :segment_id => 10, :tenant_a => on_a, :nni_a => nni_a, :vlan_a => 466, :member_handle_a =>"Tenant A MH", :tenant_z => on_z, :member_handle_z =>"Tenant Z MH", :nni_z => nni_z, :vlan_z => 567, :cir => 110, :requested_service_date => "13/10/12", :user => "bjw@coresite.com"}
  # The new CIR and Requested Service date will be in the :cir and :requested_service_date
  def process_disconnect_order(order_info)
    start_time = Time.now
    @build_details = "\nDisconnect Order Placed"
    log_details("Time Started #{start_time.utc}")
    @summary = "Failed"
    @summary_detail = "" # Should be a one line summary of the failure
    
    original_order = order_info.clone
    original_order[:auto_accept] = true
    order_info = convert_ids_to_strings(original_order)
    z_side_nni = nil
    cached_data = {}
        
    @poes = []   
    logger.info "Received Disconnet Order from #{order_info[:tenant_a]}"
    path = Path.find_by_id(order_info[:path_id])
    segment = Segment.find_by_id(order_info[:segment_id])

    if order_info.blank? || !validate_order_info(order_info)
    elsif check_change_disconnect_path_state(order_info, path, segment)
      begin
        ActiveRecord::Base.transaction(:requires_new => true) do
          # Create a disconnect Service Order
          if !create_service_order(order_info, "Disconnect")
          elsif !set_state(segment, ProvMaintenance)
            set_summary_detail_and_details("Failed to change state to Maintenance")
          elsif !update_coresite_oss("disconnect", order_info, path, cached_data)        
          elsif send_disconnect_order_placed_emails(path, order_info)
            @summary = "Ok" 
          end
          raise ActiveRecord::Rollback if @summary != "Ok"
        end
        
        if @summary == "Ok"
          @summary = "Failed"
          router_config_ok = delete_router_config(segment)
          if !router_config_ok
            set_summary_detail_and_details(path.alarm_state.details)
          end
          release_vlan_cir_vpls(segment)
          update_cdb_with_oss_results(cached_data[:so], cached_data[:mh_to_update], cached_data[:wo])  
          cached_data = {}                                        
          if !set_state(segment, ProvDeleted)
            set_summary_detail_and_details("Failed to change state to Deleted")
          elsif router_config_ok
            # Only set it to Ok if the deletion of the router config worked.
            @summary = "Ok"
          end
          if !send_disconnect_order_complete_emails(path, order_info)
            @summary = "Failed"
          end
          set_fallout(path, @summary, @summary_detail)
          @summary = "Fallout" if @summary == "Failed"        
        end        
      rescue
        log_details("Exception occurred when processing #{order_info[:tenant_a]} - Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}")
        @summary_detail = "Exception occurred when processing #{order_info[:tenant_a]} - Exception is #{$!}"
        SW_ERR "Exception occurred when processing #{order_info[:tenant_a]} - Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}", :IGNORE_IF_TEST
      ensure
        update_cdb_with_oss_results(cached_data[:so], cached_data[:mh_to_update], cached_data[:wo]) if cached_data.present? 
      end
    end
    
    log_details("Time Ended #{Time.now.utc}\nTotal Build time #{Time.now-start_time} seconds")
        
    log_poes(@poes)
    delete_poes(@poes)
    bl = update_build_logs(unique_name(order_info), segment ? segment.get_latest_order : nil)
    logger.info "End of disconnect order: Total time:#{Time.now-start_time}"
    return bl
  end

  # order_info = {:path_id => 2, :segment_id => 3, :tenant_a => on_a.id, :nni_a => nni_a.id, :vlan_a => 46, :member_handle_a =>"Tenant A MH", :tenant_z => on_z.id, :nni_z => nni_z.id, :vlan_z => 567, :member_handle_z =>"Tenant Z MH", :cir => 100, :requested_service_date => "12/10/12"}
  # Used by the target to reject an order - the order info is the same as would be passed to an accept order
  def reject_order(order_info)
    return abort_order_internal(order_info, "Rejected")
  end

  # order_info = {:path_id => 2, :segment_id => 3, :tenant_a => on_a.id, :nni_a => nni_a.id, :vlan_a => 46, :member_handle_a =>"Tenant A MH", :tenant_z => on_z.id, :nni_z => nni_z.id, :vlan_z => 567, :member_handle_z =>"Tenant Z MH", :cir => 100, :requested_service_date => "12/10/12"}
  # Used by the buyer to cancel an order - the order info is the same as would be passed to an accept order
  def cancel_order(order_info)
    return abort_order_internal(order_info, "Cancelled")
  end

  #order_info = {:tenant_name => on_a.id, :phy_type => "10GigE LR; 1310nm; SMF", :node => node.id, :port => "6/2", :work_order => "5768987590", :external_name => "Tenant Z MH"}  
  def enni_order(order_info)    
    start_time = Time.now
    @build_details = "\nOrder ENNI"
    log_details("Time Started #{start_time.utc}")
    @summary = "Failed"
    @summary_detail = "" # Should be a one line summary of the failure
    
    order_info = convert_ids_to_strings(order_info)
    @poes = []   
    logger.info "Received ENNI Order from #{order_info[:tenant_name]}"
    if !order_info.blank? && validate_order_info(order_info)
      begin
        ActiveRecord::Base.transaction(:requires_new => true) do
          result, enni = create_enni(order_info)
          if result
            if StateHelper.enni_live(enni, @build_details)
              if send_enni_create_emails(enni, order_info)
                @summary = "Ok"
              end
            else
              set_summary_detail_and_details("Failed to bring NNI #{enni.name} Live")
            end
          end
          raise ActiveRecord::Rollback if @summary != "Ok"
        end
      rescue
        log_details("Exception occurred when processing #{order_info[:tenant_name]} - Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}")
        @summary_detail = "Exception occurred when processing #{order_info[:tenant_name]} - Exception is #{$!}"
        SW_ERR "Exception occurred when processing #{order_info[:tenant_name]} - Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}", :IGNORE_IF_TEST
      end
    end
      
    log_details("Time Ended #{Time.now.utc}\nTotal Build time #{Time.now-start_time} seconds")
        
    log_poes(@poes)
    delete_poes(@poes)
    bl = update_build_logs(enni_unique_name(order_info))
    logger.info "End of ENNI order: Total time:#{Time.now-start_time}"
    return bl      
  end

  #order_info = {:nni_id => nni.id :work_order => "5768987590"}  
  def enni_disconnect_order(order_info)
    start_time = Time.now
    @build_details = "\nDisconnect Order ENNI"
    log_details("Order: #{PP.pp(order_info, "")}") 
    log_details("Time Started #{start_time.utc}")
    @summary = "Failed"
    @summary_detail = "" # Should be a one line summary of the failure
    @poes = []
    
    enni = EnniNew.find_by_id(order_info[:nni_id])
    if !validate_order_info(order_info)
    elsif !enni
      set_summary_detail_and_details("Cannot find ENNI #{order_info[:nni_id]}")
    else
      begin
        # Add info to the order - This will enable the correct builder log to be updated and correct email content
        order_info[:tenant_name] = enni.operator_network.tenant_name
        order_info[:sitename] = enni.operator_network.site_name
        order_info[:port] = enni.ports.first.name
        order_info[:node] = enni.node_info
        order_info[:external_name] = enni.member_handle_instance_by_owner(enni.service_provider).value
        order_info[:phy_type] = enni.physical_medium
        # Update the tenant Contacts
        update_tenant_contacts(order_info)
        
        ActiveRecord::Base.transaction(:requires_new => true) do
          if enni.paths.all?{|p| p.get_prov_state.is_a?(ProvDeleted)}
            if set_state(enni, ProvMaintenance)
              # delete all the ports
              port_name = enni.port_name
              enni.ports.destroy_all              
              if set_state(enni, ProvDeleted)
                enni.member_handles.each do |mh|
                  mh.value = "N/A"
                  mh.save!
                end
                mh = enni.member_handle_instance_by_owner(ServiceProvider.system_owner)
                mh.value = order_info[:work_order]
                mh.save!
                if send_enni_disconnect_emails(enni, order_info, port_name)
                  @summary = "Ok"
                end
              else
                set_summary_detail_and_details("Failed to set the ENNI to Deleted")
              end
            else
              set_summary_detail_and_details("Failed to set the ENNI to Maintenance")
            end
          else
            set_summary_detail_and_details("Not all paths are Deleted (#{enni.paths.collect{|p| [p.id, p.get_prov_state.name]}})")
          end
          raise ActiveRecord::Rollback if @summary != "Ok"
        end
      rescue
        log_details("Exception occurred when processing Enni Disconnect - Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}")
        @summary_detail = "Exception occurred when processing Enni Disconnect - Exception is #{$!}"
        SW_ERR "Exception occurred when processing Enni Disconnect - Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}", :IGNORE_IF_TEST
      end        
    end
    log_details("Time Ended #{Time.now.utc}\nTotal Build time #{Time.now-start_time} seconds") 
    log_poes(@poes)
    delete_poes(@poes)
    bl = update_build_logs(enni_unique_name(order_info))
    logger.info "End of ENNI Disconnect Order: Total time:#{Time.now-start_time}"
    return bl
  end

  # order_info = {:so_id => xx}
  def remove_order(order_info)
    logger.info "Remove order for #{order_info}"
    result = false
    so = ServiceOrder.find_by_id(order_info[:so_id])
    path = so.try(:ordered_entity).try(:paths).try(:first)
    if so && path
      if ["Rejected", "Cancelled"].include?(so.get_order_state.name)
        # Delete the whole path if it's a New order thats been rejected
        # Delete just the ServiceOrder if the change request has been rejected
        if path.get_prov_state.is_a?(ProvLive)
          so.destroy
          result = true
        else
          path.destroy
          result = true
        end
      else
        logger.info "Remove order failed for #{order_info}. The latest order is not Rejected or Cancelled (#{so.try(:get_order_state).class.to_s})"
      end
    else
      logger.info "Remove order failed for #{order_info}. Cannot find the Path or ServiceOrder"
    end    
    logger.info "Remove order finished for #{order_info}"
    return result
  end
  
    
    
  def sync_contacts
    start_time = Time.now
    @build_details = "\nsync_contacts"
    log_details("Time Started #{start_time.utc}")
    @summary = "Failed"
    @summary_detail = "" # Should be a one line summary of the failure
    
    OperatorNetwork.find_each do |on|
      log_details("Attempting to update contacts for #{on.name}")
      update_tenant_contacts({:tenant_name => on.tenant_name, :sitename => on.site_name})
    end
    log_details("Time Ended #{Time.now.utc}\nTotal time #{Time.now-start_time} seconds")
    logger.info(@build_details)
  end       

  # This is for a New order
  def check_cir_and_vlan(order_info, auto_accept = true)
    @build_details = ""
    @summary = ""
    @summary_detail = ""
    reason = ""
    valid_cir = false
    valid_vlan = false
    check_items = {}
    original_order = order_info.clone
    original_order[:auto_accept] = auto_accept unless original_order.key?(:auto_accept)
    order_info = convert_ids_to_strings(original_order)
    
    # Determine if it's a accept order or a placed order by seeing if the z-side NNI is present
    # If it's an accept_order only check the Z side nni for CIR & if the VLAN has not been allocated
    # If it's a placed order only check the A side nni & if the VLAN has not been allocated. If it's auto_accept check the Z Side CIR and check
    #  there is at least one vlan free on the Z Side
    accept_order = original_order.key?(:nni_z)
    # Check CIR
    if !order_info.blank?
      if accept_order
        check_items = {EnniNew.find_by_id(original_order[:nni_z]) => original_order[:vlan_z]}
      elsif auto_accept
        check_items = {find_z_side_nni(order_info) => "AutoAccept_VLAN", EnniNew.find_by_id(original_order[:nni_a]) => original_order[:vlan_a]}
      else
        check_items = {EnniNew.find_by_id(original_order[:nni_a]) => original_order[:vlan_a]}
      end

      if check_items.keys.any? {|nni| nni.blank?}
        reason = "Could not find all NNIs"
      else
        valid_cir = !(check_items.keys.collect {|nni|
          buyer = (nni.id == original_order[:nni_a].to_i)
          valid = check_cir(nni, order_info[:cir], buyer)
          available_bandwidth = nni.rate - nni.get_provisioned_bandwidth[:grand][:total]
          reason = "Insufficient bandwidth available on #{buyer ? "Buyer NNI" : "Target NNI"}. Available bandwidth is: #{available_bandwidth} Mbps" if ( !valid and (reason.empty? or buyer))
          valid
        }.include?(false))
      end
    end
    
    if valid_cir
      # Check VLAN
      if check_items.values.any? {|vlan| vlan.blank?}
        reason = "Could not find all VLANs"
      else
        valid_vlan = check_items.all? do |nni, vlan|
          valid = false
          if vlan.to_i >= 400 && vlan.to_i <= 599
            valid = nni.get_endpoint_with_stag(vlan.to_s).nil?
            reason = "VLAN #{vlan} is already in use" if !valid
          elsif vlan == "AutoAccept_VLAN"
            # Check that there is at least one free vlan.....This is for the Auto Accept case
            valid = (400..599).any? {|possible_vlan| nni.get_endpoint_with_stag(possible_vlan.to_s).nil?}
            reason = "All VLANs have been allocated" if !valid
          else
            reason = "Invalid VLAN #{vlan} must be between 400 and 599"
          end
          valid
        end
      end
    end
    return {:result => valid_cir && valid_vlan, :reason => reason}
  end
  
  # This is for a Change order
  def check_cirs(segment_id, nnis, cir)
    @build_details = ""
    @summary = ""
    @summary_detail = ""
    
    segment = Segment.find_by_id(segment_id)
    if segment.nil?
      set_summary_detail_and_details("Failed find Segment id #{segment_id}")
    elsif (existing_cir = get_existing_cir(segment)).nil?
      set_summary_detail_and_details("Failed to find current CIR")
    else
      # Calculate the delta in CIR and pass that to the cir check
      valid_cir = !(nnis.collect{|position, nni| check_cir(nni, cir.to_i - existing_cir, position == :buyer, existing_cir)}.include?(false))
    end
    return {:result => valid_cir, :reason => @summary_detail}
  end
  
  
  # Fallout Recovery API's - For CoreSite Experts only
  def configure_router(path, ignore_errors = false)
    start_time = Time.now
    @build_details = "\nFallout Configure Router invoked on Path #{path.id}"
    log_details("Time Started #{start_time.utc}")
    @summary = "Failed"
    @summary_detail = "" # Should be a one line summary of the failure
    segment = nil
    if path && path.on_net_ovcs.present?
      segment = path.on_net_ovcs.first
      CoresiteConfigureNode.stub_options = stub_options[:router_config]  
      @summary = "Ok" if config_router(segment, segment.generate_config_hash, ignore_errors)
      CoresiteConfigureNode.stub_options = nil
      set_summary_detail_and_details(path.alarm_state.details)
    else
      set_summary_detail_and_details("Cannot find path or segment")
    end
    log_details("Time Ended #{Time.now.utc}\nTotal time #{Time.now-start_time} seconds")
    bl = update_build_logs("Fallout - Configure Router @ #{Time.now.utc}", segment ? segment.get_latest_order : nil, false)
    return bl
  end
  
  def delete_router_configuration(path, ignore_errors = false)
    start_time = Time.now
    @build_details = "\nFallout Delete Router Configuration invoked on Path #{path.id}"
    log_details("Time Started #{start_time.utc}")
    @summary = "Failed"
    @summary_detail = "" # Should be a one line summary of the failure
    segment = nil
    if path && path.on_net_ovcs.present?
      segment = path.on_net_ovcs.first
      @summary = "Ok" if delete_router_config(segment, ignore_errors)
      set_summary_detail_and_details(path.alarm_state.details)
    else
      set_summary_detail_and_details("Cannot find path or segment")
    end
    log_details("Time Ended #{Time.now.utc}\nTotal time #{Time.now-start_time} seconds")
    bl = update_build_logs("Fallout - Delete Router Config @ #{Time.now.utc}", segment ? segment.get_latest_order : nil, false)
    return bl
  end
  
  def run_router_commands(node_name, cmds, ignore_errors = false)
    start_time = Time.now
    @build_details = "\nFallout Run Router Commands on Node #{node_name}"
    log_details("Time Started #{start_time.utc}")
    @summary = "Failed"
    @summary_detail = "" # Should be a one line summary of the failure
    node = Node.find_by_name(node_name)
    
    if node && cmds.present?
      CoresiteConfigureNode.stub_options = stub_options[:router_config]
      config_result, output = CoresiteConfigureNode.configure(node, cmds, ignore_errors)
      CoresiteConfigureNode.stub_options = nil
      log_details("Config on #{node.name} #{config_result == CoresiteConfigureNode::CONFIGURE_OK ? "Successfully" : "Failed"}")
      log_details(output)      
      @summary = "Ok" if config_result == CoresiteConfigureNode::CONFIGURE_OK
    else
      set_summary_detail_and_details("Cannot find node #{node_name} or there are no commands")
    end
    log_details("Time Ended #{Time.now.utc}\nTotal time #{Time.now-start_time} seconds")
    bl = update_build_logs("Fallout - Run Router Commands on #{node_name} @ #{Time.now.utc}", nil, false)
    return bl
  end
  
  def clear_fallout(path)
    start_time = Time.now
    @build_details = "\nFallout Clear Fallout invoked on Path #{path.id}"
    log_details("Time Started #{start_time.utc}")
    @summary = "Failed"
    @summary_detail = "" # Should be a one line summary of the failure
    segment = nil
    if path && path.on_net_ovcs.present?
      segment = path.on_net_ovcs.first
      @summary = "Ok"
      set_fallout(path, @summary, "Manually cleared fallout")
    else
      set_summary_detail_and_details("Cannot find path")
    end
    log_details("Time Ended #{Time.now.utc}\nTotal time #{Time.now-start_time} seconds")
    bl = update_build_logs("Fallout - Clear Fallout @ #{Time.now.utc}", segment ? segment.get_latest_order : nil, false)
    return bl
  end
  
  def self.test_oss(url = nil, request_data = nil)
    output = "" 
    url = CoresiteConfig::instance.config["oss_url"] + "/api/contact" if url.nil?
    if request_data.nil?
      tenant_operator_network = OperatorNetwork.first
      site_id = tenant_operator_network.site.id
      tenant_id = tenant_operator_network.id
      request_data = {'TenantId' => tenant_id, 'SiteId' => site_id}
    end
    
    output << "Testing OSS url #{url} request_data #{request_data}\n"
    
    begin
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      if uri.scheme == 'https'
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      end
      request = Net::HTTP::Post.new(uri.request_uri)
      request["Content-Type"] = "application/json"
      request.body = request_data.to_json
      output << "Sending\nHeader:#{PP.pp(request.to_hash, "")}\nBody:#{PP.pp(request_data, "")} to #{uri}\n"
      response, data = http.request(request)
         
      if response.is_a?(Net::HTTPOK) 
        data = JSON.parse(data)
        output << "Received Data:#{PP.pp(data, "")}\n"
      else
        output << "CoreSite OSS HTTP response is not OK (#{response.inspect})\n"
      end        
    rescue
      output << "Failed to send to OSS Exception: #{$!} Traceback: #{$!.backtrace.join("\n")}\n"
    end
    return output
  end
  
  def self.site_check(site)
    config = "show users\n"
    node_counts = Hash.new(0)
    errored_nodes = []

    site.nodes.each do |node|
      config_result, output = CoresiteConfigureNode.configure(node, config)
      node_counts[CoresiteConfigureNode.failure_reason(config_result)] += 1
      if config_result != CoresiteConfigureNode::CONFIGURE_OK
        errored_nodes << {:node_name => node.name, :ip_address => node.primary_ip, :prompt => output[/SSH@.*#/], :reason => CoresiteConfigureNode.failure_reason(config_result)}     
      end
    end

    return node_counts, errored_nodes
  end
  
  #private
  
  def accept_order_internal(order_info)    
    # If there is no Z Side member handle then set it to TBD (used in the automatic accept flow)
    order_info[:member_handle_z] = "TBD" unless order_info.key?(:member_handle_z) 

    log_details("Order Accept")
    original_order = order_info.clone
    order_info = convert_ids_to_strings(original_order)

    logger.info "Received Accept from #{order_info[:tenant_z]}"
    path = Path.find_by_id(order_info[:path_id])
    segment = Segment.find_by_id(order_info[:segment_id])
    
    if order_info.blank? || !validate_order_info(order_info)
    elsif check_accept_path_state(order_info, path, segment, "New")    
      begin
        valid_cir = [original_order[:nni_z]].compact.all? {|nni| check_cir(nni, order_info[:cir], nni == original_order[:nni_a])}
        if valid_cir
          ActiveRecord::Base.transaction(:requires_new => true) do   
            create_z_side(path, segment, order_info)
            if update_coresite_oss("new", order_info, path)
              @summary = "Ok"
            end
            raise ActiveRecord::Rollback if @summary != "Ok"
          end  
              
          if @summary == "Ok"
            @summary = "Failed"            
            # Run auto test & save test results
            CoresiteConfigureNode.stub_options = stub_options[:router_config]
            if !path_to_state(path, ProvTesting)
              set_summary_detail_and_details("Failed to bring the Circuit to a Testing state")
            elsif false # Run auto test & save test results
              set_summary_detail_and_details("The Circuit failed the verification tests")
            elsif !path_to_state(path, ProvLive)
              set_summary_detail_and_details("Failed to bring the Circuit to a live state")  
            elsif !send_order_accepted_emails(path, order_info)
            elsif router_config_failed?(path)
              # If there is an alarm (ie the config failed set the status to Failed
              @summary = "Failed"
              set_summary_detail_and_details(path.alarm_state.details)
            else
              @summary = "Ok"
            end
            CoresiteConfigureNode.stub_options = nil
            set_fallout(path, @summary, @summary_detail)
            @summary = "Fallout" if @summary == "Failed"        
          end
        end
      rescue
        log_details("Exception occurred when processing #{order_info[:tenant_a]} - Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}")
        @summary_detail = "Exception occurred when processing #{order_info[:tenant_a]} - Exception is #{$!}"
        SW_ERR "Exception occurred when processing #{order_info[:tenant_a]} - Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}", :IGNORE_IF_TEST
      end
    end
  end

  def accept_change_order_internal(order_info)
    log_details("Change Order Accept")
    original_order = order_info.clone
    order_info = convert_ids_to_strings(original_order)
    path = Path.find_by_id(order_info[:path_id])
    segment = Segment.find_by_id(order_info[:segment_id])
    existing_cir = get_existing_cir(segment)
    delete_cmds = nil
    
    logger.info "Received Change Order Accept from #{order_info[:tenant_z]}"
    if order_info.blank? || !validate_order_info(order_info)
    elsif !existing_cir
      set_summary_detail_and_details("Cannot find existing CIR")
    elsif check_accept_path_state(order_info, path, segment, "Change")    
      begin
        valid_cir = [original_order[:nni_z]].compact.all? {|nni| check_cir(nni, order_info[:cir] - existing_cir, nni == original_order[:nni_a])}       
        ActiveRecord::Base.transaction(:requires_new => true) do
          if !set_state(segment, ProvMaintenance)
            set_summary_detail_and_details("Failed to change state to Maintenance")              
          elsif !set_state(segment, ProvPending)       
            set_summary_detail_and_details("Failed to change state to Pending")
          else          
            # Don't do the delete here - only do it if everthing is OK in the transaction
            CoresiteConfigureNode.stub_options = stub_options[:router_config]  
            delete_cmds = segment.generate_delete_config_hash
            CoresiteConfigureNode.stub_options = nil
            if !update_cir(segment) # Change CIR on CosEndPoints
              set_summary_detail_and_details("Failed to set the CIR")
            elsif update_coresite_oss("change", order_info, path)        
              @summary = "Ok"
            end
          end
          raise ActiveRecord::Rollback if @summary != "Ok"
        end
        
        if @summary == "Ok"
          @summary = "Failed"
          if !config_router(segment, delete_cmds)
            set_summary_detail_and_details("Delete: #{path.alarm_state.details}") 
          end   
          CoresiteConfigureNode.stub_options = stub_options[:router_config]  
          if !path_to_state(path, ProvTesting)
            set_summary_detail_and_details("Failed to bring the Circuit to a Testing state")
          elsif false # Run auto test & save test results
            set_summary_detail_and_details("The Circuit failed the verification tests")
          elsif !path_to_state(path, ProvLive)
            set_summary_detail_and_details("Failed to bring the Circuit to a live state")
          elsif router_config_failed?(path)
            @summary = "Failed"
            set_summary_detail_and_details(path.alarm_state.details)
          else
            @summary = "Ok"
          end
          CoresiteConfigureNode.stub_options = nil
          if !send_change_order_accepted_emails(path, order_info)
            @summary = "Failed"
          end
          set_fallout(path, @summary, @summary_detail)
          @summary = "Fallout" if @summary == "Failed"
        end
      rescue
        log_details("Exception occurred when processing #{order_info[:tenant_a]} - Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}")
        @summary_detail = "Exception occurred when processing #{order_info[:tenant_a]} - Exception is #{$!}"
        SW_ERR "Exception occurred when processing #{order_info[:tenant_a]} - Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}", :IGNORE_IF_TEST
      end
    end
  end

  # order_info = {:path_id => 2, :segment_id => 3, :tenant_a => on_a.id, :nni_a => nni_a.id, :vlan_a => 46, :member_handle_a =>"Tenant A MH", :tenant_z => on_z.id, :nni_z => nni_z.id, :vlan_z => 567, :member_handle_z =>"Tenant Z MH", :cir => 100, :requested_service_date => "12/10/12"}
  # Used by the target to reject/cancel an order - the order info is the same as would be passed to an accept order
  def abort_order_internal(order_info, action)
    start_time = Time.now
    @build_details = "\n#{action} Order Placed"
    log_details("Time Started #{start_time.utc}")
    @summary = "Failed"
    @summary_detail = "" # Should be a one line summary of the failure
    
    original_order = order_info.clone
    order_info = convert_ids_to_strings(original_order)
    z_side_nni = nil
    cached_data = {}
    end_state = case action
      when "Rejected" then OrderRejected
      when "Cancelled" then OrderCancelled
      else nil
    end
        
    @poes = []   
    logger.info "Received #{action} Order from #{order_info[:tenant_a]}"
    path = Path.find_by_id(order_info[:path_id])
    segment = Segment.find_by_id(order_info[:segment_id])

    if order_info.blank? || !validate_order_info(order_info)
    elsif end_state.nil?
      set_summary_detail_and_details("Invalid action #{action}")      
    elsif check_accept_path_state(order_info, path, segment)
      begin
        ActiveRecord::Base.transaction(:requires_new => true) do
          so = segment.get_latest_order
          if !so
            set_summary_detail_and_details("No Service Order can be found")
          elsif !set_state(so, end_state)
            set_summary_detail_and_details("Failed to change Order to #{action}")
          elsif segment.get_prov_state.is_a?(ProvLive)
            # The segment is Live (ie cancelling a Change order) so don't release the CIR/VLAN
            # as it has not changed..
            @summary = "Ok"
          else
            # Leave the segment in it's current state but release the CIR and VLAN
            release_vlan_cir_vpls(segment)
            # Make the title and member handles unique by adding a timestamp
            so.title = so.title + " #{action} at #{Time.now.utc}"
            so.save!
            [segment, path].each {|obj| obj.member_handles.each {|mh| mh.value = mh.value + " #{action} at #{Time.now.utc}";mh.save}}
            @summary = "Ok"
          end
          if @summary == "Ok"
            @summary = "Failed" unless send_aborted_emails(path, order_info, action)
          end
          raise ActiveRecord::Rollback if @summary != "Ok"
        end
      rescue
        log_details("Exception occurred when processing #{order_info[:tenant_a]} - Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}")
        @summary_detail = "Exception occurred when processing #{order_info[:tenant_a]} - Exception is #{$!}"
        SW_ERR "Exception occurred when processing #{order_info[:tenant_a]} - Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}", :IGNORE_IF_TEST 
      end
    end
    
    log_details("Time Ended #{Time.now.utc}\nTotal Build time #{Time.now-start_time} seconds")
        
    log_poes(@poes)
    delete_poes(@poes)
    bl = update_build_logs(unique_name(order_info), segment ? segment.get_latest_order : nil)
    logger.info "End of #{action} order: Total time:#{Time.now-start_time}"
    return bl
  end

  def check_change_disconnect_path_state(order_info, path, segment)
    result = false
    if !order_info
      set_summary_detail_and_details("The order is nil")
    elsif path.nil?
      set_summary_detail_and_details("Unable to find path #{order_info[:path_id]}")
    elsif segment.nil?
      set_summary_detail_and_details("Unable to find segment #{order_info[:segment_id]}")
    elsif segment.active_order?
      set_summary_detail_and_details("There is already an active order for segment #{segment.name} Service Order #{segment.get_latest_order.title} State #{segment.get_latest_order.get_order_state.name}")
    elsif !path.get_prov_state.is_a?(ProvLive)
      set_summary_detail_and_details("The path is not Live. Current State:#{path.get_prov_state.name}")
    else
      result = true
    end
    return result
  end
  
  def check_accept_path_state(order_info, path, segment, order_type = nil)
    result = false
    if !order_info
      set_summary_detail_and_details("The order is nil")
    elsif path.nil?
      set_summary_detail_and_details("Unable to find path #{order_info[:path_id]}")
    elsif segment.nil?
      set_summary_detail_and_details("Unable to find segment #{order_info[:segment_id]}")
    elsif !segment.active_order?
      set_summary_detail_and_details("Sorry the orginal order has been #{segment.get_latest_order.get_order_state.name}")
    elsif order_type && segment.get_latest_order.action != order_type
      set_summary_detail_and_details("The service order type (#{segment.get_latest_order.action}) does not match the requested action #{order_type}")
    else
      result = true
    end
    return result
  end
  
  def create_circuit(order_info)
    path = nil
    poe = make_circuit_poe(order_info)
    @poes << poe
    osl_result = do_poe_parse_again(poe)
    
    result, result_details = process_poe_result(osl_result)
    if !result
      set_summary_detail_and_details("OSL build failed for A Side #{order_info[:tenant_a]}")
      if result_details.present?      
        log_details("Result: #{result_details}")
        @summary_detail << " Result: #{result_details}"
      end
    else
      log_details("OSL successfully built A Side of the circuit")
      # Double check the circuit was created
      circuit = true; #ctv = CosTestVector.find_by_circuit_id(cid)
      if !circuit
        set_summary_detail_and_details("Failed to find built circuit #{order_info[:tenant_a]}! OSL Built but can't find the circuit in the database") if result
      else                 
        result = true
        path = osl_result[:internal].paths.first
        # Add the order email to the notes
        so = path.on_net_ovcs.first.get_latest_order
        so.notes = so.notes + "\nOrderEmail:#{order_info[:user]}\nNewCIR:#{order_info[:cir]}"
        so.title = "XC: #{order_info[:tenant_a]} to #{order_info[:tenant_z]} New #{so.id}"
        so.save!
        
        # Set the CoreSite member handles to the title
        [path, path.on_net_ovcs.first].compact.each do |obj|
          mh = obj.member_handle_instance_by_owner(ServiceProvider.system_owner)
          mh.value = so.title
          mh.save!
        end
        
        # Copy the segment A Side member handle to the Path
        on_a = OperatorNetwork.find_by_name("#{order_info[:tenant_a]} [#{order_info[:sitename_a]}]")
        tenant_a_mh = path.on_net_ovcs.first.member_handle_instance_by_owner(on_a.try(:national_tenant))
        if tenant_a_mh
          path_mh = path.member_handle_instance_by_owner(on_a.try(:national_tenant))
          path_mh.value = tenant_a_mh.value
          path_mh.save!
        else
          result = false
          set_summary_detail_and_details("Failed to find #{on_a.try(:national_tenant).try(:name).inspect} member handle on segment #{path.on_net_ovcs.first.id}")
        end
      end
    end
    return result, path
  end
  
  def create_enni(order_info)
    demarc = nil
    poe = make_enni_poe(order_info)
    @poes << poe
    osl_result = do_poe_parse_again(poe)
    
    result, result_details = process_poe_result(osl_result)
    if !result
      set_summary_detail_and_details("ENNI OSL build failed for #{order_info[:tenant_name]}")
      if result_details.present?      
        log_details("Result: #{result_details}") 
        @summary_detail << " Result: #{result_details}"
      end
    else
      log_details("OSL successfully built ENNI\n")
      # Double check the circuit was created
      circuit = true; #ctv = CosTestVector.find_by_circuit_id(cid)
      if !circuit
        set_summary_detail_and_details("Failed to find built circuit #{order_info[:tenant_a]}! OSL Built but can't find the circuit in the database") if result
      else                 
        result = true
        demarc = osl_result[:internal].demarcs.first
      end
    end
    return result, demarc
  end

  def create_z_side(path, segment, order_info)
    log_details("Creating Z Side Order_info:\n#{PP.pp(order_info,"")}\nPath:\n#{PP.pp(path.attributes,"")}\nSegment:\n#{PP.pp(segment.attributes,"")}")

    coresite = ServiceProvider.find_by_is_system_owner(true)
    tenant_z = OperatorNetwork.find_by_name("#{order_info[:tenant_z]} [#{order_info[:sitename_z]}]")
    demarc = MemberAttrInstance.find_by_affected_entity_type_and_value_and_owner_type_and_owner_id('Demarc', order_info[:nni_z], ServiceProvider, tenant_z.service_provider.id).affected_entity

    if demarc && tenant_z && coresite
      log_details("Creating Z Side Found tenant z:#{tenant_z.name} id:#{tenant_z.id} Demarc:#{demarc.name} id:#{demarc.id}")
      
      segment_endpoint_order = {
        :segment => segment,
        :demarc => demarc,
        :stags => "TBD",
      }
    
      segment_endpoint_order[:stags] = order_info[:vlan_z] if order_info[:vlan_z]
    
      segment_endpoint = ModelMaker::OnNetOvcEndPointEnniFactory.produce(segment_endpoint_order)
    
      if !order_info.key?(:vlan_z)
        result, vlan_z  = segment_endpoint.suggest_valid_stag
        if !result
          raise "Can't suggest outer tag for Z side - this endpoint is not connected to anything"
        else
          segment_endpoint.stags = vlan_z
          segment_endpoint.save!
          order_info[:vlan_z] = vlan_z
        end
      end
      
      cos_instance = segment.cos_instances.select{|c| c.cos_name == 'Guaranteed CIR'}.first
      cos_endpoint_order = {
        :segment_end_point => segment_endpoint,
        :cos_instance => cos_instance,
        :ingress_mapping => "*",
        :egress_marking => "-",
        :ingress_rate_limiting => "Policing",
        :ingress_cir_kbps => (1000 * order_info[:cir]).to_s,
        :egress_rate_limiting => "None",
      }
      cos_endpoint = ModelMaker::CenxCosEndPointFactory.produce(cos_endpoint_order)

      member_handle = path.member_handle_instance_by_owner tenant_z.service_provider
      member_handle.value = order_info[:member_handle_z]
      member_handle.save
      raise "Could not save Tenant Z member handle: #{member_handle.errors.full_messages}" if member_handle.errors.any?

      path.reload
      path.cenx_path_type = "CoreSite Intra-Node" if path.ennis.map{|e| e.node_ids}.flatten.uniq.one?
      path.save!
    
      segment.reload
      if order_info[:member_handle_z] != "TBD"
        member_handle = segment.member_handle_instance_by_owner tenant_z.service_provider
        if member_handle
          member_handle.value = order_info[:member_handle_z]
          member_handle.save
        else
          raise "Create Z Side failed it cannot find the segment member handle for #{tenant_z.service_provider.name}"
        end
      end
    
      # TODO remove - temp for core site 
      path.reload
      path.layout_diagram
      # TODO END
    else
      raise "Create Z Side failed because it did find CoreSite ServiceProvider(#{coresite.inspect}), Demarc(#{demarc.inspect}) or Tenant Z(#{tenant_z.inspect})"
    end
    return segment_endpoint
  end
  
  def find_z_side_nni(order_info)
    lowest_nni = nil
    on = OperatorNetwork.find_by_name("#{order_info[:tenant_z]} [#{order_info[:sitename_z]}]")    
    if on
      # Find the nni with the most free bandwidth
      nnis = on.enni_news.live
      highest_nni = nnis.max_by do |nni|
        cost = nni.get_provisioned_bandwidth
        nni.rate - cost[:grand][:total]
      end
    else      
      set_summary_detail_and_details("Failed to find Z Side OperatorNetwork (#{order_info[:tenant_z]} [#{order_info[:sitename_z]}])")
    end
    return highest_nni
  end
  
  def make_circuit_poe(order_info)
    path_type = order_info[:sitename_a] == order_info[:sitename_b] ? "CoreSite Intra-Site" : "CoreSite Inter-Site"
    
    data = {
      "template" => "coresite_xc",
      "PathType" => path_type,
      "TenantName_A" => order_info[:tenant_a],
      "SiteName_A" => order_info[:sitename_a],
      "NNI_A" => order_info[:nni_a],
      "VLAN_A" => order_info[:vlan_a],
      "MemberHandle_A" => order_info[:member_handle_a],
      "TenantName_Z" => order_info[:tenant_z],
      "SiteName_Z" => order_info[:sitename_z],
      "MemberHandle_Z" => order_info[:member_handle_z],
      "CIR" => order_info[:cir],
      "Requested Service Date" => Date.parse(order_info[:requested_service_date])
     }   
    outfile = ""
    csv_out = CSV.new(outfile)
    csv_out << data.keys
    csv_out << data.values
    
    poe_file = poe_filename(order_info)
    poe_file.write(outfile)
    poe_file.close
    
    return poe_file
  end
  
  def update_coresite_oss(action, order_info, path, cached_data = nil)
    log_details("Updating CoreSite OSS")
    result = false
    
    segment = path.segments.find_by_type("OnNetOvc")
    so = segment.get_latest_order
    tenant_z = OperatorNetwork.find_by_name("#{order_info[:tenant_z]} [#{order_info[:sitename_z]}]")
    tenant_a = OperatorNetwork.find_by_name("#{order_info[:tenant_a]} [#{order_info[:sitename_a]}]")
    # Get the requestor
    requesting_user = nil
    if so
      split_notes = so.notes.split("\n").find {|p| p[/^OrderEmail:/]}
      if split_notes
        requesting_user = split_notes.gsub(/OrderEmail:/,"")
      else
        log_details("Failed to find OrderEmail the Service Order notes #{so.notes}")
      end
    end
   
    if so && requesting_user && tenant_z && tenant_a
      buyer_member_attr = MemberAttrInstance.find_by_affected_entity_type_and_value_and_owner_type_and_owner_id('Demarc', order_info[:nni_a], ServiceProvider, tenant_a.service_provider.id)
      seller_member_attr = MemberAttrInstance.find_by_affected_entity_type_and_value_and_owner_type_and_owner_id('Demarc', order_info[:nni_z], ServiceProvider, tenant_z.service_provider.id)
      buyer_nni = buyer_member_attr.affected_entity
      buyer_node = Node.find_by_id(buyer_nni.node_ids.first)
      seller_nni = seller_member_attr.affected_entity
      seller_node = Node.find_by_id(seller_nni.node_ids.first)
      coresite_sp = ServiceProvider.system_owner
      coresite_path_mh = path.member_handle_instance_by_owner(coresite_sp)
      coresite_segment_mh = segment.member_handle_instance_by_owner(coresite_sp)
      requested_service_date = Date.parse(order_info[:requested_service_date]).strftime("%Y%m%d")
      
      if coresite_path_mh && coresite_segment_mh
        coresite_mh_to_update = [coresite_path_mh, coresite_segment_mh]
        previous_work_order = find_previous_work_order(segment)
      
        request_data = case action
        when "new"
          # Returns the same data but include the work order id and errors
          {
            'RequestorEmail' => requesting_user,
            'ASideTenantId' => tenant_a.id,
            'ZSideTenantId' => tenant_z.id,
            'SiteId' => buyer_nni.site.id,
            'Bandwidth' => order_info[:cir],
            'ASidePort' => buyer_nni.ports.first.name,
            'ASideAsset' => buyer_node.name,
            'ZSidePort' => seller_nni.ports.first.name,
            'ZSideAsset' => seller_node.name,
            'DesiredDate' => requested_service_date,
            'CENXID' => segment.cenx_id,
          }
        when "change"
          # Returns the same data but the work order id is the new work order id  and a new PriorWorkOrderId is set to the old work order and errors
          if previous_work_order
            {
              'RequestorEmail' => requesting_user,
              'ASideTenantId' => tenant_a.id,
              'ZSideTenantId' => tenant_z.id,
              'SiteId' => buyer_nni.site.id,
              'WorkOrderId' => previous_work_order, 
              'Bandwidth' => order_info[:cir],
              'ASidePort' => buyer_nni.ports.first.name,
              'ASideAsset' => buyer_node.name,
              'ZSidePort' => seller_nni.ports.first.name,
              'ZSideAsset' => seller_node.name,
              'DesiredDate' => requested_service_date,
              'CENXID' => segment.cenx_id,
            }
          else
            set_summary_detail_and_details("Failed to find the previous work order for Segment:#{segment.id} Num orders:#{segment.service_orders.size}")
            log_details("#{PP.pp(segment.service_orders, "")}")
            nil
          end
        when "disconnect"
          # Returns DisconnectID (work order id??) and errors
          if previous_work_order
            {
              'RequestorEmail' => requesting_user,
              'ASideTenantId' => tenant_a.id,
              'SiteId' => buyer_nni.site.id,
              'WorkOrderId' => previous_work_order, 
              'DesiredDate' => requested_service_date,
              'CENXID' => segment.cenx_id,
            }
          else
            set_summary_detail_and_details("Failed to find the previous work order for Segment:#{segment.id} Num orders:#{segment.service_orders.size}")
            log_details("#{PP.pp(segment.service_orders, "")}")
            nil
          end
        else
          set_summary_detail_and_details("Unknown CoreSite OSS action #{action}")
          nil
        end
      
        begin
          if request_data
            if @stub_oss
              uri = URI.parse("http://localhost:8090/api/evc/#{action}")
            else
              uri = URI.parse(CoresiteConfig::instance.config["oss_url"] + "api/evc/#{action}")
            end
            http = Net::HTTP.new(uri.host, uri.port)
            if uri.scheme == 'https'
              http.use_ssl = true
              http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            end
            request = Net::HTTP::Post.new(uri.request_uri)
            request["Content-Type"] = "application/json"
            request.body = request_data.to_json
            log_details("Sending\nHeader:#{PP.pp(request.to_hash, "")}\nBody:#{PP.pp(request_data, "")} to #{uri}\n")
            if !@stub_oss
              start_time = Time.now
              response, data = http.request(request)
              log_details("Time for CoreSite OSS to process the request: #{Time.now-start_time} seconds")
            else
              @@work_order_id = Time.now.to_i
              response = case stub_options[:oss]
                when :ok then Net::HTTPOK.new(1,2,3)
                when :connect_fail then Net::HTTPError.new(1,2)
                else Net::HTTPOK.new(1,2,3)
              end
              data = request_data
              data["StatusCode"] = stub_options[:oss] == :failure ? 100 : 0
              
              case action
              when "new"
                data["WorkOrderId"] = @@work_order_id.to_s
                data["Errors"] = [["Error 1"], ["Error 2"]]
              when "change"
                data["PriorWorkOrderId"] = data["WorkOrderId"] 
                data["WorkOrderId"] = @@work_order_id.to_s
                data["Errors"] = [["Error 1"], ["Error 2"]]
              when "disconnect"
                data["DisconnectID"] = @@work_order_id.to_s
                data["Errors"] = ["Error 1", "Error 2"]
              end
              data = data.to_json
            end
            
            if response.is_a?(Net::HTTPOK) 
              data = JSON.parse(data)
              log_details("Received Data:#{PP.pp(data, "")}")
              if data["StatusCode"] == 0
                case action
                when "new"
                  # Returns the same data but include the work order id and errors
                  update_cdb_with_oss_results(so, coresite_mh_to_update, data["WorkOrderId"])
                when "change"
                  # Returns the same data but the work order id is the new work order id  and a new PriorWorkOrderId is set to the old work order and errors
                  update_cdb_with_oss_results(so, coresite_mh_to_update, data["WorkOrderId"])
                when "disconnect"
                  # Returns DisconnectID (work order id) and errors
                  if cached_data
                    cached_data[:so] = so
                    cached_data[:mh_to_update] = coresite_mh_to_update
                    cached_data[:wo] = data["DisconnectID"]
                  else
                    update_cdb_with_oss_results(so, coresite_mh_to_update, data["DisconnectID"])
                  end
                end
                result = true
              else
                set_summary_detail_and_details("CoreSite StatusCode is not 0 StatusCode = #{data["StatusCode"]} Errors = #{data["Errors"]}")
              end
              log_details("Errors returned from CoreSite OSS #{data["Errors"]}") if data["Errors"].present?
            else
              set_summary_detail_and_details("CoreSite OSS HTTP response is not OK (#{response})")
            end        
          end
        rescue
          SW_ERR "Exception when trying to send data to OSS: Exception is #{$!}"
          set_summary_detail_and_details("Failed to send to OSS Exception: #{$!} Traceback: #{$!.backtrace.join("\n")}")
        end
      else
        set_summary_detail_and_details("Failed to find required objects in CDB: Path MH:#{coresite_path_mh} Segment MH:#{coresite_segment_mh}")
      end
    else
      set_summary_detail_and_details("Failed to find required objects in CDB: SO:#{so} OrderEmail:#{requesting_user.inspect} Tenant_A:#{tenant_a} Tenant_Z:#{tenant_z}")
    end
    log_details("#{result ? "Successfully updated" : "Failed to update"} CoreSite OSS - result:#{result}")
    return result
  end
  
  def find_previous_work_order(segment)
    delivered_orders = segment.service_orders.where(:order_name => OrderStateful::DELIVERED)
    delivered_orders[-1].try(:title)
  end
  
  def update_cdb_with_oss_results(so, member_handles, work_order)
    so.title = work_order.to_s
    so.save!
    member_handles.each do |mh|
      mh.value = work_order.to_s
      mh.save!
    end
    log_details("Updated CDB after OSS data. WorkOrder: #{work_order} Member Handles #{member_handles.collect {|mh| [mh.id, mh.value]}}")
  end
  
  def update_tenant_contacts(order_info)
    tenant_ons = {:tenant_a => :sitename_a, :tenant_z => :sitename_z, :tenant_name => :sitename}.collect {|tenant, site| OperatorNetwork.find_by_name(order_info[tenant] + " [#{order_info[site]}]") if order_info.key?(tenant)}.compact
    tenant_ons.each do |tenant_on|
      contacts = oss_contacts(tenant_on)
      if contacts
        sp = tenant_on.service_provider
        ["Primary", "Secondary", "Technical"].each do |contact_type|
          contact = sp.contacts.where("`position` = ? and `name` like ?", contact_type, "%[#{tenant_on.site.name}]%").first
          if contact.nil?
            contact = Contact.new(:service_provider => sp)
          end          
          if contact
            if contacts["#{contact_type}Name"].present? && contacts["#{contact_type}Email"].present?
              contact.name = contacts["#{contact_type}Name"] + " [" + tenant_on.site.name + "]"
              contact.e_mail = contacts["#{contact_type}Email"] 
              contact.work_phone = contacts["#{contact_type}Phone"]
              contact.position = contact_type
              if contact.save
                log_details("contact updated #{PP.pp(contact.attributes, "")}")
              else
                log_details("Failed to update contact #{PP.pp(contact.attributes, "")}\n Errors: #{contact.errors.messages}")
              end
            else
              log_details("No contact details for #{contact_type}. Conatcts from OSS = #{PP.pp(contacts, "")}")
            end
          end
        end
      end
    end
  end
  
  def oss_contacts(tenant_operator_network)
    t = Time.now
    log_details("Getting contact for #{tenant_operator_network.name}")
    contact_info = nil
   
    if tenant_operator_network
      site_id = tenant_operator_network.site.id
      tenant_id = tenant_operator_network.id
      request_data = {'TenantId' => tenant_id, 'SiteId' => site_id}
      
      begin
        if @stub_oss
          uri = URI.parse("http://localhost:8090/api/contact")
        else
          uri = URI.parse(CoresiteConfig::instance.config["oss_url"] + "api/contact")
        end

        http = Net::HTTP.new(uri.host, uri.port)
        if uri.scheme == 'https'
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        end
        request = Net::HTTP::Post.new(uri.request_uri)
        request["Content-Type"] = "application/json"
        request.body = request_data.to_json
        log_details("Sending\nHeader:#{PP.pp(request.to_hash, "")}\nBody:#{PP.pp(request_data, "")} to #{uri}\n")
        if !@stub_oss
          response, data = http.request(request)
        else
          response = case stub_options[:contact]
            when :ok then Net::HTTPOK.new(1,2,3)
            when :connect_fail then Net::HTTPError.new(1,2)
            else Net::HTTPOK.new(1,2,3)
          end
          data = request_data
          data["StatusCode"] = stub_options[:contact] == :failure ? 100 : 0
          data["PrimaryName"] = "TestPrimaryName"
          data["PrimaryEmail"] = "TestPrimaryEmail"
          data["PrimaryPhone"] = "TestPrimaryPhone"
          data["SecondaryName"] = "TestSecondaryName"
          data["SecondaryEmail"] = "TestSecondaryEmail"
          data["SecondaryPhone"] = "TestSecondaryPhone"
          data["TechnicalName"] = "TestTechnicalName"
          data["TechnicalEmail"] = "TestTechnicalEmail"
          data["TechnicalPhone"] = "TestTechnicalPhone"
          data["Errors"] = ["No Technical Contact available for site:1 tenant:1179"]
          data = data.to_json
        end
            
        if response.is_a?(Net::HTTPOK) 
          data = JSON.parse(data)
          log_details("Received Data:#{PP.pp(data, "")}")
          if data["StatusCode"] == 0
            contact_info = data
          else
            set_summary_detail_and_details("CoreSite Contact StatusCode is not 0 StatusCode = #{data["StatusCode"]} Errors = #{data["Errors"]}")
          end
          log_details("Errors returned from CoreSite Contact OSS #{data["Errors"]}") if data["Errors"].present?
        else
          set_summary_detail_and_details("CoreSite Contact OSS HTTP response is not OK (#{response})")
        end        
      rescue
        SW_ERR "Exception when trying to send data to OSS Contact: Exception is #{$!}"
        set_summary_detail_and_details("Failed to send to OSS Contact Exception: #{$!} Traceback: #{$!.backtrace.join("\n")}")
      end
    else
      set_summary_detail_and_details("Contact tenant_operator_network is nil")
    end

    log_details("#{contact_info ? "Successfully got" : "Failed to get"} CoreSite OSS Contact")
    log_details("Time #{Time.now-t}")
    return contact_info
  end
  
  def make_enni_poe(order_info)             
    data = {
      "template" => "coresite_enni",
      "TenantName" => order_info[:tenant_name],
      "SiteName" => order_info[:sitename],
      "PhyType" => order_info[:phy_type],
      "Node" => order_info[:node],
      "Port" => order_info[:port],
      "WorkOrder" => order_info[:work_order],
      "ExternalName" => order_info[:external_name]
     }   
    outfile = ""
    csv_out = CSV.new(outfile)
    csv_out << data.keys
    csv_out << data.values
    
    poe_file = poe_filename_enni(order_info)
    poe_file.write(outfile)
    poe_file.close
    
    return poe_file
  end
    
  def create_service_order(order_info, order_type)
    so = nil
    so_created = false
    seg = Segment.find_by_id(order_info[:segment_id])
    tenant_a = OperatorNetwork.find_by_name("#{order_info[:tenant_a]} [#{order_info[:sitename_a]}]")
    if seg && tenant_a
      tenant_a_sp = tenant_a.service_provider
      primary_contact = tenant_a_sp.contacts.where("name like ? and position = ?", "%[#{order_info[:sitename_a]}]%", "Primary").first      
      technical_contact = tenant_a_sp.contacts.where("name like ? and position = ?", "%[#{order_info[:sitename_a]}]%", "Technical").first    
      if tenant_a_sp && primary_contact && technical_contact
        so = OnNetOvcOrder.new
        so.title = "XC: #{order_info[:tenant_a]} to #{order_info[:tenant_z]} #{order_type} #{Time.now}"
        so.primary_contact_id = primary_contact.id
        so.technical_contact_id = technical_contact.id
        so.testing_contact_id = technical_contact.id
        so.action = order_type
        so.requested_service_date = Date.parse(order_info[:requested_service_date])
        so.ordered_entity = seg
        so.ordered_entity_type = "Segment"
        so.ordered_entity_subtype = "OnNetOvc"
        so.path = seg.paths.first
        so.operator_network = seg.paths.first.operator_network
        so.notes = "NewCIR:#{order_info[:cir]}\n#{order_info[:tenant_z]} [#{order_info[:sitename_z]}]\nOrderEmail:#{order_info[:user]}"
        so.save
        so.title = "XC: #{order_info[:tenant_a]} to #{order_info[:tenant_z]} #{order_type} #{so.id}"
        so_created = so.save
      else
        set_summary_detail_and_details("Failed to find tennat A service provider(ON #{tenant_a.class}:#{tenant_a.id}), Primary or Technical contact")
      end
    else
      set_summary_detail_and_details("Failed to find Segment(#{order_info[:segment_id]}) or tenaant A(#{order_info[:tenant_a]})")
    end
    log_details("#{so_created ? "Successfully Created" : "Failed to Create"} a #{order_type} Service order for Segment #{order_info[:segment_id]}")
    log_details("#{so.errors.full_messages}") if so && !so_created
    return so_created
  end
    
  def check_cir(enni, cir, buyer, existing_cir = 0)
    valid_cir = false
    nni = enni
    nni = EnniNew.find_by_id(enni) unless enni.is_a?(EnniNew)
    if nni
      costs = nni.get_cenx_class_of_service_types
      if costs.size == 1
        additional = {costs.first.name=>{:cir=>cir.to_f, :eir=>0.0}} 
        errors = nni.cac_capacity_check(additional)
        valid_cir = errors.empty?
        available_bandwidth = nni.rate - nni.get_provisioned_bandwidth[:grand][:total] + existing_cir
        if !valid_cir
          message = "Insufficient bandwidth available on #{buyer ? "Buyer NNI" : "Target NNI"}. Available bandwidth is: #{available_bandwidth} Mbps"
          if @summary_detail.empty? or buyer
            set_summary_detail_and_details(message)
          else
            log_details(message)
          end
        end
      else
        set_summary_detail_and_details("Failed to find any or too many Cos Of Service Types for demarc #{nni.class}:#{nni.id}:#{nni.name}. Class Of Service Type #{costs.inspect}")
      end
    else
      set_summary_detail_and_details("Failed to find demarc #{enni}")
    end
    return valid_cir
  end
    
  def update_cir(segment)
    result = false
    # Get CIR from the order
    if segment.get_latest_order
      split_notes = segment.get_latest_order.notes.split("\n").find {|p| p[/^NewCIR:/]}
      if split_notes
        cir = split_notes.gsub(/NewCIR:/,"")
        coseps = segment.segment_end_points.collect {|ep| ep.cos_end_points}.flatten
        coseps.each {|cosep| cosep.ingress_cir_Mbps = cir; cosep.save}
        result = true
      else
        log_details("Failed to find CIR in Order Notes(#{segment.get_latest_order.notes})")
      end
    else
      log_details("Segment #{segment.id} does not have an order")
    end
    log_details("#{result ? "Successfully Updated" : "Failed to Update"} the CIR for #{segment.name} to #{cir}")
    return result
  end
  
  def get_existing_cir(segment)
    cir = nil
    if segment
      cir = segment.segment_end_points.first.cos_end_points.first.ingress_cir_Mbps
      if cir
        cir = cir.to_f
      end
      log_details("#{cir ? "Successfully got" : "Failed to get"} the CIR for #{segment.name} - CIR #{cir.inspect}")
    end
    return cir
  end
  
  def release_vlan_cir_vpls(segment)
    # Set CIR to N/A
    coseps = segment.segment_end_points.collect {|ep| ep.cos_end_points}.flatten
    coseps.each {|cosep| cosep.ingress_cir_kbps = "N/A"; cosep.save!}
    # Set VLAN to N/A
    segment.segment_end_points.each {|ep| ep.stags = "N/A";ep.save!}
    # Set VPLS id to N/A
    segment.service_id = "N/A"
    segment.save!
    log_details("Released the CIR, VLAN and VPLS for #{segment.name}")
  end
  
  def delete_router_config(segment, ignore_errors = false)
    CoresiteConfigureNode.stub_options = stub_options[:router_config]  
    result = config_router(segment, segment.generate_delete_config_hash, ignore_errors)
    CoresiteConfigureNode.stub_options = nil
    return result
  end
  
  def config_router(segment, router_cmds, ignore_errors = false)
    result = true
    config_result = nil
    router_cmds.each do |node, config|
      CoresiteConfigureNode.stub_options = stub_options[:router_config]
      config_result, output = CoresiteConfigureNode.configure(node, config, ignore_errors)
      CoresiteConfigureNode.stub_options = nil
      log_details("Config on #{node.name} #{config_result == 0 ? "Successfully" : "Failed"}")
      log_details(output)      
      result = result && (config_result == CoresiteConfigureNode::CONFIGURE_OK)
      break unless result
    end
    unless result
      # Log commands that should of been executed
      router_cmds.each do |node, config|
        log_details("\nCommands which should be of been executed on #{node.name} (#{node.primary_ip})}\n#{config}\n")
      end
    end
    segment.alarm_histories.first.add_event(Time.now.to_f*1000, result ? "cleared" : "failed", CoresiteConfigureNode.failure_reason(config_result)) 
    return result
  end
  
  def router_config_failed?(path)
    path.alarm_state.state != AlarmSeverity::OK
  end
  
  def send_disconnect_order_placed_emails(path, order_info)
    result = true
    begin      
      to = []
      to << (CoresiteMailer.coresite_disconnect_order(path,order_info).deliver).to
      to << (CoresiteMailer.buyer_disconnect_order(path,order_info).deliver).to
      to << (CoresiteMailer.seller_disconnect_order(path,order_info).deliver).to
      to.flatten!
      log_details("Sent disconnect order emails to #{to}")
    rescue
      set_summary_detail_and_details("Failed to send disconnect order emails Exception: #{$!}")
      log_details("Traceback: #{$!.backtrace.join("\n")}")
      result = false
    end
    return result
  end
  
  def send_disconnect_order_complete_emails(path, order_info)
    result = true
    begin
      to = []
      to << (CoresiteMailer.coresite_disconnect_order_accepted(path,order_info).deliver).to
      to << (CoresiteMailer.buyer_disconnect_order_accepted(path,order_info).deliver).to
      to << (CoresiteMailer.seller_disconnect_order_accepted(path,order_info).deliver).to
      to.flatten!
      log_details("Sent Disconnect Order complete emails to #{to}")
    rescue
      set_summary_detail_and_details("Failed to send disconnect order emails Exception: #{$!}")
      log_details("Traceback: #{$!.backtrace.join("\n")}")
      result = false
    end
    return result
  end
  
  def send_change_order_placed_emails(path, order_info)
    result = true
    begin      
      to = []
      to << (CoresiteMailer.coresite_change_order(path,order_info).deliver).to
      to << (CoresiteMailer.buyer_change_order(path,order_info).deliver).to
      to << (CoresiteMailer.seller_change_order(path,order_info).deliver).to
      to.flatten!
      log_details("Sent Changed Order placed emails to #{to}")
    rescue
      set_summary_detail_and_details("Failed to send change order placed emails Exception: #{$!}")
      log_details("Traceback: #{$!.backtrace.join("\n")}")
      result = false
    end
    return result
  end

  def send_change_order_accepted_emails(path, order_info)
    result = true
    begin
      to = []
      to << (CoresiteMailer.coresite_change_order_accepted(path,order_info).deliver).to
      to << (CoresiteMailer.buyer_change_order_accepted(path,order_info).deliver).to
      to << (CoresiteMailer.seller_change_order_accepted(path,order_info).deliver).to
      to.flatten!
      log_details("Sent Change Order accepted emails to #{to}")
    rescue
      set_summary_detail_and_details("Failed to change send order accepted emails Exception: #{$!}")
      log_details("Traceback: #{$!.backtrace.join("\n")}")
      result = false
    end
    return result
  end

  def send_order_placed_emails(path, order_info)
    result = true
    begin      
      to = []
      to << (CoresiteMailer.coresite_order_placed(path,order_info).deliver).to
      to << (CoresiteMailer.buyer_order_placed(path,order_info).deliver).to
      to << (CoresiteMailer.seller_order_placed(path,order_info).deliver).to
      to.flatten!
      log_details("Sent order placed emails to #{to}")
    rescue
      set_summary_detail_and_details("Failed to send order placed emails Exception: #{$!}")
      log_details("Traceback: #{$!.backtrace.join("\n")}")
      result = false
    end
    return result
  end
  
  def send_order_accepted_emails(path, order_info)
    result = true
    begin
      to = []
      to << (CoresiteMailer.coresite_order_accepted(path,order_info).deliver).to
      to << (CoresiteMailer.buyer_order_accepted(path,order_info).deliver).to
      to << (CoresiteMailer.seller_order_accepted(path,order_info).deliver).to
      to.flatten!
      log_details("Sent order accepted emails to #{to}")
    rescue
      set_summary_detail_and_details("Failed to send order accepted emails Exception: #{$!}")
      log_details("Traceback: #{$!.backtrace.join("\n")}")
      result = false
    end
    return result
  end
   
  def send_enni_create_emails(enni, order_info)
    result = true
    begin
      to = []
      to << (CoresiteMailer.coresite_enni_create(enni,order_info).deliver).to
      to.flatten!
      log_details("Sent enni create order emails to #{to}")
    rescue
      set_summary_detail_and_details("Failed to send enni create emails Exception: #{$!}")
      log_details("Traceback: #{$!.backtrace.join("\n")}")
      result = false
    end
    return result
  end
  
  def send_enni_disconnect_emails(enni, order_info, port_name)
    result = true
    begin
      to = []
      to << (CoresiteMailer.coresite_enni_disconnect(enni, order_info, port_name).deliver).to
      to.flatten!
      log_details("Sent enni disconnect order emails to #{to}")
    rescue
      set_summary_detail_and_details("Failed to send enni disconnect emails Exception: #{$!}")
      log_details("Traceback: #{$!.backtrace.join("\n")}")
      result = false
    end
    return result
  end
  
  def send_aborted_emails(path, order_info, action)
    result = true
    begin
      to = []
      to << (CoresiteMailer.coresite_order_aborted(path,order_info, action).deliver).to
      to << (CoresiteMailer.buyer_order_aborted(path,order_info, action).deliver).to
      to << (CoresiteMailer.seller_order_aborted(path,order_info, action).deliver).to
      to.flatten!
      log_details("Sent order #{action} emails to #{to}")
    rescue
      set_summary_detail_and_details("Failed to send order #{action} emails Exception: #{$!}")
      log_details("Traceback: #{$!.backtrace.join("\n")}")
      result = false
    end
    return result
  end
  
  def unique_name(order_info)
    "XC_" + [:tenant_a, :sitename_a, :nni_a, :vlan_a, :tenant_z, :sitename_z, :nni_z, :vlan_z, :cir].collect {|i| order_info[i]}.join("_").gsub(/ /, "_").gsub(/\//, "_")
  end
  
  def enni_unique_name(order_info)
    "NNI_" + [:tenant_name, :sitename, :phy_type, :node, :port, :work_order, :external_name].collect {|i| order_info[i]}.join("_").gsub(/ /, "_").gsub(/\//, "_").gsub(/;/, "_")
  end
  
  def poe_filename(order_info)
    file = Tempfile.new(["circuit_", "_poe.csv"], poe_dir)
  end
  
  def poe_filename_enni(order_info)
    Tempfile.new(["enni_", "_poe.csv"], poe_dir)
  end
  
  def update_build_logs(name, so = nil, send_email = true)
    # Set summary to be the latest result. Create a new builder log and assocaite it with a service order
    # If the ServiceOrder already has a builder log then append to it.
    log_details("Summary: #{@summary}")
    log_details("Summary Detail: #{@summary_detail}")
    
    if so.try(:automated_build_logs).blank?
      builder_log = CoresiteBuilderLog.create(:name => name, :associated_object => so, :details => @build_details + "\n" + BUILD_LOG_SEP + "\n", :summary => @summary, :summary_detail => @summary_detail)
    else
      builder_log = so.automated_build_logs.first
      builder_log.summary = @summary
      builder_log.summary_detail = @summary_detail
      log_to_append = @build_details + "\n" + BUILD_LOG_SEP + "\n"
      if "#{builder_log.details}".size + log_to_append.size >= BuilderLog::MAX_SIZE
        # Truncate - Could actually remove previous build details until enough space...TODO later
        builder_log.details = ""
      end
      builder_log.details = "#{builder_log.details}" + log_to_append
      builder_log.save
    end

    # Send email to coresite if it's a failure
    CoresiteMailer.coresite_failure(@build_details).deliver if (@summary != "Ok" && send_email)
    return builder_log    
  end
  
  # Move to an end state going through all intermediate states
  def path_to_state(path, state)
    @build_details << "\n"
    StateHelper.path_to_state(path, state, @build_details)
  end
  
  # Set to an explicit state and append to details the results and return a single true/false
  # can be used for any object/state
  def set_state(object, state)
    details = ""
    if state.new.is_a?(ProvState)
      pre, post, pre_reason, post_reason = object.set_prov_state(state)
    elsif state.new.is_a?(OrderState)
      pre, post, pre_reason, post_reason = object.set_order_state(state)
    end
    StateHelper.process_state_results(object, state, pre, post, pre_reason, post_reason, details)
    log_details(details)
    return pre && post
  end
  
    
  def delete_poes(poes)
    poes.each do |file|
      begin
        file.unlink
      rescue
        log_details("Failed to delete file #{file.path}\n#{$!}")
        SW_ERR "Failed to delete file #{file.path}\n#{$!}"
      end
    end
  end
  
  def log_poes(poes)
    poes.each do |file|
      begin
        file.open
        log_details("\nPoe file #{file.path}:\n") << file.read
        file.close
      rescue
        log_details("Failed to open/read file #{file.path}\n#{$!}")
      end
    end
  end
  
  def process_poe_result(result)
    # Result can be nil , true/false or a hash[:internal] = ModelMaker::Circuit class   
    result_details = ""
    if result
      if result.is_a?(Hash)
        if result[:internal] == nil
          result = false
        elsif result[:internal].is_a?(ModelMaker::Circuit)
          result_details = result[:internal].error
          result = result[:internal].successful
        else
          result_details = "Invalid return type #{result.inspect}"
          result = false
        end
      elsif result.is_a?(TrueClass)
        # True is valid - nothing to do
      else
        result_details = "Invalid return type #{result.inspect}"
        result = false
      end
    end    
    
    return result, result_details
  end

  def do_poe_parse_again(poe)
    File.truncate(osl_app.console.display[:filename], 0)
    osl_result = osl_app.cmd("poe parse-again #{poe.path}" ,@osl_output, true)[:data]
    log_details(File.read(osl_app.console.display[:filename]))
    return osl_result
  end
  
  def set_fallout(path, severity, details)
    return if path.nil?    
    fe_severity = FE_SEVERITY_MAPPING[severity]
    if path.latest_fallout_exception.nil?
      fe = FalloutException.new(:fallout_item => path)
    else
      fe = path.latest_fallout_exception
    end
    if fe_severity == :ok
      fe.clear_timestamp = Time.now
    end      
    fe.exception_type = "build"
    fe.severity = fe_severity
    fe.timestamp = Time.now
    fe.details = details
    fe.save!
  end
  
  def set_summary_detail_and_details(log_data)
    @build_details << "\n" + (@summary_detail = log_data)
  end
  
  def log_details(log_data)
    @build_details << "\n" + log_data
  end
  
  def validate_order_info(order_info)
    # Check the member handles don't have {....} in them - OSL will choke because {} is a "variable"
    valid = ![:member_handle_a, :member_handle_z, :external_name].any? {|key| order_info[key][/[{}]/] if order_info.key?(key)}
    if !valid
      set_summary_detail_and_details("Member handles cannot have { or } in them")
      @summary = "Failed"
    end
    return valid
  end
  
  def convert_ids_to_strings(order_info)
    result = true
    log_details("Order before conversion to strings #{PP.pp(order_info, "")}") 
    new_order_info = order_info.clone
    new_order_info[:cir] = order_info[:cir].to_i if order_info.key?(:cir)
    # Note this must be done first before replace the tenant_x with the name
    {:nni_a => :tenant_a, :nni_z => :tenant_z}.each do |nni, tenant|
      if new_order_info[nni] && new_order_info[tenant]
        if (obj = EnniNew.find_by_id(new_order_info[nni]))
          on = OperatorNetwork.find_by_id(new_order_info[tenant])
          if on
            new_order_info[nni] = obj.member_handles.find_by_owner_type_and_owner_id(ServiceProvider, on.service_provider).try(:value)
          else
            log_details("Failed to find Tenant #{new_order_info[tenant]}")
            result = false
          end
        else
          log_details("Failed to find EnniNew with id #{new_order_info[nni]}")
          result = false
        end
      end
    end
    
    {:tenant_name => OperatorNetwork, :tenant_a => OperatorNetwork, :tenant_z => OperatorNetwork}.each do |key, klass|
      if new_order_info[key]
        if (obj = klass.find_by_id(new_order_info[key]))
          new_order_info[key] = obj.tenant_name
          if key == :tenant_name
            site = :sitename
          else
            site = key.to_s.gsub(/tenant/, "sitename").to_sym
          end
          new_order_info[site] = obj.site.name
        else
          log_details("Failed to find #{klass.to_s} with id #{new_order_info[key]}")
          result = false
        end
      end
    end
    
    {:node => Node}.each do |key, klass|
      if new_order_info[key]
        if (obj = klass.find_by_id(new_order_info[key]))
          new_order_info[key] = obj.name
        else
          log_details("Failed to find #{klass.to_s} with id #{new_order_info[key]}")
          result = false
        end
      end
    end
    
    if !result
      new_order_info = {}
      set_summary_detail_and_details("Failed to convert order to strings")
    end
    log_details("Order after conversion to strings #{PP.pp(new_order_info, "")}") 
    # Update the tenant Contacts
    update_tenant_contacts(new_order_info) if new_order_info
    return new_order_info
  end
  

  def self.import_tenants(data, output = nil)
    output ||= $stdout
    results = {:total => 0, :complete => 0, :changed => 0, :skipped => 0, :contacts => 0, :failed => 0, :missing_id => 0, :missing_site => 0}
    csv = CSV.new(data, {:headers => true, :header_converters => :symbol, :return_headers => true})

    #Create the headers
    csv.shift if csv.header_row?

    required_headers = [:tenantid, :tenantname, :nationaltenantid, :nationaltenantname, :siteid, :sitename]
    missing_headers = required_headers - csv.headers
    if missing_headers.any?
      output.puts "Tenants file is missing the following headers: #{missing_headers.map{|h| h.to_s}.join(", ")}"
      return results
    end

    csv.each do |row|
      results[:total] += 1
      if row[:nationaltenantid].to_i.zero?
        results[:missing_id] += 1;
        next
      end

      site = Site.find_by_id(row[:siteid])
      if site.nil?
        results[:missing_site] += 1
        next
      end

      begin
        service_provider = ServiceProvider.find_by_id(row[:nationaltenantid])
        operator_network_type = nil
        national_tenant_name = row[:nationaltenantname].strip
        tenant_name = row [:tenantname]
        if service_provider.nil?
          ActiveRecord::Base.transaction do
            output.puts "Service Provider: #{national_tenant_name}"
            #Build Service Provider
            service_provider = ServiceProvider.new({:name => national_tenant_name})
            service_provider.id = row[:nationaltenantid]
            service_provider.save
            raise SaveError.new("  Failed to save ServiceProvider: #{service_provider.errors.full_messages}") if service_provider.errors.any?
            service_provider.reload
            #Build Operator Network Type
            ont_name = "#{national_tenant_name} Network"
            operator_network_type = OperatorNetworkType.new({:name => ont_name})
            operator_network_type.service_provider = service_provider
            operator_network_type.save
            raise SaveError.new("  Failed to save OperatorNetworkType: #{operator_network_type.errors.full_messages}") if operator_network_type.errors.any?
            operator_network_type.reload
          end

          #Import OSS Data
          script = Rails.root.join('lib', 'oss_import', 'oss_import_new.rb')
          oss_file = Rails.root.join('lib', 'oss_import', 'imported_oss_data', 'CoreSiteTennant.txt')
          if defined?(Sin)
            script = Rails.root.join('lib', 'cdb', 'oss_import', 'oss_import_new.rb')
            oss_file = Rails.root.join('lib', 'cdb', 'oss_import', 'imported_oss_data', 'CoreSiteTennant.txt')
          end
          command = "ruby #{script} --file \"#{oss_file}\" --member \"#{service_provider.name.gsub('"', '\"')}\" --network \"#{operator_network_type.name.gsub('"', '\"')}\" --db \"#{Rails.env}\""
          @@logger.info("Tenant Import: Running oss_import command: #{command}")
          process = IO.popen(command)
          @@logger.info("Tenant Import: Output of oss_import command: \n#{process.readlines.join("\n")}")
          Process.wait(process.pid)
          raise SaveError.new("  OSS Import Failed for #{national_tenant_name}") unless $? == 0 
        elsif service_provider.name != national_tenant_name
          output.puts "Changing Service Provider: #{service_provider.name} to #{national_tenant_name}"
          operator_network_type = service_provider.operator_network_types.find_by_name("#{service_provider.name} Network")
          raise SaveError.new("  Could not find OperatorNetworkType #{service_provider.name} Network for #{service_provider.name}") if operator_network_type.nil?
          operator_network_type.name = "#{national_tenant_name} Network"
          operator_network_type.save
          raise SaveError.new("  Failed to save OperatorNetworkType: #{operator_network_type.errors.full_messages}") if operator_network_type.errors.any?
          operator_network_type.reload

          service_provider.name = national_tenant_name
          service_provider.save
          raise SaveError.new("  Failed to save ServiceProvider: #{service_provider.errors.full_messages}") if service_provider.errors.any?
          service_provider.reload
          results[:changed] += 1
        else
          operator_network_type = service_provider.operator_network_types.find_by_name("#{national_tenant_name} Network")
          raise SaveError.new("  Could not find OperatorNetworkType #{national_tenant_name} Network for #{service_provider.name}") if operator_network_type.nil?
        end

        operator_network = OperatorNetwork.find_by_id(row[:tenantid])
        if operator_network.nil?
          operator_network_name = "#{tenant_name} [#{site.name}]"
          output.puts "Operator Network: #{operator_network_name} for #{national_tenant_name}"
          operator_network = OperatorNetwork.new({:name => operator_network_name})
          operator_network.service_provider = service_provider
          operator_network.operator_network_type = operator_network_type
          operator_network.id = row[:tenantid]
          operator_network.save
          raise SaveError.new("  Failed to save OperatorNetwork: #{operator_network.errors.full_messages}") if operator_network.errors.any?
          results[:complete] += 1
        else
          results[:skipped] += 1
        end

        contact_string = ""
        if row[:contact1email]
          primary_contact_name = "#{row[:contact1email].match(/(.*)@.*/)[1]} [#{site.name}]"
          primary_contact = service_provider.contacts.find_by_name_and_position(primary_contact_name, "Primary")
          if primary_contact.nil?
            primary_contact = Contact.new({:name => primary_contact_name, :e_mail => row[:contact1email], :work_phone => "TBD", :position => "Primary"})
            primary_contact.service_provider = service_provider
            primary_contact.save
            raise SaveError.new(" Failed to save Primary Contact: #{contact.errors.full_messages}") if primary_contact.errors.any?
            results[:contacts] += 1
            contact_string += " Primary"
          elsif primary_contact.e_mail != row[:contact1email]
            primary_contact.e_mail = row[:contact1email]
            primary_contact.save
            raise SaveError.new(" Failed to save Primary Contact: #{contact.errors.full_messages}") if primary_contact.errors.any?
            results[:contacts] += 1
          end
        end

        if row[:contact2email]
          secondary_contact_name = "#{row[:contact2email].match(/(.*)@.*/)[1]} [#{site.name}]"
          secondary_contact = service_provider.contacts.find_by_name_and_position(secondary_contact_name, "Secondary")
          if secondary_contact.nil?
            secondary_contact = Contact.new({:name => secondary_contact_name, :e_mail => row[:contact2email], :work_phone => "TBD", :position => "Secondary"})
            secondary_contact.service_provider = service_provider
            secondary_contact.save
            raise SaveError.new(" Failed to save Secondary Contact: #{contact.errors.full_messages}") if secondary_contact.errors.any?
            results[:contacts] += 1
            contact_string += " Secondary"
          elsif secondary_contact.e_mail != row[:contact2email]
            secondary_contact.e_mail = row[:contact2email]
            secondary_contact.save
            raise SaveError.new(" Failed to save Secondary Contact: #{contact.errors.full_messages}") if secondary_contact.errors.any?
            results[:contacts] += 1
          end
        end
        
        if row[:techcontactemail]
          technical_contact_name = "#{row[:techcontactemail].match(/(.*)@.*/)[1]} [#{site.name}]"
          technical_contact = service_provider.contacts.find_by_name_and_position(technical_contact_name, "Technical")
          if technical_contact.nil?
            technical_contact = Contact.new({:name => technical_contact_name, :e_mail => row[:techcontactemail], :work_phone => "TBD", :position => "Technical"})
            technical_contact.service_provider = service_provider
            technical_contact.save
            raise SaveError.new(" Failed to save Technical Contact: #{contact.errors.full_messages}") if technical_contact.errors.any?
            results[:contacts] += 1
            contact_string += " Technical"
          elsif technical_contact.e_mail != row[:techcontactemail]
            technical_contact.e_mail = row[:techcontactemail]
            technical_contact.save
            raise SaveError.new(" Failed to save Technical Contact: #{contact.errors.full_messages}") if technical_contact.errors.any?
            results[:contacts] += 1
          end
        end

        output.puts " Created#{contact_string} contacts" unless contact_string.empty?

      rescue SaveError => e
        output.puts e.message
        results[:failed] += 1
      end
    end
    return results
  end

end