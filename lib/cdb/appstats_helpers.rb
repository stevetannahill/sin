require 'appstats'

def appstats_page_counter
  return unless params[:monit].nil?
  begin
    common_contexts = {}
    if session["cas_user"].nil?
      common_contexts[:session_id] = session[:session_id]
    else
      common_contexts[:user_email] = session["cas_user"]
      common_contexts[:service_provider_id] = session["service_provider_id"]
    end
    Appstats::Logger.entry('page-view', { :controller => params[:controller], :action => params[:action] }.merge(common_contexts))
  rescue Exception => e
    Appstats::Logger.entry('appstats-exception',:error => e.message)
  end
end