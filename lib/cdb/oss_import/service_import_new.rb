require File.dirname(__FILE__) + "/common_import_new"
class RangeRow < Struct.new(:number, :lo, :hi); end

class SegmentTypeRow < Struct.new(:number, :object); end
class CosTypeRow < Struct.new(:number, :letter, :object); end
class UniTypeRow < Struct.new(:number, :letter, :object); end
class ClassOfServiceRow < Struct.new(:number, :st_number, :object); end
class UniProfileRow < Struct.new(:st_number, :object); end
class SlgRow < Struct.new(:st_number, :cos_number, :object); end
class BwpRow < Struct.new(:st_number, :cos_number, :object); end

def process_service_data section_data

  @raw_item_count += section_data.size

  #Control flags
  current_table_pointer = 0
  next_table_pointer = 0
  table_data = []

  result_ok, next_table_pointer = find_next_table section_data, 1
  while result_ok
    current_table_pointer = next_table_pointer
    result_ok, next_table_pointer = find_next_table section_data, current_table_pointer+1
    if result_ok
      print_info "\n<------------------------------------>"
      #puts "==== Next Table [#{current_table_pointer}..#{next_table_pointer-1}]"
      table_data = section_data[current_table_pointer..next_table_pointer-1]
      result_ok = process_service_table_data table_data
    end
  end

  @db_row_count += @segment_types.size
  @db_row_count += @es_types.size
  @db_row_count += @cos_types.size*6
  @db_row_count += @uni_types.size

end

def process_service_table_data table_data
  result_ok = true
  @table_count += 1
  @item_count += table_data.size

  table_title = table_data[0]
  print_info "Processing Table: #{table_title}; Size: #{table_data.size} ..."

  case table_title
    when "<Your Service Offerings>", "<Your Service Offering>"
      process_service_types table_data
    when "<Service Attributes>"
      process_service_type_attrs table_data
    when "<Your CoS Types>"
      process_cos_types table_data
    when "<Service Type CoS Mapping>"
      process_cos_mapping table_data
    when "<CoS Priority Mapping>"
      process_cos_priority_mapping table_data
    when "<Default CoS ID>"
      process_default_cos table_data
    when "<CoS Guarantees>"  
      process_cos_guarantees table_data
    when "<ENNI Ingress Rate Limiting>",'<Aggregation Interface Ingress Rate Limiting>'
      @table_processing_mode = "ENNI-INGRESS"
      process_rate_limiting table_data
    when "<ENNI Egress Rate Limiting>", '<Aggregation Interface Egress Rate Limiting>'
      @table_processing_mode = "ENNI-EGRESS"
      process_rate_limiting table_data
    when "<UNI Ingress Rate Limiting>", '<Cell Site UNI Ingress Rate Limiting>'
      @table_processing_mode = "UNI-INGRESS"
      process_rate_limiting table_data
    when "<UNI Egress Rate Limiting>", '<Cell Site UNI Egress Rate Limiting>'
      @table_processing_mode = "UNI-EGRESS"
      process_rate_limiting table_data
    when"<Service Rates per CoS>"
      process_cos_rates table_data
    when "<Your UNI Types>"
      process_uni_types table_data
    when "<Service Type UNI Mapping>"
      process_uni_mapping table_data
    when "<UNI Attributes>"
      process_uni_attributes table_data
    when '<Multi-Cell Site UNI Protection>', '<Access Provider Test Procedure>', '<Access Provider SLA Reports>','<Service Turn-up Test Procedure>' , '<SLA KPI measurement method>'
      skip_over_table table_data
    else
      table_processing_error table_title, table_data.size
      result_ok = false
  end
  return result_ok
end

def process_service_types table_data

  print_array_data table_data

  @num_service_columns = table_data.size-2

  1.upto(@num_service_columns) do |i|
    if (is_non_empty_data table_data[i])
      ot = SegmentType.new
      ot.operator_network_type_id = @ont.id
      ot.name = "#{table_data[i]} Segment"
      @segment_types << SegmentTypeRow.new( i, ot )

      est = EthernetServiceType.new
      est.operator_network_type_id = @ont.id
      est.name = "#{table_data[i]}"
      est.segment_types.push(ot)
      @es_types << SegmentTypeRow.new( i, est )
    end

#    notes = @oss.notes
#    notes = notes + table_data[table_data.size-1]

    @oss.notes = table_data[table_data.size-1]
    save_to_database "Offered Services Study", @oss
  end

  def add_any_type_est(est, params, list, table)
    if (!params)||(params[:id] == "")
      return false, nil, "invalid info"
    end

    obj = table.find(params[:id])

    if (list.include?(obj))
      return false, nil, "duplicate"
    end

    list.push(obj)

    if est.save
      return true, obj, ""
    else
      return false, obj, "save failed"
    end
  end


  print_info "Segment Types Found: #{@segment_types.size} out of possible #{@num_service_columns}"

end

def process_service_type_attrs table_data

  last_row_processed = 0
  print_array_data table_data

  @segment_types.each do |st|
    print_debug "Fill [#{st.number}]; #{st.object.name} segment_type with [#{last_row_processed+st.number}] #{table_data[last_row_processed+st.number]}"
    rows = []
    rows << TableRow.new("string", "segment_type", table_data[last_row_processed+st.number])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns

  @segment_types.each do |st|
    print_debug "Fill [#{st.number}]; #{st.object.name} underlying_technology with [#{last_row_processed+st.number}] #{table_data[last_row_processed+st.number]}"
    rows = []
    rows << TableRow.new("string", "underlying_technology", table_data[last_row_processed+st.number])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns

  if ((@oss_document_type == 'Core') && (@oss_document_version.to_f >= 3.12)) or (@oss_document_type == 'Sprint')

    @segment_types.each do |st|
      print_debug "Fill [#{st.number}]; #{st.object.name} redundancy_mechanism with [#{last_row_processed+st.number}] #{table_data[last_row_processed+st.number]}"
      rows = []
      rows << TableRow.new("string", "redundancy_mechanism", table_data[last_row_processed+st.number])

      rows.each do |r| process_row r, st.object end
    end
    
    #TODO Deal with LS versioning detals for this as well
    last_row_processed += @num_service_columns
  end


  if @oss_document_type == 'LS'
    @segment_types.each do |st|
      print_debug "Fill [#{st.number}]; #{st.object.name} Network Redundancy with [#{last_row_processed+st.number}] #{table_data[last_row_processed+st.number]}"
      rows = []
      rows << TableRow.new("string", "notes", table_data[last_row_processed+1])

      rows.each do |r| process_row r, st.object end
    end
    last_row_processed += @num_service_columns
  end

  #Skip over service impact time
  if @oss_document_type == 'Sprint'
    print_debug "Skip service impact time"
    last_row_processed += @num_service_columns
  end

  @segment_types.each do |st|
    print_debug "Fill [#{st.number}]; #{st.object.name} equipment_vendor with [#{last_row_processed+st.number}] #{table_data[last_row_processed+st.number]}"
    rows = []
    rows << TableRow.new("string", "equipment_vendor", table_data[last_row_processed+st.number])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns

  if ((@oss_document_type == 'LS') && (@oss_document_version.to_f > 1.4)) or (@oss_document_type == 'Sprint')
    # Skip the next 4 rows that deal w Impacts of SW Upgrades and BW Changes
    print_debug "Skip SW Upgrades and BW Changes"
    last_row_processed += @num_service_columns*4
  end

  @segment_types.each do |st|
    print_debug "Fill [#{st.number}]; #{st.object.name} mef9_cert with [#{last_row_processed+st.number*2-1}] #{table_data[last_row_processed+st.number*2-1]}"
    rows = []
    rows << TableRow.new("string", "mef9_cert", table_data[last_row_processed+st.number*2-1])
    rows << TableRow.new("string", "mef14_cert", table_data[last_row_processed+st.number*2])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns*2

  @segment_types.each do |st|
    print_debug "Fill [#{st.number}]; #{st.object.name} max_mtu with [#{last_row_processed+st.number}] #{table_data[last_row_processed+st.number]}"
    rows = []
    rows << TableRow.new("string", "max_mtu", table_data[last_row_processed+st.number])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns


  @segment_types.each do |st|
    print_debug "Fill [#{st.number}]; #{st.object.name} c_vlan_id_preservation with [#{last_row_processed+st.number}] #{table_data[last_row_processed+st.number]}"
    rows = []
    rows << TableRow.new("string", "c_vlan_id_preservation", table_data[last_row_processed+st.number])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns


  @segment_types.each do |st|
    print_debug "Fill [#{st.number}]; #{st.object.name} c_vlan_cos_preservation with [#{last_row_processed+st.number}] #{table_data[last_row_processed+st.number]}"
    rows = []
    rows << TableRow.new("boolean", "c_vlan_cos_preservation", table_data[last_row_processed+st.number])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns


   if (@oss_document_type == 'Sprint')
    # Skip the next 3 rows that deal w Impacts of SW Upgrades and BW Changes
    print_debug "Skip MAC Learning"
    last_row_processed += @num_service_columns*3
  end

  @segment_types.each do |st|
    print_debug "Fill [#{st.number}]; #{st.object.name} service_attribute_notes with [#{last_row_processed+st.number}] #{table_data[last_row_processed+st.number]}"
    rows = []
    rows << TableRow.new("string", "service_attribute_notes", table_data[last_row_processed+st.number])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns  

  @segment_types.each do |st|
    print_debug "Fill [#{st.number}]; #{st.object.name} max_num_cos with [#{last_row_processed+st.number}] #{table_data[last_row_processed+st.number]}"
    rows = []
    rows << TableRow.new("integer", "max_num_cos", table_data[last_row_processed+st.number])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns

  @segment_types.each do |st|
    rows = []
    rows << TableRow.new("string", "color_marking", table_data[last_row_processed+st.number*2-1])
    rows << TableRow.new("string", "color_marking_notes", table_data[last_row_processed+st.number*2])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns*2

  @segment_types.each do |st|
    print_debug "Fill [#{st.number}; #{st.object.name} with [#{last_row_processed+st.number*2-1}] #{table_data[last_row_processed+st.number*2-1]}"
    rows = []
    rows << TableRow.new("string", "unicast_frame_delivery_conditions", table_data[last_row_processed+st.number*2-1])
    rows << TableRow.new("string", "unicast_frame_delivery_details", table_data[last_row_processed+st.number*2])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns*2

  @segment_types.each do |st|
    print_debug "Fill [#{st.number}; #{st.object.name} with [#{last_row_processed+st.number*2-1}] #{table_data[last_row_processed+st.number*2-1]}"
    rows = []
    rows << TableRow.new("string", "multicast_frame_delivery_conditions", table_data[last_row_processed+st.number*2-1])
    rows << TableRow.new("string", "multicast_frame_delivery_details", table_data[last_row_processed+st.number*2])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns*2

  @segment_types.each do |st|
    print_debug "Fill [#{st.number}]; #{st.object.name} with [#{last_row_processed+st.number*2-1}] #{table_data[last_row_processed+st.number*2-1]}"
    rows = []
    rows << TableRow.new("string", "broadcast_frame_delivery_conditions", table_data[last_row_processed+st.number*2-1])
    rows << TableRow.new("string", "broadcast_frame_delivery_details", table_data[last_row_processed+st.number*2])

    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns*2

  @segment_types.each do |st|
    rows = []
    rows << TableRow.new("append_string", "notes", table_data[last_row_processed+1])
    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += 1

  # Sanity check
  if (last_row_processed != table_data.size-1)
    print_error "Service Attributes not properly Processed [#{last_row_processed}/#{table_data.size-1}]"
  else
    print_info "Service Attributes Processed OK - [#{last_row_processed}/#{table_data.size-1}]"
  end

  # Save Service Types into the DB
  @segment_types.each do |ot|
    save_to_database "Segment Type", ot.object
  end

  et = DemarcType.find(@et_id)
  if !et
    print_error "Cant Find ENNI Type"
  end

  @es_types.each do |est|
    save_to_database "Ethernet Service Type", est.object
    et.ethernet_service_types.push(est.object)
  end

end

def process_cos_types table_data

  print_array_data table_data
  @num_cos_columns = table_data.size-2

  1.upto(@num_cos_columns) do |i|
    if (is_non_empty_data table_data[i])
      cost = ClassOfServiceType.new
      cost.operator_network_type_id = @ont.id
      cost.name = "#{table_data[i]}"
      @cos_types << CosTypeRow.new( i, 'X', cost )
    end
  end
  
  letters = []
  if ((@oss_document_type == 'Core') && (@oss_document_version.to_f >= 3.14))
    letters = ['A','B','C','D','E','F','G','H','I']
  else
    letters = ['A','B','C','D','E','F','G','H']
  end

  @cos_types.each do |cost|
    cost.letter = letters[cost.number-1]
    print_info "COSType: #{cost.number} == #{cost.letter}"
  end

  #Copy Note into all CoS's
  @cos_types.each do |cost|
    rows = []
    rows << TableRow.new("string", "notes", table_data[table_data.size-1])
    rows.each do |r| process_row r, cost.object end
  end

  print_info "Cos Types Found: #{@cos_types.size} out of possible #{@num_cos_columns}"

end

def process_cos_mapping table_data
  print_array_data table_data

  # Sanity check
  expected_size = @num_service_columns*@num_cos_columns+2
  if (table_data.size != expected_size)
    print_error "#{table_data[0]} is wrong size [got: #{table_data.size} expect:#{expected_size}]"
  else
    print_info "Processing #{table_data[0]}: [got: #{table_data.size} expect:#{expected_size}]"
  end

  @es_types.each do |est|
    1.upto(@num_cos_columns) do |i|
      cos_mapping = table_data[((est.number-1)*@num_cos_columns)+i]
      if (is_non_empty_data cos_mapping)
        cost = @cos_types.select{|ct| ct.letter == cos_mapping}.pop
        if cost
          cost.object.ethernet_service_types.push(est.object)
          print_debug "Cos: #{cost.object.name} added to EST: #{est.object.name}"
        else
          print_error "Cos #{cos_mapping} NOT added to EST: #{est.object.name}"
        end
      end
    end
  end
end

def process_cos_priority_mapping table_data
  print_array_data table_data
  last_row_processed = 0

  if ((@oss_document_type == 'Core') && (@oss_document_version.to_f >= 3.14))
    data_per_cos = 8
  elsif ((@oss_document_type == 'LS') && (@oss_document_version.to_f > 1.6)) or (@oss_document_type == 'Sprint')
    data_per_cos = 7
  else
    data_per_cos = 6
  end


  @cos_types.each do |cost|
    rows = []
    if ((@oss_document_type == 'Core') && (@oss_document_version.to_f >= 3.14))
      rows << TableRow.new("string", "cos_id_type_enni", table_data[last_row_processed+1])
      rows << TableRow.new("string", "cos_mapping_enni_ingress", table_data[last_row_processed+2])
      rows << TableRow.new("string", "cos_marking_enni_egress", table_data[last_row_processed+3])
      rows << TableRow.new("string", "cos_marking_enni_egress_yellow", table_data[last_row_processed+4])
      rows << TableRow.new("string", "cos_id_type_uni", table_data[last_row_processed+5])
      rows << TableRow.new("string", "cos_mapping_uni_ingress", table_data[last_row_processed+6])
      rows << TableRow.new("string", "cos_marking_uni_egress", table_data[last_row_processed+7])
      rows << TableRow.new("string", "cos_marking_uni_egress_yellow", table_data[last_row_processed+8])
    elsif ((@oss_document_type == 'LS') && (@oss_document_version.to_f > 1.6)) or (@oss_document_type == 'Sprint')
      rows << TableRow.new("string", "cos_id_type_enni", table_data[last_row_processed+1])
      rows << TableRow.new("string", "cos_mapping_enni_ingress", table_data[last_row_processed+2])
      rows << TableRow.new("string", "cos_marking_enni_egress", table_data[last_row_processed+3])
      rows << TableRow.new("string", "cos_id_type_uni", table_data[last_row_processed+5])
      rows << TableRow.new("string", "cos_mapping_uni_ingress", table_data[last_row_processed+6])
      rows << TableRow.new("string", "cos_marking_uni_egress", table_data[last_row_processed+7])
    else
      rows << TableRow.new("string", "cos_id_type_enni", table_data[last_row_processed+1])
      rows << TableRow.new("string", "cos_mapping_enni_ingress", table_data[last_row_processed+2])
      rows << TableRow.new("string", "cos_marking_enni_egress", table_data[last_row_processed+3])
      rows << TableRow.new("string", "cos_id_type_uni", table_data[last_row_processed+4])
      rows << TableRow.new("string", "cos_mapping_uni_ingress", table_data[last_row_processed+5])
      rows << TableRow.new("string", "cos_marking_uni_egress", table_data[last_row_processed+6])
    end

    rows.each do |r| process_row r, cost.object end
    last_row_processed += data_per_cos
  end

  #Copy Note into all CoS's
  @cos_types.each do |cost|
    rows = []
    rows << TableRow.new("append_string", "notes", table_data[table_data.size-1])
    rows.each do |r| process_row r, cost.object end
  end

  # Sanity check
  if (last_row_processed != @cos_types.size*data_per_cos)
    print_error "Cos Priority not properly Processed [#{last_row_processed}/#{@cos_types.size*data_per_cos}"
  else
    print_info "Cos Priority Processed OK - [#{last_row_processed}/#{@cos_types.size*data_per_cos}]"
  end

end

def process_default_cos table_data
  print_array_data table_data

  # Sanity check
  if ((@oss_document_type == 'Core') && (@oss_document_version.to_f >= 3.14))
    expected_size = @num_service_columns*2+2
  else
    expected_size = @num_service_columns*2+1
  end
  
  if (table_data.size != expected_size)
    print_error "#{table_data[0]} is wrong size [got: #{table_data.size} expect:#{expected_size}]"
  else
    print_info "Processing #{table_data[0]}: [got: #{table_data.size} expect:#{expected_size}]"
  end

  num_end_point_columns = 2
  endpoint_column_names = ["ENNI", "UNI"]
  @es_types.each do |est|
    1.upto(num_end_point_columns) do |i|
      location = endpoint_column_names[i-1]
      default_cos = table_data[((est.number-1)*num_end_point_columns)+i]
      if (is_non_empty_data default_cos)
        if default_cos == "Drop"
          cost = CosTypeRow.new( i, 'X', nil )
        elsif default_cos == "See Note"
          cost = CosTypeRow.new( i, 'See Note', nil )
        elsif default_cos == "N/A"
          cost = CosTypeRow.new( i, 'N/A', nil )
        else
          cost = @cos_types.select{|ct| ct.letter == default_cos}.pop
        end
        
        if cost
          segmentept = SegmentEndPointType.new
          segmentept.operator_network_type_id = @ont.id
          segmentept.name = "#{location} Endpoint [#{est.object.name}]"
          if cost.letter == 'See Note' || cost.letter == 'N/A'
            segmentept.class_of_service_type_id = 0
            if cost.letter == 'N/A'
              segmentept.notes = "N/A: single CoS"
            elsif ((@oss_document_type == 'Core') && (@oss_document_version.to_f >= 3.14))
              segmentept.notes = table_data[table_data.size-1]
            end
          else
            segmentept.class_of_service_type = cost.object
          end 

          segmentept.ethernet_service_types.push(est.object)

          save_to_database "Segment Endpoint Type ", segmentept
          print_debug "Cos: #{segmentept.name} added to EST: #{est.object.name} - #{segmentept.class_of_service_type_id}"
        else
          print_error "Cos #{default_cos} NOT added to EST: #{est.object.name}"
        end
      else
        # Cant be blank
        print_error "Default Cos Data Missing for ST: #{est.object.name} (on #{location} side)"
      end
    end
  end
end

def process_cos_guarantees table_data

  print_array_data table_data
  last_row_processed = 0
  data_per_cos = 5

  @cos_types.each do |cost|
    rows = []
    rows << TableRow.new("string", "availability", table_data[last_row_processed+1])
    rows << TableRow.new("string", "frame_loss_ratio", table_data[last_row_processed+2])
    rows << TableRow.new("string", "mttr_hrs", table_data[last_row_processed+3])
    #Copy Note into all CoS's
    rows << TableRow.new("append_new_line", "notes", table_data[table_data.size-1])
    rows.each do |r| process_row r, cost.object end

    save_to_database "CoS Type #{cost.object.name}", cost.object

    slg = ServiceLevelGuaranteeType.new
    slg.class_of_service_type_id = cost.object.id
    slg.min_dist_km = '0'
    slg.max_dist_km = '*'

    slg.delay_us = is_non_empty_data(table_data[last_row_processed+4]) ? (table_data[last_row_processed+4]).to_i*1000 : 'N/A'
    slg.delay_variation_us = is_non_empty_data(table_data[last_row_processed+5]) ? table_data[last_row_processed+5] : 'N/A'

    save_to_database "CoS Type #{cost.object.name} SLG ", slg

    last_row_processed += data_per_cos
  end

  # Sanity check
  if (last_row_processed != @cos_types.size*data_per_cos)
    print_error "Cos Guarantees not properly Processed [#{last_row_processed}/#{@cos_types.size*data_per_cos}"
  else
    print_info "Cos Guarantees Processed OK - [#{last_row_processed}/#{@cos_types.size*data_per_cos}]"
  end

end

def process_rate_limiting table_data
  print_array_data table_data

  if ((@oss_document_type == 'Sprint') && (@oss_document_version.to_f > 1.3))
    expected_size = 6
  else
    expected_size = 5
  end

  # Sanity check
  if (table_data.size != expected_size)
    print_error "#{table_data[0]} is wrong size [got: #{table_data.size} expect:#{expected_size}]"
  else
    print_info "Processing #{table_data[0]}: [got: #{table_data.size} expect:#{expected_size}]"
  end

   @rate_limiting_performed = table_data[1]
   @rate_limiting_mechanism = table_data[2]
   if expected_size == 6
    @rate_limiting_layer = table_data[4]
    @rate_limiting_notes = table_data[5]
   else
    @rate_limiting_layer = table_data[3]
    @rate_limiting_notes = table_data[4]
  end
end

def process_cos_rates table_data
  print_array_data table_data

  # Sanity check
  if ((@oss_document_type == 'Core'))
    expected_size = @num_cos_columns*6+2
    data_per_cos = 6
  elsif (@oss_document_type == 'LS') or (@oss_document_type == 'Sprint')
    expected_size = @num_cos_columns*4+2
    data_per_cos = 4
  end
 
  if (table_data.size != expected_size)
    print_error "#{table_data[0]} is wrong size [got: #{table_data.size} expect:#{expected_size}]"
  else
    print_info "Processing #{table_data[0]}: [got: #{table_data.size} expect:#{expected_size}]"
  end

  last_row_processed = 0

  @cos_types.each do |cost|

   bwpt = BwProfileType.new
   bwpt.class_of_service_type_id = cost.object.id
   bwpt.bwp_type = @table_processing_mode

   rows = []
   rows << TableRow.new("boolean", "rate_limiting_performed", @rate_limiting_performed)
   rows << TableRow.new("string", "rate_limiting_mechanism", @rate_limiting_mechanism)
   rows << TableRow.new("string", "rate_limiting_layer", @rate_limiting_layer)
   rows << TableRow.new("string", "rate_limiting_notes", @rate_limiting_notes)
   rows << TableRow.new("string", "cir_range_notes", table_data[last_row_processed+1])
   rows << TableRow.new("string", "cbs_range_notes", table_data[last_row_processed+2])
   rows << TableRow.new("string", "eir_range_notes", table_data[last_row_processed+3])
   rows << TableRow.new("string", "ebs_range_notes", table_data[last_row_processed+4])
   if ((@oss_document_type == 'Core'))
    rows << TableRow.new("string", "coupling_flag", table_data[last_row_processed+5])
    rows << TableRow.new("string", "color_mode", table_data[last_row_processed+6])
   end
   rows << TableRow.new("string", "notes", table_data[table_data.size-1])

   rows.each do |r| process_row r, bwpt end
   last_row_processed += data_per_cos
   
   save_to_database "BWP Type", bwpt

  end

  # Sanity check
  if (last_row_processed != @cos_types.size*data_per_cos)
    print_error "Cos Rates not properly Processed [#{last_row_processed}/#{@cos_types.size*data_per_cos}"
  else
    print_info "Cos Rates Processed OK - [#{last_row_processed}/#{@cos_types.size*data_per_cos}]"
  end

end

def process_uni_types table_data
  print_array_data table_data
  @num_uni_columns = table_data.size-2

  1.upto(@num_uni_columns) do |i|
    if (is_non_empty_data table_data[i])
      ut = UniType.new
      ut.demarc_type_type ='UNI Type'
      ut.operator_network_type_id = @ont.id

      if @oss_document_type == 'Core'
        ut.name = "#{table_data[i]}"
      else
        ut.name = "#{table_data[i]}"
        #ut.name = "#{table_data[i]} (for Light Squared)"
      end
      
      @uni_types << UniTypeRow.new( i, 'X', ut )
    end
  end

  letters = ['A','B','C','D','E','F','G','H',]
  @uni_types.each do |ut|
    ut.letter = letters[ut.number-1]
    print_info "UNIT #{ut.number} == #{ut.letter}"
  end

  print_info "Uni Types Found: #{@uni_types.size} out of possible #{@num_uni_columns}"
end

def process_uni_mapping table_data
  print_array_data table_data

  # Sanity check
  if ((@oss_document_type == 'Core') && (@oss_document_version.to_f >= 3.10))
    expected_size = @num_service_columns*@num_uni_columns+2
  elsif ((@oss_document_type == 'Core') && (@oss_document_version.to_f < 3.10))
      expected_size = @num_service_columns*@num_uni_columns+1
  elsif (@oss_document_type == 'LS') or (@oss_document_type == 'Sprint')
    expected_size = @num_service_columns*@num_uni_columns+2
  else
    print_error "#{table_data[0]} - unexpected Version Number #{@oss_document_version}"
  end
    
  if (table_data.size != expected_size)
    print_error "#{table_data[0]} is wrong size [got: #{table_data.size} expect:#{expected_size} Version: #{@oss_document_version}]"
  else
    print_info "Processing #{table_data[0]}: [got: #{table_data.size} expect:#{expected_size}]"
  end

  @es_types.each do |est|
    1.upto(@num_uni_columns) do |i|
      uni_mapping = table_data[((est.number-1)*@num_uni_columns)+i]
      if (is_non_empty_data uni_mapping)
        ut = @uni_types.select{|u| u.letter == uni_mapping}.pop
        if ut
          ut.object.ethernet_service_types.push(est.object)
          #est.object.demarc_types.push(ut.object)
          print_debug "UNI: #{ut.object.name} added to EST: #{est.object.name}"
        else
          print_error "UNI #{uni_mapping} NOT added to EST: #{est.object.name}"
        end
      end
    end
  end
end

def process_uni_attributes table_data
  print_array_data table_data

  last_row_processed = 0
  max_number_of_phy_types = 5
  max_number_of_loopback_types = 6

  if ((@oss_document_type == 'Core') && (@oss_document_version.to_f >= 3.12))
    expected_table_size = @num_uni_columns*(max_number_of_phy_types+2*max_number_of_loopback_types+10)+2
  elsif @oss_document_type == 'Core'
    expected_table_size = @num_uni_columns*(max_number_of_phy_types+max_number_of_loopback_types+8)+2
  # TODO deal with LS versions post addition of turnup reflection
  elsif ((@oss_document_type == 'LS') && (@oss_document_version.to_f > 1.6))
    expected_table_size = @num_uni_columns*(max_number_of_phy_types+max_number_of_loopback_types+16)+2
  elsif ((@oss_document_type == 'LS') && (@oss_document_version.to_f > 1.4))
    expected_table_size = @num_uni_columns*(max_number_of_phy_types+max_number_of_loopback_types+15)+2
  elsif ((@oss_document_type == 'Sprint'))
    expected_table_size = @num_uni_columns*(max_number_of_phy_types+max_number_of_loopback_types+26)+2
  else
    expected_table_size = @num_uni_columns*(max_number_of_phy_types+max_number_of_loopback_types+11)+2
  end

  if table_data.size != expected_table_size
    print_error "Error: Unexpected Table #{table_data[0]} size got: #{table_data.size} expect: #{expected_table_size}"
    return
  end

  #Skip info about uplink paths
  if ((@oss_document_type == 'LS') && (@oss_document_version.to_f > 1.6))
      last_row_processed += @num_uni_columns
  end

  # Device Type
  if (@oss_document_type == 'Sprint')
    @uni_types.each do |ut|
      print_info "Sprint Device Type #{table_data[last_row_processed+ut.number]}"
      rows = []
      rows << TableRow.new("string", "connected_device_type", table_data[last_row_processed+ut.number])
      rows.each do |r| process_row r, ut.object end
    end
    last_row_processed += @num_uni_columns

    # Skip uplink protection
    last_row_processed += @num_uni_columns

  end


  # Phy types
  @uni_types.each do |ut|
    root_index = last_row_processed+(ut.number-1)*max_number_of_phy_types+1
    rows = []
    rows << TableRow.new("string", "physical_medium", table_data[root_index])
    1.upto(max_number_of_phy_types-2) do |i|
      if is_non_empty_data table_data[root_index+i]
        #print_info "**** UNI Append physical_medium #{table_data[root_index+i]}"
        rows << TableRow.new("append_string", "physical_medium", table_data[root_index+i])
      end
    end
    rows.each do |r| process_row r, ut.object end
  end
  last_row_processed += @num_uni_columns*max_number_of_phy_types

  #Auto-negotiate
  @uni_types.each do |ut|
    rows = []
    rows << TableRow.new("string", "auto_negotiate", table_data[last_row_processed+ut.number])
    rows.each do |r| process_row r, ut.object end
  end
  last_row_processed += @num_uni_columns

  #Skip info about NID Hardening and SW upgrades
  if ((@oss_document_type == 'LS') && (@oss_document_version.to_f > 1.4)) or (@oss_document_type == 'Sprint')
      last_row_processed += @num_uni_columns*4
  end

  #MTU
  @uni_types.each do |ut|
    print_info "MTU #{table_data[last_row_processed+ut.number]}"
    rows = []
    rows << TableRow.new("integer", "mtu", table_data[last_row_processed+ut.number])
    rows.each do |r| process_row r, ut.object end
  end
  last_row_processed += @num_uni_columns

  #ah
  if (@oss_document_type == 'LS') or (@oss_document_type == 'Sprint')
    @uni_types.each do |ut|
      rows = []
      rows << TableRow.new("boolean", "ah_supported", table_data[last_row_processed+ut.number])
      rows.each do |r| process_row r, ut.object end
    end
    last_row_processed += @num_uni_columns
  end

  #CFM
  @uni_types.each do |ut|
    rows = []
    rows << TableRow.new("boolean", "cfm_supported", table_data[last_row_processed+ut.number])
    rows.each do |r| process_row r, ut.object end
  end
  last_row_processed += @num_uni_columns

  #Skip CFM MD Levels
  if (@oss_document_type == 'Sprint')
      last_row_processed += @num_uni_columns*10
  end

  #Loopback Mechanism
  @uni_types.each do |ut|
    first_write = true
    root_index = last_row_processed+(ut.number-1)*max_number_of_loopback_types
    rows = []
    string_to_enter = ''
    need_to_write = false

    1.upto(max_number_of_loopback_types) do |i|
      print_info "Processing data: [#{root_index+i}] #{table_data[root_index+i]}"
      need_to_write = table_data[root_index+i].to_i == 1 ? true : false
      case i
        when 1
          string_to_enter = 'DMM/DMR'
        when 2
          string_to_enter = 'LBM/LBR'
        when 3
          string_to_enter = 'Raw-Loopback'
        when 4
          string_to_enter = 'IP-Ping'
          #puts "Reflection Detect: #{string_to_enter} W: #{need_to_write}"
        when 5
          need_to_write = false
        when 6
          if is_non_empty_data table_data[root_index+i]
            string_to_enter = table_data[root_index+i]
            need_to_write = true
          end
      end

      #puts "Reflection Check Write: #{need_to_write}"
      if( need_to_write)
        print_info "Writing Reflection Mechanism: [#{string_to_enter}]"

        if first_write
          rows << TableRow.new("string", "reflection_mechanisms", string_to_enter)
          first_write = false
        else
          rows << TableRow.new("append_string", "reflection_mechanisms", string_to_enter)
        end
      end
    end
    rows.each do |r| process_row r, ut.object end

  end
  last_row_processed += @num_uni_columns*max_number_of_loopback_types

  #Turnup Loopback Mechanism
  if ((@oss_document_type == 'Core') && (@oss_document_version.to_f >= 3.12))
    @uni_types.each do |ut|
      first_write = true
      root_index = last_row_processed+(ut.number-1)*max_number_of_loopback_types
      rows = []
      string_to_enter = ''
      need_to_write = false

      1.upto(max_number_of_loopback_types) do |i|
        print_info "Processing data: [#{root_index+i}] #{table_data[root_index+i]}"
        need_to_write = table_data[root_index+i].to_i == 1 ? true : false
        case i
          when 1
            string_to_enter = 'DMM/DMR'
          when 2
            string_to_enter = 'LBM/LBR'
          when 3
            string_to_enter = 'Raw-Loopback'
          when 4
            string_to_enter = 'IP-Ping'
            puts "Turnup Reflection Detect: #{string_to_enter} W: #{need_to_write}"
          when 5
            need_to_write = false
          when 6
            if is_non_empty_data table_data[root_index+i]
              string_to_enter = table_data[root_index+i]
              need_to_write = true
            end
        end

        print_info "Turnup Reflection Check Write: #{need_to_write}"
        if( need_to_write)
          print_info "Writing Turnup Reflection Mechanism: [#{string_to_enter}]"

          if first_write
            rows << TableRow.new("string", "turnup_reflection_mechanisms", string_to_enter)
            first_write = false
          else
            rows << TableRow.new("append_string", "turnup_reflection_mechanisms", string_to_enter)
          end
        end
      end
      rows.each do |r| process_row r, ut.object end

    end
    last_row_processed += @num_uni_columns*max_number_of_loopback_types
  end

  # Line rate reflection
  if ((@oss_document_type == 'Core') && (@oss_document_version.to_f >= 3.12)) or (@oss_document_type == 'LS') or (@oss_document_type == 'Sprint')
    @uni_types.each do |ut|
      rows = []
      rows << TableRow.new("boolean", "service_rate_reflection_supported", table_data[last_row_processed+ut.number*2-1])
      rows << TableRow.new("string", "max_reflection_rate", table_data[last_row_processed+ut.number*2])
      rows.each do |r| process_row r, ut.object end
    end
    last_row_processed += @num_uni_columns*2
  end

  #Multiplexing
  @uni_types.each do |ut|
    print_info "Multiplexing: #{table_data[last_row_processed+ut.number*2-1]}"
    rows = []
    rows << TableRow.new("boolean", "service_multiplexing", table_data[last_row_processed+ut.number*2-1])
    rows << TableRow.new("string", "max_num_segments", table_data[last_row_processed+ut.number*2])
    rows.each do |r| process_row r, ut.object end
  end
  last_row_processed += @num_uni_columns*2

  #Bundling
  @uni_types.each do |ut|
    print_info "Bundling #{table_data[last_row_processed+ut.number]}"
    rows = []
    rows << TableRow.new("boolean", "bundling", table_data[last_row_processed+ut.number])
    rows.each do |r| process_row r, ut.object end
  end
  last_row_processed += @num_uni_columns

  #ATO Bundling
  @uni_types.each do |ut|
    print_info "Ato1 Bundling #{table_data[last_row_processed+ut.number]}"
    rows = []
    rows << TableRow.new("boolean", "ato_bundling", table_data[last_row_processed+ut.number])
    rows.each do |r| process_row r, ut.object end
  end
  last_row_processed += @num_uni_columns

  #Device Type
  if (@oss_document_type != 'Sprint')
    @uni_types.each do |ut|
      rows = []
      rows << TableRow.new("string", "connected_device_type", table_data[last_row_processed+ut.number])
      rows.each do |r| process_row r, ut.object end
    end
    last_row_processed += @num_uni_columns
  end

  @uni_types.each do |ut|
    print_info "Remarks #{table_data[last_row_processed+1]}"
    rows = []
    rows << TableRow.new("string", "remarks", table_data[last_row_processed+1])
    rows.each do |r| process_row r, ut.object end
  end
  
  last_row_processed += 1

  # Sanity check
  if (last_row_processed != table_data.size-1)
    print_error "UNI Attributes not properly Processed [#{last_row_processed}/#{table_data.size-1}]"
  else
    print_info "UNI Attributes Processed OK - [#{last_row_processed}/#{table_data.size-1}]"
  end


  # Save UNI Types into the DB
  @uni_types.each do |ut|
    ut.object.ls_access_solution_model = @ls_solution_model
    save_to_database "Uni Type", ut.object
  end
  
end

