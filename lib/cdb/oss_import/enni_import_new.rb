require File.dirname(__FILE__) + "/common_import_new"

def process_enni_data section_data

  @raw_item_count += section_data.size

  if (!@et_id)
    print_info "Create new ENNI Type"
    et = EnniType.new
    et.operator_network_type_id = @ont.id
    et.demarc_type_type = 'ENNI Type'

    if @oss_document_type == 'Core'
      et.name = "ENNI"
    else
      et.name = "ENNI"
      #et.name = "ENNI (for Light Squared)"
    end

    et.ls_access_solution_model = @ls_solution_model
  else
    print_info "Use existing ENNI Type"
    et = DemarcType.find(@et_id)
  end

  #Control flags
  current_table_pointer = 0
  next_table_pointer = 0
  table_data = []


  result_ok, next_table_pointer = find_next_table section_data, 1

  while result_ok
    current_table_pointer = next_table_pointer
    result_ok, next_table_pointer = find_next_table section_data, current_table_pointer+1
    if result_ok
      print_info "\n<------------------------------------>"
      #print_info "==== Next Table [#{current_table_pointer}..#{next_table_pointer-1}]"
      table_data = section_data[current_table_pointer..next_table_pointer-1]
      result_ok = process_enni_table_data table_data, et
    end
  end


  @et_id = save_to_database "ENNI Type", et
  @db_row_count += 1

end


def process_enni_table_data table_data, et
  result_ok = true
  @table_count += 1
  @item_count += table_data.size

  table_title = table_data[0]
  print_info "Process Title: #{table_title}; Size: #{table_data.size} ..."

  case table_title
    when "<Physical Layer>"
      process_enni_physical_layer table_data, et
    when "<Frame Format>",'<ENNI Frame Format>'
      process_enni_frame_format table_data, et
    when "<Number of Links / Protection>"
      process_enni_protection table_data, et
    when "<Maximum Number of OVCs>", '<Maximum Number of Paths/OVCs>'
      process_enni_max_segments table_data, et
    when "<ENNI OAM Characteristics>", '<Aggregation Interface OAM Characteristics>'
      process_enni_oam_characteristics table_data, et
    when "<ENNI/UNI L2CP Handling Un-tagged>"
      process_enni_l2cp_handling table_data, et, 'untagged'
    when "<ENNI/UNI L2CP Handling Tagged>"
      process_enni_l2cp_handling table_data, et, 'tagged'
    when "<Connected Device Type>"
      process_enni_connected_device table_data, et
    when '<Aggregation Site Locations>', '<Market 1>', '<Market 2>', '<Market 3>', '<Maximum Number of EVCs>'
      skip_over_table table_data
    else
      table_processing_error table_title, table_data.size
      result_ok = false
  end

  return result_ok
end

def process_enni_physical_layer table_data, et

  print_array_data table_data

  #Sanity check
  expected_size = 8
  if( table_data.size != expected_size)
    print_error "Table: #{table_data[0]} is wrong size: #{table_data.size} expected #{expected_size}"
    return
  end

  rows = [
    TableRow.new("table_title", "xxx", table_data[0]),
    TableRow.new("string", 'physical_medium', table_data[1]),
    TableRow.new("append_new_line", 'physical_medium', table_data[2]),
    TableRow.new("append_new_line", 'physical_medium', table_data[3]),
    TableRow.new("append_new_line", 'physical_medium', table_data[4]),
    TableRow.new("string", 'physical_medium_notes', table_data[5]),
    TableRow.new("string", 'auto_negotiate', table_data[6]),
    TableRow.new("string", 'physical_medium_notes', table_data[7])
    ]

   rows.each do |r| process_row r, et end

end

def process_enni_frame_format table_data, et

  print_array_data table_data

  #Sanity check
  expected_size = 6
  if( table_data.size != expected_size)
    print_error "Table: #{table_data[0]} is wrong size: #{table_data.size} expected #{expected_size}"
    return
  end

  rows = [
    TableRow.new("table_title", "xxx", table_data[0]),
    TableRow.new("string", 'frame_format', table_data[1]),
    TableRow.new("string", 'frame_format_notes', table_data[2]),
    TableRow.new("boolean", 'consistent_ethertype', table_data[3]),
    TableRow.new("string", 'outer_tag_segment_mapping', table_data[4]),
    TableRow.new("append_new_line", 'frame_format_notes', table_data[5])
    ]

  rows.each do |r| process_row r, et end

end

def process_enni_protection table_data, et
  print_array_data table_data

  #Sanity check
  if (@oss_document_type == 'Core' or @oss_document_type == 'Sprint')
    expected_size = 9
  else
    expected_size = 8
  end
  
  if( table_data.size != expected_size)
    print_error "Table: #{table_data[0]} is wrong size: #{table_data.size} expected #{expected_size}"
    return
  end

  rows = [
    TableRow.new("table_title", "xxx", table_data[0]),
    TableRow.new("boolean", 'lag_supported', table_data[1]),
    TableRow.new("string", 'lag_type', table_data[2]),
    TableRow.new("boolean", 'multi_link_support', table_data[3]),
    TableRow.new("string", 'max_links_supported', table_data[4]),
    TableRow.new("boolean", 'lacp_supported', table_data[5]),
    TableRow.new("boolean", 'lacp_priority_support', table_data[6]),
  ]

  if @oss_document_type == 'Core'
    rows << TableRow.new("string", 'lag_control', table_data[7])
    rows << TableRow.new("string", 'protection_notes', table_data[8])
  else
    rows << TableRow.new("string", 'lag_control', 'N/A')
    rows << TableRow.new("string", 'protection_notes', table_data[7])
  end

  rows.each do |r| process_row r, et end

end


def process_enni_max_segments table_data, et
  print_array_data table_data

  #Sanity check
  if @oss_document_type == 'Core'
    expected_size = 3
  else
    expected_size = 4
  end
  
  if( table_data.size != expected_size)
    print_error "Table: #{table_data[0]} is wrong size: #{table_data.size} expected #{expected_size}"
    return
  end

  rows = [
      TableRow.new("table_title", "xxx", table_data[0]),
      TableRow.new("string", 'max_num_segments', table_data[1]),
      TableRow.new("string", 'max_num_segments_notes', table_data[2])
    ]
    
  if @oss_document_type == 'LS'
    rows << TableRow.new("string", 'max_num_segments_notes', "1G Max.: #{table_data[1]}")
    rows << TableRow.new("append_new_line", 'max_num_segments_notes', "10G Max.: #{table_data[2]}")
    rows << TableRow.new("append_new_line", 'max_num_segments_notes', table_data[3])
  end


  rows.each do |r| process_row r, et end

end

def process_enni_oam_characteristics table_data, et
  print_array_data table_data

  #Sanity check
  if @oss_document_type == 'Core'
    expected_size = 4
  else
    expected_size = 3
  end
  
  if( table_data.size != expected_size)
    print_error "Table: #{table_data[0]} is wrong size: #{table_data.size} expected #{expected_size}"
    return
  end
  
  if @oss_document_type == 'Core'
    rows = [
      TableRow.new("table_title", "xxx", table_data[0]),
      TableRow.new("boolean", 'ah_supported', table_data[1]),
      TableRow.new("boolean", 'ag_supported', table_data[2]),
      TableRow.new("string", 'oam_notes', table_data[3])
    ]
  else
    rows = [
      TableRow.new("table_title", "xxx", table_data[0]),
      TableRow.new("boolean", 'ah_supported', table_data[1]),
      TableRow.new("string", 'oam_notes', table_data[2])
    ]
  end

  rows.each do |r| process_row r, et end
end

def process_enni_l2cp_handling table_data, et, suffix
  print_array_data table_data

  #Sanity check
  expected_size = 11
  if( table_data.size != expected_size)
    print_error "Table: #{table_data[0]} is wrong size: #{table_data.size} expected #{expected_size}"
    return
  end

  rows = [
    TableRow.new("table_title", "xxx", table_data[0]),
    TableRow.new("string", "stp_#{suffix}", table_data[1]),
    TableRow.new("string", "rstp_#{suffix}", table_data[1]),
    TableRow.new("string", "mstp_#{suffix}", table_data[1]),
    TableRow.new("string", "pause_#{suffix}", table_data[2]),
    TableRow.new("string", "lacp_#{suffix}", table_data[3]),
    TableRow.new("string", "lamp_#{suffix}", table_data[3]),
    TableRow.new("string", "link_oam_#{suffix}", table_data[4]),
    TableRow.new("string", "port_auth_#{suffix}", table_data[5]),
    TableRow.new("string", "lldp_#{suffix}", table_data[6]),
    TableRow.new("string", "garp_#{suffix}", table_data[7]),
    TableRow.new("string", "mrp_b_#{suffix}", table_data[7]),
    TableRow.new("string", "cisco_bpdu_#{suffix}", table_data[8]),
    TableRow.new("string", "default_l2cp_#{suffix}", table_data[9]),

    TableRow.new("string", "lacp_notes_#{suffix}", table_data[10])

  ]

  rows.each do |r| process_row r, et end
end

def process_enni_connected_device table_data, et
  print_array_data table_data

  #Sanity check
  if ((@oss_document_type == 'Core') && (@oss_document_version.to_f < 3.12))
    expected_size = 3
  elsif  ((@oss_document_type == 'LS') && (@oss_document_version.to_f > 1.6)) or ((@oss_document_type == 'Sprint') && (@oss_document_version.to_f < 1.4))
    expected_size = 11
  elsif  ((@oss_document_type == 'LS') && (@oss_document_version.to_f > 1.4))
    expected_size = 10
  elsif ((@oss_document_type == 'Sprint') && (@oss_document_version.to_f > 1.3))
    expected_size = 9
  else
    expected_size = 9
  end

  if( table_data.size != expected_size)
    print_error "Table: #{table_data[0]} is wrong size: #{table_data.size} expected #{expected_size}"
    return
  end

  if (expected_size == 3)
    rows = [
    TableRow.new("table_title", "xxx", table_data[0]),
    TableRow.new("string", 'connected_device_type', table_data[1]),
    TableRow.new("string", 'remarks', table_data[2])
    ]
  elsif (expected_size == 11)
    rows = [
    TableRow.new("table_title", "xxx", table_data[0]),
    TableRow.new("string", 'connected_device_type', table_data[1]),
    TableRow.new("string", 'remarks', table_data[10])
    ]
  elsif  (expected_size == 10)
    rows = [
    TableRow.new("table_title", "xxx", table_data[0]),
    TableRow.new("string", 'connected_device_type', table_data[1]),
    TableRow.new("string", 'remarks', table_data[9])
    ]
  else
    rows = [
    TableRow.new("table_title", "xxx", table_data[0]),
    TableRow.new("string", 'connected_device_type', table_data[1]),
    TableRow.new("string", 'remarks', table_data[8])
    ]
  end

  rows.each do |r| process_row r, et end

end

