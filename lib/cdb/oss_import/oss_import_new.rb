# encoding: UTF-8

is_test = $0.match(/^(.*\/)?spec$/) || $0.match(/^(.*\/)?rcov$/) || (Object.const_defined?("Rails") && Rails.env.test?)
if !is_test
  filename = File.dirname(__FILE__) + "/../../config/environment.rb"
  unless File.exists?(filename) #For SIN
    filename = File.dirname(__FILE__) + "/../../../config/environment.rb"
  end
  require filename
#  require "rubygems"
#  require "thread"
#  require "active_record"
#  require 'active_record/base'
#  require 'optparse'
#  require 'optparse/time'
#  require 'ostruct'
#  require 'pp'
#
#  require File.dirname(__FILE__) + "/../../lib/name_tools/cenx_name_generator.rb"
#  require File.dirname(__FILE__) + "/../../lib/validations.rb"
#  require File.dirname(__FILE__) + "/../../lib/constants/common_types.rb"
#  require File.dirname(__FILE__) + "/../../app/models/instance_element"
#  require File.dirname(__FILE__) + "/../../app/models/member_attr_instance"
#  require File.dirname(__FILE__) + "/../../app/models/member_attr"
#  require File.dirname(__FILE__) + "/../../app/models/type_element"
#  require File.dirname(__FILE__) + "/../../app/models/service_provider"
#  require File.dirname(__FILE__) + "/../../app/models/operator_network"
#  require File.dirname(__FILE__) + "/../../app/models/operator_network_type"
#  require File.dirname(__FILE__) + "/../../app/models/oss_info"
#  require File.dirname(__FILE__) + "/../../app/models/segment_type"
#  require File.dirname(__FILE__) + "/../../app/models/alarm_severity"
#  require File.dirname(__FILE__) + "/../../app/models/eventable"
#  require File.dirname(__FILE__) + "/../../app/models/admin/stateable"
#  require File.dirname(__FILE__) + "/../../app/models/segment"
#  require File.dirname(__FILE__) + "/../../app/models/off_net_ovc"
#  require File.dirname(__FILE__) + "/../../app/models/demarc_type"
#  require File.dirname(__FILE__) + "/../../app/models/uni_type"
#  require File.dirname(__FILE__) + "/../../app/models/enni_type"
#  require File.dirname(__FILE__) + "/../../app/models/demarc"
#  require File.dirname(__FILE__) + "/../../app/models/enni_new"
#  require File.dirname(__FILE__) + "/../../app/models/type_element"
#  require File.dirname(__FILE__) + "/../../app/models/segment_end_point"
#  require File.dirname(__FILE__) + "/../../app/models/ovc_end_point_uni"
#  require File.dirname(__FILE__) + "/../../app/models/ovc_end_point_enni"
#  require File.dirname(__FILE__) + "/../../app/models/segment_end_point_type"
#  require File.dirname(__FILE__) + "/../../app/models/ethernet_service_type"
#  require File.dirname(__FILE__) + "/../../app/models/path_type"
#  require File.dirname(__FILE__) + "/../../app/models/cos_instance"
#  require File.dirname(__FILE__) + "/../../lib/bx_stats_if/bx_api"
#  require File.dirname(__FILE__) + "/../../lib/tools/sw_err"
#  require File.dirname(__FILE__) + "/../../app/models/cos_test_vector"
#  require File.dirname(__FILE__) + "/../../app/models/class_of_service_type"
#  require File.dirname(__FILE__) + "/../../app/models/service_level_guarantee_type"
#  require File.dirname(__FILE__) + "/../../app/models/bw_profile_type"
end
require File.dirname(__FILE__) + "/header_import_new"
require File.dirname(__FILE__) + "/enni_import_new"
require File.dirname(__FILE__) + "/service_import_new"
require File.dirname(__FILE__) + "/lh_data_import"

def process_section_data section_data
  section_title = section_data[0]
  result_ok = true
  @section_count += 1

  print_info "Processing Section #{section_title}; Size: #{section_data.size}..."

  case section_title
    when "[Completed by/for]" , "[Access Provider Name]", "[LH Provider Name]"
      process_header_data section_data
      print_info "OSS Document Type: #{@oss_document_type} Version: #{@oss_document_version}"
    when "[ENNI Service Attributes]" , '[Aggregation Interface Service Attributes]'
      process_enni_data section_data
    when "[Network Service Attributes]"
      process_service_data section_data
    when "[Post Member Test ENNI Data]"
      process_enni_data section_data
    when "[LH Provider Attributes]"
      process_lh_data section_data
    else
      print_error "Section #{section_title} not recognized..."
      result_ok = false
      @section_count -= 1
  end

  return result_ok
  #process_any_section

end

def parse_options args
  opts = OptionParser.new do |opts|
    opts.banner = "Usage: #{File.basename(__FILE__)} [options]"

    opts.separator ""
    opts.separator "Specific options:"

    # Mandatory argument.
    opts.on("-f", "--file FILE",
            "Need FILE to parse") do |file|
      @options.file_to_parse << file
    end

    # Optional argument; Member Name
    opts.on("-m", "--member [MEMBER_NAME]",
            "Name of Member (Service Provider) OSS applies to") do |m_name|
      @options.member_name = m_name
    end

    opts.on("-n", "--network [NETWORK_NAME]",
            "Type of Network OSS applies to") do |n_name|
      @options.network_name = n_name
    end

    # Optional argument with keyword completion.
    opts.on("--db [DB]", ["production", "development", "test"],
            "Select Data Base (production, development, test)") do |db|
      @options.db_type = db
    end

    # Boolean switch.
    opts.on("-v", "--[no-]verbose", "Run verbosely") do |v|
      @options.verbose = v
    end

    opts.on("-c", "--[no-]clean", "Cleanup before Running") do |c|
      @options.clean = c
    end

#    # Optional argument with keyword completion.
#    opts.on("--doc_type [type]", ["default", "light_squared"],
#            "Select Data Base (default, light_squared)") do |dt|
#      @options.doc_type = dt
#    end

    opts.separator ""
    opts.separator "Common options:"

    # No argument, shows at tail.  This will print an options summary.
    # Try it and see!
    opts.on_tail("-h", "--help", "Show this message") do
      puts opts
      pp @options
      exit
    end
  end

  opts.parse!(args)
  
end

def cleanup_ont

  count = 0

  @ont.oss_infos.each do |i|
    i.destroy
    count += 1
  end
  
  @ont.ethernet_service_types.each do |i|
    i.delete
    count += 1
  end

  @ont.segment_types.each do |i|
    i.destroy
    count += 1
  end
  
  @ont.class_of_service_types.each do |i|
    i.destroy
    count += 1
  end
  
  @ont.path_types.each do |i|
    i.destroy
    count += 1
  end
  
  @ont.demarc_types.each do |i|
    i.destroy
    count += 1
  end

  @ont.uni_types.each do |i|
    i.destroy
    count += 1
  end

  @ont.enni_types.each do |i|
    i.destroy
    count += 1
  end  
 
  @ont.segment_end_point_types.each do |i|
    i.delete
    count += 1
  end

  puts "#{count} Items Deleted"
  exit
end

def get_member_name
  sps = ServiceProvider.find_rows_for_index
  got_selection = false
  i = 0

  puts "\nWhich Member does the Offered Services Study apply to ?"
  while !got_selection
    i = 0
    sps.each do |sp|
      puts "[#{i}] #{sp.name}"
      i += 1
    end
    puts "Choose [0 - #{i-1}]:"

    choice = gets.chomp
    if choice.to_i < sps.size
      got_selection = true
    else
       print_error "Invalid Choice"
    end
  end

  @options.member_name = sps[choice.to_i].name
  puts "Member is: #{@options.member_name}"

end

def get_network_name
  onts = @sp.operator_network_types
  got_selection = false
  i = 0

  puts "\nWhich Network Type owned by #{@sp.name} does the Offered Services Study apply to ?"
  while !got_selection
    i = 0
    onts.each do |on|
      puts "[#{i}] #{on.name}"
      i += 1
    end
    puts "Choose [0 - #{i-1}]:"

    choice = gets.chomp
    if choice.to_i < onts.size
      got_selection = true
    else
       print_error "Invalid Choice"
    end
  end

  @options.network_name = onts[choice.to_i].name
  puts "Network Type is: #{@options.network_name}\n\n"

end

#------------------------ Main -----------------------------#
#

# Parse command line options
@options = OpenStruct.new
@options.file_to_parse = ''
@options.member_name = ''
@options.network_name = ''
@options.db_type = 'development'
@options.verbose = false
@options.clean = false

# Versioning and Light Squared Vs Not
@oss_document_version = ''
@oss_document_type = 'unknown'

#Specific to light Squared
@ls_solution_model = ''

# ENNI Types
@et_id = nil

# Service Types
@table_processing_mode = ''
@rate_limiting_performed = ''
@rate_limiting_mechanism = ''
@rate_limiting_layer = ''
@rate_limiting_notes = ''
@oss_document_version = ''

@segment_types = []
@es_types = []
@cos_types = []
@uni_types = []

parse_options !is_test ? ARGV : $args
          
puts "\nOptions Used:\n #{@options}\n"

# Debug stats
@section_count = 0
@table_count = 0
@item_count = 0
@raw_item_count = 0
@db_row_count = 0
@db_save_count = 0
@db_save_attempt = 0
@db_save_failures = ""
@warning_count = 0
@all_warnings = ""
@error_count = 0
@all_errors = ""

section_data = []
current_section_pointer = 0
next_section_pointer = 0

result_ok = false

if !is_test
  #Establish connection to database
  if RUBY_PLATFORM =~ /java/
    dbconfig = YAML::load(File.open(Rails.root.join('config', 'databasej.yml')))
  else
    dbconfig = YAML::load(File.open(Rails.root.join('config', 'database.yml')))
  end

  #dbconfig = YAML::load(File.open(File.dirname(__FILE__) + '/../../config/database.yml'))
  ActiveRecord::Base.establish_connection(dbconfig[@options.db_type])
end

if @options.member_name == ""
  get_member_name
end

@sp = ServiceProvider.find_by_name("#{@options.member_name}")
unless @sp
  print_error"Failed to find Member: #{@options.member_name}"
  exit(1)
end

if @options.network_name == ""
  get_network_name
end

@ont = OperatorNetworkType.find_by_name_and_service_provider_id(@options.network_name, @sp.id)
unless @ont
  print_error "Failed to find Network Type: #{@options.network_name}"
  exit(1)
end

if @options.clean == true
  puts "Cleaning all data from #{@ont.name} Network Type - continue?"
  choice = gets.chomp

  if( !str_to_bool choice)
    puts "Aborting...."
    exit(1)
  end

  cleanup_ont
end

if @options.file_to_parse == ""
  print_error "Please specify filename to process"
  exit(1)
end

file = File.new(@options.file_to_parse, 'r')
file_contents = file.gets

#Read the comma separated values as a Ruby Array
@items = eval('[' + file_contents + ']')


puts "\nFile: #{@options.file_to_parse} has #{@items.size} pieces of data to extract..."

if @options.db_type != "development" && !is_test
  puts "This will affect the #{@options.db_type} database - are you sure?"
  choice = gets.chomp

  if( !str_to_bool choice)
    puts "Aborting...."
    exit(1)
  end
 
end

@oss = nil

result_ok, next_section_pointer = find_next_section @items, 0

while result_ok 
  current_section_pointer = next_section_pointer
  result_ok, next_section_pointer = find_next_section @items, current_section_pointer+1
  if result_ok
    print_info "\n[====================================]"
    print_debug "==== Next Section Data [#{current_section_pointer}..#{next_section_pointer-1}]"
    section_data = @items[current_section_pointer..next_section_pointer-1]
    result_ok = process_section_data section_data
  end
end

puts "\n-----------------------------"
puts "Parse Summary:"
puts "-----------------------------"
puts "OSS Document Version: #{@oss_document_version}"
puts "Sections Processed: #{@section_count}"
puts "Tables Processed: #{@table_count}"
puts "Items Processed: #{@item_count}/#{@raw_item_count}"
puts "Total Items: #{@items.size}"
puts "DB Rows added: #{@db_row_count}"
puts "DB Saves: #{@db_save_count}/#{@db_save_attempt}"
puts "DB Save Failures: #{@db_save_failures}"
puts "Warnings: #{@warning_count}"
if ( @warning_count > 0 ) 
  puts "#{@all_warnings}"
end
puts "Errors: #{@error_count}"
if ( @error_count > 0 ) 
  puts "#{@all_errors}"
end
puts "............................."
puts "Options Used:\n #{@options}\n"
puts "-----------------------------"


