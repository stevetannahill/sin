module CoreSite_900NAlameda


  #The following is default site seeding info

  #Nodes Specific ONLY to this Site
  # Node Structure: (NOTE: Key may not be the same as node type
  # so don't use it as a shortcut, just most of the time it will be the same
  # the node's :type)
  # "" => {
  #    :type => "",
  #    :vendor => "",
  #    :make_model => "",
  #    :description => "",
  #    :nm_connection => {:name => "", nm_type => ""},
  #    :ports => [
  #      {:name => "", :phy_type => ""},
  #    ]
  # },
  NODE = {
    "N_MLXe16" => {
      :type => "Brocade MLXe16",
      :vendor => "Brocade",
      :make_model => "Brocade MLXe16",
      :description => "Brocade MLXe16",
      :nm_connection => {:name => "CoreSite SolarWinds", :nm_type => "SolarWinds"},
      :ports => [
        {:name => "1/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/8", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/3", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "15/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "15/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "15/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "15/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "15/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "15/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "15/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "15/8", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "16/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "16/3", :phy_type => "10GigE LR; 1310nm; SMF"},
      ]
    },
  }

  #This submodule is a specific site for this site
  # EACH Site MUST have a NODE_INSTANCE hash of nodes and
  # a PORT_CONNECTIONS array of internal connections
  module Primary
    NODE_INSTANCE = {
      "N_MLXe16_1" => {
        :node => CoreSite_900NAlameda::NODE["N_MLXe16"], :id => 1,
        :identifier => ".IDCLL.MMR.MLX16.", :id_length => 2,
        :ip_suffix => { :primary => "20", :secondary => nil, :gateway => "1", :loopback => "51" },
        :enni_connections => []
       },
       "N_MLXe16_2" => {
        :node => CoreSite_900NAlameda::NODE["N_MLXe16"], :id => 2,
        :identifier => ".IDCLL.MMR.MLX16.", :id_length => 2,
        :ip_suffix => { :primary => "21", :secondary => nil, :gateway => "1", :loopback => "52" },
        :enni_connections => []
       },
       #IDCLL.CL2.IDF2
       "N_CES_2048_IDCLL_CL2_IDF2_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".IDCLL.CL2.IDF2.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "32", :secondary => nil, :gateway => "1", :loopback => "53" },
        :enni_connections => []
       },
       "N_CES_2048_IDCLL_CL2_IDF2_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".IDCLL.CL2.IDF2.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "33", :secondary => nil, :gateway => "1", :loopback => "54" },
        :enni_connections => []
       },
       #IDCLL.MMR
       "N_CES_2048_IDCLL_MMR_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".IDCLL.MMR.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "30", :secondary => nil, :gateway => "1", :loopback => "55" },
        :enni_connections => []
       },
       "N_CES_2048_IDCLL_MMR_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".IDCLL.MMR.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "31", :secondary => nil, :gateway => "1", :loopback => "56" },
        :enni_connections => []
       },
       #IDCLL.85
       "N_CES_2048_IDCLL_85_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".IDCLL.85.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "34", :secondary => nil, :gateway => "1", :loopback => "57" },
        :enni_connections => []
       },
       "N_CES_2048_IDCLL_85_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".IDCLL.85.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "35", :secondary => nil, :gateway => "1", :loopback => "58" },
        :enni_connections => []
       },
       #IDC201
       "N_CES_2048_IDC201_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".IDC201.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "36", :secondary => nil, :gateway => "1", :loopback => "59" },
        :enni_connections => []
       },
       "N_CES_2048_IDC201_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".IDC201.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "37", :secondary => nil, :gateway => "1", :loopback => "60" },
        :enni_connections => []
       },
       #FLR270
       "N_CES_2048_FLR270_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".FLR270.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "38", :secondary => nil, :gateway => "1", :loopback => "61" },
        :enni_connections => []
       },
       "N_CES_2048_FLR270_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".FLR270.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "39", :secondary => nil, :gateway => "1", :loopback => "62" },
        :enni_connections => []
       },
       #IDC01.113
       "N_CES_2048_IDC01_113_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".IDC01.113.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "60", :secondary => nil, :gateway => "1", :loopback => "63" },
        :enni_connections => []
       },
       "N_CES_2048_IDC01_113_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".IDC01.113.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "61", :secondary => nil, :gateway => "1", :loopback => "64" },
        :enni_connections => []
       },
       #IDC185
       "N_CES_2024_IDC185_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 1,
        :identifier => ".IDC185.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "62", :secondary => nil, :gateway => "1", :loopback => "65" },
        :enni_connections => []
       },
       "N_CES_2024_IDC185_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 2,
        :identifier => ".IDC185.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "63", :secondary => nil, :gateway => "1", :loopback => "66" },
        :enni_connections => []
       },
       #IDC285
       "N_CES_2024_IDC285_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 1,
        :identifier => ".IDC285.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "64", :secondary => nil, :gateway => "1", :loopback => "67" },
        :enni_connections => []
       },
       "N_CES_2024_IDC285_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 2,
        :identifier => ".IDC285.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "65", :secondary => nil, :gateway => "1", :loopback => "68" },
        :enni_connections => []
       },
    }

    # [ [ [node_type, port], [node_type, port] ], [ [node_type, port], [node_type, port] ], ... ]
    # | | | Single Port   |  | Conected Port | |                                                |
    # | |Single connection (only need one way) |                                                |
    # | List of all connections                                                                 |

    PORT_CONNECTIONS = [
      #IDCLL_CL2_IDF2
      [[NODE_INSTANCE["N_MLXe16_1"], "1/1"], [NODE_INSTANCE["N_CES_2048_IDCLL_CL2_IDF2_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "1/2"], [NODE_INSTANCE["N_CES_2048_IDCLL_CL2_IDF2_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "1/1"], [NODE_INSTANCE["N_CES_2048_IDCLL_CL2_IDF2_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "1/2"], [NODE_INSTANCE["N_CES_2048_IDCLL_CL2_IDF2_2"], "1/2"]],
      #IDCLL_MMR
      [[NODE_INSTANCE["N_MLXe16_1"], "1/3"], [NODE_INSTANCE["N_CES_2048_IDCLL_MMR_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "1/4"], [NODE_INSTANCE["N_CES_2048_IDCLL_MMR_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "1/3"], [NODE_INSTANCE["N_CES_2048_IDCLL_MMR_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "1/4"], [NODE_INSTANCE["N_CES_2048_IDCLL_MMR_2"], "1/2"]],
      #IDCLL_85
      [[NODE_INSTANCE["N_MLXe16_1"], "1/5"], [NODE_INSTANCE["N_CES_2048_IDCLL_85_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "1/6"], [NODE_INSTANCE["N_CES_2048_IDCLL_85_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "1/5"], [NODE_INSTANCE["N_CES_2048_IDCLL_85_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "1/6"], [NODE_INSTANCE["N_CES_2048_IDCLL_85_2"], "1/2"]],
      #IDC201
      [[NODE_INSTANCE["N_MLXe16_1"], "1/7"], [NODE_INSTANCE["N_CES_2048_IDC201_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "1/8"], [NODE_INSTANCE["N_CES_2048_IDC201_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "1/7"], [NODE_INSTANCE["N_CES_2048_IDC201_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "1/8"], [NODE_INSTANCE["N_CES_2048_IDC201_2"], "1/2"]],
      #FLR270
      [[NODE_INSTANCE["N_MLXe16_1"], "15/1"], [NODE_INSTANCE["N_CES_2048_FLR270_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "15/2"], [NODE_INSTANCE["N_CES_2048_FLR270_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "15/1"], [NODE_INSTANCE["N_CES_2048_FLR270_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "15/2"], [NODE_INSTANCE["N_CES_2048_FLR270_2"], "1/2"]],
      #IDC01_113
      [[NODE_INSTANCE["N_MLXe16_1"], "15/3"], [NODE_INSTANCE["N_CES_2048_IDC01_113_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "15/4"], [NODE_INSTANCE["N_CES_2048_IDC01_113_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "15/3"], [NODE_INSTANCE["N_CES_2048_IDC01_113_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "15/4"], [NODE_INSTANCE["N_CES_2048_IDC01_113_2"], "1/2"]],
      #IDC185
      [[NODE_INSTANCE["N_MLXe16_1"], "15/5"], [NODE_INSTANCE["N_CES_2024_IDC185_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "15/6"], [NODE_INSTANCE["N_CES_2024_IDC185_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "15/5"], [NODE_INSTANCE["N_CES_2024_IDC185_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "15/6"], [NODE_INSTANCE["N_CES_2024_IDC185_2"], "1/2"]],
      #IDC285
      [[NODE_INSTANCE["N_MLXe16_1"], "15/7"], [NODE_INSTANCE["N_CES_2024_IDC285_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "15/8"], [NODE_INSTANCE["N_CES_2024_IDC285_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "15/7"], [NODE_INSTANCE["N_CES_2024_IDC285_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "15/8"], [NODE_INSTANCE["N_CES_2024_IDC285_2"], "1/2"]],
      #Interconnect
      [[NODE_INSTANCE["N_MLXe16_1"], "2/1"], [NODE_INSTANCE["N_MLXe16_2"], "2/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "16/1"], [NODE_INSTANCE["N_MLXe16_2"], "16/1"]],
    ]
  end

  #Hash of all sites (a.k.a. submodules) for this Site type.
  SITES = {
    "Primary" => Primary
  }

  #Inter Site Connections
  # [ [ [ Site String, Node Instance Name, Port ], [ Site String, Node Instance Name, Port ], ... ]
  # | | |    Single Port                            |  |         Connected Port                    |      |
  # | |         Single Connection (Only need one way)                                              |      |
  # |    List of all connection between all sites                                                     |
  #
  CONNECTIONS = [
  ]

end
