module CoreSite_Cenx_Playground

  #The following is default site seeding info

  #Nodes Specific ONLY to this Site
  # Node Structure: (NOTE: Key may not be the same as node type
  # so don't use it as a shortcut, just most of the time it will be the same
  # the node's :type)
  # "" => {
  #    :type => "",
  #    :vendor => "",
  #    :make_model => "",
  #    :description => "",
  #    :nm_connection => {:name => "", nm_type => ""},
  #    :ports => [
  #      {:name => "", :phy_type => ""},
  #    ]
  # },
  NODE = {
    "N_MLXe16" => {
      :type => "Brocade MLXe16",
      :vendor => "Brocade",
      :make_model => "Brocade MLXe16",
      :description => "Brocade MLXe16",
      :nm_connection => {:name => "CoreSite SolarWinds", :nm_type => "SolarWinds"},
      :ports => [
        {:name => "1/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "15/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "16/1", :phy_type => "10GigE LR; 1310nm; SMF"},
      ]
    },
  }

  #This submodule is a specific site for this site
  # EACH Site MUST have a NODE_INSTANCE hash of nodes and
  # a PORT_CONNECTIONS array of internal connections
  module Primary
    NODE_INSTANCE = {
      "N_MLXe16_1" => {
        :node => CoreSite_Cenx_Playground::NODE["N_MLXe16"], :id => 1,
        :identifier => ".IDC185.LAB.MLX16.", :id_length => 2,
        :ip_suffix => { :primary => "170", :secondary => nil, :gateway => "1" },
        :enni_connections => []
       },
       "N_MLXe16_2" => {
        :node => CoreSite_Cenx_Playground::NODE["N_MLXe16"], :id => 2,
        :identifier => ".IDC185.LAB.MLX16.", :id_length => 2,
        :ip_suffix => { :primary => "171", :secondary => nil, :gateway => "1" },
        :enni_connections => []
       },
       "N_CES_2024_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 1,
        :identifier => ".IDC185.LAB.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "172", :secondary => nil, :gateway => "1" },
        :enni_connections => []
       },
       "N_CES_2024_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 2,
        :identifier => ".IDC185.LAB.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "173", :secondary => nil, :gateway => "1" },
        :enni_connections => []
       },
    }

    # [ [ [node_type, port], [node_type, port] ], [ [node_type, port], [node_type, port] ], ... ]
    # | | | Single Port   |  | Conected Port | |                                                |
    # | |Single connection (only need one way) |                                                |
    # | List of all connections                                                                 |

    PORT_CONNECTIONS = [
      [[NODE_INSTANCE["N_MLXe16_1"], "1/1"], [NODE_INSTANCE["N_CES_2024_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "15/1"], [NODE_INSTANCE["N_CES_2024_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "1/1"], [NODE_INSTANCE["N_CES_2024_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "15/1"], [NODE_INSTANCE["N_CES_2024_2"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "2/1"], [NODE_INSTANCE["N_MLXe16_2"], "2/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "16/1"], [NODE_INSTANCE["N_MLXe16_2"], "16/1"]],
    ]
  end

  #Hash of all sites (a.k.a. submodules) for this Site type.
  SITES = {
    "Primary" => Primary
  }

  #Inter Site Connections
  # [ [ [ Site String, Node Instance Name, Port ], [ Site String, Node Instance Name, Port ], ... ]
  # | | |    Single Port                            |  |         Connected Port                    |      |
  # | |         Single Connection (Only need one way)                                              |      |
  # |    List of all connection between all sites                                                     |
  #
  CONNECTIONS = [
  ]

end
