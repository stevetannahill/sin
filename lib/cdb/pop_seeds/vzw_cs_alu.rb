module VzW_CS_ALU

  #The following is default site seeding info

  #Nodes Specific ONLY to this Site
  # Node Structure: (NOTE: Key may not be the same as node type
  # so don't use it as a shortcut, just most of the time it will be the same
  # the node's :type)
  # "" => {
  #    :type => "",
  #    :vendor => "",
  #    :make_model => "",
  #    :description => "",
  #    :nm_connection => {:name => "", nm_type => ""},
  #    :ports => [
  #      {:name => "", :phy_type => ""},
  #    ]
  # },
  NODE = {
    "N_7705 SAR-8" => {
      :type => "7705 SAR-8",
      :vendor => "Alcatel",
      :make_model => "7705 SAR-8 - <Model>",
      :description => "<Description>",
      :nm_connection => {:name => "Verizon SAM #1", :nm_type => "5620Sam"},
      :ports => [
      	{:name => "1/5/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/5/8", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "Console", :phy_type => "Other"},
        {:name => "MGMT A", :phy_type => "10/100/1000 Base T"},
      ]
    },
     "N_ALU MMBTS" => {
      :type => "ALU MMBTS",
      :vendor => "Alcatel",
      :make_model => "ALU MMBTS - <Model>",
      :description => "<Description>",
      :nm_connection => {:name => "Verizon SAM #1", :nm_type => "5620Sam"},
      :ports => [
      	{:name => "1/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
      	{:name => "Radio", :phy_type => "Other"},
        {:name => "Console", :phy_type => "Other"},
        {:name => "MGMT A", :phy_type => "10/100/1000 Base T"},
      ]
    }   
  }

  #This submodule is a specific site for this site
  # EACH Site MUST have a NODE_INSTANCE hash of nodes and
  # a PORT_CONNECTIONS array of internal connections
  module Primary
    NODE_INSTANCE = {
      "N_7705 SAR-8_1" => {
        :node => VzW_CS_ALU::NODE["N_7705 SAR-8"], :id => 1,
        :identifier => "TACMWA43T1A-P-AL-0118-", :id_length => 2,
        :ip_suffix => { :primary => "44", :secondary => "45", :gateway => "0" },
        :enni_connections => nil
       },
       "N_ALU MMBTS_1" => {
        :node => VzW_CS_ALU::NODE["N_ALU MMBTS"], :id => 1,
        :identifier => "TACMWA43T1A-P-AL-BTS-", :id_length => 2,
        :ip_suffix => { :primary => "46", :secondary => "47", :gateway => "0" },
        :enni_connections => [
          { :protection_type => "unprotected", :physical_medium => "Other", :demarc_icon => "cell site",
            :ether_type => "Other", :auto_negotiate => false, :port_enap_type => "NULL",
            :port_connections => ["Radio"], :member_handle => "MMBTS-Radio-01" }
        ]
       }
    }

    # [ [ [node_type, port], [node_type, port] ], [ [node_type, port], [node_type, port] ], ... ]
    # | | | Single Port   |  | Conected Port | |                                                |
    # | |Single connection (only need one way) |                                                |
    # | List of all connections                                                                 |

    PORT_CONNECTIONS = [
      [[NODE_INSTANCE["N_7705 SAR-8_1"], "1/5/7"], [NODE_INSTANCE["N_ALU MMBTS_1"], "1/1"]]

    ]
  end

  #Hash of all sites (a.k.a. submodules) for this Site type.
  SITES = {
    "Primary" => Primary
  }

  #Inter Site Connections
  # [ [ [ Site String, Node Instance Name, Port ], [ Site String, Node Instance Name, Port ], ... ]
  # | | |    Single Port                            |  |         Connected Port                    |      |
  # | |         Single Connection (Only need one way)                                              |      |
  # |    List of all connection between all sites                                                     |
  #
  CONNECTIONS = [
  ]

end
