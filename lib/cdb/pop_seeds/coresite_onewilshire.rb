module CoreSite_OneWilshire

  #The following is default site seeding info

  #Nodes Specific ONLY to this Site
  # Node Structure: (NOTE: Key may not be the same as node type
  # so don't use it as a shortcut, just most of the time it will be the same
  # the node's :type)
  # "" => {
  #    :type => "",
  #    :vendor => "",
  #    :make_model => "",
  #    :description => "",
  #    :nm_connection => {:name => "", nm_type => ""},
  #    :ports => [
  #      {:name => "", :phy_type => ""},
  #    ]
  # },
  NODE = {
    "N_MLXe32_1" => {
      :type => "Brocade MLXe32",
      :vendor => "Brocade",
      :make_model => "Brocade MLXe32",
      :description => "Brocade MLXe32",
      :nm_connection => {:name => "CoreSite SolarWinds", :nm_type => "SolarWinds"},
      :ports => [
        {:name => "4/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "4/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "4/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "4/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "4/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "4/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "4/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "4/8", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "4/9", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "4/10", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "4/11", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "4/12", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "4/13", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "4/14", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "4/15", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "4/16", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "14/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "14/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "14/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "14/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "14/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "14/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "14/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "14/8", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "14/9", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "14/10", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "14/11", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "14/12", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "14/13", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "14/14", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "14/15", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "14/16", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "19/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "19/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "19/3", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "21/8", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "25/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "25/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "25/3", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "25/4", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "27/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "27/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "27/3", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "27/4", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "29/8", :phy_type => "10GigE LR; 1310nm; SMF"},
      ]
    },
    "N_MLXe32_2" => {
      :type => "Brocade MLXe32",
      :vendor => "Brocade",
      :make_model => "Brocade MLXe32",
      :description => "Brocade MLXe32",
      :nm_connection => {:name => "CoreSite SolarWinds", :nm_type => "SolarWinds"},
      :ports => [
        {:name => "2/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/8", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/9", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/10", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/11", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/12", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/13", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/14", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/15", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/16", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "16/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "16/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "16/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "16/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "16/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "16/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "16/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "16/8", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "16/9", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "16/10", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "16/11", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "16/12", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "16/13", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "16/14", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "16/15", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "16/16", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "17/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "17/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "17/3", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "18/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "18/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "18/3", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "18/8", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "19/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "19/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "19/3", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "19/4", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "32/8", :phy_type => "10GigE LR; 1310nm; SMF"},
      ]
    },
  }

  #This submodule is a specific site for this site
  # EACH Site MUST have a NODE_INSTANCE hash of nodes and
  # a PORT_CONNECTIONS array of internal connections
  module Primary
    NODE_INSTANCE = {
      "N_MLXe32_1" => {
        :node => CoreSite_OneWilshire::NODE["N_MLXe32_1"], :id => 1,
        :identifier => ".IDC01-L1.HSE.MLX32.", :id_length => 2,
        :ip_suffix => { :primary => "243", :secondary => nil, :gateway => "1", :loopback => "1" },
        :enni_connections => []
       },
       "N_MLXe32_2" => {
        :node => CoreSite_OneWilshire::NODE["N_MLXe32_2"], :id => 2,
        :identifier => ".IDC01-L1.HSE.MLX32.", :id_length => 2,
        :ip_suffix => { :primary => "244", :secondary => nil, :gateway => "1", :loopback => "2" },
        :enni_connections => []
       },
       #MMR4.IDF
       "N_CES_2048_MMR4_IDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".MMR4.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "22", :secondary => nil, :gateway => "1", :loopback => "3" },
        :enni_connections => []
       },
       "N_CES_2048_MMR4_IDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".MMR4.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "23", :secondary => nil, :gateway => "1", :loopback => "4" },
        :enni_connections => []
       },
       #MMR4.MUX1
       "N_CES_2048_MMR4_MUX1_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".MMR4.MUX1.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "24", :secondary => nil, :gateway => "1", :loopback => "5" },
        :enni_connections => []
       },
       "N_CES_2048_MMR4_MUX1_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".MMR4.MUX1.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "25", :secondary => nil, :gateway => "1", :loopback => "6" },
        :enni_connections => []
       },
       #MMR4.MUX2
       "N_CES_2048_MMR4_MUX2_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".MMR4.MUX2.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "26", :secondary => nil, :gateway => "1", :loopback => "7" },
        :enni_connections => []
       },
       "N_CES_2048_MMR4_MUX2_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".MMR4.MUX2.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "27", :secondary => nil, :gateway => "1", :loopback => "8" },
        :enni_connections => []
       },
       #IDC01.L1.HSE
       "N_CES_2048_IDC01_L1_HSE_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".IDC01.L1.HSE.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "20", :secondary => nil, :gateway => "1", :loopback => "9" },
        :enni_connections => []
       },
       "N_CES_2048_IDC01_L1_HSE_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".IDC01.L1.HSE.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "21", :secondary => nil, :gateway => "1", :loopback => "10" },
        :enni_connections => []
       },
       #MMR4.MUX2
       "N_CES_2048_MMR4_MUX2_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".MMR4.MUX2.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "28", :secondary => nil, :gateway => "1", :loopback => "11" },
        :enni_connections => []
       },
       "N_CES_2048_MMR4_MUX2_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".MMR4.MUX2.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "29", :secondary => nil, :gateway => "1", :loopback => "12" },
        :enni_connections => []
       },
       #IDC07.HSE
       "N_CES_2024_IDC07_HSE_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 1,
        :identifier => ".IDC07.HSE.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "60", :secondary => nil, :gateway => "1", :loopback => "13" },
        :enni_connections => []
       },
       "N_CES_2024_IDC07_HSE_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 2,
        :identifier => ".IDC07.HSE.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "61", :secondary => nil, :gateway => "1", :loopback => "14" },
        :enni_connections => []
       },
       #IDC08.LA43
       "N_CES_2024_IDC08_LA43_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 1,
        :identifier => ".IDC08.LA43.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "30", :secondary => nil, :gateway => "1", :loopback => "15" },
        :enni_connections => []
       },
       "N_CES_2024_IDC08_LA43_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 2,
        :identifier => ".IDC08.LA43.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "31", :secondary => nil, :gateway => "1", :loopback => "16" },
        :enni_connections => []
       },
       #IDC08.AB19
       "N_CES_2024_IDC08_AB19_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 1,
        :identifier => ".IDC08.AB19.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "62", :secondary => nil, :gateway => "1", :loopback => "17" },
        :enni_connections => []
       },
       "N_CES_2024_IDC08_AB19_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 2,
        :identifier => ".IDC08.AB19.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "63", :secondary => nil, :gateway => "1", :loopback => "18" },
        :enni_connections => []
       },
       #IDC1010.IDF
       "N_CES_2048_IDC1010_IDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".IDC1010.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "32", :secondary => nil, :gateway => "1", :loopback => "19" },
        :enni_connections => []
       },
       "N_CES_2048_IDC1010_IDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".IDC1010.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "33", :secondary => nil, :gateway => "1", :loopback => "20" },
        :enni_connections => []
       },
       #IDC1010.IDF
       "N_CES_2048_IDC1010_IDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".IDC1010.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "64", :secondary => nil, :gateway => "1", :loopback => "21" },
        :enni_connections => []
       },
       "N_CES_2048_IDC1010_IDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".IDC1010.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "65", :secondary => nil, :gateway => "1", :loopback => "22" },
        :enni_connections => []
       },
       #IDC1014.IDF
       "N_CES_2048_IDC1014_IDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".IDC1014.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "34", :secondary => nil, :gateway => "1", :loopback => "23" },
        :enni_connections => []
       },
       "N_CES_2048_IDC1014_IDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".IDC1014.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "35", :secondary => nil, :gateway => "1", :loopback => "24" },
        :enni_connections => []
       },
       #IDC11.HSE
       "N_CES_2048_IDC11_HSE_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".IDC11.HSE.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "36", :secondary => nil, :gateway => "1", :loopback => "25" },
        :enni_connections => []
       },
       "N_CES_2048_IDC11_HSE_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".IDC11.HSE.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "37", :secondary => nil, :gateway => "1", :loopback => "26" },
        :enni_connections => []
       },
       #IDC19.HSE
       "N_CES_2048_IDC19_HSE_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".IDC19.HSE.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "38", :secondary => nil, :gateway => "1", :loopback => "27" },
        :enni_connections => []
       },
       "N_CES_2048_IDC19_HSE_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".IDC19.HSE.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "39", :secondary => nil, :gateway => "1", :loopback => "28" },
        :enni_connections => []
       },
       #IDC27.HSE
       "N_CES_2048_IDC27_HSE_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".IDC27.HSE.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "40", :secondary => nil, :gateway => "1", :loopback => "29" },
        :enni_connections => []
       },
       "N_CES_2048_IDC27_HSE_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".IDC27.HSE.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "41", :secondary => nil, :gateway => "1", :loopback => "30" },
        :enni_connections => []
       },
       #IDC27.IDF
       "N_CES_2048_IDC27_IDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".IDC27.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "66", :secondary => nil, :gateway => "1", :loopback => "31" },
        :enni_connections => []
       },
       "N_CES_2048_IDC27_IDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".IDC27.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "67", :secondary => nil, :gateway => "1", :loopback => "32" },
        :enni_connections => []
       },
       #IDC28.HSE
       "N_CES_2048_IDC28_HSE_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".IDC28.HSE.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "68", :secondary => nil, :gateway => "1", :loopback => "33" },
        :enni_connections => []
       },
       "N_CES_2048_IDC28_HSE_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".IDC28.HSE.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "69", :secondary => nil, :gateway => "1", :loopback => "34" },
        :enni_connections => []
       },

    }

    # [ [ [node_type, port], [node_type, port] ], [ [node_type, port], [node_type, port] ], ... ]
    # | | | Single Port   |  | Conected Port | |                                                |
    # | |Single connection (only need one way) |                                                |
    # | List of all connections                                                                 |

    PORT_CONNECTIONS = [
      #MMR4.IDF
      [[NODE_INSTANCE["N_MLXe32_1"], "4/1"], [NODE_INSTANCE["N_CES_2048_MMR4_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "4/2"], [NODE_INSTANCE["N_CES_2048_MMR4_IDF_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "2/1"], [NODE_INSTANCE["N_CES_2048_MMR4_IDF_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "2/2"], [NODE_INSTANCE["N_CES_2048_MMR4_IDF_2"], "1/2"]],
      #MMR4.MUX1
      [[NODE_INSTANCE["N_MLXe32_1"], "4/3"], [NODE_INSTANCE["N_CES_2048_MMR4_MUX1_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "4/4"], [NODE_INSTANCE["N_CES_2048_MMR4_MUX1_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "2/3"], [NODE_INSTANCE["N_CES_2048_MMR4_MUX1_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "2/4"], [NODE_INSTANCE["N_CES_2048_MMR4_MUX1_2"], "1/2"]],
      #MMR4.MUX2
      [[NODE_INSTANCE["N_MLXe32_1"], "4/5"], [NODE_INSTANCE["N_CES_2048_MMR4_MUX2_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "4/6"], [NODE_INSTANCE["N_CES_2048_MMR4_MUX2_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "2/5"], [NODE_INSTANCE["N_CES_2048_MMR4_MUX2_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "2/6"], [NODE_INSTANCE["N_CES_2048_MMR4_MUX2_2"], "1/2"]],
      #IDC01.L1.HSE
      [[NODE_INSTANCE["N_MLXe32_1"], "4/7"], [NODE_INSTANCE["N_CES_2048_IDC01_L1_HSE_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "4/8"], [NODE_INSTANCE["N_CES_2048_IDC01_L1_HSE_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "2/7"], [NODE_INSTANCE["N_CES_2048_IDC01_L1_HSE_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "2/8"], [NODE_INSTANCE["N_CES_2048_IDC01_L1_HSE_2"], "1/2"]],
      #MMR4.MUX2
      [[NODE_INSTANCE["N_MLXe32_1"], "4/9"], [NODE_INSTANCE["N_CES_2048_MMR4_MUX2_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "4/10"], [NODE_INSTANCE["N_CES_2048_MMR4_MUX2_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "2/9"], [NODE_INSTANCE["N_CES_2048_MMR4_MUX2_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "2/10"], [NODE_INSTANCE["N_CES_2048_MMR4_MUX2_2"], "1/2"]],
      #IDC07.HSE
      [[NODE_INSTANCE["N_MLXe32_1"], "4/11"], [NODE_INSTANCE["N_CES_2024_IDC07_HSE_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "4/12"], [NODE_INSTANCE["N_CES_2024_IDC07_HSE_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "2/11"], [NODE_INSTANCE["N_CES_2024_IDC07_HSE_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "2/12"], [NODE_INSTANCE["N_CES_2024_IDC07_HSE_2"], "1/2"]],
      #IDC08.LA43
      [[NODE_INSTANCE["N_MLXe32_1"], "4/13"], [NODE_INSTANCE["N_CES_2024_IDC08_LA43_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "4/14"], [NODE_INSTANCE["N_CES_2024_IDC08_LA43_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "2/13"], [NODE_INSTANCE["N_CES_2024_IDC08_LA43_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "2/14"], [NODE_INSTANCE["N_CES_2024_IDC08_LA43_2"], "1/2"]],
      #IDC08.AB19
      [[NODE_INSTANCE["N_MLXe32_1"], "4/15"], [NODE_INSTANCE["N_CES_2024_IDC08_AB19_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "4/16"], [NODE_INSTANCE["N_CES_2024_IDC08_AB19_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "2/15"], [NODE_INSTANCE["N_CES_2024_IDC08_AB19_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "2/16"], [NODE_INSTANCE["N_CES_2024_IDC08_AB19_2"], "1/2"]],
      #IDC1010.IDF
      [[NODE_INSTANCE["N_MLXe32_1"], "14/1"], [NODE_INSTANCE["N_CES_2048_IDC1010_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "14/2"], [NODE_INSTANCE["N_CES_2048_IDC1010_IDF_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "16/1"], [NODE_INSTANCE["N_CES_2048_IDC1010_IDF_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "16/2"], [NODE_INSTANCE["N_CES_2048_IDC1010_IDF_2"], "1/2"]],
      #IDC1010.IDF
      [[NODE_INSTANCE["N_MLXe32_1"], "14/3"], [NODE_INSTANCE["N_CES_2048_IDC1010_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "14/4"], [NODE_INSTANCE["N_CES_2048_IDC1010_IDF_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "16/3"], [NODE_INSTANCE["N_CES_2048_IDC1010_IDF_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "16/4"], [NODE_INSTANCE["N_CES_2048_IDC1010_IDF_2"], "1/2"]],
      #IDC1014.IDF
      [[NODE_INSTANCE["N_MLXe32_1"], "14/5"], [NODE_INSTANCE["N_CES_2048_IDC1014_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "14/6"], [NODE_INSTANCE["N_CES_2048_IDC1014_IDF_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "16/5"], [NODE_INSTANCE["N_CES_2048_IDC1014_IDF_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "16/6"], [NODE_INSTANCE["N_CES_2048_IDC1014_IDF_2"], "1/2"]],
      #IDC11.HSE
      [[NODE_INSTANCE["N_MLXe32_1"], "14/7"], [NODE_INSTANCE["N_CES_2048_IDC11_HSE_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "14/8"], [NODE_INSTANCE["N_CES_2048_IDC11_HSE_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "16/7"], [NODE_INSTANCE["N_CES_2048_IDC11_HSE_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "16/8"], [NODE_INSTANCE["N_CES_2048_IDC11_HSE_2"], "1/2"]],
      #IDC19.HSE
      [[NODE_INSTANCE["N_MLXe32_1"], "14/9"], [NODE_INSTANCE["N_CES_2048_IDC19_HSE_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "14/10"], [NODE_INSTANCE["N_CES_2048_IDC19_HSE_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "16/9"], [NODE_INSTANCE["N_CES_2048_IDC19_HSE_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "16/10"], [NODE_INSTANCE["N_CES_2048_IDC19_HSE_2"], "1/2"]],
      #IDC27.HSE
      [[NODE_INSTANCE["N_MLXe32_1"], "14/11"], [NODE_INSTANCE["N_CES_2048_IDC27_HSE_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "14/12"], [NODE_INSTANCE["N_CES_2048_IDC27_HSE_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "16/11"], [NODE_INSTANCE["N_CES_2048_IDC27_HSE_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "16/12"], [NODE_INSTANCE["N_CES_2048_IDC27_HSE_2"], "1/2"]],
      #IDC27.IDF
      [[NODE_INSTANCE["N_MLXe32_1"], "14/13"], [NODE_INSTANCE["N_CES_2048_IDC27_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "14/14"], [NODE_INSTANCE["N_CES_2048_IDC27_IDF_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "16/13"], [NODE_INSTANCE["N_CES_2048_IDC27_IDF_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "16/14"], [NODE_INSTANCE["N_CES_2048_IDC27_IDF_2"], "1/2"]],
      #IDC28.HSE
      [[NODE_INSTANCE["N_MLXe32_1"], "14/15"], [NODE_INSTANCE["N_CES_2048_IDC28_HSE_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "14/16"], [NODE_INSTANCE["N_CES_2048_IDC28_HSE_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "16/15"], [NODE_INSTANCE["N_CES_2048_IDC28_HSE_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe32_2"], "16/16"], [NODE_INSTANCE["N_CES_2048_IDC28_HSE_2"], "1/2"]],
      #Interconnect
      [[NODE_INSTANCE["N_MLXe32_1"], "19/1"], [NODE_INSTANCE["N_MLXe32_2"], "17/1"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "19/2"], [NODE_INSTANCE["N_MLXe32_2"], "17/2"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "19/3"], [NODE_INSTANCE["N_MLXe32_2"], "17/3"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "25/1"], [NODE_INSTANCE["N_MLXe32_2"], "18/1"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "25/2"], [NODE_INSTANCE["N_MLXe32_2"], "18/2"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "25/3"], [NODE_INSTANCE["N_MLXe32_2"], "18/3"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "27/1"], [NODE_INSTANCE["N_MLXe32_2"], "19/1"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "27/2"], [NODE_INSTANCE["N_MLXe32_2"], "19/2"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "27/3"], [NODE_INSTANCE["N_MLXe32_2"], "19/3"]],
      [[NODE_INSTANCE["N_MLXe32_1"], "27/4"], [NODE_INSTANCE["N_MLXe32_2"], "19/4"]],
    ]
  end

  #Hash of all sites (a.k.a. submodules) for this Site type.
  SITES = {
    "Primary" => Primary
  }

  #Inter Site Connections
  # [ [ [ Site String, Node Instance Name, Port ], [ Site String, Node Instance Name, Port ], ... ]
  # | | |    Single Port                            |  |         Connected Port                    |      |
  # | |         Single Connection (Only need one way)                                              |      |
  # |    List of all connection between all sites                                                     |
  #
  CONNECTIONS = [
  ]

end
