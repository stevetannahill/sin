module CommonCoreSitePop

  # Node Structure: (NOTE: Key may not be the same as node type
  # so don't use it as a shortcut, just most of the time it will be the same
  # the node's :type)
  # "" => {
  #    :type => "",
  #    :vendor => "",
  #    :make_model => "",
  #    :description => "",
  #    :nm_connection => {:name => "", nm_type => ""},
  #    :ports => [
  #      {:name => "", :phy_type => ""},
  #    ]
  # },
  NODE = {
    "N_MLXe8" => {
      :type => "Brocade MLXe8",
      :vendor => "Brocade",
      :make_model => "Brocade MLXe8",
      :description => "Brocade MLXe8",
      :nm_connection => {:name => "CoreSite SolarWinds", :nm_type => "SolarWinds"},
      :ports => [
        {:name => "1/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "3/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "4/1", :phy_type => "10GigE LR; 1310nm; SMF"},
      ]
    },
    "N_MLXe16" => {
      :type => "Brocade MLXe16",
      :vendor => "Brocade",
      :make_model => "Brocade MLXe16",
      :description => "Brocade MLXe16",
      :nm_connection => {:name => "CoreSite SolarWinds", :nm_type => "SolarWinds"},
      :ports => [
        {:name => "1/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/8", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/8", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "15/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "15/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "16/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "16/2", :phy_type => "10GigE LR; 1310nm; SMF"},
      ]
    },
    "N_CES_2024" => {
      :type => "Brocade CES 2024",
      :vendor => "Brocade",
      :make_model => "Brocade CES 2024",
      :description => "Brocade CES 2024",
      :nm_connection => {:name => "CoreSite SolarWinds", :nm_type => "SolarWinds"},
      :ports => [
        {:name => "1/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
      ]
    },
    "N_CES_2048" => {
      :type => "Brocade CES 2048",
      :vendor => "Brocade",
      :make_model => "Brocade CES 2048",
      :description => "Brocade CES 2048",
      :nm_connection => {:name => "CoreSite SolarWinds", :nm_type => "SolarWinds"},
      :ports => [
        {:name => "1/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
      ]
    }
  }
end