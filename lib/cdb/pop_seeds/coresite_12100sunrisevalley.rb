module CoreSite_12100SunriseValley


  #The following is default site seeding info

  #Nodes Specific ONLY to this Site
  # Node Structure: (NOTE: Key may not be the same as node type
  # so don't use it as a shortcut, just most of the time it will be the same
  # the node's :type)
  # "" => {
  #    :type => "",
  #    :vendor => "",
  #    :make_model => "",
  #    :description => "",
  #    :nm_connection => {:name => "", nm_type => ""},
  #    :ports => [
  #      {:name => "", :phy_type => ""},
  #    ]
  # },
  NODE = {
  }

  #This submodule is a specific site for this site
  # EACH Site MUST have a NODE_INSTANCE hash of nodes and
  # a PORT_CONNECTIONS array of internal connections
  module Primary
    NODE_INSTANCE = {
      "N_MLXe16_1" => {
        :node => CommonCoreSitePop::NODE["N_MLXe16"], :id => 1,
        :identifier => ".CR8.MDF.MLX16.", :id_length => 2,
        :ip_suffix => { :primary => "100", :secondary => nil, :gateway => "1", :loopback => "1" },
        :enni_connections => []
       },
       "N_MLXe16_2" => {
        :node => CommonCoreSitePop::NODE["N_MLXe16"], :id => 2,
        :identifier => ".CR8.MDF.MLX16.", :id_length => 2,
        :ip_suffix => { :primary => "101", :secondary => nil, :gateway => "1", :loopback => "2" },
        :enni_connections => []
       },
       #CR5.IDF
       "N_CES_2024_CR5_IDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 1,
        :identifier => ".CR5.IDF.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "98", :secondary => nil, :gateway => "1", :loopback => "4" },
        :enni_connections => []
       },
       "N_CES_2024_CR5_IDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 2,
        :identifier => ".CR5.IDF.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "91", :secondary => nil, :gateway => "1", :loopback => "5" },
        :enni_connections => []
       },
       #CR6.IDF
       "N_CES_2048_CR6_IDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".CR6.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "78", :secondary => nil, :gateway => "1", :loopback => "6" },
        :enni_connections => []
       },
       "N_CES_2048_CR6_IDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".CR6.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "71", :secondary => nil, :gateway => "1", :loopback => "7" },
        :enni_connections => []
       },
       #CR7.IDF
       "N_CES_2048_CR7_IDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".CR7.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "72", :secondary => nil, :gateway => "1", :loopback => "8" },
        :enni_connections => []
       },
       "N_CES_2048_CR7_IDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".CR7.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "73", :secondary => nil, :gateway => "1", :loopback => "9" },
        :enni_connections => []
       },
       #CR8.MDF
       "N_CES_2048_CR8_MDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".CR8.MDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "74", :secondary => nil, :gateway => "1", :loopback => "10" },
        :enni_connections => []
       },
       "N_CES_2048_CR8_MDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".CR8.MDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "75", :secondary => nil, :gateway => "1", :loopback => "11" },
        :enni_connections => []
       },
       #CR10.IDF
       "N_CES_2048_CR10_IDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".CR10.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "76", :secondary => nil, :gateway => "1", :loopback => "12" },
        :enni_connections => []
       },
       "N_CES_2048_CR10_IDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".CR10.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "77", :secondary => nil, :gateway => "1", :loopback => "13" },
        :enni_connections => []
       },
       #CR12.IDF
       "N_CES_2024_CR12_IDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 1,
        :identifier => ".CR12.IDF.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "92", :secondary => nil, :gateway => "1", :loopback => "14" },
        :enni_connections => []
       },
       "N_CES_2024_CR12_IDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 2,
        :identifier => ".CR12.IDF.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "93", :secondary => nil, :gateway => "1", :loopback => "15" },
        :enni_connections => []
       },
       #CR13.IDF
       "N_CES_2024_CR13_IDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 1,
        :identifier => ".CR13.IDF.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "94", :secondary => nil, :gateway => "1", :loopback => "16" },
        :enni_connections => []
       },
       "N_CES_2024_CR13_IDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 2,
        :identifier => ".CR13.IDF.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "95", :secondary => nil, :gateway => "1", :loopback => "17" },
        :enni_connections => []
       },
       #FL1.MTC
       "N_CES_2024_FL1_MTC_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 1,
        :identifier => ".FL1.MTC.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "96", :secondary => nil, :gateway => "1", :loopback => "18" },
        :enni_connections => []
       },
       "N_CES_2024_FL1_MTC_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 2,
        :identifier => ".FL1.MTC.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "97", :secondary => nil, :gateway => "1", :loopback => "19" },
        :enni_connections => []
       },
    }

    # [ [ [node_type, port], [node_type, port] ], [ [node_type, port], [node_type, port] ], ... ]
    # | | | Single Port   |  | Conected Port | |                                                |
    # | |Single connection (only need one way) |                                                |
    # | List of all connections                                                                 |

    PORT_CONNECTIONS = [
      #CR5_IDF
      [[NODE_INSTANCE["N_MLXe16_1"], "1/1"], [NODE_INSTANCE["N_CES_2024_CR5_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "1/2"], [NODE_INSTANCE["N_CES_2024_CR5_IDF_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "1/1"], [NODE_INSTANCE["N_CES_2024_CR5_IDF_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "1/2"], [NODE_INSTANCE["N_CES_2024_CR5_IDF_2"], "1/2"]],
      #CR6_IDF
      [[NODE_INSTANCE["N_MLXe16_1"], "2/1"], [NODE_INSTANCE["N_CES_2048_CR6_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "2/2"], [NODE_INSTANCE["N_CES_2048_CR6_IDF_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "2/1"], [NODE_INSTANCE["N_CES_2048_CR6_IDF_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "2/2"], [NODE_INSTANCE["N_CES_2048_CR6_IDF_2"], "1/2"]],
      #CR7_IDF
      [[NODE_INSTANCE["N_MLXe16_1"], "2/3"], [NODE_INSTANCE["N_CES_2048_CR7_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "2/4"], [NODE_INSTANCE["N_CES_2048_CR7_IDF_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "2/3"], [NODE_INSTANCE["N_CES_2048_CR7_IDF_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "2/4"], [NODE_INSTANCE["N_CES_2048_CR7_IDF_2"], "1/2"]],
      #CR8_MDF
      [[NODE_INSTANCE["N_MLXe16_1"], "2/5"], [NODE_INSTANCE["N_CES_2048_CR8_MDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "2/6"], [NODE_INSTANCE["N_CES_2048_CR8_MDF_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "2/5"], [NODE_INSTANCE["N_CES_2048_CR8_MDF_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "2/6"], [NODE_INSTANCE["N_CES_2048_CR8_MDF_2"], "1/2"]],
      #CR10_IDF
      [[NODE_INSTANCE["N_MLXe16_1"], "2/7"], [NODE_INSTANCE["N_CES_2048_CR10_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "2/8"], [NODE_INSTANCE["N_CES_2048_CR10_IDF_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "2/7"], [NODE_INSTANCE["N_CES_2048_CR10_IDF_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "2/8"], [NODE_INSTANCE["N_CES_2048_CR10_IDF_2"], "1/2"]],
      #CR12_IDF
      [[NODE_INSTANCE["N_MLXe16_1"], "1/3"], [NODE_INSTANCE["N_CES_2024_CR12_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "1/4"], [NODE_INSTANCE["N_CES_2024_CR12_IDF_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "1/3"], [NODE_INSTANCE["N_CES_2024_CR12_IDF_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "1/4"], [NODE_INSTANCE["N_CES_2024_CR12_IDF_2"], "1/2"]],
      #CR13_IDF
      [[NODE_INSTANCE["N_MLXe16_1"], "1/5"], [NODE_INSTANCE["N_CES_2024_CR13_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "1/6"], [NODE_INSTANCE["N_CES_2024_CR13_IDF_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "1/5"], [NODE_INSTANCE["N_CES_2024_CR13_IDF_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "1/6"], [NODE_INSTANCE["N_CES_2024_CR13_IDF_2"], "1/2"]],
      #FL1.MTC
      [[NODE_INSTANCE["N_MLXe16_1"], "1/7"], [NODE_INSTANCE["N_CES_2024_FL1_MTC_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "1/8"], [NODE_INSTANCE["N_CES_2024_FL1_MTC_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "1/7"], [NODE_INSTANCE["N_CES_2024_FL1_MTC_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "1/8"], [NODE_INSTANCE["N_CES_2024_FL1_MTC_2"], "1/2"]],
      #Interconnect
      [[NODE_INSTANCE["N_MLXe16_1"], "15/1"], [NODE_INSTANCE["N_MLXe16_2"], "15/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "16/1"], [NODE_INSTANCE["N_MLXe16_2"], "16/1"]],
    ]
  end

  #Hash of all sites (a.k.a. submodules) for this Site type.
  SITES = {
    "Primary" => Primary
  }

  #Inter Site Connections
  # [ [ [ Site String, Node Instance Name, Port ], [ Site String, Node Instance Name, Port ], ... ]
  # | | |    Single Port                            |  |         Connected Port                    |      |
  # | |         Single Connection (Only need one way)                                              |      |
  # |    List of all connection between all sites                                                     |
  #
  CONNECTIONS = [
  ]

end
