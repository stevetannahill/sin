#!/bin/env brixperl
# $Id: extraction.pl,v 1.3 2007/05/16 13:07:51 bbennett Exp $
use strict;
use warnings;

use BxAPI;
use Data::Dumper;


# Process the command line arguments
my %args = @ARGV;
foreach my $k (keys %args) {
    my $ok = $k;
    $k =~ s/^-//;
    $args{$k} = delete $args{$ok}
        if $ok ne $k;
}

my $username    = $args{username}      || $args{user};
my $password    = $args{password}      || $args{pw};
my $database    = $args{database}      || $args{db};
my $provider_id = $args{provider_id}   || 1000;
my $debug       = $args{debug}         || 0;

my $test_name   = $args{test}          || usage();
my $results     = $args{results}       || usage();

my $start_time  = $args{start_time}    || "6 minutes ago";
my $end_time    = $args{end_time}      || "1 minute ago";


# Parse the results
my @results = split /\s*,\s*/, $results;
my @scale   = ();
foreach my $r (@results) {
    if ($r =~ s/\/(\d+)$//) {
	push @scale, $1;
    } else {
	push @scale, 1;
    }
}

# Get a database handle
my $dbh = bx_db_connect(username => $username,
			password => $password,
			database => $database)
    or die "Unable to connect to the database";

# Check the test we were given
$test_name = get_matching_test($test_name, $provider_id, $dbh);

# Get all of the verifiers in the system
# We have to get all the slas first so we can pass it to bx_get_verifiers
my %verifiers = ();
foreach my $s (@{ bx_get_slas(dbh => $dbh) }) {
    my $sla_id = $s->[1];
    my $verifiers = 
	bx_get_verifiers(provider_id => $provider_id,
			 sla_id      => $sla_id,
			 dbh         => $dbh);

    # Store the verifiers by name (this has the advantage of 
    # making the verifier list unique if SLAs share verifiers)
    foreach my $v (@$verifiers) {
	$verifiers{ $v->{verifier_name} } = $v;
    }
}

# Keep the cross verifier averages
my @averaged = ();
my $count = 0;

# Get the test data
my @verifier_names = map { $_->{verifier_name} } values %verifiers;
my $cursor = 
    bx_get_test_data(test_name   => $test_name,
		     verifiers   => \@verifier_names,
		     start_time  => $start_time,
		     end_time    => $end_time,
		     
		     # Note we are copying the results to avoid a bug
		     show_fixed_results 
		                 => [ @results ],
		
		     provider_id => $provider_id,
		     null_string => '',
		     dbh         => $dbh);

# Loop over the cursor and read the results
# Sum each result and store the count so we can average later
while ( my $data = $cursor->bx_get_next_result() ) {
    $count++;
    for (my $i = 3; $i < @$data; $i++) {
	$averaged[ $i - 3 ] += $data->[$i];
    }
}

# Now do the averaging and print the results
printf("Test: %s (from '%s', to '%s')\n", 
       $test_name, $start_time, $end_time);
for (my $i = 0; $i < @averaged; $i++) {
    $averaged[$i] /= $count;
    $averaged[$i] /= $scale[$i]
	if $scale[$i] != 1;

    print "\t$results[$i]: $averaged[$i]\n";
}

# Free up the DBH
bx_db_disconnect(dbh => $dbh);


exit;

sub usage {
    print STDERR <<"EOF;";
    Usage: $0 -test test_name \
              -results endToEndDelayAvg/1000,jitter \
	     [-start_time "6 minutes ago"] \
	     [-end_time "1 minute ago"] \
	     [-username username] \
	     [-password password] \
             [-database database] \
	     [-provider_id provider_id]

    This script retrieves all named results from the given test for
    all verifiers in the BrixWorx system and averages the results
    together.  You can specify a scale by putting /scale_num after
    the result name in the comma separated list.

EOF;

    exit;
}

sub get_matching_test {
    my ($test_name, $provider_id, $dbh) = @_;

    # Get the available tests from the database and see if ours works
    my $available_tests = bx_get_tests(test_name   => '.', # Match anything
				       provider_id => $provider_id,
				       dbh         => $dbh);
    
    my %matching_tests = ();
    foreach my $t (@$available_tests) {
	my ($display_name, $name, $peer_to_peer, $passive) = @$t;
	next unless $name =~ /$test_name/;
	$matching_tests{$name} = $display_name;
    }
    
    if (keys %matching_tests != 1) {
	print STDERR "Unable to find a single matching test for '$test_name'.  Got:\n";
	foreach my $k (sort { lc($matching_tests{$a}) cmp lc($matching_tests{$b}) } 
		       keys %matching_tests)
	{
	    print STDERR "\t$k: $matching_tests{$k}\n";
	}
	print STDERR "Please select one of the above and try again.\n";
	exit 1;
    }
    
    return (keys %matching_tests)[0];
}
