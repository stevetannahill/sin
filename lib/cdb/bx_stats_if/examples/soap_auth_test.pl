#!/bin/env brixperl
# $Id: soap_auth_test.pl,v 1.2 2010/05/12 18:33:24 harrison Exp $

# This example script calls the Authenticate
# function of the SOAP API.

use Data::Dumper;
$Data::Dumper::Indent=1;
$Data::Dumper::Deepcopy=1;
use SOAP::Lite;
#SOAP::Lite->import(+trace => 'all');
use HTTP::Cookies;
use Timer;

# Modify these to match your system settings
my $SERVER = 'localhost:8080';
my $UNAME  = 'administrator';
my $PWORD  = 'admin';

my $utils_request = SOAP::Lite->new( uri   => 'API/SOAP/Utils',
				     proxy => "http://${SERVER}/API/SOAP/",
				   );

my $auth = Authenticate();

print "AUTH TOKEN: $auth\n";

my $slas = getSLAs( auth => $auth, req => $utils_request );

print Dumper('SLAs',$slas);

sub Authenticate {
    # Establish the request. Authentication is
    # managed through API/SOAP/Auth, but other
    # functions are available through:
    #
    # - API/SOAP/Utils           (Top-level object functions)
    # - API/SOAP/DataExtraction  (Data extraction functions)
    #
    my $auth_request = SOAP::Lite->new
	( uri   => 'API/SOAP/Auth',
	  proxy => "http://${SERVER}/API/SOAP/" );

    # Call the desired function as a method. Note that input
    # parameters must be packaged as SOAP data.
    my $result = 
	eval {
	    $auth_request->Authenticate
		( SOAP::Data->name(uname => $UNAME),
		  SOAP::Data->name(pword => $PWORD),
		);
	};
    if ($@) {
	print STDERR "Request error for Authenticate: $@\n";
	exit;
    }

    _handle_soap_error($result);

    my $body = $result->body();
    my $resp = $body->{AuthenticateResponse};

    _handle_api_error($resp);

    return $resp->{token};
}

sub getSLAs {
    my %args = @_;
    my $auth = $args{auth};
    my $req  = $args{req};

    $result = eval {
        $utils_request->getSLAS
	    ( SOAP::Data->name( authority => $auth ),
	      SOAP::Data->name( sla_match => '.*' ),
        );
    };
    if ($@) {
        print STDERR "Request error for SLAS: $@\n";
	exit;
    }

    _handle_soap_error($result);

    my $body = $result->body();
    my $resp = $body->{getSLASResponse};

    _handle_api_error($resp);

    return $resp->{sla_list};
}

sub _handle_soap_error {
    my $result = shift;

    return 
	unless $result->fault;

    my $fault = $result->fault->faultstring();

    print 'SOAP Error: '.$fault."\n";
    exit(1);
}

sub _handle_api_error {
    my $resp = shift;

    return
	unless $resp->{result};

    print 'API Error ['.$resp->{result}.']: '.$resp->{error_string}."\n";
    exit(2);
}

1;
