require 'rubygems'
require 'soap/element'
require 'profiler'
require 'soap/rpc/driver'
require 'soap/processor'
require 'soap/streamHandler'
require 'soap/property'
require 'rexml/document'
require 'xml'

class BxSoapInterface
  attr_reader :nms
  
  def initialize(nms_id)
    @nms = NetworkManager.find(nms_id)
    @active_nms = nil
    @mutex = Mutex.new
    reload_model
  end  

  # Need to reload the nms data from the database everytime because of caching.
  # The nms data may have changed so need to check if it still exists and
  # the active IP is still associated with the nms
  def reload_model
    result = true
    @mutex.synchronize {        
      begin
        @nms.reload
        if @nms.network_management_servers.find_by_primary_ip(@active_nms) == nil
           @active_nms = nil
           if @nms.network_management_servers.first != nil
            @active_nms = @nms.network_management_servers.first.primary_ip
          end
        end
      rescue ActiveRecord::RecordNotFound
        # The nms object no longer exists! 
        result = false
      end
    }
    return result
  end
  
  def get_auth_token 

    auth_token = ""

    request, envelope = create_soap_header
    body = XML::Node.new('soap:Body')
    envelope << body
    auth = XML::Node.new('Authenticate')
    XML::Namespace.new(auth, nil, 'API/SOAP/Auth')
    body << auth
    uname = XML::Node.new('uname')
    XML::Attr.new(uname, 'xsi:type', 'xsd:string')
    uname.content = BxIfCommon::BX_USER
    auth << uname
    pword = XML::Node.new('pword')
    XML::Attr.new(pword, 'xsi:type', 'xsd:string')
    pword.content = BxIfCommon::BX_PASSWD
    auth << pword

    resp_string = send_request request.to_s, BxIfCommon::BX_SOAP_AUTH_URL

    doc = REXML::Document.new resp_string
    if !soap_fault(doc)
      auth_token = doc.elements["soap:Envelope/soap:Body/AuthenticateResponse/token"].get_text.value
      Rails.logger.info "Auth Token is: #{auth_token}"
    end
    
    return auth_token

  end


  def get_parameter_values(auth_token, msg)
    Rails.logger.info "Getting Parameter Values"

    filters = msg.get_filters
    Rails.logger.info "Params are: #{filters['sla_id']}"

    request, envelope = create_soap_header
    verb = encode_verb_header envelope,'API/SOAP/DataExtraction','getParameterValues',auth_token

    test_instance = XML::Node.new('test_instance')
    verb << test_instance
    sla_id = XML::Node.new('sla_id')
    XML::Attr.new(sla_id, 'xsi:type', 'xsd:string')
    sla_id.content = filters['sla_id']
    test_instance << sla_id
    test_information = XML::Node.new('test_information')
    test_instance << test_information
    test_name = XML::Node.new('module_name')
    XML::Attr.new(test_name, 'xsi:type', 'xsd:string')
    test_name.content = filters['test_name']
    test_information << test_name

    resp_string = send_request request.to_s, BxIfCommon::BX_SOAP_DE_URL

    return "ok", resp_string
  end
  
  def get_test_result_info(auth_token, msg)
    Rails.logger.info "get_test_result_info"
    
    filters = msg.get_filters
    Rails.logger.info "Params are: #{filters['sla_id']}"
    
    request, envelope = create_soap_header
    verb = encode_verb_header envelope,'API/SOAP/DataExtraction','getTestResultInfo',auth_token

    test_instance = XML::Node.new('test')
    verb << test_instance
    module_name = XML::Node.new('module_name')
    XML::Attr.new(module_name, 'xsi:type', 'xsd:string')
    module_name.content = filters['test_name']
    test_instance << module_name

    resp_string = send_request request.to_s, BxIfCommon::BX_SOAP_DE_URL

    result_def = {}
    
    context = XML::Parser::Context.string(resp_string)
    parser = XML::Parser.new(context)
    doc, statuses = parser.parse, []
    
    # Check for a soap fault
    if soap_fault(doc)    
      return result_def
    end
    
    # Get the /soap:Envelope/soap:Body/getTestResultInfoResponse to check for errors
    test_reposnse = {}
    doc.find('*/*')[0].each {|r| test_reposnse[r.name] = r.content}
    
    if test_reposnse['result'].to_i != 0
      Rails.logger.info "Error in soap reply #{test_reposnse['result']} #{test_reposnse['error_string']}"
      return result_def
    end
    
    # Get the result_defintion for the requested attributes
    z = doc.find('*/*/*/*/*/ResultDefinitionObject')
    z.each {|rd|
      if filters['result_list'].include?(rd.find('result_name').to_a[0].content)
        rd.find("enum_list").to_a[0].remove!  
        result_def[rd.find('result_name').to_a[0].content] = rd
      end  
    }

    return result_def
  end
  

  def get_test_instances(auth_token, msg)
    Rails.logger.info "Getting Test Insatnces"

    filters = msg.get_filters
    Rails.logger.info "Params are: #{filters['sla_id']}"
    request, envelope = create_soap_header
    verb = encode_verb_header envelope,'API/SOAP/DataExtraction','getTestInstances',auth_token
    filter = XML::Node.new('filter_object')
    verb << filter

    sla_id = XML::Node.new('sla_ids')
    XML::Attr.new(sla_id, 'xsi:type', 'xsd:string')
    sla_id.content = filters['sla_id']
    filter << sla_id
    service_instance_id = XML::Node.new('service_instance_ids')
    XML::Attr.new(service_instance_id, 'xsi:type', 'xsd:string')
    service_instance_id.content = filters['service_instance_id']
    filter << service_instance_id
    
    test = XML::Node.new('test')
    filter << test
    
    module_name = XML::Node.new('module_name')
    XML::Attr.new(module_name, 'xsi:type', 'xsd:string')
    module_name.content = filters['test_name']
    
    test << module_name
    resp_string = send_request request.to_s, BxIfCommon::BX_SOAP_DE_URL

    doc = REXML::Document.new resp_string
    data = []
    if !soap_fault(doc)
      result = doc.elements["soap:Envelope/soap:Body/getTestInstancesResponse/result"].get_text.value
      Rails.logger.info "Result: #{result}"

      if result == "0"
        doc.elements.each("soap:Envelope/soap:Body/getTestInstancesResponse/test_instances/TestInstanceObject") {
          |ti|
          h = {}
          ti.each_element_with_text{ |ei|
            h[ei.name] = ei.get_text.value
            Rails.logger.info "getTestInstancesResponse #{ei.name} => #{ei.get_text.value}"
          }
          data << h
        }
      end
    end

    #p data
    result_string = ''
    result_string +=  "Records found: #{data.size}"
    Rails.logger.info "get_test_instances Records found: #{data.size}"
    return result_string, data

  end

  def get_test_data(auth_token, qt, msg, result_def)
    
    filters = msg.get_filters

    request, envelope = create_soap_header
    verb = encode_verb_header envelope,'API/SOAP/DataExtraction','getTestData',auth_token
    if qt
      query_token = XML::Node.new('query_token')
      XML::Attr.new(query_token, 'xsi:type', 'xsd:string')
      query_token.content = qt
      verb << query_token
    else
      filter = XML::Node.new('filter_object')
      verb << filter
      test = XML::Node.new('test')
      filter << test
      test_name = XML::Node.new('module_name')
      XML::Attr.new(test_name, 'xsi:type', 'xsd:string')
      test_name.content = filters['test_name']
      test << test_name
      start_time = XML::Node.new('start_time')
      XML::Attr.new(start_time, 'xsi:type', 'xsd:string')
      start_time.content = filters['start_time']
      filter << start_time
      end_time = XML::Node.new('end_time')
      XML::Attr.new(end_time, 'xsi:type', 'xsd:string')
      end_time.content = filters['end_time']
      filter << end_time
      sla_ids = XML::Node.new('sla_ids')
      XML::Attr.new(sla_ids, 'xsi:type', 'xsd:string')
      sla_ids.content = filters['sla_id']
      filter << sla_ids
      test_instance_class_ids = XML::Node.new('test_instance_class_ids')
      XML::Attr.new(test_instance_class_ids, 'xsi:type', 'xsd:string')
      test_instance_class_ids.content = filters['test_instance_class_id']
      filter << test_instance_class_ids

      test_result_list = XML::Node.new('test_result_list')

      verb << test_result_list
      result_def.each_value {|kk| test_result_list << request.import(kk) }

      rows = XML::Node.new('max_rows')
      XML::Attr.new(rows, 'xsi:type', 'xsd:int')
      rows.content = "2000"
      verb << rows
    end

    resp_string = send_request request.to_s, BxIfCommon::BX_SOAP_DE_URL
    #File.open("/Users/bwessels/brix_data_#{Time.now}", 'w') {|f| f.write(resp_string) }

    t = Time.now
    context = XML::Parser::Context.string(resp_string)
    parser = XML::Parser.new(context)
    doc, statuses = parser.parse, []
    
    # Check for a soap fault
    if soap_fault(doc)    
      continue = 0
      data = []
      query_token = nil
      return continue, qt, data
    end
    
    # Get the /soap:Envelope/soap:Body/getTestDataResponse
    test_reposnse = {}
    doc.find('*/*')[0].each {|r| test_reposnse[r.name] = r.content}
    
    if test_reposnse['result'].to_i != 0
      Rails.logger.info "Error in soap reply #{test_reposnse['result']} #{test_reposnse['error_string']}"
      continue = 0
      data = []
      return continue, qt, data
    end
    
    query_token = test_reposnse['query_token']
    Rails.logger.info "query_token #{query_token}"

    more_data = test_reposnse['more_data']
    Rails.logger.info "more_data #{more_data}"

    continue = more_data.to_i
    Rails.logger.info "continue #{continue}"
    
    data = []
    test_data_objects = doc.find('*/*/*/TestDataObject')
    test_data_objects.each do |test_data_object|    
      h = {}
      h["test_instance_class_id"] = test_data_object.find('test_instance_id').to_a[0].content
      test_data_object.find("*/ResultDataObject").to_a.each {|p| 
        result_name = p.find("result_name").to_a[0].content
        result_value = p.find("result_value").to_a[0].content
        if result_name == 'Time Stamp'
          h[result_name] = (result_value.to_i/(1000*1000)).to_s
        else
          h[result_name] = result_value
        end 
      }
      data << [h]
    end
    
    Rails.logger.debug "TIMING XML doc processing #{Time.now - t}"
    Rails.logger.info "get_test_data Records found: #{data.size}"
    return continue, query_token, data

  end


  #-----------------------------
  # Utilities APIs
  #-----------------------------

  def get_sla_id(auth_token, msg)
    filters = msg.get_filters
    request, envelope = create_soap_header
    verb = encode_verb_header envelope,'API/SOAP/Utils','getSLAS',auth_token
  	sla_names = XML::Node.new('sla_names')
    XML::Attr.new(sla_names, 'soapenc:arrayType', 'xsd:string[1]')
    XML::Attr.new(sla_names, 'xsi:type', 'soapenc:Array')
    verb << sla_names
    item = XML::Node.new('item')
    XML::Attr.new(item, 'xsi:type', 'xsd:string')
    item.content = filters['sla_name']
    sla_names << item
    sla_ids = XML::Node.new('sla_ids')
    XML::Attr.new(sla_ids, 'xsi:nil', 'true')
    verb << sla_ids
    sla_match = XML::Node.new('sla_match')
    XML::Attr.new(sla_match, 'xsi:nil', 'true')
    verb << sla_match
    case_sensitive = XML::Node.new('case_sensitive')
    XML::Attr.new(case_sensitive, 'xsi:type', 'xsd:int')
    case_sensitive.content = '1'
    verb << case_sensitive

    resp_string = send_request request.to_s, BxIfCommon::BX_SOAP_UTIL_URL

    doc = REXML::Document.new resp_string
    data = []
    if !soap_fault(doc)
      result = doc.elements["soap:Envelope/soap:Body/getSLASResponse/result"].get_text.value
      Rails.logger.info "Result: #{result}"

      if result == "0"
        h = {}
        sla_info = "soap:Envelope/soap:Body/getSLASResponse/sla_list/SLAObject"

        sla_id = doc.elements["#{sla_info}/sla_id"].get_text.value
        h["sla_id"] = sla_id

        pending = doc.elements["#{sla_info}/pending"].get_text.value
        h["pending"] = pending

        active = doc.elements["#{sla_info}/active"].get_text.value
        h["active"] = active

        shared = doc.elements["#{sla_info}/shared"].get_text.value
        h["shared"] = shared

        data << h
      end
    end
    result_string = ''
    result_string +=  "Records found: #{data.size}"
    Rails.logger.info "get_sla_id Records found: #{data.size}"
    return result_string, data

  end

  def get_service_instances(auth_token, msg)
    filters = msg.get_filters
    request, envelope = create_soap_header
    verb = encode_verb_header envelope,'API/SOAP/Utils','getServiceInstances',auth_token

    sla_id = XML::Node.new('sla_id')
    XML::Attr.new(sla_id, 'xsi:type', 'xsd:int')
    sla_id.content = filters['sla_id']
    verb << sla_id
    service_id = XML::Node.new('service_id')
    XML::Attr.new(service_id, 'xsi:nil', 'true')
    verb << service_id
    case_sensitive = XML::Node.new('case_sensitive')
    XML::Attr.new(case_sensitive, 'xsi:type', 'xsd:int')
    case_sensitive.content = '1'
    verb << case_sensitive

    resp_string = send_request request.to_s, BxIfCommon::BX_SOAP_UTIL_URL

    doc = REXML::Document.new resp_string
    data = []
    if !soap_fault(doc)    
      result = doc.elements["soap:Envelope/soap:Body/getServiceInstancesResponse/result"].get_text.value
      Rails.logger.info "Result: #{result}"

      if result == "0"
        doc.elements.each("soap:Envelope/soap:Body/getServiceInstancesResponse/service_instance_list/ServiceInstanceObject") {|si|
          h = {}
          si.each_element_with_text{ |ei|
            h[ei.name] = ei.get_text.value
            Rails.logger.info "getServiceInstancesResponse: #{ei.name} => #{ei.get_text.value}"
          }
          data << h
        }
      end
    end

    result_string = ''
    result_string +=  "Records found: #{data.size}"
    Rails.logger.info "get_service_instances Records found: #{data.size}"
    return result_string, data
  end
  
  private
  
  def send_request(data,url)
    t = Time.now
    Rails.logger.info "SENDING to #{url} #{t.strftime("%Y-%m-%d %H:%M:%S")}.#{t.usec}}:"
    Rails.logger.info data
    
    current_nms = @active_nms
    # No server can be found exit
    raise if current_nms == nil
    
    attempts = 1
    begin
      stream = SOAP::HTTPStreamHandler.new(SOAP::Property.new)
      stream.client.connect_timeout=30 #10
      stream.client.send_timeout=60 #20
      stream.client.receive_timeout=60 #10 # For each 16K block

      request = SOAP::StreamHandler::ConnectionData.new(data)
      #'http://172.16.24.8:8080'
      
      bx_url = "http://#{current_nms}:8080/#{url}"
      resp_data = stream.send(bx_url, request)
    rescue Exception => e
      if attempts >= @nms.network_management_servers.size * 2
        raise # Tried all servers twice - fail
      else 
        # Try the other server 
        Rails.logger.info "Failed to connect #{current_nms}"
        attempts = attempts + 1
        server = @nms.network_management_servers.find_by_primary_ip(current_nms)
        server = @nms.network_management_servers[@nms.network_management_servers.index(server)+1]
        server = @nms.network_management_servers.first if server == nil
        if server == nil
          current_nms = nil
        else
          current_nms = server.primary_ip
        end
        Rails.logger.info "Attempting #{current_nms}"
        retry
      end
    end
    # Managed to talk to the server - now check if it is the same server as the active_nms
    # if it's not then update the active_nms
    # Doing this allows for multiple threads as each thread will only update the active_nms
    # after it's figured out which one is working
    @mutex.synchronize {    
      @active_nms = current_nms if current_nms != @active_nms
    }

    resp_string = resp_data.receive_string
    t = Time.now - t
    Rails.logger.info "TIMING Got response: #{t}"
    #Rails.logger.info resp_string
    Rails.logger.info "Response size: #{resp_string.size}"

    return resp_string
  end
  
  def create_soap_header
    request = XML::Document.new()
    envelope = XML::Node.new('soap:Envelope')
    XML::Namespace.new(envelope, 'xsi', 'http://www.w3.org/2001/XMLSchema-instance')
    XML::Namespace.new(envelope, 'soapenc', 'http://schemas.xmlsoap.org/soap/encoding/')
    XML::Namespace.new(envelope, 'xsd', 'http://www.w3.org/2001/XMLSchema')
    XML::Namespace.new(envelope, 'soap', 'http://schemas.xmlsoap.org/soap/envelope/')
    XML::Namespace.new(envelope,  'namesp1', 'http://namespaces.soaplite.com/perl')
    XML::Attr.new(envelope, 'soap:encodingStyle', 'http://schemas.xmlsoap.org/soap/encoding/')

    request.root = envelope

    return request, envelope
  end

  def encode_verb_header(envelope, verb_path, verb_name, auth_token)
    body = XML::Node.new('soap:Body')
    envelope << body
    verb = XML::Node.new("#{verb_name}")
    XML::Namespace.new(verb, nil, "#{verb_path}")
    body << verb
    authority = XML::Node.new('authority')
    XML::Attr.new(authority, 'xsi:type', 'xsd:string')
    authority.content = auth_token
    verb << authority
    return verb
  end
  
  def soap_fault(doc)
    # doc can be either a REXML or a LIBXML document
    if doc.is_a?(REXML::Document)
      fault = doc.elements["soap:Envelope/soap:Body/soap:Fault"]
      fault_string = fault.elements["faultstring"].get_text.value if fault != nil
    else
      fault = doc.find("/soap:Envelope/soap:Body/soap:Fault").to_a[0]
      fault_string = doc.find("/soap:Envelope/soap:Body/soap:Fault/faultstring").to_a[0].content if fault != nil
    end
    
    if fault == nil
      return false
    else
      SW_ERR "Soap Fault (#{fault_string})"
      return true
    end
  end
  

end