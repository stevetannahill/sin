
-- (c) 2000-2007 by Brix Networks, Inc. All rights reserved.
-- Unauthorized reproduction, distribution, transmission,
-- republication, display or performance is strictly 
-- prohibited.
--

--
--  MIB for Textual Conventions for the Brix System for End-to-End
--  IP Service Level Verification.
--

BRIX-TC-MIB DEFINITIONS ::= BEGIN

IMPORTS
    MODULE-IDENTITY,  
    Integer32, Counter32, Unsigned32 
        FROM SNMPv2-SMI
    TEXTUAL-CONVENTION
        FROM SNMPv2-TC
    brixGeneric
        FROM BRIX-GLOBAL-REG
    ;

brixTextualConventionsMIB MODULE-IDENTITY
    LAST-UPDATED "200605011500Z"  -- May 1, 2006
    ORGANIZATION "Brix Networks, Inc."
    CONTACT-INFO 
        "
         Brix Networks, Inc.
         285 Mill Road
         Chelmsford, MA  01824
         phone: 1-888-BRIXNET (1-888-274-9638)
         email: support@brixnet.com"

    DESCRIPTION
        "This MIB contains textual conventions for 
        Brix Networks Products."
    ::= { brixGeneric 1 } 

--********************************************************************
-- Brix Networks Textual Conventions
--********************************************************************
--

BrixVerifierUId ::= TEXTUAL-CONVENTION
   DISPLAY-HINT "12a"
   STATUS    current
   DESCRIPTION
      "This is the Brix Verifier's Unique Identifier.
      Each Verifier has a unique identifier which is
      a 12 byte octet string in human-readable form.

      To facilitate internationalization, this 
      information is represented using the ISO/IEC
      IS 10646-1 character set, encoded as an octet
      string using the UTF-8 transformation format
      described in [RFC2279].

      For information encoded in 7-bit US-ASCII,
      the UTF-8 encoding is identical to the
      US-ASCII encoding."
   SYNTAX  OCTET STRING (SIZE(12)) 

BrixWorxServerRoleType ::= TEXTUAL-CONVENTION
   STATUS    current
   DESCRIPTION
       "This textual convention represents the types of 
       roles that the BrixWorx Server performs.  The 
       BrixWorx Server could have one or more of the 
       following roles for each Host that it serves."
   SYNTAX  INTEGER 
      {
         other(1),        -- none of the following 
         universal(2),    -- role type is a Universal Registry 
         network(3),      -- role type is a Network Registry
         local(4),        -- role type is a Local Registry 
         consolidator(5), -- role type is a Consolidator
         collector(6)     -- role type is a Collector 
      }

BrixWorxServerComponentType ::= TEXTUAL-CONVENTION
    STATUS    current
    DESCRIPTION
       "This textual convention represents the types of 
       components of the BrixWorx Server."
    SYNTAX  INTEGER 
      {
         other(1),        -- none of the following
         bxauditor(2),    -- component type is the auditor
         bxcollmon(3),    -- component type is the collection monitor
         bxpreflight(4),  -- component type is the preflight manager
         bxqmgr(5),       -- component type is the queue manager
         bxwebagent(6),   -- component type is the web agent
         bxsnmpdbsa(7),   -- component type is the SNMP db subagent
         bxsaamgr(8)      -- component type is the SA agent
      }

END


