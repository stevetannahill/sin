module BxIfCommon

  BX_SOAP_AUTH_URL = '/API/SOAP/Auth'
  BX_SOAP_DE_URL = '/API/SOAP/DataExtraction'
  BX_SOAP_UTIL_URL = '/API/SOAP/Utils'
  BX_IF_SERVER_IP = 'localhost'
  BX_IF_SERVER_PORT = 2010

  BX_USER = "administrator"
  BX_PASSWD = "admin"

  class BxIfMsg
    def initialize(nms_id, command, filters)
      @h = {}
      @h['nms_id'] = nms_id
      @h['command'] = command
      @h['filters'] = filters
    end

    def get_command
      return @h['command']
    end
    

    def get_filters
      return @h['filters']
    end

  end #class BxIfMsg

end
