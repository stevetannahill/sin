app_base = File.join(File.dirname(__FILE__), "../../")
$: << File.expand_path(File.dirname(__FILE__))
#require 'CSV'
require 'socket'
require 'logger'
require "bx_if_common"
require "bx_soap_interface"
require "#{Rails.root}/lib/tools/sw_err"

#gem 'libxml-ruby', '>= 0.8.3'
#require 'xml'

# set up logging
MAX_LOG_SIZE = 10*1024*1024
class FormatLogger < Logger
  def format_message(severity, timestamp, progname, msg)
    "#{timestamp.to_s} #{severity} #{Thread.current} #{Thread.current["nms_name"]} #{msg}\n"
  end
end

def respond_to_client client, data
  serialized_data = ""
  serialized_data = Marshal.dump(data)
  serialized_size = sprintf "%08d", serialized_data.size
  Rails.logger.info "Reply size: #{serialized_size}"
  client.write serialized_size
  client.write serialized_data
end

# Main begins
logger = FormatLogger.new("#{Rails.root}/log/#{File.basename(__FILE__, '.*')}.log", 1, MAX_LOG_SIZE)
Rails.logger = logger # Give logger access to all ruby objects
Rails.logger.level = (Rails.env == "development") ? Logger::Severity::DEBUG : Logger::Severity::INFO

# Don't log SQL queries
ActiveRecord::Base.logger = nil  
ActiveRecord::Base.clear_active_connections!

# The sam_soap_interfaces is added to when a new request comes in with an unknown nms id
# This is done in different threads so make sure the creation is protected
brix_soap_mutex = Mutex.new
brix_soap_interfaces = Hash.new {|hash,key|
  brix_soap_mutex.synchronize {
    begin
      if NetworkManager.find(key).nm_type == "BrixWorx"
        hash[key] = BxSoapInterface.new(key)
      else
        SW_ERR "NetworkManager with id #{key} id is not a Brix NMS it is a #{NetworkManager.find(key).nm_type}"
      end
    rescue ActiveRecord::RecordNotFound
      SW_ERR "Cannot find NMS with id #{key}"
    end
  }
}

@thread_count = 0
server = TCPServer.open(BxIfCommon::BX_IF_SERVER_PORT)
Rails.logger.info "Listening for TCP clients on port #{BxIfCommon::BX_IF_SERVER_PORT}"

keep_going = true
while (keep_going)
  
  Rails.logger.info "Create new thread..."
  @thread_count += 1
  client_socket = server.accept
  
  # For each message ensure the connection to the database is up
  ActiveRecord::Base.verify_active_connections!  
  
  # Remove any network managers that no longer exist 
  brix_soap_mutex.synchronize {
    all_nms = NetworkManager.find_all_by_nm_type("BrixWorx").collect{|nms| nms.id} 
    brix_soap_interfaces.delete_if {|key, value| !all_nms.include?(key)}
  }
  
  t = Thread.start(client_socket) do |client|
    loop {
      begin
        Rails.logger.info "Thread: Waiting for client msg..."

        s = client.recvfrom(8)[0]
        break if s.empty?
        result_str = ""
        raw_data = ""
        while raw_data.size < s.to_i
          raw_data += client.recvfrom( 8000 )[0]
        end

        msg = Marshal.load(raw_data)
      
        # Find (and possiblly create - if it's not been seen before) the sam soap interface based on
        # the nms id. If it's not found exit the thread
        soap_if = brix_soap_interfaces[msg.get_nms_id]
        break if soap_if == nil

        if !soap_if.reload_model
          SW_ERR "Failed to reload the model for #{soap_if.nms} it's been deleted"
           brix_soap_mutex.synchronize { brix_soap_interfaces.delete(soap_if) }
          break
        end
      
        # Set a Thread variable to the Brix name so it can be included in the logs
        Thread.current["nms_name"] = soap_if.nms.name.gsub(/\s/,'_')
      
        command = msg.get_command
        Rails.logger.info "Got command: #{command}"
        case command
        when "get_parameter_values"
          result, data = soap_if.get_parameter_values(soap_if.get_auth_token, msg)
          respond_to_client(client, data)
        when "get_test_instances"
          result, data = soap_if.get_test_instances(soap_if.get_auth_token, msg)
          respond_to_client(client, data)
        when "get_test_data"
          auth_token = soap_if.get_auth_token
          result_def = soap_if.get_test_result_info(auth_token, msg)
          
          query_token = nil
          data = []
          continue = true
          count = 1

          while continue != 0
            continue, query_token, d = soap_if.get_test_data(auth_token, query_token, msg, result_def)
            data = data + d if !d.empty?
            count += 1
            # Get the auth token again incase the get_test_data was > 30 seconds
            auth_token = soap_if.get_auth_token if continue != 0 
            Rails.logger.info "**** #{count} #{data.size}"                        
          end
          
          raw_size = data.size
#          CSV.open("/Users/bwessels/brix_data.csv", "wb") do |csv|
#            csv << data[0][0].keys
#            data.each {|row| csv << row[0].values}
#          end
            
          data.uniq!
          if raw_size != data.size
            SW_ERR "Duplicate records found in Brix data raw size #{raw_size} uniq size #{data.size}"
          end         
          data.sort! {|a,b| a[0]["Time Stamp"].to_i <=> b[0]["Time Stamp"].to_i}
          
          respond_to_client(client, data)
        when "get_sla_id"
          result, data = soap_if.get_sla_id(soap_if.get_auth_token, msg)
          respond_to_client(client, data)
        when "get_service_instances"
          result, data = soap_if.get_service_instances(soap_if.get_auth_token, msg)
          respond_to_client(client, data)
        else
          SW_ERR "Bad Command: #{command}"
        end
      rescue Exception => e
        Rails.logger.info "Exception in thread #{Thread.current} #{@thread_count} #{e.message} #{e.backtrace.join("\n")}"
        client.close
        raise # re-raise which will exit the thread
      end
    }
    # At the end of the thread always close the socket
    client.close
  end
end
Rails.logger.info "Server Shutting down port #{BxIfCommon::BX_IF_SERVER_PORT}"
