require 'socket'      # Sockets are in standard library
require File.dirname(__FILE__) + "/bx_api"

api = BxIf::StatsApi.new

while 1
  puts "Enter a command for the server:"
  command = gets.chomp
  result = ""

  if command == 'q'
    break
  elsif command == 'p'
    result, data = api.get_parameter_values("1125", "com.brixnet.swv.ethframedelaytest.1.333")
  else
    break
  end

  puts "Server said:"
  puts "#{result}"
  puts "Size: #{result.size}"
end 

