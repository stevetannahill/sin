namespace :reindex do
  desc "Check and do if a reindex is needed"
  task :check => :environment do
    mr = ReindexSphinx.master_record
    if mr.scheduled and mr.finished_at
      puts "Reindex needed, running it now..."
      mr.update_attributes(:scheduled => false, :started_at => Time.now, :finished_at => nil)
      Rake::Task['thinking_sphinx:reindex'].invoke
      mr.update_attributes(:finished_at => Time.now)
    else
      puts "Doesn't need a reindex..."
    end
  end
end
