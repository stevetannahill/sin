desc "Check the JavaScript source with JSLint - exit with status 1 if any of the files fail."
task :jslint do
  failed_files = []
  classpath = File.join(Rails.root, "vendor", "js.jar")
  jslint_path = File.join(Rails.root, "vendor", "fulljslint.js")
  
  puts 'This does not work yet, it needs to generate the javascript for the application from a rake task'
  
  Dir['public/**/*.js'].each do |fname|
    cmd = "java -cp #{classpath} org.mozilla.javascript.tools.shell.Main #{jslint_path} #{fname}"
    results = %x{#{cmd}}
    unless results =~ /^jslint: No problems found in/
      puts "#{fname}:"
      puts results
      failed_files << fname
    end
  end
  if failed_files.size > 0
    exit 1
  end
end
