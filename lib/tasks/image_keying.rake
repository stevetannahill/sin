namespace :image_keying do
  desc "Adds logo_urls to all service_providers (only if required) and lists the keys"
  task :create_and_list => :environment do
    ServiceProvider.all.each do |provider|
      if !provider.website.blank?
        puts "Key: #{ImageKeying.key(provider)}, Provider: #{provider.name}, Website: #{provider.website}"
      else
        puts "Skipped #{provider.name}, needs a website to be defined"
      end
    end
  end
end
