namespace :db do
  desc "Erase and populate the database with yml files in extracted_fixtures"
  task :import_extracted => :environment do
    require 'active_record/fixtures'
    
    fixtures_path = File.join(Rails.root,'db','extracted_fixtures')
    fixtures = Dir.new(fixtures_path).find_all{|f| f.end_with?(".yml")}.map{|f| File.basename(f,'.yml')}
    Fixtures.create_fixtures(fixtures_path,fixtures)
  end
end