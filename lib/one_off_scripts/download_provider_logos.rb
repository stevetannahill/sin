require 'google_image_api'
require "open-uri"
require 'chunky_png'
require 'ipaddr'
require '../../config/environment.rb'

SP_DIR = 'service_providers'
LOGO_WIDTH = 150.0
LOGO_HEIGHT = 37.0

# search for provider logos
@service_providers = ServiceProvider.where(:logo_url => nil)

@service_providers.each do |sp|
  # generate random 32 char string for logo_url
  chars = [('a'..'z'),(1..9)].map{|i| i.to_a}.flatten
  file_name = (0...32).map{ chars[rand(chars.length)] }.join

  begin
    results = GoogleImageApi.find("#{sp.name.gsub(/(\(.*\))|(,.*$)|(LLC$)|(INC$)/, '')} logo", {:userip => "#{IPAddr.new(rand(2**32),Socket::AF_INET)}"})

    if results
      results.images.each do |image|
        begin
          img = open(image['url']).read
          File.open("#{SP_DIR}/#{file_name}#{File.extname(image['url'])}", 'wb') do |fo|
            fo.write(img)
            sp.update_attributes(:logo_url => file_name)
          end
          break
        rescue Exception
        end
      end
    else
      raise Exception
    end
  rescue Exception
    puts "Error: Could not save logo for #{sp.name} - #{sp.id}"
  end
  sleep rand(60)
end

# @service_providers = ServiceProvider.all

# @service_providers.each do |sp|
#   logo = "#{SP_DIR}/#{sp.logo_url}.png"
#   if File.exist?("#{SP_DIR}/#{sp.logo_url}.png")

#     # resize logo to correct dimensions
#     # Dir.glob("#{SP_DIR}/*.png") do |logo|
#       begin
#         image = ChunkyPNG::Image.from_file(logo)
#         canvas = ChunkyPNG::Image.new(LOGO_WIDTH.floor, LOGO_HEIGHT.floor, ChunkyPNG::Color('white'))

#         width_ratio = LOGO_WIDTH/image.width
#         height_ratio = LOGO_HEIGHT/image.height

#         # resize images
#         if width_ratio > height_ratio
#           image = image.resize((image.width*height_ratio).floor, (image.height*height_ratio).floor)
#         elsif height_ratio > width_ratio
#           image = image.resize((image.width*width_ratio).floor, (image.height*width_ratio).floor)
#         else
#           image = image.resize(LOGO_HEIGHT.floor, LOGO_HEIGHT.floor)
#         end

#         # place the image in center of the canvas
#         canvas.compose!(image, ((LOGO_WIDTH/2)-(image.width/2)).floor, ((LOGO_HEIGHT/2)-(image.height/2)).floor)
#         canvas.save(logo, :fast_rgba)
#       rescue ChunkyPNG::Exception
#         puts "Issue with #{logo}"
#       end
#       puts "{:names => ['#{sp.name}'], :logo_url => '#{sp.logo_url}'},"
#     # end
#   end
# end

# generate output for populate_provider_logos
# {:names => ['CenturyLink'], :logo_url => 'bd258d522390316c2eb70c5d08a94113'},