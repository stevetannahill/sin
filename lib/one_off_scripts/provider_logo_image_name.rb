website = ARGV[0]
if website.blank?
  puts "Please supply a website address"
else
  require('image_keying')
  puts ImageKeying.name(website)
end
