require 'benchmark'

error_count = 0
ok_count = 0

Path.find(:all).each do |e| 

  time = Benchmark.realtime do
    if e.layout_diagram 
      puts "√ Diagram generated for #{e.id} (CENX ID: #{e.cenx_id})" 
      ok_count += 1
    else 
      "X Problem with #{e.id} (CENX ID: #{e.cenx_id})"
      error_count += 1
    end
  end
  
  puts time
  
end

puts '-' * 50
puts "SUCCESSFUL: #{ok_count}"
puts "ERRORS: #{error_count}"
