class RangeDate
  
  DATE_FORMAT = '%Y-%m-%d'

  # date is the format of 
  def initialize(date, default=:none)
    now = Time.now.in_time_zone
    defaults = {:today => now.strftime(DATE_FORMAT),
                :now => now.strftime(DATE_FORMAT),
                :week => now.beginning_of_week.strftime(DATE_FORMAT),
                :week_previous => (now - 7.days).beginning_of_week.strftime(DATE_FORMAT),
                :week_previous_end => (now - 7.days).end_of_week.strftime(DATE_FORMAT),
                :month => now.beginning_of_month.strftime(DATE_FORMAT),
                :month_end => (now.end_of_month).strftime(DATE_FORMAT),
                :month_previous => (now.prev_month.beginning_of_month).strftime(DATE_FORMAT),
                :month_previous_end => (now.prev_month.end_of_month).strftime(DATE_FORMAT),
                :three_months_previous => (now - 3.months).beginning_of_month.strftime(DATE_FORMAT),
                :six_months_previous => (now - 6.months).beginning_of_month.strftime(DATE_FORMAT),
                :nine_months_previous => (now - 9.months).beginning_of_month.strftime(DATE_FORMAT),
                :twelve_months_previous => (now - 12.months).beginning_of_month.strftime(DATE_FORMAT),
                :quarter => now.beginning_of_quarter.strftime(DATE_FORMAT),
                :year => now.beginning_of_year.strftime(DATE_FORMAT),

                # Hack start and end dates for demos
                :hack_start => Time.new(2012, 10, 1).strftime(DATE_FORMAT),
                :hack_end => Time.new(2012, 10, 3).strftime(DATE_FORMAT),
                
                :none => nil}

    @value = if date.blank? 
      defaults[default] 
    else 
      date.is_a?(String) ? date : date.strftime(DATE_FORMAT)
    end
  end
  
  # String representation of date
  def to_s
    @value ? @value : ''
  end
  
  def to_i
    DateTime.strptime(@value, DATE_FORMAT).beginning_of_day.in_time_zone.to_i unless @value.blank?
  end
  
  def to_ms
    to_i * 1000 unless @value.blank?
  end
  
  def to_long
    DateTime.strptime(@value, DATE_FORMAT).in_time_zone.to_formatted_s(:long) unless @value.blank?
  end
  
  def to_datetime
    DateTime.strptime(@value, DATE_FORMAT)
  end
end

class RangeDateStart < RangeDate
end

class RangeDateEnd < RangeDate
  def to_i
    DateTime.strptime(@value, DATE_FORMAT).end_of_day.in_time_zone.to_i unless @value.blank?
  end
end
