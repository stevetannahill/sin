// tipsy, facebook style tooltips for jquery
// version 1.0.0a
// (c) 2008-2010 jason frame [jason@onehackoranother.com]
// released under the MIT license

(function($) {
    
    function Tipsy(element, options) {
        this.$element = $(element);
        this.options = options;
        this.enabled = true;
        this.fixTitle();
    }
    
    Tipsy.prototype = {
        show: function() {
            var title = this.getTitle();
            if (title && this.enabled) {
                var $tip = this.tip();
                
                $tip.find('.tipsy-inner')[this.options.html ? 'html' : 'text'](title);
                $tip[0].className = 'tipsy'; // reset classname in case of dynamic gravity
                $tip.remove().css({top: 0, left: 0, visibility: 'hidden', display: 'block'}).appendTo(document.body);

                // HACK: Added by Shane call before function before size of tooltip is calculated
                if (this.options.before) {
                  this.options.before.call(this.$element[0]);
                }

                this.actualWidth = $tip[0].offsetWidth, this.actualHeight = $tip[0].offsetHeight;
                // HACK: wrote my own gravity function, tried to write $.fn.tipsy.autoNSWE, but couldn't access to $tip (actual tipsy box) so I hacked the code here
                // var gravity = (typeof this.options.gravity == 'function')
                //                 ? this.options.gravity.call(this.$element[0])
                //                 : this.options.gravity;

                var elem = $(this.$element[0]);
                var room_above = elem.offset().top - $(document).scrollTop();
                var center_of_element = elem.offset().left + (elem.width() / 2);

                this.gravity = this.options.gravity ? this.options.gravity : elem.attr('data-popup_direction');
                this.start_of_window = $(document).scrollLeft();
                this.width_of_window = $(window).width();
                this.end_of_window = this.start_of_window + this.width_of_window;
                this.center_of_window = this.end_of_window / 2;

                // special gravity setting determine by space available on screen
                if (this.gravity == 'nswe') {
                  this.gravity = (room_above > this.actualHeight ? 's' : 'n') 
                    
                  // If on right side of screen and will fit left the display left (east)
                  if (center_of_element > this.center_of_window) {
                    this.gravity += (center_of_element - this.actualWidth > this.start_of_window) ? 'e' : 'r';
                  // Never want to go out past left side of screen, no scroll bars and left side of box will not be viewable
                  } else {
                    this.gravity += (center_of_element + this.actualWidth < this.end_of_window) ? 'w' : 'l';
                  }
                }

                if (this.gravity == 'nwe') {
  
                  // default to centered
                  this.gravity = 'n';

                  // Won't fit centered so try left (east) or right (west)
                  if (!(center_of_element + (this.actualWidth / 2) < this.end_of_window && center_of_element - (this.actualWidth / 2) > this.start_of_window)) {
                    // Edge case with bubble is bigger than window, start at far left and stretch out to right
                    if (this.actualWidth > this.width_of_window) {
                      this.gravity = 'nl';
                    // If on left side of screen then display out to right (west)
                    // Will get scroll bars if out past end of screen but that is alright
                    } else {
                      // If on right side of screen and will fit left then display left (east)
                      if (center_of_element > this.center_of_window) {
                        this.gravity = (center_of_element - this.actualWidth > this.start_of_window) ? 'ne' : 'nr';
                      // Never want to go out past left side of screen, no scroll bars and left side of box will not be viewable
                      } else {
                        this.gravity = (center_of_element + this.actualWidth < this.end_of_window) ? 'nw' : 'nl';
                      }
                    }
                  }
                }

                // position bubble
                this.position();
 
                if (this.options.fade) {
                    $tip.stop().css({opacity: 0, display: 'block', visibility: 'visible'}).animate({opacity: this.options.opacity});
                } else {
                    $tip.css({visibility: 'visible', opacity: this.options.opacity});
                }

                $tip.addClass('tipsy-' + this.gravity);
                
                // HACK: Added by Shane call after function
                if (this.options.after) {
                  this.options.after.call(this.$element[0], $tip);
                }
            }
        },
        
        position: function($tip) {
        
          var $tip = this.tip(), 
              tipsy_arrow = $tip.find('.tipsy-arrow'),
              elem = $(this.$element[0]),
              center_of_element = elem.offset().left + (elem.width() / 2);

          var tp;

          var pos = $.extend({}, this.$element.offset(), {
              width: this.$element[0].offsetWidth,
              height: this.$element[0].offsetHeight
          });
          
          // Tipsy bubbles are reused so make sure we clear the left attribute of the arrow incase it was set explicitly 
          tipsy_arrow.css({left: ''});

          switch (this.gravity.charAt(0)) {
              case 'n':
                  tp = {top: pos.top + pos.height + this.options.offset, left: pos.left + pos.width / 2 - this.actualWidth / 2 - 5};
                  break;
              case 's':
                  tp = {top: pos.top - this.actualHeight - this.options.offset, left: pos.left + pos.width / 2 - this.actualWidth / 2};
                  break;
              case 'e':
                  tp = {top: pos.top + pos.height / 2 - this.actualHeight / 2, left: pos.left - this.actualWidth - this.options.offset};
                  break;
              case 'w':
                  tp = {top: pos.top + pos.height / 2 - this.actualHeight / 2, left: pos.left + pos.width + this.options.offset};
                  break;
          }

          var arrow_can_move = false;

          if (this.gravity.length == 2) {
            switch (this.gravity.charAt(1)) {
              case 'w':
                tp.left = pos.left + pos.width / 2 - 35;
                break;
              case 'e':
                tp.left = pos.left + pos.width / 2 - this.actualWidth + 35;
                break;
              case 'l':
                tp.left = this.start_of_window - 13;
                tipsy_arrow.css({left: center_of_element});
                arrow_can_move = true;
                break;
              case 'r':
                tp.left = this.end_of_window - this.actualWidth;
                tipsy_arrow.css({left: center_of_element - tp.left - 13});
                arrow_can_move = true;
                break;
            }
          }
          
          $tip.css(tp);
          
          if (arrow_can_move) {
            var new_gravity,
                arrow_left = tipsy_arrow.position().left;
                
            // is arrow at edge of bubble, if so then set a new gravity
            if (arrow_left < 20) {
              new_gravity = 'nw';
            } else if (arrow_left > (this.actualWidth - 50)) {
              new_gravity = 'ne';
            }
            
            if (new_gravity != undefined) {
              // Set new gravity
              $tip.removeClass('tipsy-nr tipsy-nl').addClass('tipsy-' + new_gravity);
              this.gravity = new_gravity;
              
              // reposition bubble using new gravity
              this.position();
            }
          }


                                
        },
        
        hide: function() {
            if (this.options.fade) {
                this.tip().stop().fadeOut(function() { $(this).remove(); });
            } else {
                this.tip().remove();
            }
        },
        
        fixTitle: function() {
            var $e = this.$element;
            if ($e.attr('title') || typeof($e.attr('original-title')) != 'string') {
                $e.attr('original-title', $e.attr('title') || '').removeAttr('title');
            }
        },
        
        getTitle: function() {
            var title, $e = this.$element, o = this.options;
            this.fixTitle();
            var title, o = this.options;
            if (typeof o.title == 'string') {
                title = $e.attr(o.title == 'title' ? 'original-title' : o.title);
            } else if (typeof o.title == 'function') {
                title = o.title.call($e[0]);
            }
            title = ('' + title).replace(/(^\s*|\s*$)/, "");
            return title || o.fallback;
        },
        
        tip: function() {
            if (!this.$tip) {
                this.$tip = $('<div class="tipsy"></div>').html('<div class="tipsy-arrow"></div><div class="tipsy-inner"></div>');
            }
            return this.$tip;
        },
        
        validate: function() {
            if (!this.$element[0].parentNode) {
                this.hide();
                this.$element = null;
                this.options = null;
            }
        },
        
        enable: function() { this.enabled = true; },
        disable: function() { this.enabled = false; },
        toggleEnabled: function() { this.enabled = !this.enabled; }
    };
    
    $.fn.tipsy = function(options) {
        
        if (options === true) {
            return this.data('tipsy');
        } else if (typeof options == 'string') {
            var tipsy = this.data('tipsy');
            if (tipsy) tipsy[options]();
            return this;
        }
        
        options = $.extend({}, $.fn.tipsy.defaults, options);
        
        function get(ele) {
            var tipsy = $.data(ele, 'tipsy');
            if (!tipsy) {
                tipsy = new Tipsy(ele, $.fn.tipsy.elementOptions(ele, options));
                $.data(ele, 'tipsy', tipsy);
            }
            return tipsy;
        }
        
        function enter() {
            var tipsy = get(this);
            tipsy.hoverState = 'in';
            if (options.delayIn == 0) {
                tipsy.show();
            } else {
                tipsy.fixTitle();
                setTimeout(function() { if (tipsy.hoverState == 'in') tipsy.show(); }, options.delayIn);
            }
        };
        
        function leave() {
            var tipsy = get(this);
            tipsy.hoverState = 'out';
            if (options.delayOut == 0) {
                tipsy.hide();
            } else {
                setTimeout(function() { if (tipsy.hoverState == 'out') tipsy.hide(); }, options.delayOut);
            }
        };
        
        if (!options.live) this.each(function() { get(this); });
        
        if (options.trigger != 'manual') {
            var binder   = options.live ? 'live' : 'bind',
                eventIn  = options.trigger == 'hover' ? 'mouseenter' : 'focus',
                eventOut = options.trigger == 'hover' ? 'mouseleave' : 'blur';
            this[binder](eventIn, enter)[binder](eventOut, leave);
        }
        
        return this;
        
    };
    
    $.fn.tipsy.defaults = {
        delayIn: 0,
        delayOut: 0,
        fade: false,
        fallback: '',
        gravity: 'nwe',
        html: false,
        live: false,
        offset: 0,
        opacity: 0.8,
        title: 'title',
        trigger: 'hover'
    };
    
    // Overwrite this method to provide options on a per-element basis.
    // For example, you could store the gravity in a 'tipsy-gravity' attribute:
    // return $.extend({}, options, {gravity: $(ele).attr('tipsy-gravity') || 'n' });
    // (remember - do not modify 'options' in place!)
    $.fn.tipsy.elementOptions = function(ele, options) {
        return $.metadata ? $.extend({}, options, $(ele).metadata()) : options;
    };
    
    $.fn.tipsy.autoNS = function() {
        return $(this).offset().top > ($(document).scrollTop() + $(window).height() / 2) ? 's' : 'n';
    };
    
    $.fn.tipsy.autoWE = function() {
        return $(this).offset().left > ($(document).scrollLeft() + $(window).width() / 2) ? 'e' : 'w';
    };
    
})(jQuery);
